//  EZBadgeView.m
//  © Lucid Vapor LLC 2012
//
//  control.support@lucidvapor.com
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
//  INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
//  PARTICULAR PURPOSE AND NONINFRINGEMENT OF THIRD PARTY RIGHTS. IN NO EVENT SHALL 
//  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
//  WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN 
//  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
//  This source code is not to be distributed to any parties other than the purchasing
//  license holder(s).  Contact control.support@lucidvapor.com for any support 


#import "EZBadgeView.h"

#define VERTICAL_PADDING_RATIO 0.2f
#define HORIZONTAL_PADDING_RATIO 0.47f
#define SHADOW_OFFSET 3.0f

#define TRADITIONAL_RED_BADGE_COLOR [UIColor colorWithRed:220.0/255.0 green:0.0/255.0 blue:11.0/255.0 alpha:1.0]
#define TRADITIONAL_BLUE_GRAY_BADGE_COLOR [UIColor colorWithRed:135.0/255.0 green:148.0/255.0 blue:175.0/255.0 alpha:1.0]

#define BASE_DARK_MULTIPLIER 0.8f


@interface EZBadgeView ()
-(void)setupProperties;
-(void)readjustFrames;
CGGradientRef createWhiteGradient(void);
CGGradientRef createDarkGradient(UIColor * backgroundColor);
BOOL iOS_5(void);
-(void)autoRefresh;
@property (nonatomic, retain) UILabel *textLabel;
@property (nonatomic, assign) CGFloat cornerRadius;
@end

@implementation EZBadgeView

@synthesize badgeAlignment = _badgeAlignment;
@synthesize badgeBackgroundColor = _badgeBackgroundColor;
@synthesize badgeBorderColor = _badgeBorderColor;
@synthesize badgeBorderWidth = _badgeBorderWidth;
@synthesize badgeIntValue = _badgeIntValue;
@synthesize badgeTextColor = _badgeTextColor;
@synthesize badgeTextFont = _badgeTextFont;
@synthesize badgeValue = _badgeValue;
@synthesize maximumWidth = _maximumWidth;
@synthesize shouldAutomaticallyRefresh = _shouldAutomaticallyRefresh;
@synthesize shouldShowBorder = _shouldShowBorder;
@synthesize shouldShowGradient = _shouldShowGradient;
@synthesize shouldShowShadow = _shouldShowShadow;
@synthesize shouldShowShine = _shouldShowShine;
@synthesize textLabel = _textLabel;
@synthesize cornerRadius = _cornerRadius;
@synthesize userInfo = _userInfo;


#pragma mark - Drawing

- (void)drawRect:(CGRect)rect
{
	CGContextRef ctx = UIGraphicsGetCurrentContext();
	
	CGRect innerRect = self.bounds;
	innerRect.size.height -= SHADOW_OFFSET;
	
	CGRect borderRect = CGRectInset(innerRect, self.badgeBorderWidth / 2.0, self.badgeBorderWidth / 2.0);
	
	
	// fills a rounded rect and draws the border
	CGSize shadowOffset = CGSizeMake (0,  SHADOW_OFFSET);
	
	
	UIBezierPath *mainBackgroundPath = [UIBezierPath bezierPathWithRoundedRect:borderRect cornerRadius:self.cornerRadius];
	mainBackgroundPath.lineWidth = self.badgeBorderWidth;
	
	if (self.shouldShowGradient) 
	{
		// makes a gradient layer based on the background
		// color and an attempt to darken it toward the 
		// bottom
		CGContextSaveGState(ctx);
		
		if (self.shouldShowShadow) 
		{
			CGContextSetShadow(ctx, shadowOffset, 1.0);
		}
		
		CGContextBeginTransparencyLayer(ctx, NULL);
		
		CGGradientRef darkGradient = createDarkGradient(self.badgeBackgroundColor);
		CGPoint darkGradientStart = CGPointMake(0.0, 0.0);
		CGPoint darkGradientEnd = CGPointMake(0.0, CGRectGetHeight(innerRect));
		
		
		CGContextAddPath(ctx, mainBackgroundPath.CGPath);
		CGContextClip(ctx);
		
		CGContextDrawLinearGradient (ctx, darkGradient, darkGradientStart, darkGradientEnd, 0);
		
		CGContextEndTransparencyLayer (ctx);
		
		CGGradientRelease(darkGradient);
		
		CGContextRestoreGState(ctx);
	}
	else
	{
		// else just make a solid
		// background color
		CGContextSaveGState(ctx);
		
		
		if (self.shouldShowShadow) 
		{
			CGContextSetShadow(ctx, shadowOffset, 1.0);
		}
		[self.badgeBackgroundColor set];
		[mainBackgroundPath fill];
		
		CGContextRestoreGState(ctx);
	}
	
	
	

	if (self.shouldShowBorder) 
	{
		[self.badgeBorderColor set];
		[mainBackgroundPath stroke];
	}
	
	
	
	
	if (self.shouldShowShine) 
	{
		// this draws the shine
		CGContextSaveGState(ctx);
		
		
		// create a layer
		CGContextBeginTransparencyLayer(ctx, NULL);
		
		
		UIBezierPath *shineMask;
		if (self.shouldShowBorder) 
		{
			
			shineMask = [UIBezierPath bezierPathWithRoundedRect:innerRect cornerRadius:self.cornerRadius];
		}
		else
		{
			CGRect clipRect = CGRectInset(innerRect, self.badgeBorderWidth / 2.0, self.badgeBorderWidth / 2.0);
			shineMask = [UIBezierPath bezierPathWithRoundedRect:clipRect cornerRadius:self.cornerRadius];
		}
		
		// clip this layer the rounded rect so we don't draw outside of that
		CGContextAddPath(ctx, shineMask.CGPath);
		CGContextClip(ctx);
		
		
		// make a gradient that fades from transparent white to solid white
		CGGradientRef shineGradient = createWhiteGradient();
		CGPoint gradientStart = CGPointMake(0.0, 0.0);
		CGPoint gradientEnd = CGPointMake(0.0, CGRectGetHeight(innerRect));
		
		
		
		
		
		// create a circle, which is the actual shape that we're going to 
		// make the gradient
		// circle must be larger than the rect so as to get a nice curve
		// to the shine
		CGFloat circleMultiplier = 2.0;
		CGRect largeCircleRect = CGRectMake(0, 0, CGRectGetWidth(innerRect) * circleMultiplier,  CGRectGetHeight(innerRect) * circleMultiplier);
		
		// going to shift the circle up so that the bottom touches the half way point
		// of the badge. However, it looks a little better if it goes a little further
		// down than exactly half way. This adds a spill over buffer of about 7.5%
		CGFloat shineSpill = CGRectGetHeight(innerRect) * .075;
		
		// calculate the amount of pixels to move the circle up and over
		CGFloat upSpace = (CGRectGetHeight(largeCircleRect) - CGRectGetHeight(innerRect)) + (CGRectGetHeight(innerRect) / 2.0) - shineSpill;
		CGFloat sideSpace = (CGRectGetWidth(largeCircleRect) - CGRectGetWidth(innerRect)) / 2.0;
		largeCircleRect.origin.y -= (int)upSpace;
		largeCircleRect.origin.x -= sideSpace;
		
		
		// clip the context again by the this circle
		CGContextAddEllipseInRect(ctx, largeCircleRect);
		CGContextClip(ctx);
		
		// finally draw the gradient
		CGContextDrawLinearGradient (ctx, shineGradient, gradientStart, gradientEnd, 0);
		
		// close the layer
		CGContextEndTransparencyLayer (ctx);
		CGContextRestoreGState(ctx);
		
		CGGradientRelease(shineGradient);
		
		
		// The following is an alternate way to make the shine.
		// It actually creates a rounded rect as the shape of the
		// shine. Leaving here as an option or teaching tool.
		/*
		// create a rounded rect that has a smaller corner radius so
		// that it appears to shine at a more horizontal angle
		CGRect shineRect = innerRect;

		// going to shift the rect up so that the bottom touches the half way point
		// of the badge. However, it looks a little better if it goes a little further
		// down than exactly half way. This adds a spill over buffer of about 7.5%
		CGFloat shineSpill = CGRectGetHeight(innerRect) * .075;
		
		// calculate the amount of pixels to move the rect up and over
		CGFloat upSpace = CGRectGetHeight(shineRect) / 2.0 - shineSpill;
		shineRect.origin.y -= roundf(upSpace * 2.0f) / 2.0f; //round to nearest half
		
		// calculate the new radius of the shine corners
		CGFloat shineRadius = roundf((self.cornerRadius / 1.5) * 2.0f) / 2.0f; //round to nearest half
		UIBezierPath *shinePath = [UIBezierPath bezierPathWithRoundedRect:shineRect cornerRadius:shineRadius];
		
		// clip the context again by the larger rounded rect
		CGContextAddPath(ctx, shinePath.CGPath);
		CGContextClip(ctx);
		
		// finally draw the gradient
		CGContextDrawLinearGradient (ctx, shineGradient, gradientStart, gradientEnd, 0);
		
		// close the layer
		CGContextEndTransparencyLayer (ctx);
		CGContextRestoreGState(ctx);
		
		CGGradientRelease(shineGradient);
		*/
		
		
	}
	
	
	
}

CGGradientRef createWhiteGradient (void)
{
	CGGradientRef gradient;
	CGColorSpaceRef colorspace;
	size_t num_locations = 2;
	CGFloat locations[2] = { 0.0, 0.4 };
	CGFloat components[8] = { 1.0, 1.0, 1.0, 1.0,  // Start color
		1.0, 1.0, 1.0, 0.25 }; // End color

	
	colorspace = CGColorSpaceCreateDeviceRGB();
	gradient = CGGradientCreateWithColorComponents (colorspace, components,
													  locations, num_locations);

	CGColorSpaceRelease(colorspace);
	return gradient;
}

CGGradientRef createDarkGradient (UIColor * backgroundColor)
{
	CGGradientRef gradient;
	CGColorSpaceRef colorspace;
	CGFloat locations[2] = { 0.5, 1.0 };
	
	CGColorRef badgeColor = backgroundColor.CGColor;
	CGColorRef darkShade;
	
	// if iOS5 or greater is being used, use HSB to
	// make a slightly darker gradient
	if (iOS_5()) 
	{
		colorspace = CGColorSpaceCreateDeviceRGB();
		CGFloat hue;
		CGFloat saturation;
		CGFloat brightness;
		CGFloat alpha;
		
		[backgroundColor getHue:&hue saturation:&saturation brightness:&brightness alpha:&alpha];
		UIColor *darkUIColor = [UIColor colorWithHue:hue saturation:saturation brightness:brightness * BASE_DARK_MULTIPLIER alpha:1.0];
		darkShade = darkUIColor.CGColor;
		
		const void *colorRefs[2] = {badgeColor, darkShade};
		CFArrayRef colorArray = CFArrayCreate(kCFAllocatorDefault, colorRefs, 2, &kCFTypeArrayCallBacks);
		
		gradient = CGGradientCreateWithColors(colorspace, colorArray, locations);
		
		CFRelease(colorArray);
		CGColorSpaceRelease(colorspace);
		return gradient;
	}
	else
	{
		// sigh. Trying to get a darker shade with RGB sucks
		int numComponents = CGColorGetNumberOfComponents(badgeColor);
		if (numComponents == 4) 
		{
			colorspace = CGColorSpaceCreateDeviceRGB();
			const CGFloat *existingComponents = CGColorGetComponents(badgeColor);
			CGFloat red = existingComponents[0];
			CGFloat green = existingComponents[1];
			CGFloat blue = existingComponents[2];
			CGFloat alpha = existingComponents[3];
			
			red = red * BASE_DARK_MULTIPLIER;
			green = green * BASE_DARK_MULTIPLIER;
			blue = blue * BASE_DARK_MULTIPLIER;
			
			CGFloat darkComponents[4] = {red, green, blue, alpha};
			
			darkShade = CGColorCreate(colorspace, darkComponents);
			
			const void *colorRefs[2] = {badgeColor, darkShade};
			CFArrayRef colorArray = CFArrayCreate(kCFAllocatorDefault, colorRefs, 2, &kCFTypeArrayCallBacks);
			
			gradient = CGGradientCreateWithColors(colorspace, colorArray, locations);
			
			CFRelease(colorArray);
			CGColorRelease(darkShade);
			CGColorSpaceRelease(colorspace);
			return gradient;
		}
		else if (numComponents == 2) //gray scale
		{
			colorspace = CGColorSpaceCreateDeviceGray();
			const CGFloat *existingComponents = CGColorGetComponents(badgeColor);
			CGFloat white = existingComponents[0];
			CGFloat alpha = existingComponents[1];
			
			white = white * BASE_DARK_MULTIPLIER;
			
			CGFloat darkComponents[2] = {white, alpha};
			
			darkShade = CGColorCreate(colorspace, darkComponents);
			
			const void *colorRefs[2] = {badgeColor, darkShade};
			CFArrayRef colorArray = CFArrayCreate(kCFAllocatorDefault, colorRefs, 2, &kCFTypeArrayCallBacks);
			
			gradient = CGGradientCreateWithColors(colorspace, colorArray, locations);
			
			CFRelease(colorArray);
			CGColorRelease(darkShade);
			CGColorSpaceRelease(colorspace);
			return gradient;
		}
		else // just return the same two colors
		{
			colorspace = CGColorSpaceCreateDeviceRGB();
			const void *colorRefs[2] = {badgeColor, badgeColor};
			CFArrayRef colorArray = CFArrayCreate(kCFAllocatorDefault, colorRefs, 2, &kCFTypeArrayCallBacks);
			gradient = CGGradientCreateWithColors(colorspace, colorArray, locations);
			
			CFRelease(colorArray);
			CGColorSpaceRelease(colorspace);
			
			return gradient;
		}
	}
	return gradient;
}

							
BOOL iOS_5(void)
{
	NSString *osVersion = @"5.0";
	NSString *currOsVersion = [[UIDevice currentDevice] systemVersion];
	return [currOsVersion compare:osVersion] != NSOrderedAscending;
}


#pragma mark - Exposed Methods


+(UIColor *)classicRedColor
{
	return TRADITIONAL_RED_BADGE_COLOR;
}

+(UIColor *)classicBlueGrayColor
{
	return TRADITIONAL_BLUE_GRAY_BADGE_COLOR;
}


-(void)makePlain
{
	self.shouldShowBorder = NO;
	self.shouldShowShine = NO;
	self.shouldShowGradient = NO;
	self.shouldShowShadow = NO;
	[self autoRefresh];
}

-(void)makeFancy
{
	self.shouldShowBorder = YES;
	self.shouldShowShine = YES;
	self.shouldShowGradient = YES;
	self.shouldShowShadow = YES;
	[self autoRefresh];
}


-(void)setBadgeIntValue:(int)badgeIntValue
{
	self.badgeValue = [NSString stringWithFormat:@"%d", badgeIntValue];
}

-(int)badgeIntValue
{
	return [self.badgeValue intValue];
}

-(void)setBadgeTextColor:(UIColor *)badgeTextColor
{
	if (badgeTextColor != _badgeTextColor) 
	{
		[_badgeTextColor release];
		_badgeTextColor = [badgeTextColor retain];
		self.textLabel.textColor = badgeTextColor;
		[self autoRefresh];
	}
}

-(void)setBadgeTextFont:(UIFont *)badgeTextFont
{
	if (badgeTextFont != _badgeTextFont) 
	{
		[_badgeTextFont release];
		_badgeTextFont = [badgeTextFont retain];
		self.textLabel.font = badgeTextFont;
		[self autoRefresh];
	}
}

-(void)setBadgeValue:(NSString *)badgeValue
{
	if (badgeValue == nil) 
	{
		self.hidden = YES;
	}
	else
	{
		self.hidden = NO;
	}
	if (badgeValue != _badgeValue) 
	{
		[_badgeValue release];
		_badgeValue = [badgeValue retain];
		self.textLabel.text = badgeValue;
		[self autoRefresh];
	}
}

-(void)setMaximumWidth:(CGFloat)maximumWidth
{
	if (maximumWidth != _maximumWidth) 
	{
		_maximumWidth = maximumWidth;
		[self autoRefresh];
	}
}


-(void)setShouldShowBorder:(BOOL)shouldShowBorder
{
	if (shouldShowBorder != _shouldShowBorder) 
	{
		_shouldShowBorder = shouldShowBorder;
		[self autoRefresh];
	}
}

-(void)setShouldShowGradient:(BOOL)shouldShowGradient
{
	if (shouldShowGradient != _shouldShowGradient) 
	{
		_shouldShowGradient = shouldShowGradient;
		[self autoRefresh];
	}
}

-(void)setShouldShowShadow:(BOOL)shouldShowShadow
{
	if (shouldShowShadow != _shouldShowShadow) 
	{
		_shouldShowShadow = shouldShowShadow;
		[self autoRefresh];
	}
}

-(void)setShouldShowShine:(BOOL)shouldShowShine
{
	if (shouldShowShine != _shouldShowShine) 
	{
		_shouldShowShine = shouldShowShine;
		[self autoRefresh];
	}
}

-(void)setBadgeBorderColor:(UIColor *)badgeBorderColor
{
	if (badgeBorderColor != _badgeBorderColor) 
	{
		[_badgeBorderColor release];
		_badgeBorderColor = [badgeBorderColor retain];
		[self autoRefresh];
	}
}

-(void)setBadgeBackgroundColor:(UIColor *)badgeBackgroundColor
{
	if (badgeBackgroundColor != _badgeBackgroundColor) 
	{
		[_badgeBackgroundColor release];
		_badgeBackgroundColor = [badgeBackgroundColor retain];
		[self autoRefresh];
	}
}

-(void)setBadgeBorderWidth:(CGFloat)badgeBorderWidth
{
	if (badgeBorderWidth != _badgeBorderWidth) 
	{
		_badgeBorderWidth = badgeBorderWidth;
		[self autoRefresh];
	}
}

-(void)refresh
{
	[self readjustFrames];
	[self setNeedsDisplay];
}

-(void)autoRefresh
{
	if (self.shouldAutomaticallyRefresh) 
	{
		[self refresh];
	}
}


// Readjusts the size based on text size. Also readjusts
// the origin based on the alignment.
-(void)readjustFrames
{
	if (self.hidden) 
	{
		return;
	}
	NSString *text;
	if (!self.textLabel.text || [self.textLabel.text length] == 0) 
	{
		text = @"1"; // place holder so that the badge doesn't look too skinny
	}
	else
	{
		text = self.textLabel.text;
	}
	
	// get the text size
	CGSize labelSize = [text sizeWithFont:self.textLabel.font
									constrainedToSize:CGSizeMake(self.maximumWidth, 0)
										lineBreakMode:NSLineBreakByTruncatingTail];
	
	CGRect labelFrame = self.textLabel.frame;
	CGRect frame = self.frame;
	
	CGFloat verticalPadding = floorf(self.textLabel.font.lineHeight * VERTICAL_PADDING_RATIO);
	CGFloat horizontalPadding = floorf(self.textLabel.font.lineHeight * HORIZONTAL_PADDING_RATIO);
	labelFrame.size.width = labelSize.width;
	labelFrame.size.height = labelSize.height;
	labelFrame.origin.x = horizontalPadding;
	labelFrame.origin.y = verticalPadding;
	self.textLabel.frame = labelFrame;
	
	// adjust the corner radius so that it becomes an "oval" so to speak
	self.cornerRadius = ((self.textLabel.frame.size.height + verticalPadding * 2) / 2.0);
	
	// add the totality of the space for the entire frame size
	frame.size.height = self.textLabel.bounds.size.height + (verticalPadding * 2) + SHADOW_OFFSET;
	frame.size.width = self.textLabel.bounds.size.width + (horizontalPadding * 2);
	
	// adjust the origin based on the alignment
	switch (self.badgeAlignment) 
	{
		case EZBadgeAlignmentLeft:
			
			break;
		case EZBadgeAlignmentCenter:
			frame.origin.x -= (int)(frame.size.width / 2.0 - self.frame.size.width / 2.0);
			break;
		case EZBadgeAlignmentRight:
		{
			CGFloat rightEdge = CGRectGetMaxX(self.frame);
			frame.origin.x = (int)(rightEdge - frame.size.width);
			break;
		}
		default:
			break;
	}
	self.frame = frame;
}


#pragma mark - Memory

-(void)dealloc
{
	[_badgeBackgroundColor release];
	[_badgeBorderColor release];
	[_badgeTextColor release];
	[_badgeTextFont release];
	[_badgeValue release];
	[_textLabel release];
	[_userInfo release];
	[super dealloc];
}


#pragma mark - Initialization

-(id)init
{
	return [self initWithFrame:CGRectZero];
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setupProperties];
    }
    return self;
}

-(id)initWithRightEdge:(CGFloat)rightEdge
{
	return [self initWithFrame:CGRectMake(0, 0, rightEdge, 0)];
}
-(id)initWithTopRightPoint:(CGPoint)topRight
{
	return [self initWithFrame:CGRectMake(0, topRight.y, topRight.x, 0)];
}

-(id)initWithPlainBackgroundColor:(UIColor *)backgroundColor textColor:(UIColor *)textColor
{
	self = [self init];
	if (self) 
	{
		[self makePlain];
		self.badgeBackgroundColor = backgroundColor;
		self.badgeTextColor = textColor;
		
	}
	return self;
}

-(void)awakeFromNib
{
	[self setupProperties];
}

-(void)setupProperties
{
	self.textLabel = [[[UILabel alloc] init] autorelease];
	self.textLabel.backgroundColor = [UIColor clearColor];
	self.textLabel.lineBreakMode = NSLineBreakByTruncatingTail;
	self.textLabel.numberOfLines = 1;
	self.textLabel.textAlignment = NSTextAlignmentCenter;
	self.textLabel.adjustsFontSizeToFitWidth = NO;
	[self addSubview:self.textLabel];
	
	// setting this to NO but then YES at the end
	self.shouldAutomaticallyRefresh = NO;
	
	// doing this so that if someone doesn't enter
	// a frame it will give them a 0,0 origin
	// but then setting it back to the default
	// of alignment right after it adjusts itself
	if (CGRectEqualToRect(self.frame, CGRectZero)) 
	{
		self.badgeAlignment = EZBadgeAlignmentLeft;
	}
	else
	{
		self.badgeAlignment = EZBadgeAlignmentRight;
	}
	self.badgeBackgroundColor = TRADITIONAL_RED_BADGE_COLOR; 
	self.badgeBorderColor = [UIColor whiteColor];
	self.badgeBorderWidth = 2.f;
	self.badgeTextFont = [UIFont fontWithName:@"Helvetica-Bold" size:11.0f];
	self.badgeTextColor = [UIColor whiteColor];
	self.maximumWidth = MAXFLOAT;
	self.shouldShowBorder = YES;
	self.shouldShowGradient = YES;
	self.shouldShowShadow = YES;
	self.shouldShowShine = YES;
	self.backgroundColor = [UIColor clearColor];
	self.badgeValue = @"";
	self.shouldAutomaticallyRefresh = YES;
	
	[self refresh];
	
	// setting it back to default alignment. See above.
	self.badgeAlignment = EZBadgeAlignmentRight;
}

-(NSString *)description
{
	return [NSString stringWithFormat:@"\n badgeAlignment:%d\n badgeBackgroundColor:%@\n badgeBorderColor:%@\n badgeBorderWidth:%f\n badgeTextColor:%@\n badgeTextFont:%@,%f\n badgeValue:%@\n maximumWidth:%f\n shouldShowBorder:%d\n shouldShowGradient:%d\n shouldShowShadow:%d\n shouldShowShine:%d\n lineHeight:%f\n frame:%f,%f,%f,%f\n",
		  self.badgeAlignment, self.badgeBackgroundColor, self.badgeBorderColor, self.badgeBorderWidth, self.badgeTextColor, self.badgeTextFont.fontName, self.badgeTextFont.pointSize, self.badgeValue, self.maximumWidth, self.shouldShowBorder,
		  self.shouldShowGradient, self.shouldShowShadow, self.shouldShowShine, self.badgeTextFont.lineHeight, self.frame.origin.x, self.frame.origin.y, self.frame.size.width, self.frame.size.height];
}


@end
