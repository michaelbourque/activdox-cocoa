//  EZBadgeView.h
//  © Lucid Vapor LLC 2012
//
//  control.support@lucidvapor.com
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
//  INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
//  PARTICULAR PURPOSE AND NONINFRINGEMENT OF THIRD PARTY RIGHTS. IN NO EVENT SHALL 
//  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
//  WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN 
//  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
//  This source code is not to be distributed to any parties other than the purchasing
//  license holder(s).  Contact control.support@lucidvapor.com for any support 


#import <UIKit/UIKit.h>

/**
 EZBadgeView is an easy to use badge similar to the badges found in UIKit.  
 It is fully customizable, self contained, and easy to install.  
 No need for external images.  No need to link or import dependent libraries.
 
 See [EZBadgeView How-To](http://www.lucidvapor.com/controls/documentation/EZBadgeView/howto/)
 for installation and use instructions.
 */

 

typedef enum EZBadgeAlignment
{
	EZBadgeAlignmentLeft = 1,
	EZBadgeAlignmentRight = 2,
	EZBadgeAlignmentCenter = 3
} EZBadgeAlignment;



@interface EZBadgeView : UIView




/** @name Configuring the Badge's Frame */

/**
 The alignment method used when resizing the badge.
 
 The badge will resize itself based on various settings.
 Using the alignment will ensure your view does not spill
 over into unwanted areas. The default value is *EZBadgeAlignmentRight*
 which keeps the right side of the badge stationary, and extends
 leftward as it grows in width. Other values are *EZBadgeAlignmentLeft*
 and *EZBadgeAlignmentCenter*.
 */
@property (nonatomic, assign) EZBadgeAlignment badgeAlignment;


/**
 The maximum width that the badge can grow in size.
 
 Use this to limit the width of the badge. If the badgeValue
 text is larger than this value, a truncation
 will occur with trailing *"..."* text. Default value is MAX_FLOAT.
 */
@property (nonatomic, assign) CGFloat maximumWidth;


/**
 The font used for displaying text inside the badge.
 
 This property has the most impact on vertical size. Set this
 to the appropriate UIFont with size.  Default is Helvetica-Bold
 11.0 pt.
 */
@property (nonatomic, retain) UIFont *badgeTextFont;







/** @name Configuring the Badge's Visual Appearance */

/**
 The border line width when shouldShowBorder is set to YES.
 
 Default value is 2.0.
 */
@property (nonatomic, assign) CGFloat badgeBorderWidth;


/**
 The flag used to determine whether to display a colored
 border around the badge.
 
 Default value is YES.
 */
@property (nonatomic, assign) BOOL shouldShowBorder;


/**
 The flag used to determine whether to display a slight
 gradient to the badge's main background color.
 
 Default value is YES.
 */
@property (nonatomic, assign) BOOL shouldShowGradient;


/**
 The flag used to determine whether to display a shadow
 under the badge.
 
 Default value is YES.
 */
@property (nonatomic, assign) BOOL shouldShowShadow;


/**
 The flag used to determine whether to display a shine
 effect over the badge.
 
 Default value is YES.
 */
@property (nonatomic, assign) BOOL shouldShowShine;


/**
 The color used as the badge's main background color.
 
 Default value is [EZBadgeView classicRedColor].
 */
@property (nonatomic, retain) UIColor *badgeBackgroundColor;


/**
 The color used as the badge's border color.
 
 Default value is [UIColor whiteColor].
 */
@property (nonatomic, retain) UIColor *badgeBorderColor;



/**
 The color used as the badge's text color.
 
 Default value is [UIColor whiteColor].
 */
@property (nonatomic, retain) UIColor *badgeTextColor;






/** @name Accessing the Badge's Text Attributes */

/**
 A convenience method to accessing the badge's text using integers.
 
 This property can be used instead of badgeValue if only setting integer
 values.  This makes it easier to set/get the integer value of the badge
 without having to cast and/or format to NSString.
 */
@property (nonatomic, assign) int badgeIntValue;


/**
 The text displaying in the badge.
 
 Default is a blank string, which is not the same as nil.  Setting this
 property to nil, hides the badge completely by setting the *hidden* property
 to YES.
 */
@property (nonatomic, retain) NSString *badgeValue;






/** @name Custom User Generated Information */

/**
 An optional property to be used for anything custom.
 
 Use this to attach any custom information along with this badge. The
 *tag* property of *UIView* can also be used for simplier custom information.
 */
@property (nonatomic, retain) NSDictionary *userInfo;







/** @name Updating the UI */

/**
 Updates the display should there be a need to force a refresh.
 
 This should not need to be called unless shouldAutomaticallyRefresh is set to NO. Call this
 method instead of setNeedsDisplay if needed.
 */
-(void)refresh;

/**
 The flag used to determine whether to refresh the appearance 
 after changing certain properties.
 
 This should only be set to NO if an increase in performance is absolutely needed.
 For instance, setting this property to NO, then subsequently changing the visual
 properties of the badge will require a you to call refresh in order to update the
 badge's display.  This will save some computation in the process. Default value is YES.
 */
@property (nonatomic, assign) BOOL shouldAutomaticallyRefresh;






/** @name Initiializing a New Badge */

/**
 Initializes and returns an allocated and automatically turns
 off all effects such as shine, border, gradient, and shadow.
 
 @param backgroundColor The solid background color of the badge.
 @param textColor The color of the text displayed in the badge.
 @return An initialized badge or nil if the object couldn't be created.
 
 This would be the option to use when creating a plain badge similar to what appears in the Mail app from Apple.
 Further, you can set the background color to [EZBadgeView classicBlueGrayColor] to obtain the same 
 appearance as the Mail app. See also makePlain.
 */
-(id)initWithPlainBackgroundColor:(UIColor *)backgroundColor textColor:(UIColor *)textColor;


/**
 Initializes and returns an allocated badge that is right aligned to a given x value. 
 
 @param rightEdge The right most x value to allow the badge to expand.
 @return An initialized badge or nil if the object couldn't be created.
 
 The y origin value of this badge's frame will be zero.
 */
-(id)initWithRightEdge:(CGFloat)rightEdge;


/**
 Initializes and returns an allocated badge that is right aligned and positioned to a given point. 
 
 @param topRight The most top-right x,y coordinate to all the badge to expand.
 @return An initialized badge or nil if the object couldn't be created.
 
 The top right corner of this badge's view will always "stick" to what is passed here.
 */
-(id)initWithTopRightPoint:(CGPoint)topRight;








/** @name Convenient property defaults */


/**
 Convenience method to turn off most effects
 
 See also initWithPlainBackgroundColor:textColor:. The opposite effect of calling makeFancy.
 */
-(void)makePlain;


/**
 Convenience method to turn on most effects
 
 The opposite effect of calling makePlain.
 */
-(void)makeFancy;








/** @name Common Colors */

/**
 The classic red color used in UIKit badges.
 
 @return A UIColor object representing the traditional red badge color.
 */
+(UIColor *)classicRedColor;

/**
 The classic blue-gray color used in the Mail app.
 
 @return A UIColor object representing the traditional blue-gray badge color.
 */
+(UIColor *)classicBlueGrayColor;

@end
