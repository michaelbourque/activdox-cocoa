//
//  PSPDFSinglePageViewController.m
//  PSPDFKit
//
//  Copyright (c) 2011-2012 Peter Steinberger. All rights reserved.
//

#import "PSPDFSinglePageViewController.h"
#import "PSPDFPageViewController.h"
#import "PSPDFViewController.h"
#import "PSPDFViewController+Internal.h"
#import "PSPDFKitGlobal+Internal.h"
#import "PSPDFScrollView.h"
#import "PSPDFPageView.h"
#import "PSPDFDocument.h"
#import "PSPDFConverter.h"
#import <QuartzCore/QuartzCore.h>

// provides write-access to properites, needed in PSPDFPage
@interface PSPDFPageView (PSPDFSinglePageInternal)
@property (nonatomic, assign) NSUInteger page;
@property (nonatomic, strong) PSPDFDocument *document;
@end

@implementation PSPDFSinglePageView @end

@implementation PSPDFSinglePageViewController {
    BOOL _isVisible;
    BOOL _needsRedraw;
}

static char *PSPDFSinglePageViewContext;

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (id)initWithPDFController:(PSPDFViewController *)pdfController page:(NSUInteger)page {
    if ((self = [super init])) {
        _pdfController = pdfController;
        _page = page;
        [self addObserver:self forKeyPath:NSStringFromSelector(@selector(parentViewController)) options:0 context:PSPDFSinglePageViewContext];
    }
    return self;
}

- (void)dealloc {
    [self removeObserver:self forKeyPath:NSStringFromSelector(@selector(parentViewController)) context:PSPDFSinglePageViewContext];
    [_delegate pspdfSinglePageViewControllerWillDealloc:self];
    self.pdfController = nil;
}

- (NSString *)description {
    return [NSString stringWithFormat:@"<%@ %p page:%d frame:%@ pageView:%@>", NSStringFromClass([self class]), self, self.page, NSStringFromCGRect([self isViewLoaded] ? self.view.frame : CGRectZero), self.pageView];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    if (context == PSPDFSinglePageViewContext) {
        if (!self.parentViewController && !_isVisible) {
            [_delegate pspdfSinglePageViewControllerReadyForReuse:self];
        }
    }else {
        [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
    }
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UIViewController

- (void)loadView {
    self.view = [[PSPDFSinglePageView alloc] initWithFrame:CGRectZero];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // transparency doesn't work very well here
    if (_useSolidBackground) {
        self.view.backgroundColor = self.pdfController.backgroundColor;
    }
    
    if (kPSPDFDebugScrollViews) {
        self.view.backgroundColor = [UIColor purpleColor];
        self.view.alpha = 0.7;
    }
    
    // don't load content if we're on an invalid page
    _pageView = [[[self.pdfController classForClass:[PSPDFPageView class]] alloc] initWithFrame:self.view.bounds pdfController:self.pdfController];
    _pageView.frame = self.view.bounds;
    _pageView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    _pageView.shadowEnabled = self.pdfController.isShadowEnabled;

    if (_page < [_pdfController.document pageCount]) {
        _pageView.page = _page;
    }else if (_page != -1 && [_pdfController.document isValid]) {
        PSPDFLogWarning(@"Invalid page %d for document %@ with pageCount: %d", _page, _pdfController.document.title, [_pdfController.document pageCount]);
    }
    
    _pageView.document = _pdfController.document;
    _pageView.pdfController = self.pdfController;
    
    [self layoutPage];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    _isVisible = YES;

    if (_needsRedraw) {
        _needsRedraw = NO;
        [self layoutPage];
    }
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    _isVisible = NO;

    // if we get disappeared and are not attached, we can insta-reuse the controller.
    if (!self.parentViewController) {
        [_delegate pspdfSinglePageViewControllerReadyForReuse:self];
    }
}

- (void)prepareForReuse {
    [_pageView prepareForReuse];
    _pageView.hidden = YES;
    _page = -1;
    _pdfController = nil;
}

- (void)updateShadow {
    BOOL doublePageModeOnFirstPage = self.pdfController.doublePageModeOnFirstPage;
    BOOL isRightAligned = NO;
    if ([self.pdfController isDoublePageMode]) {
        isRightAligned = !self.pageView.isRightPage;
    }
    
    // update shadow depending on position
    __weak PSPDFPageView *weakPageView = _pageView;
    [_pageView setUpdateShadowBlock:^(PSPDFPageView *pageView) {
        PSPDFPageView *strongPageView = weakPageView;
        CALayer *backgroundLayer = pageView.layer;
        BOOL shouldHideShadow = strongPageView.pdfController.isRotationActive;
        backgroundLayer.shadowOpacity = shouldHideShadow ? 0.f : strongPageView.shadowOpacity;
        CGSize size = pageView.bounds.size; 
        CGFloat moveShadow = -12;
        CGRect bezierRect = CGRectMake(moveShadow, moveShadow, size.width+fabs(moveShadow/2), size.height+fabs(moveShadow/2));
        
        if ([strongPageView.pdfController isDoublePageMode]) {
            // don't trunicate shadow if we open the document.
            if (!isRightAligned && (doublePageModeOnFirstPage || pageView.page > 0)) {
                bezierRect = CGRectMake(0, moveShadow, size.width+fabs(moveShadow/2)+moveShadow, size.height+fabs(moveShadow/2));
            }else {
                bezierRect = CGRectMake(moveShadow, moveShadow, size.width+fabs(moveShadow/2)+moveShadow, size.height+fabs(moveShadow/2));
            }
        }
        
        UIBezierPath *path = [UIBezierPath bezierPathWithRect:CGRectIntegral(bezierRect)];
        backgroundLayer.shadowPath = path.CGPath;
    }];
}

// Initially adds the view, later only re-calculates the frame
- (void)layoutPage {
    // stop on invalid pages
    // Note: we can't check for self.parentViewController here, as the UIPageViewController sets this at a later point.
    if (_page == NSUIntegerMax || _page >= [_pdfController.document pageCount]) {
        _pageView.hidden = YES;
        return;
    }
    _pageView.hidden = NO;
    
    PSPDFDocument *document = _pdfController.document;
    CGRect pageRect = [document rectBoxForPage:_page];
    
    // use superview, as the view is changed to fit the pages
    CGSize boundsSize = UIEdgeInsetsInsetRect(_pdfController.view.bounds, self.pdfController.margin).size;
    
    // as we "steal" the coordintes from above PSPDFPageViewController, re-calculate our real space
    if ([_pdfController isDoublePageMode]) {
        boundsSize = CGSizeMake(boundsSize.width/2, boundsSize.height);
    }
    
    CGFloat scale = PSPDFScaleForSizeWithinSizeWithOptions(pageRect.size, boundsSize, self.pdfController.isZoomingSmallDocumentsEnabled, NO);
    
    _pageView.frame = self.view.bounds;
    // delay initially. faster scrolling, also fixes a bug where video didn't appear on landscape mode.
    BOOL delayAnnotations = self.page != _page || !_pageView.document;

    BOOL sendNewPageViewDelegate = NO;
    if (!self.pageView.superview || self.pageView.pdfController || _pageView.document != document || _pageView.page != _page) {
        [_pageView displayDocument:document page:_page pageRect:pageRect scale:scale delayPageAnnotations:delayAnnotations pdfController:_pdfController];
        sendNewPageViewDelegate = YES;
        [self updateShadow];
    }
    
    // center view and position to the center
    CGSize viewSize = self.view.bounds.size;
    CGFloat leftPos = (viewSize.width - _pageView.frame.size.width)/2;
    
    // for double page mode, align the pages like a magazine
    if ([self.pdfController isDoublePageMode]) {
        BOOL shouldAlignRight = ![self.pdfController isRightPageInDoublePageMode:_page];
        // The -1 compensates a one-pixel bleed-through of the background for certain documents.
        leftPos = shouldAlignRight ? viewSize.width-_pageView.frame.size.width : -1.f;
    }
    pageRect.size = PSPDFSizeForScale(pageRect.size, scale);
    
    _pageView.frame = CGRectMake(leftPos, roundf(viewSize.height - _pageView.frame.size.height)/2, roundf(pageRect.size.width), roundf(pageRect.size.height));
    
    // add only once
    if (!self.pageView.superview) {
        [self.view addSubview:self.pageView];
        PSPDFLogVerbose(@"site %d frame: %@ pageView:%@", self.page, NSStringFromCGRect(self.view.frame), NSStringFromCGRect(_pageView.frame));
    }

    // send delegate events
    if (sendNewPageViewDelegate) {
        [self.pdfController delegateDidLoadPageView:_pageView];
    }
}

// called when e.g. the view frame changes. Recalculate the pageView frame.
- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    [self layoutPage];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    if (![self isViewLoaded]) {
        [_pageView prepareForReuse]; // call delegate
        _pageView = nil;
    }
}

// prevent crash with: all provided view controllers (( "<PSPDFSinglePageViewController: 0x88ccbe0>" )) must support the pending interface orientation (UIInterfaceOrientationPortraitUpsideDown) 
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation {
    return YES;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public

- (void)setPage:(NSUInteger)page {
    if (page != _page) {
        _page = page;
        _needsRedraw = YES;
    }
}

- (void)setPdfController:(PSPDFViewController *)pdfController {
    if (pdfController != _pdfController) {
        
        // only call reuse here if the new pdfController is a value (to set delegate
        if (pdfController) {
            [_pageView prepareForReuse];
        }
        
        _pdfController = pdfController;
        _pageView.pdfController = pdfController;
        [_pageView prepareForReuse];
        _needsRedraw = YES;
    }
}

@end
