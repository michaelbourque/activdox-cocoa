//
//  PSPDFPatches.m
//  PSPDFKit
//
//  Copyright (c) 2012 Peter Steinberger. All rights reserved.
//

#import "PSPDFPatches.h"
#import "PSPDFKitGlobal.h"
#import "PSPDFViewController.h"
#import "PSPDFBaseViewController.h"
#import <libkern/OSAtomic.h>
#import <objc/runtime.h>
#import <objc/message.h>

#if __has_feature(objc_arc)
#error "Compile this file without ARC"
#endif

static void PSPDFSwizzleMethod(Class c, SEL orig, SEL new) {
    Method origMethod = class_getInstanceMethod(c, orig);
    Method newMethod = class_getInstanceMethod(c, new);
    if (class_addMethod(c, orig, method_getImplementation(newMethod), method_getTypeEncoding(newMethod))) {
        class_replaceMethod(c, new, method_getImplementation(origMethod), method_getTypeEncoding(origMethod));
    }else {
        method_exchangeImplementations(origMethod, newMethod);
    }
}

void PSPDFReplaceMethod(Class c, SEL orig, SEL newSel, IMP impl) {
    Method method = class_getInstanceMethod(c, orig);
    const char *encoding = method_getTypeEncoding(method);
    if (!class_addMethod(c, newSel, impl, encoding)) {
        PSPDFLogError(@"Failed to add method: %@ on %@", NSStringFromSelector(newSel), NSStringFromClass(c));
    }else PSPDFSwizzleMethod(c, orig, newSel);
}

// To use the pageCurl feature, we have to apply several fixes to Apple's UIPageViewController, as this class is new and still relatively buggy.
// Strictly speaking this is private API, so if you're afraid of it, just set the preprocessor define _PSPDFKIT_DONT_USE_OBFUSCATED_PRIVATE_API_.
// If you disable this, the pageCurl feature will be disabled and the framework falls back to scrolling.
//
// I'd wish those hacks were not neccessary, but I'm pragmatic and like to use stuff and not wait for iOS6 to hopefully fix this.
__attribute__((constructor)) static void pspdf_applyRuntimePatches(void) {
    @autoreleasepool {
#ifndef _PSPDFKIT_DONT_USE_OBFUSCATED_PRIVATE_API_
        // no need to patch this on iOS4, those classes don't yet exist there.
        // TODO: This might be fixed in iOS6.


        // still crashes on iOS6 if we remove the controller while pageCurl is active.
        // _UIPageCurlState message sent to deallocated instance
        Class pagecurlstate = NSClassFromString([NSString stringWithFormat:@"_%@Cu%@ate", @"UIPage", @"rlSt"]);
        SEL customDeallocSEL = NSSelectorFromString(@"pspdf_customDealloc");

        // primary reason why we can't use ARC here.
        IMP customDeallocIMP = imp_implementationWithBlock(^(id _self) {
            __block id weakThis = _self;
            double delayInSeconds = PSIsSimulator() ? 10.f : 1.0f;  // support slow animations in simulator
            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
            dispatch_after(popTime, dispatch_get_main_queue(), ^ {
                [weakThis performSelector:customDeallocSEL];
            });
        });

        if (pagecurlstate) {
            PSPDFReplaceMethod(pagecurlstate, NSSelectorFromString(@"dealloc"), customDeallocSEL, customDeallocIMP);
        }

        // [_UIPageCurl _pageCurlAnimationDidStop:withState:]: message sent to deallocated instance 0x14a03fe0
        Class pagecurl = NSClassFromString([NSString stringWithFormat:@"_%@Cu%@", @"UIPage", @"rl"]);
        if (pagecurl) {
            PSPDFReplaceMethod(pagecurl, NSSelectorFromString(@"dealloc"), customDeallocSEL, customDeallocIMP);
        }
#endif
    }
}

#ifdef __IPHONE_6_0
static int pspdf_rotationLockNumber = 0;
inline static NSUInteger PSDPFInterfaceMaskForWindow(UIWindow * window) {
    // Note: If we would use window.rootViewController.interfaceOrientation, we might get a exception when interfaceOrientation is queried while a application is pushed.
    // 'Supported orientations has no common orientation with the application, and shouldAutorotate is returning YES'
    return pspdf_rotationLockNumber <= 0 ? UIInterfaceOrientationMaskAll : 1 << [UIApplication sharedApplication].statusBarOrientation;
}

static void PSPDFApplyiOS6RotationPatch(void) {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        if (kCFCoreFoundationVersionNumber < kCFCoreFoundationVersionNumber_iOS_6_0) return;

        // swizzle - only works if application:supportedInterfaceOrientationsForWindow: is implemented in the app delegate
        SEL pspdf_suppIOSEL = NSSelectorFromString(@"pspdf_application:supportedInterfaceOrientationsForWindow:");
        IMP pspdf_suppIOIMP = imp_implementationWithBlock(^(id _self, UIApplication *application, UIWindow *window) { return pspdf_rotationLockNumber <= 0 ? (NSUInteger)objc_msgSend(_self, pspdf_suppIOSEL, application, window) : PSDPFInterfaceMaskForWindow(window); });
        PSPDFReplaceMethod([[UIApplication sharedApplication].delegate class], NSSelectorFromString(@"application:supportedInterfaceOrientationsForWindow:"), pspdf_suppIOSEL, pspdf_suppIOIMP);


        // if not, the default implementation in UIApplication is called. Swizzle that too.
        SEL pspdf_suppAppSEL = NSSelectorFromString(@"pspdf_supportedInterfaceOrientationsForWindow:");
        IMP pspdf_suppAppIMP = imp_implementationWithBlock(^(id _self, UIWindow *window) { return PSDPFInterfaceMaskForWindow(window); });
        PSPDFReplaceMethod([[UIApplication sharedApplication] class], NSSelectorFromString(@"supportedInterfaceOrientationsForWindow:"), pspdf_suppAppSEL, pspdf_suppAppIMP);
    });
}
#endif

BOOL PSPDFIsRotationLocked(void) {
#ifdef __IPHONE_6_0
    return pspdf_rotationLockNumber > 0;
#endif
    return NO;
}

void PSPDFLockRotation(void) {
#ifdef __IPHONE_6_0
    PSPDFApplyiOS6RotationPatch();
    OSAtomicIncrement32Barrier(&pspdf_rotationLockNumber);
#endif
}

void PSPDFUnlockRotation(void) {
#ifdef __IPHONE_6_0
    if (pspdf_rotationLockNumber == 0) {
        PSPDFLogError(@"Warning: rotation lock is not active.");
    }else {
        OSAtomicDecrement32Barrier(&pspdf_rotationLockNumber);
    }
#endif
}


#ifdef DEBUG
#ifndef _PSPDFKIT_DONT_USE_OBFUSCATED_PRIVATE_API_

static BOOL PSPDFIsVisibleView(UIView *view) {
    BOOL isViewHidden = view.isHidden || view.alpha == 0 || CGRectIsEmpty(view.frame);
    return !view || (PSPDFIsVisibleView(view.superview) && !isViewHidden);
}

// Following code patches UIView's description to show the class name of an an view controller, if one is attached.
// Will only get compiled for debugging. Use 'po [[UIWindow keyWindow] recursiveDescription]' to invoke.
__attribute__((constructor)) static void PSPDFKitImproveRecursiveDescription(void) {
    @autoreleasepool {
        SEL customViewDescriptionSEL = NSSelectorFromString(@"pspdf_customViewDescription");
        IMP customViewDescriptionIMP = imp_implementationWithBlock(^(id _self) {
            NSMutableString *description = [_self performSelector:customViewDescriptionSEL];
            SEL viewDelegateSEL = NSSelectorFromString([NSString stringWithFormat:@"%@ewDelegate", @"_vi"]); // pr. API
            if ([_self respondsToSelector:viewDelegateSEL]) {
                UIViewController *viewController = [_self performSelector:viewDelegateSEL];
                NSString *viewControllerClassName = NSStringFromClass([viewController class]);

                if ([viewControllerClassName length]) {
                    NSString *children = @""; // iterate over childViewControllers

                    if ([viewController respondsToSelector:@selector(childViewControllers)] && [viewController.childViewControllers count]) {
                        NSString *origDescription = description;
                        description = [NSMutableString stringWithFormat:@"%dc.[", [viewController.childViewControllers count]];
                        for (UIViewController *childViewController in viewController.childViewControllers) {
                            [description appendFormat:@"%@:%p ", NSStringFromClass([childViewController class]), childViewController];
                        }
                        [description appendFormat:@"] %@", origDescription];
                    }

                    // check if the frame of a childViewController is bigger than the one of a parentViewController. (usually this is a bug)
                    NSString *warnString = @"";
                    if (viewController && viewController.parentViewController && [viewController isViewLoaded] && [viewController.parentViewController isViewLoaded]) {
                        CGRect parentRect = viewController.parentViewController.view.bounds;
                        CGRect childRect = viewController.view.frame;

                        if (parentRect.size.width < childRect.origin.x + childRect.size.width ||
                            parentRect.size.height < childRect.origin.y + childRect.size.height) {
                            warnString = @"* OVERLAP! ";
                        }else if (CGRectIsEmpty(childRect)) {
                            warnString = @"* ZERORECT! " ;
                        }
                    }
                    description = [NSMutableString stringWithFormat:@"%@%@:%p%@ %@", warnString, viewControllerClassName, viewController, children, description];
                }
            }

            // add marker if view (or a superview) is hidden
            if (!PSPDFIsVisibleView(_self)) {
                description = [NSMutableString stringWithFormat:@"XX (%@)", description];
            }

            return description;
        });
        PSPDFReplaceMethod([UIView class], @selector(description), customViewDescriptionSEL,customViewDescriptionIMP);
    }
}
#endif

// Instead of "<UIImage: 0x8b612f0>" we want "<UIImage:0x8b612f0 size:{768, 1001} scale:1 imageOrientation:0>".
// Doesn't use any private API.
__attribute__((constructor)) static void PSPDFKitImproveImageDescription(void) {
    @autoreleasepool {
        SEL customImageDescriptionSEL = NSSelectorFromString(@"pspdf_customImageDescription");
        IMP customImageDescriptionIMP = imp_implementationWithBlock(^(id _self) {
            NSMutableString *description = [NSMutableString stringWithFormat:@"<%@:%p size:%@", NSStringFromClass([_self class]), _self, NSStringFromCGSize([(UIImage *)_self size])];
            if ([(UIImage *)_self scale] > 1) {
                [description appendFormat:@" scale:%.0f", [(UIImage *)_self scale]];
            }
            if ([_self imageOrientation] != UIImageOrientationUp) {
                [description appendFormat:@" imageOrientation:%d", [_self imageOrientation]];
            }
            [description appendString:@">"];
            return [[description copy] autorelease];
        });
        PSPDFReplaceMethod([UIImage class], @selector(description), customImageDescriptionSEL, customImageDescriptionIMP);
    }
}

// Add image description to UIImageView.
__attribute__((constructor)) static void PSPDFKitImproveImageViewDescription(void) {
    @autoreleasepool {
        SEL customImageViewDescSEL = NSSelectorFromString(@"pspdf_customImageViewDescription");
        IMP customImageViewDescIMP = imp_implementationWithBlock(^(id _self) {
            return [NSString stringWithFormat:@"%@->%@", [_self performSelector:customImageViewDescSEL], [[_self image] description]];
        });
        PSPDFReplaceMethod([UIImageView class], @selector(description), customImageViewDescSEL, customImageViewDescIMP);
    }
}


@implementation PSPDFRetainTracker
- (id)retain {
    if (![NSThread isMainThread]) {
        const char *queueName = dispatch_queue_get_label(dispatch_get_current_queue());
        NSLog(@"RETAIN %@ (queue:%@) -> %@", [NSThread currentThread].name, (queueName ? @(queueName) : @""), [NSThread callStackSymbols]);
    }
    return [super retain];
}

- (id)autorelease {
    if (![NSThread isMainThread]) {
        const char *queueName = dispatch_queue_get_label(dispatch_get_current_queue());
        NSLog(@"AUTORELEASE %@ (queue:%@) -> %@", [NSThread currentThread].name, (queueName ? @(queueName) : @""), [NSThread callStackSymbols]);
    }
    return [super autorelease];
}

- (oneway void)release {
    if (![NSThread isMainThread]) {
        const char *queueName = dispatch_queue_get_label(dispatch_get_current_queue());
        NSLog(@"RELEASE %@ (queue:%@) -> %@", [NSThread currentThread].name, (queueName ? @(queueName) : @""), [NSThread callStackSymbols]);
    }
    [super release];
}
@end

#endif
