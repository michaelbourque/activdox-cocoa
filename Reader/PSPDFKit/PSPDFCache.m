//
//  PSPDFCache.m
//  PSPDFKit
//
//  Copyright 2011-2012 Peter Steinberger. All rights reserved.
//

#import "PSPDFCache.h"
#import "PSPDFKit.h"
#import "PSPDFKitGlobal.h"
#import "UIImage+PSPDFKitAdditions.h"
#import "PSPDFKitGlobal+Internal.h"
#import <CoreGraphics/CoreGraphics.h>
#import <CoreFoundation/CoreFoundation.h>
#import <libkern/OSAtomic.h>
#import <objc/runtime.h>

#define kPSPDFCacheTimerInterval 0.5f
#define kPSPDFCachePause (PSPDFIsCrappyDevice() ? 0.3f : 0.1f)

// demo token that's inside cache folder to detect watermarked images
#define kPSPDFCacheDemoToken @"PSPDFKitDEMO"

NSString *kPSPDFCachedRenderRequest = @"kPSPDFCachedRenderRequest";

@interface PSPDFCache() {
    dispatch_queue_t _delegateQueue;      // syncs access for the delegates_ set
    dispatch_queue_t _cacheMgmtQueue;     // syncs adding/releasing queuedItems_/queuedDocuments_
    dispatch_queue_t _fileMgmtQueue;      // syncs fileMgmtQueue_
    dispatch_queue_t _cacheRequestQueue;  // syncs access for adding/removing queue requests

    NSMutableDictionary *_cachedFiles;
    BOOL _cacheFileDictLoaded;

    NSMutableArray *_queuedItems;     // PSPDFCacheQueuedDocument
    NSMutableArray *_queuedDocuments; // PSPDFCacheQueuedDocument
    NSTimer *_cacheTimer;
    NSOperationQueue *_cacheQueue;
    NSMutableSet *_delegates;
    NSMutableSet *_pauseServices;
}
@property (atomic, strong, readonly) NSCache *thumbnailCache;
@property (atomic, strong, readonly) NSCache *fullPageCache;
@property (nonatomic, copy) NSString *cachedCacheDirectory;
@property (atomic, assign, getter=isCacheFileDictLoaded) BOOL cacheFileDictLoaded;
@end

@implementation PSPDFCache

static OSSpinLock _pauseServiceSpinLock;

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (id)init {
    if ((self = [super init])) {
        _delegateQueue = pspdf_dispatch_queue_create("com.PSPDFKit.cache.delegateQueue", DISPATCH_QUEUE_CONCURRENT);
        _cacheMgmtQueue = pspdf_dispatch_queue_create("com.PSPDFKit.cache.cacheManagementQueue", 0);
        _cacheRequestQueue = pspdf_dispatch_queue_create("com.PSPDFKit.cache.cacheRequestQueue", 0);
        _fileMgmtQueue = pspdf_dispatch_queue_create("com.PSPDFKit.cache.fileQueue", DISPATCH_QUEUE_CONCURRENT);

        // set useful defaults
        _strategy = PSPDFCacheOpportunistic;
        _numberOfMaximumCachedDocuments = PSPDFIsCrappyDevice() ? 3 : 5;
        _numberOfNearCachedPages = 3;
        _JPGFormatCompression = 0.9f;
        _useJPGFormat = YES;

        _downscaleInterpolationQuality = kCGInterpolationHigh;
        _thumbnailSize = CGSizeMake(200, 400);
        _tinySize = CGSizeMake(50, 100);
        _cacheDirectory = [@"PSPDFKit" copy];

        // alloc data structures
        _queuedItems = [[NSMutableArray alloc] init];
        _queuedDocuments = [[NSMutableArray alloc] init];
        _pauseServices = [[NSMutableSet alloc] init];

        // delegate set is non-retaining
        _delegates = CFBridgingRelease(CFSetCreateMutable(nil, 0, NULL));

        _thumbnailCache = [[NSCache alloc] init];
        _thumbnailCache.name = @"PSPDFThumbnailCache";
        [_thumbnailCache setCountLimit:kPSPDFLowMemoryMode ? 10 : (PSPDFIsCrappyDevice() ? 50 : 100)];
        _thumbnailCache.delegate = self;
        _fullPageCache = [[NSCache alloc] init];
        _fullPageCache.name = @"PSPDFFullPageCache";
        [_fullPageCache setCountLimit:kPSPDFLowMemoryMode ? 1 : _numberOfMaximumCachedDocuments];
        _fullPageCache.delegate = self;
        _cachedFiles = [[NSMutableDictionary alloc] init];
        _cacheQueue = [[NSOperationQueue alloc] init];
        _cacheQueue.name = @"PSPDFCacheQueue";
        _cacheQueue.maxConcurrentOperationCount = [self numberOfConcurrentCacheOperations];

        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didReceiveMemoryWarning) name:UIApplicationDidReceiveMemoryWarningNotification object:nil];

        // load cache in background
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            [self loadCachedFilesArray];
        });

        // ensure timer is registered on main thread
        dispatch_async(dispatch_get_main_queue(), ^ {
            _cacheTimer = [NSTimer scheduledTimerWithTimeInterval:kPSPDFCacheTimerInterval target:self selector:@selector(timerFired) userInfo:nil repeats:YES];
        });
    }
    return self;
}

- (void)dealloc {
    //[[NSNotificationCenter defaultCenter] removeObject:self];
    PSPDFDispatchRelease(_cacheMgmtQueue);
    PSPDFDispatchRelease(_fileMgmtQueue);
    PSPDFDispatchRelease(_cacheRequestQueue);
    PSPDFDispatchRelease(_delegateQueue);
    [_cacheTimer invalidate];
    [_cacheQueue cancelAllOperations];
    [_cacheQueue waitUntilAllOperationsAreFinished];
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public

- (void)setCacheDirectory:(NSString *)cacheDirectory {
    if (_cacheDirectory != cacheDirectory) {
        self.cachedCacheDirectory = nil;
        PSPDFAtomicCopiedSetToFrom(_cacheDirectory, cacheDirectory);
    }
}

- (void)setNumberOfMaximumCachedDocuments:(NSUInteger)numberOfMaximumCachedDocuments {
    // ensure number is odd (or zero)
    if (numberOfMaximumCachedDocuments > 0 && numberOfMaximumCachedDocuments % 2 == 0) {
        numberOfMaximumCachedDocuments++;
    }
    _numberOfMaximumCachedDocuments = numberOfMaximumCachedDocuments;
    [self.fullPageCache setCountLimit:numberOfMaximumCachedDocuments];
}

- (BOOL)isDocumentCached:(PSPDFDocument *)document size:(PSPDFSize)size {
    NSUInteger pageCount = [document pageCount];
    for (NSUInteger i = 0; i < pageCount; i++) {
        if (![self isImageCachedForDocument:document page:i size:size]) {
            return NO;
        }
    }
    return YES;
}

// check if document is cached. first check memory, then hit filesystem
- (BOOL)isImageCachedForDocument:(PSPDFDocument *)document page:(NSUInteger)page size:(PSPDFSize)size {
    NSFileManager *fileManager = nil;

    // check own memory-cache
    __block BOOL cachedImageExists = [self imageForDocument:document page:page size:size] != nil;

    // check document solution (custom provided thumbs?)
    if (!cachedImageExists) {
        NSURL *externalURL = [document cachedImageURLForPage:page andSize:size];
        // don't trust external sources
        if (externalURL) {
            fileManager = [NSFileManager new];
            cachedImageExists = [fileManager fileExistsAtPath:[externalURL path] isDirectory:nil];
        }
    }

    // check disk-store
    if (!cachedImageExists) {
        // cache already available?
        if (self.isCacheFileDictLoaded) {
            NSString *fileName = [self cachedImageFileNameForPage:page size:size];
            dispatch_sync(_fileMgmtQueue, ^{
                NSSet *cachedFiles = _cachedFiles[document.UID];
                cachedImageExists = [cachedFiles containsObject:fileName];
            });
        }else {
            if (!fileManager) fileManager = [NSFileManager new];

            // fixes an edge-case where the cache dict isn't yet loaded and we have watermarked images cached.
            NSString *pspdfDemoTokenPath = [[self cachedImagePathForDocument:document] stringByAppendingPathComponent:kPSPDFCacheDemoToken];
            if (![fileManager fileExistsAtPath:pspdfDemoTokenPath]) {

                // only bother looking for file after demo token has been checked for.
                NSString *cachedImagePathForDocumentPage = [self cachedImageFilePathForDocument:document page:page size:size];
                // small performance hit, but protects us against wrong thumbnails paths
                cachedImageExists = [fileManager fileExistsAtPath:cachedImagePathForDocumentPage];
            }
        }
    }

    PSPDF_IF_SIMULATOR(// the device cache might change on the simulator, so check dimensions!
                       if (cachedImageExists && size == PSPDFSizeNative) {
                           static dispatch_once_t onceToken;
                           dispatch_once(&onceToken, ^{
                               NSString *path = [self cachedImageFilePathForDocument:document page:page size:size];
                               UIImage *image = [UIImage imageWithContentsOfFile:path];
                               CGSize screenSize = [UIScreen mainScreen].bounds.size;
                               CGFloat fuzzy = 100;
                               // Images are in wrong size. Cleaning cache for new device...
                               if (image && image.size.height+fuzzy < screenSize.height && image.size.width+fuzzy < screenSize.width) {
                                   [self clearCache];
                                   cachedImageExists = NO;
                               }
                           });
                       })
    return cachedImageExists;
}

- (UIImage *)cachedImageForDocument:(PSPDFDocument *)document page:(NSUInteger)page size:(PSPDFSize)size {
    return [self cachedImageForDocument:document page:page size:size preload:NO];
}

- (UIImage *)cachedImageForDocument:(PSPDFDocument *)document page:(NSUInteger)page size:(PSPDFSize)size preload:(BOOL)preload {
    if (!document) {PSPDFLogWarning(@"Document is nil!"); return nil;}
    if (page == NSNotFound || page >= [document pageCount]) {
        PSPDFLogWarning(@"Page is invalid: %d (pageCount: %d)", page, [document pageCount]); return nil;
    }

    // returns cached image of document. If not found, add to TOP of current caching queue.
    if (![self isImageCachedForDocument:document page:page size:size]) {
        PSPDFLogVerbose(@"cache miss for %d", page);

        // accesses pageCount, may be slow ->
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{

            if (document.isLocked) {
                PSPDFLogWarning(@"Document %@ is locked; can't render image.", document); return;
            }

            PSPDFCacheQueuedDocument *queueItem = [PSPDFCacheQueuedDocument queuedDocumentWithDocument:document page:page size:size];
            [self enqueueItem:queueItem];
        });
        return nil;
    }

    // first try to load via memory cache
    __block UIImage *cacheImage = [self imageForDocument:document page:page size:size];

    // custom (external) document store
    if (!cacheImage) {
        cacheImage = [UIImage imageWithContentsOfFile:[[document cachedImageURLForPage:page andSize:size] path]];
        if (cacheImage) {
            [self cacheImage:cacheImage document:document page:page size:size];
        }
    }

    // disk store
    if (!cacheImage) {
        NSString *cacheImagePath = [self cachedImageFilePathForDocument:document page:page size:size];

        cacheImage = [[UIImage alloc] initWithContentsOfFile:cacheImagePath];
        if (preload) cacheImage = [cacheImage pspdf_preloadedImage];

        // re-fill thumbnail cache
        if (cacheImage && (size != PSPDFSizeNative || [self shouldMemoryCacheFullPage:page forDocument:document])) {
            [self cacheImage:cacheImage document:document page:page size:size];
        }
    }

    return cacheImage;
}

// checks all files
- (void)cacheDocument:(PSPDFDocument *)document startAtPage:(NSUInteger)aPage size:(PSPDFSize)size {
    if (!document) { PSPDFLogWarning(@"Document is nil!"); return; }

    dispatch_async(_cacheRequestQueue, ^{
        NSUInteger page = aPage;
        if (![document isValid]) { PSPDFLogVerbose(@"Document is nil/has zero pages. Not caching."); return; }

        if (page >= [document pageCount]) {
            PSPDFLogWarning(@"startPage:%d to high, resetting to 0.", page);
            page = 0;
        }
        if (document.isLocked) { PSPDFLogWarning(@"Document %@ is locked; can't cache yet.", document); return; }

        // manage queuedDocuments_ array, add new caching document request
        pspdf_dispatch_sync_reentrant(_cacheMgmtQueue, ^{
            PSPDFCacheQueuedDocument *queueItem = [self queuedDocumentFromDocument:document size:size];
            // if not found, create new queue item, else update
            if (!queueItem) {
                PSPDFCacheQueuedDocument *newQueueItem = [PSPDFCacheQueuedDocument queuedDocumentWithDocument:document page:page size:size];
                [_queuedDocuments addObject:newQueueItem];
            }else {
                queueItem.page = page;
                queueItem.size = size;
                [queueItem.pagesCached removeAllObjects]; // try again to cache.
            }
        });

        // fire up caching machinery!
        [self cacheNextDocumentInStack];
    });
}

- (BOOL)cacheThumbnailsForDocument:(PSPDFDocument *)aDocument {
    BOOL willPreload = ![self isDocumentCached:aDocument size:PSPDFSizeTiny] || ![self isDocumentCached:aDocument size:PSPDFSizeThumbnail];
    [self cacheDocument:aDocument startAtPage:0 size:PSPDFSizeTiny];
    return willPreload;
}

// remove from cache dict
- (void)stopCachingDocument:(PSPDFDocument *)document {
    NSParameterAssert(document);
    if (!document) { PSPDFLogVerbose(@"Called with nil document!"); return; }

    // remove from queued documents
    dispatch_barrier_async(_cacheMgmtQueue, ^{
        [_queuedDocuments removeObjectsInArray:[self queuedDocumentsFromDocument:document]];

        NSArray *cachedItems = [_queuedItems filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"document = %@", document]];
        PSPDFLogVerbose(@"removing queued items: %@", _queuedItems);
        if ([cachedItems count]) {
            [_queuedItems removeObjectsInArray:cachedItems];
        }
    });
}

- (BOOL)pauseCachingForService:(id)service {
    NSParameterAssert(service);
    if (!service) { PSPDFLogWarning(@"Won't pause - service is nil!"); return NO; }
    PSPDFLogVerbose(@"Adding %@ to the pause list.", service);

    OSSpinLockLock(&_pauseServiceSpinLock);
    BOOL cacheWillBePausedNow = NO;
    if (![_pauseServices containsObject:service]) {
        [_pauseServices addObject:service]; // ignores if already a member
        [_cacheQueue setSuspended:YES];
        cacheWillBePausedNow = [_pauseServices count] == 1;
    }
    OSSpinLockUnlock(&_pauseServiceSpinLock);

    return cacheWillBePausedNow;
}

- (BOOL)resumeCachingForService:(id)service {
    NSParameterAssert(service);
    if (!service) { PSPDFLogWarning(@"Won't resume - service is nil!"); return NO; }
    PSPDFLogVerbose(@"Removing %@ from the pause list.", service);

    OSSpinLockLock(&_pauseServiceSpinLock);
    BOOL serviceIsInList = [_pauseServices containsObject:service];
    if (serviceIsInList) [_pauseServices removeObject:service];

    BOOL cacheWillContinue = serviceIsInList && [_pauseServices count] == 0;
    if (cacheWillContinue) [_cacheQueue setSuspended:[_pauseServices count] > 0];
    OSSpinLockUnlock(&_pauseServiceSpinLock);

    return cacheWillContinue;
}

- (void)addDelegate:(id<PSPDFCacheDelegate>)aDelegate {
    dispatch_barrier_sync(_delegateQueue, ^{
        if (aDelegate) [_delegates addObject:aDelegate];
    });
}

- (BOOL)removeDelegate:(id<PSPDFCacheDelegate>)aDelegate {
    __block BOOL success = NO;
    dispatch_barrier_sync(_delegateQueue, ^{
        for (id<PSPDFCacheDelegate> weakRef in _delegates) {
            if (weakRef == aDelegate) {
                [_delegates removeObject:weakRef];
                success = YES; break;
            }
        }
    });
    return success;
}

// don't call directly, use store for deletion
- (BOOL)removeCacheForDocument:(PSPDFDocument *)document deleteDocument:(BOOL)deleteDocument error:(NSError **)error {
    if (!document) { PSPDFError(PSPDFErrorCodeRemoveCacheError, @"Document is nil.", error); return NO; }

    // remove from cache queue instantly
    [self stopCachingDocument:document];

    // remove file-exist memory cache
    dispatch_barrier_async(_fileMgmtQueue, ^{
        if ([document.UID length] > 0) {
            [_cachedFiles removeObjectForKey:document.UID];
        }
    });

    NSError *localError = nil;
    NSFileManager *fileManager = [NSFileManager new];
    if (![fileManager removeItemAtPath:[self cachedImagePathForDocument:document] error:&localError]) {
        PSPDFLogWarning(@"Deletion failed: %@", [localError localizedDescription]);
    }

    if (deleteDocument) {
        for (NSURL *fileURL in [document filesWithBasePath]) {
            if ([fileURL isFileURL] && ![fileManager removeItemAtURL:fileURL error:&localError]) {
                PSPDFLogWarning(@"Document deletion failed: %@", [localError localizedDescription]);
            }

            // clean up metadata (bookmarks, annotations, ...)
            [fileManager removeItemAtPath:document.cacheDirectory error:&localError];
        }
    }
    PSPDFLogVerbose(@"Cache deletion for %@ complete.", document.title);

    if (error) *error = localError;

    // also clear memory in case there are some thumbnails left...
    [self clearMemoryCache];

    return localError == nil;
}

- (BOOL)clearCache {
    __block BOOL success = NO;

    // Wait for potential async cache add requests that are currently running (else document may gets inserted again after we remove it)
    dispatch_async(_cacheRequestQueue, ^{
        NSError *error = nil;
        NSString *cacheFolder = self.cachedCacheDirectory;

        // stop all current operations
        pspdf_dispatch_sync_reentrant(_cacheMgmtQueue, ^{
            [_queuedItems removeAllObjects];
            [_queuedDocuments removeAllObjects];
        });

        // cancel all running operations, wait until already running requests finish.
        [_cacheQueue cancelAllOperations];
        [_cacheQueue waitUntilAllOperationsAreFinished]; // might lock on the main thread

        // Deletes the file, link, or directory (including, recursively, all subdirectories, files, and links in the directory) identified by a given path.
        NSFileManager *fileManager = [NSFileManager new];
        if ([fileManager fileExistsAtPath:cacheFolder]) {
            success = [fileManager removeItemAtPath:cacheFolder error:&error];
        }else {
            success = YES;
        }

        // remove file-exist memory cache
        dispatch_barrier_async(_fileMgmtQueue, ^{
            [_cachedFiles removeAllObjects];
        });

        if (error) PSPDFLogError(@"Failed to delete cache folder: %@", [error localizedDescription]);

        // also clear thumbnail cache
        [self clearMemoryCache];

        // clear potential cache in global lock (and wait for threads to finish up)
        [[PSPDFGlobalLock sharedGlobalLock] requestClearCacheAndWait:YES];
    });

    return success;
}

// identifier for thumbnail caching (Number is faster than string in NSCache key checking)
- (NSNumber *)identifierForDocument:(PSPDFDocument *)document page:(NSUInteger)page size:(PSPDFSize)size {
    NSUInteger hash = [document hash] + page + [document pageCount] * size;
    return @(hash);
}

// save image in an NSCache object for specified identifier.
- (void)cacheImage:(UIImage *)image document:(PSPDFDocument *)document page:(NSUInteger)page size:(PSPDFSize)size {
    if (!image) { PSPDFLogWarning(@"Tried to save nil in cache!"); return; }
    
    NSNumber *identifier = [self identifierForDocument:document page:page size:size];
    NSUInteger cost = 0;
    switch (size) {
        case PSPDFSizeNative:    cost = 100; break;
        case PSPDFSizeThumbnail: cost = 10;  break;
        case PSPDFSizeTiny:      cost = 1;   break;
        default: break;
    }

    if (size == PSPDFSizeNative) {
        [self.fullPageCache setObject:image forKey:identifier];
    }else {
        [self.thumbnailCache setObject:image forKey:identifier cost:cost];
    }
}

// load image for a certain identifier
- (UIImage *)imageForDocument:(PSPDFDocument *)document page:(NSUInteger)page size:(PSPDFSize)size {
    NSNumber *identifier = [self identifierForDocument:document page:page size:size];
    UIImage *cachedImage;
    if (size == PSPDFSizeNative) {
        cachedImage = [self.fullPageCache objectForKey:identifier];
    }else {
        cachedImage = [self.thumbnailCache objectForKey:identifier];
    }
    return cachedImage;
}

- (void)clearMemoryCache {
    [self.fullPageCache removeAllObjects];
    [self.thumbnailCache removeAllObjects];
    [[PSPDFGlobalLock sharedGlobalLock] requestClearCacheAndWait:YES];
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Private

- (NSUInteger)numberOfConcurrentCacheOperations {
    // run only one caching thead on crappy devices, more threads on all other.
    return kPSPDFLowMemoryMode ? 1 : 2;
}

// return *cahced* cache folder
- (NSString *)cachedCacheDirectory {
    if (!_cachedCacheDirectory) {
        // NSSearchPathForDirectoriesInDomains is slow!
        NSString *cacheFolder = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES)[0];
        cacheFolder = [cacheFolder stringByAppendingPathComponent:self.cacheDirectory];
        _cachedCacheDirectory = cacheFolder;
    }
    return _cachedCacheDirectory;
}

// return cache folder for specific document
- (NSString *)cachedImagePathForDocument:(PSPDFDocument *)aDocument {
    if (!aDocument.UID) { PSPDFLogWarning(@"Document has empty UID, cannot build cache path!"); return nil; }

    NSString *cacheFolder = self.cachedCacheDirectory;
    NSString *documentCacheFolder = [cacheFolder stringByAppendingPathComponent:aDocument.UID];
    return documentCacheFolder;
}

- (NSString *)cachedImageFileNameForPage:(NSUInteger)page size:(PSPDFSize)size {
    NSUInteger humanPage = page + 1;
    NSString *formatString = self.useJPGFormat ? @"jpg" : @"png";
    char pageFormat;

    if (size == PSPDFSizeNative) {
        pageFormat = 'p';
    }else if (size == PSPDFSizeThumbnail) {
        pageFormat = 't';
    }else { // tiny
        pageFormat = 'y';
    }

    NSString *newCurrentPdfFileName = [NSString stringWithFormat:@"%c%i.%@", pageFormat, humanPage, formatString];
    return newCurrentPdfFileName;
}

// Returns full path to a cached image
- (NSString *)cachedImageFilePathForDocument:(PSPDFDocument *)document page:(NSUInteger)page size:(PSPDFSize)size {
    NSString *documentCacheFolder = [self cachedImagePathForDocument:document];
    NSString *newCurrentPdfFileName = [self cachedImageFileNameForPage:page size:size];
    NSString *completeFileName = [documentCacheFolder stringByAppendingPathComponent:newCurrentPdfFileName];
    return completeFileName;
}

// Filesystem is crawled for already cached files (file-exist cache)
// Async - this might be slow if there are a lot of files.
- (void)loadCachedFilesArray {
    dispatch_barrier_async(_fileMgmtQueue, ^{
        PSPDFLogVerbose(@"loading cache files from disk...");
        [_cachedFiles removeAllObjects];

        NSFileManager *fileManager = [NSFileManager new];
        NSError *error = nil;
        NSString *cacheFolder = [self cachedCacheDirectory];
        NSArray *cacheFolders = [fileManager contentsOfDirectoryAtPath:cacheFolder error:&error];

        // create cache entry for every cache folder
        for (NSString *documentCacheFolder in cacheFolders) {
            NSString *cacheFolderPath = [cacheFolder stringByAppendingPathComponent:documentCacheFolder];
            NSArray *folderContents = [fileManager contentsOfDirectoryAtPath:cacheFolderPath error:&error];
            if ([folderContents count]) {
                // if files are watermarked, remove the cache folder.
                if ([folderContents containsObject:kPSPDFCacheDemoToken]) {
                    [fileManager removeItemAtPath:cacheFolderPath error:NULL];
                }else {
                    NSMutableSet *set = [NSMutableSet set];
                    _cachedFiles[documentCacheFolder] = set;
                    [set addObjectsFromArray:folderContents];
                }
            }
        }
        self.cacheFileDictLoaded = YES;
    });
}

// Add a file to the file-exist cache (checking if a file exists on disk is slow!)
- (void)addFileToExistCacheForDocument:(PSPDFDocument *)document page:(NSUInteger)page size:(PSPDFSize)size {
    dispatch_barrier_async(_fileMgmtQueue, ^{
        NSMutableSet *cachedFileSet = _cachedFiles[document.UID];
        if (!cachedFileSet) {
            cachedFileSet = [NSMutableSet set];
            _cachedFiles[document.UID] = cachedFileSet;
        }
        NSString *fileName = [self cachedImageFileNameForPage:page size:size];
        [cachedFileSet addObject:fileName];
    });
}

// Removes a file from the file-exist cache
- (void)removeFileFromExistCacheForDocument:(PSPDFDocument *)document page:(NSUInteger)page size:(PSPDFSize)size {
    dispatch_barrier_async(_fileMgmtQueue, ^{
        NSMutableSet *cachedFileSet = _cachedFiles[document.UID];
        if (cachedFileSet) {
            NSString *fileName = [self cachedImageFileNameForPage:page size:size];
            [cachedFileSet removeObject:fileName];
        }
    });
}

// calculate optimal scaling for a page
- (CGFloat)scaleForImageSize:(CGSize)imageSize size:(PSPDFSize)size {
    CGSize boundsSize;
    if (size == PSPDFSizeThumbnail) {
        boundsSize = self.thumbnailSize;
    }else if (size == PSPDFSizeTiny) {
        boundsSize = self.tinySize;
    }else {
        // PSPDFSizeNative, removes size of the statusBar if visible
        boundsSize = [[UIScreen mainScreen] bounds].size;   // PSPDFSizeNative
        if (![UIApplication sharedApplication].statusBarHidden) {
            CGRect statusBarFrame = [UIApplication sharedApplication].statusBarFrame;
            BOOL isLandscape = UIInterfaceOrientationIsLandscape([UIApplication sharedApplication].statusBarOrientation);
            CGFloat height = isLandscape ? statusBarFrame.size.width : statusBarFrame.size.height;
            boundsSize.height -= height;
        }

        // we can't use applicationFrame, as it takes landscape into account, and we only render portrait mode here.
        //boundsSize = [[UIScreen mainScreen] applicationFrame].size;

        // always generate maximum size for native resolution (e.g. if document is larger in landscape, use that)
        if (imageSize.width > imageSize.height) {
            boundsSize = CGSizeMake(boundsSize.height, boundsSize.width);
        }
    }

    // use default settings
    CGFloat scale = PSPDFScaleForSizeWithinSize(imageSize, boundsSize);
    return scale;
}

// calling delegates for document in main thread
- (void)callDelegatesForDocument:(PSPDFDocument *)aDocument page:(NSUInteger)aPage image:(UIImage *)cachedImage size:(PSPDFSize)size {
    dispatch_async(dispatch_get_main_queue(), ^{
        pspdf_dispatch_sync_reentrant(_delegateQueue, ^{
            for (id<PSPDFCacheDelegate> aDelegate in _delegates) {
                [aDelegate didCachePageForDocument:aDocument page:aPage image:cachedImage size:size];
            }
        });
    });
}

- (void)callDelegatesForDocument:(PSPDFDocument *)aDocument {
    dispatch_async(dispatch_get_main_queue(), ^{
        pspdf_dispatch_sync_reentrant(_delegateQueue, ^{
            for (id<PSPDFCacheDelegate> aDelegate in _delegates) {
                if ([aDelegate respondsToSelector:@selector(didFinishCachingDocument:)]) {
                    [aDelegate didFinishCachingDocument:aDocument];
                }
            }
        });
    });
}

- (PSPDFCacheQueuedDocument *)queuedDocumentFromDocument:(PSPDFDocument *)document size:(PSPDFSize)size {
    NSArray *queuedDocuments = [self queuedDocumentsFromDocument:document];
    for (PSPDFCacheQueuedDocument *queuedDocument in queuedDocuments) {
        if (queuedDocument.size == size) {
            return queuedDocument;
        }
    }
    return nil;
}

- (NSArray *)queuedDocumentsFromDocument:(PSPDFDocument *)document {
    __block NSMutableArray *queuedDocuments = [NSMutableArray array];

    pspdf_dispatch_sync_reentrant(_cacheMgmtQueue, ^{
        for (PSPDFCacheQueuedDocument *aQueuedItem in _queuedDocuments) {
            if ([aQueuedItem.document isEqual:document]) {
                [queuedDocuments addObject:aQueuedItem];
            }
        }
    });

    return queuedDocuments; // not returning an immutable copy, as this is a private method anyways
}

// determine if page should be pre-cached
- (BOOL)shouldMemoryCacheFullPage:(NSInteger)page forDocument:(PSPDFDocument *)document {
    int range = abs(document.displayingPage - page);
    BOOL isNearPage = document.displayingPage != NSNotFound && range <= floorf(self.numberOfMaximumCachedDocuments/2);
    if (kPSPDFLowMemoryMode) isNearPage = NO;
    BOOL isMemCached = [self imageForDocument:document page:page size:PSPDFSizeNative] != nil;
    BOOL shouldMemoryCacheFullPage = isNearPage && !isMemCached;
    if (shouldMemoryCacheFullPage) PSPDFLogVerbose(@"shouldMemoryCacheFullPage:%d -> YES", page);
    return shouldMemoryCacheFullPage;
}

// determine depending on the caching strategy if we should save the page on disk
- (BOOL)shouldSaveCachedImageForDocument:(PSPDFDocument *)document page:(NSUInteger)page size:(PSPDFSize)size {
    // cannot cache of uid is not set.
    if (!document.UID) { PSPDFLogWarning(@"Cannot cache, UID is nil!"); return NO; }

    // allows to override the cache strategy on a per-document bases.
    PSPDFCacheStrategy cacheStrategy = document.cacheStrategy != -1 ? document.cacheStrategy : self.strategy;

    BOOL shouldSave = NO;
    switch (cacheStrategy) {
        case PSPDFCacheThumbnails: {
            shouldSave = size != PSPDFSizeNative;
        }break;
        case PSPDFCacheThumbnailsAndNearPages: {
            PSPDFCacheQueuedDocument *queuedInfo = [self queuedDocumentFromDocument:document size:size];
            if (size != PSPDFSizeNative || (queuedInfo && abs(queuedInfo.page - page) < self.numberOfNearCachedPages)) {
                shouldSave = YES;
            }
        }break;
        case PSPDFCacheOpportunistic: { // opportunistic caching saves everything, clears later.
            shouldSave = YES;
        }break;
        case PSPDFCacheNothing:
        default: break;
    }

    return shouldSave;
}

// page starts at 0.
- (void)saveCachedImage:(UIImage *)renderedImage document:(PSPDFDocument *)document page:(NSUInteger)page size:(PSPDFSize)size {
    if ([self shouldSaveCachedImageForDocument:document page:page size:size]) {
        NSError *error = nil;

        // check that cache folder exists.
        NSString *documentCacheFolder = [self cachedImagePathForDocument:document];
        NSFileManager *fileManager = [NSFileManager new];
        if (![fileManager fileExistsAtPath:documentCacheFolder]) {
            if (![fileManager createDirectoryAtPath:documentCacheFolder withIntermediateDirectories:YES attributes:nil error:&error]) {
                PSPDFLogError(@"Unable to create cache folder at %@: %@", documentCacheFolder, error);
            }else {
#ifdef kPSPDFKitDemoMode
                // Write demo token. error is non-critical
                [@"" writeToFile:[documentCacheFolder stringByAppendingPathComponent:kPSPDFCacheDemoToken] atomically:YES encoding:NSASCIIStringEncoding error:NULL];
#endif
            }
        }

        NSData *imageData;
        NSString *cachedImagePathForDocumentPage = [self cachedImageFilePathForDocument:document page:page size:size];

        if (self.useJPGFormat) {
            imageData = [NSData dataWithData:UIImageJPEGRepresentation(renderedImage, self.JPGFormatCompression)];
        }else {
            imageData = [NSData dataWithData:UIImagePNGRepresentation(renderedImage)];
        }

        // writing is slow
        if (![imageData writeToFile:cachedImagePathForDocumentPage options:NSDataWritingAtomic error:&error]) {
            PSPDFLogWarning(@"Error while writing image: %@", [error localizedDescription]);
        }
        PSPDFLogVerbose(@"Wrote full-size page %d: %@", page, [self cachedImageFileNameForPage:page size:size]);

        // add to cachedFiles file-exist cache
        [self addFileToExistCacheForDocument:document page:page size:size];
    }
}

- (UIImage *)renderImageForDocument:(PSPDFDocument *)document page:(NSUInteger)page size:(PSPDFSize)size PDFPage:(CGPDFPageRef)pdfPage {
    if (!pdfPage) return nil; // if we didn't get a lock, return here. will try later again!

    // determine the size of the PDF page.
    PSPDFPageInfo *pageInfo = [document pageInfoForPage:page pageRef:pdfPage];
    CGRect cropBox = pageInfo.rotatedPageRect;

    // we're building the thumbnail here, don't send tiny size. Will reduce later!
    CGFloat pdfScale = [self scaleForImageSize:cropBox.size size:size];
    cropBox.size = PSPDFSizeForScale(cropBox.size, pdfScale);

    UIImage *renderedImage = [document renderImageForPage:page withSize:cropBox.size clippedToRect:CGRectZero withAnnotations:nil options:@{kPSPDFInterpolationQuality : @(self.downscaleInterpolationQuality), @"PSPDFSize" : @(size), kPSPDFCachedRenderRequest : @YES}];

    return renderedImage;
}

- (void)saveNativeRenderedImage:(UIImage *)image document:(PSPDFDocument *)document page:(NSUInteger)page {
    [self callDelegatesForDocument:document page:page image:image size:PSPDFSizeNative];
    [self saveCachedImage:image document:document page:page size:PSPDFSizeNative];
    PSPDFLog(@"image rendering complete: %@, page:%d", document, page);
}

// Creates a single pdf image out of a Document.
- (UIImage *)renderAndCacheImageForDocument:(PSPDFDocument *)document page:(NSUInteger)page size:(PSPDFSize)size error:(NSError **)error {
    if (page >= [document pageCount]) {
        PSPDFError(PSPDFErrorCodeUnknown, [NSString stringWithFormat:@"Requested Page does not exist: %d (pageCount: %d, document: %@)", page, [document pageCount], document], error);
        return nil;
    }
    PSPDFLogVerbose(@"Caching page %d for %@ (size:%d)", page, document.title, size);

    // rendered size may differ from requested size (optimizations)
    PSPDFSize renderedSize = (size == PSPDFSizeTiny) ? PSPDFSizeThumbnail : size;

    // if we want a tiny image, and the thumbnail is already there, use it (and skip pdf rendering)
    BOOL isCached = NO;
    UIImage *renderedImage = nil;
    if (size == PSPDFSizeTiny && [self isImageCachedForDocument:document page:page size:PSPDFSizeThumbnail]) {
        // load thumbnail from disk or cache
        renderedImage = [self cachedImageForDocument:document page:page size:PSPDFSizeThumbnail];
        isCached = YES;
    }

    // render pdf (unless already loaded)
    if (!renderedImage) {
        // open pdf
        CGPDFPageRef pdfPage = [[PSPDFGlobalLock sharedGlobalLock] tryLockWithDocument:document page:page error:error];
        if (!pdfPage) return nil;

        renderedImage = [self renderImageForDocument:document page:page size:renderedSize PDFPage:pdfPage];
        [[PSPDFGlobalLock sharedGlobalLock] freeWithPDFPageRef:pdfPage];
    }

    if (!renderedImage) { PSPDFLogError(@"Rendered image is nil. Delegates will not be called."); return nil; }

    // save image, call delegates
    if (size == PSPDFSizeNative) {
        [self callDelegatesForDocument:document page:(NSUInteger)page image:renderedImage size:size];
    }

    [self saveCachedImage:renderedImage document:document page:page size:renderedSize];
    PSPDFLogVerbose(@"Cached page %d %@ (size:%d).", page, document.title, size);

    // if size is tiny, we'll have to recode image
    if (size == PSPDFSizeTiny) {
        // internally, we created a thumbnail - send events for it!
        if (!isCached) {
            [self cacheImage:renderedImage document:document page:page size:PSPDFSizeThumbnail];
        }
        // tiny listeners listen for thumbnail too.
        [self callDelegatesForDocument:document page:page image:renderedImage size:PSPDFSizeThumbnail];

        // now reduce size to tiny
        CGFloat reduceFactor = [self scaleForImageSize:self.thumbnailSize size:size];
        CGSize tinySize = CGSizeMake(ceilf(reduceFactor*renderedImage.size.width), ceilf(reduceFactor*renderedImage.size.height));
        renderedImage = [renderedImage pspdf_imageToFitSize:tinySize method:PSPDFImageResizeCrop honorScaleFactor:YES opaque:YES];
        [self saveCachedImage:renderedImage document:document page:page size:size];
    }

    // save image in thumbnail cache
    if (size != PSPDFSizeNative || [self shouldMemoryCacheFullPage:page forDocument:document]) {
        [self cacheImage:renderedImage document:document page:page size:size];
    }

    // must be at the very bottom for the way we create tiny images
    if (size != PSPDFSizeNative) {
        [self callDelegatesForDocument:document page:(NSUInteger)page image:renderedImage size:size];
    }
    if (!renderedImage) PSPDFLogError(@"failed to render image for %@, page %d", document, page);

    return renderedImage;
}

- (void)cacheNextDocumentInStack {
    PSPDFLogVerbose(@"--cacheNextDocumentInStack--");

    // first check the queued items
    __block PSPDFCacheQueuedDocument *cacheRequest = nil;
    __block BOOL isCachingItems = NO;
    pspdf_dispatch_sync_reentrant(_cacheMgmtQueue, ^{
        // search a thumbnail request with prority, if not, use last in FIFO
        [_queuedItems enumerateObjectsWithOptions:NSEnumerationReverse usingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            PSPDFCacheQueuedDocument *aCacheRequest = (PSPDFCacheQueuedDocument *)obj;
            if (aCacheRequest.size != PSPDFSizeNative && !aCacheRequest.isCaching) {
                cacheRequest = aCacheRequest;
                *stop = YES;
            }
        }];

        // if no thumbnail request found, try again, this time use any request that's not currently running
        if (!cacheRequest) {
            [_queuedItems enumerateObjectsWithOptions:NSEnumerationReverse usingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                PSPDFCacheQueuedDocument *aCacheRequest = (PSPDFCacheQueuedDocument *)obj;
                if (!aCacheRequest.isCaching) {
                    cacheRequest = aCacheRequest;
                    *stop = YES;
                }else {
                    isCachingItems = YES;
                }
            }];
        }
    });

    if (cacheRequest) {
        PSPDFCacheQueuedDocument *request = cacheRequest;
        request.caching = YES;

        NSBlockOperation *blockOperation = [NSBlockOperation new];
        __weak NSBlockOperation *weakBlockOperation = blockOperation;
        [blockOperation addExecutionBlock:^{
            @autoreleasepool {
                PSPDFLogVerbose(@"starting operation for page: %d, size:%d", request.page, request.size);
                NSError *error = nil;

                PSPDFLogVerbose(@"BLOCK STARTED: %d", request.page);
                BOOL thumbnailCached = [self isImageCachedForDocument:request.document page:request.page size:request.size];
                BOOL shoudBeMemCached = [self shouldMemoryCacheFullPage:request.page forDocument:request.document];

                if (!thumbnailCached) {
                    UIImage *cachedImage = [self renderAndCacheImageForDocument:request.document page:request.page size:request.size error:&error];
                    if (error && !(error.domain == kPSPDFErrorDomain && error.code == PSPDFErrorCodeDocumentLocked)) {
                        PSPDFLogError(@"Error while caching: %@", error);
                    }

                    if (!error) {
                        if (cachedImage) {
                            thumbnailCached = YES;
                        }else {
                            // reset caching status, try again.
                            if (![weakBlockOperation isCancelled]) {
                                [NSThread sleepForTimeInterval:kPSPDFCachePause]; // sleep some time (system is rendering...)
                            }
                            request.caching = NO;
                        }
                    }
                }else if (shoudBeMemCached && request.size == PSPDFSizeNative) {
                    // is intelligent enough to automatically memcache if needed
                    PSPDFLogVerbose(@"mem-caching page:%d", request.page);
                    [self cachedImageForDocument:request.document page:request.page size:PSPDFSizeNative preload:YES];
                }

                if (thumbnailCached || error) {
                    pspdf_dispatch_sync_reentrant(_cacheMgmtQueue, ^{
                        if ([_queuedItems indexOfObject:request] != NSNotFound) {
                            [_queuedItems removeObject:request];
                        }});
                }

                // completion block
                if (![weakBlockOperation isCancelled]) {
                    [self timerFired];
                }

                PSPDFLogVerbose(@"BLOCK ENDED: %d", request.page);
            }
        }];

        blockOperation.threadPriority = 0.1f; // perform when everything is idle!
        [_cacheQueue addOperation:blockOperation];
        //PSPDFLog(@"operations: %d, concurrent: %d (waiting: %d)", [cacheQueue_ operationCount], [cacheQueue_ maxConcurrentOperationCount], [cachedFiles_ count]);
    }
    // just go away if we're still caching
    else if (!isCachingItems) {

        // if we're still there, yet don't have a caching request, look into the queued documents
        __block PSPDFCacheQueuedDocument *cacheMasterRequest = nil;
        pspdf_dispatch_sync_reentrant(_cacheRequestQueue, ^{
            pspdf_dispatch_sync_reentrant(_cacheMgmtQueue, ^{
                cacheMasterRequest = [_queuedDocuments lastObject];
            });

            // create a cache request out of it!
            if (cacheMasterRequest) {
                NSUInteger requestsCreated = 0;
                BOOL loopedOver = NO;
                for (NSInteger i=cacheMasterRequest.page; i >= 0;) {

                    NSNumber *iNum = @(i);
                    BOOL isCached = [self isImageCachedForDocument:cacheMasterRequest.document page:i size:cacheMasterRequest.size];
                    BOOL shouldSave = [self shouldSaveCachedImageForDocument:cacheMasterRequest.document page:i size:cacheMasterRequest.size];
                    BOOL shoudBeMemCached = [self shouldMemoryCacheFullPage:i forDocument:cacheMasterRequest.document];
                    BOOL alreadyTriedToCache = [cacheMasterRequest.pagesCached containsObject:iNum];

                    if ((!isCached && shouldSave) || (shoudBeMemCached && !alreadyTriedToCache)) {
                        [cacheMasterRequest.pagesCached addObject:iNum];
                        PSPDFCacheQueuedDocument *queueItem = [PSPDFCacheQueuedDocument queuedDocumentWithDocument:cacheMasterRequest.document page:i size:cacheMasterRequest.size];
                        if ([queueItem.document isValid]) {
                            [self enqueueItem:queueItem];
                            requestsCreated++;

                            if (requestsCreated >= [self numberOfConcurrentCacheOperations]) {
                                break;
                            }
                        }
                    }

                    // first, look at the FOLLOWING pages, then look BACKWARDS (intelligent caching)
                    NSInteger pageCount = [cacheMasterRequest.document pageCount];
                    if (!loopedOver && i >= pageCount-1) {
                        loopedOver = YES;
                        i = cacheMasterRequest.page;
                    }else if (loopedOver && i > pageCount) {
                        break; // quit special for loop
                    }

                    if (!loopedOver) {
                        i++;
                    }else {
                        i--;
                    }
                }

                // if we created a request, great, call ourself and get out of here
                if (requestsCreated == 0) {
                    PSPDFLogVerbose(@"Document finished caching: %@", cacheMasterRequest.document);
                    pspdf_dispatch_sync_reentrant(_cacheMgmtQueue, ^{
                        if ([_queuedDocuments indexOfObject:cacheMasterRequest] != NSNotFound) {
                            [_queuedDocuments removeObject:cacheMasterRequest];
                        }
                        BOOL moreJobsForDocument = NO;
                        for (PSPDFCacheQueuedDocument *item in _queuedDocuments) {
                            if ([item.document isEqual:cacheMasterRequest.document]) {
                                moreJobsForDocument = YES;
                                break;
                            }
                        }
                        if (!moreJobsForDocument) {
                            [self callDelegatesForDocument:cacheMasterRequest.document];
                        }
                    });
                }

                // only fire if no operations are currently running
                if (![_cacheQueue operationCount] <= [self numberOfConcurrentCacheOperations] && [_queuedItems count]) {
                    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
                        [self cacheNextDocumentInStack];
                    });
                }
            }
        });
    }
}

- (void)timerFired {
    // start caching if there's stuff in the queue [and the operation queue is empty!
    NSUInteger operationCount = [_cacheQueue operationCount];
    if (([_queuedItems count] || [_queuedDocuments count]) && operationCount < [self numberOfConcurrentCacheOperations]) {

        // don't call cacheNextDocumentInStack directly, or we can deadlock in PSPDFCache.
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            [self cacheNextDocumentInStack];
        });
    }
}

// enqueues an cache request in the current cache
- (void)enqueueItem:(PSPDFCacheQueuedDocument *)item {
    if (![item.document isValid]) {
        if (item.document == nil) PSPDFLogVerbose(@"Warning: Document is nil!");
        return;
    }

    pspdf_dispatch_sync_reentrant(_cacheMgmtQueue, ^{
        // perform this inside the block because pageCount maybe is lazy evaluated and we would spawn a lot of threads otherwise
        if (item.page == NSNotFound || item.page >= [item.document pageCount]) {
            PSPDFLogWarning(@"Page is invalid: %d (pageCount: %d)", item.page, [item.document pageCount]);
        }else {
            PSPDFCacheQueuedDocument *queuedDocument = item;

            // queue is unique - if already in there, move to bottom!
            NSUInteger currentPos = [_queuedItems indexOfObject:queuedDocument];
            if (currentPos != NSNotFound) {
                // preserve object! (cached status, etc)
                queuedDocument = _queuedItems[currentPos];
                [_queuedItems removeObjectAtIndex:currentPos];
            }

            // always add to bottom!
            [_queuedItems addObject:queuedDocument];
        }
    });
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        [self cacheNextDocumentInStack];
    });
}

- (void)didReceiveMemoryWarning {
    // on memory warning, clear full page cache instantly
    [self.fullPageCache removeAllObjects];

    if (PSPDFIsCrappyDevice()) {
        [self.thumbnailCache removeAllObjects];
    }
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSCacheDelegate

- (void)cache:(NSCache *)cache willEvictObject:(id)obj {
    PSPDFLogVerbose(@"object removed from %@: %@ (size: %@)", cache.name, obj, NSStringFromCGSize([(UIImage *)obj size]));
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Singleton

+ (PSPDFCache *)sharedCache {
    __strong static PSPDFCache *_sharedCache = nil;
    static dispatch_once_t pred;
    dispatch_once(&pred, ^{ _sharedCache = [NSClassFromString(kPSPDFCacheClassName) new]; });
    return _sharedCache;
}

@end

@implementation PSPDFCacheQueuedDocument

+ (PSPDFCacheQueuedDocument *)queuedDocumentWithDocument:(PSPDFDocument *)document page:(NSUInteger)page size:(PSPDFSize)size {
    PSPDFCacheQueuedDocument *queuedDocument = [[self class] new];
    queuedDocument.document = document;
    queuedDocument.page = page;
    queuedDocument.size = size;
    return queuedDocument;
}

- (id)init {
    if ((self = [super init])) {
        _pagesCached = [[NSMutableSet alloc] init];
    }
    return self;
}

- (NSUInteger)hash {
    // http://stackoverflow.com/questions/254281/best-practices-for-overriding-isequal-and-hash/254380#254380
    return (([_document hash] + _page) * 31) + (_size ? 1231 : 1237);
}

- (BOOL)isEqual:(id)other {
    if ([other isKindOfClass:[self class]]) {
        PSPDFCacheQueuedDocument *otherQueueItem = (PSPDFCacheQueuedDocument *)other;
        if (_page == otherQueueItem.page && _size == otherQueueItem.size) {
            if (![_document isEqual:[other document]] || !_document || ![other document]) {
                return NO;
            }
            return YES;
        }
    }
    return NO;
}

static NSString *NSStringFromPSPDFSize(PSPDFSize size) {
    switch(size) {
        case PSPDFSizeNative:    return @"Native";
        case PSPDFSizeThumbnail: return @"Thumbnail";
        case PSPDFSizeTiny:      return @"Tiny";
        default:                 return @"";
    }
}

- (NSString *)description {
    return [NSString stringWithFormat:@"<%@ document:%@ page:%d size:%@ caching:%d>", NSStringFromClass([self class]), self.document, self.page, NSStringFromPSPDFSize(self.size), self.caching];
}

@end
