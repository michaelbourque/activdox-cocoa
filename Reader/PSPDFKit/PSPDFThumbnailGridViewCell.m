//
//  PSPDFThumbnailGridViewCell.m
//  PSPDFKit
//
//  Copyright 2011-2012 Peter Steinberger. All rights reserved.
//

#import "PSPDFKit.h"
#import "PSPDFIconGenerator.h"
#import "PSPDFBookmarkParser.h"
#import <QuartzCore/QuartzCore.h>

// Simple rounded label.
@interface PSPDFRoundedLabel : UILabel
@property (nonatomic, assign) NSInteger cornerRadius;
@property (nonatomic, strong) UIColor *rectColor;
@end

@interface PSPDFThumbnailGridViewCell() {
    CGSize _customImageSize;      // allow custom image size even if image is not set.
    UIImageView *_bookmarkImage;
    CALayer *_shadowLayer;
}
@property (nonatomic, strong) UIImageView *imageView;
@property (nonatomic, strong) UILabel *siteLabel;
@end

@implementation PSPDFThumbnailGridViewCell

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Static

// custom queue for thumbnail parsing
+ (NSOperationQueue *)thumbnailQueue {
    static NSOperationQueue *_queue;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _queue = [[NSOperationQueue alloc] init];
        _queue.maxConcurrentOperationCount = 4;
        _queue.name = @"PSPDFThumbnailQueue";
    });
    return _queue;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (id)initWithFrame:(CGRect)frame {
    if ((self = [super initWithFrame:frame])) {
        self.clipsToBounds = NO;   // allow drop shadow
        self.exclusiveTouch = YES; // don't allow touching more cells at once

        self.contentView.layer.masksToBounds = NO;
        self.contentView.layer.shadowOffset = CGSizeMake(5, 5);
        self.contentView.layer.shadowPath = [UIBezierPath bezierPathWithRect:self.bounds].CGPath;

        _imageView = [[UIImageView alloc] initWithFrame:frame];
        _imageView.clipsToBounds = YES;
        _imageView.contentMode = UIViewContentModeScaleAspectFill;
        [self updateImageViewBackgroundColor];

        [self.contentView addSubview:_imageView];
        self.contentView.backgroundColor = [UIColor clearColor];
        self.contentView.opaque = NO;
        self.backgroundColor = [UIColor clearColor];
        self.opaque = NO;

        // shadow is enabled per default
        _shadowEnabled = !PSPDFIsCrappyDevice();
        _showingSiteLabel = YES;
        _edgeInsets = UIEdgeInsetsZero;

        [[PSPDFCache sharedCache] addDelegate:self];
        [self setNeedsLayout];
    }
    return self;
}

- (void)dealloc {
    [[PSPDFCache sharedCache] removeDelegate:self];
}

- (NSString *)description {
    return [NSString stringWithFormat:@"%@ page:%d>", [super description], self.page];
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public

- (void)updateCell {
    [self updateBookmarkImage];
}

- (void)setPage:(NSUInteger)page {
    _page = page;

    if (self.document) {
        [self updateBookmarkImage];
        [self loadImageAsync];
    }

    [self updateCell];
    self.accessibilityLabel = [NSString stringWithFormat:@"%@ %d", PSPDFLocalize(@"Page"), page+1];
}

- (void)setShadowEnabled:(BOOL)shadowEnabled {
    _shadowEnabled = shadowEnabled;
    [self setNeedsLayout];
}

- (void)setShowingSiteLabel:(BOOL)showingSiteLabel {
    _showingSiteLabel = showingSiteLabel;
    [self setNeedsLayout];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    [self updateImageSize];
    [self updateShadow];
    [self updateBookmarkImage];
    [self updateSiteLabel];
}

- (void)prepareForReuse {
    [super prepareForReuse];
    self.document = nil;
    self.page = 0;
    _imageView.image = nil;
    [self updateImageViewBackgroundColor];
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Private

- (void)updateBookmarkImage {
    if (!self.document) {
        _bookmarkImage.hidden = YES;
    }else {
        BOOL hasBookmark = [self.document.bookmarkParser bookmarkForPage:self.page] != nil;
        if (hasBookmark) {
            if (!_bookmarkImage) {
                UIImage *bookmarkImage = [[[PSPDFIconGenerator sharedGenerator] iconForType:PSPDFIconTypeBookmarkActive] pdpdf_imageTintedWithColor:[UIColor redColor] fraction:0.3f];
                
                _bookmarkImage = [[UIImageView alloc] initWithImage:bookmarkImage];
                [_bookmarkImage sizeToFit];
                [self addSubview:_bookmarkImage];
                [self setNeedsDisplay];
            }
            
            _bookmarkImage.frame = CGRectMake(CGRectGetMaxX(_imageView.frame)-30, _imageView.frame.origin.y-1, _bookmarkImage.bounds.size.width, _bookmarkImage.bounds.size.height);
        }
        _bookmarkImage.hidden = !hasBookmark;
    }
}

/// Creates the shadow. Subclass to change. Returns a CGPathRef.
- (id)pathShadowForView:(UIView *)imgView {
    CGSize size = imgView.bounds.size;
    UIBezierPath *path = nil;

    CGFloat moveShadow = -8;
    path = [UIBezierPath bezierPathWithRect:CGRectMake(moveShadow, moveShadow, size.width+fabs(moveShadow/2), size.height+fabs(moveShadow/2))];

    // copy path, else ARC instantly deallocates the UIBezierPath backing store
    id cgPath = path ? CFBridgingRelease(CGPathCreateCopy(path.CGPath)) : nil;
    return cgPath;
}

- (void)updateShadow {
    if (!_shadowLayer && self.isShadowEnabled) {
        _shadowLayer = [[CALayer alloc] init];
        [self.contentView.layer insertSublayer:_shadowLayer atIndex:1];
        [self.contentView bringSubviewToFront:self.imageView];
    }

    // enable/disable shadows
    CALayer *shadowLayer = _shadowLayer;
    if (!CGRectEqualToRect(shadowLayer.frame, self.imageView.frame) || (!self.isShadowEnabled && shadowLayer)) {
        [CATransaction begin];
        [CATransaction setDisableActions:YES];
        shadowLayer.shadowPath = (__bridge CGPathRef)[self pathShadowForView:self.imageView];
        shadowLayer.frame = self.imageView.frame;

        CGFloat shadowRadius = 2.f;
        if (self.isShadowEnabled && shadowLayer.shadowRadius != shadowRadius) {
            shadowLayer.shadowColor = [UIColor blackColor].CGColor;
            shadowLayer.shadowOpacity = 0.5f;
            shadowLayer.shadowOffset = CGSizeMake(6.0f, 6.0f);
            shadowLayer.shadowRadius = shadowRadius;
            shadowLayer.masksToBounds = NO;
        }else if (!self.isShadowEnabled && shadowLayer.shadowRadius > 0.f) {
            shadowLayer.shadowRadius = 0.f;
            shadowLayer.shadowOpacity = 0.f;
        }
        [CATransaction commit];
    }
}

#define kPSPDFThumbnailLabelHeight 20
- (void)updateSiteLabel {
    CGFloat cornerRadius = 10.f;
    if (self.isShowingSiteLabel && !_siteLabel.superview) {
        PSPDFRoundedLabel *siteLabel = [[PSPDFRoundedLabel alloc] initWithFrame:CGRectZero];
        siteLabel.backgroundColor = [UIColor colorWithWhite:0.f alpha:0.6f];
        siteLabel.textColor = [UIColor colorWithWhite:1.f alpha:1.f];
        siteLabel.textAlignment = UITextAlignmentCenter;
        siteLabel.font = [UIFont boldSystemFontOfSize:16];
        siteLabel.minimumFontSize = 10;
        siteLabel.cornerRadius = cornerRadius;
        siteLabel.adjustsFontSizeToFitWidth = YES;
        _siteLabel = siteLabel;
        [self.contentView addSubview:_siteLabel];
    }else if (!self.isShowingSiteLabel && _siteLabel.superview) {
        [_siteLabel removeFromSuperview];
    }

    // calculate new frame and position correct
    [_siteLabel sizeToFit];

    // limit width!
    CGRect labelRect = _siteLabel.frame; labelRect.size.width = fminf(labelRect.size.width, self.imageView.frame.size.width - cornerRadius*2); _siteLabel.frame = labelRect;

    CGFloat siteLabelWidth = _siteLabel.frame.size.width + 20.f;
    _siteLabel.frame = CGRectIntegral(CGRectMake(self.imageView.frame.origin.x + (self.imageView.frame.size.width-siteLabelWidth)/2, self.imageView.frame.origin.y+self.imageView.frame.size.height-kPSPDFThumbnailLabelHeight-5.f, siteLabelWidth, kPSPDFThumbnailLabelHeight));
    if (_siteLabel.superview) {
        [self.contentView bringSubviewToFront:_siteLabel];
    }
}

- (void)setImageSize:(CGSize)imageSize {
    // set aspect ratio and center image
    _customImageSize = imageSize;
    [self updateImageSize];
    [self updateShadow];
    [self updateSiteLabel];
}

- (void)updateImageSize {
    CGRect newFrame = UIEdgeInsetsInsetRect(self.bounds, _edgeInsets);
    CGSize imageSize = self.imageView.image ? self.imageView.image.size : _customImageSize;
    if (!CGSizeEqualToSize(imageSize, CGSizeZero)) {
        CGFloat scale = PSPDFScaleForSizeWithinSize(imageSize, newFrame.size);
        CGSize thumbSize = CGSizeMake(roundf(imageSize.width * scale), roundf(imageSize.height * scale));
        self.imageView.frame = CGRectMake(roundf(_edgeInsets.left+_edgeInsets.right+(newFrame.size.width-thumbSize.width)/2.f), roundf(_edgeInsets.top+(newFrame.size.height-thumbSize.height)/2.f), thumbSize.width, thumbSize.height);
    }else {
        self.imageView.frame = newFrame;
    }
}

- (void)updateImageViewBackgroundColor {
    _imageView.backgroundColor = _imageView.image ? [UIColor clearColor] : [UIColor colorWithWhite:1.f alpha:0.8f];
}

- (void)setImage:(UIImage *)image animated:(BOOL)animated {
    if (animated) {
        [self.imageView.layer addAnimation:PSPDFFadeTransition() forKey:@"image"];
    }

    self.imageView.image = image;
    self.imageView.opaque = YES;
    self.opaque = YES;
    CGSize imageSize = image ? image.size : CGSizeZero;
    [self setImageSize:imageSize];
    [self updateImageViewBackgroundColor];
}

// tries to load thumbnail - loads it async if not existing
- (void)loadImageAsync {
    if (!self.document) { PSPDFLogWarning(@"Document is nil!"); return; }

    // capture data
    NSUInteger page = self.page;
    PSPDFDocument *document = self.document;

    // only returns image directly if it's already in memory
    UIImage *cachedImage = [[PSPDFCache sharedCache] imageForDocument:document page:page size:PSPDFSizeThumbnail];
    if (cachedImage) {
        [self setImage:cachedImage animated:NO];
    }else {
        // at least try to set correct aspect ratio
        PSPDFPageInfo *pageInfo = nil;
        if ([self.document hasPageInfoForPage:self.page]) {
            pageInfo = [self.document pageInfoForPage:self.page];
        }else {
            // just try to get the pageInfo for the last detected page instead (will work in many cases)
            pageInfo = [self.document nearestPageInfoForPage:self.page];
        }

        if (pageInfo) {
            [self setImageSize:pageInfo.rotatedPageRect.size];
        }else {
            [self setImageSize:self.bounds.size];
        }

        // load image in background
        NSBlockOperation *blockOperation = [NSBlockOperation blockOperationWithBlock:^{
            @autoreleasepool {
                BOOL shouldPreload = YES;
                UIImage *thumbnailImage = nil;

                // try to load image directly from document
                NSURL *thumbImagePath = [document cachedImageURLForPage:page andSize:PSPDFSizeThumbnail];
                if (thumbImagePath) {
                    thumbnailImage = [UIImage pspdf_preloadedImageWithContentsOfFile:[thumbImagePath path]];

                    if (thumbnailImage) {
                        [[PSPDFCache sharedCache] cacheImage:thumbnailImage document:document page:page size:PSPDFSizeThumbnail];
                    }

                    // external thumbs may are too large - need shrinking (or else system is slow in scrolling)
                    if (thumbnailImage && ((thumbnailImage.size.width / self.bounds.size.width > kPSPDFShrinkOwnImagesTresholdFactor) ||
                                           (thumbnailImage.size.height / self.bounds.size.height > kPSPDFShrinkOwnImagesTresholdFactor))) {
                        PSPDFLogVerbose(@"apply additional shrinking for image cells to %@", NSStringFromCGRect(self.bounds));

                        thumbnailImage = [thumbnailImage pspdf_imageToFitSize:self.bounds.size method:PSPDFImageResizeCrop honorScaleFactor:YES opaque:YES];
                        shouldPreload = NO;
                    }
                }

                // if we still miss a thumbnail, try to get a cached one from the cache
                if (!thumbnailImage) {
                    thumbnailImage = [[PSPDFCache sharedCache] cachedImageForDocument:document page:page size:PSPDFSizeThumbnail preload:shouldPreload];
                }

                // we may or may not have the thumbnail now
                if (thumbnailImage) {
                    // set image in main thread
                    dispatch_async(dispatch_get_main_queue(), ^{
                        if (page == self.page && document == self.document) {
                            [self setImage:thumbnailImage animated:!PSPDFIsCrappyDevice()];
                        }else {
                            PSPDFLogVerbose(@"Ignoring loaded thumbnail...");
                        }
                    });
                }
            }
        }];
        [[[self class] thumbnailQueue] addOperation:blockOperation];
    }
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Touches

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [super touchesBegan:touches withEvent:event];
    self.imageView.alpha = 0.7f;
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event {
    [super touchesCancelled:touches withEvent:event];
    self.imageView.alpha = 1.0f;
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    [super touchesEnded:touches withEvent:event];
    self.imageView.alpha = 1.0f;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - PSPDFCacheDelegate

- (void)didCachePageForDocument:(PSPDFDocument *)pdfdocument page:(NSUInteger)aPage image:(UIImage *)cachedImage size:(PSPDFSize)size {
    if (self.document == pdfdocument && aPage == self.page && size == PSPDFSizeThumbnail) {
        [self setImage:cachedImage animated:YES];
    }
}

@end


@implementation PSPDFRoundedLabel

- (void)setBackgroundColor:(UIColor *)color {
    [super setBackgroundColor:[UIColor clearColor]];
    self.rectColor = color;
}

- (void)drawRect:(CGRect)rect {
    CGContextRef context = UIGraphicsGetCurrentContext();

    // draw rounded background
    CGContextAddPath(context, [UIBezierPath bezierPathWithRoundedRect:rect cornerRadius:_cornerRadius].CGPath);
    CGContextSetFillColorWithColor(context, _rectColor.CGColor);
    CGContextFillPath(context);
    
    // draw text
    [super drawRect:rect];
}

@end
