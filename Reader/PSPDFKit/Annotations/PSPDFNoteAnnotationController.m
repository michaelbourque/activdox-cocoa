//
//  PSPDFTextAnnotationController.m
//  PSPDFKit
//
//  Copyright (c) 2012 Peter Steinberger. All rights reserved.
//

#import "PSPDFNoteAnnotationController.h"
#import "PSPDFNoteAnnotation.h"
#import "PSPDFAnnotation.h"
#import "PSPDFDocument.h"
#import "PSPDFAnnotationParser.h"
#import "PSPDFActionSheet.h"
#import "PSPDFGradientView.h"
#import "PSPDFConverter.h"
#import "PSPDFColorButton.h"
#import "PSPDFPopoverController.h"
#import "PSPDFTransparentToolbar.h"
#import "UIImage+PSPDFKitAdditions.h"
#import "UIColor+PSPDFKitAdditions.h"
#import <QuartzCore/QuartzCore.h>

@interface PSPDFNoteAnnotationController () <UITextViewDelegate, PSPDFPopoverControllerDismissable>
@property (nonatomic, strong) UITextView *textView;
@property (nonatomic, copy) NSArray *iconButtons;
@property (nonatomic, strong) PSPDFGradientView *backgroundView;
@property (nonatomic, strong) UIView *optionsView;
@end

@implementation PSPDFNoteAnnotationController

@synthesize annotation = _annotation;
@synthesize shouldTintToolbarButtons = _shouldTintToolbarButtons;
@synthesize tintColor = _tintColor;

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (id)initWithAnnotation:(PSPDFAnnotation *)annotation editable:(BOOL)allowEditing {
    if ((self = [super initWithNibName:nil bundle:nil])) {
        switch (annotation.type) {
            case PSPDFAnnotationTypeNote:
                self.title = PSPDFLocalize(@"Note"); break;
            case PSPDFAnnotationTypeText:
                self.title = PSPDFLocalize(@"Free Text"); break;
            case PSPDFAnnotationTypeHighlight:
            case PSPDFAnnotationTypeShape:
            default:
                self.title = PSPDFLocalize(@"Text"); break;
        }

        // if a user is set, this will override the default title.
        if ([annotation.user length]) self.title = annotation.user;

        _annotation = annotation;
        _allowEditing = allowEditing;
        _showColorAndIconOptions = YES;
        self.contentSizeForViewInPopover = CGSizeMake(300, 300);

        // create textView early to make it easier ot customize externally.
        self.textView = [[UITextView alloc] initWithFrame:CGRectZero];
        self.textView.font = [UIFont fontWithName:@"Noteworthy" size:18.f];
        _textView.backgroundColor = [UIColor clearColor];
        _textView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    }
    return self;
}

- (void)dealloc {
    _textView.delegate = nil;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UIViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

//    [self.textView becomeFirstResponder];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];

    [self.textView resignFirstResponder];

    if ([self.delegate respondsToSelector:@selector(noteAnnotationController:willDismissWithAnnotation:)]) {
        [self.delegate noteAnnotationController:self willDismissWithAnnotation:self.annotation];
    }
}

- (void)createToolbar {
    if (self.allowEditing) {
        NSMutableArray *toolbarItems = [NSMutableArray arrayWithCapacity:2];
        
        // edit is only possible for note annotations
        if (self.annotation.type == PSPDFAnnotationTypeNote && self.showColorAndIconOptions) {
            [toolbarItems addObject:[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemEdit target:self action:@selector(toggleOptions:)]];
        }
        
        // always add trash
        [toolbarItems addObject:[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemTrash target:self action:@selector(deleteAnnotation:)]];
        
        // tint each button on it's own.
        if (self.shouldTintToolbarButtons) {
            [toolbarItems makeObjectsPerformSelector:@selector(setTintColor:) withObject:self.tintColor];
        }
        
        if ([toolbarItems count] == 2) {
            NSMutableArray *leftBarButtonItems = [self.navigationItem.leftBarButtonItems mutableCopy] ?: [NSMutableArray array];
            [leftBarButtonItems addObject:toolbarItems[0]];
            self.navigationItem.leftBarButtonItems = leftBarButtonItems;
        }
        NSMutableArray *rightBarButtonItems = [self.navigationItem.rightBarButtonItems mutableCopy] ?: [NSMutableArray array];
        [rightBarButtonItems addObject:[toolbarItems lastObject]];
        self.navigationItem.rightBarButtonItems = rightBarButtonItems;
    }else {
        self.navigationItem.leftBarButtonItems = nil;
        self.navigationItem.rightBarButtonItems = nil;
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];

    // The toolbar of a UIPopverController has a private style and doesn't support tinting. We tint with PSPDFPopoverBackgroundView. (but enable to tint for iPhone)
    self.navigationController.navigationBar.tintColor = self.tintColor;

    [self createToolbar];

    self.backgroundView = [[PSPDFGradientView alloc] initWithFrame:self.view.bounds];
    self.backgroundView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    [self.view addSubview:self.backgroundView];

    self.textView.frame = CGRectInset(self.view.bounds, 5, 5);
    self.textView.delegate = self;
    [self.view addSubview:self.textView];
    [self updateTextView];

    // create option view
    NSArray *colors = @[[UIColor colorWithRed:1.0 green:0.85 blue:0.25 alpha:1],
                       [UIColor colorWithRed:1.0 green:0.5 blue:0 alpha:1],
                       [UIColor colorWithRed:1.0 green:0.2 blue:0 alpha:1],
                       [UIColor colorWithRed:0.9 green:0.2 blue:1.0 alpha:1],
                       [UIColor colorWithRed:0.0 green:0.6 blue:1.0 alpha:1],
                       [UIColor colorWithRed:0.45 green:1.0 blue:0.0 alpha:1]];

    if (self.annotation.type == PSPDFAnnotationTypeNote) {
        PSPDFNoteAnnotation *noteAnnotation = (PSPDFNoteAnnotation *)self.annotation;
        self.optionsView = [[UIView alloc] initWithFrame:CGRectMake(0, self.view.bounds.size.height, 240, 116)];
        _optionsView.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleWidth;
        _optionsView.backgroundColor = [UIColor colorWithRed:0.086 green:0.118 blue:0.196 alpha:1.000];

        float x = 4;
        for (UIColor *color in colors) {
            PSPDFColorButton *colorButton = [PSPDFColorButton buttonWithType:UIButtonTypeCustom];
            colorButton.frame = CGRectMake(x, 79, 32, 32);
            colorButton.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleWidth;
            colorButton.color = color;
            colorButton.contentMode = UIViewContentModeRedraw;
            [colorButton addTarget:self action:@selector(changeColor:) forControlEvents:UIControlEventTouchUpInside];
            [_optionsView addSubview:colorButton];
            x += 40;
        }

        x = 4;
        float y = 5;
        int i = 0;
        NSMutableArray *buttons = [NSMutableArray array];
        NSArray *iconImageNames = [self iconNames];

        for (NSString *iconName in iconImageNames) {
            UIButton *iconButton = [UIButton buttonWithType:UIButtonTypeCustom];
            iconButton.tag = i;
            [iconButton addTarget:self action:@selector(changeIcon:) forControlEvents:UIControlEventTouchUpInside];
            iconButton.showsTouchWhenHighlighted = YES;
            [iconButton setImage:[[UIImage imageNamed:[NSString stringWithFormat:@"PSPDFKit.bundle/%@", iconName]] pdpdf_imageTintedWithColor:[UIColor whiteColor] fraction:0.f] forState:UIControlStateNormal];
            [iconButton setFrame:CGRectMake(x, y, 32, 32)];
            [buttons addObject:iconButton];
            if ([iconName isEqualToString:noteAnnotation.iconName]) {
                iconButton.layer.cornerRadius = 5.0;
                iconButton.layer.borderWidth = 2.0;
                iconButton.layer.borderColor = [[UIColor whiteColor] CGColor];
            }
            iconButton.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleWidth;
            [_optionsView addSubview:iconButton];
            x += 40;
            if (x >= 240) {
                x = 4;
                y += 32 + 5;
            }
            i++;
        }
        self.iconButtons = [NSArray arrayWithArray:buttons];
        _optionsView.frame = CGRectMake(0, self.view.bounds.size.height, self.view.bounds.size.width, 116);
        [self.view addSubview:_optionsView];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    if (![self isViewLoaded]) {
        self.textView.delegate = nil;
    }
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public

- (void)setShowColorAndIconOptions:(BOOL)showColorAndIconOptions {
    _showColorAndIconOptions = showColorAndIconOptions;
    [self createToolbar];
}

- (void)setAnnotation:(PSPDFNoteAnnotation *)annotation {
    if (annotation != _annotation) {
        _annotation = annotation;
        [self updateTextView];
    }
}

- (NSString *)deleteAnnotationActionTitle {
    NSString *deleteTitle;
    switch (self.annotation.type) {
        case PSPDFAnnotationTypeNote: deleteTitle = PSPDFLocalize(@"Delete Note"); break;
        case PSPDFAnnotationTypeText: deleteTitle = PSPDFLocalize(@"Delete Free Text"); break;
        case PSPDFAnnotationTypeHighlight: deleteTitle = PSPDFLocalize(@"Delete Highlight"); break;
        default: deleteTitle = PSPDFLocalize(@"Remove");
    }
    return deleteTitle;
}

- (void)deleteAnnotation:(UIBarButtonItem *)barButtonItem {
    PSPDFActionSheet *sheet = [[PSPDFActionSheet alloc] initWithTitle:nil];
    [sheet setDestructiveButtonWithTitle:[self deleteAnnotationActionTitle] block:^{
        self.annotation.deleted = YES;

        // send change event for deleted
        [[NSNotificationCenter defaultCenter] postNotificationName:PSPDFAnnotationChangedNotification object:self.annotation userInfo:@{PSPDFAnnotationChangedNotificationKeyPathKey : @[NSStringFromSelector(@selector(isDeleted))], PSPDFAnnotationChangedNotificationOriginalAnnotationKey : self.annotation}];

        if ([self.delegate respondsToSelector:@selector(noteAnnotationController:didDeleteAnnotation:)]) {
            [self.delegate noteAnnotationController:self didDeleteAnnotation:self.annotation];
        }
    }];
    [sheet setCancelButtonWithTitle:PSPDFLocalize(@"Cancel") block:NULL];
    [sheet showFromBarButtonItem:barButtonItem animated:YES];
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Private

// recognized iconNames
- (NSArray *)iconNames {
	return @[@"Comment", @"RightPointer", @"RightArrow", @"Check", @"Circle", @"Cross", @"Insert", @"NewParagraph", @"Note", @"Paragraph", @"Help", @"Star"];
}

- (void)toggleOptions:(id)sender {
    [UIView animateWithDuration:0.25f delay:0.f options:UIViewAnimationOptionAllowUserInteraction | UIViewAnimationOptionBeginFromCurrentState animations:^{
        if (self.optionsView.frame.origin.y >= self.view.bounds.size.height) {
            self.optionsView.frame = CGRectMake(0, self.view.bounds.size.height - 116, self.view.bounds.size.width, 116);
            CGRect textViewFrame = CGRectInset(self.view.bounds, 5, 5);
            textViewFrame.size.height -= 116;
            self.textView.frame = textViewFrame;

            // show options menu on iPhone.
            if (!PSIsIpad()) [self.textView resignFirstResponder];
        } else {
            self.optionsView.frame = CGRectMake(0, self.view.bounds.size.height, self.view.bounds.size.width, 116);
            self.textView.frame = CGRectInset(self.view.bounds, 5, 5);

            // show options menu on iPhone.
            if (!PSIsIpad()) [self.textView becomeFirstResponder];
        }
    } completion:NULL];
}

- (void)changeColor:(PSPDFColorButton *)colorButton {
    if (self.annotation.type != PSPDFAnnotationTypeNote) return;

	UIColor *color = colorButton.color;
	self.backgroundView.colors = PSPDFGradientColorsForColor(color);

    // copy and edit
    PSPDFNoteAnnotation *originalAnnotation = (PSPDFNoteAnnotation *)self.annotation;
    self.annotation = [self.annotation copyAndDeleteOriginalIfNeeded];
    if (originalAnnotation == self.annotation) {
        originalAnnotation = [self.annotation copy]; // ensure original annotation doesn't change
    }
    self.annotation.color = colorButton.color;
    [[NSNotificationCenter defaultCenter] postNotificationName:PSPDFAnnotationChangedNotification object:self.annotation userInfo:@{PSPDFAnnotationChangedNotificationKeyPathKey : @[NSStringFromSelector(@selector(color))], PSPDFAnnotationChangedNotificationOriginalAnnotationKey : originalAnnotation}];

    if ([self.delegate respondsToSelector:@selector(noteAnnotationController:didChangeAnnotation:originalAnnotation:)]) {
        [self.delegate noteAnnotationController:self didChangeAnnotation:self.annotation originalAnnotation:originalAnnotation];
    }
}

- (void)changeIcon:(UIButton *)sender {
    if (self.annotation.type != PSPDFAnnotationTypeNote) return;

	for (UIButton *button in self.iconButtons) {
		button.layer.cornerRadius = 0.0;
		button.layer.borderWidth = 0.0;
		button.layer.borderColor = [[UIColor clearColor] CGColor];
	}

	sender.layer.cornerRadius = 5.0;
	sender.layer.borderWidth = 2.0;
	sender.layer.borderColor = [[UIColor whiteColor] CGColor];

    // copy and edit
    PSPDFNoteAnnotation *originalAnnotation = (PSPDFNoteAnnotation *)self.annotation;
    self.annotation = [self.annotation copyAndDeleteOriginalIfNeeded];
    if (originalAnnotation == self.annotation) {
        originalAnnotation = [self.annotation copy]; // ensure original annotation doesn't change
    }

    ((PSPDFNoteAnnotation *)self.annotation).iconName = [self iconNames][sender.tag];

    [[NSNotificationCenter defaultCenter] postNotificationName:PSPDFAnnotationChangedNotification object:self.annotation userInfo:@{PSPDFAnnotationChangedNotificationKeyPathKey : @[NSStringFromSelector(@selector(iconName))], PSPDFAnnotationChangedNotificationOriginalAnnotationKey : originalAnnotation}];

    if ([self.delegate respondsToSelector:@selector(noteAnnotationController:didChangeAnnotation:originalAnnotation:)]) {
        [self.delegate noteAnnotationController:self didChangeAnnotation:self.annotation originalAnnotation:originalAnnotation];
    }
}

- (void)updateTextView {
    self.textView.text = self.annotation.contents;
    // fill color has priority (e.g. shape annotations)
//    self.backgroundView.colors = PSPDFGradientColorsForColor(self.annotation.fillColor ?: self.annotation.color);
    // MODIFIED BY GAVIN MCKENZIE:
    self.backgroundView.colors = PSPDFGradientColorsForColor([UIColor colorWithRed:238/255.0 green:238/255.0 blue:0/255.0 alpha:0.5]);
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UITextViewDelegate

- (void)textViewDidChange:(UITextView *)textView {
    // copy and edit
    PSPDFAnnotation *originalAnnotation = self.annotation;
    PSPDFAnnotation *newAnnotation = [self.annotation copyAndDeleteOriginalIfNeeded];
    if (originalAnnotation == self.annotation) {
        originalAnnotation = [self.annotation copy]; // ensure original annotation doesn't change
    }
    newAnnotation.contents = self.textView.text;

    [[NSNotificationCenter defaultCenter] postNotificationName:PSPDFAnnotationChangedNotification object:newAnnotation userInfo:@{PSPDFAnnotationChangedNotificationKeyPathKey : @[NSStringFromSelector(@selector(contents))], PSPDFAnnotationChangedNotificationOriginalAnnotationKey : originalAnnotation}];

    self.annotation = newAnnotation;

    if ([self.delegate respondsToSelector:@selector(noteAnnotationController:didChangeAnnotation:originalAnnotation:)]) {
        [self.delegate noteAnnotationController:self didChangeAnnotation:self.annotation originalAnnotation:originalAnnotation];
    }
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - PSPDFPopoverControllerDismissable

- (void)willDismissPopover:(UIPopoverController *)popover animated:(BOOL)animated {
    [self.textView resignFirstResponder];
}

@end
