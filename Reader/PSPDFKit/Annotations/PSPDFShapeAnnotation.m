//
//  PSPDFShapeAnnotation.m
//  PSPDFKit
//
//  Copyright 2012 Peter Steinberger. All rights reserved.
//

#import "PSPDFShapeAnnotation.h"
#import "PSPDFConverter.h"

@implementation PSPDFShapeAnnotation

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Static

+ (NSArray *)supportedTypes {
    return @[@"Square", @"Circle"];
}

+ (BOOL)isWriteable {
    return YES;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (id)init {
    return [self initWithShapeType:PSPDFShapeAnnotationSquare];
}

- (id)initWithShapeType:(PSPDFShapeAnnotationType)shapeType {
	if((self = [super initWithType:PSPDFAnnotationTypeShape])) {
        [self setShapeTypeInternal:shapeType]; // also sets typeString
    }
	return self;
}

- (id)initWithAnnotationDictionary:(CGPDFDictionaryRef)annotDict inAnnotsArray:(CGPDFArrayRef)annotsArray  {
    if ((self = [super initWithAnnotationDictionary:annotDict inAnnotsArray:annotsArray type:PSPDFAnnotationTypeShape])) {
        self.shapeType = [self shapeTypeFromString:self.typeString];

        // shapes can have a border without "S" being set.
        NSDictionary *borderDict = PSPDFDictionaryGetObjectForPath(annotDict, @"BS");
        if ([borderDict count] && self.borderStyle == PSPDFAnnotationBorderStyleNone) {
            self.borderStyle = PSPDFAnnotationBorderStyleSolid;
        }
        self.dirty = NO;
    }
    return self;
}

- (NSString *)description {
	return [NSString stringWithFormat:@"%@ - shapeType:%d>", [super description], self.shapeType];
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - PSPDFAnnotation

- (void)drawInContext:(CGContextRef)context {
    if (self.isDeleted) return;

    CGContextSaveGState(context);

    float lineWidth = self.lineWidth;
    CGContextSetStrokeColorWithColor(context, self.colorWithAlpha.CGColor);

    if (self.borderStyle == PSPDFAnnotationBorderStyleDashed) {
        CGFloat *dashes = malloc(fminf([self.dashArray count], 2) * sizeof(CGFloat));
        if ([self.dashArray count] == 0) {
            dashes[0] = 3; dashes[1] = 3;
        }else {
            [self.dashArray enumerateObjectsUsingBlock:^(NSNumber *number, NSUInteger idx, BOOL *stop) {
                dashes[idx] = [number floatValue];
            }];
        }
        CGContextSetLineDash(context, 0.0, dashes, 2);
        free(dashes);
    }

    UIColor *fillColor = self.fillColorWithAlpha;
    if (fillColor) CGContextSetFillColorWithColor(context, fillColor.CGColor);
    CGContextSetLineWidth(context, lineWidth);
    CGRect selfRect = self.boundingBox;
    
    if (_shapeType == PSPDFShapeAnnotationSquare || _shapeType == PSPDFShapeAnnotationUnknown) {
        if (fillColor) CGContextFillRect(context, selfRect);
        CGContextStrokeRect(context, selfRect);
    }else if (_shapeType == PSPDFShapeAnnotationCircle) {
        if (fillColor) CGContextFillEllipseInRect(context, selfRect);
        CGContextStrokeEllipseInRect(context, selfRect);
    }
    
    CGContextRestoreGState(context);
}

- (NSData *)pdfDataRepresentation {
    NSMutableData *pdfData = [NSMutableData data];
    [pdfData pspdf_appendDataString:[NSString stringWithFormat:@"<</Type /Annot /Subtype /%@ /F 4 %@ %@ %@ ", self.typeString, [self pdfRectString], [self pdfColorWithAlphaString], [self pdfFillColorString]]];
    [self appendEscapedContents:pdfData];
    [pdfData pspdf_appendDataString:@">>"];
	return pdfData;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Properties

- (void)setShapeTypeInternal:(PSPDFShapeAnnotationType)shapeType {
    _shapeType = shapeType;
    if (shapeType == PSPDFShapeAnnotationCircle)      self.typeString = @"Circle";
    else if (shapeType == PSPDFShapeAnnotationSquare) self.typeString = @"Square";
}

- (void)setShapeType:(PSPDFShapeAnnotationType)shapeType {
    if (shapeType != _shapeType) {
        [self setShapeTypeInternal:shapeType];
        self.dirty = YES;
    }
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Private

- (PSPDFShapeAnnotationType)shapeTypeFromString:(NSString *)typeString {
    if ([typeString isEqualToString:@"Square"])      return PSPDFShapeAnnotationSquare;
    else if ([typeString isEqualToString:@"Circle"]) return PSPDFShapeAnnotationCircle;
    else {
        PSPDFLogWarning(@"Unknown shape annotation type: %@", self.typeString);
        return PSPDFShapeAnnotationUnknown;
    }
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSCoding

- (id)initWithCoder:(NSCoder *)decoder {
	if((self = [super initWithCoder:decoder])) {
        self.shapeType = [self shapeTypeFromString:self.typeString];
    }
	return self;
}

- (void)encodeWithCoder:(NSCoder *)coder {
	[super encodeWithCoder:coder];
    // shape type is parsed from the typeString, so don't encode it here.
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSCopying

- (id)copyWithZone:(NSZone *)zone {
    PSPDFShapeAnnotation *shapeAnnotation = (PSPDFShapeAnnotation *)[super copyWithZone:zone];
    shapeAnnotation.shapeType = self.shapeType;
    return shapeAnnotation;
}

@end
