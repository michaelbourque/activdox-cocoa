//
//  PSPDFAnnotationController.m
//  PSPDFKit
//
//  Copyright (c) 2012 Peter Steinberger. All rights reserved.
//

#import "PSPDFViewController.h"
#import "PSPDFViewController+Internal.h"
#import "PSPDFKitGlobal.h"
#import "PSPDFKitGlobal+Internal.h"
#import "PSPDFAnnotationController.h"
#import "PSPDFAnnotationView.h"
#import "PSPDFLinkAnnotationView.h"
#import "PSPDFLinkAnnotation.h"
#import "PSPDFDocument.h"
#import "PSPDFAlertView.h"
#import "PSPDFAnnotationParser.h"
#import "NSURL+PSPDFUnicodeURL.h"

@interface PSPDFAnnotationController () {
    NSMutableDictionary *_annotationViewCache;
}
@end

@implementation PSPDFAnnotationController

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (id)initWithPDFController:(PSPDFViewController *)pdfController {
    if ((self = [super init])) {
        _pdfController = pdfController;
        _annotationViewCache = [NSMutableDictionary new];

        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didReceiveMemoryWarning) name:UIApplicationDidReceiveMemoryWarningNotification object:nil];
    }
    return self;
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidReceiveMemoryWarningNotification object:nil];
}

- (NSString *)description {
    return [NSString stringWithFormat:@"<%@ objects:%@>", NSStringFromClass([self class]), _annotationViewCache];
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public

- (UIView <PSPDFAnnotationView>*)createAnnotationViewForAnnotation:(PSPDFAnnotation *)annotation frame:(CGRect)annotationRect {
    PSPDFAnnotationParser *annotationParser = [self.pdfController.document annotationParserForPage:self.pdfController.page];
    UIView <PSPDFAnnotationView> *annotationView = nil;

    // annotation factory
    Class annotationClass = [annotationParser annotationViewClassForAnnotation:annotation];
    if (annotationClass) {
        annotationClass = [self.pdfController classForClass:annotationClass];
        annotationView = [[annotationClass alloc] initWithFrame:annotationRect];
        annotationView.annotation = annotation;
    }
    return annotationView;
}

- (UIView <PSPDFAnnotationView> *)prepareAnnotationViewForAnnotation:(PSPDFAnnotation *)annotation frame:(CGRect)annotationRect pageView:(PSPDFPageView *)pageView {
    PSPDFAnnotationParser *annotationParser = [self.pdfController.document annotationParserForPage:self.pdfController.page];

    // loop up of we already have the annotation in cache
    UIView <PSPDFAnnotationView> *annotationView = [self dequeueViewFromCacheForAnnotation:annotation class:[annotationParser annotationViewClassForAnnotation:annotation]];

    if (!annotationView) {
        annotationView = [self createAnnotationViewForAnnotation:annotation frame:annotationRect];
    }else {
        annotationView.frame = annotationRect;
        annotationView.annotation = annotation;
    }

    // default clipsToBounds (but this is changeable via delegate call)
    annotationView.clipsToBounds = YES;

    // call delegate with created annotation, let user modify/return a new one
    annotationView = [self.pdfController delegateAnnotationView:annotationView forAnnotation:annotation onPageView:pageView];

    return annotationView;
}

- (void)handleTouchUpForAnnotationIgnoredByDelegate:(PSPDFLinkAnnotationView *)annotationView {
    // add DEPRECATED compatibility (old method was in PSPDFViewController)
    if ([self.pdfController respondsToSelector:@selector(handleTouchUpForAnnotationIgnoredByDelegate:)]) {
        [(PSPDFAnnotationController *)self.pdfController handleTouchUpForAnnotationIgnoredByDelegate:annotationView]; return;
    }

    // handle special document-annotations
    PSPDFLinkAnnotation *annotation = annotationView.linkAnnotation;
    if (annotation.linkType == PSPDFLinkAnnotationDocument) {
        // check if file exists, show alert if not.
        if (![annotation.URL isFileURL] || [[NSFileManager defaultManager] fileExistsAtPath:[annotation.URL path]]) {
            self.pdfController.document = [PSPDFDocument PDFDocumentWithURL:annotation.URL];
            [self.pdfController setPage:annotation.pageLinkTarget animated:NO];
        }else {
            NSString *message = [NSString stringWithFormat:PSPDFLocalize(@"%@ could not be found."), annotation.siteLinkTarget];
            [[[UIAlertView alloc] initWithTitle:PSPDFAppName() message:message delegate:nil cancelButtonTitle:PSPDFLocalize(@"OK") otherButtonTitles:nil] show];
        }
    }
    // handle regular page links
    else if (annotation.pageLinkTarget > 0) {
        [self.pdfController setPage:annotation.pageLinkTarget-1 animated:YES];
    }
    else if ([annotation.siteLinkTarget length]) {
        // handle mail
        if ([[annotation.siteLinkTarget lowercaseString] hasPrefix:@"mailto:"]) {
            if ([MFMailComposeViewController canSendMail]) {
                MFMailComposeViewController *mailVC = [[MFMailComposeViewController alloc] init];
                mailVC.mailComposeDelegate = self.pdfController;
                NSString *email = [annotation.siteLinkTarget stringByReplacingOccurrencesOfString:@"mailto:" withString:@"" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [annotation.siteLinkTarget length])];

                // fix encoding
                email = [email stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];

                // search for subject
                NSRange subjectRange = [email rangeOfString:@"?subject=" options:NSCaseInsensitiveSearch];
                if (subjectRange.length > 0) {
                    NSRange subjectContentRange = NSMakeRange(subjectRange.location + subjectRange.length, [email length] - subjectRange.location - subjectRange.length);
                    NSString *subject = [email substringWithRange:subjectContentRange];
                    if ([subject length]) {
                        [mailVC setSubject:subject];
                    }

                    // remove subject from email
                    email = [email substringWithRange:NSMakeRange(0, subjectRange.location)];
                }

                [mailVC setToRecipients:@[email]];
                mailVC.modalPresentationStyle = UIModalPresentationFormSheet;
                [self.pdfController presentModalViewController:mailVC embeddedInNavigationController:NO withCloseButton:NO animated:YES];
            }else {
                NSString *message = [NSString stringWithFormat:PSPDFLocalize(@"Your %@ is not configured to send emails."), [[UIDevice currentDevice] name]];
                [[[UIAlertView alloc] initWithTitle:PSPDFAppName() message:message delegate:nil cancelButtonTitle:PSPDFLocalize(@"OK") otherButtonTitles:nil] show];
            }
            // handle URL
        }else {
            NSURL *URL = annotation.URL ?: [[NSURL alloc] initWithUnicodeString_pspdf:annotation.siteLinkTarget];
            if ([[UIApplication sharedApplication] canOpenURL:URL]) {
                PSPDFLinkAction linkAction = self.pdfController.linkAction;


                // special case if we want to use an inline browser
                if (annotation.isModal || linkAction == PSPDFLinkActionInlineBrowser) {
                    PSPDFWebViewController *webViewController = [[[self.pdfController classForClass:[PSPDFWebViewController class]] alloc] initWithURL:URL];

                    if (!annotation.controlsEnabled) {
                        webViewController.availableActions = PSPDFWebViewControllerAvailableActionsNone;
                    }

                    CGSize targetSize = CGSizeMake(fmaxf(annotation.size.width, 400.0), fmaxf(annotation.size.height, 400.0));
                    if (PSIsIpad() && (annotation.isPopover || !CGSizeEqualToSize(annotation.size, CGSizeZero))) {
                        CGRect popoverRect = [self.pdfController.view convertRect:annotationView.frame fromView:annotationView.superview];

                        // Note: there's currently (iOS 5.1) a known bug when you put a video into a FormSheet and try to fullscreen it. Best workaround: don't set a size, use full modal size.
                        // http://openradar.appspot.com/radar?id=1721401
                        UIModalPresentationStyle presentationStyle = CGSizeEqualToSize(annotation.size, CGSizeZero) ? UIModalPresentationFullScreen : UIModalPresentationFormSheet;

                        BOOL shouldWrapInNavigationController = annotation.controlsEnabled;
                        [self.pdfController presentViewControllerModalOrPopover:webViewController embeddedInNavigationController:shouldWrapInNavigationController withCloseButton:YES animated:YES sender:nil options:
                         @{ PSPDFPresentOptionRect : BOXED(popoverRect),
                            PSPDFPresentOptionPopoverContentSize : BOXED(targetSize),
                 PSPDFPresentOptionAllowedPopoverArrowDirections : @(UIPopoverArrowDirectionAny),
                        PSPDFPresentOptionModalPresentationStyle : @(presentationStyle)
                         }];

                        // if we go to small, we hide the Done button
                        if (presentationStyle != UIModalPresentationFullScreen) {
                            webViewController.view.bounds = (CGRect){.size=targetSize};
                        }
                    }else {
                        [self.pdfController presentModalViewController:webViewController embeddedInNavigationController:YES withCloseButton:YES animated:YES];
                    }
                }else {
                    // web browser
                    if (linkAction == PSPDFLinkActionAlertView) {
                        PSPDFAlertView *alert = [[PSPDFAlertView alloc] initWithTitle:annotation.siteLinkTarget];
                        [alert setCancelButtonWithTitle:PSPDFLocalize(@"Cancel") block:NULL];
                        [alert addButtonWithTitle:PSPDFLocalize(@"Open") block:^{
                            [[UIApplication sharedApplication] openURL:URL];
                        }];
                        [alert show];
                    }else if (linkAction == PSPDFLinkActionOpenSafari) {
                        [[UIApplication sharedApplication] openURL:URL];
                    }
                }
            }else {
                PSPDFLogWarning(@"Ignoring tap to %@ - No system handler registered.", annotation.siteLinkTarget);
            }
        }
    }
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Cache

- (void)clearCache {
    [_annotationViewCache removeAllObjects];
}

- (void)recycleAnnotationView:(id<PSPDFAnnotationView>)annotationView {
    NSParameterAssert(annotationView);

    PSPDFAnnotation *annotation = [annotationView respondsToSelector:@selector(annotation)] ? annotationView.annotation : nil;
    if (![annotationView respondsToSelector:@selector(annotation)] || !annotation) return;

    NSMutableDictionary *cache = _annotationViewCache[[annotationView class]];
    if (!cache) {
        cache = [NSMutableDictionary new];
        _annotationViewCache[(id<NSCopying>)[annotationView class]] = cache;
    }

    cache[@([annotation hash])] = annotationView;
}

- (UIView <PSPDFAnnotationView>*)dequeueViewFromCacheForAnnotation:(PSPDFAnnotation *)annotation class:(Class)annotationViewClass {
    UIView <PSPDFAnnotationView>* annotationView = nil;
    NSMutableDictionary *cache = _annotationViewCache[annotationViewClass];
    if (cache) {
        id key = @([annotation hash]);
        annotationView = cache[key];
        if (!annotationView) {
            key = [[cache keyEnumerator] nextObject];
            if (key) {
                annotationView = cache[key];
            }
        }

        if (annotationView) {
            [cache removeObjectForKey:key];
        }
    }
    return annotationView;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Private

- (void)didReceiveMemoryWarning {
    [self clearCache];
}

@end
