//
//  PSPDFStampAnnotation.m
//  PSPDFKit
//
//  Copyright (c) 2012 Peter Steinberger. All rights reserved.
//

#import "PSPDFStampAnnotation.h"
#import "PSPDFStream.h"
#import "PSPDFConverter.h"
#import <CoreText/CoreText.h>

@implementation PSPDFStampAnnotation

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Static

+ (NSArray *)supportedTypes {
    return @[@"Stamp"];
}

static UIImage *PSPDFImageFromStream(PSPDFStream *stream) {
    __block UIImage *image = nil;

    NSDictionary *xObjects = [stream.dictionary valueForKeyPath:@"Resources.XObject"];
    if ([xObjects isKindOfClass:[NSDictionary class]]) {
        [xObjects enumerateKeysAndObjectsUsingBlock:^(NSString *objectName, PSPDFStream *innerStream, BOOL *stop) {
            if ([innerStream isKindOfClass:[PSPDFStream class]]) {
                image = [innerStream image];
                if (image) {
                    *stop = YES;
                }else {
                    image = PSPDFImageFromStream(innerStream);
                    if (image) *stop = YES;
                }
            }
        }];
    }
    return image;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (id)init {
    return self = [super initWithType:PSPDFAnnotationTypeStamp];
}

- (id)initWithAnnotationDictionary:(CGPDFDictionaryRef)annotDict inAnnotsArray:(CGPDFArrayRef)annotsArray  {
    if ((self = [super initWithAnnotationDictionary:annotDict inAnnotsArray:annotsArray type:PSPDFAnnotationTypeStamp])) {
        //NSLog(@"dict: %@", PSPDFConvertPDFDictionary(annotDict));
        // Try to extract the first image from the appearance stream.
        PSPDFStream *stream = PSPDFDictionaryGetStreamForPath(annotDict, @"AP.N");
        self.image = PSPDFImageFromStream(stream);
        self.subject = PSPDFDictionaryGetString(annotDict, @"Subj");
        // NSLog(@"dict: %@", PSPDFConvertPDFDictionary(annotDict));
    }
    return self;
}

- (NSString *)description {
	return [NSString stringWithFormat:@"%@ - image:%@ subject:%@>", [super description], self.image, self.subject];
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - PSPDFAnnotation

- (void)drawInContext:(CGContextRef)context {
    [super drawInContext:context];
    if (self.isDeleted) return;

    CGContextSaveGState(context);

    if (self.image) {
        CGContextDrawImage(context, self.boundingBox, self.image.CGImage);
    }else {
        CGContextSetStrokeColorWithColor(context, self.colorWithAlpha.CGColor);
        CGContextSetLineWidth(context, 1);
        [[UIBezierPath bezierPathWithRoundedRect:self.boundingBox cornerRadius:5.f] stroke];

        CGRect boundingBox = self.boundingBox;
        NSString *subject = self.subject;
        if ([subject length]) {
            [self.colorWithAlpha set];

            //  creating and formatting CFMutableAttributedString
            CFMutableAttributedStringRef attrStr = CFAttributedStringCreateMutable(kCFAllocatorDefault, 0);
            CFAttributedStringReplaceString(attrStr, CFRangeMake(0, 0), (__bridge CFStringRef)subject);
            CTFontRef font = CTFontCreateWithName(CFSTR("Helvetica-Bold"), 3, NULL);
            CFAttributedStringSetAttribute(attrStr, CFRangeMake(0, CFAttributedStringGetLength(attrStr)), kCTFontAttributeName, font);
            CFRelease(font);
            CTFramesetterRef framesetter = CTFramesetterCreateWithAttributedString(attrStr);
            CFRelease(attrStr);

            // calculate padded rect
            CGFloat padding = 3;
            CGSize boundingBoxSize = CGSizeMake(boundingBox.size.width-padding*2, boundingBox.size.height-padding*2);
            CGPoint boundingBoxPadding = CGPointMake(boundingBox.origin.x + padding, boundingBox.origin.y + padding);

            // get suggested frame size for font size 5.
            CFRange range;
            CGSize coreTextSize = CTFramesetterSuggestFrameSizeWithConstraints(framesetter, CFRangeMake(0, [subject length]), nil, boundingBoxSize, &range);

            // create the frame to draw
            CGMutablePathRef textPath = CGPathCreateMutable();
            CGRect textRect = CGRectMake(boundingBoxPadding.x, boundingBoxPadding.y, coreTextSize.width, coreTextSize.height);
            CGPathAddRect(textPath, NULL, textRect);
            CTFrameRef textFrame = CTFramesetterCreateFrame(framesetter, CFRangeMake(0, 0), textPath, NULL);

            // scale and translate the view so we fit our text right in
            float sx = boundingBoxSize.width / coreTextSize.width;
            float sy = boundingBoxSize.height / coreTextSize.height;
            //float factor = fminf(sx, sy);
            CGContextTranslateCTM(context, boundingBoxPadding.x-boundingBoxPadding.x*sx, boundingBoxPadding.y-boundingBoxPadding.y*sy);
            CGContextScaleCTM(context, sx, sy);

            CTFrameDraw(textFrame, context);
            if (textFrame) CFRelease(textFrame);
            if (textPath) CFRelease(textPath);
        }
    }
    CGContextRestoreGState(context);
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSCoding

- (id)initWithCoder:(NSCoder *)decoder {
	if((self = [super initWithCoder:decoder])) {
        self.subject = [decoder decodeObjectForKey:NSStringFromSelector(@selector(subject))];
        self.image = [decoder decodeObjectForKey:NSStringFromSelector(@selector(image))];
    }
	return self;
}

- (void)encodeWithCoder:(NSCoder *)coder {
	[super encodeWithCoder:coder];
    [coder encodeObject:self.subject forKey:NSStringFromSelector(@selector(subject))];
    [coder encodeObject:self.image forKey:NSStringFromSelector(@selector(image))];
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSCopying

- (id)copyWithZone:(NSZone *)zone {
    PSPDFStampAnnotation *stampAnnotation = (PSPDFStampAnnotation *)[super copyWithZone:zone];
    stampAnnotation.subject = self.subject;
    return stampAnnotation;
}

@end
