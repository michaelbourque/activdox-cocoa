//
//  PSPDFFreeTextAnnotation.m
//  PSPDFKit
//
//  Copyright 2012 Peter Steinberger. All rights reserved.
//

#import "PSPDFFreeTextAnnotation.h"
#import "PSPDFConverter.h"
#import "UIColor+PSPDFKitAdditions.h"
#import "PSPDFKitGlobal+Internal.h"
#import <CoreText/CoreText.h>

@implementation PSPDFFreeTextAnnotation

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Static

+ (NSArray *)supportedTypes {
    return @[PSPDFAnnotationTypeStringFreeText];
}

+ (BOOL)isWriteable {
    return YES;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (id)init {
	if((self = [super initWithType:PSPDFAnnotationTypeText])) {
        self.color = [UIColor blackColor];
    }
	return self;
}

- (id)initWithAnnotationDictionary:(CGPDFDictionaryRef)annotDict inAnnotsArray:(CGPDFArrayRef)annotsArray  {
    if ((self = [super initWithAnnotationDictionary:annotDict inAnnotsArray:annotsArray type:PSPDFAnnotationTypeText])) {
        if (annotDict) {
            //NSLog(@"dict: %@", PSPDFConvertPDFDictionary(annotDict));
            
            //NSLog(@"dict: %@", PSPDFConvertPDFDictionary(annotDict));
            // The default appearance string (DA) contains any graphics state or text state operators needed to establish the graphics state parameters, such as text size and colour, for displaying the field’s variable text.
            // e.g. "/Helvetica 36 Tf 0.969 0.290 0.243 rg";
            NSString *appearanceString = PSPDFDictionaryGetString(annotDict, @"DA");

            NSScanner *scanner = [NSScanner scannerWithString:appearanceString];
            NSString *colorInfo = nil, *tfOperator = nil;
            [scanner scanUpToString:@"/" intoString:&colorInfo];
            [scanner scanUpToString:@"Tf" intoString:&tfOperator];

            // color stuff (e.g. 1 1 1 rg)
            // TODO: ignore for now?

            // e.g. /Helvetica 36 (or /Helv 12)
            if ([tfOperator length] && [tfOperator hasPrefix:@"/"]) {
                NSArray *components = [tfOperator componentsSeparatedByString:@" "];
                NSString *fontName = [[components ps_firstObject] substringFromIndex:1];
                if ([fontName isEqualToString:@"Helv"]) fontName = @"Helvetica";
                self.fontName = fontName;
                self.fontSize = [components count] >= 2 ? [components[1] floatValue] : 0;
            }

            // ignore other settings, just set color. Color in DS overrides other color parameters.
            NSString *defaultStyleString = PSPDFDictionaryGetString(annotDict, @"DS");
            NSString *colorHexString = PSPDFFirstMatchInRegexString(@"color:.*#([\\dA-F]{6})", defaultStyleString);
            if ([colorHexString length]) {
                UIColor *color = [UIColor pspdf_colorWithHexString:colorHexString];
                if (color) self.color = color;
            }
        }
    }
	return self;
}

- (NSUInteger)hash {
    return [super hash] * 31 + [self.fontName hash] * 31 + self.fontSize;
}

- (BOOL)isEqualToAnnotation:(PSPDFAnnotation *)otherAnnotation {
    PSPDFFreeTextAnnotation *otherFreeTextAnnotations = (PSPDFFreeTextAnnotation *)otherAnnotation;
    if ([super isEqualToAnnotation:otherAnnotation] && (self.fontName == otherFreeTextAnnotations.fontName || [self.fontName isEqualToString:otherFreeTextAnnotations.fontName]) && self.fontSize == otherFreeTextAnnotations.fontSize) {
        return YES;
    }
    return NO;
}

- (NSString *)description {
    return [NSString stringWithFormat:@"%@ fontName:%@ fontSize:%f>", [super description], self.fontName, self.fontSize];
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Private

- (NSString *)defaultFontName {
    return [self.fontName length] ? self.fontName : @"Helvetica";;
}

- (CGFloat)defaultFontSize {
    return self.fontSize ? self.fontSize : 12;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - PSPDFAnnotation

- (void)drawInContext:(CGContextRef)context {
    [super drawInContext:context];
    if (self.isDeleted) return;

    NSInteger rotation = self.rotation;
    UIColor *color = self.colorWithAlpha ?: [UIColor blackColor];

    CGContextSaveGState(context);
    
    CGRect boundingBox = self.boundingBox;

    UIColor *fillColor = self.fillColor;
    if (fillColor) {
        CGContextSetFillColorWithColor(context, fillColor.CGColor);
        CGContextFillRect(context, boundingBox);
    }
    CGContextSetTextMatrix(context, CGAffineTransformIdentity);

    // Initialize a rectangular path.
    CGMutablePathRef path = CGPathCreateMutable();
    CGPathAddRect(path, NULL, boundingBox);
    
    // Initialize an attributed string.
    NSString *contents = self.contents;
    if ([contents length]) {
        CFStringRef string = (__bridge CFStringRef)contents;
        CFMutableAttributedStringRef attrString =
        CFAttributedStringCreateMutable(kCFAllocatorDefault, 0);
        CFAttributedStringReplaceString(attrString, CFRangeMake(0, 0), string);

        NSString *fontName = [self defaultFontName];
        CGFloat defaultFontSize = [self defaultFontSize];
        NSDictionary* descriptorAttr = @{(const NSString *)kCTFontFamilyNameAttribute: fontName};
        CTFontDescriptorRef descriptor = CTFontDescriptorCreateWithAttributes((__bridge CFDictionaryRef)descriptorAttr);
        CTFontRef font = CTFontCreateWithFontDescriptor(descriptor, defaultFontSize, nil);
        if (descriptor) CFRelease(descriptor);
        CFAttributedStringSetAttribute(attrString, CFRangeMake(0, [contents length]), kCTFontAttributeName, font);
        if (font) CFRelease(font);

        // Create a color and add it as an attribute to the string.
        CFAttributedStringSetAttribute(attrString, CFRangeMake(0, [contents length]), kCTForegroundColorAttributeName, color.CGColor);

        // Create the framesetter with the attributed string.
        CTFramesetterRef framesetter = CTFramesetterCreateWithAttributedString(attrString);
        CFRelease(attrString);
        if (rotation == 90 || rotation == 270) {
            //CGRect bounds = CGContextGetClipBoundingBox(context);
            //CGContextTranslateCTM(context, bounds.size.width/2, bounds.size.height/2); // 6-4
            //fCGContextScaleCTM(context, 1.0, -1.0); // 7-4
            //CGContextRotateCTM(context, M_PI_2); // 8-4
        }

        // Create the frame and draw it into the graphics context
        CTFrameRef frame = CTFramesetterCreateFrame(framesetter, CFRangeMake(0, 0), path, NULL);
        CFRelease(framesetter);
        CTFrameDraw(frame, context);
        CFRelease(frame);
    }

    CGPathRelease(path);
    CGContextRestoreGState(context);
}

- (NSData *)pdfDataRepresentation {
    // TODO: [self pdfColorWithAlphaString] doesn't work yet here (fills whole block?)
    NSMutableData *pdfData = [NSMutableData data];
    [pdfData pspdf_appendDataString:[NSString stringWithFormat:@"<</Type /Annot /Subtype /%@ /F 4 %@ %@", self.typeString, [self pdfRectString], [self pdfAppearanceString]]];
	[self appendEscapedContents:pdfData];
    [pdfData pspdf_appendDataString:@">>"];
	return pdfData;
}

- (NSString *)pdfAppearanceString {
    NSString *appearance = [NSString stringWithFormat:@"/DA (/%@ %f Tf)", [self defaultFontName] , [self defaultFontSize]];
    return appearance;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSCoding

- (id)initWithCoder:(NSCoder *)decoder {
	if((self = [super initWithCoder:decoder])) {
        _fontName = [decoder decodeObjectForKey:NSStringFromSelector(@selector(fontName))];
        _fontSize  = [decoder decodeFloatForKey:NSStringFromSelector(@selector(fontSize))];
    }
	return self;
}

- (void)encodeWithCoder:(NSCoder *)coder {
	[super encodeWithCoder:coder];
    if ([_fontName length]) [coder encodeObject:_fontName forKey:NSStringFromSelector(@selector(fontName))];
    if (_fontSize > 0) [coder encodeFloat:_fontSize forKey:NSStringFromSelector(@selector(fontSize))];
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSCopying

- (id)copyWithZone:(NSZone *)zone {
    PSPDFFreeTextAnnotation *freeTextAnnotation = (PSPDFFreeTextAnnotation *)[super copyWithZone:zone];
    freeTextAnnotation.fontName = self.fontName;
    freeTextAnnotation.fontSize = self.fontSize;
    return freeTextAnnotation;
}

@end
