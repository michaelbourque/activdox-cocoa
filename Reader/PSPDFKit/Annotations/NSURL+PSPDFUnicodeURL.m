//
//  NSURL+PSPDFUnicodeURL.m
//  PSPDFKit
//
//  Created by Sean Heber on 4/22/10,
//  Copyright (c) 2012 Peter Steinberger. All rights reserved.
//

#import "NSURL+PSPDFUnicodeURL.h"
#import "IDNSDK/xcode.h"

@interface NSString (PSPDFUnicodeURLHelpers)
- (NSArray *)PSPDFUnicodeURL_splitAfterString:(NSString *)string;
- (NSArray *)PSPDFUnicodeURL_splitBeforeCharactersInSet:(NSCharacterSet *)chars;
@end

@implementation NSString (PSPDFUnicodeURLHelpers)

- (NSArray *)PSPDFUnicodeURL_splitAfterString:(NSString *)string {
	NSString *firstPart;
	NSString *secondPart;
	NSRange range = [self rangeOfString:string];
	
	if (range.location != NSNotFound) {
		NSUInteger index = range.location+range.length;
		firstPart = [self substringToIndex:index];
		secondPart = [self substringFromIndex:index];
	} else {
		firstPart = @"";
		secondPart = self;
	}
	
	return @[firstPart, secondPart];
}

- (NSArray *)PSPDFUnicodeURL_splitBeforeCharactersInSet:(NSCharacterSet *)chars {
	NSUInteger index=0;
	for (; index<[self length]; index++) {
		if ([chars characterIsMember:[self characterAtIndex:index]]) {
			break;
		}
	}
	
	return @[[self substringToIndex:index], [self substringFromIndex:index]];
}
@end

static NSString *ConvertUnicodeDomainString(NSString *hostname, BOOL toAscii) {
	const UTF16CHAR *inputString = (const UTF16CHAR *)[hostname cStringUsingEncoding:NSUTF16StringEncoding];
	int inputLength = [hostname lengthOfBytesUsingEncoding:NSUTF16StringEncoding] / sizeof(UTF16CHAR);
	
	if (toAscii) {
		int outputLength = MAX_DOMAIN_SIZE_8;
		UCHAR8 outputString[outputLength];
		
		if (XCODE_SUCCESS == Xcode_DomainToASCII(inputString, inputLength, outputString, &outputLength)) {
			hostname = [[NSString alloc] initWithBytes:outputString length:outputLength encoding:NSASCIIStringEncoding];
		}
	} else {
		int outputLength = MAX_DOMAIN_SIZE_16;
		UTF16CHAR outputString[outputLength];
		if (XCODE_SUCCESS == Xcode_DomainToUnicode16(inputString, inputLength, outputString, &outputLength)) {
			hostname = [[NSString alloc] initWithCharacters:outputString length:outputLength];
		}
	}
	
	return hostname;
}

static NSString *ConvertUnicodeURLString(NSString *str, BOOL toAscii) {
	NSMutableArray *urlParts = [NSMutableArray new];
	NSString *hostname = nil;
	NSArray *parts = nil;
	
	parts = [str PSPDFUnicodeURL_splitAfterString:@":"];
	hostname = parts[1];
	[urlParts addObject:parts[0]];
	
	parts = [hostname PSPDFUnicodeURL_splitAfterString:@"//"];
	hostname = parts[1];
	[urlParts addObject:parts[0]];
	
	parts = [hostname PSPDFUnicodeURL_splitBeforeCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"/?#"]];
	hostname = parts[0];
	[urlParts addObject:parts[1]];
	
	parts = [hostname PSPDFUnicodeURL_splitAfterString:@"@"];
	hostname = parts[1];
	[urlParts addObject:parts[0]];
	
	parts = [hostname PSPDFUnicodeURL_splitBeforeCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@":"]];
	hostname = parts[0];
	[urlParts addObject:parts[1]];
	
	// Now that we have isolated just the hostname, do the magic decoding...
	hostname = ConvertUnicodeDomainString(hostname, toAscii);
	
	// This will try to clean up the stuff after the hostname in the URL by making sure it's all encoded properly.
	// NSURL doesn't normally do anything like this, but I found it useful for my purposes to put it in here.
	NSString *afterHostname = [urlParts[4] stringByAppendingString:urlParts[2]];
	CFStringRef cleanedAfterHostname = CFURLCreateStringByReplacingPercentEscapesUsingEncoding(kCFAllocatorDefault, (__bridge CFStringRef)afterHostname, CFSTR(""), kCFStringEncodingUTF8);
	NSString *processedAfterHostname = (__bridge NSString *)cleanedAfterHostname ?: afterHostname;
	CFStringRef finalAfterHostname = CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault, (__bridge CFStringRef)processedAfterHostname, CFSTR("#[]"), NULL, kCFStringEncodingUTF8);
	
	// Now recreate the URL safely with the new hostname (if it was successful) instead...
	NSArray *reconstructedArray = @[urlParts[0], urlParts[1], urlParts[3], hostname, (__bridge NSString *)finalAfterHostname];
	NSString *reconstructedURLString = [reconstructedArray componentsJoinedByString:@""];

	if (cleanedAfterHostname) CFRelease(cleanedAfterHostname);
	CFRelease(finalAfterHostname);

	return reconstructedURLString;
}

@implementation NSURL (PSPDFUnicodeURL)

- (id)initWithUnicodeString_pspdf:(NSString *)string {
	return [self initWithString:ConvertUnicodeURLString(string, YES)];
}

- (NSString *)pspdf_unicodeAbsoluteString {
	return ConvertUnicodeURLString([self absoluteString], NO);
}

- (NSString *)pspdf_unicodeHost {
	return ConvertUnicodeDomainString([self host], NO);
}

@end
