//
//  PSPDFImageAnnotationView.m
//  PSPDFKit
//
//  Copyright (c) 2012 Peter Steinberger. All rights reserved.
//  Thanks to Niklas Saers / Trifork A/S for the contribution.
//

#import "PSPDFKitGlobal.h"
#import "PSPDFImageAnnotationView.h"
#import "PSPDFAnnotation.h"
#import "PSPDFLinkAnnotation.h"
#import "UIImage+PSPDFKitAdditions.h"

@implementation PSPDFImageAnnotationView

@synthesize imageView = _imageView;

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public

- (void)setURL:(NSURL *)URL {
    if (URL != _URL) {
        _URL = URL;
        [self updateImage];
    }
}

- (void)setAnnotation:(PSPDFAnnotation *)annotation {
    [super setAnnotation:annotation];
    self.URL = self.linkAnnotation.URL;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Private

- (void)updateImage {
    // try loading the image
    if (_URL) {
        BOOL isGIF = [[self.URL path] hasSuffix:@".gif"];

        UIImage *image;
        if (isGIF) {
            image = [UIImage pspdf_animatedGIFWithPath:[self.URL path]];
        }else {
            image = [[UIImage alloc] initWithContentsOfResolutionIndependentFile_pspdf:[self.URL path]];
        }
        _imageView.image = image;
    }
}

- (UIImageView *)imageView {
    if (!_imageView) {
        _imageView = [[UIImageView alloc] initWithFrame:self.frame];
        _imageView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        _imageView.frame = self.bounds;

        // allow custom content mode
        if (self.linkAnnotation.options[kPSPDFImageContentMode]) {
            _imageView.contentMode = [self.linkAnnotation.options[kPSPDFImageContentMode] integerValue];
        }else {
            _imageView.contentMode = UIViewContentModeScaleAspectFit;
        }
        [self updateImage];
        [self addSubview:_imageView];
     }
    return _imageView;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - PSPDFAnnotationView

/// page is displayed
- (void)didShowPage:(NSUInteger)page {
    [self imageView]; // access imageView to be sure it's created.
}

@end
