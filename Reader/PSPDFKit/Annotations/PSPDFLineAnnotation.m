//
//  PSPDFLineAnnotation.m
//  PSPDFKit
//
//  Copyright 2012 Peter Steinberger. All rights reserved.
//

#import "PSPDFLineAnnotation.h"

@implementation PSPDFLineAnnotation

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Static

+ (NSArray *)supportedTypes {
    return @[@"Line"];
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (id)init {
    return self = [super initWithType:PSPDFAnnotationTypeLine];
}

- (id)initWithAnnotationDictionary:(CGPDFDictionaryRef)annotDict inAnnotsArray:(CGPDFArrayRef)annotsArray  {
	if((self = [super initWithAnnotationDictionary:annotDict inAnnotsArray:annotsArray type:PSPDFAnnotationTypeLine])) {

        // parse start and end point
        CGPDFArrayRef lineArrayRef = NULL;
        CGPDFDictionaryGetArray(annotDict, "L", &lineArrayRef);
        if (lineArrayRef != NULL && CGPDFArrayGetCount(lineArrayRef) == 4) {
            CGPDFReal x1, y1, x2, y2;
            CGPDFArrayGetNumber(lineArrayRef, 0, &x1);
            CGPDFArrayGetNumber(lineArrayRef, 1, &y1);
            CGPDFArrayGetNumber(lineArrayRef, 2, &x2);
            CGPDFArrayGetNumber(lineArrayRef, 3, &y2);
            _point1 = CGPointMake(x1, y1);
            _point2 = CGPointMake(x2, y2);
        } else {
            _point1 = CGPointZero;
            _point2 = CGPointZero;
        }

        // parse line endings
        CGPDFArrayRef lineEndArrayRef = NULL;
        CGPDFDictionaryGetArray(annotDict, "LE", &lineEndArrayRef);
        if (lineEndArrayRef != NULL && CGPDFArrayGetCount(lineEndArrayRef) == 2) {
            const char *lineEnd1Name;
            const char *lineEnd2Name;
            CGPDFArrayGetName(lineEndArrayRef, 0, &lineEnd1Name);
            CGPDFArrayGetName(lineEndArrayRef, 1, &lineEnd2Name);

            if (strcmp(lineEnd1Name, "ClosedArrow") == 0) _lineEnd1 = PSPDFLineEndTypeClosedArrow;
            else if (strcmp(lineEnd1Name, "OpenArrow") == 0) _lineEnd1 = PSPDFLineEndTypeOpenArrow;
            else if (strcmp(lineEnd1Name, "Square") == 0) _lineEnd1 = PSPDFLineEndTypeSquare;
            else if (strcmp(lineEnd1Name, "Circle") == 0) _lineEnd1 = PSPDFLineEndTypeCircle;
            else if (strcmp(lineEnd1Name, "Diamond") == 0) _lineEnd1 = PSPDFLineEndTypeDiamond;
            else if (strcmp(lineEnd1Name, "Butt") == 0) _lineEnd1 = PSPDFLineEndTypeButt;
            else if (strcmp(lineEnd1Name, "ROpenArrow") == 0) _lineEnd1 = PSPDFLineEndTypeReverseOpenArrow;
            else if (strcmp(lineEnd1Name, "RClosedArrow") == 0) _lineEnd1 = PSPDFLineEndTypeReverseClosedArrow;
            else if (strcmp(lineEnd1Name, "Slash") == 0) _lineEnd1 = PSPDFLineEndTypeSlash;
            else _lineEnd1 = PSPDFLineEndTypeNone;

            if (strcmp(lineEnd2Name, "ClosedArrow") == 0) _lineEnd2 = PSPDFLineEndTypeClosedArrow;
            else if (strcmp(lineEnd2Name, "OpenArrow") == 0) _lineEnd2 = PSPDFLineEndTypeOpenArrow;
            else if (strcmp(lineEnd2Name, "Square") == 0) _lineEnd2 = PSPDFLineEndTypeSquare;
            else if (strcmp(lineEnd2Name, "Circle") == 0) _lineEnd2 = PSPDFLineEndTypeCircle;
            else if (strcmp(lineEnd2Name, "Diamond") == 0) _lineEnd2 = PSPDFLineEndTypeDiamond;
            else if (strcmp(lineEnd2Name, "Butt") == 0) _lineEnd2 = PSPDFLineEndTypeButt;
            else if (strcmp(lineEnd2Name, "ROpenArrow") == 0) _lineEnd2 = PSPDFLineEndTypeReverseOpenArrow;
            else if (strcmp(lineEnd2Name, "RClosedArrow") == 0) _lineEnd2 = PSPDFLineEndTypeReverseClosedArrow;
            else if (strcmp(lineEnd2Name, "Slash") == 0) _lineEnd2 = PSPDFLineEndTypeSlash;
            else _lineEnd2 = PSPDFLineEndTypeNone;

        } else {
            _lineEnd1 = PSPDFLineEndTypeNone;
            _lineEnd2 = PSPDFLineEndTypeNone;
        }
        self.dirty = NO;
	}
	return self;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - PSPDFAnnotation

- (void)drawInContext:(CGContextRef)context {
    if (self.isDeleted) return;

    CGContextSaveGState(context);

    [self prepareBorderStyleInContext:context];
    float lineWidth = self.lineWidth;
    CGContextSetStrokeColorWithColor(context, self.colorWithAlpha.CGColor);
    CGContextSetFillColorWithColor(context, self.colorWithAlpha.CGColor);
    CGContextSetLineWidth(context, lineWidth);

    CGContextSetLineCap(context, kCGLineCapSquare);
    
    float capLength = lineWidth * 5.0;
    CGPoint point1 = self.point1;
    CGPoint point2 = self.point2;
    
    float length = sqrtf(powf(point2.x-point1.x,2) + powf(point2.y-point1.y,2));
    CGPoint unitVector = CGPointMake((point2.x-point1.x)/length, (point2.y-point1.y)/length);
    CGPoint innerPoint1 = CGPointMake(point1.x + unitVector.x * capLength, point1.y + unitVector.y * capLength);
    CGPoint innerPoint2 = CGPointMake(point2.x - unitVector.x * capLength, point2.y - unitVector.y * capLength);
    
    //draw inner part of line (without line caps):
    CGContextMoveToPoint(context, innerPoint1.x, innerPoint1.y);
    CGContextAddLineToPoint(context, innerPoint2.x, innerPoint2.y);
    CGContextStrokePath(context);
    
    float angle = atan2f(point1.y - point2.y, point2.x - point1.x);
    
    // draw line caps:
    for (int i=0; i<2; i++) {
        CGContextSaveGState(context);
        if (i == 0) { //end of line
            CGContextTranslateCTM(context, point2.x, point2.y);
            CGContextRotateCTM(context, -angle);
        } else { //start of line
            CGContextTranslateCTM(context, point1.x, point1.y);
            CGContextRotateCTM(context, M_PI - angle);
        }
        
        PSPDFLineEndType capType = (i == 0) ? self.lineEnd2 : self.lineEnd1;
        if (capType == PSPDFLineEndTypeNone) {
            CGContextMoveToPoint(context, -capLength, 0);
            CGContextAddLineToPoint(context, 0, 0);
            CGContextStrokePath(context);
        } else if (capType == PSPDFLineEndTypeClosedArrow) {
            CGContextBeginPath(context);
            CGContextMoveToPoint(context, -capLength, 0.5*capLength);
            CGContextAddLineToPoint(context, -capLength, 0.5*capLength);
            CGContextAddLineToPoint(context, 0.5*lineWidth, 0);
            CGContextAddLineToPoint(context, -capLength, -0.5*capLength);
            CGContextFillPath(context);
        } else if (capType == PSPDFLineEndTypeOpenArrow) {
            CGContextBeginPath(context);
            CGContextMoveToPoint(context, -capLength, 0.5*capLength);
            CGContextAddLineToPoint(context, 0, 0);
            CGContextAddLineToPoint(context, -capLength, -0.5*capLength);
            CGContextStrokePath(context);
        } else if (capType == PSPDFLineEndTypeSquare) {
            CGContextFillRect(context, CGRectMake(-capLength, -0.5*capLength, capLength, capLength));
        } else if (capType == PSPDFLineEndTypeCircle) {
            CGContextFillEllipseInRect(context, CGRectMake(-capLength, -0.5*capLength, capLength, capLength));
        } else if (capType == PSPDFLineEndTypeDiamond) {
            CGContextRotateCTM(context, 0.25*M_PI);
            CGContextTranslateCTM(context, 0, capLength*0.5);
            CGContextFillRect(context, CGRectMake(-capLength, -0.5*capLength, capLength, capLength));
        } else if (capType == PSPDFLineEndTypeButt) {
            CGContextMoveToPoint(context, -capLength, 0);
            CGContextAddLineToPoint(context, 0, 0);
            CGContextAddLineToPoint(context, 0, 0.5*capLength);
            CGContextAddLineToPoint(context, 0, -0.5*capLength);
            CGContextSetLineCap(context, kCGLineCapButt);
            CGContextSetLineJoin(context, kCGLineJoinBevel);
            CGContextStrokePath(context);
        } else if (capType == PSPDFLineEndTypeSlash) {
            CGContextMoveToPoint(context, -capLength, 0);
            CGContextAddLineToPoint(context, 0, 0);
            CGContextRotateCTM(context, -0.52); //30 deg.
            CGContextMoveToPoint(context, 0, -0.5*capLength);
            CGContextAddLineToPoint(context, 0, 0.5*capLength);
            CGContextStrokePath(context);
        } else if (capType == PSPDFLineEndTypeReverseOpenArrow) {
            CGContextMoveToPoint(context, -capLength, 0);
            CGContextAddLineToPoint(context, 0, 0);
            CGContextAddLineToPoint(context, capLength, 0.5*capLength);
            CGContextMoveToPoint(context, 0, 0);
            CGContextAddLineToPoint(context, capLength, -0.5*capLength);
            CGContextStrokePath(context);
        } else if (capType == PSPDFLineEndTypeReverseClosedArrow) {
            CGContextMoveToPoint(context, -capLength, 0);
            CGContextAddLineToPoint(context, 0, 0);
            CGContextAddLineToPoint(context, capLength, 0.5*capLength);
            CGContextAddLineToPoint(context, capLength, -0.5*capLength);
            CGContextAddLineToPoint(context, 0, 0);
            CGContextStrokePath(context);
        }
        
        CGContextRestoreGState(context);
    }

    CGContextRestoreGState(context);
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Properties

- (void)setPoint1:(CGPoint)point1 {
    if (!CGPointEqualToPoint(point1, _point1)) {
        _point1 = point1;
        self.dirty = YES;
    }
}

- (void)setPoint2:(CGPoint)point2 {
    if (!CGPointEqualToPoint(point2, _point2)) {
        self.dirty = YES;
    }
}

- (void)setLineEnd1:(PSPDFLineEndType)lineEnd1 {
    if (lineEnd1 != _lineEnd1) {
        _lineEnd1 = lineEnd1;
        self.dirty = YES;
    }
}

- (void)setLineEnd2:(PSPDFLineEndType)lineEnd2 {
    if (lineEnd2 != _lineEnd2) {
        _lineEnd2 = lineEnd2;
        self.dirty = YES;
    }
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSCoding

- (id)initWithCoder:(NSCoder *)decoder {
	if((self = [super initWithCoder:decoder])) {
        _point1 = [decoder decodeCGPointForKey:NSStringFromSelector(@selector(point1))];
        _point2 = [decoder decodeCGPointForKey:NSStringFromSelector(@selector(point2))];
        _lineEnd1 = [decoder decodeIntegerForKey:NSStringFromSelector(@selector(lineEnd1))];
        _lineEnd2 = [decoder decodeIntegerForKey:NSStringFromSelector(@selector(lineEnd2))];
    }
	return self;
}

- (void)encodeWithCoder:(NSCoder *)coder {
	[super encodeWithCoder:coder];
    [coder encodeCGPoint:_point1 forKey:NSStringFromSelector(@selector(point1))];
    [coder encodeCGPoint:_point2 forKey:NSStringFromSelector(@selector(point2))];
    [coder encodeInteger:_lineEnd1 forKey:NSStringFromSelector(@selector(lineEnd1))];
    [coder encodeInteger:_lineEnd2 forKey:NSStringFromSelector(@selector(lineEnd2))];
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSCopying

- (id)copyWithZone:(NSZone *)zone {
    PSPDFLineAnnotation *lineAnnotation = (PSPDFLineAnnotation *)[super copyWithZone:zone];
    lineAnnotation.point1 = self.point1;
    lineAnnotation.point2 = self.point2;
    lineAnnotation.lineEnd1 = self.lineEnd1;
    lineAnnotation.lineEnd2 = self.lineEnd2;
    return lineAnnotation;
}

@end
