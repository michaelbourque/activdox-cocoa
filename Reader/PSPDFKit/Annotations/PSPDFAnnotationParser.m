//
//  PSPDFAnnotationParser.m
//  PSPDFKit
//
//  Copyright 2011-2012 Peter Steinberger. All rights reserved.
//

#import "PSPDFViewController+Internal.h"
#import "PSPDFAnnotationParser.h"
#import "PSPDFAnnotation.h"
#import "PSPDFConverter.h"
#import "PSPDFDocumentProvider.h"
#import "PSPDFDocument.h"
#import "PSPDFAnnotationView.h"
#import "PSPDFHighlightAnnotationView.h"
#import "PSPDFNoteAnnotation.h"
#import "PSPDFLinkAnnotation.h"
#import "PSPDFOutlineParser.h"
#import "PSPDFDocument.h"
#import "PSPDFViewController.h"
#import "PSPDFViewControllerDelegate.h"
#import "PSPDFKitGlobal+Internal.h"
#import "PSPDFFileAnnotationProvider.h"
#import "PSPDFYouTubeAnnotationView.h"
#import "PSPDFImageAnnotationView.h"
#import "PSPDFVideoAnnotationView.h"
#import "PSPDFWebAnnotationView.h"
#import "PSPDFLinkAnnotationView.h"
#import "PSPDFNoteAnnotationView.h"
#import "PSPDFHostingAnnotationView.h"

@interface PSPDFAnnotationParser ()
@property (nonatomic, weak) PSPDFDocumentProvider *documentProvider;
@property (nonatomic, copy) NSString *annotationsPath;
@end

NSString *const PSPDFAnnotationChangedNotification = @"PSPDFAnnotationChangedNotification";
NSString *const PSPDFAnnotationChangedNotificationKeyPathKey = @"PSPDFAnnotationChangedNotificationKeyPathKey";
NSString *const PSPDFAnnotationChangedNotificationOriginalAnnotationKey = @"PSPDFAnnotationChangedNotificationOriginalAnnotationKey";

@implementation PSPDFAnnotationParser

@synthesize annotationProviders = _annotationProviders;

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (id)initWithDocumentProvider:(PSPDFDocumentProvider *)documentProvider {
    if ((self = [super init])) {
        _documentProvider = documentProvider;
        _annotationProviders = [NSMutableArray new];
        _fileAnnotationProvider = [[[documentProvider.document classForClass:[PSPDFFileAnnotationProvider class]] alloc] initWithDocumentProvider:documentProvider];
        self.annotationProviders = @[_fileAnnotationProvider]; // use setter to set delegate

        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(annotationsChanged:) name:PSPDFAnnotationChangedNotification object:nil];
    }
    return self;
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];

    // nil out delegates
    [self setProviderDelegate:nil onArray:_annotationProviders];
}

- (NSString *)description {
    return [NSString stringWithFormat:@"<%@: %p %@>", NSStringFromClass([self class]), self, self.annotationProviders];
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public

- (void)setProviderDelegate:(PSPDFAnnotationParser *)delegate onArray:(NSArray *)array {
    [array enumerateObjectsUsingBlock:^(id<PSPDFAnnotationProvider> annotationProvider, NSUInteger idx, BOOL *stop) {
        if ([annotationProvider respondsToSelector:@selector(setProviderDelegate:)]) [annotationProvider setProviderDelegate:delegate];
    }];
}

// getter/setter are atomic, and set/nil out providerDelegate on set.=
- (NSArray *)annotationProviders { return PSPDFAtomicAutoreleasedGet(_annotationProviders); }
- (void)setAnnotationProviders:(NSArray *)annotationProviders {
    [self setProviderDelegate:self onArray:annotationProviders];
    NSArray *oldValues = _annotationProviders;
    PSPDFAtomicCopiedSetToFrom(_annotationProviders, annotationProviders);
    [self setProviderDelegate:nil onArray:oldValues];
}

- (void)annotationsChanged:(NSNotification *)notification {
    PSPDFAnnotation *annotation = notification.object;
    NSArray *keyPaths = notification.userInfo[PSPDFAnnotationChangedNotificationKeyPathKey];
    PSPDFAnnotation *originalAnnotation = notification.userInfo[PSPDFAnnotationChangedNotificationOriginalAnnotationKey];
    [self didChangeAnnotation:annotation originalAnnotation:originalAnnotation keyPaths:keyPaths options:notification.userInfo];
}

- (void)didChangeAnnotation:(PSPDFAnnotation *)annotation originalAnnotation:(PSPDFAnnotation *)originalAnnotation keyPaths:(NSArray *)keyPaths options:(NSDictionary *)options {
    if (![annotation isKindOfClass:[PSPDFAnnotation class]]) return;
    if (annotation.document != self.documentProvider.document) return;
    if (!originalAnnotation) originalAnnotation = annotation; // fall back to same object if nil

    for (id<PSPDFAnnotationProvider> annotationProvider in self.annotationProviders) {
        if ([annotationProvider respondsToSelector:@selector(didChangeAnnotation:originalAnnotation:keyPaths:options:)]) {
            [annotationProvider didChangeAnnotation:annotation originalAnnotation:originalAnnotation keyPaths:keyPaths options:options];
        }
    }
}

/// Default handler for annotation types. This is NOT in PSPDFFileAnnotationProvider sicen this also has to work when just custom annotation providers 
- (Class)defaultAnnotationViewClassForAnnotation:(PSPDFAnnotation *)annotation {
    Class annotationClass = nil;

    // currently only link annotations are handled here.
    if ([annotation isKindOfClass:[PSPDFLinkAnnotation class]]) {
        PSPDFLinkAnnotation *linkAnnotation = (PSPDFLinkAnnotation *)annotation;
        PSPDFLinkAnnotationType linkType = linkAnnotation.linkType;
        if (linkAnnotation.showAsLinkView) linkType = PSPDFLinkAnnotationWebURL;

        switch (linkType) {
            case PSPDFLinkAnnotationAudio:
            case PSPDFLinkAnnotationVideo: {
                annotationClass = [PSPDFVideoAnnotationView class];
            }break;
            case PSPDFLinkAnnotationYouTube: {
                annotationClass = [PSPDFYouTubeAnnotationView class];
            }break;
            case PSPDFLinkAnnotationImage: {
                annotationClass = [PSPDFImageAnnotationView class];
            }break;
            case PSPDFLinkAnnotationBrowser: {
                annotationClass = [PSPDFWebAnnotationView class];
            }break;
            case PSPDFLinkAnnotationPage:
            case PSPDFLinkAnnotationWebURL:
            case PSPDFLinkAnnotationDocument: {
                annotationClass = [PSPDFLinkAnnotationView class];
            }break;
            default: {
                PSPDFLogVerbose(@"Annotation %@ not handled.", annotation);
            }
        }
    }

    if ([annotation isKindOfClass:[PSPDFNoteAnnotation class]]) {
        annotationClass = [PSPDFNoteAnnotationView class];
    }

    if (!annotationClass) {
        annotationClass = [PSPDFHostingAnnotationView class];
    }

    return annotationClass;
}

// returns the corresponding view class for the PSPDFLinkAnnotation additions
- (Class)annotationViewClassForAnnotation:(PSPDFAnnotation *)annotation {
    Class annotationClass = [self defaultAnnotationViewClassForAnnotation:annotation];
    for (id<PSPDFAnnotationProvider> annotationProvider in self.annotationProviders) {
        if ([annotationProvider respondsToSelector:@selector(annotationViewClassForAnnotation:)] && (annotationClass = [annotationProvider annotationViewClassForAnnotation:annotation])) break;
    }

    return [self.documentProvider.document classForClass:annotationClass];
}

- (BOOL)hasLoadedAnnotationsForPage:(NSUInteger)page {
    BOOL hasLoadedAnnotationsForPage = YES;
    for (id<PSPDFAnnotationProvider> annotationProvider in self.annotationProviders) {
        if ([annotationProvider respondsToSelector:@selector(hasLoadedAnnotationsForPage:)] && ![annotationProvider hasLoadedAnnotationsForPage:page]) {
            hasLoadedAnnotationsForPage = NO; break;
        }
    }
    return hasLoadedAnnotationsForPage;
}

- (NSArray *)annotationsForPage:(NSUInteger)page type:(PSPDFAnnotationType)type {
    return [self annotationsForPage:page type:type pageRef:nil];
}

- (NSArray *)annotationsForPage:(NSUInteger)page type:(PSPDFAnnotationType)type pageRef:(CGPDFPageRef)pageRef {
    // get annotations from the providers
    NSMutableArray *annotations = [NSMutableArray array];
    for (id<PSPDFAnnotationProvider> annotationProvider in self.annotationProviders) {
        NSArray *providerAnnots = nil;
        if (pageRef && [annotationProvider respondsToSelector:@selector(annotationsForPage:pageRef:)]) { // fast path
            providerAnnots = [(PSPDFFileAnnotationProvider *)annotationProvider annotationsForPage:page pageRef:pageRef];
        }else {
            providerAnnots = [annotationProvider annotationsForPage:page];
        }
        if ([providerAnnots count] > 0) [annotations addObjectsFromArray:providerAnnots];
    }

    // filter annotations.
    if (type != PSPDFAnnotationTypeAll) {
        [annotations filterUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(id evaluatedObject, NSDictionary *bindings) {
            return ([(PSPDFAnnotation *)evaluatedObject type] & type) > 0;
        }]];
    }

    // ensure the documentProvider is set correctly.
    [annotations makeObjectsPerformSelector:@selector(setDocumentProvider:) withObject:self.documentProvider];

    return annotations;
}

- (NSDictionary *)dirtyAnnotations {
    NSMutableDictionary *dirtyAnnotations = [NSMutableDictionary dictionary];
    for (id<PSPDFAnnotationProvider> annotationProvider in self.annotationProviders) {
        if (![annotationProvider respondsToSelector:@selector(dirtyAnnotations)]) continue;

        // merge annotations
        NSDictionary *providerDirtyAnnots = [annotationProvider dirtyAnnotations];
        [providerDirtyAnnots enumerateKeysAndObjectsUsingBlock:^(NSNumber *pageNumber, NSArray *annotations, BOOL *stop) {
            NSMutableArray *annots = dirtyAnnotations[pageNumber] ?: [NSMutableArray array];
            [annots addObjectsFromArray:annotations];
            dirtyAnnotations[pageNumber] = annots;
        }];
    }
    return dirtyAnnotations;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Adding/Saving/Loading

// ask all annotationProviders to handle adding.
- (BOOL)addAnnotations:(NSArray *)annotations forPage:(NSUInteger)page {
    BOOL success = NO;
    for (id<PSPDFAnnotationProvider> annotationProvider in self.annotationProviders) {
        if ([annotationProvider respondsToSelector:@selector(addAnnotations:forPage:)] &&
            [annotationProvider addAnnotations:annotations forPage:page]) { success = YES; break; }
    }
    return success;
}

- (BOOL)saveAnnotationsWithError:(NSError **)error {
    if (error) *error = nil; // ensure error is nilled out in all cases.

    BOOL success = YES;
    for (id<PSPDFAnnotationProvider> annotationProvider in self.annotationProviders) {
        if (![annotationProvider respondsToSelector:@selector(saveAnnotationsWithError:)]) continue;

        NSError *localError = nil;
        success &= [annotationProvider saveAnnotationsWithError:&localError]; // no as soon as one provider failed.
        if (!success && localError) {
            PSPDFLogWarning(@"Saving returned following error: %@", [localError localizedDescription]);
            if (error) *error = localError;
        }
    }
    return success;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - PSPDFAnnotationProviderChangeNotifier

- (void)updateAnnotations:(NSArray *)annotations originalAnnotations:(NSArray *)originalAnnotations animated:(BOOL)animated {
    if (!originalAnnotations) originalAnnotations = annotations;
    if ([annotations count] != [originalAnnotations count]) {
        PSPDFLogWarning("Mismatch between annotations and originalAnnotation. Need to be the same amount of items!"); return;
    }

    dispatch_block_t actionBlock = ^{
        PSPDFViewController *pdfController = self.documentProvider.document.displayingPdfController;
        if (!pdfController) return; // no work to do then!

        [annotations enumerateObjectsUsingBlock:^(PSPDFAnnotation *annotation, NSUInteger idx, BOOL *stop) {
            PSPDFAnnotation *originalAnnotation = originalAnnotations[idx];
            
            PSPDFPageView *pageView = [pdfController pageViewForPage:annotation.page];
            if (annotation.isOverlay) {
                [pageView removeAnnotation:originalAnnotation animated:animated];
                [pageView addAnnotation:annotation animated:animated];
            }
            else [pageView updateView];
        }];
    };    
    [NSThread isMainThread] ? actionBlock() : dispatch_async(dispatch_get_main_queue(), actionBlock);
}

- (PSPDFDocumentProvider *)parentDocumentProvider { return self.documentProvider; }

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Deprecated

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-declarations"
- (NSString *)protocolString { return self.fileAnnotationProvider.protocolString; }
- (void)setProtocolString:(NSString *)protocolString { self.fileAnnotationProvider.protocolString = protocolString; }
- (NSString *)annotationsPath { return self.fileAnnotationProvider.annotationsPath; }
- (void)setAnnotationsPath:(NSString *)annotationsPath { self.fileAnnotationProvider.annotationsPath = annotationsPath; }

- (void)setAnnotations:(NSArray *)annotations forPage:(NSUInteger)page {
    [self.fileAnnotationProvider setAnnotations:annotations forPage:page];
}
- (void)clearCache { [self.fileAnnotationProvider clearCache]; }
#pragma clang diagnostic pop


@end
