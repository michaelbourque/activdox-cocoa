//
//  PSPDFSignatureViewController.m
//  PSPDFKit
//
//  Copyright (c) 2012 Peter Steinberger. All rights reserved.
//

#import "PSPDFSignatureViewController.h"
#import "PSPDFDrawView.h"

@interface PSPDFSignatureViewController ()
@property (nonatomic, strong) PSPDFDrawView *drawView;
@end

@implementation PSPDFSignatureViewController

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (id)init {
    if ((self = [super initWithNibName:nil bundle:nil])) {
        self.title = PSPDFLocalize(@"Signature");
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancel:)];
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(done:)];
    }
    return self;
}

- (void)dealloc {
    self.drawView.delegate = nil;
}

- (void)viewDidLoad {
    self.view.backgroundColor = [UIColor whiteColor];
    self.drawView = [[PSPDFDrawView alloc] initWithFrame:self.view.bounds];
    self.drawView.delegate = self;
    self.drawView.strokeColor = [UIColor blackColor];
    
    self.drawView.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
    [self.view addSubview:self.drawView];
}

- (void)viewDidUnload {
    self.drawView.delegate = nil;
    self.drawView = nil;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Toolbar Actions

- (void)cancel:(id)sender {
    if ([self.delegate respondsToSelector:@selector(signatureViewControllerDidCancel:)]) {
        [self.delegate signatureViewControllerDidCancel:self];
    }

    [self dismissModalViewControllerAnimated:YES];
}

- (void)done:(id)sender {
    if ([self.delegate respondsToSelector:@selector(signatureViewControllerDidSave:)]) {
        [self.delegate signatureViewControllerDidSave:self];
    }

    [self dismissModalViewControllerAnimated:YES];
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public

- (NSArray *)lines {
    NSMutableArray *lines = [NSMutableArray array];
    for (NSDictionary *lineDict in self.drawView.linesDictionaries) {
        [lines addObject:lineDict[kPSPDFPointsKey]];
    }
    return [lines copy];
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - PSPDFDrawViewDelegate

@end
