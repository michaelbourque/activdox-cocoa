//
//  PSPDFHighlightAnnotationView.m
//  PSPDFKit
//
//  Copyright (c) 2011-2012 Peter Steinberger. All rights reserved.
//

#import "PSPDFAnnotation.h"
#import "PSPDFHighlightAnnotationView.h"
#import <QuartzCore/QuartzCore.h>

@implementation PSPDFHighlightAnnotationView

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Private

- (void)touchDown {
    self.backgroundColor = [self.annotation.color colorWithAlphaComponent:0.8];
}

- (void)touchUp {
    self.backgroundColor = [self.annotation.color colorWithAlphaComponent:0.5];
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UIView

- (id)initWithFrame:(CGRect)frame {
    if ((self = [super initWithFrame:frame])) {
        
        _button = [UIButton buttonWithType:UIButtonTypeCustom];
        _button.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        _button.frame = CGRectMake(0.0, 0.0, frame.size.width, frame.size.height);
        [self addSubview:_button];
        
        [_button addTarget:self action:@selector(touchDown) forControlEvents:UIControlEventTouchDown];
        [_button addTarget:self action:@selector(touchUp) forControlEvents:UIControlEventTouchUpInside | UIControlEventTouchUpOutside | UIControlEventTouchCancel];
    }
    return self;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public

- (void)setAnnotation:(PSPDFAnnotation *)annotation {
    if (self.annotation != annotation) {
        [super setAnnotation:annotation];
        self.backgroundColor = [annotation.color colorWithAlphaComponent:0.5];
    }
}

@end
