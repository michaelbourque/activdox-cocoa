//
//  PSPDFTextAnnotationView.m
//  PSPDFKit
//
//  Copyright (c) 2012 Peter Steinberger. All rights reserved.
//

#import "PSPDFNoteAnnotationView.h"
#import "PSPDFNoteAnnotation.h"
#import "UIImage+PSPDFKitAdditions.h"

@implementation PSPDFNoteAnnotationView

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (id)initWithAnnotation:(PSPDFNoteAnnotation *)noteAnnotation {
	if((self = [self initWithFrame:CGRectMake(0, 0, kPSPDFNoteAnnotationViewFixedSize.width, kPSPDFNoteAnnotationViewFixedSize.height)])) {
        self.annotation = noteAnnotation;
    }
	return self;
}

- (void)setFrame:(CGRect)frame {
    frame.size.width = kPSPDFNoteAnnotationViewFixedSize.width;
    frame.size.height = kPSPDFNoteAnnotationViewFixedSize.height;
    [super setFrame:frame];
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UIView

- (void)drawRect:(CGRect)rect {
    self.clipsToBounds = YES;

	if ([self.annotation isKindOfClass:[PSPDFNoteAnnotation class]]) {
		CGContextRef c = UIGraphicsGetCurrentContext();
		CGContextConcatCTM(c, CGAffineTransformMakeScale(1, -1));
		CGContextTranslateCTM(c, 0, -self.bounds.size.height);
		
		UIImage *iconImage = [UIImage imageNamed:[NSString stringWithFormat:@"PSPDFKit.bundle/%@", [(PSPDFNoteAnnotation *)self.annotation iconName] ?: @"Comment"]];
        if (!iconImage) {
            // fallback to comment image if other image wasn't found.
            iconImage = [UIImage imageNamed:@"PSPDFKit.bundle/Comment"];
        }
		CGContextSaveGState(c);
		CGContextSetShadowWithColor(c, CGSizeMake(0, 1.0), 3.0, [[UIColor colorWithWhite:0.0 alpha:0.85] CGColor]);
		
		CGContextDrawImage(c, self.bounds, [iconImage CGImage]);
        [self.annotation.color set];
		
		CGRect maskRect = CGRectMake(0, 0, self.bounds.size.width, self.bounds.size.height);
		CGContextClipToMask(c, maskRect, [iconImage CGImage]);
		
		CGContextSetBlendMode(c, kCGBlendModeSourceAtop);
		CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
		size_t num_locations = 2;
		CGFloat locations[2] = { 0.0, 1.0 };

        const float *color = CGColorGetComponents(self.annotation.color.CGColor);
		CGFloat components[8] = {
            color[0] + 0.05, color[1] - 0.1, color[2] - 0.1, 1.0,
            color[0] + 0.3, color[1] + 0.3, color[2] + 0.3, 1.0};

		CGGradientRef gradient = CGGradientCreateWithColorComponents(colorSpace, components, locations, num_locations);
		CGContextDrawLinearGradient(c, gradient, CGPointMake(0, 0), CGPointMake(0, CGRectGetMaxY(self.bounds)), 0);
        CGGradientRelease(gradient);
		
		CGContextSetShadowWithColor(c, CGSizeMake(0, -1.0), 1.0, [[UIColor colorWithWhite:1.0 alpha:0.75] CGColor]);

        // TODO 2.0?
		if (iconImage.scale == 1.0) {
			CGContextRef maskContext = CGBitmapContextCreate(NULL, CGImageGetWidth([iconImage CGImage]), CGImageGetHeight([iconImage CGImage]), 8, CGImageGetWidth([iconImage CGImage]) * 4, colorSpace, kCGImageAlphaPremultipliedLast);
			CGContextSetBlendMode(maskContext, kCGBlendModeXOR);
			CGContextDrawImage(maskContext, self.bounds, [iconImage CGImage]);
			CGContextSetRGBFillColor(maskContext, 1.0, 1.0, 1.0, 1.0);
			CGContextFillRect(maskContext, self.bounds);
			CGImageRef invertedMaskImage = CGBitmapContextCreateImage(maskContext);
			CGContextDrawImage(c, self.bounds, invertedMaskImage);
			CGImageRelease(invertedMaskImage);
			CGContextRelease(maskContext);
		}
        CGColorSpaceRelease(colorSpace);
		CGContextRestoreGState(c);
        
        if ([[(PSPDFNoteAnnotation *)self.annotation iconName] isEqualToString:@"Comment"]) {
			CGContextDrawImage(c, self.bounds, [[UIImage imageNamed:@"PSPDFKit.bundle/CommentOverlay"] CGImage]);
		}
	}
}

@end
