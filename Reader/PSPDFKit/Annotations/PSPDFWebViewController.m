//
//  PSPDFWebViewController.m
//  PSPDFKit
//
//  Copyright (c) 2012 Peter Steinberger. All rights reserved.
//

#import "PSPDFKitGlobal.h"
#import "PSPDFKitGlobal+Internal.h"
#import "PSPDFActionSheet.h"
#import "PSPDFAlertView.h"
#import "PSPDFWebViewController.h"
#import "PSPDFTransparentToolbar.h"
#import "PSPDFIconGenerator.h"
#import "NSURL+PSPDFUnicodeURL.h"
#import "PSPDFViewController+Internal.h"
#import <MessageUI/MessageUI.h>

BOOL PSPDFHonorBlackAndTranslucentSettingsOnWebViewController = NO;

@interface PSPDFWebViewController() <MFMailComposeViewControllerDelegate, UIActionSheetDelegate, UIPrintInteractionControllerDelegate> {
    BOOL _userLinkTouchActionOnWebView:1;
    BOOL _toolbarWasHidden:1;
    BOOL _didEnableActivityIndicator:1;
    UIBarButtonItem *_backBarButtonItem;
    UIBarButtonItem *_forwardBarButtonItem;
    UIBarButtonItem *_actionBarButtonItem;
    UIBarButtonItem *_stopBarButtonItem;
    UIBarButtonItem *_refreshBarButtonItem;
}
@property (nonatomic, strong) UIToolbar *toolbar;
@property (nonatomic, strong) NSURL *URL;
@property (nonatomic, retain) PSPDFActionSheet *pageActionSheet;
@property (nonatomic, strong) UIPrintInteractionController *printController;
@end

@implementation PSPDFWebViewController

@synthesize popoverController = popoverController_; // _popoverController is already used!
@synthesize tintColor = _tintColor, barStyle = _barStyle;
@synthesize isBarTranslucent = _isBarTranslucent, isInPopover = _isInPopover;

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Static

+ (UINavigationController *)modalWebViewWithURL:(NSURL *)URL {
    PSPDFWebViewController *webViewController = [[[self class] alloc] initWithURL:URL];
    webViewController.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:webViewController action:@selector(doneButtonClicked:)];
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:webViewController];
    return navController;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (id)initWithURL:(NSURL *)URL {
    if ((self = [self initWithNibName:nil bundle:nil])) {
        _URL = URL;
        self.title = [URL pspdf_unicodeAbsoluteString];
    }
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])) {
        _updateGlobalActivityIndicator = YES;
        _availableActions = PSPDFWebViewControllerAvailableActionsAll;
    }
    return self;
}

- (void)dealloc {
    // "the deallocation problem" - it's not safe to dealloc a controler from a thread different than the main thread
    // http://developer.apple.com/library/ios/#technotes/tn2109/_index.html#//apple_ref/doc/uid/DTS40010274-CH1-SUBSECTION11
    NSAssert([NSThread isMainThread], @"Must run on main thread, see http://developer.apple.com/library/ios/#technotes/tn2109/_index.html#//apple_ref/doc/uid/DTS40010274-CH1-SUBSECTION11");
    [self cleanupWebView];
    [_pageActionSheet destroy];
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UIViewController

- (void)loadView {
    _webView = [[UIWebView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    _webView.delegate = self;
    _webView.scalesPageToFit = YES;
    _webView.allowsInlineMediaPlayback = YES;
    [_webView loadRequest:[NSURLRequest requestWithURL:self.URL]];
    self.view = _webView;
}

- (void)viewDidLoad {
    [super viewDidLoad];

    self.toolbar = [[PSPDFTransparentToolbar alloc] initWithFrame:CGRectZero];

    // CREATE ... ALL THE BUTTONS!
    _stopBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemStop target:self action:@selector(stop:)];
    _refreshBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh target:self action:@selector(reload:)];
    _actionBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAction target:self action:@selector(action:)];
    _forwardBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemPlay target:self action:@selector(goForward:)];

    UIImage *backImage = [[PSPDFIconGenerator sharedGenerator] iconForType:PSPDFIconTypeBackArrow];

    UIImage *smallBackImage = [[PSPDFIconGenerator sharedGenerator] iconForType:PSPDFIconTypeBackArrowSmall];
    _backBarButtonItem = [[UIBarButtonItem alloc] initWithImage:backImage landscapeImagePhone:smallBackImage style:UIBarButtonItemStylePlain target:self action:@selector(goBack:)];

    [self updateScrollViewContentInset];
}

- (BOOL)isModal {
    BOOL isModal = [self.navigationController.viewControllers count] && (self.navigationController.viewControllers)[0] == self;
    return isModal;
}

- (BOOL)navigationIsPopoverStyle {
    // If we're embedded in an UIPopoverController, barStyle is set to the private enum value 3
    return self.navigationController.navigationBar.barStyle > UIBarStyleBlackTranslucent;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self updateToolbarItems];

    // set navbar preferences
    if (![self navigationIsPopoverStyle]) {
        if (PSPDFHonorBlackAndTranslucentSettingsOnWebViewController) {
            self.navigationController.navigationBar.barStyle = self.barStyle;
        }
    }else {
        // ensure toolbar icons are WHITE in the popover.
        self.toolbar.barStyle = UIBarStyleBlack;
    }
    if (PSPDFHonorBlackAndTranslucentSettingsOnWebViewController) {
        self.navigationController.navigationBar.translucent = self.isBarTranslucent;
    }
    self.navigationController.navigationBar.tintColor = self.tintColor;

    if (!PSIsIpad()) {
        // animations
        _toolbarWasHidden = self.navigationController.isToolbarHidden;
        [self.navigationController setToolbarHidden:NO animated:animated && ![self isModal]]; // should not be animated when used modally
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self setActivityIndicatorEnabled:NO];

    if (!PSIsIpad()) {
        [self.navigationController setToolbarHidden:_toolbarWasHidden animated:animated && ![self isModal]];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    if (![self isViewLoaded]) {
        [self cleanupWebView];
        _backBarButtonItem = nil;
        _forwardBarButtonItem = nil;
        _refreshBarButtonItem = nil;
        _stopBarButtonItem = nil;
        _actionBarButtonItem = nil;
        [_pageActionSheet destroy];
        _pageActionSheet = nil;
    }
}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    [self updateToolbarItems];
    [self updateScrollViewContentInset];
}

- (void)updateScrollViewContentInset {
    if (PSPDFHonorBlackAndTranslucentSettingsOnWebViewController && self.isBarTranslucent) {
        BOOL shouldScrollToTop = self.webView.scrollView.contentOffset.y == -self.webView.scrollView.contentInset.top && self.webView.scrollView.contentOffset.x == 0;
        CGFloat toolbarHeight = PSPDFToolbarHeightForOrientation(self.interfaceOrientation);
        self.webView.scrollView.contentInset = UIEdgeInsetsMake(toolbarHeight, 0, toolbarHeight, 0);
        if (shouldScrollToTop) {
            self.webView.scrollView.contentOffset = CGPointMake(0, -toolbarHeight);
        }
    }
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public

- (void)setTintColor:(UIColor *)tintColor {
    if (tintColor != _tintColor) {
        _tintColor = tintColor;
        self.navigationController.navigationBar.tintColor = tintColor;
    }
}

- (void)setActivityIndicatorEnabled:(BOOL)enabled {
    if (self.updateGlobalActivityIndicator) {
        if (enabled && !_didEnableActivityIndicator) {
            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
            _didEnableActivityIndicator = YES;
        }else if (!enabled && _didEnableActivityIndicator) {
            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
            _didEnableActivityIndicator = NO;
        }
    }
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Target selectors

- (void)updateToolbarItems {
    _backBarButtonItem.enabled = _webView.canGoBack;
    _forwardBarButtonItem.enabled = _webView.canGoForward;
    _actionBarButtonItem.enabled = !_webView.isLoading;

    UIBarButtonItem *refreshStopBarButtonItem = _webView.isLoading ? _stopBarButtonItem : _refreshBarButtonItem;
    UIBarButtonItem *fixedSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    fixedSpace.width = 5.0f;
    UIBarButtonItem *flexibleSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];

    UIToolbar *toolbar = nil;
    CGFloat toolbarWidth = 250.f;
    NSMutableArray *items = [NSMutableArray arrayWithObject:fixedSpace];

    if (self.availableActions & PSPDFWebViewControllerAvailableActionsStopReload) {
        [items addObjectsFromArray:@[refreshStopBarButtonItem, flexibleSpace]];
    }
    if (self.availableActions & PSPDFWebViewControllerAvailableActionsBack) {
        [items addObjectsFromArray:@[_backBarButtonItem, flexibleSpace]];
    }
    if (self.availableActions & PSPDFWebViewControllerAvailableActionsForward) {
        [items addObjectsFromArray:@[_forwardBarButtonItem, flexibleSpace]];
    }
    if (self.availableActions & PSPDFWebViewControllerAvailableActionsOpenInSafari || self.availableActions & PSPDFWebViewControllerAvailableActionsMailLink || self.availableActions & PSPDFWebViewControllerAvailableActionsCopyLink || self.availableActions & PSPDFWebViewControllerAvailableActionsPrint) {
        [items addObjectsFromArray:@[_actionBarButtonItem, flexibleSpace]];
    }else {
        toolbarWidth = 200.0f;
    }

    // replace last flexibleSpace with a fixed space
    [items removeLastObject]; [items addObject:fixedSpace];

    if (PSIsIpad()) {
        // make toolbar more compact on small sizes
        CGFloat viewWidth = self.view.bounds.size.width;
        if (viewWidth < 400.f) {
            toolbarWidth -= 70.f;
        }

        self.toolbar.frame = CGRectMake(0.0f, 0.0f, toolbarWidth, PSPDFToolbarHeightForOrientation(self.interfaceOrientation));
        self.toolbar.items = items;
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:self.toolbar];
    }else {
        toolbar = self.navigationController.toolbar;
        self.toolbarItems = items;
    }

    if (PSPDFHonorBlackAndTranslucentSettingsOnWebViewController) toolbar.barStyle = self.barStyle;
    toolbar.tintColor = self.tintColor;
    if (PSPDFHonorBlackAndTranslucentSettingsOnWebViewController) toolbar.translucent = self.isBarTranslucent;
}

- (void)goBack:(UIBarButtonItem *)sender {
    [_webView goBack];
}

- (void)goForward:(UIBarButtonItem *)sender {
    [_webView goForward];
}

- (void)reload:(UIBarButtonItem *)sender {
    [_webView reload];
}

- (void)stop:(UIBarButtonItem *)sender {
    [_webView stopLoading];

    [self setActivityIndicatorEnabled:NO];
	[self updateToolbarItems];
}

- (void)action:(id)sender {
    if (_pageActionSheet) {
        [_pageActionSheet dismissWithClickedButtonIndex:0 animated:YES];
        _pageActionSheet = nil;
        return;
    }

    if (_printController) {
        [_printController dismissAnimated:YES];
        _printController.delegate = nil;
        _printController = nil;
        return;
    }

    // create action sheet
    _pageActionSheet = [[PSPDFActionSheet alloc] initWithTitle:_webView.request.URL.absoluteString];
    if (PSPDFHonorBlackAndTranslucentSettingsOnWebViewController) {
        _pageActionSheet.actionSheetStyle = PSPDPFActionSheetStyleForBarButtonStyle(self.barStyle, self.isBarTranslucent);
    }
    _pageActionSheet.delegate = self;

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-retain-cycles"
    if (self.availableActions & PSPDFWebViewControllerAvailableActionsCopyLink) {
        [_pageActionSheet addButtonWithTitle:PSPDFLocalize(@"Copy Link") block:^{
            UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
            pasteboard.string = _webView.request.URL.absoluteString;
        }];
    }

    if (self.availableActions & PSPDFWebViewControllerAvailableActionsOpenInSafari) {
        [_pageActionSheet addButtonWithTitle:PSPDFLocalize(@"Open in Safari") block:^{
            [[UIApplication sharedApplication] openURL:_webView.request.URL];
        }];
    }

    if (self.availableActions & PSPDFWebViewControllerAvailableActionsMailLink && [MFMailComposeViewController canSendMail]) {
        [_pageActionSheet addButtonWithTitle:PSPDFLocalize(@"Mail Link to this Page") block:^{
            if (![MFMailComposeViewController canSendMail]) {
                PSPDFLogWarning(@"Stopping here, cannot send mail");
                return;
            }
            MFMailComposeViewController *mailViewController = [[MFMailComposeViewController alloc] init];
            mailViewController.mailComposeDelegate = self;
            [mailViewController setSubject:[_webView stringByEvaluatingJavaScriptFromString:@"document.title"]];
            [mailViewController setMessageBody:_webView.request.URL.absoluteString isHTML:NO];
            mailViewController.modalPresentationStyle = UIModalPresentationFormSheet;
            [self presentModalViewController:mailViewController animated:YES];
        }];
    }

    if (self.availableActions & PSPDFWebViewControllerAvailableActionsPrint && [UIPrintInteractionController isPrintingAvailable]) {
        [_pageActionSheet addButtonWithTitle:PSPDFLocalize(@"Print") block:^{
            _printController = [UIPrintInteractionController sharedPrintController];
            _printController.delegate = self;

            UIPrintInfo *printInfo = [UIPrintInfo printInfo];
            printInfo.duplex = UIPrintInfoDuplexLongEdge;
            printInfo.outputType = UIPrintInfoOutputGeneral;
            printInfo.jobName = [self.webView stringByEvaluatingJavaScriptFromString:@"document.title"];
            _printController.printInfo = printInfo;
            _printController.showsPageRange = YES;
            UIViewPrintFormatter *formatter = [self.webView viewPrintFormatter];
            _printController.printFormatter = formatter;

            [_printController presentFromBarButtonItem:_actionBarButtonItem
                                              animated:YES
                                     completionHandler:NULL];
        }];
    }

    // we need the cancel button only on the iPad if we're within a popover
    if (!PSIsIpad() || self.popoverController) {
        [_pageActionSheet setCancelButtonWithTitle:PSPDFLocalize(@"Cancel") block:nil];
    }

#pragma clang diagnostic pop

    if (PSIsIpad()) {
        [_pageActionSheet showFromBarButtonItem:_actionBarButtonItem animated:YES];
    }else {
        [_pageActionSheet showFromToolbar:self.navigationController.toolbar];
    }
}

- (void)doneButtonClicked:(id)sender {
    if (self.popoverController) {
        [self.popoverController dismissPopoverAnimated:YES];
        self.popoverController = nil;
    }else {
        UIViewController *masterViewController = [self.delegate masterViewController] ?: self;
        [masterViewController dismissViewControllerAnimated:YES completion:NULL];
    }
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Private

// clear delegate, stop loading, deallocate
- (void)cleanupWebView {
    _URL = nil;
    _webView.delegate = nil; // delegate is self here, so first set to nil before call stopLoading.
    [_webView stopLoading];
    _webView = nil;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UIWebViewDelegate

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    PSPDFLogVerbose(@"WebView: %@ loading %@", webView, request.URL);

    // track user action.
    // we only close the webview if the user had no action (e.g. direct iTunes URL)
    if (navigationType != UIWebViewNavigationTypeOther) _userLinkTouchActionOnWebView = YES;

    NSString *URLString = [request.URL absoluteString];
    if ([URLString hasPrefix:@"http:"] || [URLString hasPrefix:@"https:"] || [URLString hasPrefix:@"file:"]) {
        return YES;
    }else if (![[UIApplication sharedApplication] canOpenURL:request.URL]) {
        // just try to handle if some weird things happen, e.g. website connects to about:blank...
        return YES;
    }else {
        // catch requsts that would open an external app (e.g. the app store)
        NSString *appName = PSPDFLocalize(@"an external application");
        if ([URLString hasPrefix:@"itms-appss:"]) appName = PSPDFLocalize(@"the App Store");
        if ([URLString hasPrefix:@"mailto:"]) appName = PSPDFLocalize(@"Mail");
        NSString *message = [NSString stringWithFormat:PSPDFLocalize(@"The link is requesting to open %@. Would you like to continue?"), appName];

        PSPDFAlertView *alert = [[PSPDFAlertView alloc] initWithTitle:[NSString stringWithFormat:PSPDFLocalize(@"Leave %@?"), PSPDFAppName()] message:message];
        [alert setCancelButtonWithTitle:PSPDFLocalize(@"Cancel") block:^{
            if (!_userLinkTouchActionOnWebView) [self doneButtonClicked:nil]; // close view
        }];
        [alert addButtonWithTitle:PSPDFLocalize(@"Open") block:^{
            if (!_userLinkTouchActionOnWebView) [self doneButtonClicked:nil]; // close view
            [[UIApplication sharedApplication] openURL:request.URL];
        }];
        [alert show];
        return NO;
    }
}

- (void)webViewDidStartLoad:(UIWebView *)webView {
    [self setActivityIndicatorEnabled:YES];
    [self updateToolbarItems];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    [self setActivityIndicatorEnabled:NO];

    self.navigationItem.title = [webView stringByEvaluatingJavaScriptFromString:@"document.title"];
    [self updateToolbarItems];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    [self setActivityIndicatorEnabled:NO];
    [self updateToolbarItems];
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - MFMailComposeViewControllerDelegate

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error {
    [self dismissViewControllerAnimated:YES completion:NULL];
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UIActionSheetDelegate

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex {
    self.pageActionSheet = nil;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UIPrintInteractionControllerDelegate

- (void)printInteractionControllerDidDismissPrinterOptions:(UIPrintInteractionController *)printInteractionController {
    self.printController = nil;
}

@end
