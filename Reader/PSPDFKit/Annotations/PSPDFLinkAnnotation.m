//
//  LinkAnnotation.m
//  PSPDFKit
//
//  Copyright 2012 Peter Steinberger. All rights reserved.
//

#import "PSPDFLinkAnnotation.h"
#import "PSPDFDocument.h"

@implementation PSPDFLinkAnnotation

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Static

+ (NSArray *)supportedTypes {
    return @[@"Link"];
}

+ (BOOL)isWriteable {
    return YES;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (id)init {
    return self = [super initWithType:PSPDFAnnotationTypeLink];
}

- (id)initWithLinkAnnotationType:(PSPDFLinkAnnotationType)linkAnotationType {
    if ((self = [self init])) {
        _linkType = linkAnotationType;
    }
    return self;
}

- (id)initWithSiteLinkTarget:(NSString *)siteLinkTarget {
    if ((self = [self init])) {
        _siteLinkTarget = siteLinkTarget;
    }
    return self;
}

- (id)initWithAnnotationDictionary:(CGPDFDictionaryRef)annotDict inAnnotsArray:(CGPDFArrayRef)annotsArray {
	if((self = [super initWithAnnotationDictionary:annotDict inAnnotsArray:annotsArray type:PSPDFAnnotationTypeLink])) {
        CGPDFArrayRef quadPointsArray = NULL;
        CGPDFDictionaryGetArray(annotDict, "QuadPoints", &quadPointsArray);
        _rects = [self rectsFromQuadPointsInArray:quadPointsArray];
	}
	return self;
}

// for debugging
static NSString *PSPDFLinkAnnotationTypeToString(PSPDFLinkAnnotationType linkAnnotationType) {
    switch (linkAnnotationType) {
        case PSPDFLinkAnnotationPage: return @"Page";
        case PSPDFLinkAnnotationWebURL : return @"WebURL";
        case PSPDFLinkAnnotationVideo : return @"Video";
        case PSPDFLinkAnnotationYouTube : return @"YouTube";
        case PSPDFLinkAnnotationAudio : return @"Audio";
        case PSPDFLinkAnnotationImage : return @"Image";
        case PSPDFLinkAnnotationBrowser : return @"Browser";
        case PSPDFLinkAnnotationCustom : return @"Custom";
        default: return @"Unknown";
    }
}

- (NSString *)description {
    NSMutableString *description = [NSMutableString stringWithFormat:@"%@ linkType:%d(%@) pageLinkTarget:%u", [super description], self.linkType, PSPDFLinkAnnotationTypeToString(self.linkType), self.pageLinkTarget];

    if (self.siteLinkTarget) [description appendFormat:@" siteLinkTarget:%@", self.siteLinkTarget];
    if (self.URL) [description appendFormat:@" URL:%@", self.URL];

    [description appendString:@">"];
    return [description copy];
}

- (NSUInteger)hash {
    // http://stackoverflow.com/questions/254281/best-practices-for-overriding-isequal-and-hash/254380#254380
    return (((([super hash] + _pageLinkTarget * 31) + _pageLinkTarget * 31) + [_siteLinkTarget hash] * 31) + [_options hash] * 31) + _linkType;
}

- (BOOL)isEqualToAnnotation:(PSPDFAnnotation *)otherAnnotation {
    PSPDFLinkAnnotation *otherLinkAnnotation = (PSPDFLinkAnnotation *)otherAnnotation;
    if (![super isEqualToAnnotation:otherLinkAnnotation]) {
        return NO;
    }

    // siteLinkTarget might be nil, in that case a == helps.
    if (_pageLinkTarget == otherLinkAnnotation.pageLinkTarget && _linkType == otherLinkAnnotation.linkType && (_siteLinkTarget == otherLinkAnnotation.siteLinkTarget || [_siteLinkTarget isEqual:otherLinkAnnotation.siteLinkTarget])) {
        return YES;
    }
    return NO;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - PSPDFAnnotation

// Links are drawn via an UIView overlay, not within the context.
- (void)drawInContext:(CGContextRef)context {
    [super drawInContext:context];
    if (self.isDeleted) return;

    CGContextSaveGState(context);

    CGContextSetFillColorWithColor(context, [[UIColor colorWithRed:0.9 green:0.9 blue:0.9 alpha:1.0] CGColor]);
    CGContextSetBlendMode(context, kCGBlendModeMultiply);
    if ([self.rects count] > 0) {
        for (NSValue *rectValue in self.rects) {
            CGContextAddRect(context, [rectValue CGRectValue]);
        }
    } else {
        CGContextAddRect(context, self.boundingBox);
    }
    CGContextFillPath(context);

    CGContextRestoreGState(context);
}

- (NSData *)pdfDataRepresentation {
    NSMutableData *pdfData = [NSMutableData data];

    // Link annotations either contain a /Dest string to the named destination or a /A (action) dictionary
    [pdfData pspdf_appendDataString:[NSString stringWithFormat:@"<</Type /Annot /Subtype /Link /F 4 %@ \
                          /Border[0 0 0] /A <</Type /Action ", [self pdfRectString]]];
    // /BS <</Type /Border /W 3 /S /U>> ??

    NSInteger numberedPage = self.pageLinkTarget;

    if (self.linkType == PSPDFLinkAnnotationPage) {
        [pdfData pspdf_appendDataString:[NSString stringWithFormat:@"/S /GoTo /D [%d /Fit]", numberedPage]];
    }else if (self.linkType == PSPDFLinkAnnotationDocument) {
        /* here is what we are parsing:
         pspdfkit://localhost/%@#page=%d&anchor=%@"
         */
        NSString *linkTarget = self.siteLinkTarget;
        NSString *anchor = nil;

        NSRange range = [linkTarget rangeOfString:@"anchor="];
        if (range.location != NSNotFound) {
            anchor = [linkTarget substringFromIndex:(range.location+range.length)];
            linkTarget = [linkTarget substringToIndex:range.location];
        }

        range = [linkTarget rangeOfString:@"page="];
        if (range.location != NSNotFound) {
            NSInteger pageNo = [[linkTarget substringFromIndex:(range.location+range.length)] integerValue];
            if (pageNo > 0) {
                numberedPage = pageNo - 1;
            }
        }

        [pdfData pspdf_appendDataString:[NSString stringWithFormat:@"/S /GoToR /F <</F (%@)>> ", linkTarget]];

        if (anchor)[pdfData pspdf_appendDataString:[NSString stringWithFormat:@"/D [%d (%@)]", numberedPage, anchor]];
        else [pdfData pspdf_appendDataString:[NSString stringWithFormat:@"/D [%d /Fit]", numberedPage]];
    }else {
        NSString *siteLinkTarget = self.siteLinkTarget;
        if ([siteLinkTarget length]) {
            NSString *percentEscapedString = CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(NULL, (__bridge CFStringRef)(siteLinkTarget), NULL, NULL, kCFStringEncodingUTF8));
            [pdfData pspdf_appendDataString:[NSString stringWithFormat:@"/S/URI /URI(%@)", percentEscapedString]];
        }
    }

    [pdfData pspdf_appendDataString:@">> "];
    [self appendEscapedContents:pdfData];
    [pdfData pspdf_appendDataString:@">>"];
    return pdfData;
}

// Links are always displayed as overlay.
- (BOOL)isOverlay {
    return YES;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public

- (NSString *)targetString {
    NSString *title;
    if ([self.siteLinkTarget length]) {
        title = [NSString stringWithFormat:PSPDFLocalize(@"Go to %@"), self.siteLinkTarget];
    }else {
        // pageLinkTarget starts at 1.
        title = [NSString stringWithFormat:PSPDFLocalize(@"Page %@"), [self.documentProvider.document pageLabelForPage:self.pageLinkTarget-1 substituteWithPlainLabel:YES]];
    }
    return title;
}

- (BOOL)showAsLinkView {
    return self.isModal || self.isPopover || !self.isMultimediaExtension;
}

- (BOOL)isMultimediaExtension {
    return self.linkType >= PSPDFLinkAnnotationVideo;
}

- (void)setSiteLinkTarget:(NSString *)siteLinkTarget {
    if (siteLinkTarget != _siteLinkTarget) {
        _siteLinkTarget = siteLinkTarget;

#ifdef DEBUG
        if ([siteLinkTarget rangeOfString:@"%20"].length > 0) {
            PSPDFLogError(@"You're adding a siteLinkTarget with a URL-encoded string: %@. Are you using [url description] instead of [url path]?", siteLinkTarget);
        }
#endif

        // pre-set to web url, this may change if a pspdfkit url is detected
        self.linkType = PSPDFLinkAnnotationWebURL;
        self.dirty = YES;
    }
}

// helper for options dict
- (void)changeOptionsDict:(void (^)(NSMutableDictionary *options))optionBlock {
    NSMutableDictionary *newOptions = _options ? [_options mutableCopy] : [NSMutableDictionary dictionaryWithCapacity:1];
    if (optionBlock) optionBlock(newOptions);
    _options = newOptions;
}

- (BOOL)isModal {
    return [_options[@"modal"] boolValue];
}
- (void)setModal:(BOOL)modal {
    [self changeOptionsDict:^(NSMutableDictionary *options) { options[@"modal"] = @(modal); }];
}

- (BOOL)isPopover {
    return [_options[@"popover"] boolValue];
}
- (void)setPopover:(BOOL)popover {
    [self changeOptionsDict:^(NSMutableDictionary *options) { options[@"popover"] = @(popover); }];
}

- (BOOL)controlsEnabled {
    return _options[@"controls"] ? [_options[@"controls"] boolValue] : YES;
}
- (void)setControlsEnabled:(BOOL)controlsEnabled {
    [self changeOptionsDict:^(NSMutableDictionary *options) { options[@"controls"] = @(controlsEnabled); }];
}

// support both "autoplay? and "autostart"
- (BOOL)isAutoplayEnabled {
    if (_options[@"autoplay"]) {
        return [_options[@"autoplay"] boolValue];
    }else {
        if (_options[@"autostart"]) {
            return [_options[@"autostart"] boolValue];
        }
    }
    return NO;
}
- (void)setAutoplayEnabled:(BOOL)autoplayEnabled {
    [self changeOptionsDict:^(NSMutableDictionary *options) { options[@"autoplay"] = @(autoplayEnabled); }];
}

- (CGSize)size {
    CGSize size = CGSizeZero;
    if ([_options[@"size"] isKindOfClass:[NSString class]]) {
        NSArray *parts = [_options[@"size"] componentsSeparatedByString:@"x"];
        if ([parts count] == 2) size = CGSizeMake([parts[0] floatValue], [parts[1] floatValue]);
	}
    return size;
}
- (void)setSize:(CGSize)size {
    NSString *sizeString = [NSString stringWithFormat:@"%fx%f", size.width, size.height];
    [self changeOptionsDict:^(NSMutableDictionary *options) { options[@"size"] = sizeString; }];
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Properties

- (void)setRects:(NSArray *)rects {
    if (rects != _rects) {
        _rects = rects;
        self.dirty = YES;
    }
}

- (void)setPageLinkTarget:(NSUInteger)pageLinkTarget {
    if (pageLinkTarget != _pageLinkTarget) {
        _pageLinkTarget = pageLinkTarget;
        self.dirty = YES;
    }
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSCoding

- (id)initWithCoder:(NSCoder *)decoder {
	if((self = [super initWithCoder:decoder])) {
        NSArray *rectStrings = [decoder decodeObjectForKey:NSStringFromSelector(@selector(rects))];
        _rects = [[self class] rectsFromStringsArray:rectStrings];
        _pageLinkTarget = [decoder decodeIntegerForKey:NSStringFromSelector(@selector(pageLinkTarget))];
        _siteLinkTarget = [decoder decodeObjectForKey:NSStringFromSelector(@selector(siteLinkTarget))];
        _URL = [decoder decodeObjectForKey:NSStringFromSelector(@selector(URL))];
        _options = [decoder decodeObjectForKey:NSStringFromSelector(@selector(options))];
        _linkType = [decoder decodeIntegerForKey:NSStringFromSelector(@selector(linkType))];
    }
	return self;
}

- (void)encodeWithCoder:(NSCoder *)coder {
	[super encodeWithCoder:coder];
	[coder encodeObject:[[self class] stringsFromRectsArray:_rects] forKey:NSStringFromSelector(@selector(rects))];
    [coder encodeInteger:_pageLinkTarget forKey:NSStringFromSelector(@selector(pageLinkTarget))];
    [coder encodeObject:_siteLinkTarget forKey:NSStringFromSelector(@selector(siteLinkTarget))];
    [coder encodeObject:_URL forKey:NSStringFromSelector(@selector(URL))];
    [coder encodeObject:_options forKey:NSStringFromSelector(@selector(options))];
    [coder encodeInteger:_linkType forKey:NSStringFromSelector(@selector(linkType))];
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSCopying

- (id)copyWithZone:(NSZone *)zone {
    PSPDFLinkAnnotation *linkAnnotation = (PSPDFLinkAnnotation *)[super copyWithZone:zone];
    linkAnnotation.rects = self.rects;
    linkAnnotation.pageLinkTarget = self.pageLinkTarget;
    linkAnnotation.siteLinkTarget = self.siteLinkTarget;
    linkAnnotation.URL = self.URL;
    linkAnnotation.options = self.options;
    linkAnnotation.linkType = self.linkType; // set at last because prev. setters might change this
    return linkAnnotation;
}

@end
