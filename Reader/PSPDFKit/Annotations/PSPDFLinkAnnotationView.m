//
//  PSPDFLinkAnnotation.m
//  PSPDFKit
//
//  Copyright (c) 2011-2012 Peter Steinberger. All rights reserved.
//

#import "PSPDFLinkAnnotationView.h"
#import "PSPDFAnnotation.h"
#import <QuartzCore/QuartzCore.h>

@interface PSPDFLinkAnnotationView() {
    BOOL _buttonPressing:1;
    BOOL _delayActive:1;
    UIColor *_originalBackgroundColor;
}
@end

@implementation PSPDFLinkAnnotationView

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (id)initWithFrame:(CGRect)frame {
    if ((self = [super initWithFrame:frame])) {
        _hideSmallLinks = YES;
        _overspan = CGSizeMake(15, 15);
        self.highlightBackgroundColor = [UIColor colorWithWhite:0.f alpha:0.5f];

        self.layer.borderColor = [UIColor colorWithRed:0.055f green:0.129f blue:0.800f alpha:0.1f].CGColor; // Google-Blue :)
        self.layer.cornerRadius = 4.f;
        [self updateBorder];
    }
    return self;
}

- (void)dealloc {
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(restoreBackgroundColorIfNotPressed) object:nil];
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UIView

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [super touchesBegan:touches withEvent:event];

    // wait until the next runloop to allow touble-taps
    [self cancelTouchDown];
    [self performSelector:@selector(touchDown) withObject:nil afterDelay:0.f];
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    [super touchesEnded:touches withEvent:event];
    [self cancelTouchDown];
    [self touchCancel];
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event {
    [super touchesCancelled:touches withEvent:event];
    [self cancelTouchDown];
    [self touchCancel];
}

- (void)setFrame:(CGRect)frame {
    [super setFrame:frame];
    [self updateBorder];
}

- (void)layoutSubviews {
    [super layoutSubviews];

    // reposition automatically
    CGRect annotationRect = [self.annotation rectForPageRect:self.superview.bounds];
    if (!CGRectEqualToRect(self.frame, annotationRect)) {
        self.frame = annotationRect;
    }
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public

- (void)setAnnotation:(PSPDFAnnotation *)annotation {
    if (self.annotation != annotation) {
        [super setAnnotation:annotation];

        // reset all properties
        _buttonPressing = NO;
        _delayActive = NO;

        [self updateBorder];
        [self restoreButtonBackgroundAnimated:NO];

        [self cancelTouchDown];
        [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(restoreBackgroundColorIfNotPressed) object:nil];
    }
}

- (void)flashBackground {
    [self touchDown];
    [self touchUp];
}

- (void)setBorderColor:(UIColor *)borderColor {
    self.layer.borderColor = borderColor.CGColor;
}

- (UIColor *)borderColor {
    return [UIColor colorWithCGColor:self.layer.borderColor];
}

- (void)setStrokeWidth:(CGFloat)strokeWidth {
    self.layer.borderWidth = strokeWidth;
}

- (CGFloat)strokeWidth {
    return self.layer.borderWidth;
}

- (void)setCornerRadius:(CGFloat)cornerRadius {
    self.layer.cornerRadius = cornerRadius;
}

- (CGFloat)cornerRadius {
    return self.layer.cornerRadius;
}

- (void)setHideSmallLinks:(BOOL)hideSmallLinks {
    _hideSmallLinks = hideSmallLinks;
    [self updateBorder];
}

- (void)setOverspan:(CGSize)overspan {
    _overspan = overspan;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Private

- (BOOL)shouldShowPressedColor {
    return _delayActive || _buttonPressing;
}

- (void)restoreButtonBackgroundAnimated:(BOOL)animated {
    if (![self shouldShowPressedColor]) {
        [UIView animateWithDuration:animated ? 0.25f : 0.f delay:0.f options:UIViewAnimationOptionAllowUserInteraction animations:^{
            //PSPDFLogVerbose(@"Restoring color to %@, animated: %d", _originalBackgroundColor, animated);
            if (_originalBackgroundColor) { self.backgroundColor = _originalBackgroundColor; _originalBackgroundColor = nil; }
        } completion:^(BOOL finished) {
            self.layer.mask = NULL;
        }];
    }
}

- (void)restoreBackgroundColorIfNotPressed {
    _delayActive = NO;
    [self restoreButtonBackgroundAnimated:YES];
}

- (void)cancelTouchDown {
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(touchDown) object:nil];
}

- (void)touchDown {
    _buttonPressing = YES;
    _delayActive = YES;
    [self setNeedsDisplay];

    CALayer *mask = [CALayer layer];
    [mask setMasksToBounds:YES];
    [mask setCornerRadius:self.cornerRadius];
    mask.frame = self.bounds;
    [mask setBorderWidth:1.0f];
    [mask setBorderColor:[[UIColor blackColor] CGColor]];
    [mask setBackgroundColor:[[UIColor whiteColor] CGColor]];
    self.layer.mask = mask;

    // only set original background color if it's currently nil (else we loose the data)
    if (!_originalBackgroundColor) _originalBackgroundColor = self.backgroundColor ?: [UIColor clearColor];

    self.backgroundColor = self.highlightBackgroundColor;

    [self performSelector:@selector(restoreBackgroundColorIfNotPressed) withObject:nil afterDelay:0.25f];
}

- (void)touchUp {
    _buttonPressing = NO;
    [self restoreButtonBackgroundAnimated:NO];
}

- (void)touchCancel {
    if (_buttonPressing) {
        _buttonPressing = NO;
        _delayActive = NO;
        [self restoreButtonBackgroundAnimated:NO];
    }
}

- (BOOL)pointInside:(CGPoint)point withEvent:(UIEvent *)event {
    BOOL inside = [super pointInside:point withEvent:event];
    if (!inside) {
        // first check if there is a LinkAnnotationView with the *exact* frame
        BOOL hasExactMatch = NO;
        for (UIView *view in self.superview.subviews) {
            CGPoint convertedPoint = [self convertPoint:point toView:view];
            if (CGRectContainsPoint(view.bounds, convertedPoint) && [view isKindOfClass:[self class]]) {
                hasExactMatch = YES;
                break;
            }
        }

        if (!hasExactMatch) {
            CGRect origRect = self.bounds;
            CGRect expandedRect = CGRectInset(origRect, -_overspan.width, -_overspan.height);
            inside = CGRectContainsPoint(expandedRect, point);
        }
    }
    return inside;
}

- (void)updateBorder {
    // style button
    BOOL hideBorder = _hideSmallLinks && (self.frame.size.width < 6 || self.frame.size.height < 6);
    if (hideBorder) {
        self.strokeWidth = 0.f;
        self.userInteractionEnabled = NO;
    }else {
        self.strokeWidth = 1.f;
        self.userInteractionEnabled = YES;
    }
}

@end
