//
//  PSPDFRichMediaAnnotation.m
//  PSPDFKit
//
//  Copyright (c) 2012 Peter Steinberger. All rights reserved.
//

#import "PSPDFRichMediaAnnotation.h"
#import "PSPDFConverter.h"
#import "PSPDFStream.h"
#import "PSPDFDocument.h"
#import "PSPDFAnnotationParser.h"
#import "PSPDFFileAnnotationProvider.h"

@implementation PSPDFRichMediaAnnotation {
    CGPDFDictionaryRef _annotDict;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Static

+ (NSArray *)supportedTypes {
    return @[@"RichMedia"];
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (id)init {
    return self = [super initWithType:PSPDFAnnotationTypeRichMedia];
}

- (id)initWithAnnotationDictionary:(CGPDFDictionaryRef)annotDict inAnnotsArray:(CGPDFArrayRef)annotsArray {
	if((self = [super initWithAnnotationDictionary:annotDict inAnnotsArray:annotsArray type:PSPDFAnnotationTypeRichMedia])) {
        self.linkType = PSPDFLinkAnnotationVideo; // set PSPDFLinkAnnotation subtype to generate the views.
        //NSLog(@"%@", PSPDFConvertPDFDictionary(annotDict));

        // unless skin is defined, controls are disabled.
        self.controlsEnabled = NO;

        // find the video URL
        NSString *flashVarString = PSPDFDictionaryGetObjectForPath(annotDict, @"RichMediaContent.Configurations.#0.Instances.#0.Params.FlashVars");
        NSArray *flashVars = [flashVarString componentsSeparatedByString:@"&"];
        //PSPDFLogVerbose(@"flashVars: %@", flashVars);
        for (NSString *flashVar in flashVars) {
            if ([flashVar hasPrefix:@"source=http://"] || [flashVar hasPrefix:@"source=https://"]) {
                self.URL = [NSURL URLWithString:[flashVar stringByReplacingOccurrencesOfString:@"source=" withString:@""]];
            }
            // don't support all control variants, just enable/disable. If skin is defined, controls should be displayed.
            else if ([flashVar hasPrefix:@"skin="]) {
                self.controlsEnabled = YES;
            }
        }

        // do we have an embedded stream here?
        if (!self.URL) {
            _annotDict = annotDict; // save to process later
        }
	}
	return self;
}

- (void)parse {
    if (!self.URL && _annotDict) {
        // find asset name, and skip any embedded flash players
        BOOL skipFirstEntry = NO;
        NSString *assetName = PSPDFDictionaryGetObjectForPath(_annotDict, @"RichMediaContent.Assets.Names.#1.F");
        if ([[assetName pathExtension] isEqualToString:@"swf"]) {
            // embedded flash player? check for more content.
            assetName = PSPDFDictionaryGetObjectForPath(_annotDict, @"RichMediaContent.Assets.Names.#3.F");
            skipFirstEntry = YES;
        }

        if ([assetName length]) {
            // check if video type is supported
            PSPDFAnnotationParser *annotParser = [self.document annotationParserForPage:self.page];
            if ([annotParser.fileAnnotationProvider.videoFileTypes containsObject:[assetName pathExtension]]) {

                // get stream and convert
                PSPDFStream *stream = PSPDFDictionaryGetObjectForPath(_annotDict, [NSString stringWithFormat:@"RichMediaContent.Assets.Names.#%@.EF.F", skipFirstEntry ? @"3" : @"1"]);
                if ([stream isKindOfClass:[PSPDFStream class]]) {
                    self.URL = [stream fileURLWithAssetName:assetName document:self.document page:self.page];
                }
            }
        }
        _annotDict = nil; // remove reference, breaks anyway after a CGPDFDocument reload.
    }
}

@end
