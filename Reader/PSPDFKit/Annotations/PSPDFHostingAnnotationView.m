//
//  PSPDFHostingAnnotationView.m
//  PSPDFKit
//
//  Copyright (c) 2012 Peter Steinberger. All rights reserved.
//

#import "PSPDFHostingAnnotationView.h"
#import "PSPDFPageRenderer.h"
#import "PSPDFAnnotation.h"
#import "PSPDFDocument.h"

@interface PSPDFHostingAnnotationView ()
@property (nonatomic, strong) UIImageView *annotationImageView;
@end

@implementation PSPDFHostingAnnotationView

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (void)dealloc {
    [[PSPDFRenderQueue sharedRenderQueue] cancelRenderingForDelegate:self async:NO];
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UIView

- (void)didMoveToSuperview {
    [super didMoveToSuperview];

    if (self.superview) {
        PSPDFAnnotation *annotation = self.annotation;
        if (annotation) {

            // loaded lazily
            if (!_annotationImageView) {
                _annotationImageView = [[UIImageView alloc] initWithFrame:self.bounds];
                _annotationImageView.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
                _annotationImageView.contentMode = UIViewContentModeScaleToFill;
                _annotationImageView.opaque = NO;
                [self addSubview:_annotationImageView];
            }

            // render image in sync (so we don't get a white box at the beginning)
            _annotationImageView.image = [annotation.document renderImageForPage:annotation.absolutePage withSize:self.superview.bounds.size clippedToRect:self.frame withAnnotations:@[annotation] options:@{kPSPDFDisablePageRendering : @YES, kPSPDFRenderOverlayAnnotations : @YES, kPSPDFBackgroundFillColor : [UIColor clearColor]}];
        }
    }
}

- (void)setNeedsDisplay {
    [super setNeedsDisplay];

    dispatch_async(dispatch_get_main_queue(), ^{
        PSPDFAnnotation *annotation = [self.annotation copy];
        if (annotation) {
            [[PSPDFRenderQueue sharedRenderQueue] cancelRenderingForDelegate:self async:NO];
            [[PSPDFRenderQueue sharedRenderQueue] requestRenderedImageForDocument:annotation.document forPage:annotation.absolutePage withSize:self.superview.bounds.size clippedToRect:self.frame withAnnotations:@[annotation] options:@{kPSPDFDisablePageRendering : @YES, kPSPDFRenderOverlayAnnotations : @YES, kPSPDFBackgroundFillColor : [UIColor clearColor]} delegate:self];
        }
    });
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - PSPDFRenderDelegate

- (void)renderQueue:(PSPDFRenderQueue *)renderQueue jobDidFinish:(PSPDFRenderJob *)job {
    _annotationImageView.image = job.renderedImage;
}

@end
