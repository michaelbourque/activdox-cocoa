//
//  PSPDFDrawView.m
//  PSPDFKit
//
//  Copyright (c) 2012 Peter Steinberger. All rights reserved.
//

#import "PSPDFDrawView.h"
#import "PSPDFConverter.h"
#import "UIColor+PSPDFKitAdditions.h"

@interface PSPDFDrawView () {
	NSMutableArray *_currentPathPoints;
	NSMutableArray *_actionList;
	NSMutableArray *_redoActionList;
    NSMutableArray *_linesDictionaries;
	CGPoint _lastPoint;
    BOOL _isDrawing;
}
@property (nonatomic, strong) NSArray *linesDictionaries;
@property (nonatomic, strong) CAShapeLayer *shapeLayer;
@property (nonatomic, strong) UIBezierPath *path;
@property (nonatomic, strong) UIImage *currentImage;
@property (nonatomic, assign) BOOL hasChanges;
@end

NSString *const kPSPDFPointsKey = @"points";
NSString *const kPSPDFWidthKey = @"width";
NSString *const kPSPDFColorKey = @"color";

@implementation PSPDFDrawView

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (id)initWithFrame:(CGRect)frame {
    if ((self = [super initWithFrame:frame])) {
		self.backgroundColor = [UIColor colorWithWhite:1.0 alpha:0.5];
		self.path = [UIBezierPath new];
		_path.lineJoinStyle = kCGLineJoinRound;
		_path.lineCapStyle = kCGLineCapRound;
		_currentPathPoints = [NSMutableArray new];
		_actionList = [NSMutableArray new];
		_redoActionList = [NSMutableArray new];

		self.linesDictionaries = [NSMutableArray array];

        // initialize the shape layer
		_shapeLayer = [CAShapeLayer layer];
		_shapeLayer.fillColor = nil;
		_shapeLayer.lineCap = kCALineCapRound;
		_shapeLayer.lineJoin = kCALineJoinRound;
		[self.layer addSublayer:_shapeLayer];

        // default color
        self.strokeColor = [UIColor colorWithRed:1.0 green:0.0 blue:0.0 alpha:1.0]; // red
        _lineWidth = 3.f;
        _shapeLayer.lineWidth = _lineWidth;
    }
    return self;
}

- (void)drawRect:(CGRect)rect {
	[self.currentImage drawInRect:self.bounds];
	[self.strokeColor set];
	[self.path stroke];
}

- (void)loadImage:(UIImage *)image {
	self.currentImage = image;
	[_actionList removeAllObjects];
	[_redoActionList removeAllObjects];
	self.hasChanges = NO;
	[self setNeedsDisplay];
}

- (void)setLineWidth:(CGFloat)lineWidth {
	_lineWidth = lineWidth;
	self.shapeLayer.lineWidth = lineWidth;
}

- (void)setStrokeColor:(UIColor *)strokeColor {
	_strokeColor = strokeColor;
	self.shapeLayer.strokeColor = [strokeColor CGColor];
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Undo/Redo

- (BOOL)canUndo {
	return [_actionList count] != 0;
}

- (BOOL)undo {
	if (![self canUndo]) return NO;

	UIGraphicsBeginImageContextWithOptions(self.bounds.size, NO, 0.0);
	for (int i=0; i<[_actionList count]-1; i++) {
		[_actionList[i] apply];
	}

	[_linesDictionaries removeLastObject];

	self.currentImage = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();

	[self setNeedsDisplay];
	self.path = nil;

	[_redoActionList addObject:[_actionList lastObject]];
	[_actionList removeLastObject];

	[_currentPathPoints removeAllObjects];
    return YES;
}

- (BOOL)canRedo {
	return [_redoActionList count] != 0;
}

- (BOOL)redo {
	if (![self canRedo]) return NO;

	UIGraphicsBeginImageContextWithOptions(self.bounds.size, NO, 0.0);
	[_currentImage drawInRect:self.bounds];

	PSPDFDrawAction *redoAction = [_redoActionList lastObject];
	[redoAction apply];

	[_linesDictionaries addObject:@{kPSPDFPointsKey: [NSArray arrayWithArray:redoAction.points],
     kPSPDFWidthKey: @(_lineWidth), kPSPDFColorKey: self.strokeColor}];

	self.currentImage = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
	[self setNeedsDisplay];
	self.path = nil;

	[_actionList addObject:[_redoActionList lastObject]];
	[_redoActionList removeLastObject];

	[_currentPathPoints removeAllObjects];

    if ([self.delegate respondsToSelector:@selector(drawView:didChange:)]) {
        [self.delegate drawView:self didChange:redoAction];
    }
    return YES;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Touch Handling

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
	UITouch *touch = [touches anyObject];
	CGPoint location = [touch locationInView:self];

	UIView *hitView = [self hitTest:location withEvent:event];
	if (hitView != self) {
		return; // toolbar was touched... ignore
	}

	_isDrawing = YES;

	if ([self.delegate respondsToSelector:@selector(drawViewDidBeginDrawing:)]) {
		[self.delegate drawViewDidBeginDrawing:nil];
	}

	self.shapeLayer.hidden = NO;

	self.path = [UIBezierPath new];
	_path.lineJoinStyle = kCGLineJoinRound;
	_path.lineCapStyle = kCGLineCapRound;
	_path.lineWidth = _lineWidth;

	[_path moveToPoint:location];
	[_currentPathPoints addObject:[NSValue valueWithCGPoint:location]];

	_shapeLayer.path = [_path CGPath];

	_lastPoint = location;
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
	if (!_isDrawing) return;

	UITouch *touch = [touches anyObject];
	CGPoint location = [touch locationInView:self];

	[_path addLineToPoint:location];
	[_currentPathPoints addObject:[NSValue valueWithCGPoint:location]];
	_shapeLayer.path = [_path CGPath];
	_lastPoint = location;
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
	if (!_isDrawing) return;
	_isDrawing = NO;

	_hasChanges = YES;
	UIGraphicsBeginImageContextWithOptions(self.bounds.size, NO, 0.0);
	[_currentImage drawInRect:self.bounds];

	NSString *actionType = @"draw";
	PSPDFDrawAction *action = [[PSPDFDrawAction alloc] initWithPoints:_currentPathPoints path:_path type:actionType strokeColor:_strokeColor lineWidth:_lineWidth];
	[action apply];

	[_linesDictionaries addObject:@{kPSPDFPointsKey: [NSArray arrayWithArray:_currentPathPoints], kPSPDFWidthKey: @(_lineWidth), kPSPDFColorKey: self.strokeColor}];

	self.currentImage = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();

	if ([_currentPathPoints count] == 1) {
		CGPoint point = [_currentPathPoints[0] CGPointValue];
		[self setNeedsDisplayInRect:CGRectMake(point.x - _lineWidth, point.y - _lineWidth, _lineWidth*2, _lineWidth*2)];
	} else {
		[self setNeedsDisplayInRect:CGRectInset([_path bounds], -_lineWidth, -_lineWidth)];
	}
	self.path = nil;

	[_actionList addObject:action];

	[_currentPathPoints removeAllObjects];
	[_redoActionList removeAllObjects];

	if ([self.delegate respondsToSelector:@selector(drawView:didChange:)]) {
		[self.delegate drawView:self didChange:action];
	}
	_shapeLayer.hidden = YES;
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event {
	self.path = nil;
	[_currentPathPoints removeAllObjects];
}

@end

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - PSPDFDrawAction

@interface PSPDFDrawAction ()
@property (nonatomic, copy) NSString *type;
@property (nonatomic, strong) UIColor *strokeColor;
@property (nonatomic, strong) UIBezierPath *path;
@property (nonatomic, assign) CGFloat lineWidth;
@end

@implementation PSPDFDrawAction

- (id)initWithPoints:(NSArray *)pointArray path:(UIBezierPath *)bezierPath type:(NSString *)actionType strokeColor:(UIColor *)color lineWidth:(CGFloat)lineWidth {
	if((self = [super init])) {
        _points = [[NSArray alloc] initWithArray:pointArray];
        _type = actionType;
        _strokeColor = color;
        _path = [bezierPath copy];
        _lineWidth = lineWidth;
    }
	return self;
}

- (NSString *)description {
    return [NSString stringWithFormat:@"<%@ type:%@ strokeColor:%@(%@) path:%@ lineWidth:%f points:%@>", NSStringFromClass([self class]), self.type, [self.strokeColor pspdf_closestAnnotationColorName], self.strokeColor, self.path, self.lineWidth, self.points];
}

- (void)apply {
	[_strokeColor set];
	float alpha = CGColorGetAlpha(_strokeColor.CGColor);

	if ([_points count] == 1) {
		CGPoint point = [_points[0] CGPointValue];
		CGContextFillEllipseInRect(UIGraphicsGetCurrentContext(), CGRectMake(point.x - _lineWidth/2, point.y - _lineWidth/2, _lineWidth, _lineWidth));
	} else {
		if (alpha == 1.0) {
            UIBezierPath *splinePath = PSPDFSplineWithPointArray(_points, _lineWidth);
            splinePath.lineWidth = _lineWidth;
            [splinePath stroke];
		} else {
			// don't smoothen paths with alpha (not fully implemented)
			UIBezierPath *rawPath = [[UIBezierPath alloc] init];
			rawPath.lineWidth = _lineWidth;
			rawPath.lineJoinStyle = kCGLineJoinRound;
			rawPath.lineCapStyle = kCGLineCapRound;
			[rawPath moveToPoint:[_points[0] CGPointValue]];
			for (int i=1; i<[_points count]; i++) {
				[rawPath addLineToPoint:[_points[i] CGPointValue]];
			}
			[rawPath stroke];
		}
	}
}

@end
