//
//  PSPDFNoteAnnotation
//  PSPDFKit
//
//  Copyright 2012 Peter Steinberger. All rights reserved.
//

#import "PSPDFNoteAnnotation.h"
#import "PSPDFNoteAnnotationView.h"
#import "PSPDFPageInfo.h"
#import "PSPDFDocument.h"
#import "PSPDFConverter.h"

CGSize kPSPDFNoteAnnotationViewFixedSize = (CGSize){32, 32};

@implementation PSPDFNoteAnnotation

@synthesize iconName = _iconName;

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Static

+ (NSArray *)supportedTypes {
    return @[PSPDFAnnotationTypeStringNote];
}

+ (BOOL)isWriteable {
    return YES;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (id)init {
	if((self = [super initWithType:PSPDFAnnotationTypeNote])) {
        _iconName = @"Comment";
        self.color = [UIColor colorWithRed:0.95 green:0.87 blue:0.28 alpha:1.f]; // yellow
    }
	return self;
}

- (id)initWithAnnotationDictionary:(CGPDFDictionaryRef)annotDict inAnnotsArray:(CGPDFArrayRef)annotsArray {
	if((self = [super initWithAnnotationDictionary:annotDict inAnnotsArray:annotsArray type:PSPDFAnnotationTypeNote])) {
        _iconName = PSPDFDictionaryGetString(annotDict, @"Name") ?: @"Comment";
        self.color = self.color ?: [UIColor colorWithRed:0.95 green:0.87 blue:0.28 alpha:1.f];
        self.dirty = NO;
    }
	return self;
}

- (NSUInteger)hash {
    return [super hash] * 31 + [self.iconName hash];
}

- (BOOL)isEqualToAnnotation:(PSPDFAnnotation *)otherAnnotation {
    if ([super isEqualToAnnotation:otherAnnotation]) {
        PSPDFNoteAnnotation *otherNoteAnnotations = (PSPDFNoteAnnotation *)otherAnnotation;
        if ([self.iconName isEqualToString:otherNoteAnnotations.iconName]) {
            return YES;
        }
    }
    return NO;
}

- (NSString *)description {
    return [NSString stringWithFormat:@"%@ iconName:%@>", [super description], self.iconName];
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - PSPDFAnnotation

- (BOOL)isOverlay {
    return YES;
}

- (NSData *)pdfDataRepresentation {
	NSMutableData *pdfData = [NSMutableData data];
	[pdfData pspdf_appendDataString:[NSString stringWithFormat:@"<</Type/Annot/Subtype/Text/F 4 %@ %@ /Name /%@ ", [self pdfColorString], [self pdfRectString], self.iconName]];
    [self appendEscapedContents:pdfData];
	[pdfData pspdf_appendDataString:@">>"];
	return pdfData;
}

- (CGRect)boundingBoxForPageViewBounds:(CGRect)pageBounds {
    CGRect customBoundingBox = self.boundingBox;
    PSPDFPageInfo *pageInfo = [self.document pageInfoForPage:self.page];
    CGRect pdfSizeRect = PSPDFConvertViewRectToPDFRect((CGRect){.size=kPSPDFNoteAnnotationViewFixedSize}, pageInfo.pageRect, pageInfo.pageRotation, pageBounds);

    // update bounding box and compensate size increases to center.
    customBoundingBox.size = pdfSizeRect.size;
    customBoundingBox.origin.y -= customBoundingBox.size.height - self.boundingBox.size.height;
    return customBoundingBox;
}

// custom method because we have a fixed size here.
- (BOOL)hitTest:(CGPoint)point withViewBounds:(CGRect)bounds {
    CGRect customBoundingBox = [self boundingBoxForPageViewBounds:bounds];
    if ((customBoundingBox.origin.x <= point.x) &&
        (customBoundingBox.origin.y <= point.y) &&
        (point.x <= customBoundingBox.origin.x + customBoundingBox.size.width) &&
        (point.y <= customBoundingBox.origin.y + customBoundingBox.size.height)) {
        return YES;
    } else {
        return NO;
    }
}

- (void)drawInContext:(CGContextRef)context {
    // [super drawInContext:context];
    // will be drawn using a UIView to allow moving the annotation.

    // just used for debugging.
    /*
    CGContextSaveGState(context);

    [[UIColor orangeColor] setFill];
    CGRect customBoundingBox = self.boundingBox;
    customBoundingBox.size = CGSizeMake(60, 60);
    customBoundingBox.origin.y -= customBoundingBox.size.height - _boundingBox.size.height;

    CGContextFillRect(context, customBoundingBox);

    CGContextRestoreGState(context);
     */
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Properties

- (NSString *)iconName {
    return _iconName ?: @"Comment";
}

- (void)setIconName:(NSString *)iconName {
    if (iconName != _iconName) {
        _iconName = iconName;
        self.dirty = YES;
    }
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSCoding

- (id)initWithCoder:(NSCoder *)decoder {
	if((self = [super initWithCoder:decoder])) {
        _iconName = [decoder decodeObjectForKey:NSStringFromSelector(@selector(iconName))];
    }
	return self;
}

- (void)encodeWithCoder:(NSCoder *)coder {
	[super encodeWithCoder:coder];
    [coder encodeObject:self.iconName forKey:NSStringFromSelector(@selector(iconName))];
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSCopying

- (id)copyWithZone:(NSZone *)zone {
    PSPDFNoteAnnotation *textAnnotation = (PSPDFNoteAnnotation *)[super copyWithZone:zone];
    textAnnotation.iconName = self.iconName;
    return textAnnotation;
}

@end
