//
//  PSPDFColorButton.m
//  PSPDFKit
//
//  Copyright (c) 2012 Peter Steinberger. All rights reserved.
//

#import "PSPDFColorButton.h"
#import "UIColor+PSPDFKitAdditions.h"

@implementation PSPDFColorButton

- (void)drawRect:(CGRect)rect {
	if (!self.color) return;

	UIBezierPath *roundRect = [UIBezierPath bezierPathWithRoundedRect:self.bounds cornerRadius:5.0];
	UIBezierPath *innerRect = [UIBezierPath bezierPathWithRoundedRect:CGRectInset(self.bounds, 3, 3) cornerRadius:3];

	if (self.displayAsEllipse) {
		[[UIColor whiteColor] set];
		CGContextFillEllipseInRect(UIGraphicsGetCurrentContext(), CGRectInset(self.bounds, 1, 1));
	}
	if (self.highlighted) {
		[[UIColor whiteColor] set];
	} else {
		[[self.color pspdf_colorByMultiplyingBy:0.75] set];
	}
	if (self.displayAsEllipse) {
		CGContextFillEllipseInRect(UIGraphicsGetCurrentContext(), self.bounds);
		[self.color set];
		CGContextFillEllipseInRect(UIGraphicsGetCurrentContext(), CGRectInset(self.bounds, 3, 3));
	} else {
		[roundRect fill];
		[self.color set];
		[innerRect fill];
	}
}

- (void)setHighlighted:(BOOL)highlighted {
	[super setHighlighted:highlighted];
	[self setNeedsDisplay];
}

- (void)setColor:(UIColor *)color {
    if (color != _color) {
        _color = color;
        [self setNeedsDisplay];
    }
}

@end
