//
//  PSPDFWebAnnotationView.m
//  PSPDFKit
//
//  Copyright (c) 2011-2012 Peter Steinberger. All rights reserved.
//

#import "PSPDFKit.h"
#import "PSPDFWebAnnotationView.h"

@implementation PSPDFWebAnnotationView {
    BOOL _shadowsHidden;
    NSUInteger _requestCount;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Private

- (void)loadingStatusChanged_ {
    PSPDFLog(@"Finished loading %@", _webView.request);
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (id)initWithFrame:(CGRect)frame {
    if ((self = [super initWithFrame:frame])) {
        _webView = [[UIWebView alloc] initWithFrame:self.bounds];
        _webView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        [self addSubview:_webView];
        _webView.delegate = self;
        _webView.scalesPageToFit = YES;
        
        // allow inline playback, even on iPhone
        _webView.allowsInlineMediaPlayback = YES;
    }
    return self;
}

- (void)dealloc {
    // "the deallocation problem" - it's not safe to dealloc a controler from a thread different than the main thread
    // http://developer.apple.com/library/ios/#technotes/tn2109/_index.html#//apple_ref/doc/uid/DTS40010274-CH1-SUBSECTION11
    NSAssert([NSThread isMainThread], @"Must run on main thread, see http://developer.apple.com/library/ios/#technotes/tn2109/_index.html#//apple_ref/doc/uid/DTS40010274-CH1-SUBSECTION11");
    _webView.delegate = nil; // delegate is self here, so first set to nil before call stopLoading.
    [_webView stopLoading];
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UIView

- (void)layoutSubviews {
    [super layoutSubviews];
    _webView.frame = self.bounds;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public

- (void)setAnnotation:(PSPDFAnnotation *)annotation {
    if (self.annotation != annotation) {
        [super setAnnotation:annotation];
        [self.webView loadRequest:[NSURLRequest requestWithURL:self.linkAnnotation.URL]];
    }
}

- (BOOL)shadowsHidden {
	for (UIView *view in [_webView subviews]) {
		if ([view isKindOfClass:[UIScrollView class]]) {
			for (UIView *innerView in [view subviews]) {
				if ([innerView isKindOfClass:[UIImageView class]]) {
					return [innerView isHidden];
				}
			}
		}
	}
	return NO;
}

- (void)setShadowsHidden:(BOOL)hide {
	if (_shadowsHidden == hide) {
		return;
	}    
	_shadowsHidden = hide;
    
	for (UIView *view in [_webView subviews]) {
		if ([view isKindOfClass:[UIScrollView class]]) {
			for (UIView *innerView in [view subviews]) {
				if ([innerView isKindOfClass:[UIImageView class]]) {
					innerView.hidden = _shadowsHidden;
				}
			}
		}
	}
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UIWebViewDelegate

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
	_requestCount--;
	if (_requestCount == 0) {
		[self loadingStatusChanged_];
	}
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
	_requestCount--;
	if (_requestCount == 0) {
		[self loadingStatusChanged_];
	}
}

- (void)webViewDidStartLoad:(UIWebView *)webView {
	_requestCount++;
}

@end
