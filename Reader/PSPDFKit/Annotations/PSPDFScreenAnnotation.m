//
//  PSPDFScreenAnnotation.m
//  PSPDFKit
//
//  Copyright (c) 2012 Peter Steinberger. All rights reserved.
//

#import "PSPDFScreenAnnotation.h"
#import "PSPDFConverter.h"
#import "PSPDFStream.h"
#import "PSPDFDocument.h"
#import "PSPDFAnnotationParser.h"
#import "PSPDFFileAnnotationProvider.h"

@implementation PSPDFScreenAnnotation {
    CGPDFDictionaryRef _annotDict;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Static

+ (NSArray *)supportedTypes {
    return @[@"Screen"];
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (id)init {
    return self = [super initWithType:PSPDFAnnotationTypeScreen];
}

- (id)initWithAnnotationDictionary:(CGPDFDictionaryRef)annotDict inAnnotsArray:(CGPDFArrayRef)annotsArray {
	if((self = [super initWithAnnotationDictionary:annotDict inAnnotsArray:annotsArray type:PSPDFAnnotationTypeScreen])) {
        self.linkType = PSPDFLinkAnnotationVideo; // set PSPDFLinkAnnotation subtype to generate the views.
       _annotDict = annotDict; // save to process later
        self.controlsEnabled = NO; // set as default.
	}
	return self;
}

- (void)parse {
    if (!self.URL && _annotDict) {
        NSString *assetName = PSPDFDictionaryGetObjectForPath(_annotDict, @"A.R.C.N");

        // if original asset has no file extension, check the file name.
        if (![[assetName pathExtension] length]) {
            assetName = PSPDFDictionaryGetObjectForPath(_annotDict, @"A.R.C.D.F");
        }

        PSPDFAnnotationParser *annotParser = [self.document annotationParserForPage:self.page];
        if ([annotParser.fileAnnotationProvider.videoFileTypes containsObject:[assetName pathExtension]]) {

            // get stream and convert
            PSPDFStream *stream = PSPDFDictionaryGetObjectForPath(_annotDict, @"A.R.C.D.EF.F");
            if ([stream isKindOfClass:[PSPDFStream class]]) {
                self.URL = [stream fileURLWithAssetName:assetName document:self.document page:self.page];
            }
        }
        _annotDict = nil; // remove reference, breaks anyway after a CGPDFDocument reload.
    }
}

@end
