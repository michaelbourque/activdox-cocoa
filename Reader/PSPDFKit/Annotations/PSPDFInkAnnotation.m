//
//  PSPDFInkAnnotation.m
//  PSPDFKit
//
//  Copyright 2012 Peter Steinberger. All rights reserved.
//

#import "PSPDFInkAnnotation.h"
#import "PSPDFConverter.h"
#import "UIColor+PSPDFKitAdditions.h"
#import "PSPDFConverter.h"

@implementation PSPDFInkAnnotation

@synthesize paths = _paths;

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Static

+ (NSArray *)supportedTypes {
    return @[PSPDFAnnotationTypeStringInk];
}

+ (BOOL)isWriteable {
    return YES;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (id)init {
    return self = [super initWithType:PSPDFAnnotationTypeInk];
}

- (id)initWithAnnotationDictionary:(CGPDFDictionaryRef)annotDict inAnnotsArray:(CGPDFArrayRef)annotsArray {
    if ((self = [super initWithAnnotationDictionary:annotDict inAnnotsArray:annotsArray type:PSPDFAnnotationTypeInk])) {
        
        // default color is black
        self.color = self.color ?: [UIColor blackColor];
        
        // lines
        CGPDFArrayRef inkList = NULL;
        CGPDFDictionaryGetArray(annotDict, "InkList", &inkList);
        
        NSMutableArray *lineBuffer = [NSMutableArray array];
        
        for (int i=0; i<CGPDFArrayGetCount(inkList); i++) {
            NSMutableArray *currentLine = [NSMutableArray array];
            CGPDFArrayRef line = NULL;
            CGPDFArrayGetArray(inkList, i, &line);
            for (int j=0; j<CGPDFArrayGetCount(line); j+=2) {
                CGPDFReal x;
                CGPDFReal y;
                CGPDFArrayGetNumber(line, j, &x);
                CGPDFArrayGetNumber(line, j+1, &y);
                CGPoint point = CGPointMake(x, y);
                NSValue *pointValue = [NSValue valueWithCGPoint:point];
                if (pointValue) [currentLine addObject:pointValue];
            }
            [lineBuffer addObject:[NSArray arrayWithArray:currentLine]];
        }
        self.lines = [NSArray arrayWithArray:lineBuffer];
        self.dirty = NO;
	}
	return self;
}

- (NSString *)description {
    NSMutableString *description = [[super description] mutableCopy];
    if (self.lines) {
        [description appendFormat:@" lines:#%d", [self.lines count]];
    }
    return [description copy];
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - PSPDFAnnotation

- (void)drawInContext:(CGContextRef)context {
    if (self.isDeleted) return;

    CGContextSaveGState(context);
    
    CGContextSetStrokeColorWithColor(context, self.colorWithAlpha.CGColor);
    [self prepareBorderStyleInContext:context];
    for (UIBezierPath *path in self.paths) {
        path.lineCapStyle = kCGLineCapRound;
        path.lineJoinStyle = kCGLineJoinRound;
        path.lineWidth = self.lineWidth;
        [path stroke];
    }

    CGContextRestoreGState(context);
}

- (NSData *)pdfDataRepresentation {
	NSMutableString *inkList = [NSMutableString stringWithString:@"["];
	for (NSArray *line in self.lines) {
		[inkList appendString:@"["];
		for (NSValue *pointValue in line) {
			CGPoint point = [pointValue CGPointValue];
			[inkList appendFormat:@"%f %f ", point.x, point.y];
		}
		[inkList appendString:@"] "];
	}
	[inkList appendString:@"]"];

    NSMutableData *pdfData = [NSMutableData data];
    [pdfData pspdf_appendDataString:[NSString stringWithFormat:@"<</Type/Annot/Subtype/Ink /F 4 %@ %@ /BS <</W %f>> /InkList %@ ", [self pdfRectString], [self pdfColorWithAlphaString], self.lineWidth, inkList]];
    [self appendEscapedContents:pdfData];
    [pdfData pspdf_appendDataString:@">>"];
	return pdfData;
}

- (void)setBoundingBox:(CGRect)boundingBox {
    CGRect oldBox = self.boundingBox;
    if (!CGRectEqualToRect(boundingBox, oldBox)) {
        [super setBoundingBox:boundingBox];

        // create transform that needs to be applied on all lines
        if (!CGRectIsEmpty(oldBox)) {
            float sx = boundingBox.size.width / oldBox.size.width;
            float sy = boundingBox.size.height / oldBox.size.height;
            CGAffineTransform transform = CGAffineTransformMakeTranslation(boundingBox.origin.x-oldBox.origin.x*sx, boundingBox.origin.y-oldBox.origin.y*sy);
            transform = CGAffineTransformScale(transform, sx, sy);

            self.lines = [self copyLinesByApplyingTransform:transform];
        }
    }
}

- (NSArray *)copyLinesByApplyingTransform:(CGAffineTransform)transform {
    NSMutableArray *newLines = [NSMutableArray array];
    for (NSArray *line in self.lines) {
        NSMutableArray *newLine = [NSMutableArray array];
        for (NSValue *pointValue in line) {
            CGPoint point = [pointValue CGPointValue];
            point = CGPointApplyAffineTransform(point, transform);
            [newLine addObject:BOXED(point)];
        }
        [newLines addObject:[newLine copy]];
    }
    return [newLines copy];
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public

- (NSArray *)paths {
	if (!_paths) {
		[self rebuildPaths];
	}
	return _paths;
}

- (void)rebuildPaths {
	NSMutableArray *smoothedPaths = [NSMutableArray array];
	for (NSArray *line in _lines) {
		UIBezierPath *spline = PSPDFSplineWithPointArray(line, 2.0);
		if (spline) {
			[smoothedPaths addObject:spline];
		}
	}
	self.paths = smoothedPaths;

    /*
     // This code is only for testing PSPDFBezierPathGetPoints
    NSMutableArray *testArray = [NSMutableArray array];
    for (UIBezierPath *path in self.paths) {
        [testArray addObject:PSPDFBezierPathGetPoints(path)];
    }
    NSLog(@"rebuilt array: %@", testArray);
    */
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Properties

// lines are PDF data, path is just a cache
- (void)setLines:(NSArray *)lines {
    if (lines != _lines) {
        _lines = lines;
        self.paths = nil; // clear path cache
        self.dirty = YES;
    }
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSCoding

- (id)initWithCoder:(NSCoder *)aDecoder {
	if((self = [super initWithCoder:aDecoder])) {
        NSArray *stringsLines = [aDecoder decodeObjectForKey:NSStringFromSelector(@selector(lines))];
        NSMutableArray *tmpLines = [NSMutableArray array];
        for (NSArray *stringsLine in stringsLines) {
            NSMutableArray *line = [NSMutableArray array];
            for (NSString *pointString in stringsLine) {
                CGPoint point = CGPointFromString(pointString);
                NSValue *pointValue = [NSValue valueWithCGPoint:point];
                if (pointValue) [line addObject:pointValue];
            }
            [tmpLines addObject:[NSArray arrayWithArray:line]];
        }
        _lines = [[NSArray alloc] initWithArray:tmpLines];
    }
	return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
	[super encodeWithCoder:aCoder];
	NSMutableArray *tmpStringsLines = [NSMutableArray array];
	for (NSArray *line in _lines) {
		NSMutableArray *tmpStringsLine = [[NSMutableArray alloc] init];
		for (NSValue *pointValue in line) {
			CGPoint point = [pointValue CGPointValue];
			NSString *pointString = NSStringFromCGPoint(point);
			[tmpStringsLine addObject:pointString];
		}
		NSArray *stringsLine = [[NSArray alloc] initWithArray:tmpStringsLine];
		[tmpStringsLines addObject:stringsLine];
	}
	NSArray *stringsLines = [NSArray arrayWithArray:tmpStringsLines];
    
	[aCoder encodeObject:stringsLines forKey:NSStringFromSelector(@selector(lines))];
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSCopying

- (id)copyWithZone:(NSZone *)zone {
    PSPDFInkAnnotation *inkAnnotation = (PSPDFInkAnnotation *)[super copyWithZone:zone];
    inkAnnotation.lines = self.lines;
    return inkAnnotation;
}

@end


static void PSPDFPathApplierFunction(void *info, const CGPathElement *element) {
    NSMutableArray *bezierPoints = (__bridge NSMutableArray *)info;

    CGPoint *points = element->points;
    CGPathElementType type = element->type;

    void (^addObjectBlock)(NSUInteger pointCount) = ^(NSUInteger pc) {
        for(int i = 0; i < pc; ++i) {
            [bezierPoints addObject:[NSValue valueWithCGPoint:points[i]]];
        }
    };

    switch(type) {
        case kCGPathElementMoveToPoint:
        case kCGPathElementAddLineToPoint:      // contains 1 point
            addObjectBlock(1);
            break;
        case kCGPathElementAddQuadCurveToPoint: // contains 2 points
            addObjectBlock(2);
            break;
        case kCGPathElementAddCurveToPoint:     // contains 3 points
            addObjectBlock(3);
            break;
        case kCGPathElementCloseSubpath:        // contains no point
            break;
    }
}

NSArray *PSPDFBezierPathGetPoints(UIBezierPath *path) {
    NSMutableArray *array = [@[] mutableCopy];
    CGPathApply([path CGPath], (__bridge void *)array, PSPDFPathApplierFunction);
    return [array copy];
}

CGRect PSPDFBoundingBoxFromLines(NSArray *lines, CGFloat lineWidth) {
    float minX = MAXFLOAT;
    float maxX = -MAXFLOAT;
    float minY = MAXFLOAT;
    float maxY = -MAXFLOAT;

    for (NSArray *line in lines) {
        for (NSValue *pointValue in line) {
            CGPoint point = [pointValue CGPointValue];
            if (point.x < minX) minX = point.x;
            if (point.y < minY) minY = point.y;
            if (point.x > maxX) maxX = point.x;
            if (point.y > maxY) maxY = point.y;
        }
    }

    return CGRectMake(minX - lineWidth/2, minY - lineWidth/2, maxX-minX + lineWidth, maxY-minY + lineWidth);
}

NSArray *PSPDFConvertViewLinesToPDFLines(NSArray *lines, CGRect cropBox, NSUInteger rotation, CGRect bounds) {
    NSMutableArray *newLines = [NSMutableArray array];
    for (NSArray *line in lines) {
        NSMutableArray *newLine = [NSMutableArray array];
        for (NSValue *pointValue in line) {
            CGPoint point = [pointValue CGPointValue];
            CGPoint transformedPoint = PSPDFConvertViewPointToPDFPoint(point, cropBox, rotation, bounds);
            [newLine addObject:BOXED(transformedPoint)];
        }
        [newLines addObject:newLine];
    }
    return newLines;
}

NSArray *PSPDFConvertPDFLinesToViewLines(NSArray *lines, CGRect cropBox, NSUInteger rotation, CGRect bounds) {
    NSMutableArray *newLines = [NSMutableArray array];
    for (NSArray *line in lines) {
        NSMutableArray *newLine = [NSMutableArray array];
        for (NSValue *pointValue in line) {
            CGPoint point = [pointValue CGPointValue];
            CGPoint transformedPoint = PSPDFConvertPDFPointToViewPoint(point, cropBox, rotation, bounds);
            [newLine addObject:BOXED(transformedPoint)];
        }
        [newLines addObject:newLine];
    }
    return newLines;
}
