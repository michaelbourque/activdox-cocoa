//
//  PSPDFYouTubeAnnotationView.m
//  PSPDFKit
//
//  Copyright (c) 2012 Peter Steinberger. All rights reserved.
//

#import "PSPDFYouTubeAnnotationView.h"
#import "PSPDFVideoAnnotationView.h"
#import "PSPDFKitGlobal.h"
#import "PSPDFAnnotation.h"
#import "PSPDFLinkAnnotation.h"
#import "PSPDFViewController.h"
#import "PSPDFPatches.h"
#import <objc/runtime.h>
#import <objc/message.h>

@interface PSPDFYouTubeAnnotationView() {
    BOOL _isShowing:1;
    BOOL _wasPlaying:1;
}
@property (nonatomic, copy) void (^setupWebView)(void);
@end

@implementation PSPDFYouTubeAnnotationView

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (void)dealloc {
    _webView.delegate = nil;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Private

- (void)setupYouTubeAnnotation {
    // cleanup
    [_webView removeFromSuperview];
    _webView.delegate = nil;
    _webView = nil;

    _youTubeURL = self.linkAnnotation.URL;
    self.autoplayEnabled = self.linkAnnotation.isAutoplayEnabled;

    // fixin' invalid YouTube formats
    if ([[_youTubeURL absoluteString] rangeOfString:@"/v/" options:NSCaseInsensitiveSearch].length > 0) {
        _youTubeURL = [NSURL URLWithString:[[_youTubeURL absoluteString] stringByReplacingOccurrencesOfString:@"/v/" withString:@"/watch?v=" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [[_youTubeURL absoluteString] length])]];
    }
    if ([[_youTubeURL absoluteString] rangeOfString:@"/embed/" options:NSCaseInsensitiveSearch].length > 0) {
        _youTubeURL = [NSURL URLWithString:[[_youTubeURL absoluteString] stringByReplacingOccurrencesOfString:@"/embed/" withString:@"/watch?v=" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [[_youTubeURL absoluteString] length])]];
    }

    __weak PSPDFYouTubeAnnotationView *weakSelf = self;
    _setupWebView = ^{
        PSPDFYouTubeAnnotationView *strongSelf = weakSelf;
        if (!strongSelf.webView) {

#ifdef __i386__
            NSLog(@"\n-----------------------------------------------------------------------------------------------------------");
            NSLog(@"Note: There is no YouTube plugin in the iPhone Simulator. View will be blank. Please test this on a device.");
            NSLog(@"-----------------------------------------------------------------------------------------------------------\n");
#endif
            UIWebView *webView = [[UIWebView alloc] initWithFrame:strongSelf.bounds];
            webView.delegate = strongSelf;
            webView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
            webView.scrollView.scrollEnabled = NO;
            [strongSelf insertSubview:webView atIndex:0];
            // allow inline playback, even on iPhone
            webView.allowsInlineMediaPlayback = YES;
            strongSelf.webView = webView;

            //#ifdef __IPHONE_6_0
            if (kCFCoreFoundationVersionNumber >= kCFCoreFoundationVersionNumber_iOS_6_0) {
                // NEW: http://www.youtube.com/embed/VIDEO_ID OLD: http://www.youtube.com/watch?v=QMa2gb-O1aE?autoplay=1
                NSString *embeddedYouTubeURL = [[strongSelf.youTubeURL absoluteString] stringByReplacingOccurrencesOfString:@"watch?v=" withString:@"embed/"];
                [webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:embeddedYouTubeURL]]];
            }else {
                //#else
                // old style
                NSString *embedHTML = @"<html><head><style type=\"text/css\"> \
                body {background-color:transparent;color:white;}</style> \
                </head><body style=\"margin:0\"> \
                <embed id=\"yt\" src=\"%@\" type=\"application/x-shockwave-flash\" \
                width=\"%0.0f\" height=\"%0.0f\"></embed></body></html>";
                NSString *youTubeURL = [strongSelf.youTubeURL absoluteString];
                NSString *html = [NSString stringWithFormat:embedHTML, youTubeURL, strongSelf.frame.size.width, strongSelf.frame.size.height];
                [webView loadHTMLString:html baseURL:nil];
            }
            //#endif
        }
    };
    _setupWebView();
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - PSPDFAnnotationView

- (void)setAnnotation:(PSPDFAnnotation *)annotation {
    if (self.annotation != annotation) {
        [super setAnnotation:annotation];
        [self setupYouTubeAnnotation];
    }
}

// iOS4/5. Get the class called MPAVController to play/pause the YouTube video.
// https://github.com/steipete/iOS-Runtime-Headers/blob/master/Frameworks/MediaPlayer.framework/MPAVController.h
- (UIViewController *)movieController {
    UIViewController *movieController = nil;
#ifndef _PSPDFKIT_DONT_USE_OBFUSCATED_PRIVATE_API_
    @try {
        UIView *movieView = PSPDFGetViewInsideView(self.webView, [NSString stringWithFormat:@"MP%@oView", @"Vide"]);
        SEL mpavControllerSel = NSSelectorFromString([NSString stringWithFormat:@"mp%@roller", @"avCont"]);
        if ([movieView respondsToSelector:mpavControllerSel]) {
            movieController = (UIViewController *)objc_msgSend(movieView, mpavControllerSel);
        }
    }
    @catch (NSException *exception) {
        PSPDFLogWarning(@"Failed to get movieController: %@", exception);
    }
#endif
    return movieController;
}

- (void)willShowPage:(NSUInteger)page {
    [self didShowPage:page];
}

- (BOOL)startPlayingYouTube {
    if (_isShowing) {
        // In iOS6, YouTube is now a webview before it's started.
        if (kCFCoreFoundationVersionNumber >= kCFCoreFoundationVersionNumber_iOS_6_0) {
            [self.webView stringByEvaluatingJavaScriptFromString:@"function pspdf_pollToPlay() {\
             var vph5 = document.getElementById(\"video-player-html5\");\
             if (vph5) {vph5.click();} else { setTimeout(pspdf_pollToPlay, 100); } } pspdf_pollToPlay();"];
        }else {
            // iOS5 has native YouTube code.
#ifndef _PSPDFKIT_DONT_USE_OBFUSCATED_PRIVATE_API_
            @try {
                UIView *youTubePluginView = PSPDFGetViewInsideView(self.webView, [NSString stringWithFormat:@"YouTub%@View", @"ePlugIn"]);
                UIButton *button = (UIButton *)PSPDFGetViewInsideView(youTubePluginView, NSStringFromClass([UIButton class]));
                [button sendActionsForControlEvents:UIControlEventTouchUpInside];
                return button != nil;
            }
            @catch (NSException *exception) {
                PSPDFLogWarning(@"Failed to invoke autostart on YouTube view: %@", exception);
                return NO;
            }
#endif
        }
    }
    return NO;
}

/// page is displayed
- (void)didShowPage:(NSUInteger)page {
    if (!_isShowing) {
        _isShowing = YES;

        // invoke the view generation as soon as the view will be added to the screen
        if (!self.webView && _setupWebView) {
            _setupWebView();
        }

#ifndef _PSPDFKIT_DONT_USE_OBFUSCATED_PRIVATE_API_
        if (kCFCoreFoundationVersionNumber < kCFCoreFoundationVersionNumber_iOS_6_0) {
            if (self.isAutoplayEnabled) {
                // autostart needs to be delayed as we wait for UIWebView to load. (very hacky, but only way as mp4 links are not exposed via YouTube)
                double delayInSeconds = PSPDFIsCrappyDevice() ? 0.9f : 0.5f;
                dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
                dispatch_after(popTime, dispatch_get_main_queue(), ^{
                    if (![self startPlayingYouTube]) {
                        // if this fails, try once more
                        dispatch_after(popTime, dispatch_get_main_queue(), ^{
                            [self startPlayingYouTube];
                        });
                    }
                });
            }
        }
        PSPDF_IF_PRE_IOS6([self updateiOS5YouTubePluginFrame:self.frame];)
        if (_wasPlaying) {
            [self play];
        }
#endif
    }
}

- (void)willHidePage:(NSUInteger)page {
    [self didHidePage:page];
}

/// page is hidden
- (void)didHidePage:(NSUInteger)page {
    if (_isShowing) {
        _isShowing = NO;

#ifndef _PSPDFKIT_DONT_USE_OBFUSCATED_PRIVATE_API_
        @try {
            UIViewController *movieController = [self movieController];
            if ([movieController respondsToSelector:NSSelectorFromString(@"isPlaying")]) {
                _wasPlaying = [[movieController valueForKey:@"isPlaying"] boolValue];
            }
            if (_wasPlaying) {
                [self pause];
            }
        }
        @catch (NSException *exception) {
            PSPDFLogWarning(@"Failed to pause YouTube view: %@", exception);
        }
#endif
    }
}

- (void)didChangePageFrame:(CGRect)frame {
    PSPDF_IF_PRE_IOS6([self updateiOS5YouTubePluginFrame:frame];)
}

// all this custom hacking, again. but we need to make the YouTube view to resize.
- (void)updateiOS5YouTubePluginFrame:(CGRect)frame {
#ifndef _PSPDFKIT_DONT_USE_OBFUSCATED_PRIVATE_API_
    CGRect targetRect = (CGRect){CGPointZero, self.frame.size};

    // we need to delay that... oh well
    dispatch_async(dispatch_get_main_queue(), ^{
        @try {
            UIView *webBrowserView = PSPDFGetViewInsideView(self.webView, @"UIWebB");
            NSLog(@"old: %@ new: %@", NSStringFromCGRect(webBrowserView.frame), NSStringFromCGRect(targetRect));
            webBrowserView.frame = targetRect;
            // perform async again - else we might get resized back
            dispatch_async(dispatch_get_main_queue(), ^{
                UIView *youTubeView = PSPDFGetViewInsideView(self.webView, @"YouTubeP");
                youTubeView.frame = targetRect;
            });
        }
        @catch (NSException *exception) {
            PSPDFLogWarning(@"Whoops, subview hacking failed. Won't resize view then. %@", exception);
        }
    });
#endif
}

// Danger! Only call within a @try @catch block!
- (void)pressiOS6PlayButton {
#ifndef _PSPDFKIT_DONT_USE_OBFUSCATED_PRIVATE_API_
    id transportControls = PSPDFGetViewInsideView(self.webView, [NSString stringWithFormat:@"%@%@TransportC", @"MP", @"Inline"]);
    UIButton *transportButton = (UIButton *)[transportControls valueForKey:[NSString stringWithFormat:@"%@playButton", @"_"]];
    [transportButton sendActionsForControlEvents:UIControlEventTouchUpInside];
#endif
}

- (void)play {
#ifndef _PSPDFKIT_DONT_USE_OBFUSCATED_PRIVATE_API_
    @try {
        if (kCFCoreFoundationVersionNumber >= kCFCoreFoundationVersionNumber_iOS_6_0) {
            [self pressiOS6PlayButton];
        }else {
            UIViewController *movieController = [self movieController];
            if ([movieController respondsToSelector:@selector(play)]) {
                [movieController performSelector:@selector(play)];
            }
        }
    }
    @catch (NSException *exception) {
        PSPDFLogWarning(@"Failed to play YouTube view: %@", exception);
    }
#endif
}

- (void)pause {
#ifndef _PSPDFKIT_DONT_USE_OBFUSCATED_PRIVATE_API_
    @try {
        // manually "press" the pause button.
        // directly using MPAVController doesn't seem to work (video is restarted after 0.3 seconds)
        if (kCFCoreFoundationVersionNumber >= kCFCoreFoundationVersionNumber_iOS_6_0) {
            [self pressiOS6PlayButton];
        }else {
            UIViewController *movieController = [self movieController];
            if ([movieController respondsToSelector:@selector(pause)]) {
                [movieController performSelector:@selector(pause)];
            }
        }
    }
    @catch (NSException *exception) {
        PSPDFLogWarning(@"Failed to pause YouTube view: %@", exception);
    }
#endif
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UIWebViewDelegate

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    if (kCFCoreFoundationVersionNumber >= kCFCoreFoundationVersionNumber_iOS_6_0) {
        if (self.isAutoplayEnabled) {
            [self startPlayingYouTube];
        }
    }
}

@end
