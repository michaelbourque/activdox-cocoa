//
//  PSPDFAnnotation.m
//  PSPDFKit
//
//  Copyright 2011-2012 Peter Steinberger. All rights reserved.
//

#import "UIColor+PSPDFKitAdditions.h"
#import "PSPDFKitGlobal+Internal.h"
#import "PSPDFAnnotation.h"
#import "PSPDFStream.h"
#import "PSPDFKit.h"

@implementation NSMutableData (PSPDFAdditions)

- (void)pspdf_appendDataString:(NSString *)dataString {
    [self appendData:[dataString dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES]];
}

@end

@interface PSPDFAnnotation() {
    CGRect _boundingBox;
    int _popupIndex;
    int _indexOnPage;
    float _alpha;
}
@property (nonatomic, assign) PSPDFDocument *document; // deprecation support
@property (nonatomic, assign) int indexOnPage;
@property (nonatomic, assign) int popupIndex;
@end

@implementation PSPDFAnnotation

NSString *const PSPDFAnnotationTypeStringLink = @"Link";
NSString *const PSPDFAnnotationTypeStringHighlight = @"Highlight";
NSString *const PSPDFAnnotationTypeStringUnderline = @"Underline";
NSString *const PSPDFAnnotationTypeStringStrikeout = @"StrikeOut";
NSString *const PSPDFAnnotationTypeStringNote = @"Text";
NSString *const PSPDFAnnotationTypeStringFreeText = @"FreeText";
NSString *const PSPDFAnnotationTypeStringInk = @"Ink";
NSString *const PSPDFAnnotationTypeStringSquare = @"Square";
NSString *const PSPDFAnnotationTypeStringCircle = @"Circle";
NSString *const PSPDFAnnotationTypeStringSignature = @"Signature";

@synthesize color = _color, fillColor = _fillColor, contents = _contents;

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Static

// override in subclass!
+ (NSArray *)supportedTypes {
    return @[];
}

+ (BOOL)isWriteable {
    return NO;
}

- (BOOL)isMovable {
    return [[self class] isWriteable];
}

NSString *PSPDFTypeStringFromAnnotationType(PSPDFAnnotationType annotationType) {
    switch (annotationType) {
        case PSPDFAnnotationTypeLink: return @"Link";
        case PSPDFAnnotationTypeHighlight: return PSPDFAnnotationTypeStringHighlight;
        case PSPDFAnnotationTypeText: return PSPDFAnnotationTypeStringFreeText;
        case PSPDFAnnotationTypeInk: return PSPDFAnnotationTypeStringInk;
        case PSPDFAnnotationTypeShape: return PSPDFAnnotationTypeStringSquare;
        case PSPDFAnnotationTypeLine: return @"Line";
        case PSPDFAnnotationTypeNote: return PSPDFAnnotationTypeStringNote;
        case PSPDFAnnotationTypeStamp: return @"Stamp";
        case PSPDFAnnotationTypeRichMedia: return @"RichMedia";
        case PSPDFAnnotationTypeScreen: return @"Screen";
        default: return nil;
    }
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (id)initWithType:(PSPDFAnnotationType)annotationType {
	if((self = [super init])) {
        // check for class usage to prevent user errors.
        if ([self class] == [PSPDFAnnotation class] && annotationType != PSPDFAnnotationTypeUndefined) {
            PSPDFLogError(@"PSPDFAnnotation is an abstract class. Use the subclasses to initialize a specific type or use PSPDFAnnotationTypeUndefined.");
            return nil;
        }
        _type = annotationType;
        _color = [[UIColor blackColor] pspdf_colorInRGBColorSpace]; // ensure correct color space
        _alpha = 1.0f;
        _contents = @"";
        _lineWidth = 1.0f;
        _boundingBox = CGRectZero;
        _indexOnPage = -1;
        _popupIndex = -1;
        _deleted = NO;
        _dirty = NO;
        _editable = [[self class] isWriteable];
        _typeString = PSPDFTypeStringFromAnnotationType(annotationType);
    }
	return self;
}

- (id)initWithAnnotationDictionary:(CGPDFDictionaryRef)annotDict inAnnotsArray:(CGPDFArrayRef)annotsArray {
    return [self initWithAnnotationDictionary:annotDict inAnnotsArray:annotsArray type:PSPDFAnnotationTypeUndefined];
}

- (id)initWithAnnotationDictionary:(CGPDFDictionaryRef)annotDict inAnnotsArray:(CGPDFArrayRef)annotsArray type:(PSPDFAnnotationType)annotationType {
    if ((self = [self initWithType:annotationType])) {

#ifdef DEBUG
        if (annotationType == PSPDFAnnotationTypeUndefined) {
            //            NSLog(@"dict: %@", PSPDFConvertPDFDictionary(annotDict));
        }
#endif

        /*
         PSPDFStream *appearanceStream = PSPDFDictionaryGetObjectForPath(annotDict, @"AP.N");
         if ([appearanceStream isKindOfClass:[PSPDFStream class]]) {
         NSDictionary *appearanceDict = appearanceStream.dictionary;
         NSLog(@"%@", appearanceDict);
         }*/

        if (annotDict) {
            _typeString = PSPDFDictionaryGetString(annotDict, @"Subtype");
        }else {
            _typeString = PSPDFTypeStringFromAnnotationType(annotationType);
        }

        // Text that shall be displayed for the annotation or, if this type of annotation does not display text, an alternate description of the annotation’s contents in human-readable form. In either case, this text is useful when extracting the document’s contents in support of accessibility to users with disabilities or for other purposes
        _contents = PSPDFDictionaryGetString(annotDict, @"Contents") ?: @"";
        _name = PSPDFDictionaryGetString(annotDict, @"NM");
        _lastModified = PSPDFDateFromString(PSPDFDictionaryGetString(annotDict, @"M"));

        // user
        _user = PSPDFDictionaryGetString(annotDict, @"T");

        // parse coordinates
        if (annotDict) {
            CGPDFArrayRef rectArray = NULL;
            CGPDFDictionaryGetArray(annotDict, "Rect", &rectArray);
            if (rectArray != NULL) {
                _boundingBox = [self rectFromPDFArray:rectArray];
            }

            // parse color and alpha value
            (self.color = [[UIColor alloc] initWithCGPDFDictionary:annotDict]);
            CGPDFDictionaryGetNumber(annotDict, "CA", &_alpha);
            if (_alpha == 0.0) _alpha = 1.0;

            // parse fill color. Does not have an alpha value.
            CGPDFArrayRef fillColorArrayRef = NULL;
            if (CGPDFDictionaryGetArray(annotDict, "IC", &fillColorArrayRef)) {
                self.fillColor = [[UIColor alloc] initWithCGPDFArray:fillColorArrayRef];
            }
        }

        // parse popupIndex (Highlight, Ink, Text)
        if (annotsArray) {
            CGPDFDictionaryRef popupDict = NULL;
            CGPDFDictionaryGetDictionary(annotDict, "Popup", &popupDict);
            if (popupDict != NULL) {
                for (int i=0; i<CGPDFArrayGetCount(annotsArray); i++) {
                    CGPDFDictionaryRef annot = NULL;
                    CGPDFArrayGetDictionary(annotsArray, i, &annot);
                    if (annot == popupDict) {
                        _popupIndex = i;
                        break;
                    }
                }
            }
        }else {
            _popupIndex = -1;
        }

        // parse extended BS border
        // (PDF 1.2) The dictionaries for some annotation types (such as free text and polygon annotations) can include the BS entry. That entry specifies a border style dictionary that has more settings than the array specified for the Border entry. If an annotation dictionary includes the BS entry, then the Border entry is ignored.
        CGPDFDictionaryRef borderStyleDict = NULL;
        bool hasBorderStyle = CGPDFDictionaryGetDictionary(annotDict, "BS", &borderStyleDict);
        if (hasBorderStyle) {
            CGPDFDictionaryGetNumber(borderStyleDict, "W", &_lineWidth);

            NSString *borderStyle = PSPDFDictionaryGetString(borderStyleDict, @"S");
            if (borderStyle == nil) {
                _borderStyle = PSPDFAnnotationBorderStyleNone;
            }else if ([borderStyle isEqualToString:@"S"]) {
                _borderStyle = PSPDFAnnotationBorderStyleSolid;
            }else if ([borderStyle isEqualToString:@"D"]) {
                _borderStyle = PSPDFAnnotationBorderStyleDashed;
                NSArray *dashArray = PSPDFDictionaryGetObjectForPath(annotDict, @"D");
                // only allow NSNumbers
                if ([dashArray isKindOfClass:[NSArray class]]) {
                    _dashArray = [dashArray filteredArrayUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(id evaluatedObject, NSDictionary *bindings) {
                        return [evaluatedObject isKindOfClass:[NSNumber class]];
                    }]];
                }
            }else if ([borderStyle isEqualToString:@"B"]) {
                _borderStyle = PSPDFAnnotationBorderStyleBelved;
            }else if ([borderStyle isEqualToString:@"I"]) {
                _borderStyle = PSPDFAnnotationBorderStyleInset;
            }else if ([borderStyle isEqualToString:@"U"]) {
                _borderStyle = PSPDFAnnotationBorderStyleUnderline;
            }else {
                _borderStyle = PSPDFAnnotationBorderStyleUnknown;
            }
        } else {
            _lineWidth = 1.0;
        }
    }
    return self;
}

- (NSString *)description {
    NSMutableString *description = [NSMutableString stringWithFormat:@"<%@ type:%d(%@)", NSStringFromClass([self class]), self.type, self.typeString];

    if (self.isDirty) {
        [description appendFormat:@" DIRTY"];
    }
    if (self.isDeleted) {
        [description appendFormat:@" DELETED"];
    }
    if ([self.contents length]) {
        [description appendFormat:@" contents:%@", self.contents];
    }
    if ([self.name length]) {
        [description appendFormat:@" name:%@", self.name];
    }
    if (self.indexOnPage >= 0) {
        [description appendFormat:@" indexOnPage:%d", self.indexOnPage];
    }else {
        [description appendFormat:@" indexOnPage:<NONE YET - NOT SAVED>"];
    }
    if (self.popupIndex > 0) {
        [description appendFormat:@" popupIndex:%d", self.popupIndex];
    }
    if (!CGRectEqualToRect(self.boundingBox, CGRectZero)) {
        [description appendFormat:@" boundingBox:%@", NSStringFromCGRect(self.boundingBox)];
    }
    if (self.alpha < 1) {
        [description appendFormat:@" alpha:%.1f", self.alpha];
    }
    if (self.color) {
        [description appendFormat:@" color:%@ (%@)", [[self.color pspdf_closestAnnotationColorName] capitalizedString], self.color];
    }
    if (self.fillColor) {
        [description appendFormat:@" fillColor:%@ (%@)", [[self.fillColor pspdf_closestAnnotationColorName] capitalizedString], self.fillColor];
    }
    if (self.lineWidth > 0) {
        [description appendFormat:@" lineWidth:%.1f", self.lineWidth];
    }
    if ([self.user length]) {
        [description appendFormat:@" user %@", self.user];
    }
    if (self.lastModified) {
        [description appendFormat:@" lastmodified: %@", [self.lastModified description]];
    }
    if (self.page != NSUIntegerMax) {
        [description appendFormat:@" page:%d", self.page];
    }
    if (self.document) {
        [description appendFormat:@" document: %@", self.document.UID];
    }

    return [description copy];
}

- (NSUInteger)hash {
    // http://stackoverflow.com/questions/254281/best-practices-for-overriding-isequal-and-hash/254380#254380
    return (((_type * 31 + _page) * 31) + (_boundingBox.origin.x + _boundingBox.size.width) * 31) + (_boundingBox.origin.y + _boundingBox.size.height);
}

- (BOOL)isEqual:(id)other {
    if ([other isKindOfClass:[self class]]) {
        return [self isEqualToAnnotation:(PSPDFAnnotation *)other];
    }
    return NO;
}

- (BOOL)isEqualToAnnotation:(PSPDFAnnotation *)otherAnnotation {
    if (_documentProvider != otherAnnotation.documentProvider) return NO;

    if (_page == otherAnnotation.page && CGRectEqualToRect(_boundingBox, otherAnnotation.boundingBox) && _type == otherAnnotation.type) {
        return YES;
    }
    return NO;
}

- (instancetype)copyAndDeleteOriginalIfNeeded {
    if (!self.document) PSPDFLogWarning(@"Document is not set for annotation %@!", self);

    PSPDFAnnotation *newAnnotation = self;
    if (self.indexOnPage >= 0) {
        newAnnotation = [self copy];
        newAnnotation.indexOnPage = -1;
        self.deleted = YES;

        [[NSNotificationCenter defaultCenter] postNotificationName:PSPDFAnnotationChangedNotification object:self userInfo:@{PSPDFAnnotationChangedNotificationKeyPathKey : @[NSStringFromSelector(@selector(isDeleted))]}];

        [self.document addAnnotations:@[newAnnotation] forPage:_page];
    }
    return newAnnotation;
}

- (void)parse {
    // subclass
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public / PDF Helpers

- (NSArray *)rectsFromQuadPointsInArray:(CGPDFArrayRef)quadPointsArray {
	if (CGPDFArrayGetCount(quadPointsArray) % 8 != 0) {
		PSPDFLogWarning(@"Invalid quad point array.");
		return @[];
	}
	NSMutableArray *quadPointRects = [NSMutableArray array];
	for (int i=0; i<CGPDFArrayGetCount(quadPointsArray); i+=8) {
		float x1, y1, x2, y2, x3, y3, x4, y4;
		CGPDFArrayGetNumber(quadPointsArray, i, &x1);
		CGPDFArrayGetNumber(quadPointsArray, i+1, &y1);
		CGPDFArrayGetNumber(quadPointsArray, i+2, &x2);
		CGPDFArrayGetNumber(quadPointsArray, i+3, &y2);
		CGPDFArrayGetNumber(quadPointsArray, i+4, &x3);
		CGPDFArrayGetNumber(quadPointsArray, i+5, &y3);
		CGPDFArrayGetNumber(quadPointsArray, i+6, &x4);
		CGPDFArrayGetNumber(quadPointsArray, i+7, &y4);
		CGRect rect = CGRectMake(MIN(x2,x3), MIN(y2,y3), MAX(x1,x4)-MIN(x3,x2), (MAX(y1,y4)-MIN(y2, y3)));
		[quadPointRects addObject:[NSValue valueWithCGRect:rect]];
	}
	return [quadPointRects copy];
}

+ (NSArray *)stringsFromRectsArray:(NSArray *)rects {
    NSMutableArray *rectStrings = [NSMutableArray array];
	for (NSValue *rectValue in rects) {
		NSString *rectString = NSStringFromCGRect([rectValue CGRectValue]);
		if (rectString) [rectStrings addObject:rectString];
	}
    return [rectStrings copy];
}

+ (NSArray *)rectsFromStringsArray:(NSArray *)rectStrings {
    NSMutableArray *rectValues = [[NSMutableArray alloc] init];
    for (NSString *rectString in rectStrings) {
        CGRect rect = CGRectFromString(rectString);
        NSValue *rectValue = [NSValue valueWithCGRect:rect];
        if (rectValue) [rectValues addObject:rectValue];
    }
    return [rectValues copy];
}

- (CGRect)rectFromPDFArray:(CGPDFArrayRef)array {
	if (CGPDFArrayGetCount(array) != 4) {
		PSPDFLogWarning(@"Invalid rect array (expected 4 elements, got %zd)", CGPDFArrayGetCount(array));
		return CGRectNull;
	}
	float lowerLeftX, lowerLeftY, upperRightX, upperRightY;
	CGPDFArrayGetNumber(array, 0, &lowerLeftX);
	CGPDFArrayGetNumber(array, 1, &lowerLeftY);
	CGPDFArrayGetNumber(array, 2, &upperRightX);
	CGPDFArrayGetNumber(array, 3, &upperRightY);
	CGRect rect = CGRectMake(lowerLeftX, lowerLeftY, upperRightX - lowerLeftX, upperRightY - lowerLeftY);
	return PSPDFNormalizeRect(rect);
}

- (NSData *)pdfDataRepresentation {
	return [NSData data];
}

- (NSString *)pdfRectString {
    float lowerLeftX = CGRectGetMinX(_boundingBox);
	float lowerLeftY = CGRectGetMinY(_boundingBox);
	float upperRightX = CGRectGetMaxX(_boundingBox);
	float upperRightY = CGRectGetMaxY(_boundingBox);
    return [NSString stringWithFormat:@"/Rect [%f %f %f %f]", lowerLeftX, lowerLeftY, upperRightX, upperRightY];
}

// transform PDF color to string
static NSString *PSPDFStringColorRepresentation(UIColor *color) {
    const CGFloat *components = CGColorGetComponents(color.CGColor);
    return [NSString stringWithFormat:@"[%f %f %f]", components[0], components[1], components[2]];
}

- (NSString *)pdfColorString {
    return [NSString stringWithFormat:@"/C %@", PSPDFStringColorRepresentation(self.color)];
}

- (NSString *)pdfColorWithAlphaString {
    if (_alpha != 1.0) {
        const CGFloat *color = CGColorGetComponents(self.color.CGColor);
        return [NSString stringWithFormat:@"/C [%f %f %f] /CA %f", color[0], color[1], color[2], _alpha];
    }else {
        return [self pdfColorString];
    }
}

- (NSString *)pdfFillColorString {
    return self.fillColor ? [NSString stringWithFormat:@"/IC %@", PSPDFStringColorRepresentation(self.fillColor)] : @"";
}

static NSString *PSPDFEspcapedString(NSString *string) {
    // TODO: Escape parentheses properly (dirty workaround now: replace them with unicode characters that look similar...)!
    NSMutableString *escapedText = [string mutableCopy];
    [escapedText replaceOccurrencesOfString:@"(" withString:@"（" options:0 range:NSMakeRange(0, [escapedText length])];
    [escapedText replaceOccurrencesOfString:@")" withString:@"）" options:0 range:NSMakeRange(0, [escapedText length])];
    return escapedText;
}

// appends the contents text that can be in any PDF annotation.
- (void)appendEscapedContents:(NSMutableData *)pdfData {
    uint8_t bom1 = 254;
    uint8_t bom2 = 255;

    if ([self.contents length]) {
        [pdfData pspdf_appendDataString:@"/Contents ("];
        [pdfData appendBytes:&bom1 length:1];
        [pdfData appendBytes:&bom2 length:1];
        NSString *escapedText = PSPDFEspcapedString(self.contents);
        [pdfData appendData:[escapedText dataUsingEncoding:NSUTF16BigEndianStringEncoding allowLossyConversion:YES]];
        [pdfData pspdf_appendDataString:@")"];
    }

    // add user
    if ([self.user length]) {
        [pdfData pspdf_appendDataString:@"/T ("];
        [pdfData appendBytes:&bom1 length:1];
        [pdfData appendBytes:&bom2 length:1];
        NSString *escapedText = PSPDFEspcapedString(self.user);
        [pdfData appendData:[escapedText dataUsingEncoding:NSUTF16BigEndianStringEncoding allowLossyConversion:YES]];
        [pdfData pspdf_appendDataString:@")"];
    }

    // add annotation name
    if ([self.name length]) {
        [pdfData pspdf_appendDataString:@"/NM ("];
        [pdfData appendBytes:&bom1 length:1];
        [pdfData appendBytes:&bom2 length:1];
        NSString *escapedText = PSPDFEspcapedString(self.name);
        [pdfData appendData:[escapedText dataUsingEncoding:NSUTF16BigEndianStringEncoding allowLossyConversion:YES]];
        [pdfData pspdf_appendDataString:@")"];
    }

    // add last changed
    if (self.lastModified) {
       [pdfData pspdf_appendDataString:[NSString stringWithFormat:@"/M(%@)", PSPDFStringFromDate(self.lastModified)]];
    }
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public

- (NSUInteger)absolutePage {
    return self.page + [self.documentProvider.document pageOffsetForDocumentProvider:self.documentProvider];
}

- (void)prepareBorderStyleInContext:(CGContextRef)context {
    CGContextSetStrokeColorWithColor(context, self.colorWithAlpha.CGColor);

    if (self.borderStyle == PSPDFAnnotationBorderStyleDashed) {
        CGFloat *dashes = malloc(fminf([self.dashArray count], 2) * sizeof(CGFloat));
        if ([self.dashArray count] == 0) {
            dashes[0] = 3; dashes[1] = 3;
        }else {
            [self.dashArray enumerateObjectsUsingBlock:^(NSNumber *number, NSUInteger idx, BOOL *stop) {
                dashes[idx] = [number floatValue];
            }];
        }
        CGContextSetLineDash(context, 0.0, dashes, 2);
        free(dashes);
    }
    CGContextSetLineWidth(context, self.lineWidth);
}

// shape handle borders specially (ellipse...)
- (void)drawBorderInContext:(CGContextRef)context {
    if (self.borderStyle != PSPDFAnnotationBorderStyleNone) {
        CGContextSaveGState(context);
        [self prepareBorderStyleInContext:context];
        CGContextStrokeRect(context, self.boundingBox);
        CGContextRestoreGState(context);
    }
}

- (void)drawInContext:(CGContextRef)context {
    if (self.isDeleted) return;
    [self drawBorderInContext:context];
}

- (CGRect)rectForPageRect:(CGRect)pageRect {
    PSPDFPageInfo *pageInfo = [_documentProvider pageInfoForPage:_page];
    CGRect rect = PSPDFConvertPDFRectToViewRect(_boundingBox, pageInfo.pageRect, pageInfo.pageRotation, pageRect);

    // sanity check - rect can't be larger than the page.
    rect = CGRectMake(fmaxf(rect.origin.x, 0), fmaxf(rect.origin.y, 0), fminf(rect.size.width, pageRect.size.width), fminf(rect.size.height, pageRect.size.height));

    return rect;
}

- (BOOL)hitTest:(CGPoint)point {
    if ((_boundingBox.origin.x <= point.x) &&
        (_boundingBox.origin.y <= point.y) &&
        (point.x <= _boundingBox.origin.x + _boundingBox.size.width) &&
        (point.y <= _boundingBox.origin.y + _boundingBox.size.height)) {
        return YES;
    } else {
        return NO;
    }
}

- (NSComparisonResult)compareByPositionOnPage:(PSPDFAnnotation *)otherAnnotation {
	CGPoint origin1 = CGPointMake(_boundingBox.origin.x, _boundingBox.origin.y + _boundingBox.size.height);
	CGPoint origin2 = CGPointMake(otherAnnotation.boundingBox.origin.x, otherAnnotation.boundingBox.origin.y + otherAnnotation.boundingBox.size.height);
	if (origin2.y < origin1.y) return NSOrderedAscending;
	if (origin2.y > origin1.y) return NSOrderedDescending;

	if (origin2.x < origin1.x) return NSOrderedAscending;
	if (origin2.x > origin1.x) return NSOrderedDescending;
	return NSOrderedSame;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Properties

- (UIColor *)colorWithAlpha { return [self.color colorWithAlphaComponent:self.alpha]; }
- (UIColor *)color { return [_color pspdf_colorInRGBColorSpace]; }
- (void)setColor:(UIColor *)color {
    if (color != _color) {
        _color = [[color pspdf_colorInRGBColorSpace] colorWithAlphaComponent:1.f];
        self.dirty = YES;
    }
}

- (UIColor *)fillColorWithAlpha { return [self.fillColor colorWithAlphaComponent:self.alpha]; }
- (UIColor *)fillColor { return [_fillColor pspdf_colorInRGBColorSpace]; }
- (void)setFillColor:(UIColor *)fillColor {
    if (fillColor != _fillColor) {
        _fillColor = [[fillColor pspdf_colorInRGBColorSpace] colorWithAlphaComponent:1.f];
        self.dirty = YES;
    }
}

- (void)setBoundingBox:(CGRect)boundingBox {
    if (!CGRectEqualToRect(boundingBox, _boundingBox)) {
        _boundingBox = boundingBox;
        self.dirty = YES;
    }
}

- (void)setAlpha:(float)alpha {
    if (alpha != _alpha) {
        _alpha = alpha;
        self.dirty = YES;
    }
}

- (NSString *)contents { return PSPDFAtomicAutoreleasedGet(_contents); }
- (void)setContents:(NSString *)contents {
    if (contents != _contents) {
        PSPDFAtomicCopiedSetToFrom(_contents, contents);
        self.dirty = YES;
    }
}

- (void)setName:(NSString *)name {
    if (name != _name) {
        _name = name;
        self.dirty = YES;
    }
}

- (void)setLineWidth:(float)lineWidth {
    if (lineWidth != _lineWidth) {
        _lineWidth = lineWidth;
        self.dirty = YES;
    }
}

- (void)setDeleted:(BOOL)deleted {
    if (deleted != _deleted) {
        _deleted = deleted;
        self.dirty = YES;
    }
}

- (void)setDirty:(BOOL)dirty {
    if (dirty != _dirty) {
        _dirty = dirty;
        self.lastModified = [NSDate date];
    }
}

- (void)setDocumentProvider:(PSPDFDocumentProvider *)documentProvider {
    if (documentProvider != _documentProvider) {
        _documentProvider = documentProvider;
        self.rotation = [documentProvider pageInfoForPage:self.page].pageRotation;
    }
}

- (PSPDFDocument *)document { return self.documentProvider.document; }

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSCoding

- (id)initWithCoder:(NSCoder *)decoder {
	if((self = [self initWithType:PSPDFAnnotationTypeUndefined])) {
        _type = [decoder decodeIntegerForKey:NSStringFromSelector(@selector(type))];
        _typeString = [decoder decodeObjectForKey:NSStringFromSelector(@selector(typeString))];
        _boundingBox = [decoder decodeCGRectForKey:NSStringFromSelector(@selector(boundingBox))];
        _indexOnPage = [decoder decodeIntForKey:NSStringFromSelector(@selector(indexOnPage))];
        _popupIndex = [decoder decodeIntForKey:NSStringFromSelector(@selector(popupIndex))];
        _page = [decoder decodeIntegerForKey:NSStringFromSelector(@selector(page))];
        if ([decoder containsValueForKey:@"deleted"]) {
            _deleted = [decoder decodeBoolForKey:@"deleted"]; // deprecated format support
        }else {
            _deleted = [decoder decodeBoolForKey:NSStringFromSelector(@selector(isDeleted))];
        }
        _color = [[decoder decodeObjectForKey:NSStringFromSelector(@selector(color))] pspdf_colorInRGBColorSpace];
        _alpha = [decoder decodeFloatForKey:NSStringFromSelector(@selector(alpha))];
        _fillColor = [[decoder decodeObjectForKey:NSStringFromSelector(@selector(fillColor))] pspdf_colorInRGBColorSpace];
        _contents = [decoder decodeObjectForKey:NSStringFromSelector(@selector(contents))];
        _name = [decoder decodeObjectForKey:NSStringFromSelector(@selector(name))];
        _lineWidth = [decoder decodeFloatForKey:NSStringFromSelector(@selector(lineWidth))];
        _borderStyle = [decoder decodeIntegerForKey:NSStringFromSelector(@selector(borderStyle))];
        _dashArray = [decoder decodeObjectForKey:NSStringFromSelector(@selector(dashArray))];
        _lastModified = [decoder decodeObjectForKey:NSStringFromSelector(@selector(lastModified))]; // added in PSPDFKit 2.5.5
    }
	return self;
}

- (void)encodeWithCoder:(NSCoder *)coder {
    [coder encodeInteger:_type forKey:NSStringFromSelector(@selector(type))];
    [coder encodeObject:_typeString forKey:NSStringFromSelector(@selector(typeString))];
	[coder encodeCGRect:_boundingBox forKey:NSStringFromSelector(@selector(boundingBox))];
	[coder encodeInt:_indexOnPage forKey:NSStringFromSelector(@selector(indexOnPage))];
	[coder encodeInt:_popupIndex forKey:NSStringFromSelector(@selector(popupIndex))];
    [coder encodeInteger:_page forKey:NSStringFromSelector(@selector(page))];
	[coder encodeBool:_deleted forKey:NSStringFromSelector(@selector(isDeleted))];
	[coder encodeObject:_color forKey:NSStringFromSelector(@selector(color))];
	[coder encodeFloat:_alpha forKey:NSStringFromSelector(@selector(alpha))];
	[coder encodeObject:_fillColor forKey:NSStringFromSelector(@selector(fillColor))];
    [coder encodeObject:_contents forKey:NSStringFromSelector(@selector(contents))];
    if (_name) [coder encodeObject:_name forKey:NSStringFromSelector(@selector(name))];
    [coder encodeFloat:_lineWidth forKey:NSStringFromSelector(@selector(lineWidth))];
    [coder encodeInteger:_borderStyle forKey:NSStringFromSelector(@selector(borderStyle))];
    [coder encodeObject:_dashArray forKey:NSStringFromSelector(@selector(dashArray))];
    [coder encodeObject:_lastModified forKey:NSStringFromSelector(@selector(lastModified))];
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSCopying

- (id)copyWithZone:(NSZone *)zone {
    PSPDFAnnotation *annotation = [[[self class] alloc] initWithType:self.type];
    annotation->_typeString = self.typeString;
    annotation->_color = self.color;
    annotation->_alpha = self.alpha;
    annotation->_fillColor = self.fillColor;
    annotation->_contents = self.contents;
    annotation->_name = self.name;
    annotation->_lineWidth = self.lineWidth;
    annotation->_indexOnPage = self.indexOnPage;
    annotation->_popupIndex = self.popupIndex;
    annotation->_boundingBox = self.boundingBox;
    annotation->_page = self.page;
    annotation->_documentProvider = self.documentProvider;
    annotation->_rotation = self.rotation;
    annotation->_borderStyle = self.borderStyle;
    annotation->_dashArray = self.dashArray;
    annotation->_overlay = self.isOverlay; // state only, don't persist.
    annotation->_lastModified = self.lastModified;
    return annotation;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Deprecated

- (void)setDocument:(PSPDFDocument *)document {
    self.documentProvider = [document documentProviderForPage:self.page];
}

@end
