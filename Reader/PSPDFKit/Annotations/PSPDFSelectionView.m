//
//  PSPDFSelectionView.m
//  PSPDFKit
//
//  Copyright (c) 2012 Peter Steinberger. All rights reserved.
//

#import "PSPDFSelectionView.h"
#import "PSPDFConverter.h"
#import "UIColor+PSPDFKitAdditions.h"
#import <QuartzCore/QuartzCore.h>

@interface PSPDFSelectionView () {
    BOOL _acceptedStartPoint;
    UIView *_selectionRectView;
    CGPoint _touchStartPoint;
    CGRect *_rawRects;
    NSUInteger _rawRectCount;
}
@end

@implementation PSPDFSelectionView

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (id)initWithFrame:(CGRect)frame delegate:(id<PSPDFSelectionViewDelegate>)delegate {
    if ((self = [super initWithFrame:frame])) {
        self.opaque = NO;
        _delegate = delegate;
        _wordSelectionColor = [[UIColor pspdf_selectionColor] colorWithAlphaComponent:0.3f];
        _selectionColor = [[UIColor pspdf_selectionColor] colorWithAlphaComponent:[UIColor pspdf_selectionAlpha]];
        _selectionRectView = [[UIView alloc] initWithFrame:CGRectZero];
        _selectionRectView.backgroundColor = _selectionColor;
        _selectionRectView.opaque = NO;
        _selectionRectView.hidden = YES;
        [self addSubview:_selectionRectView];

        // iOS6 feature
        if ([self.layer respondsToSelector:@selector(setDrawsAsynchronously:)]) {
            self.layer.drawsAsynchronously = YES;
        }
    }
    return self;
}

- (void)dealloc {
    free(_rawRects);
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UIView

// might be called from a thread (drawsAsynchronously)
- (void)drawRect:(CGRect)rect {
    CGContextRef context = UIGraphicsGetCurrentContext();

    [_wordSelectionColor setFill];
    @synchronized(self) {
        if (self.rects) {
            for (NSValue *rectValue in self.rects) {
                CGRect fillRect = [rectValue CGRectValue];
                CGContextFillRect(context, fillRect);
            }
        }else {
            // fast path!
            for (int i=0; i<_rawRectCount; i++) {
                CGContextFillRect(context, _rawRects[i]);
            }
        }
    }
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public

- (void)setSelectionColor:(UIColor *)selectionColor {
    if (selectionColor != _selectionColor) {
        _selectionColor = selectionColor;
        _selectionRectView.backgroundColor = selectionColor;
        [self setNeedsDisplay];
    }
}

- (void)setWordSelectionColor:(UIColor *)wordSelectionColor {
    if (_wordSelectionColor != wordSelectionColor) {
        _wordSelectionColor = wordSelectionColor;
        [self setNeedsDisplay];
    }
}

- (void)setRects:(NSArray *)rects {
    @synchronized(self) {
        if (rects != _rects) {
            _rects = rects; // not doing copy here for performance reasons
        }
        [self setRawRects:NULL count:0];
    }
    [self setNeedsDisplay];
}

- (void)setRawRects:(CGRect *)rawRects count:(NSUInteger)count {
    @synchronized(self) {
        if (_rawRects) free(_rawRects);
        _rawRects = rawRects;
        _rawRectCount = count;
        if (rawRects) _rects = nil;
    }
    [self setNeedsDisplay];
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Touch Handling

// calculates selected text between start point and end point
- (void)updateSelectionForEndPoint:(CGPoint)endPoint {
    CGRect unionRect = PSPDFCGRectFromPoints(_touchStartPoint, endPoint);
    _selectionRectView.frame = unionRect;
    _selectionRectView.hidden = NO;

    if ([self.delegate respondsToSelector:@selector(selectionView:updateSelectedRect:)]) {
        [self.delegate selectionView:self updateSelectedRect:unionRect];
    }
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
	UITouch *touch = [touches anyObject];
	CGPoint location = [touch locationInView:self];
    _touchStartPoint = location;

    _acceptedStartPoint = YES;
    if ([self.delegate respondsToSelector:@selector(selectionView:shouldStartSelectionAtPoint:)]) {
        _acceptedStartPoint = [self.delegate selectionView:self shouldStartSelectionAtPoint:location];
    }
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    if (!_acceptedStartPoint) return;

	UITouch *touch = [touches anyObject];
	CGPoint location = [touch locationInView:self];
    [self updateSelectionForEndPoint:location];
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    if (!_acceptedStartPoint) return;

    _selectionRectView.hidden = YES;
	UITouch *touch = [touches anyObject];
	CGPoint location = [touch locationInView:self];
    CGRect unionRect = PSPDFCGRectFromPoints(_touchStartPoint, location);

    if ([self.delegate respondsToSelector:@selector(selectionView:finishedWithSelectedRect:)]) {
        [self.delegate selectionView:self finishedWithSelectedRect:unionRect];
    }
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event {
    if (!_acceptedStartPoint) return;

    _selectionRectView.hidden = YES;
    UITouch *touch = [touches anyObject];
	CGPoint location = [touch locationInView:self];
    CGRect unionRect = PSPDFCGRectFromPoints(_touchStartPoint, location);

    if ([self.delegate respondsToSelector:@selector(selectionView:cancelledWithSelectedRect:)]) {
        [self.delegate selectionView:self cancelledWithSelectedRect:unionRect];
    }
}

@end
