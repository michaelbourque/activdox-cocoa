//
//  PSPDFGenericAnnotationView.m
//  PSPDFKit
//
//  Copyright (c) 2012 Peter Steinberger. All rights reserved.
//

#import "PSPDFKitGlobal+Internal.h"
#import "PSPDFGenericAnnotationView.h"
#import "PSPDFAnnotation.h"
#import "PSPDFPageRenderer.h"
#import "PSPDFDocument.h"
#import "PSPDFAnnotationParser.h"
#import "PSPDFRenderQueue.h"
#import <QuartzCore/QuartzCore.h>

@implementation PSPDFGenericAnnotationView

@synthesize annotation = _annotation;

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (id)initWithFrame:(CGRect)frame {
    if ((self = [super initWithFrame:frame])) {
        self.backgroundColor = [UIColor clearColor];
        self.clipsToBounds = NO;
        self.opaque = NO;

        // listen for annotation changes
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(annotationsChanged:) name:PSPDFAnnotationChangedNotification object:nil];
    }
    return self;
}

- (id)initWithAnnotation:(PSPDFAnnotation *)annotation {
	if((self = [self initWithFrame:CGRectZero])) {
        self.annotation = annotation;
    }
	return self;
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (NSString *)description {
    return [NSString stringWithFormat:@"%@ annotation:%@>", [super description], self.annotation];
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UIView

- (void)setFrame:(CGRect)frame {
    CGRect oldFrame = self.frame;
    [super setFrame:frame];

    // we only need to redraw if the size changes.
    if (!CGSizeEqualToSize(oldFrame.size, frame.size)) {
        [self setNeedsDisplay];
    }
}

/*
 // might be called async
 - (void)drawRect:(CGRect)rect {
 CGContextRef context = UIGraphicsGetCurrentContext();

 PSPDFAnnotation *annotation = self.annotation;
 [annotation.document renderPage:annotation.absolutePage inContext:context withSize:_superviewSize clippedToRect:_selfFrame withAnnotations:@[annotation] options:@{kPSPDFDisablePageRendering : @YES, kPSPDFRenderOverlayAnnotations : @YES, kPSPDFIgnorePageClip : @YES}];
 }
 */
///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public

// setting a new annotation needs an updated view
- (PSPDFAnnotation *)annotation { return PSPDFAtomicAutoreleasedGet(_annotation); }
- (void)setAnnotation:(PSPDFAnnotation *)annotation {
    if (annotation != _annotation) {
        PSPDFAtomicRetainedSetToFrom(_annotation, annotation);
        [self setNeedsDisplay];
    }
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Private

// listen for annotation changes
- (void)annotationsChanged:(NSNotification *)notification {
    PSPDFAnnotation *annotation = notification.object;
    PSPDFAnnotation *originalAnnotation = notification.userInfo[PSPDFAnnotationChangedNotificationOriginalAnnotationKey];
    NSArray *keyPaths = notification.userInfo[PSPDFAnnotationChangedNotificationKeyPathKey];

    if (self.annotation == annotation) {
        // only redraw if something other than the boundingBox changed (dragging)
        if (![keyPaths containsObject:NSStringFromSelector(@selector(boundingBox))]) {
            [self setNeedsDisplay];
        }
    }else if (self.annotation == originalAnnotation) {
        self.annotation = annotation;
    }
}

@end
