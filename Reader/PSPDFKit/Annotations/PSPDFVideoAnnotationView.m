//
//  PSPDFVideoAnnotationView.m
//  PSPDFKit
//
//  Copyright (c) 2011-2012 Peter Steinberger. All rights reserved.
//

#import "PSPDFKitGlobal.h"
#import "PSPDFVideoAnnotationView.h"
#import "PSPDFAnnotation.h"
#import "PSPDFViewController+Internal.h"
#import "PSPDFDocument.h"
#import "PSPDFLinkAnnotation.h"
#import "UIImage+PSPDFKitAdditions.h"
#import "PSPDFConverter.h"

@interface PSPDFVideoAnnotationView () {
    PSPDFVideoAnnotationCoverView *_coverView;
    BOOL _coverViewPressed:1;
    BOOL _isShowing:1;
    BOOL _wasPlaying:1;
    BOOL _autoPlayEnabledHandled:1;
}
@end

@implementation PSPDFVideoAnnotationView

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (id)initWithFrame:(CGRect)frame {
    if ((self = [super initWithFrame:frame])) {
    }
    return self;
}

- (void)dealloc {
    [self deregisterNotifications];
    [_player stop];
    [_player.view removeFromSuperview];
    // Sometimes throws KVODeallocate errors with presentationSize and nonForcedSubtitleDisplayEnabled
    // Seems like a non-critical bug in iOS4/5.
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Private

- (void)fullscreenGesture:(UIPinchGestureRecognizer *)gesture {
    if (!self.linkAnnotation.controlsEnabled) {
        if (_player.playbackState == MPMoviePlaybackStatePlaying) {
            // velocity > 0 = pinch out
            if (_player.isFullscreen && gesture.velocity > 0) return;
            if (!_player.isFullscreen && gesture.velocity < 0) return;

            [_player setFullscreen:!_player.isFullscreen animated:YES];
        }
    }
}

- (void)videoTapped:(UITapGestureRecognizer *)gesture {
        if (_player.playbackState == MPMoviePlaybackStatePlaying) {
            [_player pause];
        }else {
            [_player play];
        }
}

- (void)videoDoubleTapped:(UITapGestureRecognizer *)gesture {
    if (_player.playbackState == MPMoviePlaybackStatePlaying) {
        [_player setFullscreen:!_player.isFullscreen animated:YES];
    }
}

// try to find the root view. With a cheap fallback if the rootViewController (wrongly) isn't set.
- (UIView *)rootView {
    if (!self.window.rootViewController) {
        PSPDFLogWarning(@"Cannot find the rootViewController. Are you using the deprecated method of using addSubview to add yout window to the rootViewController? This is wrong, please change this to the iOS4+ way of doing it. (See your AppDelegate)");
    }
    UIView *rootView = self.window.rootViewController.view;
    if (!rootView && [self.window.subviews count] == 1) {
        rootView = [self.window.subviews lastObject];
    }
    // if rootView isn't what we want (no window!) query the view hierarchy up.
    if (!rootView.window) {
        rootView = self.superview;
        while (rootView.superview) {
            rootView = rootView.superview;
        }
    }
    return rootView;
}

- (void)updateFullscreenPlayerFrame {
    CGRect newFrame = [self convertRect:self.bounds toView:[self rootView]];
    _player.view.frame = newFrame;
}

// attach view to window while we are in fullscreen (to survive rotation events)
- (void)willEnterFullscreen {
    [[self rootView] insertSubview:self.player.view atIndex:0];
    _player.view.autoresizingMask = UIViewAutoresizingNone;
    [self updateFullscreenPlayerFrame];

    // hide HUD. First of all, this is expected, second the navigationBar bleeds through when we rotate.
    [self.annotation.document.displayingPdfController hideControls];
}

- (void)willExitFullscreen {
    [[NSNotificationCenter defaultCenter] postNotificationName:PSPDFFixNavigationBarFrameNotification object:self];

    // re-attach to annotation view one runloop later (else we brek the animation)
    dispatch_async(dispatch_get_main_queue(), ^{
        [self addSubview:self.player.view];
        _player.view.frame = self.bounds;
        _player.view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    });
}

// Somehow is no longer getting called on iOS6?
- (void)didExitFullscreen {

    // send both at will and did, as depending on the animationbar state we can fix this while or after the animation.
    [[NSNotificationCenter defaultCenter] postNotificationName:PSPDFFixNavigationBarFrameNotification object:self];
}

// ensure that we don't play in the background because if some timing issues and autostart
- (void)didChangePlaybackState {
    if (_player.playbackState == MPMoviePlaybackStatePlaying && !_isShowing) {
        [_player pause];
    }
}

- (void)registerNotifications {
    if (_player) {
        NSNotificationCenter *dnc = [NSNotificationCenter defaultCenter];
        [dnc addObserver:self selector:@selector(willEnterFullscreen) name:MPMoviePlayerWillEnterFullscreenNotification object:_player];
        [dnc addObserver:self selector:@selector(willExitFullscreen) name:MPMoviePlayerWillExitFullscreenNotification object:_player];
        [dnc addObserver:self selector:@selector(didExitFullscreen) name:MPMoviePlayerDidExitFullscreenNotification object:_player];
        [dnc addObserver:self selector:@selector(didChangePlaybackState) name:MPMoviePlayerPlaybackStateDidChangeNotification object:_player];
    }
}

- (void)deregisterNotifications {
    if (_player) {
        NSNotificationCenter *dnc = [NSNotificationCenter defaultCenter];
        [dnc removeObserver:self name:MPMoviePlayerWillEnterFullscreenNotification object:_player];
        [dnc removeObserver:self name:MPMoviePlayerWillExitFullscreenNotification object:_player];
        [dnc removeObserver:self name:MPMoviePlayerDidExitFullscreenNotification object:_player];
        [dnc removeObserver:self name:MPMoviePlayerPlaybackStateDidChangeNotification object:_player];
    }
}

- (void)addPlayerController {
    if (!_player || ![_player.contentURL isEqual:_URL]) {

#if defined(__i386__) || defined(__x86_64__)
        NSLog(@"------------------------------------------------------------------------------");
        NSLog(@"Note: If the embedded video crashes, it's a bug in Apple's Simulator. Please try on a real device.");
        NSLog(@"Referencing \"Error loading /System/Library/Extensions/AudioIPCDriver.kext/... Symbol not found: ___CFObjCIsCollectable.\"");
        NSLog(@"This is a known bug in Xcode 4.2+ / Lion");
        NSLog(@"This note will only show up in the i386 codebase and not on the device.");
        NSLog(@"------------------------------------------------------------------------------");
#endif

        [self deregisterNotifications];
        _player = [[MPMoviePlayerController alloc] initWithContentURL:_URL];

        // alternative controls if full video controls are disabled
        UITapGestureRecognizer *tapGesture = nil;
        if (!self.linkAnnotation.controlsEnabled) {
            tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(videoTapped:)];
            [[_player.view.subviews ps_firstObject] addGestureRecognizer:tapGesture];
            UIPinchGestureRecognizer *pinch = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(fullscreenGesture:)];
            [[_player.view.subviews ps_firstObject] addGestureRecognizer:pinch];
        }

        UITapGestureRecognizer *doubleTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(videoDoubleTapped:)];
        if (tapGesture) [doubleTapGesture requireGestureRecognizerToFail:tapGesture];
        doubleTapGesture.numberOfTapsRequired = 2;
        [[_player.view.subviews ps_firstObject] addGestureRecognizer:doubleTapGesture];

        // only works if video is currently blacked out because multiple MPMovieControllers are visible. Enables video playback.
        UITapGestureRecognizer *secondTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(videoTapped:)];
        [_player.view addGestureRecognizer:secondTapGesture];

        _player.useApplicationAudioSession = _useApplicationAudioSession;
        _player.controlStyle = self.linkAnnotation.controlsEnabled ? MPMovieControlStyleEmbedded : MPMovieControlStyleNone;
        [self registerNotifications];
        _player.view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        _player.view.frame = self.bounds;
        [self addSubview:_player.view];
    }
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UIView

- (void)layoutSubviews {
    [super layoutSubviews];
    if (_player.view.superview == self) {
        _player.view.frame = self.bounds;
    }else {
        [self updateFullscreenPlayerFrame];
    }
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public

- (void)setURL:(NSURL *)URL {
    if (URL != _URL) {
        _URL = URL;

        // only update if we're not showing a cover or already pressed play.
        if (![self coverURL] || ([self coverURL] && self.player)) {
            [self addPlayerController];
        }

        // if it's a file reference, check if the file actually exists!
        if ([URL isFileURL] && ![[NSFileManager defaultManager] isReadableFileAtPath:[URL path]]) {
            PSPDFLogWarning(@"File not found: %@ referenced from annotation at page %d.", [URL path], self.annotation.page);
        }
    }
}

- (void)setAnnotation:(PSPDFAnnotation *)annotation {
    if (self.annotation != annotation) {
        [super setAnnotation:annotation];
        self.backgroundColor = [annotation.color colorWithAlphaComponent:0.5];

        // reset cached properties
        _coverViewPressed = NO;
        _wasPlaying = NO;
        _useApplicationAudioSession = NO;

        // defaults to NO if not found.
        self.autoplayEnabled = self.linkAnnotation.isAutoplayEnabled;
        _autoPlayEnabledHandled = NO; // handle autoplay only once

        // lastly, set URL. This creates the control.
        self.URL = self.linkAnnotation.URL;
    }
}

- (NSURL *)coverURL {
    return (self.linkAnnotation.options)[@"cover"];
}

- (void)addCoverView {
    if (!self.coverView) {
        self.coverView = [[PSPDFVideoAnnotationCoverView alloc] initWithFrame:self.bounds];
        self.coverView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        [self addSubview:self.coverView];

        NSURL *coverURL = [self coverURL];
        if ([coverURL isKindOfClass:[NSURL class]]) {
            // TODO: need to support web-images too...
            if ([coverURL isFileURL]) {
                self.coverView.coverImage.image = [UIImage imageWithContentsOfFile:[coverURL path]];
            }
        }
        [self.coverView.playButton addTarget:self action:@selector(showPlayerAndPlay) forControlEvents:UIControlEventTouchUpInside];
    }
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - PSPDFAnnotationView

- (void)willShowPage:(NSUInteger)page {
    [self didShowPage:page];
}

/// page is displayed
- (void)didShowPage:(NSUInteger)page {
    // don't handle this event at all; fullscreen is handled totally differently
    if (_player && _player.view.superview != self) return;

    // check if we have a cover defined and create the view if so
    NSURL *coverURL = [self coverURL];
    if (coverURL && !_coverViewPressed) {
        [self addCoverView];
    }else {
        [self showPlayer];
    }
}

- (void)showPlayerAndPlay {
    _wasPlaying = YES;
    [self showPlayerAndForceAutoplay:YES];
}

- (void)showPlayer {
    [self showPlayerAndForceAutoplay:NO];
}

- (void)showPlayerAndForceAutoplay:(BOOL)forceAutoplay {
    _coverViewPressed = YES;

    // always remove cover view
    [self.coverView removeFromSuperview];
    self.coverView = nil;

    if (!_isShowing) {
        _isShowing = YES;

        // only handle autoplay once per annotation
        BOOL shouldAutoplay = (!_autoPlayEnabledHandled && self.isAutoplayEnabled) || forceAutoplay;
        _autoPlayEnabledHandled = YES;
        
        [self addPlayerController];
        [_player prepareToPlay];
        [_player setShouldAutoplay:shouldAutoplay];

        // start the video for iOS4, prepareToPlay isn't enough to show the controls here
        if (_wasPlaying) [_player play];
    }
}

- (void)willHidePage:(NSUInteger)page {
    [self didHidePage:page];
}

/// page is hidden
- (void)didHidePage:(NSUInteger)page {
    // don't handle this event at all; fullscreen is handled totally differently
    if (_player.view.superview != self) return;

    if (_isShowing) {
        _isShowing = NO;

        if (_player.playbackState == MPMoviePlaybackStatePlaying) {
            [_player pause];
            _wasPlaying = YES;
        }else {
            _wasPlaying = NO;
        }
    }
}

- (void)didChangePageFrame:(CGRect)frame {
    CGRect targetRect = (CGRect){CGPointZero, self.frame.size};
    _player.view.frame = targetRect;
}

@end


@implementation PSPDFVideoAnnotationCoverView

- (id)initWithFrame:(CGRect)frame {
    if ((self = [super initWithFrame:frame])) {
        self.backgroundColor = [UIColor clearColor];
        
        _coverImage = [[UIImageView alloc] initWithFrame:self.bounds];
        _coverImage.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        _coverImage.contentMode = UIViewContentModeScaleAspectFill;
        _coverImage.backgroundColor = [UIColor clearColor];
        [self addSubview:_coverImage];

        // add play button
        _playButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_playButton setImage:[UIImage imageNamed:@"PSPDFKit.bundle/Play"] forState:UIControlStateNormal];
        [_playButton sizeToFit];
        _playButton.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin;
        _playButton.frame = PSPDFAlignRectangles(_playButton.bounds, self.bounds, PSPDFRectAlignCenter);
        [self addSubview:_playButton];
    }
    return self;
}

@end
