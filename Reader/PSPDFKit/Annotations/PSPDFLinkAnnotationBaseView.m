//
//  PSPDFLinkAnnotationBaseView.m
//  PSPDFKit
//
//  Copyright (c) 2012 Peter Steinberger. All rights reserved.
//

#import "PSPDFLinkAnnotationBaseView.h"
#import "PSPDFLinkAnnotation.h"

@implementation PSPDFLinkAnnotationBaseView

@synthesize annotation = _annotation, zIndex = _zIndex;

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (id)initWithFrame:(CGRect)frame {
    if ((self = [super initWithFrame:frame])) {
        _zIndex = 1;
    }
    return self;
}

- (NSString *)description {
    return [NSString stringWithFormat:@"%@ annotation:%@>", [super description], self.annotation];
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - PSPDFAnnotationView

- (void)didChangePageFrame:(CGRect)frame {
    [self setNeedsLayout];
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public

// just a convenient cast
- (PSPDFLinkAnnotation *)linkAnnotation {
    return (PSPDFLinkAnnotation *)self.annotation;
}


@end
