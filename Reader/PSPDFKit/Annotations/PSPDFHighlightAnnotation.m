//
//  HighlightAnnotation.m
//  PSPDFKit
//
//  Copyright 2012 Peter Steinberger. All rights reserved.
//

#import "PSPDFHighlightAnnotation.h"
#import "UIColor+PSPDFKitAdditions.h"
#import "PSPDFDocument.h"
#import "PSPDFWord.h"

@implementation PSPDFHighlightAnnotation

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Static

+ (NSArray *)supportedTypes {
    return @[PSPDFAnnotationTypeStringHighlight, PSPDFAnnotationTypeStringUnderline, PSPDFAnnotationTypeStringStrikeout];
}

+ (BOOL)isWriteable {
    return YES;
}

- (BOOL)isMovable {
    return NO;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (id)init {
    return [self initWithHighlightType:PSPDFHighlightAnnotationHighlight];
}

- (id)initWithHighlightType:(PSPDFHighlightAnnotationType)annotationType {
	if((self = [super initWithType:PSPDFAnnotationTypeHighlight])) {
        self.indexOnPage = -1;
        self.popupIndex = -1;
        self.boundingBox = CGRectZero;
        self.rects = @[];
        [self setType:annotationType withDefaultColor:YES];
    }
	return self;
}

- (id)initWithAnnotationDictionary:(CGPDFDictionaryRef)annotDict inAnnotsArray:(CGPDFArrayRef)annotsArray {
	if((self = [super initWithAnnotationDictionary:annotDict inAnnotsArray:annotsArray type:PSPDFAnnotationTypeHighlight])) {
        CGPDFArrayRef quadPointsArray = NULL;
        CGPDFDictionaryGetArray(annotDict, "QuadPoints", &quadPointsArray);
        _rects = [self rectsFromQuadPointsInArray:quadPointsArray];

        // Reconstruct bounding box from quadpoints (more reliable, in case the information is incosistent)
        if ([_rects count] >= 1) {
            float minX = MAXFLOAT;
            float minY = MAXFLOAT;
            float maxX = -MAXFLOAT;
            float maxY = -MAXFLOAT;
            for (NSValue *rectValue in _rects) {
                CGRect rect = [rectValue CGRectValue];
                if (CGRectGetMinX(rect) < minX) minX = CGRectGetMinX(rect);
                if (CGRectGetMaxX(rect) > maxX) maxX = CGRectGetMaxX(rect);
                if (CGRectGetMinY(rect) < minY) minY = CGRectGetMinY(rect);
                if (CGRectGetMaxY(rect) > maxY) maxY = CGRectGetMaxY(rect);
            }
            self.boundingBox = CGRectMake(minX, minY, maxX-minX, maxY-minY);
        }

        PSPDFHighlightAnnotationType highlightType = PSPDFHighlightAnnotationUnknown;
        if ([self.typeString isEqual:PSPDFAnnotationTypeStringHighlight]) {
            highlightType = PSPDFHighlightAnnotationHighlight;
        }else if ([self.typeString isEqual:PSPDFAnnotationTypeStringUnderline]) {
            highlightType = PSPDFHighlightAnnotationUnderline;
        }else if ([self.typeString isEqual:PSPDFAnnotationTypeStringStrikeout]) {
            highlightType = PSPDFHighlightAnnotationStrikeOut;
        }
        [self setType:highlightType withDefaultColor:self.color == nil];
        self.dirty = NO;
    }
	return self;
}

- (void)setType:(PSPDFHighlightAnnotationType)highlightType withDefaultColor:(BOOL)useDefaultColor {
    _highlightType = highlightType;
    switch (highlightType) {
        case PSPDFHighlightAnnotationHighlight:
            if (useDefaultColor) {
                self.color = [UIColor colorWithRed:0.99 green:0.93 blue:0.55 alpha:1.f];
            }
            self.typeString = PSPDFAnnotationTypeStringHighlight;
            break;
        case PSPDFHighlightAnnotationStrikeOut:
            if (useDefaultColor) {
                self.color = [UIColor colorWithRed:0.97 green:0.38 blue:0.33 alpha:1.f];
            }
            self.typeString = PSPDFAnnotationTypeStringStrikeout;
            break;
        case PSPDFHighlightAnnotationUnderline:
            if (useDefaultColor) {
                self.color = [[UIColor blackColor] pspdf_colorInRGBColorSpace];
            }
            self.typeString = PSPDFAnnotationTypeStringUnderline;
            break;
        default:
            self.color = [[UIColor grayColor] pspdf_colorInRGBColorSpace];
            break;
    }
}

- (NSString *)description {
	return [NSString stringWithFormat:@"%@ - highlightType:%d rects: %@>", [super description], self.highlightType, self.rects];
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - PSPDFAnnotation

- (void)drawInContext:(CGContextRef)context {
    [super drawInContext:context];
    if (self.isDeleted) return;

    CGContextSaveGState(context);

	if (_highlightType == PSPDFHighlightAnnotationHighlight) {
		CGContextSetBlendMode(context, kCGBlendModeMultiply);
	} else {
		CGContextSetBlendMode(context, kCGBlendModeNormal);
	}

    CGContextSetFillColorWithColor(context, self.colorWithAlpha.CGColor);
    NSUInteger rotation = self.rotation;
	if (_highlightType == PSPDFHighlightAnnotationHighlight) {
		for (NSValue *rectValue in self.rects) {
			CGRect rect = [rectValue CGRectValue];

			CGPoint p1 = CGPointMake(CGRectGetMinX(rect), CGRectGetMinY(rect));
			CGPoint p2 = CGPointMake(CGRectGetMinX(rect), CGRectGetMaxY(rect));
			CGPoint p3 = CGPointMake(CGRectGetMaxX(rect), CGRectGetMaxY(rect));
			CGPoint p4 = CGPointMake(CGRectGetMaxX(rect), CGRectGetMinY(rect));
			CGPoint c1, c2;
			float curveWidth;
			float radius = 1.0/6.0;
			if (rotation == 0 || rotation == 180) {
				curveWidth = (p2.y - p1.y) * radius;
				c1 = CGPointMake(p1.x - curveWidth, p1.y + 0.5*(p2.y-p1.y));
				c2 = CGPointMake(p3.x + curveWidth, p4.y + 0.5*(p3.y-p4.y));
			} else {
				CGPoint temp = p1;
				p1 = p2;
				p2 = p3;
				p3 = p4;
				p4 = temp;
				curveWidth = (p2.x - p1.x) * radius;
				c1 = CGPointMake(p1.x + 0.5*(p2.x-p1.x), p1.y + curveWidth);
				c2 = CGPointMake(p4.x + 0.5*(p3.x-p4.x), p4.y - curveWidth);
			}

			CGContextMoveToPoint(context, p1.x, p1.y);
			CGContextAddQuadCurveToPoint(context, c1.x, c1.y, p2.x, p2.y);
			CGContextAddLineToPoint(context, p3.x, p3.y);
			CGContextAddQuadCurveToPoint(context, c2.x, c2.y, p4.x, p4.y);
			CGContextClosePath(context);

		}
	} else if (_highlightType == PSPDFHighlightAnnotationStrikeOut) {
		for (NSValue *rectValue in self.rects) {
			CGRect rect = [rectValue CGRectValue];
			if (rotation != 90 && rotation != 270) {
				float strikeOutWidth = rect.size.height / 10.0;
				CGRect strikeOutRect = CGRectMake(CGRectGetMinX(rect), CGRectGetMidY(rect) - strikeOutWidth/2, CGRectGetWidth(rect), strikeOutWidth);
				CGContextAddRect(context, strikeOutRect);
			} else {
				float strikeOutWidth = rect.size.width / 10.0;
				CGRect strikeOutRect = CGRectMake(CGRectGetMidX(rect) - strikeOutWidth/2, CGRectGetMinY(rect), strikeOutWidth, CGRectGetHeight(rect));
				CGContextAddRect(context, strikeOutRect);
			}
		}
	} else if (_highlightType == PSPDFHighlightAnnotationUnderline) {
		for (NSValue *rectValue in self.rects) {
			CGRect rect = [rectValue CGRectValue];
			if (rotation == 0) {
				float underlineWidth = rect.size.height / 12.0;
				CGRect underlineRect = CGRectMake(CGRectGetMinX(rect), CGRectGetMinY(rect), CGRectGetWidth(rect), underlineWidth);
				CGContextAddRect(context, underlineRect);
			} else if (rotation == 270) {
				float underlineWidth = rect.size.width / 12.0;
				CGRect strikeOutRect = CGRectMake(CGRectGetMinX(rect)+underlineWidth, CGRectGetMinY(rect), underlineWidth, CGRectGetHeight(rect));
				CGContextAddRect(context, strikeOutRect);
			} else {
				float underlineWidth = rect.size.width / 12.0;
				CGRect strikeOutRect = CGRectMake(CGRectGetMaxX(rect)-underlineWidth, CGRectGetMinY(rect), underlineWidth, CGRectGetHeight(rect));
				CGContextAddRect(context, strikeOutRect);
			}
		}
	}

	if ([self.contents length]) {
		if (_highlightType == PSPDFHighlightAnnotationHighlight) {
			float darkenOutlineFactor = 0.8;
            const CGFloat *color = CGColorGetComponents(self.colorWithAlpha.CGColor);
			CGContextSetRGBStrokeColor(context, color[0]*darkenOutlineFactor, color[1]*darkenOutlineFactor, color[2]*darkenOutlineFactor, 1.0);
			CGContextSetLineWidth(context, 1.0);
			CGContextDrawPath(context, kCGPathFillStroke);
		} else {
			CGContextFillPath(context);
			for (NSValue *rectValue in self.rects) {
				CGRect rect = [rectValue CGRectValue];
				CGContextAddRect(context, rect);
			}
            CGContextSetFillColorWithColor(context, [self.color colorWithAlphaComponent:0.1].CGColor);
			CGContextSetBlendMode(context, kCGBlendModeMultiply);
			CGContextFillPath(context);
		}
	} else {
		CGContextFillPath(context);
	}

	CGContextRestoreGState(context);
}

- (NSString *)quadPointsString {
	NSMutableString *quadPoints = [NSMutableString stringWithString:@"["];
	for (NSValue *rectValue in _rects) {
		CGRect rect = [rectValue CGRectValue];
		float x1, y1, x2, y2, x3, y3, x4, y4;
		if (CGRectGetWidth(rect) > CGRectGetHeight(rect)) {
			x1 = CGRectGetMinX(rect);
			y1 = CGRectGetMaxY(rect);
			x2 = CGRectGetMaxX(rect);
			y2 = CGRectGetMaxY(rect);
			x3 = CGRectGetMinX(rect);
			y3 = CGRectGetMinY(rect);
			x4 = CGRectGetMaxX(rect);
			y4 = CGRectGetMinY(rect);
		} else {
			x1 = CGRectGetMinX(rect);
			y1 = CGRectGetMinY(rect);
			x2 = CGRectGetMinX(rect);
			y2 = CGRectGetMaxY(rect);
			x3 = CGRectGetMaxX(rect);
			y3 = CGRectGetMinY(rect);
			x4 = CGRectGetMaxX(rect);
			y4 = CGRectGetMaxY(rect);
		}
		[quadPoints appendFormat:@"%f %f %f %f %f %f %f %f ", x1, y1, x2, y2, x3, y3, x4, y4];
	}
	[quadPoints appendString:@"]"];
    return quadPoints;
}

- (NSData *)pdfDataRepresentation {
	NSMutableData *pdfData = [NSMutableData data];
    [pdfData pspdf_appendDataString:[NSString stringWithFormat:@"<</Type /Annot /Subtype /%@ /F 4 %@ %@ /QuadPoints %@ ", self.typeString, [self pdfRectString], [self pdfColorWithAlphaString], [self quadPointsString]]];
	[self appendEscapedContents:pdfData];
    [pdfData pspdf_appendDataString:@">>"];
	return pdfData;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public

- (NSString *)highlightedString {
    NSMutableString *highlightedString = [NSMutableString string];
    NSUInteger absolutePage = self.page + [self.document pageOffsetForDocumentProvider:self.documentProvider];
    BOOL needsLineBreak = NO;
    for (NSValue *rectValue in self.rects) {
        if ([highlightedString length]) {
            [highlightedString appendString:needsLineBreak ? @"\n" : @" "];
        }
        NSDictionary *objects = [self.document objectsAtPDFRect:[rectValue CGRectValue] page:absolutePage options:@{kPSPDFObjectsFullWords : @YES, kPSPDFObjectsText : @YES}];
        [highlightedString appendString:objects[kPSPDFText]];
        needsLineBreak = [[objects[kPSPDFWords] lastObject] lineBreaker];
    }
    return [highlightedString copy];
}

+ (PSPDFHighlightAnnotationType)highlightTypeFromTypeString:(NSString *)typeString {
    if ([typeString isEqualToString:PSPDFAnnotationTypeStringHighlight]) {
        return PSPDFHighlightAnnotationHighlight;
    }else if ([typeString isEqualToString:PSPDFAnnotationTypeStringUnderline]) {
        return PSPDFHighlightAnnotationUnderline;
    }else if ([typeString isEqualToString:PSPDFAnnotationTypeStringStrikeout]) {
        return PSPDFHighlightAnnotationStrikeOut;
    }else {
        return PSPDFHighlightAnnotationUnknown;
    }
}

+ (NSString *)typeStringForHighlightType:(PSPDFHighlightAnnotationType)highlightType {
    switch (highlightType) {
        case PSPDFHighlightAnnotationHighlight: return PSPDFAnnotationTypeStringHighlight;
        case PSPDFHighlightAnnotationUnderline: return PSPDFAnnotationTypeStringUnderline;
        case PSPDFHighlightAnnotationStrikeOut: return PSPDFAnnotationTypeStringStrikeout;
        default: return PSPDFAnnotationTypeStringHighlight;
    }
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Properties

- (void)setRects:(NSArray *)rects {
    if (rects != _rects) {
        _rects = rects;
        self.dirty = YES;
    }
}

- (void)setHighlightType:(PSPDFHighlightAnnotationType)highlightType {
    if (highlightType != _highlightType) {
        _highlightType = highlightType;
        self.typeString = [[self class] typeStringForHighlightType:highlightType];
        self.dirty = YES;
    }
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSCoding

- (id)initWithCoder:(NSCoder *)decoder {
	if((self = [super initWithCoder:decoder])) {
        NSArray *rectStrings = [decoder decodeObjectForKey:NSStringFromSelector(@selector(rects))];
        _rects = [[self class] rectsFromStringsArray:rectStrings];

        // highlight type is parsed
        _highlightType = [[self class] highlightTypeFromTypeString:self.typeString];
    }
	return self;
}

- (void)encodeWithCoder:(NSCoder *)coder {
	[super encodeWithCoder:coder];

    NSArray *rectStrings = [[self class] stringsFromRectsArray:_rects];
	[coder encodeObject:rectStrings forKey:NSStringFromSelector(@selector(rects))];
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSCopying

- (id)copyWithZone:(NSZone *)zone {
    PSPDFHighlightAnnotation *highlightAnnotation = (PSPDFHighlightAnnotation *)[super copyWithZone:zone];
    highlightAnnotation.rects = self.rects;
    highlightAnnotation.highlightType = self.highlightType;
    return highlightAnnotation;
}

@end
