//
//  PSPDFFileAnnotationProvider.m
//  PSPDFKit
//
//  Copyright (c) 2012 Peter Steinberger. All rights reserved.
//

#import "PSPDFViewController+Internal.h"
#import "PSPDFKitGlobal+Internal.h"
#import "PSPDFDocumentProvider.h"
#import "PSPDFDocument.h"
#import "PSPDFDocumentParser.h"
#import "PSPDFConverter.h"
#import "PSPDFFileAnnotationProvider.h"
#import "PSPDFAnnotation.h"
#import "PSPDFOutlineParser.h"
#import "PSPDFLinkAnnotation.h"
#import "PSPDFNoteAnnotation.h"
#import "NSURL+PSPDFUnicodeURL.h"
#import "PSPDFAnnotationParser.h"
#import <objc/runtime.h>

@interface PSPDFFileAnnotationProvider () {
    NSMutableDictionary *_annotationCache;
    dispatch_queue_t _dictCacheQueue;
    NSArray *_videoFileTypes;
}
@property (nonatomic, strong) NSMutableDictionary *namedDestinations;
@end

@implementation PSPDFFileAnnotationProvider

@synthesize providerDelegate = _providerDelegate;

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Static

+ (NSDictionary *)defaultFileTypeTranslationTable {
    static __strong NSDictionary *_fileTypeTranslationTable;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _fileTypeTranslationTable =
        @{
        @"pdf" : @(PSPDFLinkAnnotationDocument),

        @"m3u8": @(PSPDFLinkAnnotationVideo),
        @"mov" : @(PSPDFLinkAnnotationVideo),
        @"mpg" : @(PSPDFLinkAnnotationVideo),
        @"avi" : @(PSPDFLinkAnnotationVideo),
        @"m4v" : @(PSPDFLinkAnnotationVideo),
        @"mp4" : @(PSPDFLinkAnnotationVideo), // might also be audio

        @"m4a" : @(PSPDFLinkAnnotationAudio),
        @"mp3" : @(PSPDFLinkAnnotationAudio),

        @"jpg" : @(PSPDFLinkAnnotationImage),
        @"jpeg": @(PSPDFLinkAnnotationImage),
        @"tif" : @(PSPDFLinkAnnotationImage),
        @"tiff": @(PSPDFLinkAnnotationImage),
        @"png" : @(PSPDFLinkAnnotationImage),
        @"gif" : @(PSPDFLinkAnnotationImage),
        @"cur" : @(PSPDFLinkAnnotationImage),
        @"bmp" : @(PSPDFLinkAnnotationImage),
        @"bmpf": @(PSPDFLinkAnnotationImage),
        @"ico" : @(PSPDFLinkAnnotationImage),
        @"xbm" : @(PSPDFLinkAnnotationImage)
        };
    });
    return _fileTypeTranslationTable;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (id)initWithDocumentProvider:(PSPDFDocumentProvider *)documentProvider {
    if ((self = [super init])) {
        _documentProvider = documentProvider;
        _dictCacheQueue = pspdf_dispatch_queue_create("com.PSPDFKit.annotationCache", DISPATCH_QUEUE_CONCURRENT);
        _fileTypeTranslationTable = [[self class] defaultFileTypeTranslationTable];
        _protocolString = @"pspdfkit://";
        _annotationCache = [NSMutableDictionary new];   // annotation page cache
        _namedDestinations = [NSMutableDictionary new]; // resolve page refs
        [self tryLoadAnnotationsFromFileWithError:nil];
    }
    return self;
}

- (void)dealloc {
    PSPDFDispatchRelease(_dictCacheQueue);
}

- (NSString *)description {
    __block NSString *description;
    dispatch_sync(_dictCacheQueue, ^{
        description = [NSString stringWithFormat:@"<%@: %p %@>", NSStringFromClass([self class]), self, _annotationCache];
    });
    return description;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - PSPDFAnnotationProvider

- (NSArray *)annotationsForPage:(NSUInteger)page {
    return [self annotationsForPage:page pageRef:nil];
}

- (NSArray *)annotationsForPage:(NSUInteger)page pageRef:(CGPDFPageRef)pageRef {
    __block NSArray *annotations = nil;

    // sanity check
    if (!_documentProvider) { PSPDFLogWarning(@"No document provider attached."); return nil; }

    // fast lookup
    NSNumber *pageNumber = @(page);
    pspdf_dispatch_sync_reentrant(_dictCacheQueue, ^{
        annotations = _annotationCache[pageNumber];
    });

    // parse
    if (!annotations) {
        @synchronized(self) {
            // look up again - check if annotations are already parsed.
            pspdf_dispatch_sync_reentrant(_dictCacheQueue, ^{
                annotations = _annotationCache[pageNumber];
            });

            if (!annotations) {
                annotations = [self parseAnnotationsForPage:page pageRef:pageRef];

                dispatch_barrier_sync(_dictCacheQueue, ^{
                    _annotationCache[pageNumber] = annotations;
                });
            }
        }
    }

    return annotations;
}

- (BOOL)hasLoadedAnnotationsForPage:(NSUInteger)page {
    __block NSArray *annotations;
    pspdf_dispatch_sync_reentrant(_dictCacheQueue, ^{
        annotations = _annotationCache[@(page)];
    });

    BOOL hasLoaded = annotations != nil;
    return hasLoaded;
}

// optional
- (BOOL)shouldChangeAnnotation:(PSPDFAnnotation *)annotation {
    return YES;
}

- (void)didChangeAnnotation:(PSPDFAnnotation *)annotation originalAnnotation:(PSPDFAnnotation *)originalAnnotation keyPaths:(NSArray *)keyPaths options:(NSDictionary *)options {
    // no automatic updates
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Parsing

- (NSArray *)parseAnnotationsForPage:(NSUInteger)page pageRef:(CGPDFPageRef)pageRef {
    NSMutableArray *newAnnotations = [NSMutableArray array];
    // lock to not allow querying multiple times
    PSPDFLogVerbose(@"fetching annotations for page %d", page);

    // if no pageRef was given, open document ourself
    PSPDFDocumentProvider *documentProvider = nil;
    if (!pageRef) {
        documentProvider = _documentProvider;
        pageRef = [documentProvider requestPageRefForPageNumber:page+1];
    }
    CGPDFDocumentRef documentRef = CGPDFPageGetDocument(pageRef);
    CGPDFDictionaryRef pageDict = CGPDFPageGetDictionary(pageRef);
    CGPDFArrayRef annotsArray = NULL;
    CGPDFArrayRef annots = NULL;
    CGPDFDictionaryGetArray(pageDict, "Annots", &annots);
    if (annots != NULL) {
        for (int i=0; i<CGPDFArrayGetCount(annots); i++) {
            @autoreleasepool {
                CGPDFDictionaryRef annotDict;
                if (CGPDFArrayGetDictionary(annots, i, &annotDict)) {
                    NSString *annotationType = PSPDFDictionaryGetString(annotDict, @"Subtype");
                    Class annotationClass = [self annotationClassForType:annotationType];
                    PSPDFAnnotation *annotation = [[annotationClass alloc] initWithAnnotationDictionary:annotDict inAnnotsArray:annotsArray];

                    // further process annotation if it's a link (resolve)
                    if ([annotation isKindOfClass:[PSPDFLinkAnnotation class]]) {
                        [self parsePageAnnotation:(PSPDFLinkAnnotation *)annotation dictionary:annotDict document:documentRef page:page];
                    }

                    // set common properties
                    if (annotation) {
                        annotation.page = page;
                        annotation.indexOnPage = i;
                        annotation.documentProvider = self.documentProvider;
                        [annotation parse];
                        annotation.dirty = NO; // initially annotations aren't dirty.
                        [newAnnotations addObject:annotation];
                    }
                }
            }
        }
    }

    // resolve external name references (only needed if name is saved in /GoTo page)
    NSDictionary *resolvedNames = [PSPDFOutlineParser resolveDestNames:[NSSet setWithArray:[_namedDestinations allKeys]] documentRef:documentRef];
    for (NSNumber *destPageName in [resolvedNames allKeys]) {
        NSSet *pageAnnotations = _namedDestinations[destPageName];
        for (PSPDFLinkAnnotation *annotation in pageAnnotations) {
            NSInteger destPage = [resolvedNames[destPageName] integerValue];
            annotation.pageLinkTarget = destPage;
        }
    }
    [documentProvider releasePageRef:pageRef];

    return newAnnotations;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Adding/Changing Annotations

- (void)setAnnotations:(NSArray *)annotations forPage:(NSUInteger)page {
    dispatch_barrier_sync(_dictCacheQueue, ^{
        NSNumber *pageNumber = @(page);
        if (annotations) {
            [self updateAnnotationsPageAndDocumentReference:annotations page:page];
            _annotationCache[pageNumber] = [annotations mutableCopy];
        }else {
            [_annotationCache removeObjectForKey:pageNumber];
        }
    });
}

// ensure document/page references are set correctly before adding annotations.
- (void)updateAnnotationsPageAndDocumentReference:(NSArray *)annotations page:(NSUInteger)page {
    for (PSPDFAnnotation *annotation in annotations) {
        annotation.page = page;
        annotation.documentProvider = self.documentProvider;

        if ([annotation isKindOfClass:[PSPDFLinkAnnotation class]]) {
            PSPDFLinkAnnotation *linkAnnotation = (PSPDFLinkAnnotation *)annotation;
            if ([linkAnnotation.siteLinkTarget length] && !linkAnnotation.URL) {
                [self parseAnnotationLinkTarget:linkAnnotation];
            }
        }
    }
}

- (BOOL)addAnnotations:(NSArray *)annotations forPage:(NSUInteger)page {
    if ([annotations count] == 0) return YES;

    // for add, we need to make sure annotations are actually loaded.
    // (else we might go in a state where annotations are cleaned after a save and then a new annotation is added and the system doesn't trigger annotation loading anymore)
    [self annotationsForPage:page];

    NSString *defaultAnnotationUsername = self.defaultAnnotationUsername;
    if ([defaultAnnotationUsername length]) {
        for (PSPDFAnnotation *annotation in annotations) {
            if (![annotation.user length]) annotation.user = defaultAnnotationUsername;
        }
    }

    dispatch_barrier_sync(_dictCacheQueue, ^{
        [self updateAnnotationsPageAndDocumentReference:annotations page:page];
        // ensure annotations have the document set correctly.
        NSMutableArray *mergedAnnotations = _annotationCache[@(page)] ?: [NSMutableArray array];
        [mergedAnnotations addObjectsFromArray:annotations];
        _annotationCache[@(page)] = mergedAnnotations;
    });
    return YES;
}

- (void)setProtocolString:(NSString *)protocolString {
    if (protocolString != _protocolString) {
        _protocolString = protocolString;
        dispatch_barrier_sync(_dictCacheQueue, ^{
            [_annotationCache removeAllObjects]; // clear page cache after protocol has been re-set
        });
    }
}

- (void)clearCache {
    pspdf_dispatch_sync_reentrant(_dictCacheQueue, ^{
        [_annotationCache removeAllObjects];
    });
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Getting annotation classes

/// override to customize and support new URL schemes.
- (void)parseAnnotationLinkTarget:(PSPDFLinkAnnotation *)annotation {
    if (![annotation.siteLinkTarget length]) {
        PSPDFLogWarning(@"Not parsign empty annotation: %@", annotation); return;
    }

    NSMutableString *link = [NSMutableString stringWithString:annotation.siteLinkTarget];

    // some annotations (wrongly) don't add the protocol, work around this.
    if ([[link lowercaseString] hasPrefix:@"www"]) {
        [link insertString:@"http://" atIndex:0];
    }

    BOOL linkContainsLocalhost = [link rangeOfString:@"localhost" options:NSCaseInsensitiveSearch].length > 0;
    BOOL linkContainsProtocol = [[link lowercaseString] hasPrefix:self.protocolString];

    // if link contains localhost, it's most likely a link to a different PDF document.
    if (linkContainsLocalhost || linkContainsProtocol) {
        // we both support a style like pspdfkit://www.xxx and pspdfkit://https://www.xxx
        // for local files, use pspdfkit://localhost/Folder/File.ext
        if (linkContainsProtocol) {
            [link deleteCharactersInRange:NSMakeRange(0, [self.protocolString length])];
        }

        NSMutableDictionary *linkOptions = nil;
        static NSString *const pdfOptionMarker = @"[";
        static NSString *const optionEndMarker = @"]";
        if ([[link lowercaseString] hasPrefix:pdfOptionMarker]) {
            NSRange endRange = [link rangeOfString:optionEndMarker options:0 range:NSMakeRange([pdfOptionMarker length], [link length] - [pdfOptionMarker length])];
            if (endRange.length > 0) {
                NSString *optionStr = [link substringWithRange:NSMakeRange([pdfOptionMarker length], endRange.location - [pdfOptionMarker length])];
                [link deleteCharactersInRange:NSMakeRange(0, endRange.location + endRange.length)];

                // convert linkOptions to a dictionary
                NSArray *options = [optionStr componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@":,="]];
                linkOptions = [NSMutableDictionary dictionary];
                NSUInteger optIndex = 0;
                while (optIndex+1 < [options count]) {
                    NSString *key = options[optIndex];
                    NSString *option = options[optIndex+1];
                    linkOptions[key] = option;
                    optIndex+=2;
                }
            }
        }
        if ([linkOptions count]) {
            // special hook to parse certain options that contain links
            NSMutableDictionary *processedOptions = [linkOptions mutableCopy];
            [linkOptions enumerateKeysAndObjectsUsingBlock:^(id key, id option, BOOL *stop) {
                // http:// , https:// , localhost/
                if ([option isKindOfClass:[NSString class]] && [option rangeOfString:@"(http[s]?:/|localhost)/" options:NSRegularExpressionSearch | NSCaseInsensitiveSearch].length > 0) {
                    NSURL *newOption = [[self class] resolvePath:option forDocument:self.documentProvider.document page:annotation.page];
                    if (newOption) {
                        processedOptions[key] = newOption;
                        PSPDFLog(@"Processed %@ to %@", option, [newOption absoluteString]);
                    }
                }
            }];
            annotation.options = processedOptions;
        }

        // !!! also modifies link (since it's mutable)
        annotation.URL = [[self class] resolvePathWithMutableString:link forDocument:self.documentProvider.document page:annotation.page];

        // If we have a #page=x token, remove that first
        NSRange pageTokenRange = [link rangeOfString:@"#page=" options:NSCaseInsensitiveSearch];
        if (pageTokenRange.length > 0) {
            NSString *pageReferenceString = [link substringFromIndex:pageTokenRange.location + pageTokenRange.length];
            NSInteger pageReference = [pageReferenceString integerValue];
            if (pageReference > 0) {
                annotation.pageLinkTarget = pageReference; // pageLinkTarget starts at 1.
            }
            [link deleteCharactersInRange:NSMakeRange(pageTokenRange.location, [link length] - pageTokenRange.location)];
            annotation.URL = [NSURL fileURLWithPath:link];
        }

        // enumerate dict to find a matching file ending
        NSString *loLink = [link lowercaseString];
        __block BOOL typeRecognized = NO;
        [self.fileTypeTranslationTable enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
            if ([loLink hasSuffix:key]) {
                annotation.linkType = [obj unsignedIntegerValue];
                typeRecognized = YES;
                *stop = YES;
            }
        }];

        if (!typeRecognized && [loLink rangeOfString:@"youtube.com/" options:NSCaseInsensitiveSearch].length > 0) {
            typeRecognized = YES;
            annotation.linkType = PSPDFLinkAnnotationYouTube;
        }

        if (!typeRecognized) {
            annotation.linkType = PSPDFLinkAnnotationBrowser;
        }

        // force annotation to be of a specific type
        if ([linkOptions[@"type"] isKindOfClass:[NSString class]]) {
            NSString *manualType = [linkOptions[@"type"] lowercaseString];
            if ([manualType hasSuffix:@"video"]) {
                annotation.linkType = PSPDFLinkAnnotationVideo;
            }else if ([manualType hasSuffix:@"audio"]) {
                annotation.linkType = PSPDFLinkAnnotationAudio;
            }else if ([manualType hasSuffix:@"youtube"]) {
                annotation.linkType = PSPDFLinkAnnotationYouTube;
            }else if ([manualType hasSuffix:@"link"]) {
                annotation.linkType = PSPDFLinkAnnotationWebURL;
            }else if ([manualType hasSuffix:@"image"]) {
                annotation.linkType = PSPDFLinkAnnotationImage;
            }else if ([manualType hasSuffix:@"browser"]) {
                annotation.linkType = PSPDFLinkAnnotationBrowser;
            }else {
                PSPDFLogWarning(@"Unknown type specified: %@", manualType);
            }
        }

        // be specific, so that we can have workaround for Keynote/iBookAuthor limitation for custom protocol in 'webpage' hyperlink, eg if we have myprotocol://, Keynote disallows this, but allows http:myprotocol://
    }else if (![[link lowercaseString] hasPrefix:@"http://"] && ![[link lowercaseString] hasPrefix:@"https://"] && ![[link lowercaseString] hasPrefix:@"mailto:"]) {

        // if we parsed a Launch annotation action, resolve the document
        if (annotation.linkType == PSPDFLinkAnnotationDocument) {
            // ensure documents are treated locally if nothing is sete
            if (!linkContainsLocalhost) {
                [link insertString:@"localhost/" atIndex:0];
            }
            annotation.URL = [[self class] resolvePathWithMutableString:link forDocument:self.documentProvider.document page:annotation.page];
        }else {
            // unknown, don't handle
            annotation.linkType = PSPDFLinkAnnotationCustom;
        }
    }else {
        annotation.linkType = PSPDFLinkAnnotationWebURL;
    }
}

- (void)setFileTypeTranslationTable:(NSDictionary *)fileTypeTranslationTable {
    if (fileTypeTranslationTable != _fileTypeTranslationTable) {
        _fileTypeTranslationTable = fileTypeTranslationTable;
        _videoFileTypes = nil;
    }
}

- (NSArray *)videoFileTypes {
    if (!_videoFileTypes) {
        NSMutableArray *videoFileTypes = [NSMutableArray array];
        [self.fileTypeTranslationTable enumerateKeysAndObjectsUsingBlock:^(NSString *fileExtension, NSNumber *type, BOOL *stop) {
            if ([type integerValue] == PSPDFLinkAnnotationVideo) {
                [videoFileTypes addObject:fileExtension];
            }
        }];
        _videoFileTypes = videoFileTypes;
    }
    return _videoFileTypes;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Annotation Loading/Saving

// return an immutable copy of all cached annotations
- (NSDictionary *)annotations {
    __block NSDictionary *annotationCache;
    pspdf_dispatch_sync_reentrant(_dictCacheQueue, ^{
        annotationCache = [_annotationCache copy];
    });
    return annotationCache;
}

- (NSDictionary *)dirtyAnnotations {
    NSMutableDictionary *dirtyAnnotationsDict = [NSMutableDictionary dictionary];
    pspdf_dispatch_sync_reentrant(_dictCacheQueue, ^{
        [_annotationCache enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
            NSArray *dirtyAnnotations = [obj filteredArrayUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(id evaluatedObject, NSDictionary *bindings) {
                return [evaluatedObject isDirty];
            }]];
            if ([dirtyAnnotations count]) dirtyAnnotationsDict[key] = dirtyAnnotations;
        }];
    });
    return dirtyAnnotationsDict;
}

- (NSUInteger)removeDeletedAnnotations {
    __block NSUInteger removedAnnotationCount = 0;
    pspdf_dispatch_sync_reentrant(_dictCacheQueue, ^{
        [_annotationCache enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
            // obj is an NSMutableArray, key an NSNumber (the page)
            NSUInteger annotationCount = [obj count];
            [obj filterUsingPredicate:[NSPredicate predicateWithFormat:@"isDeleted = NO"]];
            removedAnnotationCount += annotationCount - [obj count];
        }];
    });
    return removedAnnotationCount;
}

- (BOOL)tryLoadAnnotationsFromFileWithError:(NSError **)error {
    // try loading annotations
    NSError *localError = nil;
    NSDictionary *loadedAnnotations = [self loadAnnotationsWithError:&localError];
    if (error) {
        PSPDFLogWarning(@"Error loading annotations: %@", localError);
    }else if (loadedAnnotations){
        _annotationCache = [loadedAnnotations mutableCopy];
    }

    if (error) *error = localError;
    return localError == nil;
}

- (BOOL)shouldEmbeddAnnotations {
    PSPDFAnnotationSaveMode saveMode = self.documentProvider.document.annotationSaveMode;
    BOOL shouldEmbeddAnnotations = saveMode == PSPDFAnnotationSaveModeEmbedded || saveMode == PSPDFAnnotationSaveModeEmbeddedWithExternalFileAsFallback;
    return shouldEmbeddAnnotations;
}

- (BOOL)shouldLoadSaveAnnotationExternally {
    PSPDFAnnotationSaveMode saveMode = self.documentProvider.document.annotationSaveMode;
    BOOL shouldLoadAnnotationsFile = saveMode == PSPDFAnnotationSaveModeExternalFile || saveMode == PSPDFAnnotationSaveModeEmbeddedWithExternalFileAsFallback;
    return shouldLoadAnnotationsFile;
}

/// Load annotations (returning NO + eventually an error if it fails)
- (NSDictionary *)loadAnnotationsWithError:(NSError **)error {
    NSError *localError = nil;
    NSDictionary *loadedAnnotations = nil;

    // if external file is enabled, try loading from a file
    if ([self shouldLoadSaveAnnotationExternally]) {
        NSData *fileData = [NSData dataWithContentsOfFile:[self annotationsPath] options:0 error:&localError];
        if (!fileData) {
            PSPDFErrorWithUnderlyingError(PSPDFErrorCodeFailedToLoadAnnotations, @"Failed to load file.", localError, error);
        }else {
            @try {
                NSKeyedUnarchiver *unarchiver = [[NSKeyedUnarchiver alloc] initForReadingWithData:fileData];

                // check for custom annotation subclasses and make sure they are used when annotations are loaded
                for (Class annotationClass in [pspdf_typeToClassDict allValues]) {
                    Class subclass = [self.documentProvider.document classForClass:annotationClass];
                    if (![subclass isEqual:annotationClass]) {
                        [unarchiver setClass:subclass forClassName:NSStringFromClass(annotationClass)];
                    }
                }
                // same as unarchiveObjectWithData (root object is "root")
                loadedAnnotations = [unarchiver decodeObjectForKey:@"root"];

                // Ensure page and document is correctly set.
                // (prior to 2.4.1 page wasn't even saved at all, and document is always nil)
                PSPDFDocument *document = self.documentProvider.document;
                [loadedAnnotations enumerateKeysAndObjectsUsingBlock:^(NSNumber *pageNumber, NSArray *annotations, BOOL *stop) {
                    NSUInteger page = [pageNumber unsignedIntegerValue];
                    for (PSPDFAnnotation *annotation in annotations) {
                        annotation.page = page;
                        annotation.document = document;
                    }
                }];

            }
            @catch (NSException *exception) {
                PSPDFErrorWithException(PSPDFErrorCodeFailedToLoadAnnotations, [NSString stringWithFormat:@"Failed to load annotations: %@", exception], exception, error);
            }
        }

        // ensure correct document is set
        [loadedAnnotations enumerateKeysAndObjectsUsingBlock:^(NSNumber *key, NSArray *anntations, BOOL *stop) {
            [anntations makeObjectsPerformSelector:@selector(setDocumentProvider:) withObject:self.documentProvider];
        }];
    }
    return loadedAnnotations;
}

/// Save annotations (returning NO + eventually an error if it fails)
- (BOOL)saveAnnotationsWithError:(NSError **)error {
    PSPDFDocumentProvider *documentProvider = self.documentProvider;
    NSError *localError = nil;
    NSDictionary *annotationDict = [self annotations];
    BOOL success = NO;

    PSPDFLogVerbose(@"saving annotations. annotationSaveMode:%d should embedded:%d, can embedd:%d", documentProvider.document.annotationSaveMode, [self shouldEmbeddAnnotations], documentProvider.canEmbedAnnotations);

    if ([self shouldEmbeddAnnotations] && documentProvider.canEmbedAnnotations) {
        success = [documentProvider.documentParser saveAnnotations:annotationDict withError:&localError];
    }

    if (!success && [self shouldLoadSaveAnnotationExternally]) {
        [documentProvider.document ensureCacheDirectoryExistsWithError:&localError];
        @try {
            [NSKeyedArchiver archiveRootObject:_annotationCache toFile:[self annotationsPath]];
            success = YES;
        }
        @catch (NSException *exception) {
            PSPDFErrorWithException(PSPDFErrorCodeFailedToWriteAnnotations, [NSString stringWithFormat:@"Failed to write annotations: %@", exception], exception, &localError);
        }
    }

    if (success) {
        NSUInteger removedDeletedAnnotations = [self removeDeletedAnnotations];
        PSPDFLog(@"Removed %d deleted annotations.", removedDeletedAnnotations);
    }else {
        PSPDFLogWarning(@"Saving annotations failed. (error: %@)", localError);
    }

    if (error) *error = localError;

    return success;
}

- (NSString *)annotationsPath {
    NSString *annotationPath = _annotationsPath ?: [self.documentProvider.document.cacheDirectory stringByAppendingPathComponent:@"annotations.pspdfkit"];

    // support legacy path
    if (![[NSFileManager new] fileExistsAtPath:annotationPath]) {

        // this is a quite weak reference for annotations; but the best solution currently (since a PSPDFDocumentProvider doesn't has a UID)
        NSUInteger documentProviderNumber = [self.documentProvider.document.documentProviders indexOfObject:self.documentProvider];
        annotationPath = _annotationsPath ?: [self.documentProvider.document.cacheDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"annotations_%d.pspdfkit", documentProviderNumber]];
    }

    return annotationPath;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Annotation resolving

+ (NSURL *)resolvePath:(NSString *)path forDocument:(PSPDFDocument *)document page:(NSUInteger)page {
    return [self resolvePathWithMutableString:[path mutableCopy] forDocument:document page:page];
}

+ (NSURL *)resolvePathWithMutableString:(NSMutableString *)link forDocument:(PSPDFDocument *)document page:(NSUInteger)page {
    NSURL *resolvedURL = nil;
    NSString *lowercaseLink = [link lowercaseString];

    BOOL hasHttpInside = [lowercaseLink hasPrefix:@"http"];
    BOOL isLocalFile = [lowercaseLink hasPrefix:@"localhost"] || [lowercaseLink hasPrefix:@"/localhost"];
    if (!hasHttpInside && !isLocalFile) {
        [link insertString:@"http://" atIndex:0];
    }

    if (isLocalFile) {
        // support both lowercase and /lowercase to be extra-compatible
        [link replaceOccurrencesOfString:@"/localhost" withString:@"" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [link length])];
        [link replaceOccurrencesOfString:@"localhost" withString:@"" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [link length])];
        NSString *fileBasePath = [[[document pathForPage:page] path] stringByDeletingLastPathComponent];

        // resolve with delegate callback if unknown token was found.
        PSPDFResolvePathNamesInMutableString(link, fileBasePath, ^NSString *(NSString *unknownPath) {
            NSString *newPath = nil;

            if ([document.delegate respondsToSelector:@selector(pdfDocument:resolveCustomAnnotationPathToken:)]) {
                newPath = [document.delegate pdfDocument:document resolveCustomAnnotationPathToken:unknownPath];
            }
            return newPath;
        });
        resolvedURL = [NSURL fileURLWithPath:link];
    }else {
        resolvedURL = [[NSURL alloc] initWithUnicodeString_pspdf:link];
    }
    return resolvedURL;
}

// Searches the runtime for all subclasses of PSPDFAnnotation and builds up a map.
static __strong NSDictionary *pspdf_typeToClassDict;
static dispatch_once_t pspdf_typeToClassDictOnceToken;
- (Class)annotationClassForType:(NSString *)annotationType {
    dispatch_once(&pspdf_typeToClassDictOnceToken, ^{
        NSMutableDictionary *typeToClassMutable = [NSMutableDictionary dictionary];

        // iterates over 5000 or more classes. But only once, so it's ok.
        unsigned int count = 0;
        Class *classList = objc_copyClassList(&count);
        for(int index=0; index < count; ++index) {
            Class class = classList[index];
            if (class != [PSPDFAnnotation class] && class_getClassMethod(class, @selector(isSubclassOfClass:)) && [class isSubclassOfClass:[PSPDFAnnotation class]]) {
                NSArray *supportedTypes = [class supportedTypes];
                for (NSString *supportedType in supportedTypes) {
                    if (typeToClassMutable[supportedType]) {
                        PSPDFLogWarning(@"Conflict! Class %@ and %@ both implement annotationType %@. The winning class will be random, decided by the runtime. If you want to customize an existing annotation class, use overrideClassNames on PSPDFDocument instead.", NSStringFromClass(typeToClassMutable[supportedType]), NSStringFromClass(class), supportedType);
                    }
                    typeToClassMutable[supportedType] = class;
                }
            }
            pspdf_typeToClassDict = [typeToClassMutable copy];
        }
    });

    Class annotationClass = pspdf_typeToClassDict[annotationType];
    if (!annotationClass) {
        PSPDFLogWarning(@"Annotation type %@ not supported.", annotationType);
        annotationClass = [PSPDFAnnotation class];
    }

    // allow class name overriding.
    return [self.documentProvider.document classForClass:annotationClass];
}

- (PSPDFAnnotationType)annotationTypeForDictionary:(CGPDFDictionaryRef)annotationDictionary document:(CGPDFDocumentRef)documentRef {
    //identifies and validates the given annotation dictionary
    const char *annotationType;
    CGPDFDictionaryGetName(annotationDictionary, "Subtype", &annotationType);
    PSPDFAnnotationType type;

    // Link annotations are identified by Link name stored in Subtype key in annotation dictionary.
    if (strcmp(annotationType, "Link") == 0) {
        // as per previous commits
        type = PSPDFAnnotationTypeLink;

    } else if (strcmp(annotationType, "Highlight") == 0) {
        type = PSPDFAnnotationTypeHighlight;
    } else {
        type = PSPDFAnnotationTypeUndefined;
    }

    return type;
}

- (void)parsePageAnnotation:(PSPDFLinkAnnotation *)annotation dictionary:(CGPDFDictionaryRef)annotationDictionary document:(CGPDFDocumentRef)documentRef page:(NSUInteger)page {
    // Link target can be stored either in A entry or in Dest entry in annotation dictionary.
    // Dest entry is the destination array that we're looking for. It can be a direct array definition
    // or a name. If it is a name, we need to search recursively for the corresponding destination array
    // in document's Dests tree.
    // A entry is an action dictionary. There are many action types, we're looking for GoTo and URI actions.
    // GoTo actions are used for links within the same document. The GoTo action has a D entry which is the
    // destination array, same format like Dest entry in annotation dictionary.
    // URI actions are used for links to web resources. The URI action has a
    // URI entry which is the destination URL.
    // If both entries are present, A entry takes precedence.
    CGPDFArrayRef destArray = NULL;
    CGPDFDictionaryRef actionDictionary = NULL;
    if (CGPDFDictionaryGetDictionary(annotationDictionary, "A", &actionDictionary)) {
        //NSLog(@"%@", PSPDFConvertPDFDictionary(actionDictionary));
        const char* actionType;
        if (CGPDFDictionaryGetName(actionDictionary, "S", &actionType)) {
            if (strcmp(actionType, "GoTo") == 0) {
                if (!CGPDFDictionaryGetArray(actionDictionary, "D", &destArray)) {
                    // D is not an array but a named reference?
                    CGPDFStringRef destNameRef;
                    if (CGPDFDictionaryGetString(actionDictionary, "D", &destNameRef)) {
                        NSString *destinationName = CFBridgingRelease(CGPDFStringCopyTextString(destNameRef));
                        NSMutableSet *annotations = _namedDestinations[destinationName];
                        if (!annotations) {
                            annotations = [NSMutableSet set];
                            _namedDestinations[destinationName] = annotations;
                        }
                        [annotations addObject:annotation];
                    }
                }
            }
            // open web link
            else if (strcmp(actionType, "URI") == 0) {
                CGPDFStringRef uriRef = NULL;
                if (CGPDFDictionaryGetString(actionDictionary, "URI", &uriRef)) {
                    CFStringRef uriStringRef = CGPDFStringCopyTextString(uriRef);
                    if (uriStringRef) {
                        // URLs need to be encoded to UTF8 (we decode them later, but this is needed for punycode URL support)
                        annotation.siteLinkTarget = CFBridgingRelease(CFURLCreateStringByReplacingPercentEscapes(kCFAllocatorDefault, uriStringRef, CFSTR("")));
                        CFRelease(uriStringRef);
                    }
                    [self parseAnnotationLinkTarget:annotation];
                }
            }
            // launch a file
            else if (strcmp(actionType, "Launch") == 0 || strcmp(actionType, "GoToR") == 0) {
                BOOL isRemote = strcmp(actionType, "GoToR") == 0;
                CGPDFDictionaryRef fDictRef = NULL;
                if (CGPDFDictionaryGetDictionary(actionDictionary, "F", &fDictRef)) {
                    NSString *path = PSPDFDictionaryGetString(fDictRef, @"F");
                    if ([path length]) {
                        annotation.siteLinkTarget = path;
                        annotation.linkType = PSPDFLinkAnnotationDocument;
                        [self parseAnnotationLinkTarget:annotation];
                    }
                }

                // parse target page.
                if (isRemote) {
                    CGPDFArrayRef dArrayRef = NULL;
                    if (CGPDFDictionaryGetArray(actionDictionary, "D", &dArrayRef)) {
                        CGPDFReal pageIndex;
                        // TODO: should we use the over values of the D array? Are those named references?
                        if (CGPDFArrayGetNumber(dArrayRef, 0, &pageIndex)) {
                            annotation.pageLinkTarget = pageIndex;
                        }
                    }
                }
            }
            // Named = execute a menu item. support simple gestures like Next Page, Previous Page
            else if (strcmp(actionType, "Named") == 0) {
                // page of pageLinkTarget has index of 1.
                NSString *nameAction = PSPDFDictionaryGetString(actionDictionary, @"N");
                if ([nameAction isEqualToString:@"NextPage"]) {
                    annotation.pageLinkTarget = page+2;
                }else if ([nameAction isEqualToString:@"PrevPage"]) {
                    annotation.pageLinkTarget = page;
                }else if ([nameAction isEqualToString:@"FirstPage"]) {
                    annotation.pageLinkTarget = 1;
                }else if ([nameAction isEqualToString:@"LastPage"]) {
                    annotation.pageLinkTarget = CGPDFDocumentGetNumberOfPages(documentRef);
                }
                else {
                    PSPDFLogWarning(@"Named action %@ not supported.", nameAction);
                }
            }
            else {
                // See PDF spec 1.7, Page 417+
                PSPDFLogWarning(@"actionType %s not supported.", actionType);
                //NSLog(@"annotation: %@", PSPDFConvertPDFDictionary(annotationDictionary));
            }
        }
    } else {
        // Dest entry can be either a string object or an array object.
        if (!CGPDFDictionaryGetArray(annotationDictionary, "Dest", &destArray)) {
            CGPDFStringRef destName;
            if (CGPDFDictionaryGetString(annotationDictionary, "Dest", &destName)) {
                // Traverse the Dests tree to locate the destination array.
                CGPDFDictionaryRef catalogDictionary = CGPDFDocumentGetCatalog(documentRef);
                CGPDFDictionaryRef namesDictionary = NULL;
                if (CGPDFDictionaryGetDictionary(catalogDictionary, "Names", &namesDictionary)) {
                    CGPDFDictionaryRef destsDictionary = NULL;
                    if (CGPDFDictionaryGetDictionary(namesDictionary, "Dests", &destsDictionary)) {
                        destArray = [self findDestinationByName:destName inDestsTree:destsDictionary];
                    }
                }
            }
        }
    }

    if (destArray != NULL) {
        int targetPageNumber = 0;
        // First entry in the array is the page the links points to.
        CGPDFDictionaryRef pageDictionaryFromDestArray = NULL;
        if (CGPDFArrayGetDictionary(destArray, 0, &pageDictionaryFromDestArray)) {
            int documentPageCount = CGPDFDocumentGetNumberOfPages(documentRef);
            for (int i = 1; i <= documentPageCount; i++) {
                CGPDFPageRef pageRef = CGPDFDocumentGetPage(documentRef, i);
                CGPDFDictionaryRef pageDictionaryFromPage = CGPDFPageGetDictionary(pageRef);
                if (pageDictionaryFromPage == pageDictionaryFromDestArray) {
                    targetPageNumber = i;
                    break;
                }
            }
        } else {
            // Some PDF generators use incorrectly the page number as the first element of the array
            // instead of a reference to the actual page.
            CGPDFInteger pageNumber = 0;
            if (CGPDFArrayGetInteger(destArray, 0, &pageNumber)) {
                targetPageNumber = pageNumber + 1;
            }
        }

        if (targetPageNumber > 0) {
            annotation.pageLinkTarget = targetPageNumber;
        }
    }
}

- (CGPDFArrayRef)findDestinationByName:(CGPDFStringRef)destName inDestsTree:(CGPDFDictionaryRef)node {
    const char *destinationName = (const char *)CGPDFStringGetBytePtr(destName);
    NSString *destString = CFBridgingRelease(CGPDFStringCopyTextString(destName));
    PSPDFLogVerbose(@"destination name: %@", destString);

    CGPDFArrayRef destinationArray = NULL;
    CGPDFArrayRef limitsArray = NULL;

    // speed up search with respecting the limits table
    if (CGPDFDictionaryGetArray(node, "Limits", &limitsArray)) {
        CGPDFStringRef lowerLimit = NULL;
        CGPDFStringRef upperLimit = NULL;
        if (CGPDFArrayGetString(limitsArray, 0, &lowerLimit)) {
            if (CGPDFArrayGetString(limitsArray, 1, &upperLimit)) {
                const unsigned char *ll = CGPDFStringGetBytePtr(lowerLimit);
                const unsigned char *ul = CGPDFStringGetBytePtr(upperLimit);
                if ((strcmp(destinationName, (const char*)ll) < 0) ||
                    (strcmp(destinationName, (const char*)ul) > 0)) {
                    return NULL;
                }
            }
        }
    }

    CGPDFArrayRef namesArray = NULL;
    if (CGPDFDictionaryGetArray(node, "Names", &namesArray)) {
        int namesCount = CGPDFArrayGetCount(namesArray);
        for (int i = 0; i < namesCount; i = i + 2) {
            CGPDFStringRef otherDestName;
            if (CGPDFArrayGetString(namesArray, i, &otherDestName)) {
                // Note: initially the code used a strcmp with CGPDFStringGetBytePtr (which is faster)
                // Problem is, there are some documents where this comparison fails.
                NSString *otherDestString = CFBridgingRelease(CGPDFStringCopyTextString(otherDestName));
                if ([destString isEqualToString:otherDestString]) {
                    CGPDFDictionaryRef destinationDictionary = NULL;
                    if (CGPDFArrayGetDictionary(namesArray, i + 1, &destinationDictionary)) {
                        if (CGPDFDictionaryGetArray(destinationDictionary, "D", &destinationArray)) {
                            return destinationArray;
                        }
                    }
                }
            }
        }
    }

    CGPDFArrayRef kidsArray = NULL;
    if (CGPDFDictionaryGetArray(node, "Kids", &kidsArray)) {
        int kidsCount = CGPDFArrayGetCount(kidsArray);
        for (int i = 0; i < kidsCount; i++) {
            CGPDFDictionaryRef kidNode = NULL;
            if (CGPDFArrayGetDictionary(kidsArray, i, &kidNode)) {
                destinationArray = [self findDestinationByName:destName inDestsTree:kidNode];
                if (destinationArray != NULL) {
                    return destinationArray;
                }
            }
        }
    }
    
    return NULL;
}

@end
