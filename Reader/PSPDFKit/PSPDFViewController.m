//
//  PSPDFViewController.m
//  PSPDFKit
//
//  Copyright 2011-2012 Peter Steinberger. All rights reserved.
//

#import "PSPDFKit.h"
#import "PSPDFKitGlobal+Internal.h"
#import "PSPDFViewController+Delegates.h"
#import "PSPDFViewController+Internal.h"
#import "PSPDFViewControllerDelegate.h"
#import "PSPDFPageScrollViewController.h"
#import "PSPDFContinuousScrollViewController.h"
#import "PSPDFWebViewController.h"
#import "PSPDFAnnotationController.h"
#import "PSPDFIconGenerator.h"
#import "PSPDFPageViewController.h"
#import "PSPDFContentScrollView.h"
#import "PSPDFThumbnailGridViewCell.h"
#import "PSPDFTransparentToolbar.h"
#import "PSPDFDocumentLabelView.h"
#import "PSPDFTransitionHelper.h"
#import "PSPDFBarButtonItem.h"
#import "PSPDFViewModeBarButtonItem.h"
#import "PSPDFCloseBarButtonItem.h"
#import "PSPDFEmailBarButtonItem.h"
#import "PSPDFMoreBarButtonItem.h"
#import "PSPDFOpenInBarButtonItem.h"
#import "PSPDFOutlineBarButtonItem.h"
#import "PSPDFPrintBarButtonItem.h"
#import "PSPDFSearchBarButtonItem.h"
#import "PSPDFAnnotationBarButtonItem.h"
#import "PSPDFBookmarkBarButtonItem.h"
#import "PSPDFAnnotationToolbar.h"
#import "PSPDFBrightnessBarButtonItem.h"
#import "PSPDFActivityBarButtonItem.h"
#import "UIImage+PSPDFKitAdditions.h"
#import "PSPDFNavigationAppearanceSnapshot.h"
#import "NSURL+PSPDFUnicodeURL.h"
#import "PSPDFPopoverBackgroundView.h"
#import "PSPDFPopoverController.h"
#import <QuartzCore/QuartzCore.h>
#import <objc/runtime.h>
#import <libkern/OSAtomic.h>

#if !__has_feature(objc_arc)
#error "PSPDFKit needs to be compiled with ARC enabled."
#endif

NSString *const PSPDFFixNavigationBarFrameNotification = @"PSPDFFixNavigationBarFrameNotification";
NSString *const PSPDFHUDNotHiddenNotification = @"PSPDFHUDNotHiddenNotification";
NSString *const PSPDFViewControllerWillChangeOrientationNotification = @"PSPDFViewControllerWillChangeOrientationNotification";
NSString *const PSPDFViewControllerWillAnimateChangeOrientationNotification = @"PSPDFViewControllerWillAnimateChangeOrientationNotification";
NSString *const PSPDFViewControllerDidChangeOrientationNotification = @"PSPDFViewControllerDidChangeOrientationNotification";

@interface PSPDFContentView : PSPDFHUDView @end @implementation PSPDFContentView @end

@interface PSPDFViewController() <UIGestureRecognizerDelegate> {
    PSPDFBarButtonItem *_closeButtonItem, *_outlineButtonItem, *_searchButtonItem;
    PSPDFBarButtonItem *_viewModeButtonItem, *_printButtonItem, *_openInButtonItem;
    PSPDFBarButtonItem *_emailButtonItem, *_annotationButtonItem, *_bookmarkButtonItem;
    PSPDFBarButtonItem *_brightnessButtonItem, *_activityButtonItem, *_additionalActionsButtonItem;
    PSPDFNavigationAppearanceSnapshot *_navBarAppearanceSnapshot;
    NSArray *_leftBarButtonItems;
    CGFloat _lastContentOffset;
    CGFloat _lastStatusBarHeight;
    NSInteger _targetPageAfterRotate;
    NSUInteger _lastPage;
    NSUInteger _lockCount;
    CGRect _lastViewRect;
    struct {
        unsigned int isReloading:1;
        unsigned int documentRectCacheLoaded:1;
        unsigned int viewWillAppearAnimatedActive:1;
        unsigned int viewDidAppearCalled:1; // for a iOS5 bugfix workaround
        unsigned int gridNeedsScrollToPage:1;

        // internally only
        unsigned int savedRotationLock:1;
        unsigned int savedViewLock:1;
        unsigned int savedTextSelection:1;
        unsigned int rightBarButtonItemsSet:1;
    } _flags;
}
@property (nonatomic, weak) UINavigationController *internalNavigationController;
@property (nonatomic, copy) NSDictionary *overrideClassNames;
@property (nonatomic, assign) PSPDFDelegateFlags delegateFlags;
@property (nonatomic, strong) PSPDFViewState *restoreViewStatePending;
@property (nonatomic, strong) PSUICollectionView *gridView;
@property (nonatomic, strong) PSPDFHUDView *HUDView;
@property (nonatomic, strong) PSPDFHUDView *contentView;
@property (nonatomic, strong) PSPDFPageLabelView *pageLabel;
@property (nonatomic, strong) PSPDFDocumentLabelView *documentLabel;
@property (nonatomic, strong) PSPDFPasswordView *passwordView;
@property (nonatomic, assign, getter=isViewVisible) BOOL viewVisible;
@property (nonatomic, assign, getter=isNavigationBarHidden) BOOL navigationBarHidden;
@property (nonatomic, assign, getter=isRotationActive) BOOL rotationActive;
//@property (nonatomic, assign) NSUInteger page;
@property (nonatomic, strong) UIToolbar *leftToolbar;
@property (nonatomic, strong) UIToolbar *rightToolbar;
@property (nonatomic, strong) PSPDFScrobbleBar *scrobbleBar;
@property (nonatomic, strong) UIScrollView *pagingScrollView;
@property (nonatomic, strong) UIViewController<PSPDFTransitionProtocol> *pageTransitionController;
@property (nonatomic, strong) PSPDFAnnotationController *annotationController;
@property (atomic, copy) void (^updateSettingsForRotationBlock)(PSPDFViewController *pdfController, UIInterfaceOrientation toInterfaceOrientation);
void PSPDFShowDemoInfo(void);
@end

// Simple subclass to relay frame change events.
@interface PSPDFViewControllerView : UIView @end

@implementation PSPDFViewController

static int32_t _numberOfInstances = 0; // last visible instance cleans up the cache
@synthesize popoverController = popoverController_; // _popoverController is reserved by UIKit
@synthesize rightBarButtonItems = _rightBarButtonItems;

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (void)commonInitWithDocument:(PSPDFDocument *)document {
    PSPDFLogVerbose(@"[%@] Initalize %@ with document %@", PSPDFVersionString(), NSStringFromClass([self class]), document);
    PSPDFShowDemoInfo();
    NSNotificationCenter *dnc = [NSNotificationCenter defaultCenter];
    [dnc addObserver:self selector:@selector(fixNavigationBarFrame:) name:PSPDFFixNavigationBarFrameNotification object:nil];
    // add handler to save document before app goes to background
    [dnc addObserver:self selector:@selector(applicationDidEnterBackground:) name:UIApplicationDidEnterBackgroundNotification object:nil];
    [dnc addObserver:self selector:@selector(statusBarFrameWillChange:) name:UIApplicationWillChangeStatusBarFrameNotification object:nil];
    _viewMode = PSPDFViewModeDocument;
    [self setDocumentInternal:document];
    _annotationController = [[PSPDFAnnotationController alloc] initWithPDFController:self];
    _renderAnnotationTypes = PSPDFAnnotationTypeAll;
    _targetPageAfterRotate = 1; // on 0, first page would be omitted
    _iPhoneThumbnailSizeReductionFactor = 0.5f;
    _scrobbleBarEnabled = YES;
    _pageLabelEnabled = YES;
    _documentLabelEnabled = !PSIsIpad();
    _renderAnimationEnabled = YES;
    _toolbarEnabled = YES;
    _scrollOnTapPageEndEnabled = YES;
    _scrollOnTapPageEndMargin = 60;
    _smartZoomEnabled = YES;
    _zoomingSmallDocumentsEnabled = YES;
    _passwordDialogEnabled = YES;
    _scrollDirection = PSPDFScrollDirectionHorizontal;
    _pageMode = PSIsIpad() ? PSPDFPageModeAutomatic : PSPDFPageModeSingle;
    _statusBarStyleSetting = PSPDFStatusBarSmartBlackHideOnIpad;
    _linkAction = PSPDFLinkActionInlineBrowser;
    _doublePageModeOnFirstPage = NO;
    _lastPage = NSNotFound; // for delegates
    _page = 0;
    _minimumZoomScale = 1.f;
    _maximumZoomScale = 10.f;
    _shadowEnabled = YES;
    _pagePadding = 20.f;
    _scrollingEnabled = YES;
    _clipToPageBoundaries = YES;
    _thumbnailSize = CGSizeMake(170.f, 220.f);
    _annotationAnimationDuration = 0.25f;
    _textSelectionEnabled = YES;
    _imageSelectionEnabled = YES;
    _internalTapGesturesEnabled = YES;
    _renderContentOpacity = 1.f;
    _margin = UIEdgeInsetsZero;
    _padding = CGSizeZero;
    _HUDViewMode = PSPDFHUDViewAutomatic;
    _shouldTintPopovers = YES;
    _createAnnotationMenuEnabled = YES;
    OSAtomicIncrement32(&_numberOfInstances);
}

- (id)init {
    return self = [self initWithDocument:nil]; // ensure to always call initWithDocument
}

- (id)initWithCoder:(NSCoder *)decoder {
    if ((self = [super initWithCoder:decoder])) {
        [self commonInitWithDocument:nil];
    }
    return self;
}

- (id)initWithDocument:(PSPDFDocument *)document {
    if ((self = [super init])) {
        [self commonInitWithDocument:document];
    }
    return self;
}

- (void)dealloc {
    // "the deallocation problem" - not safe to dealloc controller from thread different than main.
    //NSAssert([NSThread isMainThread], @"Must run on main thread, see http://developer.apple.com/library/ios/#technotes/tn2109/_index.html#//apple_ref/doc/uid/DTS40010274-CH1-SUBSECTION11");
    PSPDFLogVerbose(@"Deallocating %@", self);
    if (![NSThread isMainThread]) {
        PSPDFLogWarning(@"Should should on main thread, see http://developer.apple.com/library/ios/#technotes/tn2109/_index.html#//apple_ref/doc/uid/DTS40010274-CH1-SUBSECTION11");
    }

    // cancel operations, nil out delegates
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(preloadNextThumbnails) object:nil];
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(scheduledRestoreViewState) object:nil];
    NSNotificationCenter *dnc = [NSNotificationCenter defaultCenter];
    [dnc removeObserver:self name:PSPDFFixNavigationBarFrameNotification object:nil];
    [dnc removeObserver:self name:UIApplicationDidEnterBackgroundNotification object:nil];
    [dnc removeObserver:self name:UIApplicationWillChangeStatusBarFrameNotification object:nil];

    // remove any delegate references
    [self clearDocument];

    _delegate = nil;
    [[PSPDFGlobalLock sharedGlobalLock] requestClearCacheAndWait:NO]; // request a clear cache

    // if pageCurl is enabled, nil out the delegates here.
    if ([_pagingScrollView respondsToSelector:@selector(setPdfController:)]) {
        [(PSPDFContentScrollView *)_pagingScrollView setPdfController:nil];
    }

    self.passwordView = nil;
    _gridView.delegate = nil;
    _gridView.dataSource = nil;
    _pagingScrollView.delegate = nil;
    _pageTransitionController.pdfController = nil;
    _scrobbleBar.pdfController = nil;          // deregisters KVO
    _pageLabel.pdfController = nil;            // deregisters KVO
    _documentLabel.pdfController = nil;        // deregisters KVO
    _bookmarkButtonItem.pdfController = nil;   // force deregister KVO to not get KVO errors when zombies are enabled
    _annotationButtonItem.pdfController = nil; // async operation
    _outlineButtonItem.pdfController = nil;    // async operation

    // force-nil out all used bar buttons. (can also be regular UIBarButtonItems, so check for selector)
    for (PSPDFBarButtonItem *barButton in self.leftBarButtonItems) {
        if ([barButton respondsToSelector:@selector(setPdfController:)]) barButton.pdfController = nil;
    }
    for (PSPDFBarButtonItem *barButton in self.rightBarButtonItems) {
        if ([barButton respondsToSelector:@selector(setPdfController:)]) barButton.pdfController = nil;
    }
    for (PSPDFBarButtonItem *barButton in self.additionalBarButtonItems) {
        if ([barButton respondsToSelector:@selector(setPdfController:)]) barButton.pdfController = nil;
    }

    // release rotation lock if still locked
    if (self.isRotationLockEnabled) PSPDFUnlockRotation();

    // Clear memory after the last PSPDFViewController has been deallocated.
    OSAtomicDecrement32Barrier(&_numberOfInstances);
    if (_numberOfInstances == 0) {
        pspdf_dispatch_async_if(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), !PSPDFIsCrappyDevice(), ^{
            [[PSPDFCache sharedCache] clearMemoryCache];
        });
    }
}

- (NSString *)description {
    return [NSString stringWithFormat:@"<%@:%p page:%d pageTransition:%d viewMode:%d visiblePageNumbers:%@ fitToWidthEnabled:%d>", NSStringFromClass([self class]), self, self.page, self.pageTransition, self.viewMode, [[self visiblePageNumbers] componentsJoinedByString:@","], self.isFitToWidthEnabled];
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UIView related

// Helper that returns the transparent height for the HUD. Used in thumbnail scroll offset.
- (CGFloat)hudTransparencyOffset {
    CGFloat hudTransparencyOffset = [self contentRect].origin.y;

    // Ideally, PSPDFTabbedViewController should override PSPDFViewController, but since it can be subclassed and the pdfController is an argument, it's trickier. So bake this into the default implementation.
    // The actual value is hard-coded since PSPDFTabBarView has an increased height to easier accept touch targets (and it's quite tricky to get pointInside: right with the scrollView hiararchy)
    if ([self.parentViewController isKindOfClass:[PSPDFTabbedViewController class]]) {
        hudTransparencyOffset += 30.f;
    }
    return hudTransparencyOffset;
}

// we need to know the last known status bar height for the VISIBLE statusbar.
// This is harder than it should be, but better than hard-coding the value.
- (void)statusBarFrameWillChange:(NSNotification *)notification {
    CGRect newStatusBarFrame = [[notification.userInfo valueForKey:UIApplicationStatusBarFrameUserInfoKey] CGRectValue];
    CGFloat lastStatusBarHeight = fminf(newStatusBarFrame.size.height, newStatusBarFrame.size.width);
    if (lastStatusBarHeight > 0) _lastStatusBarHeight = lastStatusBarHeight;
}

// Return rect of the content view area excluding translucent toolbar/statusbar.
- (CGRect)contentRect {
    CGRect contentRect = self.view.bounds;

    // subtract navigationBar/statusbar
    if (self.internalNavigationController.navigationBar.barStyle == UIBarStyleBlackTranslucent || self.internalNavigationController.navigationBar.translucent) {
        CGFloat navBarHeight = fminf(self.internalNavigationController.navigationBar.frame.size.width, self.internalNavigationController.navigationBar.frame.size.height);
        BOOL transparentStatusBarOrHiding = [[UIApplication sharedApplication] statusBarStyle] == UIStatusBarStyleBlackTranslucent || (self.wantsFullScreenLayout && self.statusBarStyleSetting == PSPDFStatusBarSmartBlackHideOnIpad);

        // statusBarFrame is already rotated. Try to get height, use _lastStatusBarHeight if statusBar is currently hidden.
        CGFloat statusBarHeight = fminf([[UIApplication sharedApplication] statusBarFrame].size.height, [[UIApplication sharedApplication] statusBarFrame].size.width);
        if (statusBarHeight == 0) statusBarHeight = _lastStatusBarHeight;

        CGFloat translucentStatusBarHeight = transparentStatusBarOrHiding ? statusBarHeight : 0;
        contentRect.origin.y += navBarHeight + translucentStatusBarHeight;
        contentRect.size.height -= navBarHeight + translucentStatusBarHeight;
    }
    return contentRect;
}

// Searches the active root/modal viewController. We can't use our parent, we maybe are embedded.
- (UIViewController *)masterViewController {
    UIViewController *masterViewController = [UIApplication sharedApplication].keyWindow.rootViewController;

    // use topmost modal view
    while (masterViewController.modalViewController) {
        masterViewController = masterViewController.modalViewController;
    }

    // get visible controller in a navigation controller
    if ([masterViewController isKindOfClass:[UINavigationController class]]) {
        masterViewController = [(UINavigationController *)masterViewController topViewController];
    }

    // if that didn't work out, try if there's a parent
    if (!masterViewController) {
        masterViewController = self.parentViewController;
    }

    // if the controller we found is not visible at all; better use ourself
    // this fixes strange situations where view controllers are incorrectly embedded and the hierarchy is broken.
    if (!masterViewController || ![masterViewController isViewLoaded] || !masterViewController.view.window) {
        masterViewController = self;
    }

    return masterViewController;
}

// helper
- (BOOL)isHorizontalScrolling {
    return self.scrollDirection == PSPDFScrollDirectionHorizontal;
}

- (BOOL)isPageCurlAllowed {
    BOOL allowPageCurl = YES;
#ifdef _PSPDFKIT_DONT_USE_OBFUSCATED_PRIVATE_API_
    if (allowPageCurl) {
        PSPDFLogWarning(@"Unable to enable pageCurl as you disabled the needed fixes.");
        PSPDFLogWarning(@"Remove _PSPDFKIT_DONT_USE_OBFUSCATED_PRIVATE_API_ to enable the pageCurl feature.");
        allowPageCurl = NO;
    }
#endif
    return allowPageCurl;
}

- (void)setPageTransition:(PSPDFPageTransition)pageTransition {
    if (pageTransition == PSPDFPageCurlTransition) {
        if (![self isPageCurlAllowed]) {
            PSPDFLogWarning(@"PSPDFPageCurlTransition is now allowed. Falling back to PSPDFPageScrollPerPageTransition.");
            pageTransition = PSPDFPageScrollPerPageTransition;
        }
    }
    if (pageTransition != _pageTransition) {
        _pageTransition = pageTransition;
        [self reloadData];
    }
}

- (void)setPageCurlDirectionLeftToRight:(BOOL)pageCurlDirectionLeftToRight {
    if (pageCurlDirectionLeftToRight != _pageCurlDirectionLeftToRight) {
        _pageCurlDirectionLeftToRight = pageCurlDirectionLeftToRight;
        [self reloadData];
    }
}

- (void)removeContentController {
    // remove current pdf display classes
    self.pagingScrollView.delegate = nil;
    [self.pagingScrollView removeFromSuperview];

    // remove transition controller
    [self.pageTransitionController willMoveToParentViewController:nil];
    [self.pageTransitionController removeFromParentViewController];
    self.pageTransitionController.pdfController = nil;
    self.pageTransitionController = nil;
}

// coordinates for the global scrollview
- (CGRect)boundsForPagingScrollView {
    CGRect bounds = UIEdgeInsetsInsetRect(self.view.bounds, self.margin);

    // extend the bounds for this special transition
    if (self.pageTransition == PSPDFPageScrollPerPageTransition) {
        if ([self isHorizontalScrolling]) {
            bounds.origin.x -= self.pagePadding;
            bounds.size.width += 2 * self.pagePadding;
        }else {
            bounds.origin.y -= self.pagePadding;
            bounds.size.height += 2 * self.pagePadding;
        }
    }
    return bounds;
}

- (void)createContentController {
    Class contentControllerClass;
    switch (self.pageTransition) {
        case PSPDFPageCurlTransition: contentControllerClass = [PSPDFPageViewController class]; break;
        case PSPDFPageScrollContinuousTransition: contentControllerClass = [PSPDFContinuousScrollViewController class]; break;
        default: contentControllerClass = nil;
    }
    if (contentControllerClass) {
        [self removeContentController];
        UIViewController<PSPDFTransitionProtocol> *contentController = [[[self classForClass:contentControllerClass] alloc] initWithPDFController:self];

        contentController.view.frame = self.view.bounds;
        contentController.view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        [self addChildViewController:contentController];

        self.pageTransitionController = contentController;
        PSPDFContentScrollView *pagedScrollView = [[[self classForClass:[PSPDFContentScrollView class]] alloc] initWithTransitionViewController:contentController];
        pagedScrollView.frame = UIEdgeInsetsInsetRect(self.view.bounds, self.margin);
        pagedScrollView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        [self.view insertSubview:pagedScrollView belowSubview:self.HUDView];
        [contentController didMoveToParentViewController:self];
        self.pagingScrollView = pagedScrollView;
    }else {
        // Default scrolling mode.
        // Try to re-use the paging view controller if possible.
        PSPDFPageScrollViewController *pagingViewController = (PSPDFPageScrollViewController *)self.pageTransitionController;

        if (![self.pageTransitionController isKindOfClass:[self classForClass:[PSPDFPageScrollViewController class]]]) {
            [self removeContentController];
            pagingViewController = [[[self classForClass:[PSPDFPageScrollViewController class]] alloc] initWithPDFController:self];
            [self addChildViewController:pagingViewController];
            [self.view insertSubview:pagingViewController.view belowSubview:self.HUDView];

            self.pageTransitionController = pagingViewController;
            self.pagingScrollView = pagingViewController.pagingScrollView;
            [pagingViewController didMoveToParentViewController:self];
        }else {
            [pagingViewController reloadData];
        }

        // Timing is critical here. setting the frame invokes a tilePages event that calls the delegates.
        pagingViewController.view.frame = [self boundsForPagingScrollView];
        pagingViewController.view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    }
}

- (BOOL)isTransparentHUD {
    return [self statusBarStyle] != UIStatusBarStyleDefault;
}

- (BOOL)isDarkHUD {
    BOOL isDarkHUD = [self isTransparentHUD];
    if (![self isTransparentHUD] && self.tintColor) {
        const CGFloat *components = CGColorGetComponents(self.tintColor.CGColor);
        CGFloat brightness = 0;
        if (components) {
            brightness = (components[0] * 299 + components[1] * 587 + components[2] * 114)/1000.f;
        }
        isDarkHUD = brightness < 0.6f;
    }
    return isDarkHUD;
}

- (void)updatePositionViewPosition {
    if (self.pageLabel) {
        CGFloat positionViewHeight = self.view.bounds.origin.y + self.view.bounds.size.height;
        if (self.isScrobbleBarEnabled && self.viewMode != PSPDFViewModeThumbnails) {
            positionViewHeight -= PSPDFToolbarHeightForOrientation(self.interfaceOrientation);
        }
        CGRect frame = CGRectIntegral(CGRectMake(self.view.bounds.size.width/2, positionViewHeight - 30.f, 0.f, 0.f)); // size is inferred automatically
        self.pageLabel.frame = frame;
    }
}

- (void)updateDocumentLabelViewPosition {
    if (self.documentLabel) {
        CGFloat positionViewHeight = [self contentRect].origin.y + 25.f;
        CGRect frame = CGRectIntegral(CGRectMake(self.view.bounds.size.width/2, positionViewHeight, 0.f, 0.f)); // size is inferred automatically
        self.documentLabel.frame = frame;
    }
}

// Helper that adds the position view if controller is configured to show it
- (void)addPageLabelViewToHUD {
    if (self.isPageLabelEnabled && self.HUDView && !self.pageLabel.superview) {
        self.pageLabel.pdfController = self;
        [self.HUDView addSubview:self.pageLabel];
        [self updatePositionViewPosition];
    }
}

- (PSPDFPageLabelView *)pageLabel {
    if (!_pageLabel) _pageLabel = [[[self classForClass:[PSPDFPageLabelView class]] alloc] initWithFrame:CGRectZero];
    return _pageLabel;
}

- (void)addDocumentLabelView {
    if (self.isDocumentLabelEnabled && self.HUDView && !self.documentLabel.superview) {
        self.documentLabel.pdfController = self;
        [self.HUDView addSubview:self.documentLabel];
        self.documentLabel = self.documentLabel;
        [self updateDocumentLabelViewPosition];
    }
}

- (PSPDFDocumentLabelView *)documentLabel {
    if (!_documentLabel) _documentLabel = [[[self classForClass:[PSPDFDocumentLabelView class]] alloc] initWithFrame:CGRectZero];
    return _documentLabel;
}

// Helper that initializes the scrobble bar
- (void)addScrobbleBarToHUD {
    if (self.isScrobbleBarEnabled && !self.scrobbleBar && self.HUDView) {
        if (!self.scrobbleBar) {
            self.scrobbleBar = [[[self classForClass:[PSPDFScrobbleBar class]] alloc] init];
            self.scrobbleBar.pdfController = self;
        }
        [self.HUDView addSubview:self.scrobbleBar];
    }
}

// lazily creates a property if nil
- (id)lazyInitProperty:(id __strong *)propertyAddress withClass:(Class)itemClass {
    if (!(*propertyAddress)) {
        *propertyAddress = [[[self classForClass:itemClass] alloc] initWithPDFViewController:self];
    }
    return *propertyAddress;
}

- (PSPDFCloseBarButtonItem *)closeButtonItem {
    return [self lazyInitProperty:&_closeButtonItem withClass:[PSPDFCloseBarButtonItem class]];
}

- (PSPDFOutlineBarButtonItem *)outlineButtonItem {
    return [self lazyInitProperty:&_outlineButtonItem withClass:[PSPDFOutlineBarButtonItem class]];
}

- (PSPDFSearchBarButtonItem *)searchButtonItem {
    return [self lazyInitProperty:&_searchButtonItem withClass:[PSPDFSearchBarButtonItem class]];
}

- (PSPDFViewModeBarButtonItem *)viewModeButtonItem {
    return [self lazyInitProperty:&_viewModeButtonItem withClass:[PSPDFViewModeBarButtonItem class]];
}

- (PSPDFViewModeBarButtonItem *)viewModeItem {
    return self.viewModeButtonItem;
}

- (PSPDFPrintBarButtonItem *)printButtonItem {
    return [self lazyInitProperty:&_printButtonItem withClass:[PSPDFPrintBarButtonItem class]];
}

- (PSPDFOpenInBarButtonItem *)openInButtonItem {
    return [self lazyInitProperty:&_openInButtonItem withClass:[PSPDFOpenInBarButtonItem class]];
}

- (PSPDFEmailBarButtonItem *)emailButtonItem {
    return [self lazyInitProperty:&_emailButtonItem withClass:[PSPDFEmailBarButtonItem class]];
}

- (PSPDFAnnotationBarButtonItem *)annotationButtonItem {
    return [self lazyInitProperty:&_annotationButtonItem withClass:[PSPDFAnnotationBarButtonItem class]];
}

- (PSPDFBookmarkBarButtonItem *)bookmarkButtonItem {
    return [self lazyInitProperty:&_bookmarkButtonItem withClass:[PSPDFBookmarkBarButtonItem class]];
}

- (PSPDFBrightnessBarButtonItem *)brightnessButtonItem {
    return [self lazyInitProperty:&_brightnessButtonItem withClass:[PSPDFBrightnessBarButtonItem class]];
}

- (PSPDFActivityBarButtonItem *)activityButtonItem {
    return [self lazyInitProperty:&_activityButtonItem withClass:[PSPDFActivityBarButtonItem class]];
}

- (PSPDFMoreBarButtonItem *)additionalActionsButtonItem {
    return [self lazyInitProperty:&_additionalActionsButtonItem withClass:[PSPDFMoreBarButtonItem class]];
}

- (void)setToolbarProperty:(id __strong *)propertyAddress array:(NSArray *)newArray {
    // as leftBarButtonItems is automatically created if set to nil, don't allow nil values here.
    *propertyAddress = [newArray copy] ?: @[];

    // optimization if toolbar is changed during updateSettingsForRotation; don't uselessly reload
    if (!_flags.viewWillAppearAnimatedActive) {
        [self createToolbarAnimated:NO];
    }
}

- (NSArray *)leftBarButtonItems {
    if (!_leftBarButtonItems) {
        UIViewController *topViewController = [[self navigationController] viewControllers][0];
        if (self == topViewController || self.parentViewController == topViewController) {
            _leftBarButtonItems = @[self.closeButtonItem];
        }else {
            _leftBarButtonItems = [NSArray new];
        }
    }
    return _leftBarButtonItems;
}

- (void)setLeftBarButtonItems:(NSArray *)leftBarButtonItems {
    [self setToolbarProperty:&_leftBarButtonItems array:leftBarButtonItems];
}

// As the buttons might be overridden via overrideClassNames, we need to create them lazily.
- (NSArray *)rightBarButtonItems {
    if (![_rightBarButtonItems count] && !_flags.rightBarButtonItemsSet) {
        _rightBarButtonItems = @[self.searchButtonItem, self.outlineButtonItem, self.viewModeButtonItem];
    }
    return _rightBarButtonItems;
}

- (void)setRightBarButtonItems:(NSArray *)rightBarButtonItems {
    _flags.rightBarButtonItemsSet = YES;
    [self setToolbarProperty:&_rightBarButtonItems array:rightBarButtonItems];
}

- (void)setAdditionalBarButtonItems:(NSArray *)additionalBarButtonItems {
    [self setToolbarProperty:&_additionalBarButtonItems array:additionalBarButtonItems];
}

- (void)setBarButtonItemsAlwaysEnabled:(NSArray *)barButtonItemsAlwaysEnabled {
    [self setToolbarProperty:&_barButtonItemsAlwaysEnabled array:barButtonItemsAlwaysEnabled];
}

- (NSArray *)arrayWithFilteringUnavailableBarButtons:(NSArray *)items {
    NSMutableArray *filteredItems = [NSMutableArray array];
    for (PSPDFBarButtonItem *barButtonItem in items) {
        if (![barButtonItem respondsToSelector:@selector(isAvailable)] || barButtonItem.isAvailable) {
            [filteredItems addObject:barButtonItem];
        }
    }
    return filteredItems;
}

// If barButtonItem is nil, the default size is chosen.
- (CGFloat)approximatWidthForBarButtonItem:(UIBarButtonItem *)barButtonItem {
    CGFloat width = 42;
    if ([barButtonItem.title length]) {
        width = [barButtonItem.title sizeWithFont:[UIFont systemFontOfSize:15]].width + 30;
    }else if (barButtonItem.customView) {
        width = barButtonItem.customView.frame.size.width + 15;
    }
    return  width;
}

- (void)calculateToolbarWidths {
    if ([self isViewLoaded]) {
        _minLeftToolbarWidth = 0;

        // left
        NSMutableArray *leftItems = [[self arrayWithFilteringUnavailableBarButtons:self.leftBarButtonItems] mutableCopy];
        for(UIBarButtonItem *barButtonItem in [leftItems copy]) {
            _minLeftToolbarWidth += [self approximatWidthForBarButtonItem:barButtonItem];
        }

        // right
        _minRightToolbarWidth = 0;
        NSMutableArray *rightItems = [[self arrayWithFilteringUnavailableBarButtons:self.rightBarButtonItems] mutableCopy];
        for(UIBarButtonItem *barButtonItem in [rightItems copy]) {
            _minRightToolbarWidth += [self approximatWidthForBarButtonItem:barButtonItem];
        }
        if ([self.additionalBarButtonItems count]) {
            _minRightToolbarWidth += [self approximatWidthForBarButtonItem:nil];
        }
    }
}

- (UIBarStyle)barStyle {
    return [self isDarkHUD] ? UIBarStyleBlack : UIBarStyleDefault;
}

#define kPSPDFToolbarExtraMargin (PSIsIpad() ? 7.f : 6.f)
- (UIToolbar *)createTransparentToolbar {
    PSPDFTransparentToolbar *toolbar = [[[self classForClass:[PSPDFTransparentToolbar class]] alloc] initWithFrame:CGRectMake(0.f, 0.f, 0.f, CGRectGetHeight(self.internalNavigationController.navigationBar.bounds) ?: 44.f)];
    toolbar.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    toolbar.barStyle = [self barStyle];
    toolbar.tintColor = self.tintColor;
    // add long press detection support
    UILongPressGestureRecognizer *longPressGesture = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPressGestureFired:)];
    longPressGesture.minimumPressDuration = 0.3;
    longPressGesture.delegate = self;
    [toolbar addGestureRecognizer:longPressGesture];
    //toolbar.backgroundColor = [UIColor redColor]; // Useful for debugging
    // clipToBounds is a bad idea here, since the button highlighting would be clipped (very noticable on the iPhone)
    return toolbar;
}

#define kPSPDFToolbarLargeSpace 8.f
- (void)addMoreBarButtonToArray:(NSMutableArray *)mutableArray {
    UIBarButtonItem *largeSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    largeSpace.width = kPSPDFToolbarLargeSpace;

    NSArray *availableAdditionalRightBarButtonItems = [self arrayWithFilteringUnavailableBarButtons:self.additionalBarButtonItems];
    if ([availableAdditionalRightBarButtonItems count] == 1) {
        PSPDFBarButtonItem *barButtonItem = (self.additionalBarButtonItems)[0];
        if (barButtonItem.image) {
            [mutableArray addObject:barButtonItem];
            if (barButtonItem.style != UIBarButtonItemStyleBordered) {
                [mutableArray addObject:largeSpace];
            }
        }
    }else if ([availableAdditionalRightBarButtonItems count] > 1) {
        PSPDFMoreBarButtonItem *moreButtonItem = [[PSPDFMoreBarButtonItem alloc] initWithPDFViewController:self];
        [mutableArray addObject:moreButtonItem];
        if (moreButtonItem.style != UIBarButtonItemStyleBordered) {
            [mutableArray addObject:largeSpace];
        }
    }
}

- (void)createToolbarAnimated:(BOOL)animated {
    if (self.isToolbarEnabled && [self isViewLoaded]) {
        [self calculateToolbarWidths];
        UIBarButtonItem *largeSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
        largeSpace.width = kPSPDFToolbarLargeSpace;

        BOOL moreButtonHandled = NO;

        // left toolbar
        NSMutableArray *leftToolbarItems = [NSMutableArray array];
        for (PSPDFBarButtonItem *barButtonItem in self.leftBarButtonItems) {
            if (!moreButtonHandled && [barButtonItem isKindOfClass:[PSPDFMoreBarButtonItem class]]) {
                [self addMoreBarButtonToArray:leftToolbarItems];
                moreButtonHandled = YES;
                continue;
            }
            if (![barButtonItem respondsToSelector:@selector(isAvailable)] || barButtonItem.isAvailable) {
                [leftToolbarItems addObject:barButtonItem];
                if (barButtonItem.style != UIBarButtonItemStyleBordered) {
                    [leftToolbarItems addObject:largeSpace];
                }
            }
        }
        self.leftToolbar = [self createTransparentToolbar];
        self.leftToolbar.items = leftToolbarItems;

        // right toolbar
        NSMutableArray *rightToolbarItems = [NSMutableArray array];
        if (!moreButtonHandled && ![self.rightBarButtonItems containsObject:self.additionalActionsButtonItem]) {
            [self addMoreBarButtonToArray:rightToolbarItems];
        }

        for (PSPDFBarButtonItem *barButtonItem in self.rightBarButtonItems) {
            if (!moreButtonHandled && [barButtonItem isKindOfClass:[PSPDFMoreBarButtonItem class]]) {
                [self addMoreBarButtonToArray:rightToolbarItems];
                moreButtonHandled = YES;
                continue;
            }

            if (![barButtonItem respondsToSelector:@selector(isAvailable)] || barButtonItem.isAvailable) {
                [rightToolbarItems addObject:barButtonItem];
                if (barButtonItem.style != UIBarButtonItemStyleBordered) {
                    [rightToolbarItems addObject:largeSpace];
                }
            }
        }

        [rightToolbarItems insertObject:[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:NULL] atIndex:0];
        [rightToolbarItems removeLastObject];

        self.rightToolbar = [self createTransparentToolbar];
        self.rightToolbar.items = rightToolbarItems;

        [self addScrobbleBarToHUD];
        [self addPageLabelViewToHUD];
        [self addDocumentLabelView];
        [self updateToolbarAnimated:animated];
    }
}

- (void)updateToolbarAnimated:(BOOL)animated {
    // only perform if toolbar has been created
    if (self.isToolbarEnabled && self.leftToolbar) {
        CGRect leftFrame = self.leftToolbar.frame;
        CGRect rightFrame = self.rightToolbar.frame;

        CGFloat navBarWidth = CGRectGetWidth(self.internalNavigationController.navigationBar.bounds) ?: self.view.bounds.size.width;
        CGFloat maxToolbarWidth = roundf(navBarWidth / 2.f);
        leftFrame.size.width = [self.leftToolbar.items count] ? fminf(maxToolbarWidth, _minLeftToolbarWidth) : 0.f;
        rightFrame.size.width = [self.rightToolbar.items count] ? fminf(maxToolbarWidth, _minRightToolbarWidth) : 0.f;

        // This will expand the rightToolbar to have more than 50% space.
        // this is impossible to get right, but should be good enough for most cases (on the iPhone, space is rare.)
        if (!PSIsIpad() && [_leftToolbar.items count] < 4 && [_rightToolbar.items count] > 4 && ![_leftToolbar.items containsObject:self.viewModeButtonItem]) {
            rightFrame.size.width = fmaxf(roundf(maxToolbarWidth * 6/5), _minRightToolbarWidth);
        }

        self.leftToolbar.frame = leftFrame;
        self.rightToolbar.frame = rightFrame;

        // enable/disable buttons depending on document state
        NSMutableSet *barButtonItems = [NSMutableSet new];
        if (self.leftToolbar) {
            [barButtonItems addObjectsFromArray:self.leftToolbar.items];
        }
        if (self.rightToolbar) {
            [barButtonItems addObjectsFromArray:self.rightToolbar.items];
        }

        BOOL isValidDocument = self.document.isValid;
        for (UIBarButtonItem *barButtonItem in barButtonItems) {
            if ([barButtonItem isKindOfClass:[PSPDFBarButtonItem class]]) {
                [(PSPDFBarButtonItem *)barButtonItem updateBarButtonItem];
            }else if ([barButtonItem isKindOfClass:[UIBarButtonItem class]]) {
                if (![_barButtonItemsAlwaysEnabled containsObject:barButtonItem]) {
                    barButtonItem.enabled = isValidDocument;
                }
            }
        }

        // TODO: button fade animation looks like crap; thus we don't use 'animated' here.
        if ([self.leftToolbar.items count]) {
            // add a hostingView to compensate UIToolbar margin.
            CGRect lRect = _leftToolbar.frame; lRect.origin.x = -kPSPDFToolbarExtraMargin; _leftToolbar.frame = lRect;
            UIView *hostingView = [[PSPDFHUDView alloc] initWithFrame:self.leftToolbar.bounds];
            hostingView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
            [hostingView addSubview:self.leftToolbar];
            [self.navigationItem setLeftBarButtonItem:[[UIBarButtonItem alloc] initWithCustomView:hostingView] animated:_flags.viewWillAppearAnimatedActive];
        }else if ([self.navigationItem.leftBarButtonItem.customView isKindOfClass:[PSPDFHUDView class]]) {
            self.navigationItem.leftBarButtonItem = nil;
        }

        if ([self.rightToolbar.items count]) {
            CGRect rRect = _rightToolbar.frame; rRect.origin.x = kPSPDFToolbarExtraMargin; _rightToolbar.frame = rRect;
            UIView *hostingView = [[PSPDFHUDView alloc] initWithFrame:self.rightToolbar.bounds];
            hostingView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
            [hostingView addSubview:self.rightToolbar];
            [self.navigationItem setRightBarButtonItem:[[UIBarButtonItem alloc] initWithCustomView:hostingView] animated:_flags.viewWillAppearAnimatedActive];
        }else if ([self.navigationItem.rightBarButtonItem.customView isKindOfClass:[PSPDFHUDView class]]) {
            self.navigationItem.rightBarButtonItem = nil;
        }

        // As of iOS5, the navigationBar supports setting an array of UIBarButtonItems.
        BOOL useModernNavigationBarFeatures = NO;
        if (useModernNavigationBarFeatures) {
            self.navigationItem.leftBarButtonItems = [[self.leftToolbar.items reverseObjectEnumerator] allObjects];
            self.navigationItem.rightBarButtonItems = [[self.rightToolbar.items reverseObjectEnumerator] allObjects];
        }

        // uncomment to see toolbar width
#if 0
        self.leftToolbar.backgroundColor = [UIColor redColor];
        self.rightToolbar.backgroundColor = [UIColor redColor];
#endif
    }
}

- (void)updateBarButtonItem:(UIBarButtonItem *)barButtonItem animated:(BOOL)animated {
    NSParameterAssert(barButtonItem); if (!barButtonItem) return;

    // call delegate for custom UIToolbars
    if ([self.delegate respondsToSelector:@selector(pdfViewController:requestsUpdateForBarButtonItem:animated:)]) {
        [self.delegate pdfViewController:self requestsUpdateForBarButtonItem:barButtonItem animated:animated];
    }else {
        // support updating external toolbars.
        if ([barButtonItem respondsToSelector:@selector(nextResponder)] && [[(UIResponder *)barButtonItem nextResponder] isKindOfClass:[UIToolbar class]]) {
            UIToolbar *toolbar = (UIToolbar *)[(UIResponder *)barButtonItem nextResponder];
            NSArray *items = toolbar.items;
            toolbar.items = @[]; toolbar.items = items;
        }
    }

    // TODO: add logic for a more fine-grained update mechanism.
    [self createToolbarAnimated:animated];
}

- (void)setMinLeftToolbarWidth:(CGFloat)minLeftToolbarWidth {
    _minLeftToolbarWidth = minLeftToolbarWidth;
}

- (void)setMinRightToolbarWidth:(CGFloat)minRightToolbarWidth {
    _minRightToolbarWidth = minRightToolbarWidth;
}

- (void)loadView {
    PSPDFViewControllerView *view = [[[self classForClass:[PSPDFViewControllerView class]] alloc] init];
    view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    view.clipsToBounds = YES; // don't draw outside borders
    self.view = view;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    PSPDFLogVerbose(@"Loading view for %@", self);

    // We may want fullscreen layout
    UIStatusBarStyle statusBarStyle = [self statusBarStyle];
    if (statusBarStyle == UIStatusBarStyleBlackTranslucent || self.statusBarStyleSetting == PSPDFStatusBarSmartBlackHideOnIpad || self.statusBarStyleSetting == PSPDFStatusBarDisable) {
        self.wantsFullScreenLayout = YES;
    }

    PSPDFHUDView *contentView = [[[self classForClass:[PSPDFContentView class]] alloc] initWithFrame:[self contentRect]];
    contentView.backgroundColor  = [UIColor clearColor];
    //    contentView.backgroundColor  = [[UIColor redColor] colorWithAlphaComponent:0.5];
    contentView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    [self.view addSubview:contentView];
    self.contentView = contentView;

    // create HUD view
    PSPDFHUDView *hudView = [[[self classForClass:[PSPDFHUDView class]] alloc] initWithFrame:self.view.bounds];
    hudView.backgroundColor  = [UIColor clearColor];
    hudView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    hudView.alpha = 0.f; // initially hidden
    [self.view addSubview:hudView];
    self.HUDView = hudView;

    // set custom background color if needed
    self.view.backgroundColor = self.backgroundColor ? self.backgroundColor : [UIColor scrollViewTexturedBackgroundColor];

    // view debugging
    if (kPSPDFDebugScrollViews) {
        self.view.backgroundColor = [UIColor orangeColor];
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    PSPDFLogVerbose(@"%@", self);

    _flags.viewWillAppearAnimatedActive = animated;
    [self updateNavBarTitleIfAllowed];
    self.viewVisible = YES;

    // configure toolbar if enabled
    if (self.isToolbarEnabled) {
        self.internalNavigationController = self.navigationController;
        if (self.navigationController == self.parentViewController.navigationController && !self.useParentNavigationBar) {
            self.internalNavigationController = nil;
        }

        // make an UINavigationBar appearance snapshot so we don't override settings when popped back.
        UINavigationController *internalNavController = self.internalNavigationController;
        if (!_navBarAppearanceSnapshot) {
            _navBarAppearanceSnapshot = [[PSPDFNavigationAppearanceSnapshot alloc] initForNavigationController:internalNavController];
        }

        BOOL isBeingPresented = YES;
        isBeingPresented = self.isBeingPresented || self.isMovingToParentViewController;
        BOOL animatedAndNotOnTop = animated && (isBeingPresented && [self.navigationController.viewControllers count] > 1);

        if (self.internalNavigationController && animatedAndNotOnTop && (internalNavController.navigationBar.barStyle != [self barStyle] || internalNavController.navigationBar.translucent != [self isTransparentHUD])) {
            [internalNavController.navigationBar.layer addAnimation:PSPDFFadeTransition() forKey:nil];
        }
        internalNavController.navigationBar.barStyle = [self barStyle];
        internalNavController.navigationBar.tintColor = _tintColor;
        internalNavController.navigationBar.translucent = [self isTransparentHUD];
    }

    // optimizes caching
    self.document.displayingPdfController = self;
    self.document.displayingPage = self.page;

    // save current status bar style and change to configured style
    UIStatusBarStyle statusBarStyle = [self statusBarStyle];
    [self setStatusBarStyle:statusBarStyle animated:animated];

    // if statusbar hiding is requested, hide!
    if (self.statusBarStyleSetting == PSPDFStatusBarDisable) {
        [self setStatusBarHidden:YES withAnimation:animated ? UIStatusBarAnimationFade : UIStatusBarAnimationNone];

        // correct bounds for the navigation bar is calculated one runloop later.
        // if we just hide the statusbar here, we need to show/hide the HUD to fix the statusbar gap.
        dispatch_async(dispatch_get_main_queue(), ^{
            [self setHUDVisible:NO animated:NO];
            if (self.HUDViewMode != PSPDFHUDViewNever) {
                [self setHUDVisible:YES animated:animated];
            }
        });
    }

    // update rotation specific settings
    [self updateSettingsForRotation:[UIApplication sharedApplication].statusBarOrientation];

    // notify delegates that we're about to display a document
    [self delegateWillDisplayDocument];

    [self reloadData];
    _lastViewRect = self.view.frame;

    if (self.HUDViewMode != PSPDFHUDViewNever) {
        [self setHUDVisible:YES animated:YES];
    }

    // after viewWillAppear, views most likely are regenerated. So re-set the property.
    // (This restores the lock when drawing -> colorPicker -> back to drawing)
    if (self.viewLockEnabled) self.viewLockEnabled = YES;

    _flags.viewWillAppearAnimatedActive = NO;
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];

    // reset flag
    _flags.viewDidAppearCalled = 0;

    // preserve view state for next viewWillAppear
    _restoreViewStatePending = self.viewState;

    PSPDFLogVerbose(@"%@ (animated:%d). viewStatePending: %@", self, animated, _restoreViewStatePending);

    // only save if got removed from the navigationController
    // don't save if we're dismissed because we're showing a new modal viewController (e.g. annotation editing)
    BOOL isBeingDismissed = self.navigationController.isBeingDismissed;; // if dismissed modally
    if (!self.modalViewController && (!self.navigationController || ![self.navigationController.viewControllers containsObject:self] || isBeingDismissed)) {
#ifndef PSPDFKIT_DISABLE_TEXT_SELECTION_FEATURES
        NSError *error = nil;
        if (self.document && ![self.document saveChangedAnnotationsWithError:&error]) {
            // only show log warning if we have an error; save could also return no because it's disabled.
            if (error) PSPDFLogWarning(@"Saving annotations failed: %@", error);
        }
#endif

        // ensure annotation toolbar is dismissed if we're dismissed.
        [[self visibleAnnotationToolbar] hideToolbarAnimated:animated completion:NULL];
    }

    self.popoverController = nil;
    self.viewVisible = NO;

    // switch back to document mode (but don't care if we're no longer on the viewController
    BOOL shouldSwitchBack = !self.internalNavigationController || [self.internalNavigationController.viewControllers containsObject:self];
    if (shouldSwitchBack) {
        [self setViewMode:PSPDFViewModeDocument animated:YES];
    }

    // stop potential preload request
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(preloadNextThumbnails) object:nil];

    // restore statusbar (always)
    [_navBarAppearanceSnapshot restoreStatusBarStateAnimated:animated];

    // restore navigation controller (but me more selective)
    UINavigationController *internalNavController = self.internalNavigationController;
    if (internalNavController.topViewController != self) {
        BOOL shouldRestore = [internalNavController.viewControllers lastObject] && [internalNavController.viewControllers lastObject] != self && [internalNavController.viewControllers lastObject] != self.parentViewController;

        if (shouldRestore) {
            BOOL shouldAnimate = animated && [_navBarAppearanceSnapshot shouldAnimateAgainstNavigationController:internalNavController];
            [_navBarAppearanceSnapshot restoreForNavigationController:internalNavController animated:shouldAnimate];

            // if there's an annotationToolbar in the view hiararchy, remove it!
            PSPDFAnnotationToolbar *annotationToolbar = [self visibleAnnotationToolbar];
            [annotationToolbar hideToolbarAnimated:animated completion:^{
                [annotationToolbar removeFromSuperview];
            }];
        }
    }

    // ensure keyboard of the password view isn't displayed anymore.
    if (self.passwordView) [self.passwordView.passwordField resignFirstResponder];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    PSPDFLogVerbose(@"%@ (animated:%d)", self, animated);
    _flags.viewDidAppearCalled = YES;
    [self delegateDidDisplayDocument];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    PSPDFLogVerbose(@"%@ (animated:%d)", self, animated);

    // Release reference to internal nav controller (that would retain us otherwise)
    self.internalNavigationController = nil;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];

    // clear up the grid view
    if (_gridView &&  self.viewMode == PSPDFViewModeDocument) {
        PSPDFLogVerbose(@"Clearing thumbnail grid.");
        [_gridView removeFromSuperview];
        _gridView = nil;
    }

    if (![self isViewLoaded]) {
        self.pagingScrollView.delegate = nil;
        self.pagingScrollView = nil;
        self.pageTransitionController.pdfController = nil;
        self.pageTransitionController = nil;
        self.pageLabel.pdfController = nil; // deregisters KVO
        self.pageLabel = nil;
        self.scrobbleBar.pdfController = nil; // deregisters KVO
        self.scrobbleBar = nil;
        self.documentLabel.pdfController = nil;
        self.documentLabel = nil;
        self.HUDView = nil;
        self.contentView = nil;
        self.leftToolbar = nil;
        self.rightToolbar = nil;
        self.passwordView = nil;
        _gridView.delegate = nil;
        _gridView = nil;
    }
}

// Helper to find the annotation toolbar
- (PSPDFAnnotationToolbar *)visibleAnnotationToolbar {
    PSPDFAnnotationToolbar *annotationToolbar = (PSPDFAnnotationToolbar *)PSPDFGetViewInsideView(self.view, NSStringFromClass([PSPDFAnnotationToolbar class]));
    if (!annotationToolbar) {
        // look in the view controller
        annotationToolbar = (PSPDFAnnotationToolbar *)PSPDFGetViewInsideView(self.internalNavigationController.view, NSStringFromClass([PSPDFAnnotationToolbar class]));
    }
    return annotationToolbar;
}

// Return the annotation toolbar, if available.
// Not called if we are inside a UINavigationController (if we are, the annotation toolbar isn't even in the controller view hierarchy)
- (UIView *)rotatingHeaderView {
    return [self visibleAnnotationToolbar] ?: [super rotatingHeaderView];
}

// support for rotation locking.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation {
    BOOL superValue = [super shouldAutorotateToInterfaceOrientation:toInterfaceOrientation];
    return self.isRotationLockEnabled ? (toInterfaceOrientation == self.interfaceOrientation) : superValue;
}

// iOS 6
- (BOOL)shouldAutorotate {
    return !self.rotationLockEnabled;
}

#ifdef __IPHONE_6_0
- (NSUInteger)supportedInterfaceOrientations {
    return self.isRotationLockEnabled ? (1 << self.interfaceOrientation) : [super supportedInterfaceOrientations];
}
#endif

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    PSPDFLogVerbose(@"toInterfaceOrientation: %d, duration: %f", toInterfaceOrientation, duration);

    [[NSNotificationCenter defaultCenter] postNotificationName:PSPDFViewControllerWillChangeOrientationNotification object:self userInfo:@{@"toInterfaceDuration" : @(toInterfaceOrientation), @"duration" : @(duration)}];

    // we'll have to capture our target scroll page here, as pre-rotate maybe calls tilePages and changes current page...
    _targetPageAfterRotate = self.page;

    // propagate setting to improve animation.
    if ([self.pageTransitionController respondsToSelector:@selector(setTargetPageAfterRotation:)]) {
        [(PSPDFPageScrollViewController *)(self.pageTransitionController) setTargetPageAfterRotation:_targetPageAfterRotate];
    }

    [self updateSettingsForRotation:toInterfaceOrientation];

    // if there's a popover visible, hide it on rotation!
    self.popoverController = nil;

    _rotationActive = YES;

    // PSPDFPageViewController's rotate is called before we come to willAnimateRotation, so set early.
    if (self.pageTransitionController) _page = _targetPageAfterRotate;
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {

    [[NSNotificationCenter defaultCenter] postNotificationName:PSPDFViewControllerWillAnimateChangeOrientationNotification object:self userInfo:@{@"toInterfaceDuration" : @(toInterfaceOrientation), @"duration" : @(duration)}];

    // rotation is handled implicit via the setFrame-notification
    if ([self isViewLoaded] && self.view.window) {
        self.contentView.frame = [self contentRect];
        [self updateToolbarAnimated:NO];
        [self updateDocumentLabelViewPosition];
        [self updatePositionViewPosition];
        if (!self.pagingScrollView) {
            [self setPage:_targetPageAfterRotate animated:NO];
        }
    }
}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    [self updateSettingsForRotation:self.interfaceOrientation];
    self.contentView.frame = [self contentRect];

    // ensure we reload on a frame change.
    if (!CGSizeEqualToSize(_lastViewRect.size, self.view.frame.size) && !self.isRotationActive) {
        _lastViewRect = self.view.frame;
        [self reloadData];
    }

    [self updateGridForOrientation];
    [self updateDocumentLabelViewPosition];
    [self updatePositionViewPosition];

    if (_flags.gridNeedsScrollToPage) [self scrollGridToVisiblePageAnimated:NO];
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
    [super didRotateFromInterfaceOrientation:fromInterfaceOrientation];
    _lastViewRect = self.view.frame;
    _rotationActive = NO;

    [[NSNotificationCenter defaultCenter] postNotificationName:PSPDFViewControllerDidChangeOrientationNotification object:self userInfo:@{@"fromInterfaceOrientation" : @(fromInterfaceOrientation)}];

    // Toolbars. WTF! UIKit resizes the left UIBarButtonItem to nil width if there are a lot of icons.
    // Thus, re-set the frame.
    [self updateToolbarAnimated:NO];

    // Hotfix for a weird issue where UIKit will resize a PSPDFPageView when the application is initially loaded from landscape
    // and the hierarchy is flat (UINavigationController->PSPDFViewController). Then on the first pageCurl, the page is resized and not centered anymore.
    // Only is a problem if the view gets rotated and pushed all during startup. This has been fixed in iOS6.
    PSPDF_IF_PRE_IOS6(if (self.view.window && !_flags.viewDidAppearCalled && self.pageTransition == PSPDFPageCurlTransition) [self reloadData];)
}

- (void)setPopoverController:(UIPopoverController *)popoverController passthroughViews:(NSArray *)passthroughViews {
    [self willChangeValueForKey:@"popoverController"];

    // be sure to dismiss the popover of our toolbar.
    // Some BarButtonItems can't expose their popover, so hide even if popoverController_ is nil
    [PSPDFBarButtonItem dismissPopoverAnimated:NO];

    if (popoverController != popoverController_) {
        // hide last popup
        [popoverController_ dismissPopoverAnimated:NO];
        popoverController_.delegate = nil;
        popoverController_ = popoverController;
        popoverController_.delegate = self; // set delegate to be notified when popopver controller closes!

        // don't delete preexisting passthroughViews (e.g. UIToolbars...)
        if (passthroughViews) {
            NSMutableArray *mutablePassthroughViews = [popoverController_.passthroughViews mutableCopy] ?: [NSMutableArray array];
            [mutablePassthroughViews addObjectsFromArray:passthroughViews];
            popoverController_.passthroughViews = mutablePassthroughViews;
        }

        // capture case where the contentController is embedded within a UINavigationController (e.g. PSPDFWebViewController)
        UIViewController *contentController = popoverController.contentViewController;
        if ([contentController isKindOfClass:[UINavigationController class]]) {
            contentController = ((UINavigationController *)popoverController.contentViewController).topViewController;
        }

        // relay popover if there's a setter
        if ([contentController respondsToSelector:@selector(setPopoverController:)]) {
            [(PSPDFWebViewController *)contentController setPopoverController:popoverController];
        }
    }

    [self didChangeValueForKey:@"popoverController"];
}

- (void)setPopoverController:(UIPopoverController *)popoverController {
    // set clickthroughViews navigationBar if it exists and isn't yet added.
    NSMutableSet *passthroughViewSet = nil;
    if (self.navigationController.navigationBar) {
        passthroughViewSet = [NSMutableSet setWithArray:popoverController_.passthroughViews];
        [passthroughViewSet addObject:self.navigationController.navigationBar];
    }
    [self setPopoverController:popoverController passthroughViews:[passthroughViewSet allObjects]];
}

// relay the navigationItem of a contained viewController.
- (UINavigationItem *)navigationItem {
    UINavigationItem *navigationItem = nil;
    if (_useParentNavigationBar) {
        if (self.parentViewController) {
            return self.parentViewController.navigationItem;
        }else {
            // quite a hack, it's hard to get UINavigationController if we're not correctly embedded.
            UIResponder *responder = [self nextResponder];
            do {
                responder = [responder nextResponder];
                if ([responder isKindOfClass:[UIViewController class]] && [(UIViewController *)responder navigationController]) {
                    navigationItem = [(UIViewController *)responder navigationItem];
                }
            } while (responder && !navigationItem);
        }
    }
    return navigationItem ?: [super navigationItem];
}

- (void)applicationDidEnterBackground:(NSNotification *)notification {
#ifndef PSPDFKIT_DISABLE_TEXT_SELECTION_FEATURES
    // This will only save if we're displayed.
    // On iPhone/Note edit mode it will skip, since saving would destroy the documentProviders and break editing.
    if ([self isViewLoaded] && self.view.window) {
        NSError *error;
        if (![self.document saveChangedAnnotationsWithError:&error]) {
            PSPDFLogError(@"Failed to save annotations: %@", error);
        }
    }
#endif
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public

// we don't have a setter for screenPage
- (NSUInteger)screenPage { return [self actualPage:self.page]; }
- (void)setPage:(NSUInteger)page { [self setPage:page animated:NO]; }

- (void)setPageInternal:(NSUInteger)page {
    if (page != _page || _lastPage == NSNotFound) {
        [self willChangeValueForKey:NSStringFromSelector(@selector(page))];
        _page = page == NSNotFound ? 0 : page; // never allow garbage page

        [self delegateDidShowPage:page]; // use helper to find PageView
        _lastPage = page;

        self.document.displayingPage = page;

        // preload next thumbnails (so user doesn't see an empty image on scrolling)
        [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(preloadNextThumbnails) object:nil];
        [self performSelector:@selector(preloadNextThumbnails) withObject:nil afterDelay:PSPDFIsCrappyDevice() ? 2.f : 1.f];

        [[NSNotificationCenter defaultCenter] postNotificationName:kPSPDFHidePageHUDElements object:nil];
        [self didChangeValueForKey:NSStringFromSelector(@selector(page))];
    }
}

// corects landscape-values if entered in landscape
- (BOOL)setPage:(NSUInteger)page animated:(BOOL)animated {
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(scheduledRestoreViewState) object:nil];

    if (![self.document isValid]) return NO; // silent abort of we don't have a document set
    if (page >= [self.document pageCount]) { PSPDFLogWarning(@"Cannot scroll outside boundaries (%d).", page); return NO; }
    // block all scroll calls (unless we're refreshing)
    if (self.isViewLockEnabled && !_flags.viewWillAppearAnimatedActive) return NO;

    // stop if delegate doesn't allow scrolling
    if (![self delegateShouldScrollToPage:page]) return NO;

    // disable scrolling animations if blocking image load is enabled and destination is too far.
    // (else it's just too slow)
    if (self.pageTransition == PSPDFPageScrollPerPageTransition && fabs((NSInteger)page - (NSInteger)self.page) > 2) {
        animated = animated && self.renderingMode != PSPDFPageRenderingModeFullPageBlocking;
    }

    if (self.isViewVisible) {
        // ensure menu is hidden
        [[UIMenuController sharedMenuController] setMenuVisible:NO animated:YES];

        [_pageTransitionController setPage:page animated:animated];

        // the non-scrolling controller don't animate change, thus set directly.
        if (self.pageTransition > PSPDFPageScrollContinuousTransition) [self setPageInternal:page];
    }else {
        // not visible atm, just set page
        [self setPageInternal:page];
    }

    // if page is set, any view state that is pending should be nilled out.
    _restoreViewStatePending = nil;

    UIAccessibilityPostNotification(UIAccessibilityPageScrolledNotification, nil);
    return YES;
}

- (BOOL)scrollToNextPageAnimated:(BOOL)animated {
    if (![self isLastPage]) {
        NSUInteger nextPage = [self landscapePage:self.page+1];
        [self setPage:nextPage animated:animated];
        return YES;
    }else {
        PSPDFLog(@"max page count of %d exceeded! (%d)", [self.document pageCount], self.page+1); return NO;
    }
}

- (BOOL)scrollToPreviousPageAnimated:(BOOL)animated {
    if (![self isFirstPage]) {
        NSUInteger prevPage = [self landscapePage:self.page-1];
        [self setPage:prevPage animated:animated];
        return YES;
    }else {
        PSPDFLog(@"Cannot scroll < page 0"); return NO;
    }
}

- (UIScrollView *)scrollViewForZooming {
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(scheduledRestoreViewState) object:nil];
    PSPDFPageView *pageView = [self pageViewForPage:self.page];
    return pageView.scrollView;
}

- (void)scrollRectToVisible:(CGRect)rect animated:(BOOL)animated {
    UIScrollView *scrollView = [self scrollViewForZooming];
    [scrollView scrollRectToVisible:rect animated:animated];
}

// TODO fix for double page mode
- (void)zoomToRect:(CGRect)zoomRect animated:(BOOL)animated {
    UIScrollView *scrollView = [self scrollViewForZooming];
    [scrollView zoomToRect:zoomRect animated:YES];
}

- (void)setZoomScale:(float)scale animated:(BOOL)animated {
    UIScrollView *scrollView = [self scrollViewForZooming];
    [scrollView setZoomScale:scale animated:YES];
}

- (PSPDFViewState *)viewState {
    UIScrollView *scrollView = [self pageViewForPage:self.page].scrollView;
    PSPDFViewState *state = [PSPDFViewState new];
    state.page = self.page;
    state.contentOffset = scrollView.contentOffset;
    if ([self.pageTransitionController respondsToSelector:@selector(compensatedContentOffset)]) {
        state.contentOffset = [self.pageTransitionController compensatedContentOffset];
    }
    state.zoomScale = scrollView.zoomScale;
    state.HUDVisible = self.isHUDVisible;
    return state;
}

- (void)setViewState:(PSPDFViewState *)viewState {
    [self setViewState:viewState animated:NO];
}

- (void)setViewState:(PSPDFViewState *)viewState animated:(BOOL)animated {
    if (!viewState) return;
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(scheduledRestoreViewState) object:nil];

    // we can't restore the view if we're not ready yet.
    // view might already be loaded, but not displayed (e.g. on PSPDFViewController reuse; or a pre-access to .view)
    if (![self isViewLoaded] || !self.view.window) {
        self.page = viewState.page;
        _restoreViewStatePending = viewState;
        return;
    }

    if (self.page != viewState.page) {
        [self setPage:viewState.page animated:animated];
        // if HUDViewMode is set to never, don't restore HUD here.
        BOOL targetHUDState = viewState.HUDVisible && self.HUDViewMode != PSPDFHUDViewNever;
        [self setHUDVisible:targetHUDState animated:animated];

        // do we need to zoom to a certain rect? if so, this needs to be done AFTER scrolling.
        // (using delegateDidEndPageScrolling)
        if (animated && viewState.zoomScale > 1.f) {
            _restoreViewStatePending = viewState;
            return;
        }
    }

    // Note: because we *center* the rect in PSPDFScrollView, this messes up our scrollView animation when setting a rect.
    PSPDFPageView *pageView = [self pageViewForPage:self.page];
    PSPDFScrollView *scrollView = pageView.scrollView;
    [scrollView setZoomScale:viewState.zoomScale animated:animated];

    // make sure content offset is valid
    CGPoint newOffset = viewState.contentOffset;

    // if we have just ONE scrollView (e.g. continuous scrolling) we need to compensate the current offset.
    if (self.pagingScrollView == scrollView) {
        newOffset.x += scrollView.contentOffset.x;
        newOffset.y += scrollView.contentOffset.y;
    }
    CGPoint maxOffset = CGPointMake((scrollView.contentSize.width-scrollView.bounds.size.width),
                                    (scrollView.contentSize.height-scrollView.bounds.size.height));

    newOffset.y = psrangef(0, newOffset.y, maxOffset.y);
    newOffset.x = psrangef(0, newOffset.x, maxOffset.x);

    [scrollView setContentOffset:newOffset animated:animated];

    _restoreViewStatePending = nil;
}

- (void)setBackgroundColor:(UIColor *)backgroundColor {
    if (_backgroundColor != backgroundColor) {
        _backgroundColor = backgroundColor;

        // only relay to view if loaded
        if ([self isViewLoaded]) {
            self.view.backgroundColor = backgroundColor;
        }
    }
}

- (void)setPasswordView:(PSPDFPasswordView *)passwordView {
    if (passwordView != _passwordView) {
        _passwordView.delegate = nil;
        _passwordView = passwordView;
    }
}

- (void)clearDocument {
    if (_document) { // nil out current doc
        [[PSPDFCache sharedCache] stopCachingDocument:_document];
        _document.displayingPage = NSNotFound;
        _document.displayingPdfController = nil;
        _document.textSearch.delegate = nil;
    }
    _document = nil;
}

// add special support that accepts plain NSString's as PSPDFDocument, most likely used by Storybarding. Does not fire KVO.
- (void)setDocumentInternal:(PSPDFDocument *)document {
    [self clearDocument];
    if ([document isKindOfClass:[NSString class]]) {
        NSString *documentPath = PSPDFResolvePathNames((NSString *)document, nil);
        _document = [PSPDFDocument PDFDocumentWithURL:[NSURL fileURLWithPath:documentPath]];
    }else {
        _document = document;
    }

    [self updateNavBarTitleIfAllowed];

    // set document delegate to listen for search events
    _document.textSearch.delegate = self;
}

- (void)setDocument:(PSPDFDocument *)document {
    PSPDFLogVerbose(@"Setting new document %@", document);

    if (_document != document) {
        BOOL allowed = [self delegateShouldSetDocument:document];
        if (!allowed) return;

        [self setDocumentInternal:document];
        _lastPage = NSNotFound; // reset last page
        [self setPageInternal:0];
        [self reloadData];
    }
}

- (void)setPageMode:(PSPDFPageMode)pageMode {
    if (pageMode != _pageMode) {
        _pageMode = pageMode;
        // don't rotate if rotation is active
        if (!_rotationActive) {
            [self reloadData];
        }
    }
}

- (void)setScrobbleBarEnabled:(BOOL)scrobbleBarEnabled {
    if (scrobbleBarEnabled != _scrobbleBarEnabled) {
        _scrobbleBarEnabled = scrobbleBarEnabled;

        // only adds the view if enabled, and hide initially
        [self addScrobbleBarToHUD];
        self.scrobbleBar.alpha = scrobbleBarEnabled ? 0.f : 1.f;

        // default animation, duration can be overridden when inside another animation block
        // still there's a chance thet we don't get the scrobbleBar (if HUD is not yet initialized)
        [UIView animateWithDuration:0.f delay:0.f options:UIViewAnimationOptionAllowUserInteraction animations:^{
            if (scrobbleBarEnabled) {self.scrobbleBar.hidden = NO;}
            self.scrobbleBar.alpha = scrobbleBarEnabled ? 1.f : 0.f;
        } completion:^(BOOL finished) {
            self.scrobbleBar.hidden = !scrobbleBarEnabled;
        }];
    }
}

- (void)setPageLabelEnabled:(BOOL)pageLabelEnabled {
    if (pageLabelEnabled != _pageLabelEnabled) {
        _pageLabelEnabled = pageLabelEnabled;

        // only adds the view if enabled, and hide initially
        [self addPageLabelViewToHUD];
        self.pageLabel.alpha = pageLabelEnabled ? 0.f : 1.f;

        // default animation, duration can be overridden when inside another animation block
        // still there's a chance thet we don't get the positionView (if HUD is not yet initialized)
        [UIView animateWithDuration:0.f delay:0.f options:UIViewAnimationOptionAllowUserInteraction animations:^{
            if (pageLabelEnabled) self.pageLabel.hidden = NO;
            self.pageLabel.alpha = pageLabelEnabled ? 1.f : 0.f;
        } completion:^(BOOL finished) {
            self.pageLabel.hidden = !pageLabelEnabled;
        }];
    }
}

- (void)setDocumentLabelEnabled:(BOOL)documentLabelEnabled {
    if (documentLabelEnabled != _documentLabelEnabled) {
        _documentLabelEnabled = documentLabelEnabled;

        [self addDocumentLabelView];
        self.documentLabel.alpha = documentLabelEnabled ? 0.f : 1.f;

        // default animation, duration can be overridden when inside another animation block
        // still there's a chance thet we don't get the documentLabelView (if HUD is not yet initialized)
        [UIView animateWithDuration:0.f delay:0.f options:UIViewAnimationOptionAllowUserInteraction animations:^{
            if (documentLabelEnabled) {self.documentLabel.hidden = NO;}
            self.documentLabel.alpha = documentLabelEnabled ? 1.f : 0.f;
        } completion:^(BOOL finished) {
            self.documentLabel.hidden = !documentLabelEnabled;
        }];
    }
}

- (void)setRotationLockEnabled:(BOOL)rotationLockEnabled {
    if (rotationLockEnabled      && !_rotationLockEnabled) PSPDFLockRotation();
    else if (!rotationLockEnabled &&  _rotationLockEnabled) PSPDFUnlockRotation();

    _rotationLockEnabled = rotationLockEnabled;
    if (!rotationLockEnabled) [UIViewController attemptRotationToDeviceOrientation];
}

- (void)setViewLockEnabled:(BOOL)viewLockEnabled {
    _viewLockEnabled = viewLockEnabled;

    // disable zooming.
    for (PSPDFPageView *pageView in [self visiblePageViews]) {
        pageView.scrollView.zoomingEnabled = !viewLockEnabled;
        pageView.scrollView.scrollEnabled = !viewLockEnabled;
    }
    self.pagingScrollView.scrollEnabled = !viewLockEnabled;
}

- (void)setDelegate:(id<PSPDFViewControllerDelegate>)delegate {
    if (delegate != self.delegate) {
        _delegate = delegate;
        [self updateDelegateFlags]; // in delegate category file.
    }
}

- (UIColor *)renderBackgroundColor {
    return _renderBackgroundColor ?: [UIColor whiteColor];
}

- (BOOL)isControllerLocked {
    return _lockCount > 0;
}

- (void)lockController {
    _lockCount++;
    // snapshot properties if we just activated the lock
    if (_lockCount == 1) {
        _flags.savedViewLock = self.isViewLockEnabled;
        _flags.savedRotationLock = self.isRotationLockEnabled;
        _flags.savedTextSelection = self.isTextSelectionEnabled;
        self.viewLockEnabled = YES;
        self.rotationLockEnabled = YES;
        self.textSelectionEnabled = NO;
    }
}

- (void)unlockController {
    if (_lockCount == 0) {
        PSPDFLogWarning(@"Ignoring unbalanced call to unlockController."); return;
    }

    _lockCount--;
    if (_lockCount == 0) {
        self.rotationLockEnabled = _flags.savedRotationLock;
        self.viewLockEnabled = _flags.savedViewLock;
        self.textSelectionEnabled = _flags.savedTextSelection;
    }
}

- (BOOL)isViewWillAppearing { return _flags.viewWillAppearAnimatedActive; }
- (BOOL)viewDidAppearCalled { return _flags.viewDidAppearCalled; }

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Tiling and page configuration

- (NSArray *)calculatedVisiblePageNumbers {
    NSUInteger page = self.page;

    // bail out early if it's page 0 or double page mode is not active.
    if ((page == 0 && !self.isDoublePageModeOnFirstPage) || ![self isDoublePageMode] || self.pageTransition == PSPDFPageScrollContinuousTransition) return @[@(page)];

    // if it's the right page, substract.
    if ([self isRightPageInDoublePageMode:page]) page--;

    return @[@(page), @(page+1)];
}

- (NSArray *)visiblePageNumbers {
    return [self.pageTransitionController visiblePageNumbers];
}

- (NSArray *)visiblePageViews {
    // fast path
    if ([self.pageTransitionController respondsToSelector:@selector(visiblePageViews)]) {
        return [self.pageTransitionController visiblePageViews];
    }else {
        NSMutableArray *visiblePageViews = [NSMutableArray array];
        for (NSNumber *pageNumber in [self visiblePageNumbers]) {
            PSPDFPageView *pageView = [self pageViewForPage:[pageNumber unsignedIntegerValue]];
            if (pageView) [visiblePageViews addObject:pageView];
        }
        return visiblePageViews;
    }
}

// search for the page within PSPDFScollView
- (PSPDFPageView *)pageViewForPage:(NSUInteger)page {
    return [self.pageTransitionController pageViewForPage:page];
}

// preloads next document thumbnail
- (void)preloadNextThumbnails {
    PSPDFDocument *document = self.document;
    NSUInteger page = self.page;

    if (document) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
            // cache next page's thumbnail
            if (page+1 < [document pageCount]) {
                PSPDFLogVerbose(@"Preloading thumbnails for page %d", page+1);
                [[PSPDFCache sharedCache] cachedImageForDocument:document page:page+1 size:PSPDFSizeThumbnail];
            }

            // start/update caching document
            [[PSPDFCache sharedCache] cacheDocument:document startAtPage:page size:PSPDFSizeNative];
        });

        // fill document data cache in background (once)
        if (!_flags.documentRectCacheLoaded) {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
                [document fillCache];
            });
            _flags.documentRectCacheLoaded = YES;
        }
    }
}

- (void)updateNavBarTitleIfAllowed {
    // only set title if we're allowed to make toolbar modifications, and only on iPad due to lack of space on iPhone.
    if (PSIsIpad() && self.isToolbarEnabled) self.title = self.document.title;
}

- (void)reloadData {
    if (_flags.isReloading || _rotationActive) return; // ignore multiple calls to reloadData

    NSUInteger page = self.page;
    _flags.isReloading = YES;

    // only update if window is attached
    if ([self isViewLoaded] && self.viewVisible) {
        if (self.document.isLocked && self.isPasswordDialogEnabled) {
            [self removeContentController];
            if (!_passwordView) {
                _passwordView = [[[self classForClass:[PSPDFPasswordView class]] alloc] initWithFrame:PSIsIpad() ? CGRectMake(0, 0, 300, 300) : CGRectMake(0, 0, 250, 250)];
                _passwordView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin;
                _passwordView.document = self.document;
                _passwordView.delegate = self;
            }
            if (!_passwordView.superview) {
                [self.contentView addSubview:_passwordView];
                [_passwordView becomeFirstResponder];
            }
            [self createToolbarAnimated:NO];
        }else {
            PSPDFViewState *restoreViewState = _restoreViewStatePending;

            [_passwordView removeFromSuperview];
            _page = 0; // ensure initially we're at page 0 while loading.
            [self createContentController];
            [self setPage:page animated:NO]; // will clear _restoreViewStatePending
            self.pagingScrollView.alpha = self.viewMode == PSPDFViewModeThumbnails ? 0.0 : 1.0;
            [self createToolbarAnimated:NO];
            [self.scrobbleBar updateToolbarForced];
            [self updatePositionViewPosition]; // depends on the scrobbleBar

            dispatch_async(dispatch_get_main_queue(), ^{
                [self updateDocumentLabelViewPosition];
            });
            // don't forget the thumbnails
            // don't use self.gridView, as it's lazy init
            [_gridView reloadData];
            if (restoreViewState) {
                _restoreViewStatePending = restoreViewState;
                [self performSelector:@selector(scheduledRestoreViewState) withObject:nil afterDelay:0.0];
            }
            [self delegateDidShowPage:page];
        }
        self.contentView.frame = [self contentRect];
    }

    _flags.isReloading = NO;
}

- (void)scheduledRestoreViewState {
    if (_restoreViewStatePending) self.viewState = _restoreViewStatePending;
}

- (BOOL)shouldShowControls {
    BOOL shouldShowControls = NO;
    if (self.HUDViewMode == PSPDFHUDViewAutomatic) {
        BOOL atFirstPage = self.page == 0;
        BOOL atLastPage = self.page >= [self.document pageCount]-1;
        shouldShowControls = atFirstPage || atLastPage;
    }
    return shouldShowControls;
}

- (void)hideControlsIfPageMode {
    if (self.viewMode == PSPDFViewModeDocument && ![self shouldShowControls]) [self hideControls];
}

- (BOOL)hideControlsAndPageElements {
    BOOL success = [self hideControls];
    [[NSNotificationCenter defaultCenter] postNotificationName:kPSPDFHidePageHUDElements object:nil];
    [self.popoverController dismissPopoverAnimated:YES];
    self.popoverController = nil;
    return success;
}

// Search specific barButton in the toolbar.
- (PSPDFBarButtonItem *)findBarButtonItemClass:(Class)barButtonItemClass {
    NSMutableSet *barButtons = [NSMutableSet set];
    if (self.leftToolbar.items) [barButtons addObjectsFromArray:self.leftToolbar.items];
    if (self.rightToolbar.items) [barButtons addObjectsFromArray:self.rightToolbar.items];

    PSPDFBarButtonItem *barButtonItem = nil;
    for (UIBarButtonItem *item in barButtons) {
        if ([item isKindOfClass:barButtonItemClass]) {
            barButtonItem = (PSPDFBarButtonItem *)item;
        }
    }
    return barButtonItem;
}

- (void)searchForString:(NSString *)searchText animated:(BOOL)animated {
    Class viewControllerClass = [self classForClass:[PSPDFSearchViewController class]];
    PSPDFSearchViewController *searchController = [[viewControllerClass alloc] initWithDocument:self.document pdfController:self];
    searchController.searchText = searchText;

    // find the search bar button.
    BOOL needsToShowControlsFirst = self.isNavigationBarHidden;
    if (needsToShowControlsFirst) [self showControls];

    dispatch_block_t action = ^{
        PSPDFSearchBarButtonItem *searchBarButton = (PSPDFSearchBarButtonItem *)[self findBarButtonItemClass:[PSPDFSearchBarButtonItem class]];

        // if we can't find the barbutton but the original is used
        if (!searchBarButton && [self.searchButtonItem respondsToSelector:@selector(window)] && [(UIView *)self.searchButtonItem window]) {
            searchBarButton = self.searchButtonItem;
        }

        // Get rect for the current selection. Only used if there's no searchBarButton.
        CGRect searchTargetRect = CGRectZero;
        for (PSPDFPageView *pageView in [self visiblePageViews]) {
            if (pageView.selectionView.selectedGlyphs) {
                searchTargetRect = [pageView convertRect:pageView.selectionView.firstLineRect toView:self.view];
                break;
            }
        }

        [self presentViewControllerModalOrPopover:searchController embeddedInNavigationController:NO withCloseButton:YES animated:animated sender:searchBarButton options:@{PSPDFPresentOptionRect : BOXED(searchTargetRect)}];
    };

    // do we need to wait for the HUD?
    if (needsToShowControlsFirst) dispatch_async(dispatch_get_main_queue(), action);
    else action();
}

- (BOOL)textSelectionEnabled {
#ifdef PSPDFKIT_DISABLE_TEXT_SELECTION_FEATURES
    _textSelectionEnabled = NO;
#endif
    return _textSelectionEnabled;
}

- (BOOL)imageSelectionEnabled {
#ifdef PSPDFKIT_DISABLE_TEXT_SELECTION_FEATURES
    _imageSelectionEnabled = NO;
#endif
    return _imageSelectionEnabled;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Controls

// helper to detect if we're embedded or not.
- (BOOL)isEmbedded {
    UIViewController *parentViewController = nil;
    parentViewController = self.parentViewController;
    if ((!self.view.window && parentViewController == nil) || [parentViewController isKindOfClass:[PSPDFTabbedViewController class]]) {
        return NO;
    }
    // detect a UITabBarController and increase embedded detection threshold
    CGFloat threshold = 44.f;
    do {
        if ([parentViewController isKindOfClass:[UITabBarController class]]) {
            threshold += 49; break;
        }
    }while((parentViewController = parentViewController.parentViewController));
    CGRect viewRect = self.view.bounds;
    viewRect = [self.view convertRect:viewRect toView:nil]; // Convert to the window's coordinate space.
    CGRect appRect = [[UIScreen mainScreen] applicationFrame];
    // use a heuristic to compensate status bar effects (transparency, call notification, etc)
    BOOL isEmbedded = fabs(viewRect.size.width - appRect.size.width) > threshold || fabs(viewRect.size.height - appRect.size.height) > threshold;
    PSPDFLogVerbose(@"embedded: %d (%@:%@)", isEmbedded, NSStringFromCGRect(viewRect), NSStringFromCGRect(appRect));
    return isEmbedded;
}

- (void)setStatusBarStyle:(UIStatusBarStyle)statusBarStyle animated:(BOOL)animated {
    if (![self isEmbedded] && !(self.statusBarStyleSetting & PSPDFStatusBarIgnore)) {
        [[UIApplication sharedApplication] setStatusBarStyle:statusBarStyle animated:animated];
    }
}

- (void)setStatusBarHidden:(BOOL)hidden withAnimation:(UIStatusBarAnimation)animation {
    if (![self isEmbedded] && !(self.statusBarStyleSetting & PSPDFStatusBarIgnore)) {
        [[UIApplication sharedApplication] setStatusBarHidden:hidden withAnimation:animation];
    }
}

- (void)closeModalView {
    [[self masterViewController] dismissViewControllerAnimated:YES completion:NULL];
}

NSString *const PSPDFPresentOptionRect = @"PSPDFPresentOptionRect";
NSString *const PSPDFPresentOptionPopoverContentSize = @"PSPDFPresentOptionPopoverContentSize";
NSString *const PSPDFPresentOptionAllowedPopoverArrowDirections = @"PSPDFPresentOptionAllowedPopoverArrowDirections";
NSString *const PSPDFPresentOptionModalPresentationStyle = @"PSPDFPresentOptionModalPresentationStyle";
NSString *const PSPDFPresentOptionAlwaysModal = @"PSPDFPresentOptionAlwaysModal";
NSString *const PSPDFPresentOptionPassthroughViews = @"PSPDFPresentOptionPassthroughViews";

- (id)presentViewControllerModalOrPopover:(UIViewController *)controller embeddedInNavigationController:(BOOL)embedded withCloseButton:(BOOL)closeButton animated:(BOOL)animated sender:(id)sender options:(NSDictionary *)options {

    CGRect rect = [options[PSPDFPresentOptionRect] CGRectValue];
    CGSize popoverContentSize = [options[PSPDFPresentOptionPopoverContentSize] CGSizeValue];
    UIPopoverArrowDirection arrowDirections = [options[PSPDFPresentOptionAllowedPopoverArrowDirections] unsignedIntegerValue];
    BOOL overrideModalPresentationStyle = options[PSPDFPresentOptionModalPresentationStyle] && ![controller isKindOfClass:[UIActivityViewController class]];
    UIModalPresentationStyle modalPresentationStyle = [options[PSPDFPresentOptionModalPresentationStyle] integerValue];
    BOOL alwaysModal = [options[PSPDFPresentOptionAlwaysModal] boolValue];

    if (!alwaysModal && PSIsIpad() && (!overrideModalPresentationStyle || modalPresentationStyle == UIModalPresentationFullScreen)) {
        // add a navigation controller?
        if (embedded && ![controller isKindOfClass:[UINavigationController class]]) {
            controller = [[UINavigationController alloc] initWithRootViewController:controller];
        }

        // Better log than later crash with a NSInvalidArgumentException.
        if (!self.view.window) { PSPDFLogError(@"Popovers cannot be presented from a view which does not have a window."); return nil; }

        // apply style if it confirms to PSPDFStylable
        [self applyStyleToViewController:controller isInPopover:YES];

        UIPopoverController *popoverController = [[PSPDFPopoverController alloc] initWithContentViewController:controller];

        // use custom controller background if enabled.
        if (self.shouldTintPopovers && self.tintColor) {
            [PSPDFPopoverBackgroundView setTintColor:self.tintColor];
            popoverController.popoverBackgroundViewClass = [PSPDFPopoverBackgroundView class];}

        popoverController.delegate = self;
        BOOL shouldShow = [self delegateShouldShowController:controller embeddedInController:popoverController animated:YES];

        if (shouldShow) {
            if (!PSPDFSizeIsEmpty(popoverContentSize)) {
                popoverController.popoverContentSize = popoverContentSize;
            }
            // Popovers cannot be presented from a view which does not have a window. (UIBarButton HAS a window; it's just not declared.)
            if (sender && [sender isKindOfClass:[UIBarButtonItem class]] && [sender respondsToSelector:@selector(window)] && [(UIView *)sender window]) {
                // if we have a sender (barButtonItem)
                [popoverController presentPopoverFromBarButtonItem:sender permittedArrowDirections:(arrowDirections ?: UIPopoverArrowDirectionUp | UIPopoverArrowDirectionLeft | UIPopoverArrowDirectionRight) animated:animated];
            }else {
                if (CGRectIsEmpty(rect)) {
                    if ([sender isKindOfClass:[UIView class]]) {
                        rect = [(UIView *)sender bounds];
                    }else {
                        rect = CGRectMake(0, 0, self.view.frame.size.width, 1);
                    }
                }

                // if rect is too large to usefully display a popover, change rect size and display at the center.
                if (rect.size.width > self.view.frame.size.width * 0.8 || rect.size.height > self.view.frame.size.height) {
                    rect = CGRectMake(CGRectGetMidX(rect), CGRectGetMidY(rect), 1, 1);
                }

                UIPopoverArrowDirection defaultArrowDirections = UIPopoverArrowDirectionDown | UIPopoverArrowDirectionLeft | UIPopoverArrowDirectionRight;

                // we need to prevent a bug where the popover is presented in the center with arrow down, then they keyboard is presented shrinking the popover almost out of existance. This is a known UIKit problem, best to not send UIPopoverArrowDirectionUp in such cases. Only a landscape problem, since the keyboard is so huge there. (iPad - won't be called on iPhone anyway)
                // Can be tested with selecting a full line of text in landscape right above where the keyboard would be.
                if (UIInterfaceOrientationIsPortrait(self.interfaceOrientation) || rect.origin.y > self.view.frame.size.height*0.5) {
                    defaultArrowDirections |= UIPopoverArrowDirectionUp;
                }
                [popoverController presentPopoverFromRect:rect inView:self.view permittedArrowDirections:arrowDirections ?: defaultArrowDirections animated:animated];
            }
        }

        // allow to customize the passthrough views.
        if (shouldShow) {
            if (!options[PSPDFPresentOptionPassthroughViews]) {
                self.popoverController = popoverController;
            }else {
                NSArray *passthroughViews = options[PSPDFPresentOptionPassthroughViews];
                if (![passthroughViews isKindOfClass:[NSArray class]]) passthroughViews = @[passthroughViews]; // accept everything :)
                [self setPopoverController:popoverController passthroughViews:passthroughViews];
            }
        }else {
            self.popoverController = nil;
        }
        return shouldShow ? popoverController : nil;
    }else {
        if (overrideModalPresentationStyle) controller.modalPresentationStyle = modalPresentationStyle;
        [self presentModalViewController:controller embeddedInNavigationController:embedded withCloseButton:closeButton animated:animated];
        return nil;
    }
}

- (void)presentModalViewController:(UIViewController *)controller embeddedInNavigationController:(BOOL)embedded withCloseButton:(BOOL)closeButton animated:(BOOL)animated {
    UINavigationController *navController = nil;

    if (embedded || [controller isKindOfClass:[UINavigationController class]]) {
        navController = (UINavigationController *)controller;
        if (![controller isKindOfClass:[UINavigationController class]]) {
            navController = [[UINavigationController alloc] initWithRootViewController:controller];
            navController.modalPresentationStyle = controller.modalPresentationStyle;
        }else {
            controller = navController.topViewController;
        }
        // set custom tintColor.
        navController.navigationBar.tintColor = self.tintColor;
    }

    // informal protocol (will ignore the navController part if not set)
    if (closeButton) {
        if ([controller respondsToSelector:@selector(setShowsCancelButton:)]) {
            [(PSPDFSearchViewController *)controller setShowsCancelButton:YES];
            navController.navigationBarHidden = YES;
        }else {
            controller.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:PSPDFLocalize(@"Close") style:UIBarButtonItemStyleDone target:self action:@selector(closeModalView)];
        }
    }

    // only set document as title if there's no default title.
    if ([controller.title length] == 0 && [controller.title length] == 0) {
        controller.title = self.document.title;
    }

    // apply style if it confirms to PSPDFStyleable
    [self applyStyleToViewController:controller isInPopover:NO];

    BOOL shouldShow = [self delegateShouldShowController:controller embeddedInController:navController animated:animated];

    if (shouldShow) {
        [[self masterViewController] presentViewController:navController ?: controller animated:animated completion:^{
            [self delegateDidShowController:controller embeddedInController:navController animated:animated];
        }];


        // on the iPhone, we wanna move to the default statusbar
        if (!PSIsIpad()) {
            UIStatusBarStyle statusBarStyle = UIStatusBarStyleDefault;

            // if the barSyle is set dark, don't set a white status bar.
            if (navController.navigationBar && navController.navigationBar.barStyle != UIBarStyleDefault) {
                statusBarStyle = UIStatusBarStyleBlackOpaque;
            }

            // next, look if we implement PSPDFStatusBarStyleHint - overrides navigationBar.barStyle.
            if ([controller conformsToProtocol:@protocol(PSPDFStatusBarStyleHint)]) {
                statusBarStyle = [(id<PSPDFStatusBarStyleHint>)controller preferredStatusBarStyle];
            }

            [self setStatusBarStyle:statusBarStyle animated:animated];
        }
    }
}

// Apply the style if the viewController confirms to PSPDFStyleable
- (void)applyStyleToViewController:(UIViewController *)viewController isInPopover:(BOOL)isInPopover {
    // go to content controller if we're embedded in a UINavigationController
    if ([viewController isKindOfClass:[UINavigationController class]]) {
        viewController = ((UINavigationController *)viewController).topViewController;
    }

    if ([viewController conformsToProtocol:@protocol(PSPDFStyleable)]) {
        id <PSPDFStyleable> styleableViewController = (id <PSPDFStyleable>)viewController;
        if ([styleableViewController respondsToSelector:@selector(setTintColor:)]) {
            styleableViewController.tintColor = self.tintColor;
        }
        if ([styleableViewController respondsToSelector:@selector(setBarStyle:)]) {
            styleableViewController.barStyle = [self barStyle];
        }
        if ([styleableViewController respondsToSelector:@selector(setIsBarTranslucent:)]) {
            styleableViewController.isBarTranslucent = [self isTransparentHUD];
        }
        if ([styleableViewController respondsToSelector:@selector(setIsInPopover:)]) {
            styleableViewController.isInPopover = isInPopover;
        }
        if ([styleableViewController respondsToSelector:@selector(setShouldTintToolbarButtons:)]) {
            styleableViewController.shouldTintToolbarButtons = isInPopover && self.shouldTintPopovers;
        }
    }
}

- (BOOL)showControls {
    if (self.HUDViewMode != PSPDFHUDViewNever) return [self setHUDVisible:YES animated:YES];
    else return NO;
}

- (BOOL)hideControls {
    if (![PSPDFBarButtonItem isPopoverVisible] && self.HUDViewMode != PSPDFHUDViewAlways) {
        return [self setHUDVisible:NO animated:YES];
    }else return NO;
}

- (BOOL)toggleControls {
    return [self isHUDVisible] ? [self hideControls] : [self showControls];
}

- (UIStatusBarStyle)statusBarStyle {
    UIStatusBarStyle statusBarStyle;
    switch (self.statusBarStyleSetting & ~PSPDFStatusBarIgnore) {
        case PSPDFStatusBarSmartBlack:
        case PSPDFStatusBarSmartBlackHideOnIpad:
            statusBarStyle = PSIsIpad() ? UIStatusBarStyleBlackOpaque : UIStatusBarStyleBlackTranslucent;
            break;
        case PSPDFStatusBarBlackOpaque:
            statusBarStyle = UIStatusBarStyleBlackOpaque;
            break;
        case PSPDFStatusBarDefault:
            statusBarStyle = UIStatusBarStyleDefault;
            break;
        case PSPDFStatusBarDisable:
        case PSPDFStatusBarInherit:
        default:
            statusBarStyle = [UIApplication sharedApplication].statusBarStyle;
            break;
    }
    return statusBarStyle;
}

- (BOOL)isHUDVisible {
    return self.HUDView.alpha > 0.f;
}

// send all HUD subviews hidden/visible state
- (void)setHUDSubviewsHidden:(BOOL)hidden {
    for (UIView *hudSubView in self.HUDView.subviews) {
        hudSubView.hidden = hidden;
    }
}

- (void)setHUDVisible:(BOOL)HUDVisible {
    [self setHUDVisible:HUDVisible animated:NO];
}

- (BOOL)setHUDVisible:(BOOL)show animated:(BOOL)animated {
    if (self.isViewLockEnabled) return NO;

    [self willChangeValueForKey:@"HUDVisible"];
    BOOL isShown = [self isHUDVisible];
    UIStatusBarStyle statusBarStyle = [self statusBarStyle];
    if (show == isShown) {
        // TODO: needed at all?
        if (show && self.isToolbarEnabled && statusBarStyle != UIStatusBarStyleDefault) {
            self.internalNavigationController.navigationBarHidden = !isShown;
        }
        [self didChangeValueForKey:@"HUDVisible"];
        return YES;
    }

    BOOL allowChange = YES;
    if (show) {
        if ([_delegate respondsToSelector:@selector(pdfViewController:shouldShowHUD:)]) {
            allowChange = [_delegate pdfViewController:self shouldShowHUD:animated];
        }
    }else {
        if ([_delegate respondsToSelector:@selector(pdfViewController:shouldHideHUD:)]) {
            allowChange = [_delegate pdfViewController:self shouldHideHUD:animated];
        }
    }
    if (!allowChange) { [self didChangeValueForKey:@"HUDVisible"]; return NO; }

    if (self.wantsFullScreenLayout && !(self.statusBarStyleSetting & PSPDFStatusBarIgnore)) {
        [self setStatusBarHidden:!show withAnimation:UIStatusBarAnimationFade];
    }

    BOOL viewIsAppearing = _flags.viewWillAppearAnimatedActive;
    BOOL isModal = NO;
    isModal = self.parentViewController.presentingViewController == self.presentingViewController;
    // we need to perform this AFTER changing the statusbar, or else it gets overlayed (iOS BUG)
    dispatch_async(dispatch_get_main_queue(), ^{
        // only switch if we start showing
        if (!isShown && self.isToolbarEnabled) {

            // Seems this hack for the statusbar placement is no longer needed in iOS6 anyway.
            // This is also needed when showing a view modally, regardless of iOS version.
            // Without, the buttons are not shown when using Storyboards.

            // If the navBar is not transparent, this will create frame change events and break state when zoomed in (and also it is slow) so disallow when zoomed in.
            // TODO: this is a mess, needs proper fix (maybe the navigationBarHidden hack is good enough to be invoked once on controller display?)
            BOOL isZoomed = self.pagingScrollView.zoomScale > 1 || [self pageViewForPage:self.page].scrollView.zoomScale > 1;
            if ((!viewIsAppearing || isModal) && !isZoomed) {
                self.internalNavigationController.navigationBarHidden = YES;
                self.internalNavigationController.navigationBarHidden = NO;
            }
        }

        if (show) {
            if ([_delegate respondsToSelector:@selector(pdfViewController:willShowHUD:)]) {
                [_delegate pdfViewController:self willShowHUD:animated];
            }
        }else {
            if ([_delegate respondsToSelector:@selector(pdfViewController:willHideHUD:)]) {
                [_delegate pdfViewController:self willHideHUD:animated];
            }
        }

        dispatch_block_t animationBlock = ^{
            BOOL showOverride = show || !self.document.isValid;
            self.HUDView.alpha = showOverride ? 1.f : 0.f;
            if (self.isToolbarEnabled) {
                BOOL navBarAlwaysVisible = statusBarStyle == UIStatusBarStyleDefault;
                self.internalNavigationController.navigationBar.alpha = (showOverride || navBarAlwaysVisible) ? 1.0f : 0.0f;
                self.internalNavigationController.navigationBar.translucent = statusBarStyle != UIStatusBarStyleDefault;
                self.navigationBarHidden = !showOverride && !navBarAlwaysVisible;
            }
        };

        // ensure label is positioned correctly
        if (show) [self updateDocumentLabelViewPosition];

        if (animated) {
            CGFloat animationDuration = 0.33f; // exact time for the status bar fade
            [UIView animateWithDuration:animationDuration delay:0.f options:UIViewAnimationOptionAllowUserInteraction animations:^{
                animationBlock();
            } completion:^(BOOL finished) {
                if (finished) {
                    [self setHUDSubviewsHidden:!show];

                    if (show) {
                        if ([_delegate respondsToSelector:@selector(pdfViewController:didShowHUD:)]) {
                            [_delegate pdfViewController:self didShowHUD:animated];
                        }
                    }else {
                        if ([_delegate respondsToSelector:@selector(pdfViewController:didHideHUD:)]) {
                            [_delegate pdfViewController:self didHideHUD:animated];
                        }
                    }
                }
            }];
            [self setHUDSubviewsHidden:NO];
        }else {
            animationBlock();
            [self setHUDSubviewsHidden:!show];
        }
    });
    [self didChangeValueForKey:@"HUDVisible"];
    return YES;
}

- (void)scrollGridToVisiblePageAnimated:(BOOL)animated {
    // don't use realPage as this could be the left page in a two page layout.
    // don't try to scroll if grid is not yet loaded (visibleItemsIndex.count == 0)
    NSArray *visibleItemsIndex = [self.gridView indexPathsForVisibleItems];

    // try to reload grid (maybe it's just not loaded after the frame change, this is usually async)
    if ([visibleItemsIndex count] == 0) {
        [self.gridView reloadData];
        [self.gridView layoutSubviews];
        visibleItemsIndex = [self.gridView indexPathsForVisibleItems];
    }

    NSUInteger lastVisiblePage = [[[self visiblePageNumbers] lastObject] unsignedIntegerValue];
    if ([visibleItemsIndex count]) {
        [self.gridView scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:lastVisiblePage inSection:0] atScrollPosition:PSTCollectionViewScrollPositionCenteredVertically animated:animated];
        //PSTCollectionViewScrollPositionCenteredVertically|PSTCollectionViewScrollPositionCenteredHorizontally
        [self.gridView layoutSubviews];
    }

    // scroll later (e.g. viewMode has been changed on init)
    if ([visibleItemsIndex count] == 0) _flags.gridNeedsScrollToPage = YES;
    else _flags.gridNeedsScrollToPage = NO;
}

- (void)setViewMode:(PSPDFViewMode)viewMode animated:(BOOL)animated {
    if (viewMode != _viewMode) {

        // don't allow view mode change if password view is visible.
        if (self.passwordView.window) return;

        if (animated) {
            [self willChangeValueForKey:@"viewModeAnimated"];
        }
        [self willChangeValueForKey:@"viewMode"];
        _viewMode = viewMode;

        // sync thumbnail control
        if (self.viewModeItem.viewModeSegment.selectedSegmentIndex != viewMode) {
            self.viewModeItem.viewModeSegment.selectedSegmentIndex = viewMode;
        }

        // hide any open popovers
        self.popoverController = nil;

        // preparations (insert grid in view stack
        if (viewMode == PSPDFViewModeThumbnails) {

            // At this stage, grid might not even exist at all (it's created lazily)
            // Ensure that the grid is loaded and scroll-able by calling layoutSubviews.
            // If we don't call this, the initial scrollToItemAtIndexPath would be ignored.
            [self.gridView layoutSubviews];

            // never animate, we already fade in, no need to scroll.
            [self scrollGridToVisiblePageAnimated:NO];

            // update cells to renew bookmark status
            [[self.gridView visibleCells] makeObjectsPerformSelector:@selector(updateCell)];

            self.gridView.hidden = NO;
            self.gridView.alpha = 0.0f;
            self.pagingScrollView.alpha = 1.0f;
        }else {
            [self.gridView setContentOffset:self.gridView.contentOffset animated:NO]; // stop scrolling, fixes a disappearing grid bug.
            self.gridView.alpha = 1.f;
            self.pagingScrollView.hidden = NO;
            self.pagingScrollView.alpha = 0.f;
        }

        // don't overlay animations with our scrollView
        [self.view insertSubview:self.pagingScrollView belowSubview:self.HUDView];

        // prepare views
        NSMutableArray *pageViews = [NSMutableArray array];
        NSMutableArray *pageImageViews = [NSMutableArray new];
        NSMutableArray *targetCells = [NSMutableArray array];
        NSMutableArray *shadowPageSetting = [NSMutableArray array];
        NSMutableArray *shadowScrollSetting = [NSMutableArray array];
        NSMutableArray *fullRects = [NSMutableArray array];
        NSMutableArray *smallRects = [NSMutableArray array];
        for (PSPDFPageView *pageView in [self visiblePageViews]) {
            // can return nil when we are in double page mode and show first/last page
            [pageViews addObject:pageView];

            // stop if more pages are loaded than displayed
            if ([pageViews count] && ![self isDoublePageMode]) {
                break;
            }
        }

        // load stuff/visible views
        if (animated) {
            for (PSPDFPageView *pageView in pageViews) {
                // targetCell might not be visible.
                CGRect targetCellRect;

                PSUICollectionViewCell *targetCell = (PSUICollectionViewCell *)[self.gridView cellForItemAtIndexPath:[NSIndexPath indexPathForItem:pageView.page inSection:0]];
                if (targetCell) {
                    targetCell.hidden = YES;
                    [targetCells addObject:targetCell];
                    targetCellRect = targetCell.frame;
                }else {
                    // TODO: use center!
                    targetCellRect = CGRectZero;
                }

                // try to load already visible image, else load from the cache
                CGImageRef imageRef = pageView.contentView.image.CGImage;
                UIImage *pageImage;
                if (imageRef) {
                    pageImage = [UIImage imageWithCGImage:imageRef scale:pageView.contentView.image.scale orientation:pageView.contentView.image.imageOrientation];
                }else {
                    pageImage = [[PSPDFCache sharedCache] cachedImageForDocument:self.document page:pageView.page size:PSPDFSizeNative preload:NO];
                }
                UIImageView *pageImageView = [[UIImageView alloc] initWithImage:pageImage];
                pageImageView.backgroundColor = pageImage ? [UIColor clearColor] : [UIColor whiteColor];
                [self.view insertSubview:pageImageView aboveSubview:self.pagingScrollView];

                CGRect fullRect = [pageView convertRect:pageView.bounds toView:self.view];
                [fullRects addObject:[NSValue valueWithCGRect:fullRect]];
                CGRect relativeCellRect = [self.gridView convertRect:targetCellRect toView:self.view];
                [smallRects addObject:[NSValue valueWithCGRect:relativeCellRect]];

                pageImageView.frame = viewMode == PSPDFViewModeThumbnails ? fullRect : relativeCellRect;
                pageImageView.contentMode = UIViewContentModeScaleAspectFit;
                [pageImageViews addObject:pageImageView];

                // disable shadows until we're done with the animation
                [shadowPageSetting addObject:@(pageView.isShadowEnabled)];
                [shadowScrollSetting addObject:@(pageView.scrollView.isShadowEnabled)];
            }

            // disable shadow in an extra loop (to not change connected shadow while saving state)
            for (PSPDFPageView *pageView in pageViews) {
                pageView.shadowEnabled = NO;
                pageView.scrollView.shadowEnabled = NO;
            }
        }

        [UIView animateWithDuration:animated ? 0.25f : 0.f delay:0.f options:UIViewAnimationOptionBeginFromCurrentState | UIViewAnimationOptionAllowUserInteraction animations:^{
            for (NSUInteger index = 0; index < [pageViews count]; index++) {
                PSPDFPageView *pageView = pageViews[index];
                pageView.hidden = YES;
                if (animated) {
                    [self updatePositionViewPosition];
                    [self updateDocumentLabelViewPosition];
                    UIImageView *pageImage = pageImageViews[index];
                    CGRect relativeCellRect = [(viewMode == PSPDFViewModeThumbnails ? smallRects : fullRects)[index] CGRectValue];
                    pageImage.frame = relativeCellRect;
                }
            }

            if (viewMode == PSPDFViewModeThumbnails) { // grid
                self.pagingScrollView.alpha = 0.f;
                self.gridView.alpha = 1.f;
            }else {
                self.pagingScrollView.alpha = 1.f;
                self.gridView.alpha = 0.f;
            }
        } completion:^(BOOL finished) {
            BOOL isShowingThumbnails = viewMode == PSPDFViewModeThumbnails;
            if (finished) {
                self.pagingScrollView.hidden = isShowingThumbnails;
                self.gridView.hidden = !isShowingThumbnails;
            }

            // ensure that cells are visible again
            for (NSUInteger index = 0; index < [pageViews count]; index++) {
                PSPDFPageView *pageView = pageViews[index];
                pageView.hidden = isShowingThumbnails;
                if (animated) {
                    pageView.shadowEnabled = [shadowPageSetting[index] boolValue];
                    pageView.scrollView.shadowEnabled = [shadowScrollSetting[index] boolValue];
                    PSPDFThumbnailGridViewCell *targetCell = index < [targetCells count] ? targetCells[index] : nil;
                    targetCell.hidden = NO;
                    // no matter of the animation finished or not, remove the images
                    [pageImageViews makeObjectsPerformSelector:@selector(removeFromSuperview)];
                }
            }
        }];
        if (animated) {
            [self didChangeValueForKey:@"viewModeAnimated"];
        }
        [self didChangeValueForKey:@"viewMode"];

        // update toolbar (buttons might change)
        [self createToolbarAnimated:animated];

        [self delegateDidChangeViewMode:viewMode];
        UIAccessibilityPostNotification(UIAccessibilityLayoutChangedNotification, nil);
    }
}

- (void)setViewMode:(PSPDFViewMode)viewMode {
    [self setViewMode:viewMode animated:NO];
}

- (void)updateGridForOrientation {
    // TODO: don't make this depending on the orientation.

    // on iPhone, the navigation toolbar is either 44 (portrait) or 30 (landscape) pixels
    CGFloat transparentToolbarOffset = [self hudTransparencyOffset];
    NSUInteger spacing = 15;
    UIEdgeInsets sectionInset = UIEdgeInsetsMake(spacing + transparentToolbarOffset, spacing, spacing, spacing);
    CGSize thumbnailSize = self.thumbnailSize;
    if (!PSIsIpad()) {
        thumbnailSize = CGSizeMake(floorf(_thumbnailSize.width * _iPhoneThumbnailSizeReductionFactor), floorf(thumbnailSize.height * _iPhoneThumbnailSizeReductionFactor));
    }
    UICollectionViewFlowLayout *flowLayout = ((UICollectionViewFlowLayout *)_gridView.collectionViewLayout);
    flowLayout.itemSize = thumbnailSize;
    flowLayout.sectionInset = sectionInset;
}

- (UIScrollView *)gridView {
    if (!_gridView) {
        PSUICollectionViewFlowLayout *flowLayout = [PSUICollectionViewFlowLayout new];
        PSUICollectionView *collectionView = [[PSUICollectionView alloc] initWithFrame:self.view.bounds collectionViewLayout:flowLayout];

        flowLayout.minimumLineSpacing = 10;
        flowLayout.minimumInteritemSpacing = 10;
        [collectionView registerClass:[self classForClass:[PSPDFThumbnailGridViewCell class]] forCellWithReuseIdentifier:NSStringFromClass([PSPDFThumbnailGridViewCell class])];
        collectionView.delegate = self;
        collectionView.dataSource = self;
        collectionView.backgroundColor = [UIColor clearColor];
        collectionView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        self.gridView = collectionView;
        [self updateGridForOrientation];

        [self.view insertSubview:self.gridView belowSubview:self.HUDView];
    }

    return _gridView;
}

- (BOOL)isDoublePageModeForOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // continuous scroll mode doesn't yet support two page mode (and it doesn't make a lot of sense anyway)
    if (self.pageTransition == PSPDFPageScrollContinuousTransition) return NO;

    BOOL isDoublePageMode = self.pageMode == PSPDFPageModeDouble || (self.pageMode == PSPDFPageModeAutomatic && UIInterfaceOrientationIsLandscape(interfaceOrientation));
    if (isDoublePageMode && self.pageMode == PSPDFPageModeAutomatic) {
        if (self.document.isValid) {
            PSPDFPageInfo *pageInfo = [self.document pageInfoForPage:0];
            if (pageInfo) {
                CGSize pageSize = pageInfo.rotatedPageRect.size;
                isDoublePageMode = pageSize.height > pageSize.width && _document.pageCount > 1;
            }else {
                PSPDFLogWarning(@"Could not get pageInfo for %d", self.page);
            }
        }
    }
    return isDoublePageMode;
}

// dynamically determine if we're landscape or not. (also checks if double page mode makes any sense at all)
- (BOOL)isDoublePageMode {
    return [self isDoublePageModeForOrientation:[[UIApplication sharedApplication] statusBarOrientation]];
}

- (BOOL)isDoublePageModeForPage:(NSUInteger)page {
    BOOL isDoublePageModeForPage = [self isDoublePageMode];
    if (isDoublePageModeForPage) {
        // single first page?
        if (page == 0 && !self.isDoublePageModeOnFirstPage) {
            isDoublePageModeForPage = NO;
        }

        BOOL isOddCount = [self.document pageCount] % 2;
        if (page == [self.document pageCount]-1) {
            if ((isOddCount && self.isDoublePageModeOnFirstPage) ||
                (!isOddCount && !self.isDoublePageModeOnFirstPage)) {
                isDoublePageModeForPage = NO;
            }
        }
    }
    return isDoublePageModeForPage;
}

/// YES if we are at the last page
- (BOOL)isLastPage {
    return self.page >= self.document.pageCount-1;
}

/// YES if we are at the first page
- (BOOL)isFirstPage {
    return self.page == 0;
}

- (NSUInteger)actualPage:(NSUInteger)aPage convert:(BOOL)convert {
    NSUInteger actualPage = aPage;
    if (convert) {
        if (self.doublePageModeOnFirstPage) {
            actualPage = floor(aPage/2.0);
        }else if (aPage) {
            actualPage = ceil(aPage/2.0);
        }
    }

    return actualPage;
}

// 0,1,2,3,4,5
- (NSUInteger)actualPage:(NSUInteger)aPage {
    return [self actualPage:aPage convert:[self isDoublePageMode]];
}

- (NSUInteger)landscapePage:(NSUInteger)aPage convert:(BOOL)convert {
    NSUInteger landscapePage = aPage;
    if (convert) {
        if (self.doublePageModeOnFirstPage) {
            landscapePage = aPage*2;
        }else if (aPage) { // don't produce a -1
            landscapePage = aPage*2-1;
        }
    }
    return landscapePage;
}

// doublePageModeOnFirstPage: 0,0,1,1,2,2,3
// !doublePageModeOnFirstPage 0,1,1,2,2,3,3
- (NSUInteger)landscapePage:(NSUInteger)aPage {
    return [self landscapePage:aPage convert:[self isDoublePageMode]];
}

// for showing the *human readable* page displayed
- (NSUInteger)humanReadablePageForPage:(NSUInteger)aPage {
    NSUInteger humanPage = aPage + 1; // increase on 1 (pages start at 1 for us)

    if (humanPage > [self.document pageCount]) {
        humanPage = [self.document pageCount];
    }

    return humanPage;
}

// returns if the page would be or is a right page in double page mode
- (BOOL)isRightPageInDoublePageMode:(NSUInteger)page {
    BOOL isRightPage = ((page%2 == 1 && self.isDoublePageModeOnFirstPage) || (page%2 == 0 && !self.isDoublePageModeOnFirstPage));
    return isRightPage;
}

- (void)updateSettingsForRotation:(UIInterfaceOrientation)toInterfaceOrientation {
    void (^rotationBlock)(PSPDFViewController *pdfController, UIInterfaceOrientation toInterfaceOrientation_) = self.updateSettingsForRotationBlock;
    if (rotationBlock) rotationBlock(self, toInterfaceOrientation);
}

// sadly, this is needed. The UINavigationBar frame sometimes gets out of sync after MPMoviePlayerController fullscreen rortations.
// We know about this and apply a fix after the animation is done.
- (void)fixNavigationBarFrame:(NSNotification *)notification {
    if ([self isViewLoaded] && ![self isEmbedded] && self.internalNavigationController.navigationBar) {
        CGRect navigationBarFrame = self.internalNavigationController.navigationBar.frame;
        CGRect statusBarFrame = [UIApplication sharedApplication].statusBarFrame;
        if (navigationBarFrame.origin.y <= statusBarFrame.origin.y) {
            [UIView animateWithDuration:0.25f delay:0.f options:UIViewAnimationOptionAllowUserInteraction animations:^{
                CGRect fixedFrame = navigationBarFrame;
                // compensate rotation
                fixedFrame.origin.y = fminf(statusBarFrame.size.height, statusBarFrame.size.width);
                self.internalNavigationController.navigationBar.frame = fixedFrame;
            } completion:nil];
        }
    }
}

- (void)setScrollingEnabled:(BOOL)scrollingEnabled {
    _scrollingEnabled = scrollingEnabled;
    self.pagingScrollView.scrollEnabled = scrollingEnabled;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Custom Class Override Helper

// ensure overridden class are correct subclasses.
- (void)setOverrideClassNames:(NSDictionary *)overrideClassNames {
    if (overrideClassNames != _overrideClassNames) {
        PSPDFCheckOverrideClassNames(overrideClassNames); // throws an exception if invalid.
        _overrideClassNames = overrideClassNames;
    }
}

// Looks up an entry in overrideClassNames for custom Class subclasses
- (Class)classForClass:(Class)originalClass {
    return PSPDFClassForClassInDictionary(originalClass, _overrideClassNames);
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return section == 0 ? [self.document pageCount] : 0;
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    PSPDFThumbnailGridViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([PSPDFThumbnailGridViewCell class]) forIndexPath:indexPath];

    // configure cell
    cell.document = self.document;
    cell.page = indexPath.row;
    cell.siteLabel.text = [self.document pageLabelForPage:indexPath.row substituteWithPlainLabel:YES];
    [cell setNeedsLayout]; // update siteLabel

    return (UICollectionViewCell *)cell;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    [self setPage:indexPath.item animated:NO];

    // we need some extra time to lay out the views correctly.
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.0001f * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        [self setViewMode:PSPDFViewModeDocument animated:YES];
    });
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UIPopoverController

- (BOOL)popoverControllerShouldDismissPopover:(UIPopoverController *)popoverController {
    if ([popoverController isKindOfClass:[PSPDFPopoverController class]]) {
        // we don't know about animation state here.
        [(PSPDFPopoverController *)popoverController notifyContentControllerAboutDismissalAnimated:YES];
    }
    return YES;
}

- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController {
    self.popoverController = nil;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - MFMailComposeViewControllerDelegate

// for email sheet on mailto: links
- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error {
    [[self masterViewController] dismissViewControllerAnimated:YES completion:NULL];
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - PSPDFTextSearchDelegate

- (PSPDFSearchHighlightView *)highlightViewForSearchResult:(PSPDFSearchResult *)searchResult inView:(UIView *)view {
    for (UIView *subview in view.subviews) {
        if ([subview isKindOfClass:[PSPDFSearchHighlightView class]]) {
            PSPDFSearchHighlightView *highlightView = (PSPDFSearchHighlightView *)subview;
            if ([highlightView.searchResult isEqual:searchResult]) {
                return highlightView;
            }
        }
    }
    return nil;
}

// private helper to iterate over PSPDFSearchHighlightView
- (void)iterateHighlightViewsWithBlock:(void(^)(PSPDFSearchHighlightView *highlightView))block {
    if (self.document.textSearch.searchMode == PSPDFSearchModeHighlighting) {
        for (PSPDFPageView *pageView in [self visiblePageViews]) {
            for (UIView *subview in pageView.subviews) {
                if ([subview isKindOfClass:[PSPDFSearchHighlightView class]]) {
                    PSPDFSearchHighlightView *highlightView = (PSPDFSearchHighlightView *)subview;
                    block(highlightView);
                }
            }
        }
    }
}

- (void)animateSearchHighlight:(PSPDFSearchResult *)searchResult {
    [self iterateHighlightViewsWithBlock:^(PSPDFSearchHighlightView *highlightView) {
        if (highlightView.searchResult == searchResult) {
            [highlightView popupAnimation];
        }
    }];
}

- (void)clearHighlightedSearchResults {
    [self iterateHighlightViewsWithBlock:^(PSPDFSearchHighlightView *highlightView) {
        [highlightView removeFromSuperview];
    }];
}

// add to the current page!
- (void)addHighlightSearchResults:(NSArray *)searchResults {
    if (self.document.textSearch.searchMode == PSPDFSearchModeHighlighting) {
        PSPDFPageView *pageView = nil;
        NSArray *visiblePageNumbers = [self visiblePageNumbers];
        for (PSPDFSearchResult *searchResult in searchResults) {
            if (searchResult.selection) {
                NSUInteger page = searchResult.pageIndex;
                if ([visiblePageNumbers containsObject:@(page)]) {
                    if (!pageView || pageView.page != page) {
                        pageView = [self pageViewForPage:page];
                    }

                    // mark text
                    if (pageView) {
                        PSPDFSearchHighlightView *highlight = [self highlightViewForSearchResult:searchResult inView:pageView];
                        if (!highlight) {
                            highlight = [[[self classForClass:[PSPDFSearchHighlightView class]] alloc] initWithSearchResult:searchResult];
                            [pageView insertSubview:highlight aboveSubview:pageView.contentView]; // respect other views
                            [highlight popupAnimation];
                        }
                    }
                }
            }
        }
    }
}

- (void)willStartSearch:(PSPDFTextSearch *)textSearch forTerm:(NSString *)searchTerm isFullSearch:(BOOL)isFullSearch {
    if (isFullSearch) {
        [self clearHighlightedSearchResults];
    }
}

- (void)didUpdateSearch:(PSPDFTextSearch *)textSearch forTerm:(NSString *)searchTerm newSearchResults:(NSArray *)searchResults forPage:(NSUInteger)page {    [self addHighlightSearchResults:searchResults];
}

- (void)didCancelSearch:(PSPDFTextSearch *)textSearch forTerm:(NSString *)searchTerm isFullSearch:(BOOL)isFullSearch {
    [self clearHighlightedSearchResults];
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - PSPDFPasswordViewDelegate

/// did unlock with password successfully.
- (void)passwordView:(PSPDFPasswordView *)passwordView didUnlockWithPassword:(NSString *)password {
    [passwordView removeFromSuperview];
    [self reloadData];
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - PSPDFOutlineViewDelegate

- (BOOL)outlineController:(PSPDFOutlineViewController *)outlineController didTapAtElement:(PSPDFOutlineElement *)outlineElement {
    BOOL processed = outlineElement.relativePath || outlineElement.page > 0;

    if (processed) {
        // close controller
        if (PSIsIpad()) [PSPDFBarButtonItem dismissPopoverAnimated:YES];
        else [self closeModalView]; // close outline controller

        if (outlineElement.relativePath) {
            PSPDFDocument *newDocument = [self delegateDocumentForRelativePath:outlineElement.relativePath];
            if (!newDocument) {
                [[[UIAlertView alloc] initWithTitle:PSPDFLocalize(@"File Not Found") message:outlineElement.relativePath delegate:nil cancelButtonTitle:nil otherButtonTitles:PSPDFLocalize(@"OK"), nil] show];
            }else {
                self.document = newDocument;
                [self setPage:outlineElement.page-1 animated:NO];
                self.viewMode = PSPDFViewModeDocument; // ensure that we show documents
            }
        }else {
            // scroll to page (-1 because we manage pages internally different)
            if (outlineElement.page > 0) {
                [self setPage:outlineElement.page-1 animated:PSPDFShouldAnimate()];
                self.viewMode = PSPDFViewModeDocument; // ensure that we show documents
            }
        }
    }
    return processed;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - PSPDFBookmarkViewControllerDelegate

- (NSUInteger)currentPageForBookmarkViewController:(PSPDFBookmarkViewController *)bookmarkController {
    return self.page;
}

- (void)bookmarkViewController:(PSPDFBookmarkViewController *)bookmarkController didSelectBookmark:(PSPDFBookmark *)bookmark {
    [self setPage:bookmark.page animated:YES];
    [self.popoverController dismissPopoverAnimated:YES];
    self.popoverController = nil;
    [bookmarkController dismissViewControllerAnimated:YES completion:NULL];
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UIGestureRecognizerDelegate

static UIBarButtonItem *PSPDFBarButtonItemFromView(UIView *view, BOOL resolveCopiedButton) {
    if ([view isKindOfClass:[UIControl class]]) {
        UIBarButtonItem *button = [[(UIControl *)view allTargets] anyObject];
        if (resolveCopiedButton) {
            if ([button isKindOfClass:[PSPDFBarButtonItem class]]) return (PSPDFBarButtonItem *)button;
        }else if ([button isKindOfClass:[UIBarButtonItem class]]) return button;
    }
    return nil;
}

// add support to detect long presses on the toolbar.
static char kPSPDFBarButtonItemToken, kPSPDFSenderButtonItemToken;
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    if ([gestureRecognizer isKindOfClass:[UILongPressGestureRecognizer class]] && [gestureRecognizer.view isKindOfClass:[PSPDFTransparentToolbar class]]) {
        UIBarButtonItem *copyButtonItem = PSPDFBarButtonItemFromView(touch.view, NO);
        PSPDFBarButtonItem *barButtonItem = (PSPDFBarButtonItem *)PSPDFBarButtonItemFromView(touch.view, YES);
        objc_setAssociatedObject(gestureRecognizer, &kPSPDFBarButtonItemToken, barButtonItem, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
        objc_setAssociatedObject(gestureRecognizer, &kPSPDFSenderButtonItemToken, copyButtonItem, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
        return [barButtonItem isLongPressActionAvailable]; // nil = NO.
    }
    else return YES;
}

// handle long presses from the toolbar. Since sender might be different to barButton, we need both for a correct popover target.
- (void)longPressGestureFired:(UILongPressGestureRecognizer *)gesture {
    if (gesture.state == UIGestureRecognizerStateBegan) {
        PSPDFBarButtonItem *barButtonItem = objc_getAssociatedObject(gesture, &kPSPDFBarButtonItemToken);
        PSPDFBarButtonItem *senderButtonItem = objc_getAssociatedObject(gesture, &kPSPDFSenderButtonItemToken);
        [barButtonItem longPressAction:senderButtonItem];
    }
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Deprecated

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-declarations"
- (BOOL)positionViewEnabled { return self.positionViewEnabled; }
- (void)setPositionViewEnabled:(BOOL)positionViewEnabled { self.pageLabelEnabled = positionViewEnabled; }
- (NSArray *)additionalRightBarButtonItems { return self.additionalBarButtonItems; }
- (void)setAdditionalRightBarButtonItems:(NSArray *)additionalRightBarButtonItems {
    self.additionalBarButtonItems = additionalRightBarButtonItems;
}
- (PSPDFScrollDirection)pageScrolling { return self.scrollDirection; }
- (void)setPageScrolling:(PSPDFScrollDirection)scrollDirection { self.scrollDirection = scrollDirection; }
- (NSUInteger)realPage { return self.page; }
#pragma clang diagnostic pop

@end

@implementation PSPDFViewControllerView @end

void PSPDFShowDemoInfo(void) {
#ifdef kPSPDFKitDemoMode
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        NSLog(@"-----------------------------------------------------------------------------");
        NSLog(@"Welcome to the demo version of %@.\n", PSPDFVersionString());
        NSLog(@"Check out the API documentation at http://pspdfkit.com/documentation/");
        NSLog(@"If you have specific questions, contact pspdfkit@petersteinberger.com");
        NSLog(@"-----------------------------------------------------------------------------");
    });
#endif
}
