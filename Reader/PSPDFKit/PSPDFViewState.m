//
//  PSPDFViewState.m
//  PSPDFKit
//
//  Copyright (c) 2012 Peter Steinberger. All rights reserved.
//

#import "PSPDFViewState.h"

@implementation PSPDFViewState

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (NSString *)description {
    return [NSString stringWithFormat:@"<%@ page:%d contentOffset:%@ zoomScale:%.2f HUDVisible:%@>", NSStringFromClass([self class]), _page, NSStringFromCGPoint(_contentOffset), _zoomScale, _HUDVisible ? @"YES" : @"NO"];
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSCopying

- (id)copyWithZone:(NSZone *)zone {
    PSPDFViewState *viewState = [PSPDFViewState new];
    viewState.zoomScale = self.zoomScale;
    viewState.contentOffset = self.contentOffset;
    viewState.page = self.page;
    viewState.HUDVisible = self.isHUDVisible;
    return viewState;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSCoding

// password is not serialized!
- (id)initWithCoder:(NSCoder *)coder {
    if ((self = [self init])) {
        _zoomScale = [[coder decodeObjectForKey:NSStringFromSelector(@selector(zoomScale))] floatValue];
        _contentOffset = [[coder decodeObjectForKey:NSStringFromSelector(@selector(contentOffset))] CGPointValue];
        _page = [[coder decodeObjectForKey:NSStringFromSelector(@selector(page))] unsignedIntegerValue];
        _HUDVisible = [[coder decodeObjectForKey:NSStringFromSelector(@selector(isHUDVisible))] boolValue];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)coder {
    [coder encodeObject:@(_zoomScale) forKey:NSStringFromSelector(@selector(zoomScale))];
    [coder encodeObject:[NSValue valueWithCGPoint:_contentOffset] forKey:NSStringFromSelector(@selector(contentOffset))];
    [coder encodeObject:@(_page) forKey:NSStringFromSelector(@selector(page))];
    [coder encodeObject:@(_HUDVisible) forKey:NSStringFromSelector(@selector(isHUDVisible))];
}

@end
