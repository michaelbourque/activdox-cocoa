//
//  PSPDFViewController+Internal.h
//  PSPDFKit
//
//  Copyright (c) 2011-2012 Peter Steinberger. All rights reserved.
//

#import "PSPDFKitGlobal.h"
#import "PSPDFViewController.h"
#import "PSPDFPageView.h"
#import "PSPDFViewController+Delegates.h"
#import "PSPDFDocument.h"

@class PSPDFLinkAnnotationView, PSPDFAnnotationController, PSPDFScrollView;
@protocol PSPDFAnnotationView;

// Due to rotation while in full-screen mode, the navigationBar sometimes is messed up by the statusBar height. Send a notification that this is checked and fixed.
extern NSString *const PSPDFFixNavigationBarFrameNotification;
extern NSString *const PSPDFHUDNotHiddenNotification; // give controls a chance to show they locked the HUD (e.g. annotation toolbar)
extern NSString *const PSPDFViewControllerWillChangeOrientationNotification;
extern NSString *const PSPDFViewControllerWillAnimateChangeOrientationNotification;
extern NSString *const PSPDFViewControllerDidChangeOrientationNotification;

@interface PSPDFViewController (Internal)

// Helper that queries overrideClassNames.
- (Class)classForClass:(Class)originalClass;

- (void)setStatusBarStyle:(UIStatusBarStyle)statusBarStyle animated:(BOOL)animated;

// Internal page-setter; does not relay scroll-events or checks.
- (void)setPageInternal:(NSUInteger)page;

/// Return page numbers that are visible. Only returns the current set page in continuous scroll mode
/// Useful to get exact pages for double page mode.
- (NSArray *)calculatedVisiblePageNumbers;

// For the scrobble bar
- (BOOL)isTransparentHUD;
- (BOOL)isDarkHUD;
- (UIBarStyle)barStyle;

- (NSUInteger)actualPage:(NSUInteger)aPage;
- (NSUInteger)actualPage:(NSUInteger)aPage convert:(BOOL)convert;
- (NSUInteger)landscapePage:(NSUInteger)aPage;
- (NSUInteger)landscapePage:(NSUInteger)aPage convert:(BOOL)convert;

- (BOOL)shouldShowControls;
- (void)hideControlsIfPageMode;

- (BOOL)isViewWillAppearing;
- (BOOL)viewDidAppearCalled;

// controller locking. Consolidates some other lock properties.
- (BOOL)isControllerLocked;
- (void)lockController;
- (void)unlockController;

// Allow checking rotation, for PSPDFSinglePageViewController
@property (nonatomic, assign, getter=isRotationActive, readonly) BOOL rotationActive;

@property (nonatomic, retain, readonly) PSPDFPageViewController *pageViewController;

@end

@interface PSPDFPageView (PageInternal)
@property (nonatomic, unsafe_unretained) PSPDFViewController *pdfController;
@end

@interface PSPDFDocument (DocumentProviderInternal)
- (Class)classForClass:(Class)originalClass;
@property (atomic, assign) NSUInteger displayingPage;
@end

