//
//  PSPDFDocument.m
//  PSPDFKit
//
//  Copyright 2011-2012 Peter Steinberger. All rights reserved.
//

#import "PSPDFKit.h"
#import "PSPDFKitGlobal+Internal.h"
#import "PSPDFLabelParser.h"
#import "PSPDFTextParser.h"
#import <CommonCrypto/CommonDigest.h>
#import "PSPDFConverter.h"
#import "PSPDFDocumentParser.h"
#import "PSPDFGlyph.h"
#import "PSPDFWord.h"
#import "PSPDFTextLine.h"
#import "PSPDFTextBlock.h"
#import "PSPDFImageInfo.h"
#import "PSPDFBookmarkParser.h"
#import "PSPDFViewController.h"
#import "PSPDFViewControllerDelegate.h"
#import "PSPDFOrderedDictionary.h"
#import "PSPDFGlobalLock.h"
#import "PSPDFFileAnnotationProvider.h"

@interface PSPDFDocument() {
    NSUInteger _pageCount;
    NSMutableDictionary *_pageCountCache; // needs to be mutable, so explicitely set.
    NSMutableDictionary *_fileURLCache;
    dispatch_queue_t _documentProviderQueue;
}
@property (atomic, assign) NSUInteger displayingPage;
@property (nonatomic, strong) UIColor *backgroundColor;
@property (nonatomic, copy) NSDictionary *overrideClassNames;
@property (nonatomic, copy, readonly) NSDictionary *pageCountCache;
@property (nonatomic, assign) CGDataProviderRef dataProvider;
@property (nonatomic, copy) NSArray *dataArray; // external write support
@property (atomic, copy) void (^didCreateDocumentProviderBlock)(PSPDFDocumentProvider *documentProvider);
@end

@implementation PSPDFDocument

@synthesize documentProviders = _documentProviders, dataProvider = _dataProvider, basePath = _basePath;
@synthesize files = _files, title = _title, password = _password;

//////////////////////////////////////////////////////////f/////////////////////////////////
#pragma mark - NSObject

+ (instancetype)PDFDocument {
    return [[[self class] alloc] init];
}

+ (instancetype)PDFDocumentWithData:(NSData *)data {
    return [self PDFDocumentWithDataArray:data ? @[data] : nil];
}

+ (instancetype)PDFDocumentWithDataArray:(NSArray *)dataArray {
    return [(PSPDFDocument *)[[self class] alloc] initWithDataArray:dataArray];
}

+ (instancetype)PDFDocumentWithDataProvider:(CGDataProviderRef)dataProvider {
    return [(PSPDFDocument *)[[self class] alloc] initWithDataProvider:dataProvider];
}

+ (instancetype)PDFDocumentWithURL:(NSURL *)URL {
    return [(PSPDFDocument *)[[self class] alloc] initWithURL:URL];
}

+ (instancetype)PDFDocumentWithBaseURL:(NSURL *)baseURL files:(NSArray *)files {
    return [[[self class] alloc] initWithBaseURL:baseURL files:files];
}

+ (instancetype)PDFDocumentWithBaseURL:(NSURL *)baseURL fileTemplate:(NSString *)fileTemplate startPage:(NSInteger)startPage endPage:(NSInteger)endPage {
    return [[[self class] alloc] initWithBaseURL:baseURL fileTemplate:fileTemplate startPage:startPage endPage:endPage];
}

- (id)initWithBaseURL:(NSURL *)basePath files:(NSArray *)files {
    if ((self = [self init])) {
        _basePath = [basePath copy];
        self.files = files;
        _UID = [self generateUID];
    }
    return self;
}

- (id)initWithBaseURL:(NSURL *)basePath fileTemplate:(NSString *)fileTemplate startPage:(NSInteger)startPage endPage:(NSInteger)endPage {
    if ((self = [self init])) {
        _basePath = [basePath copy];

        if ([fileTemplate rangeOfString:@"%[0-9]*d" options:NSRegularExpressionSearch].length == 0) {
            PSPDFLogError(@"Token %%d not defined in FileTemplate: %@", fileTemplate); return nil;
        }

        // can be optimized later on to make > 1000 pages documents fast. (implementation detail)
        NSMutableArray *files = [NSMutableArray arrayWithCapacity:MIN(endPage-startPage,1)];
        for (int pageIndex = startPage; pageIndex <= endPage; pageIndex++) {
            [files addObject:[NSString stringWithFormat:fileTemplate, pageIndex]];
        }

        self.files = files;
        _UID = [self generateUID];
    }
    return self;
}

- (id)initWithURL:(NSURL *)URL {
    if ((self = [self init])) {
        [self setFileURL:URL];
    }
    return self;
}

- (id)initWithData:(NSData *)data {
    return self = [self initWithDataArray:@[data]];
}

- (id)initWithDataArray:(NSArray *)dataArray {
    if ((self = [self init])) {
        _dataArray = dataArray;
        _UID = [self generateUID];
    }
    return self;
}

- (id)initWithDataProvider:(CGDataProviderRef)dataProvider {
    if ((self = [self init])) {
        self.dataProvider = dataProvider;

        // helper to get a sensible default caching strategy.
        if ([PSPDFAESCryptoDataProvider isAESCryptoDataProvider:dataProvider]) {
            self.cacheStrategy = PSPDFCacheNothing;
        }
    }
    return self;
}

- (id)init {
    if ((self = [super init])) {
        _fileURLCache = [NSMutableDictionary new];
        _documentProviderQueue = pspdf_dispatch_queue_create("com.PSPDFKit.documentProviderQueue", 0);
        _cacheStrategy = -1;
        _backgroundColor = [UIColor whiteColor];
        _PDFBox = kCGPDFCropBox;

        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didReceiveMemoryWarning) name:UIApplicationDidReceiveMemoryWarningNotification object:nil];

        _pageCount = NSUIntegerMax;
        _aspectRatioEqual = NO;
        _annotationsEnabled = YES;
        _textParserHideGlyphsOutsidePageRect = YES;

        _annotationSaveMode = PSPDFAnnotationSaveModeEmbeddedWithExternalFileAsFallback;

#ifndef PSPDFKIT_DISABLE_TEXT_SELECTION_FEATURES
        _editableAnnotationTypes = [NSSet setWithObjects:
                                    // PSPDFAnnotationTypeStringLink,
                                    PSPDFAnnotationTypeStringHighlight,
                                    PSPDFAnnotationTypeStringUnderline,
                                    PSPDFAnnotationTypeStringStrikeout,
                                    PSPDFAnnotationTypeStringNote,
                                    PSPDFAnnotationTypeStringFreeText,
                                    PSPDFAnnotationTypeStringInk,
                                    PSPDFAnnotationTypeStringSquare,
                                    PSPDFAnnotationTypeStringCircle,
                                    PSPDFAnnotationTypeStringSignature,
                                    nil];
#endif
    }
    return self;
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    PSPDFDispatchRelease(_documentProviderQueue);
    CGDataProviderRelease(_dataProvider);
    _textSearch.delegate = nil;
}

- (NSUInteger)hash {
    return [self.UID hash];
}

- (BOOL)isEqual:(id)other {
    if ([other isKindOfClass:[self class]]) {
        return [self isEqualToDocument:(PSPDFDocument *)other];
    }
    else return NO;
}

- (BOOL)isEqualToDocument:(PSPDFDocument *)otherDocument {
    if (![self.UID isEqual:otherDocument.UID] || !self.UID || !otherDocument.UID) return NO;
    else return YES;
}

- (NSString *)description {
    return [NSString stringWithFormat:@"<%@ UID:%@ files:%d pageCount:%d>", NSStringFromClass([self class]), self.UID, [self.files count], [self pageCount]];
}

- (void)didReceiveMemoryWarning {
    if (!self.displayingPdfController) {
        [self destroyDocumentProviders];
    }
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Bookmarks

// cache directory cannot be nil.
- (NSString *)cacheDirectory {
    NSString *cacheDirectory;

    if (!_cacheDirectory) {
        static dispatch_once_t onceToken;
        __strong static NSString *_staticCacheDirectory;
        dispatch_once(&onceToken, ^{
            _staticCacheDirectory = [[[NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES) ps_firstObject] stringByAppendingPathComponent:@"PrivateDocuments"] stringByAppendingPathComponent:@"PSPDFKit"];
        });
        cacheDirectory = _staticCacheDirectory;
    }else {
        cacheDirectory = _cacheDirectory;
    }
    return [cacheDirectory stringByAppendingPathComponent:self.UID];
}

- (BOOL)ensureCacheDirectoryExistsWithError:(NSError **)error {
    BOOL success = YES;
    NSString *cachePath = self.cacheDirectory;
    NSFileManager *fileManager = [NSFileManager new];
    if (![fileManager fileExistsAtPath:cachePath]) {
        if (![fileManager createDirectoryAtPath:cachePath withIntermediateDirectories:YES attributes:nil error:error]) {
            PSPDFLogError(@"Unable to create bookmark folder at %@: %@", cachePath, *error);
            success = NO;
        }
    }
    return success;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public

- (NSData *)data {
    __block NSData *data = nil;
    pspdf_dispatch_sync_reentrant(_documentProviderQueue, ^{
        data = [self.dataArray ps_firstObject];
    });
    return data;
}

- (void)setDataArray:(NSArray *)dataArray {
    pspdf_dispatch_sync_reentrant(_documentProviderQueue, ^{
        if (_dataArray != dataArray) {
            _dataArray = dataArray;
            /*
             NSMutableString *dataString = [NSMutableString stringWithString:@"<NSData array: "];
             for (NSData *newData in dataArray) {
             [dataString appendFormat:@" %d, ", [newData length]];
             }
             [dataString appendString:@">"];
             PSPDFLog(@"New: %@", dataString);
             */
        }
    });
}

- (void)setDataProvider:(CGDataProviderRef)dataProvider {
    if (dataProvider != _dataProvider) {
        if (_dataProvider) CGDataProviderRelease(_dataProvider);
        CGDataProviderRetain(dataProvider);
        _dataProvider = dataProvider;
    }
}

- (BOOL)saveChangedAnnotationsWithError:(NSError **)error {
#ifndef PSPDFKIT_DISABLE_TEXT_SELECTION_FEATURES
    if (self.annotationSaveMode == PSPDFAnnotationSaveModeDisabled) return NO;

    // should save in PDF?
    BOOL foundDirtyAnnotations = NO;
    BOOL success = YES;
    NSError *localError = nil;
    NSMutableArray *savedAnnotations = [NSMutableArray array];

    for (PSPDFDocumentProvider *documentProvider in self.documentProviders) {
        NSDictionary *dirtyAnnotationsDict = [documentProvider.annotationParser dirtyAnnotations];
        if ([dirtyAnnotationsDict count] > 0) {
            foundDirtyAnnotations = YES;

            // instantly write them back
            if (![documentProvider saveChangedAnnotationsWithError:&localError]) success = NO;

            // collect all dirty annotations
            [dirtyAnnotationsDict enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
                if ([obj count]) [savedAnnotations addObjectsFromArray:obj];
            }];
        }
    }

    // return early if there are no dirty annotations.
    if (!foundDirtyAnnotations) return YES;

    // clear whole cache
    [self destroyDocumentProviders];

    // TODO: could be made way more fine-granular
    // (but flushDocumentReference could fail if we're currently rendering...
    /*
     pspdf_dispatch_sync_reentrant(_documentProviderQueue, ^{
     for (PSPDFDocumentProvider *provider in self.documentProviders) {
     [provider flushDocumentReference];

     // clear cache and reload from file (if we should reload)
     [provider.annotationParser clearCache];
     [provider.annotationParser tryLoadAnnotationsFromFileWithError:nil];
     }
     });
     */

    // call delegate!
    if (success) {
        if ([self.delegate respondsToSelector:@selector(pdfDocument:didSaveAnnotations:)]) {
            [self.delegate pdfDocument:self didSaveAnnotations:savedAnnotations];
        }
    }else {
        if ([self.delegate respondsToSelector:@selector(pdfDocument:failedToSaveAnnotations:withError:)]) {
            [self.delegate pdfDocument:self failedToSaveAnnotations:savedAnnotations withError:localError];
        }
    }

    // copy error if one is set.
    if (error) *error = localError;
    return success;
#else
    if (error) *error = nil;
    return NO;
#endif
}

- (CGFloat)aspectRatioVariance {
    CGFloat minApectRatio = 0;
    CGFloat maxAspectRatio = 0;

    for (NSUInteger page = 0; page < [self pageCount]; page++) {
        CGRect pageRect = [self rectBoxForPage:page];
        if (pageRect.size.height > 0) {
            CGFloat aspectRatio = pageRect.size.width/pageRect.size.height;
            minApectRatio = page == 0 ? aspectRatio : fminf(minApectRatio, aspectRatio);
            maxAspectRatio = page == 0 ? aspectRatio : fmaxf(maxAspectRatio, aspectRatio);
        }
    }

    CGFloat variance = maxAspectRatio - minApectRatio;
    return variance;
}

- (void)setFileURL:(NSURL *)fileURL {
    if (fileURL) {
        if (![fileURL isKindOfClass:[NSURL class]]) {
            PSPDFLogError(@"fileURL needs to be of type NSURL!"); return;
        }

        _basePath = [[fileURL URLByDeletingLastPathComponent] copy];
        NSString  *lastPathComponent = [fileURL lastPathComponent];
        if (lastPathComponent) self.files = @[lastPathComponent];
        else { self.files = nil; PSPDFLogWarning(@"No files set? (fileURL: %@)", fileURL); }
        _UID = [self generateUID];
    }
    [self clearCache];
}

- (NSURL *)fileURL {
    NSArray *files = self.files;
    return [files count] ? [self.basePath URLByAppendingPathComponent:(files)[0]] : nil;
}

- (NSArray *)filesWithBasePath {
    // +1 is to compensate empty files array
    NSMutableArray *newFiles = [NSMutableArray array];
    for (NSString *fileName in self.files) {
        [newFiles addObject:[self fileURLWithFileName:fileName]];
    }
    return newFiles;
}

- (NSString *)fileNameForPage:(NSUInteger)pageIndex {
    return [self fileNameForIndex:[self fileIndexForPage:pageIndex]];
}

- (NSString *)fileNameForIndex:(NSUInteger)fileIndex {
    NSArray *files = self.files;
    NSString *fileName = nil;
    BOOL useFileName = fileIndex < [files count];
    fileName = useFileName ? files[fileIndex] : self.title;
    if ([fileName length] == 0) {
        fileName = PSPDFLocalize(@"Untitled"); // if PDF is build from NSData
    }
    if (![[fileName lowercaseString] hasSuffix:@".pdf"]) {
        fileName = [fileName stringByAppendingString:@".pdf"];
    }

    if (fileIndex > 0 && !useFileName) {
        fileName = [fileName stringByReplacingOccurrencesOfString:@".pdf" withString:[NSString stringWithFormat:@"_%d.pdf", fileIndex+1] options:NSCaseInsensitiveSearch | NSBackwardsSearch range:NSMakeRange(fminf(0, [fileName length]-4), [fileName length])];
    }
    return fileName;
}

- (NSDictionary *)fileNamesWithDataDictionary {
    PSPDFOrderedDictionary *orderedDictionary = [PSPDFOrderedDictionary new];

    NSUInteger fileIndex = 0;
    for (PSPDFDocumentProvider *documentProvider in self.documentProviders) {
        NSData *data = [documentProvider dataRepresentationWithError:NULL]; // error is already logged
        if (data) {
            NSString *fileName;
            do {
                fileName = [self fileNameForIndex:fileIndex];
                fileIndex++;
            }while (orderedDictionary[fileName] != nil);

            orderedDictionary[fileName] = data;
        }
    }
    return orderedDictionary;
}

/// appends a file to the current document. No PDF gets modified, just displayed together.
- (void)appendFile:(NSString *)file {
    if (!file) { PSPDFLogWarning(@"appendFile called with nil argument!"); return; }

    // check if basePath is already set and remove it.
    if (_basePath ) {
        file = [file stringByReplacingOccurrencesOfString:[[_basePath path] stringByAppendingString:@"/"] withString:@""];
    }

    // create new array and set it.
    NSMutableArray *files = [self.files mutableCopy];
    [files addObject:file];
    self.files = files;

    [self clearCache];
}

- (PSPDFPageInfo *)pageInfoForPageNoFetching:(NSUInteger)page {
    return [[self documentProviderForPage:page] pageInfoForPageNoFetching:[self compensatedPageForPage:page]];
}

- (BOOL)hasPageInfoForPage:(NSUInteger)page {
    return [self pageInfoForPageNoFetching:page] != nil;
}

- (PSPDFPageInfo *)pageInfoForPage:(NSUInteger)page {
    return [self pageInfoForPage:page pageRef:nil];
}

- (PSPDFPageInfo *)pageInfoForPage:(NSUInteger)page pageRef:(CGPDFPageRef)pageRef {
    return [[self documentProviderForPage:page] pageInfoForPage:[self compensatedPageForPage:page] pageRef:pageRef];
}

- (PSPDFPageInfo *)nearestPageInfoForPage:(NSUInteger)page {
    PSPDFPageInfo *pageInfo = nil;
    page++;
    do {
        pageInfo = [self pageInfoForPageNoFetching:page-1];
        page--;
    }while (!pageInfo && page > 0);
    return pageInfo;
}

// helper for pageInfoForPage, returns rotatedPageRect
- (CGRect)rectBoxForPage:(NSUInteger)page {
    PSPDFPageInfo *pageInfo = [self pageInfoForPage:page];
    if (pageInfo) return pageInfo.rotatedPageRect;
    else {
        if ([self isValid]) PSPDFLogWarning(@"Returning empty rect for page %d.", page);
        return CGRectZero;
    }
}

// helper for pageInfoForPage, returns pageRotation
- (int)rotationForPage:(NSUInteger)page {
    PSPDFPageInfo *pageInfo = [self pageInfoForPage:page];
    if (pageInfo) return pageInfo.pageRotation;
    else {
        if ([self isValid]) PSPDFLogWarning(@"Returning 0 rotation for page %d.", page);
        return 0;
    }
}

- (void)setPDFBox:(CGPDFBox)PDFBox {
    _PDFBox = PDFBox;
    [self clearCache];
}

- (NSArray *)documentProviders {
    __block NSArray *documentProviders;
    pspdf_dispatch_sync_reentrant(_documentProviderQueue, ^{
        if (!_documentProviders) [self createDocumentProviders];
        documentProviders = _documentProviders;
    });
    return documentProviders;
}

- (void)createDocumentProviders {
    pspdf_dispatch_sync_reentrant(_documentProviderQueue, ^{
        [self destroyDocumentProviders];
        _documentProviders = @[]; // to block ensureDocumentProvidersAreCreated
        Class documentProviderClass = [self classForClass:[PSPDFDocumentProvider class]];
        NSMutableArray *documentProviders = [NSMutableArray array];

        // create all the providers!
        PSPDFDocumentProvider *documentProvider;
        if (self.dataProvider) {
            documentProvider = [[documentProviderClass alloc] initWithDataProvider:self.dataProvider document:self];
            documentProvider = [self didCreateDocumentProvider:documentProvider];
            [documentProviders addObject:documentProvider];
        }
        for (NSData *data in self.dataArray) {
            documentProvider = [[documentProviderClass alloc] initWithData:data document:self];
            documentProvider = [self didCreateDocumentProvider:documentProvider];
            [documentProviders addObject:documentProvider];
        }
        for (NSString *file in self.files) {
            NSURL *fileURL = [self fileURLWithFileName:file];
            documentProvider = [[documentProviderClass alloc] initWithFileURL:fileURL document:self];
            documentProvider = [self didCreateDocumentProvider:documentProvider];
            [documentProviders addObject:documentProvider];
        }

        // support for subclasses that don't correctly fill the files array
        if ([documentProviders count] == 0 && [self pageCount] > 0) {
            NSMutableSet *files = [NSMutableSet set];
            for (NSUInteger page=0 ; page < [self pageCount]; page++) {
                NSURL *fileURL = [self URLForFileIndex:[self fileIndexForPage:page]];
                if (fileURL && ![files containsObject:fileURL]) {
                    [files addObject:fileURL];
                    documentProvider = [[documentProviderClass alloc] initWithFileURL:fileURL document:self];
                    documentProvider = [self didCreateDocumentProvider:documentProvider];
                    [documentProviders addObject:documentProvider];
                }
            }
        }
        _documentProviders = [documentProviders count] ? documentProviders : nil;
    });
}

- (void)destroyDocumentProviders {
    pspdf_dispatch_sync_reentrant(_documentProviderQueue, ^{
        [_documentProviders makeObjectsPerformSelector:@selector(setDelegate:) withObject:nil];
        _documentProviders = nil;
    });
}

- (PSPDFDocumentProvider *)documentProviderForPage:(NSUInteger)page {
    __block PSPDFDocumentProvider *pageDocumentProvider = nil;
    __block NSUInteger offset = 0;
    pspdf_dispatch_sync_reentrant(_documentProviderQueue, ^{
        // fast path
        if (page == 0 && [self.documentProviders count]) {
            pageDocumentProvider = (self.documentProviders)[0];
        }else {
            for (PSPDFDocumentProvider *documentProvider in self.documentProviders) {
                if (documentProvider.pageCount + offset <= page) {
                    offset += documentProvider.pageCount;
                }else {
                    pageDocumentProvider = documentProvider;
                    break;
                }
            }
        }
    });
    return pageDocumentProvider;
}

// analyze package and count files
- (NSUInteger)pageCount {
    // NSUIntegerMax = we need to calculate
    if (_pageCount == NSUIntegerMax) {
        pspdf_dispatch_sync_reentrant(_documentProviderQueue, ^{
            if (_pageCount == NSUIntegerMax) {
                NSUInteger pageCount = 0;
                for (PSPDFDocumentProvider *documentProvider in self.documentProviders) {
                    pageCount += documentProvider.pageCount;
                }
                _pageCount = pageCount;
            }
        });
    }
    // if document is locked, our pageCount will be 0.
    return _pageCount == NSUIntegerMax ? 0 : _pageCount;
}

// if we have multiple files, there is a page offset from logical to file-based page count
- (NSUInteger)pageOffsetForPage:(NSUInteger)page {
    NSUInteger offset = 0;
    for (PSPDFDocumentProvider *documentProvider in self.documentProviders) {
        if (documentProvider.pageCount + offset <= page) {
            offset += documentProvider.pageCount;
        }else break;
    }
    return offset;
}

- (NSUInteger)pageOffsetForDocumentProvider:(PSPDFDocumentProvider *)documentProvider {
    NSParameterAssert(documentProvider);
    if (!documentProvider) { PSPDFLogWarning(@"Document provider is nil!"); return 0; }

    __block NSUInteger offset = 0;
    BOOL found = NO;
    for (PSPDFDocumentProvider *aDocumentProvider in self.documentProviders) {
        if (documentProvider == aDocumentProvider) { found = YES; break; }
        else offset += aDocumentProvider.pageCount;
    }

    if (!found) PSPDFLogWarning(@"documentProvider not found!");
    return offset;
}

- (NSInteger)fileIndexForPage:(NSUInteger)page {
    __block NSInteger fileIndex = -1;
    pspdf_dispatch_sync_reentrant(_documentProviderQueue, ^{
        PSPDFDocumentProvider *documentProvider = [self documentProviderForPage:page];
        if (documentProvider) {
            fileIndex = [_documentProviders indexOfObject:documentProvider];
        }
    });
    return fileIndex;
}

- (NSURL *)URLForFileIndex:(NSInteger)fileIndex {
    NSArray *files = self.files;
    NSString *fileForPage = nil;
    if (fileIndex >= 0 && fileIndex < [files count]) {
        fileForPage = files[fileIndex];
    }
    if (!fileForPage) {
        if (!self.data && !self.dataProvider) {
            PSPDFLogWarning(@"Path for fileIndex %d is missing!", fileIndex);
        }
    }
    return fileForPage ? [self fileURLWithFileName:fileForPage] : nil;
}

- (NSURL *)pathForPage:(NSUInteger)page {
    NSInteger fileIndex = [self fileIndexForPage:page];
    return [self URLForFileIndex:fileIndex];
}

- (NSUInteger)compensatedPageForPage:(NSUInteger)page {
    return [self pageNumberForPage:page]-1;
}

- (NSUInteger)pageNumberForPage:(NSUInteger)page {
    NSUInteger pageOffset = [self pageOffsetForPage:page];
    return page - pageOffset + 1; // PDF starts at 1
}

- (void)clearCache {
    pspdf_dispatch_sync_reentrant(_documentProviderQueue, ^{ // locked because of _pageCount
        _textSearch.delegate = nil;
        _textSearch = nil;
        [_fileURLCache removeAllObjects];

        [self destroyDocumentProviders];
        _pageCount = NSUIntegerMax;
    });
}

// fills up rect cache
- (void)fillCache {
    [self metadata]; // parse title and metadata
    [self fillPageInfoCache];
    [self.outlineParser outline];
    //[[self documentParserForPage:0] parseDocumentWithError:NULL];
}

- (void)fillPageInfoCache {
    [self.documentProviders makeObjectsPerformSelector:@selector(fillPageInfoCache)];
}

- (NSURL *)basePath { return PSPDFAtomicAutoreleasedGet(_basePath); }
- (void)setBasePath:(NSURL *)basePath {
    if (_basePath != basePath) {
        PSPDFAtomicCopiedSetToFrom(_basePath, basePath);
        [self clearCache];
    }
}

- (NSArray *)files { return PSPDFAtomicAutoreleasedGet(_files); }
- (void)setFiles:(NSArray *)files {
    if (_files != files) {
        // ensure files are NSStrings
        __block BOOL needsPathFiltering = NO;
        files = [files filteredArrayUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(NSString *file, NSDictionary *bindings) {
            if (![file isKindOfClass:[NSString class]]) {
                PSPDFLogError(@"files must only contain NSString instances. Skipping %@", file); return NO;
            }else {
                if (!needsPathFiltering && PSPDFIsIncorrectPath(file)) needsPathFiltering = YES;
                return YES;
            }
        }]];

        // ensure all files have the correct path.
        if (needsPathFiltering) {
            NSMutableArray *mFiles = [files mutableCopy];
            for (int i=0; i<[mFiles count]; i++) {
                if (PSPDFIsIncorrectPath(mFiles[i])) mFiles[i] = PSPDFFixIncorrectPath(mFiles[i]);
            }
            files = [mFiles copy];
        }

        PSPDFAtomicCopiedSetToFrom(_files, files);
        [self clearCache];

        // if UID is not set at this point, generate.
        if (!_UID) _UID = [self generateUID];
    }
}

- (void)setTitle:(NSString *)title {
    if (_title != title) {
        PSPDFAtomicCopiedSetToFrom(_title, title);

        // if UID is not set at this point, generate.
        if (!_UID) _UID = [self generateUID];
    }
}

- (NSString *)title {
    if (!_title) return [[self documentProviderForPage:0] title];
    else return _title;
}

- (BOOL)isTitleLoaded {
    // if documentProviders aren't loaded, title most likely isn't either.
    return _title || (_documentProviders && [[self documentProviderForPage:0] isMetadataLoaded]);
}

- (NSDictionary *)metadata {
    return [[self documentProviderForPage:0] metadata];
}

- (BOOL)canEmbedAnnotations {
    return [[self documentProviderForPage:0] canEmbedAnnotations];
}

- (BOOL)allowsCopying {
    return [[self documentProviderForPage:0] allowsCopying];
}

- (void)setAllowsCopying:(BOOL)allowsCopying {
    [[self documentProviderForPage:0] setAllowsCopying:allowsCopying];
}

- (BOOL)allowsPrinting {
    return [[self documentProviderForPage:0] allowsPrinting];
}

- (BOOL)isEncrypted {
    return [[self documentProviderForPage:0] isEncrypted];
}

- (NSString *)encryptionFilter {
    return [[self documentProviderForPage:0] encryptionFilter];
}

- (PSPDFDocumentParser *)documentParserForPage:(NSUInteger)page {
    return [[self documentProviderForPage:page] documentParser];
}

- (BOOL)isLocked {
    return [[self documentProviderForPage:0] isLocked];
}

- (BOOL)allowsCopyingForPage:(NSUInteger)page {
    return [[self documentProviderForPage:page] allowsCopying];
}

- (BOOL)unlockWithPassword:(NSString *)password {
    BOOL success = [[self documentProviderForPage:0] unlockWithPassword:password];
    // only save password if unlocking was successful
    if (success && password != _password) {
        _password = password;
    }
    return success;
}

- (NSString *)password {
    return [[self documentProviderForPage:0] password] ?: _password;
}

- (void)setPassword:(NSString *)password {
    if (_password != password) _password = password;

    [self.documentProviders enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        [obj setPassword:password];
    }];
}

- (BOOL)isValid {
    return [self pageCount] > 0 && [self pageCount] != NSUIntegerMax;
}

- (BOOL)hasLoadedTextParserForPage:(NSUInteger)page {
    return [[self documentProviderForPage:page] hasLoadedTextParserForPage:[self compensatedPageForPage:page]];
}

- (PSPDFTextParser *)textParserForPage:(NSUInteger)page {
    return [[self documentProviderForPage:page] textParserForPage:[self compensatedPageForPage:page]];
}

- (PSPDFTextSearch *)textSearch {
    @synchronized(self) {
        if (!_textSearch) {
            _textSearch = [[[self classForClass:[PSPDFTextSearch class]] alloc] initWithDocument:self];
        }
    }
    return _textSearch;
}

- (PSPDFOutlineParser *)outlineParser {
    return [[self documentProviderForPage:0] outlineParser];
}

- (PSPDFAnnotationParser *)annotationParser {
    return [self annotationParserForPage:0];
}

- (PSPDFAnnotationParser *)annotationParserForPage:(NSUInteger)page {
    if (!self.isAnnotationsEnabled) return nil;
    return [[self documentProviderForPage:page] annotationParser];
}

- (void)setDefaultAnnotationUsername:(NSString *)defaultAnnotationUsername {
    @synchronized(self) {
        if (defaultAnnotationUsername != _defaultAnnotationUsername) {
            _defaultAnnotationUsername = [defaultAnnotationUsername copy];
            for (PSPDFDocumentProvider *provider in self.documentProviders) {
                provider.annotationParser.fileAnnotationProvider.defaultAnnotationUsername = defaultAnnotationUsername;
            }
        }
    }
}

- (NSString *)pageLabelForPage:(NSUInteger)page substituteWithPlainLabel:(BOOL)substite {
    NSUInteger compensatedPage = [self compensatedPageForPage:page];
    NSString *pageLabel = [[self documentProviderForPage:page].labelParser pageLabelForPage:compensatedPage];
    if (!pageLabel && substite) {
        pageLabel = [NSString stringWithFormat:@"%u", page+1];
    }
    return pageLabel;
}

- (NSUInteger)pageForPageLabel:(NSString *)pageLabel partialMatching:(BOOL)partialMatching {
    __block NSUInteger foundPage = NSNotFound;
    pspdf_dispatch_sync_reentrant(_documentProviderQueue, ^{
        for (PSPDFDocumentProvider *documentProvider in self.documentProviders) {
            foundPage = [documentProvider.labelParser pageForPageLabel:pageLabel partialMatching:partialMatching];

            // if found, convert page result and return early.
            if (foundPage != NSNotFound) {
                foundPage += [self pageOffsetForDocumentProvider:documentProvider];
                break;
            }
        }
    });
    return foundPage;
}

- (NSArray *)annotationsForPage:(NSUInteger)page type:(PSPDFAnnotationType)type {
    NSUInteger compensatedPage = [self compensatedPageForPage:page];
    PSPDFAnnotationParser *annotationParser = [self annotationParserForPage:page];
    return [annotationParser annotationsForPage:compensatedPage type:type];
}

- (void)addAnnotations:(NSArray *)annotations forPage:(NSUInteger)page {
    NSUInteger compensatedPage = [self compensatedPageForPage:page];
    PSPDFAnnotationParser *annotationParser = [self annotationParserForPage:page];
    [annotationParser addAnnotations:annotations forPage:compensatedPage];
}

- (PSPDFBookmarkParser *)bookmarkParser {
    PSPDFBookmarkParser *bookmarkParser;
    @synchronized(self) {
        if (!_bookmarkParser) {
            _bookmarkParser = [[[self classForClass:[PSPDFBookmarkParser class]] alloc] initWithDocument:self];
        }
        bookmarkParser = _bookmarkParser;
    }
    return bookmarkParser;
}

// if UID is zero, try to generate one.
- (NSString *)UID {
    if (!_UID) _UID = [self generateUID];
    return _UID;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Private

// returns a file URL with added basePath, if there is one set
- (NSURL *)fileURLWithFileName:(NSString *)fileName {
    NSURL *path = _fileURLCache[fileName];

    if (!path) {
        if (self.basePath) {
            path = [self.basePath URLByAppendingPathComponent:fileName];
        }else {
            path = [NSURL fileURLWithPath:fileName];
        }
        _fileURLCache[fileName] = path;
    }

    return path;
}

// helper that generates a unique id depending on the document files.
#define kPSPDFUIDDataSizeLimit 5000
- (NSString *)generateUID {
    NSArray *files = self.files;
    NSString *UID = nil;
    // don't use self.title, since that lazy evaluates.
    NSMutableString *stringHash = [NSMutableString stringWithString:_title ?: @""];
    if ([files count]) {
        NSURL *basePath = self.basePath;
        if (basePath) [stringHash appendString:[basePath absoluteString]];
        for (NSString *file in files) [stringHash appendString:file];
    }

    NSData *md5HashData = nil;
    if (self.data) {
        md5HashData = [self.data subdataWithRange:NSMakeRange(0, MIN(kPSPDFUIDDataSizeLimit, [self.data length]))];
    }else if (self.dataProvider) {
        @autoreleasepool {
            NSData *tempData = CFBridgingRelease(CGDataProviderCopyData(self.dataProvider));
            if (tempData) {
                md5HashData = [tempData subdataWithRange:NSMakeRange(0, MIN(kPSPDFUIDDataSizeLimit, [tempData length]))];
            }else {
                PSPDFLogWarning(@"dataProvider to big to create a UID. Set one manually!");
            }
        }
    }
    if ((!self.data && !_dataProvider) && [stringHash length]) {
        md5HashData = [stringHash dataUsingEncoding:NSUTF8StringEncoding];
    }

    // generate MD5 hash
    if (md5HashData) {
        unsigned char md[CC_MD5_DIGEST_LENGTH];
        CC_MD5([md5HashData bytes], [md5HashData length], md);
        NSMutableString *digest = [NSMutableString string];

        // make hashes easier find-able.
        if ([_files count]) {
            NSString *strippedTitle = [[[PSPDFStripPDFFileType([_files ps_firstObject]) componentsSeparatedByCharactersInSet:[[NSCharacterSet alphanumericCharacterSet] invertedSet]] componentsJoinedByString:@""] lowercaseString];
            [digest appendFormat:@"%@_", strippedTitle];
        }

        for (NSUInteger i = 0; i < CC_MD5_DIGEST_LENGTH; i++) {
            [digest appendFormat:@"%02x", md[i]];
        }
        UID = digest;
    }
    return UID;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Object Finder

// filter
NSString *const kPSPDFObjectsGlyphs = @"kPSPDFObjectsGlyphs";
NSString *const kPSPDFObjectsText = @"kPSPDFObjectsText";
NSString *const kPSPDFObjectsFullWords = @"kPSPDFObjectsFullWords";
NSString *const kPSPDFObjectsTextBlocks = @"kPSPDFObjectsTextBlocks";
NSString *const kPSPDFObjectsTextBlocksIgnoreLarge = @"kPSPDFObjectsTextBlocksIgnoreLarge";
NSString *const kPSPDFObjectsAnnotationTypes = @"kPSPDFObjectsAnnotationTypes";
NSString *const kPSPDFObjectsAnnotationPageBounds = @"kPSPDFObjectsAnnotationPageBounds";
NSString *const kPSPDFObjectsImages = @"kPSPDFObjectsImages";
NSString *const kPSPDFObjectsSmartSort = @"kPSPDFObjectsSmartSort";
NSString *const kPSPDFObjectsFindFirstOnly = @"kPSPDFObjectsFindFirstOnly";
// output
NSString *const kPSPDFGlyphs = @"kPSPDFGlyphs";
NSString *const kPSPDFText = @"kPSPDFText";
NSString *const kPSPDFWords = @"kPSPDFWords";
NSString *const kPSPDFTextBlocks = @"kPSPDFTextBlocks";
NSString *const kPSPDFAnnotations = @"kPSPDFAnnotations";
NSString *const kPSPDFImages = @"kPSPDFImages";

// Calculate absolute distance from the center.
static inline CGFloat PSPDFAbsoluteDistanceFromCenter(CGRect rect, CGPoint point) {
    return fabsf(CGRectGetMidX(rect) - point.x) + fabsf(CGRectGetMidY(rect) - point.y);
}

- (NSDictionary *)objectsAtPDFPoint:(CGPoint)pdfPoint page:(NSUInteger)page options:(NSDictionary *)options {
    return [self objectsAtPDFRect:CGRectMake(pdfPoint.x, pdfPoint.y, 1, 1) page:page options:options];
}

- (NSDictionary *)objectsAtPDFRect:(CGRect)pdfRect page:(NSUInteger)page options:(NSDictionary *)options {
    NSMutableDictionary *objects = [NSMutableDictionary dictionaryWithCapacity:4];
    NSMutableArray *glyphs = [NSMutableArray array];
    NSMutableArray *words = [NSMutableArray array];
    NSMutableArray *textBlocks = [NSMutableArray array];
    NSMutableArray *annotations = [NSMutableArray array];
    NSMutableArray *images = [NSMutableArray array];
    PSPDFPageInfo *pageInfo = [self pageInfoForPage:page];
    PSPDFTextParser *textParser = nil;
    CGPoint centerPoint = CGPointMake(CGRectGetMidX(pdfRect), CGRectGetMidY(pdfRect));
    NSString *extractedText = nil;

    BOOL extractText = [options[kPSPDFObjectsText] boolValue];
    BOOL extractGlyphs = [options[kPSPDFObjectsGlyphs] boolValue];
    BOOL extractWords = [options[kPSPDFObjectsFullWords] boolValue];
    BOOL extractTextBlocks = [options[kPSPDFObjectsTextBlocks] boolValue];
    BOOL extractImages = [options[kPSPDFObjectsImages] boolValue];
    BOOL smartSort = [options[kPSPDFObjectsSmartSort] boolValue];
    BOOL findFirstOnly = [options[kPSPDFObjectsFindFirstOnly] boolValue];
    PSPDFAnnotationType annotationTypes = [options[kPSPDFObjectsAnnotationTypes] unsignedIntegerValue];

    // compatiblilty
    if ([options count] == 0) {
        PSPDFLogVerbose(@"No options set - will return glyphs.");
        extractGlyphs = YES;
    }

    if (extractWords) {
        if (!textParser) textParser = [self textParserForPage:page];
        for (PSPDFWord *word in textParser.words) {
            if (CGRectIntersectsRect(pdfRect, CGRectApplyAffineTransform(word.frame, pageInfo.pageRotationTransform))) {
                [words addObject:word];
                [glyphs addObjectsFromArray:word.glyphs];
                if (findFirstOnly) break;
            }
        }

        // priorize smaller words.
        if (!findFirstOnly && smartSort) {
            [words sortUsingComparator:^NSComparisonResult(id obj1, id obj2) {
                return [obj1 frame].size.height > [obj2 frame].size.height;
            }];
        }
    }

    if (extractText || extractGlyphs) {
        if (!textParser) textParser = [self textParserForPage:page];
        for (PSPDFGlyph *glyph in textParser.glyphs) {
            if (CGRectIntersectsRect(pdfRect, CGRectApplyAffineTransform(glyph.frame, pageInfo.pageRotationTransform))) {
                [glyphs addObject:glyph];
                if (findFirstOnly) break;
            }
        }

        if (extractText) extractedText = [textParser textWithGlyphs:glyphs];
    }

    if (extractTextBlocks) {
        if (!textParser) textParser = [self textParserForPage:page];
        BOOL ignoreLargeBlocks = [options[kPSPDFObjectsTextBlocksIgnoreLarge] boolValue];
        for (PSPDFTextBlock *block in textParser.textBlocks) {

            // skip too large blocks. (too small blocks are limited via max zoom value)
            if (ignoreLargeBlocks) {
                if (block.frame.size.width > pageInfo.rotatedPageRect.size.width * 0.9 ||
                    block.frame.size.height > pageInfo.rotatedPageRect.size.height * 0.9) {
                    continue;
                }
            }
            if (CGRectContainsRect(block.frame, pdfRect)) {
                [textBlocks addObject:block];
                if (findFirstOnly) break;
            }
        }

        // sort them after center
        if (!findFirstOnly && smartSort) {
            [textBlocks sortUsingComparator:^NSComparisonResult(PSPDFTextBlock *block, PSPDFTextBlock *otherBlock) {
                CGFloat blockDistance = PSPDFAbsoluteDistanceFromCenter(block.frame, centerPoint);
                CGFloat otherBlockDistance = PSPDFAbsoluteDistanceFromCenter(otherBlock.frame, centerPoint);
                return blockDistance > otherBlockDistance;
            }];
        }
    }

    // find annotations?
    if (annotationTypes > PSPDFAnnotationTypeNone) {
        NSArray *pageAnnots = [self annotationsForPage:page type:annotationTypes];
        CGRect pageRect = [options[kPSPDFObjectsAnnotationPageBounds] CGRectValue];
        for (PSPDFAnnotation *annotation in pageAnnots) {
            CGRect annotationFrame = annotation.boundingBox;
            if (annotation.type == PSPDFAnnotationTypeNote && !CGRectIsEmpty(pageRect)) {
                annotationFrame = [(PSPDFNoteAnnotation *)annotation boundingBoxForPageViewBounds:pageRect];
            }
            if (CGRectContainsRect(annotationFrame, pdfRect)) {
                [annotations addObject:annotation];
                if (findFirstOnly) break;
            }
        }

        // sort using the center point of the touch, makes smarter decisions what annotation is most likely tapped.
        if (!findFirstOnly && smartSort) {
            [annotations sortUsingComparator:^NSComparisonResult(PSPDFAnnotation *annotation, PSPDFAnnotation *otherAnnotation) {
                CGFloat annotationDistance = PSPDFAbsoluteDistanceFromCenter(annotation.boundingBox, centerPoint);
                CGFloat otherAnnotationDistance = PSPDFAbsoluteDistanceFromCenter(otherAnnotation.boundingBox, centerPoint);
                return annotationDistance > otherAnnotationDistance;
            }];
        }
    }

    // last image is the topmost image
    if (extractImages) {
        if (!textParser) textParser = [self textParserForPage:page];
        for (PSPDFImageInfo *imageInfo in [textParser.images reverseObjectEnumerator]) {
            if (CGRectContainsRect(imageInfo.boundingBox, pdfRect)) {
                [images addObject:imageInfo];
                if (findFirstOnly) break;
            }
        }

        if (!findFirstOnly && smartSort) {
            [images sortUsingComparator:^NSComparisonResult(PSPDFImageInfo *imageInfo, PSPDFImageInfo *otherImageInfo) {
                CGFloat annotationDistance = PSPDFAbsoluteDistanceFromCenter(imageInfo.boundingBox, centerPoint);
                CGFloat otherAnnotationDistance = PSPDFAbsoluteDistanceFromCenter(otherImageInfo.boundingBox, centerPoint);
                return annotationDistance > otherAnnotationDistance;
            }];
        }
    }

    if (extractGlyphs) objects[kPSPDFGlyphs] = glyphs;
    if (extractText && extractedText) objects[kPSPDFText] = extractedText;
    if (extractWords) objects[kPSPDFWords] = words;
    if (extractTextBlocks) objects[kPSPDFTextBlocks] = textBlocks;
    if (annotationTypes) objects[kPSPDFAnnotations] = annotations;
    if (extractImages) objects[kPSPDFImages] = images;
    return objects;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Page Rendering

- (UIImage *)renderImageForPage:(NSUInteger)page withSize:(CGSize)fullSize clippedToRect:(CGRect)clipRect withAnnotations:(NSArray *)annotations options:(NSDictionary *)options {

    // silently ignore if size is invalid
    if (PSPDFSizeIsEmpty(fullSize)) {
        PSPDFLogVerbose(@"Silently ignoring nil size: %@", NSStringFromCGSize(fullSize)); return nil;
    }

    // automatic mode
    if (CGRectEqualToRect(clipRect, CGRectZero)) {
        clipRect = CGRectMake(0, 0, fullSize.width, fullSize.height);
    }

    // test against crazy sizes.
    if (CGRectGetWidth(clipRect) > 3000 || CGRectGetHeight(clipRect) > 3000) {
        PSPDFLogWarning(@"clipRect %@ seems larger than what the system can handle. Crash might follow.", NSStringFromCGRect(clipRect));
    }

    BOOL ignoreDisplaySettings = [options[kPSPDFIgnoreDisplaySettings] boolValue];

    // opaque: YES would be faster, but also generates black borders for content of we render in a wrong rect.
    UIGraphicsBeginImageContextWithOptions(clipRect.size, NO, ignoreDisplaySettings ? 1.0 : 0.0);

    CGContextRef context = UIGraphicsGetCurrentContext();
    if (!context) {
        PSPDFLogError(@"graphics context is nil - low memory?");
        UIGraphicsEndImageContext();
        return nil;
    }

    [self renderPage:page inContext:context withSize:fullSize clippedToRect:clipRect withAnnotations:annotations options:options];

    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();

    // Useful for debugging
    //static int i  = 0;
    //[UIImagePNGRepresentation(image) writeToFile:[NSString stringWithFormat:@"/Users/steipete/test_%d.png", i++] atomically:YES];

    return image;
}

/// Draw a page into a specified context
- (void)renderPage:(NSUInteger)page inContext:(CGContextRef)context withSize:(CGSize)size clippedToRect:(CGRect)clipRect withAnnotations:(NSArray *)annotations options:(NSDictionary *)options {

    // merge options with the one in PSPDFDocument
    if ([self.renderOptions count]) {
        NSMutableDictionary *mutableOptions = options ? [options mutableCopy] : [NSMutableDictionary dictionary];
        [mutableOptions addEntriesFromDictionary:self.renderOptions];
        options = mutableOptions;
    }

    // get page provider and info data
    PSPDFDocumentProvider *documentProvider = [self documentProviderForPage:page];
    NSUInteger pageNumber = [self pageNumberForPage:page];
    CGPDFPageRef pageRef = [documentProvider requestPageRefForPageNumber:pageNumber];
    if (!pageRef) { PSPDFLogWarning(@"pageRef is nil for page %d. Can't render page.", page); return; }

    PSPDFPageInfo *pageInfo = [self pageInfoForPage:page];
	CGRect pageRect = pageInfo.rotatedPageRect;
	float scaleFactor = size.width / pageRect.size.width;

    CGContextSaveGState(context);
	CGContextTranslateCTM(context, -clipRect.origin.x, -clipRect.origin.y);

	CGRect scaledPageRect = CGRectMake(0, 0, scaleFactor * pageRect.size.width, scaleFactor * pageRect.size.height);
    CGContextClipToRect(context, scaledPageRect);

    CGContextSaveGState(context);
    [PSPDFPageRenderer renderPageRef:pageRef inContext:context inRectangle:(CGRect){.size =size} pageInfo:pageInfo withAnnotations:annotations options:options];
    CGContextRestoreGState(context);

    // call delegate only if set in option array
    __weak PSPDFViewController *pdfController = options[@"pdfController"];
    if ([pdfController.delegate respondsToSelector:@selector(pdfViewController:didRenderPage:inContext:withSize:clippedToRect:withAnnotations:options:)]) {
        [pdfController.delegate pdfViewController:pdfController didRenderPage:page inContext:context withSize:size clippedToRect:(CGRect){.size =size} withAnnotations:annotations options:options];
    }

    CGContextRestoreGState(context);
    [documentProvider releasePageRef:pageRef];
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Subclassing Default Implementations

// override if needed
- (NSURL *)cachedImageURLForPage:(NSUInteger)page andSize:(PSPDFSize)size {
    return nil;
}

// override if needed
- (void)drawOverlayRect:(CGRect)rect inContext:(CGContextRef)context forPage:(NSUInteger)page zoomScale:(CGFloat)zoomScale size:(PSPDFSize)size {
}

// override if needed
- (UIColor *)backgroundColorForPage:(NSUInteger)page {
    return self.renderOptions[kPSPDFBackgroundFillColor] ?: _backgroundColor;
}

// override if needed
- (NSString *)pageContentForPage:(NSUInteger)page {
    return nil;
}

// override if needed
- (PSPDFDocumentProvider *)didCreateDocumentProvider:(PSPDFDocumentProvider *)documentProvider {
    documentProvider.delegate = self;
    if (_password) documentProvider.password = _password;
    if (_defaultAnnotationUsername) documentProvider.annotationParser.fileAnnotationProvider.defaultAnnotationUsername = self.defaultAnnotationUsername;

    // call the conveniece block.
    void (^didCreateDocumentProviderBlock)(PSPDFDocumentProvider *aDocumentProvider) = self.didCreateDocumentProviderBlock;
    if (didCreateDocumentProviderBlock) didCreateDocumentProviderBlock(documentProvider);

    return documentProvider;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Custom Class Override Helper

// ensure overridden class are correct subclasses.
- (void)setOverrideClassNames:(NSDictionary *)overrideClassNames {
    if (overrideClassNames != _overrideClassNames) {
        PSPDFCheckOverrideClassNames(overrideClassNames); // throws an exception if invalid.
        _overrideClassNames = overrideClassNames;
    }
}

// Looks up an entry in overrideClassNames for custom Class subclasses
- (Class)classForClass:(Class)originalClass {
    return PSPDFClassForClassInDictionary(originalClass, _overrideClassNames);
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSCopying

- (id)copyWithZone:(NSZone *)zone {
    PSPDFDocument *document = nil;
    if (self.dataArray) document = [[[self class] allocWithZone:zone] initWithDataArray:self.dataArray];
    else document = [[[self class] allocWithZone:zone] initWithBaseURL:self.basePath files:_files];
    document.title = _title;
    document.UID = self.UID;
    document.aspectRatioEqual = self.isAspectRatioEqual;
    document.annotationsEnabled = self.isAnnotationsEnabled;
    document.displayingPdfController = self.displayingPdfController;
    document.password = self.password;
    document.overrideClassNames = self.overrideClassNames;
    return document;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSCoding

// password is not serialized!
- (id)initWithCoder:(NSCoder *)coder {
    if ((self = [self init])) {
        _files = [coder decodeObjectForKey:NSStringFromSelector(@selector(files))];
        _basePath = [coder decodeObjectForKey:NSStringFromSelector(@selector(basePath))];
        _dataArray = [coder decodeObjectForKey:NSStringFromSelector(@selector(dataArray))];
        _UID = [coder decodeObjectForKey:NSStringFromSelector(@selector(UID))];
        _title = [coder decodeObjectForKey:NSStringFromSelector(@selector(title))];
        _aspectRatioEqual = [coder decodeBoolForKey:NSStringFromSelector(@selector(isAspectRatioEqual))];
        _annotationsEnabled = [coder decodeBoolForKey:NSStringFromSelector(@selector(isAnnotationsEnabled))];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)coder {
    [coder encodeObject:self.files forKey:NSStringFromSelector(@selector(files))];
    [coder encodeObject:_basePath forKey:NSStringFromSelector(@selector(basePath))];
    [coder encodeObject:_dataArray forKey:NSStringFromSelector(@selector(dataArray))];
    [coder encodeObject:_UID forKey:NSStringFromSelector(@selector(UID))];
    [coder encodeObject:_title forKey:NSStringFromSelector(@selector(title))];
    [coder encodeBool:_aspectRatioEqual forKey:NSStringFromSelector(@selector(isAspectRatioEqual))];
    [coder encodeBool:_annotationsEnabled forKey:NSStringFromSelector(@selector(isAnnotationsEnabled))];
}

@end

NSString *const kPSPDFMetadataKeyTitle = @"Title";
NSString *const kPSPDFMetadataKeyAuthor = @"Author";
NSString *const kPSPDFMetadataKeySubject = @"Subject";
NSString *const kPSPDFMetadataKeyKeywords = @"Keywords";
NSString *const kPSPDFMetadataKeyCreator = @"Creator";
NSString *const kPSPDFMetadataKeyProducer = @"Producer";
NSString *const kPSPDFMetadataKeyCreationDate = @"CreationDate";
NSString *const kPSPDFMetadataKeyModDate = @"ModDate";
NSString *const kPSPDFMetadataKeyTrapped = @"Trapped";
