//
//  PSPDFTransitionViewController.m
//  PSPDFKit
//
//  Copyright (c) 2012 Peter Steinberger. All rights reserved.
//

#import "PSPDFTransitionHelper.h"
#import "PSPDFPageViewController.h"
#import "PSPDFSinglePageViewController.h"
#import "PSPDFViewController.h"
#import "PSPDFViewController+Internal.h"
#import "PSPDFDocument.h"

@interface PSPDFTransitionHelper () {
    NSMutableSet *_recycledPages;
    NSMutableSet *_singlePages; // non-retaining set to keep track of single pages.
}
@property (nonatomic, weak) UIViewController<PSPDFTransitionHelperDelegate> *delegate;
@end

@implementation PSPDFTransitionHelper

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (id)initWithDelegate:(UIViewController<PSPDFTransitionHelperDelegate> *)delegate {
    if ((self = [super init])) {
        _delegate = delegate;
        _recycledPages = [NSMutableSet new];
        _singlePages = CFBridgingRelease(CFSetCreateMutable(nil, 0, nil));
        _page = delegate.pdfController.page;
    }
    return self;
}

- (void)dealloc {
    [_singlePages makeObjectsPerformSelector:@selector(setPdfController:) withObject:nil];
    [_singlePages makeObjectsPerformSelector:@selector(setDelegate:) withObject:nil];
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public

- (void)setPage:(NSUInteger)page {
    [self setPage:page animated:NO];
}

// returns the singlePageController, looks if it may already be created
- (PSPDFSinglePageViewController *)singlePageControllerForPage:(NSUInteger)page {
    PSPDFSinglePageViewController *singlePage = nil;

    NSArray *viewControllers = [self.delegate viewControllers];
    for (PSPDFSinglePageViewController *currentSinglePage in viewControllers) {
        if (currentSinglePage.page == page) {
            singlePage = currentSinglePage; break;
        }
    }

    if (!singlePage) {
        // try to dequeue
        singlePage = [_recycledPages anyObject];
        if (singlePage) {
            [_recycledPages removeObject:singlePage];
        }

        if (!singlePage) {
            Class singlePageClass = [[self.delegate pdfController] classForClass:[PSPDFSinglePageViewController class]] ?: [PSPDFSinglePageViewController class];
            
            singlePage = [[singlePageClass alloc] initWithPDFController:self.delegate.pdfController page:page];
        }else {
            singlePage.pdfController = self.delegate.pdfController;
            singlePage.page = page; // reloads layout
        }
        singlePage.delegate = self;
        [_singlePages addObject:singlePage];
    }
    // ensure page is visible
    singlePage.view.alpha = 1.f;
    singlePage.view.hidden = NO;
    return singlePage;
}

- (NSUInteger)fixPageNumberForDoublePageMode:(NSUInteger)page forceDoublePageMode:(BOOL)forceDualPageMode {
    // ensure that we've not set the wrong page for double page mode
    NSUInteger correctedPage = page;
    if (([self.delegate.pdfController isDoublePageMode] || forceDualPageMode) && [self.delegate.pdfController isRightPageInDoublePageMode:correctedPage]) {
        correctedPage--;
    }
    return correctedPage;
}

- (PSPDFSinglePageViewController *)viewControllerBeforeViewController:(UIViewController *)viewController {
    if (!viewController) return nil;

    PSPDFSinglePageViewController *previousPageController = (PSPDFSinglePageViewController *)viewController;
    PSPDFLogVerbose(@"viewControllerBeforeViewController:%d", previousPageController.page);

    // allow special case for a document open (where the leftPage is empty)
    BOOL allowEmptyFirstPage = self.delegate.pdfController.isDoublePageMode && previousPageController.page == 0 && !self.delegate.pdfController.doublePageModeOnFirstPage;
    if ((previousPageController.page == 0 || previousPageController.page >= self.delegate.pdfController.document.pageCount) && !allowEmptyFirstPage) {
        return nil;
    }
    NSUInteger newPage = previousPageController.page-1;

    // block if scrolling is not enabled
    if (!self.delegate.pdfController.scrollingEnabled || ![self.delegate.pdfController delegateShouldScrollToPage:newPage]) {
        return nil;
    }

    [self.delegate.pdfController hideControls];

    PSPDFSinglePageViewController *singlePageController = [self singlePageControllerForPage:newPage];
    return singlePageController;
}

- (PSPDFSinglePageViewController *)viewControllerAfterViewController:(UIViewController *)viewController {
    if (!viewController) return nil;

    PSPDFSinglePageViewController *nextPageController = (PSPDFSinglePageViewController *)viewController;
    PSPDFLogVerbose(@"viewControllerAfterViewController:%d", nextPageController.page);

    BOOL allowEmptyLastPage = self.delegate.pdfController.isDoublePageMode && nextPageController.page == self.delegate.pdfController.document.pageCount-1 && ![self.delegate.pdfController isRightPageInDoublePageMode:nextPageController.page];
    BOOL allowNegativeIncrement = nextPageController.page == NSUIntegerMax;
    if (nextPageController.page >= self.delegate.pdfController.document.pageCount-1 && !allowEmptyLastPage && !allowNegativeIncrement) {
        return nil;
    }
    NSUInteger newPage = nextPageController.page+1;

    // block if scrolling is not enabled
    if (!self.delegate.pdfController.scrollingEnabled || ![self.delegate.pdfController delegateShouldScrollToPage:newPage]) {
        return nil;
    }

    [self.delegate.pdfController hideControlsAndPageElements];

    PSPDFSinglePageViewController *singlePageController = [self singlePageControllerForPage:newPage];
    return singlePageController;
}

- (void)setPageInternal:(NSUInteger)page {
    _page = page;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - PSPDFTransitionProtocol

- (NSArray *)visiblePageNumbers {
    NSMutableArray *visiblePageNumbers = [NSMutableArray arrayWithCapacity:2];
    NSInteger currentPage = self.page;
    if (currentPage >= 0) { // might be an invalid page (e.g. first page right only)
        [visiblePageNumbers addObject:@(currentPage)];
    }
    BOOL isDoublePageMode = [self.delegate.pdfController isDoublePageMode];
    BOOL isNotLastPage = currentPage+1 < (NSInteger)([self.delegate.pdfController.document pageCount]);
    if (isDoublePageMode && isNotLastPage) {
        [visiblePageNumbers addObject:@(currentPage+1)];
    }
    return visiblePageNumbers;
}

- (PSPDFPageView *)pageViewForPage:(NSUInteger)page {
    PSPDFPageView *pageView = nil;
    for (PSPDFSinglePageViewController *singlePage in self.delegate.viewControllers) {
        if (singlePage.page == page) {
            pageView = singlePage.pageView;
        }
    }
    return pageView;
}

- (void)setPage:(NSUInteger)page animated:(BOOL)animated {
    // ensure that we've not set the wrong page for double page mode
    NSUInteger correctedPage = [self fixPageNumberForDoublePageMode:page forceDoublePageMode:NO];
    if (_page != correctedPage) {
        BOOL forwardAnimation = (NSInteger)correctedPage > (NSInteger)_page && correctedPage != NSUIntegerMax;
        _page = correctedPage;

        [self.delegate transitionHelper:self changedToPage:_page doublePageMode:[self.delegate.pdfController isDoublePageMode] forwardTransition:forwardAnimation animated:animated];
    }
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - PSPDFSinglePageViewControllerDelegate

- (void)pspdfSinglePageViewControllerReadyForReuse:(PSPDFSinglePageViewController *)singlePageViewController {
    [singlePageViewController prepareForReuse];
    [_recycledPages addObject:singlePageViewController];
}

- (void)pspdfSinglePageViewControllerWillDealloc:(PSPDFSinglePageViewController *)singlePageViewController {
    [_singlePages removeObject:singlePageViewController];
}

@end
