//
//  PSPDFPageViewController.m
//  PSPDFKit
//
//  Copyright (c) 2011-2012 Peter Steinberger. All rights reserved.
//

#import "PSPDFPageViewController.h"
#import "PSPDFViewController.h"
#import "PSPDFDocument.h"
#import "PSPDFSinglePageViewController.h"
#import "PSPDFPageView.h"
#import "PSPDFAnnotationView.h"
#import "PSPDFContentScrollView.h"
#import "PSPDFViewController+Internal.h"
#import "PSPDFVideoAnnotationView.h"
#import "PSPDFConverter.h"
#import "PSPDFTransitionHelper.h"
#import "PSPDFKitGlobal+Internal.h"
#import <objc/runtime.h>
#import "PSPDFPatches.h"

@interface PSPDFPageViewController () {
    BOOL _isAnimatingProgrammaticPageChange;
    CGRect _pageRect;
}
@property (nonatomic, strong) PSPDFTransitionHelper *transitionHelper;
@end

static char kPSPDFKVOToken;

@implementation PSPDFPageViewController

@synthesize pdfController = _pdfController;
@synthesize scrollView = _scrollView;

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

// allows RTL page curling support
- (UIPageViewControllerSpineLocation)splineLocationForPDFController:(PSPDFViewController *)pdfController {
    return pdfController.isPageCurlDirectionLeftToRight ? UIPageViewControllerSpineLocationMax : UIPageViewControllerSpineLocationMin;
}

- (id)initWithPDFController:(PSPDFViewController *)pdfController {
    BOOL isDoublePaged = [pdfController isDoublePageMode] && [pdfController.document pageCount] > 1;
    UIPageViewControllerSpineLocation splineLocation = isDoublePaged ? UIPageViewControllerSpineLocationMid : [self splineLocationForPDFController:pdfController];
    NSDictionary *optionDict = @{UIPageViewControllerOptionSpineLocationKey: @(splineLocation)};

    // TODO: Control pretty much breaks when using UIPageViewControllerNavigationOrientationVertical - pages are not centered anymore.
    UIPageViewControllerNavigationOrientation orientation = UIPageViewControllerNavigationOrientationHorizontal;
    //pdfController.scrollDirection == PSPDFScrollDirectionHorizontal ? UIPageViewControllerNavigationOrientationHorizontal : UIPageViewControllerNavigationOrientationVertical;

    if (self = [super initWithTransitionStyle:UIPageViewControllerTransitionStylePageCurl navigationOrientation:orientation options:optionDict]) {
        _transitionHelper = [[PSPDFTransitionHelper alloc] initWithDelegate:self];
        _pdfController = pdfController;
        [self addObserver:self forKeyPath:PSPDF_KEYPATH_SELF(pdfController.clipToPageBoundaries) options:NSKeyValueObservingOptionInitial context:&kPSPDFKVOToken];
        _pageRect = CGRectZero;
        self.delegate = self;
        self.dataSource = self;
        [self setupPageViewControllersDualPaged:isDoublePaged splineLocation:splineLocation];
    }
    
    // register for pageCurl events, to send events to annotations
    for (UIGestureRecognizer *gestureRecognizer in self.gestureRecognizers) {
        [gestureRecognizer addTarget:self action:@selector(handleGesture:)];

        // modify gesture recognizers to allow zooming (double tap) to not interfere with UIPageViewController's default prev/next tap gesture.
        if ([gestureRecognizer isKindOfClass:[UITapGestureRecognizer class]]) {
            UITapGestureRecognizer *tapGesture = (UITapGestureRecognizer *)gestureRecognizer;
            if (tapGesture.numberOfTapsRequired == 1) {
                tapGesture.enabled = NO;
            }
        }
    }

    return self;
}

- (void)dealloc {
    [self removeObserver:self forKeyPath:PSPDF_KEYPATH_SELF(pdfController.clipToPageBoundaries) context:&kPSPDFKVOToken];
    self.delegate = nil;
    self.dataSource = nil;
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    if (context == &kPSPDFKVOToken) {
        if ([keyPath isEqualToString:PSPDF_KEYPATH_SELF(pdfController.clipToPageBoundaries)]) {
            self.clipToPageBoundaries = self.pdfController.clipToPageBoundaries;
        }
    }else {
        [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
    }
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UIViewController

// later becomes - (BOOL)pspdf_customPointInside:(CGPoint)point withEvent:(UIEvent *)event on _UIPageViewControllerContentView.
static BOOL pspdf_customPointInside(id this, SEL this_cmd, CGPoint point, UIEvent *event) {
    UIView *superview = [this superview];
    CGPoint tranlatedPoint = [this convertPoint:point toView:superview];
    BOOL isPointInSuperView = [superview pointInside:tranlatedPoint withEvent:event];
    return isPointInSuperView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // hack into _UIPageViewControllerContentView to allow gestures to fire even if not in the view. All w/o private API!
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        Class pageCurlViewClass = [self.view class]; // _UIPageViewControllerContentView
        if (pageCurlViewClass) {
            PSPDFReplaceMethod(pageCurlViewClass, @selector(pointInside:withEvent:), NSSelectorFromString(@"pspdf_customPointInside:withEvent:"), (IMP)pspdf_customPointInside);
        }
    });
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    // initially force layouting to set correct size of the view.
    // this fixes offset errors that result in view bound changes during the appearance cycle (such as a s status bar that suddenly becomes transparent)
    [self.scrollView ensureContentIsCentered];
}

// iPhone orientation is limited by PSPDFViewController.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation {
    return YES;
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {

    // zoom out (or stuff breaks)
    self.scrollView.zoomScale = 1;
    
    // restore original frame before we animate.
    self.view.frame = self.view.superview.frame;
    
    // remove any MPMoviePlayerControllers, they mess with the rotation animation and get recreated anyway
    for(PSPDFSinglePageViewController *singlePage in self.viewControllers) {
        for (UIView<PSPDFAnnotationView> *annotationView in singlePage.pageView.visibleAnnotationViews) {
            // send the hide page call
            if ([annotationView respondsToSelector:@selector(willHidePage:)]) {
                [annotationView willHidePage:self.transitionHelper.page];
            }
        }
    }
    
    // call this at the end, UIPageViewController makes a screenshot of the page inside this function.
    [super willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
    [super didRotateFromInterfaceOrientation:fromInterfaceOrientation];
    [self updateViewSize];
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public

- (void)setClipToPageBoundaries:(BOOL)clipToPageBoundaries {
    _clipToPageBoundaries = clipToPageBoundaries;
    [self updateViewSize];
}

- (void)updateViewSizeIfNeeded {
    // don't update if the zoomScale is not 1, or if rotation is active.
    // use 2 pixels tolerance to fight against pixel rounding errors.
    if (!_isAnimatingProgrammaticPageChange && self.scrollView.zoomScale == 1.f && !self.pdfController.isRotationActive && (fabs(_pageRect.size.width - self.view.bounds.size.width) > 2 || fabs(_pageRect.size.height - self.view.bounds.size.height) > 2)) {
        _pageRect = self.view.bounds;
        [self updateViewSize];
    }
}

// if we detect a frame change, compensate and re-call updateViewSize
- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self updateViewSizeIfNeeded];
    });
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Private

// helper to correctly pre-setup view controllers
- (void)setupViewControllersDoubleSided:(BOOL)doubleSided animated:(BOOL)animated direction:(UIPageViewControllerNavigationDirection)direction splineLocation:(UIPageViewControllerSpineLocation)splineLocation {
    PSPDFLogVerbose(@"setupViewControllersDoubleSided:%d animated:%d direction:%d", doubleSided, animated, direction);
    NSMutableArray *viewControllers = [NSMutableArray arrayWithObject:[self.transitionHelper singlePageControllerForPage:self.transitionHelper.page]];
    if (doubleSided) {
        [viewControllers addObject:[self.transitionHelper singlePageControllerForPage:self.transitionHelper.page+1]];
    }

    // perform sanity check if something changed at all
    BOOL changed = ![viewControllers isEqualToArray:self.viewControllers];
    if (changed) {
        _isAnimatingProgrammaticPageChange = YES;
        __weak PSPDFPageViewController *weakSelf = self;

        // Doesn't always call completion handler (track with ivar) [bug still in iOS6B1]
        [self setViewControllers:viewControllers direction:direction animated:animated completion:^(BOOL finished) {
            PSPDFPageViewController *strongSelf = weakSelf;

            // updateViewSizeIfNeeded interferes with the pageCurl animation;
            // don't update size if we're not finished animating.
            strongSelf->_isAnimatingProgrammaticPageChange = NO;

            if (finished) {
                // call the delegate if finished (only if call was animated)
                if (animated) {
                    [strongSelf.pdfController delegateDidEndPageScrollingAnimation:strongSelf.scrollView];
                }

                // don't forget to update the view size
                dispatch_async(dispatch_get_main_queue(), ^{
                    [strongSelf updateViewSizeIfNeeded];
                });
            }
        }];
    }
    // ensure that this is YES if spline location is set to mid.
    if (splineLocation == UIPageViewControllerSpineLocationMid) {
        doubleSided = YES;
    }
    self.doubleSided = doubleSided;
}

// adapt the frame so that the page doesn't "bleed out".
- (void)updateViewSize {
    // cancel if we're zoomed in
    if (![self isViewLoaded] || self.scrollView.zoomScale != 1.f) return;

    // size starts with zero, but we loop through multiple controllers
    CGSize size = CGSizeZero;

    if (_clipToPageBoundaries) {
        BOOL hasTwoPages = [self.viewControllers count] > 1;

        for (PSPDFSinglePageViewController *pageController in self.viewControllers) {
            PSPDFSinglePageViewController *currentPageController = pageController;

            // if we're at first/last page and in two page mode, just copy the size of the other page
            if (currentPageController.page == NSUIntegerMax || currentPageController.page >= [[self.pdfController document] pageCount]) {
                if ([self.viewControllers indexOfObject:pageController] == 0) {
                    currentPageController = (PSPDFSinglePageViewController *)[self.viewControllers lastObject];
                }else {
                    currentPageController = (PSPDFSinglePageViewController *)(self.viewControllers)[0];
                }
            }

            // pageView's frame isn't reliable at this stage. calculate manually.
            CGRect availableRect = UIEdgeInsetsInsetRect(self.pdfController.view.bounds, self.pdfController.margin);
            if (hasTwoPages) {
                availableRect.size.width = floorf(availableRect.size.width/2);
            }

            CGRect pageRect = [currentPageController.pageView.document rectBoxForPage:currentPageController.pageView.page];
            CGFloat pageScale = PSPDFScaleForSizeWithinSizeWithOptions(pageRect.size, availableRect.size, self.pdfController.isZoomingSmallDocumentsEnabled, NO);
            CGSize pageViewSize = PSPDFSizeForScale(pageRect.size, pageScale);

            // we may need to wait for viewWillLayoutSubviews to re-set the pageView frame after a rotation
            // this can happen because pageView_ auto-resizes and may set height to zero,
            // and due to a timing issue on updateViewSize we mess up our view size if we don't break here.
            if (PSPDFSizeIsEmpty(pageViewSize) && pageViewSize.width + pageViewSize.height > 0) {
                return;
            }

            size = CGSizeMake(size.width + pageViewSize.width, MAX(size.height, pageViewSize.height));
        }
    }else {
        size = self.view.bounds.size;
    }

    // the system automatically centers the view for us - no need to do extra work!
    CGRect newFrame = CGRectMake(0, 0, size.width, size.height);
    PSPDFLogVerbose(@"old frame: %@ ---- new frame: %@", NSStringFromCGRect(self.view.frame), NSStringFromCGRect(newFrame));
    self.view.frame = newFrame;
    
     // super important to also set the correct contentSize! Else controller breaks when presented directly in landscape.
    self.scrollView.contentSize = size;
    [self.scrollView setNeedsLayout];
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - PSPDFTransitionProtocol

- (void)setPdfController:(PSPDFViewController *)pdfController {
    if (pdfController != _pdfController) {
        _pdfController = pdfController;

        // destroy transition helper if pdfController gets nilled out.
        self.transitionHelper = pdfController ? [[PSPDFTransitionHelper alloc] initWithDelegate:self] : nil;
    }
}

// Set page to new value. Optionally animates.
- (void)setPage:(NSUInteger)page animated:(BOOL)animated {
    // ensure that we're zoomed out
    [self.scrollView setZoomScale:1.f];

    [self.transitionHelper setPage:page animated:animated];
}

// Returns an array of NSNumber's for the current visible page numbers. (ordered)
- (NSArray *)visiblePageNumbers {
    return [self.transitionHelper visiblePageNumbers];
}

// Get pageView for a specific page. Returns nil if page isn't currently loaded.
- (PSPDFPageView *)pageViewForPage:(NSUInteger)page {
    return [self.transitionHelper pageViewForPage:page];
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - PSPDFTransitionViewControllerDelegate

- (void)transitionHelper:(PSPDFTransitionHelper *)transitionHelper changedToPage:(NSUInteger)page doublePageMode:(BOOL)doublePageMode forwardTransition:(BOOL)forwardTransition animated:(BOOL)animated {
    [self setupViewControllersDoubleSided:doublePageMode animated:animated direction:forwardTransition ? UIPageViewControllerNavigationDirectionForward : UIPageViewControllerNavigationDirectionReverse splineLocation:self.spineLocation];
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UIPageViewControllerDataSource

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController {
    PSPDFSinglePageViewController *singlePageController = [self.transitionHelper viewControllerBeforeViewController:viewController];
    singlePageController.useSolidBackground = _useSolidBackground;
    return singlePageController;
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController {
    PSPDFSinglePageViewController *singlePageController = [self.transitionHelper viewControllerAfterViewController:viewController];
    singlePageController.useSolidBackground = _useSolidBackground;
    return singlePageController;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UIPageViewControllerDelegate

// Sent when a gesture-initiated transition ends. The 'finished' parameter indicates whether the animation finished, while the 'completed' parameter indicates whether the transition completed or bailed out (if the user let go early).
- (void)pageViewController:(UIPageViewController *)pageViewController didFinishAnimating:(BOOL)finished previousViewControllers:(NSArray *)previousViewControllers transitionCompleted:(BOOL)completed {
    PSPDFLogVerbose(@"finished animating:%d transitionCompleted:%d", finished, completed);
    
    // save new page and apply fixes for Apple's broken UIPageViewController.
    // Note: this is improved in iOS6, but still broken for iOS5/5.1
    if (completed) {
        if ([pageViewController.viewControllers count]) {
            
            // apply new page
            PSPDFSinglePageViewController *singlePageViewController = (pageViewController.viewControllers)[0];
            NSUInteger newPage = singlePageViewController.page;
            [self.transitionHelper setPageInternal:newPage == NSUIntegerMax ? 0 : newPage];
            [self.pdfController setPageInternal:self.transitionHelper.page];

            // fixes bugs in the pre-iOS6 version of PSPDFPageViewController
            PSPDF_IF_PRE_IOS6(
            // check all views and remove any leftover pages / animation stuff (WTF, Apple!?)
            for (UIView *subview in self.view.subviews) {
                PSPDFPageView *pageView = nil;
                for (UIView *subsubView in subview.subviews) {
                    if ([subsubView isKindOfClass:[PSPDFPageView class]]) {
                        pageView = (PSPDFPageView *)subsubView; break;
                    }
                }

                BOOL removeView = YES;
                if (pageView) {
                    if (![self.pdfController isDoublePageMode]) {
                        removeView = pageView.page != self.transitionHelper.page;
                    }else {
                        // search if one of the controllers matches the page, remove if not.
                        for (PSPDFSinglePageViewController *single in pageViewController.viewControllers) {
                            if (single.page == pageView.page) {
                                removeView = NO; break;
                            }
                        }
                    }                    
                }
                
                if (removeView) {
                    PSPDFLogVerbose(@"Fixed bug in UIPageViewController: remove leftover view %@", subview);
                    [subview removeFromSuperview];
                }
            }
            
            // next, check if there is a leftover controller
            for (UIViewController *childController in self.childViewControllers) {
                BOOL found = NO;
                for (PSPDFSinglePageViewController *singlePage in pageViewController.viewControllers) {
                    if (childController == singlePage) {
                        found = YES; break;
                    }
                }
                
                if (!found) {
                    PSPDFLogVerbose(@"Fixed bug in UIPageViewController: remove leftover controller %@", childController);
                    [childController removeFromParentViewController];
                }                
            })
        }
    }
    
    // hide UI
    [self.pdfController hideControlsAndPageElements];
    
    // adapt view size in next runloop (or we get in UIKit-trouble)
    if (finished) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self updateViewSize];
        });
    }
}

- (void)setupPageViewControllersDualPaged:(BOOL)isDoublePageMode splineLocation:(UIPageViewControllerSpineLocation)splineLocation {
    // Delegate may set new view controllers or update double-sided state within this method's implementation as well.
    if (splineLocation == UIPageViewControllerSpineLocationMid) {
        [self.transitionHelper setPageInternal:[self.transitionHelper fixPageNumberForDoublePageMode:self.pdfController.page forceDoublePageMode:YES]];
        [self setupViewControllersDoubleSided:YES animated:YES direction:UIPageViewControllerNavigationDirectionForward splineLocation:splineLocation];
    }else {
        [self.transitionHelper setPageInternal:self.pdfController.page];
        [self setupViewControllersDoubleSided:NO animated:YES direction:UIPageViewControllerNavigationDirectionForward splineLocation:splineLocation];
    }
}

- (UIPageViewControllerSpineLocation)pageViewController:(UIPageViewController *)pageViewController spineLocationForInterfaceOrientation:(UIInterfaceOrientation)orientation {
    // ensure we have the correct page for double page mode
    BOOL isDoublePageMode = [self.pdfController isDoublePageModeForOrientation:orientation] && [self.pdfController.document pageCount] > 1;
    UIPageViewControllerSpineLocation spineLocation = [self splineLocationForPDFController:self.pdfController];
    
    if (isDoublePageMode) {
        spineLocation = UIPageViewControllerSpineLocationMid;
    }
    [self setupPageViewControllersDualPaged:isDoublePageMode splineLocation:spineLocation];
    
    return spineLocation;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UIGestureRecognizerDelegate

- (void)handleGesture:(UIGestureRecognizer *)gestureRecognizer {
    switch (gestureRecognizer.state) {
        case UIGestureRecognizerStateChanged:
            for(PSPDFSinglePageViewController *singlePage in self.viewControllers) {
                for (UIView<PSPDFAnnotationView> *annotationView in singlePage.pageView.visibleAnnotationViews) {
                    if ([annotationView respondsToSelector:@selector(willHidePage:)]) {
                        [annotationView willHidePage:self.transitionHelper.page];
                    }
                }
            }
            break;
        case UIGestureRecognizerStateCancelled:
        case UIGestureRecognizerStateFailed:
        case UIGestureRecognizerStateEnded:
            for(PSPDFSinglePageViewController *singlePage in self.viewControllers) {
                for (UIView<PSPDFAnnotationView> *annotationView in singlePage.pageView.visibleAnnotationViews) {
                    if ([annotationView respondsToSelector:@selector(willShowPage:)]) {
                        [annotationView willShowPage:self.transitionHelper.page];
                    }
                }
            }
        default:
            break;
    }
}

// Undocumented: this class is the gesture recognizer delegate (but pretty obvious).
// Confirmation: https://github.com/steipete/iOS-Runtime-Headers/blob/master/Frameworks/UIKit.framework/UIPageViewController.h
- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer {
    // disable any paging while we're in zoom mode
    BOOL isNotZoomed = self.scrollView.zoomScale == 1;
    return isNotZoomed && !self.pdfController.isViewLockEnabled;
}

@end
