//
//  PSPDFViewController+Delegates.m
//  PSPDFKit
//
//  Copyright (c) 2012 Peter Steinberger. All rights reserved.
//

#import "PSPDFViewController+Delegates.h"
#import "PSPDFViewControllerDelegate.h"
#import "PSPDFViewController+Internal.h"
#import "PSPDFAnnotation.h"
#import "PSPDFPageView.h"
#import "PSPDFDocument.h"

// class extension to gain access to some private properties
@interface PSPDFViewController ()
@property (nonatomic, strong, readonly) PSPDFViewState *restoreViewStatePending;
@property (nonatomic, assign) PSPDFDelegateFlags delegateFlags;
@end

@implementation PSPDFViewController (Delegates)

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Delegate

// gets called in PSPDFViewController.
- (void)updateDelegateFlags {
    id<PSPDFViewControllerDelegate> delegate = self.delegate;

    PSPDFDelegateFlags flags;
    flags.delegateShouldSetDocument = [delegate respondsToSelector:@selector(pdfViewController:shouldSetDocument:)];
    flags.delegateWillDisplayDocument = [delegate respondsToSelector:@selector(pdfViewController:willDisplayDocument:)];
    flags.delegateDidDisplayDocument = [delegate respondsToSelector:@selector(pdfViewController:didDisplayDocument:)];
    flags.delegateDidShowPageView = [delegate respondsToSelector:@selector(pdfViewController:didShowPageView:)];
    flags.delegateDidRenderPageView = [delegate respondsToSelector:@selector(pdfViewController:didRenderPageView:)];
    flags.delegateDidChangeViewMode = [delegate respondsToSelector:@selector(pdfViewController:didChangeViewMode:)];
    flags.delegateDidTapOnPageView = [delegate respondsToSelector:@selector(pdfViewController:didTapOnPageView:atPoint:)];

    // text selection
    flags.delegateDidLongPressOnPageView = [delegate respondsToSelector:@selector(pdfViewController:didLongPressOnPageView:atPoint:gestureRecognizer:)];
    flags.delegateShouldSelectText = [delegate respondsToSelector:@selector(pdfViewController:shouldSelectText:withGlyphs:atRect:onPageView:)];
    flags.delegateDidSelectText = [delegate respondsToSelector:@selector(pdfViewController:didSelectText:withGlyphs:atRect:onPageView:)];
    flags.delegateShouldShowMenuItemsForSelectedText = [delegate respondsToSelector:@selector(pdfViewController:shouldShowMenuItems:atSuggestedTargetRect:forSelectedText:inRect:onPageView:)];
    flags.delegateShouldShowMenuItemsForSelectedImage = [delegate respondsToSelector:@selector(pdfViewController:shouldShowMenuItems:atSuggestedTargetRect:forSelectedImage:inRect:onPageView:)];

    // annotations
    flags.delegateShouldSelectAnnotation = [delegate respondsToSelector:@selector(pdfViewController:shouldSelectAnnotation:onPageView:)];
    flags.delegateDidSelectAnnotation = [delegate respondsToSelector:@selector(pdfViewController:didSelectAnnotation:onPageView:)];
    flags.delegateShouldShowMenuItemsForAnnotation = [delegate respondsToSelector:@selector(pdfViewController:shouldShowMenuItems:atSuggestedTargetRect:forAnnotation:inRect:onPageView:)];
    flags.delegateDidTapOnAnnotation = [delegate respondsToSelector:@selector(pdfViewController:didTapOnAnnotation:annotationPoint:annotationView:pageView:viewPoint:)];
    flags.delegateShouldDisplayAnnotation = [delegate respondsToSelector:@selector(pdfViewController:shouldDisplayAnnotation:onPageView:)];
    flags.delegateShouldScrollToPage = [delegate respondsToSelector:@selector(pdfViewController:shouldScrollToPage:)];
    flags.delegateAnnotationViewForAnnotation = [delegate respondsToSelector:@selector(pdfViewController:annotationView:forAnnotation:onPageView:)];
    flags.delegateWillShowAnnotationView = [delegate respondsToSelector:@selector(pdfViewController:willShowAnnotationView:onPageView:)];
    flags.delegateDidShowAnnotationView = [delegate respondsToSelector:@selector(pdfViewController:didShowAnnotationView:onPageView:)];

    flags.delegateDidLoadPageView = [delegate respondsToSelector:@selector(pdfViewController:didLoadPageView:)];
    flags.delegateWillUnloadPageView = [delegate respondsToSelector:@selector(pdfViewController:willUnloadPageView:)];
    flags.delegateDidEndPageDraggingWillDecelerate = [delegate respondsToSelector:@selector(pdfViewController:didEndPageDragging:willDecelerate:withVelocity:targetContentOffset:)];
    flags.delegateDidEndPageScrollingAnimation = [delegate respondsToSelector:@selector(pdfViewController:didEndPageScrollingAnimation:)];
    flags.delegateDidEndZoomingAtScale = [delegate respondsToSelector:@selector(pdfViewController:didEndPageZooming:atScale:)];
    flags.delegateShouldShowControllerAnimated = [delegate respondsToSelector:@selector(pdfViewController:shouldShowController:embeddedInController:animated:)];
    flags.delegateDidShowControllerAnimated = [delegate respondsToSelector:@selector(pdfViewController:didShowController:embeddedInController:animated:)];
    flags.delegateDocumentForRelativePath = [delegate respondsToSelector:@selector(pdfViewController:documentForRelativePath:)];
    self.delegateFlags = flags;
}

- (BOOL)delegateShouldScrollToPage:(NSUInteger)page {
    BOOL shouldScroll = YES;
    if (self.delegateFlags.delegateShouldScrollToPage) {
        shouldScroll = [self.delegate pdfViewController:self shouldScrollToPage:page];
    }
    return shouldScroll;
}

- (BOOL)delegateShouldSetDocument:(PSPDFDocument *)document {
    BOOL shouldDisplay = YES;
    if (self.delegateFlags.delegateShouldSetDocument) {
        shouldDisplay = [self.delegate pdfViewController:self shouldSetDocument:document];
    }
    return shouldDisplay;
}

- (void)delegateWillDisplayDocument {
    if (self.delegateFlags.delegateWillDisplayDocument) {
        [self.delegate pdfViewController:self willDisplayDocument:self.document];
    }
}

- (void)delegateDidDisplayDocument {
    if (self.delegateFlags.delegateDidDisplayDocument) {
        [self.delegate pdfViewController:self didDisplayDocument:self.document];
    }
}

// helper, only look for PSPDFPageView if we really need it!
- (void)delegateDidShowPage:(NSUInteger)page {
    // don't call if view is not loaded
    if (self.delegateFlags.delegateDidShowPageView && self.document && [self isViewLoaded]) {
        PSPDFPageView *pageView = [self pageViewForPage:page];
        if (pageView) {
            [self delegateDidShowPageView:pageView];
        }
    }
}

- (void)delegateDidShowPageView:(PSPDFPageView *)pageView {
    if (!pageView.document) return; // don't call if document is nil.

    if (self.delegateFlags.delegateDidShowPageView) {
        [self.delegate pdfViewController:self didShowPageView:pageView];
    }
}

- (void)delegateDidRenderPageView:(PSPDFPageView *)pageView {
    if (self.delegateFlags.delegateDidRenderPageView) {
        [self.delegate pdfViewController:self didRenderPageView:pageView];
    }
}

- (void)delegateDidChangeViewMode:(PSPDFViewMode)viewMode {
    if (self.delegateFlags.delegateDidChangeViewMode) {
        [self.delegate pdfViewController:self didChangeViewMode:viewMode];
    }
}

- (BOOL)delegateDidTapOnPageView:(PSPDFPageView *)pageView atPoint:(CGPoint)viewPoint {
    BOOL touchProcessed = NO;
    if (self.delegateFlags.delegateDidTapOnPageView) {
        touchProcessed = [self.delegate pdfViewController:self didTapOnPageView:pageView atPoint:viewPoint];
    }
    return touchProcessed;
}

- (BOOL)delegateDidLongPressOnPageView:(PSPDFPageView *)pageView atPoint:(CGPoint)viewPoint gestureRecognizer:(UILongPressGestureRecognizer *)gestureRecognizer {
    BOOL touchProcessed = NO;
    if (self.delegateFlags.delegateDidLongPressOnPageView) {
        touchProcessed = [self.delegate pdfViewController:self didLongPressOnPageView:pageView atPoint:viewPoint gestureRecognizer:gestureRecognizer];
    }
    return touchProcessed;
}

- (BOOL)delegateShouldSelectText:(NSString *)text withGlyphs:(NSArray *)glyphs atRect:(CGRect)rect  onPageView:(PSPDFPageView *)pageView {
    BOOL shouldSelectText = YES;
    if (self.delegateFlags.delegateShouldSelectText) {
        shouldSelectText = [self.delegate pdfViewController:self shouldSelectText:text withGlyphs:glyphs atRect:rect onPageView:pageView];
    }
    return shouldSelectText;
}

- (void)delegateDidSelectText:(NSString *)text withGlyphs:(NSArray *)glyphs atRect:(CGRect)rect  onPageView:(PSPDFPageView *)pageView {
    if (self.delegateFlags.delegateDidSelectText) {
        [self.delegate pdfViewController:self didSelectText:text withGlyphs:glyphs atRect:rect onPageView:pageView];
    }
}

- (NSArray *)delegateShouldShowMenuItems:(NSArray *)menuItems atSuggestedTargetRect:(CGRect)rect forSelectedText:(NSString *)selectedText inRect:(CGRect)textRect onPageView:(PSPDFPageView *)pageView {
    NSArray *newMenuItems = menuItems;
    if (self.delegateFlags.delegateShouldShowMenuItemsForSelectedText) {
        newMenuItems = [self.delegate pdfViewController:self shouldShowMenuItems:menuItems atSuggestedTargetRect:rect forSelectedText:selectedText inRect:textRect onPageView:pageView];
    }
    return newMenuItems;
}

- (NSArray *)delegateShouldShowMenuItems:(NSArray *)menuItems atSuggestedTargetRect:(CGRect)rect forSelectedImage:(PSPDFImageInfo *)selectedImage inRect:(CGRect)textRect onPageView:(PSPDFPageView *)pageView {
    NSArray *newMenuItems = menuItems;
    if (self.delegateFlags.delegateShouldShowMenuItemsForSelectedImage) {
        newMenuItems = [self.delegate pdfViewController:self shouldShowMenuItems:menuItems atSuggestedTargetRect:rect forSelectedImage:selectedImage inRect:textRect onPageView:pageView];
    }
    return newMenuItems;
}

- (BOOL)delegateShouldSelectAnnotation:(PSPDFAnnotation *)annotation onPageView:(PSPDFPageView *)pageView {
    BOOL shouldSelect = YES;
    if (self.delegateFlags.delegateShouldSelectAnnotation) {
        shouldSelect = [self.delegate pdfViewController:self shouldSelectAnnotation:annotation onPageView:pageView];
    }
    return shouldSelect;
}

- (void)delegateDidSelectAnnotation:(PSPDFAnnotation *)annotation onPageView:(PSPDFPageView *)pageView {
    if (self.delegateFlags.delegateDidSelectAnnotation) {
        [self.delegate pdfViewController:self didSelectAnnotation:annotation onPageView:pageView];
    }
}

- (NSArray *)delegateShouldShowMenuItems:(NSArray *)menuItems atSuggestedTargetRect:(CGRect)rect forAnnotation:(PSPDFAnnotation *)annotation inRect:(CGRect)textRect onPageView:(PSPDFPageView *)pageView {
    NSArray *newMenuItems = menuItems;
    if (self.delegateFlags.delegateShouldShowMenuItemsForAnnotation) {
        newMenuItems = [self.delegate pdfViewController:self shouldShowMenuItems:menuItems atSuggestedTargetRect:rect forAnnotation:annotation inRect:textRect onPageView:pageView];
    }
    return newMenuItems;
}

- (BOOL)delegateDidTapOnAnnotation:(PSPDFAnnotation *)annotation annotationPoint:(CGPoint)annotationPoint annotationView:(UIView<PSPDFAnnotationView> *)annotationView pageView:(PSPDFPageView *)pageView viewPoint:(CGPoint)viewPoint {
    BOOL touchProcessed = NO;
    if (self.delegateFlags.delegateDidTapOnAnnotation) {
        touchProcessed = [self.delegate pdfViewController:self didTapOnAnnotation:annotation annotationPoint:annotationPoint annotationView:annotationView pageView:pageView viewPoint:viewPoint];
    }
    return touchProcessed;
}

- (BOOL)delegateShouldDisplayAnnotation:(PSPDFAnnotation *)annotation onPageView:(PSPDFPageView *)pageView {
    BOOL shouldDisplayAnnotation = YES; // default
    if (self.delegateFlags.delegateShouldDisplayAnnotation) {
        shouldDisplayAnnotation = [self.delegate pdfViewController:self shouldDisplayAnnotation:annotation onPageView:pageView];
    }
    return shouldDisplayAnnotation;
}

- (UIView <PSPDFAnnotationView> *)delegateAnnotationView:(UIView <PSPDFAnnotationView> *)annotationView forAnnotation:(PSPDFAnnotation *)annotation onPageView:(PSPDFPageView *)pageView {
    if (self.delegateFlags.delegateAnnotationViewForAnnotation) {
        annotationView = [self.delegate pdfViewController:self annotationView:annotationView forAnnotation:annotation onPageView:pageView];
    }
    return annotationView;
}

- (void)delegateWillShowAnnotationView:(UIView <PSPDFAnnotationView> *)annotationView onPageView:(PSPDFPageView *)pageView {
    if (self.delegateFlags.delegateWillShowAnnotationView) {
        [self.delegate pdfViewController:self willShowAnnotationView:annotationView onPageView:pageView];
    }
}

- (void)delegateDidShowAnnotationView:(UIView <PSPDFAnnotationView> *)annotationView onPageView:(PSPDFPageView *)pageView {
    if (self.delegateFlags.delegateDidShowAnnotationView) {
        [self.delegate pdfViewController:self didShowAnnotationView:annotationView onPageView:pageView];
    }
}

- (void)delegateDidLoadPageView:(PSPDFPageView *)pageView {
    if (!pageView.document) return; // don't call if document is nil.

    if (self.delegateFlags.delegateDidLoadPageView) {
        [self.delegate pdfViewController:self didLoadPageView:pageView];
    }
}

- (void)delegateWillUnloadPageView:(PSPDFPageView *)pageView {
    if (!pageView.document) return; // don't call if document is nil.

    if (self.delegateFlags.delegateWillUnloadPageView) {
        [self.delegate pdfViewController:self willUnloadPageView:pageView];
    }
}

- (void)delegateDidEndPageDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset {
    if (self.delegateFlags.delegateDidEndPageDraggingWillDecelerate) {
        [self.delegate pdfViewController:self didEndPageDragging:scrollView willDecelerate:decelerate withVelocity:velocity targetContentOffset:targetContentOffset];
    }
}

- (void)delegateDidEndPageScrollingAnimation:(UIScrollView *)scrollView {
    // hook for view restoring
    if (self.restoreViewStatePending) {
        [self setViewState:self.restoreViewStatePending animated:YES];
    }

    if (self.delegateFlags.delegateDidEndPageScrollingAnimation) {
        [self.delegate pdfViewController:self didEndPageScrollingAnimation:scrollView];
    }
}

- (void)delegateDidEndZooming:(UIScrollView *)scrollView withView:(UIView *)view atScale:(float)scale {
    if (self.delegateFlags.delegateDidEndZoomingAtScale) {
        [self.delegate pdfViewController:self didEndPageZooming:scrollView atScale:scale];
    }
}

- (BOOL)delegateShouldShowController:(id)viewController embeddedInController:(id)controller animated:(BOOL)animated {
    BOOL shouldShow = YES;
    
    if (self.delegateFlags.delegateShouldShowControllerAnimated) {
        shouldShow = [self.delegate pdfViewController:self shouldShowController:viewController embeddedInController:controller animated:animated];
    }
    return shouldShow;
}

- (void)delegateDidShowController:(id)viewController embeddedInController:(id)controller animated:(BOOL)animated {
    if (self.delegateFlags.delegateDidShowControllerAnimated) {
        [self.delegate pdfViewController:self didShowController:viewController embeddedInController:controller animated:animated];
    }
}

- (PSPDFDocument *)delegateDocumentForRelativePath:(NSString *)relativePath {
    PSPDFDocument *document = nil;
    if (self.delegateFlags.delegateDocumentForRelativePath) {
        document = [self.delegate pdfViewController:self documentForRelativePath:relativePath];
    }

    // try to create the document ourselves.
    if (!document) {
        NSURL *newURL = [self.document.basePath URLByAppendingPathComponent:relativePath];
        BOOL fileExists = ![newURL isFileURL] || [[NSFileManager defaultManager] fileExistsAtPath:[newURL path]];
        if (fileExists) {
            document = [PSPDFDocument PDFDocumentWithURL:newURL];
        }
    }
    return document;
}

@end
