//
//  PSPDFKitGlobal.m
//  PSPDFKit
//
//  Copyright 2011-2012 Peter Steinberger. All rights reserved.
//

#import "PSPDFKitGlobal.h"
#import "PSPDFKitGlobal+Internal.h"
#import "PSPDFKit.h"
#import "PSPDFPatches.h"
#import <objc/runtime.h>
#import <mach/mach.h>
#import <mach/mach_host.h>
#import "InfoPlist.h" // defines the version string

// draw demo mode code
inline void PSPDFDrawHelper(CGContextRef context, CGFloat magnifier) {
#ifdef kPSPDFKitDemoMode
    char *text = "PSPDFKit DEMO"; \
    CGFloat demoPosition = (PSIsIpad() ? 70.f : 40.f) * magnifier;
    NSUInteger fontSize = (PSIsIpad() ? 25 : 13) * magnifier;
    CGContextSetFillColorWithColor(context, [[UIColor blackColor] colorWithAlphaComponent:0.7f].CGColor);
    CGContextSelectFont(context, "Helvetica-Bold", fontSize, kCGEncodingMacRoman);
    CGAffineTransform xform = CGAffineTransformMake(1.0, 0.0, 0.0, -1.0, 0.0, 0.0);
    CGContextSetTextMatrix(context, xform);
    CGContextSetTextDrawingMode(context, kCGTextFill);
    CGContextSetTextPosition(context, demoPosition, demoPosition + round(fontSize / 4.0f));
    CGContextShowText(context, text, (text[0] == 'P') ? 13 : 99999); // be nasty, in case text gets deleted strlen(text)
#endif
}

// global variables
NSString *const kPSPDFErrorDomain = @"com.PSPDFKit.error";
PSPDFLogLevel kPSPDFLogLevel = PSPDFLogLevelWarning;
PSPDFAnimate kPSPDFAnimateOption = PSPDFAnimateModernDevices;
CGFloat kPSPDFAnimationDuration = 0.1f;
CGFloat kPSPDFInitialAnnotationLoadDelay = 0.2f;
BOOL kPSPDFDebugScrollViews = NO;
NSString *kPSPDFCacheClassName = @"PSPDFCache";
NSString *kPSPDFIconGeneratorClassName = @"PSPDFIconGenerator";
NSUInteger kPSPDFMaximumNumberOfOpenDocumentRefs = 1;
BOOL kPSPDFLowMemoryMode = NO;

__attribute__((constructor)) static void PSPDFSetupDefaults(void) {
    @autoreleasepool {
        kPSPDFMaximumNumberOfOpenDocumentRefs = PSPDFIsCrappyDevice() ? 1 : 3;
    }
}

static char kPSPDFQueueKey;
inline dispatch_queue_t pspdf_dispatch_queue_create(const char *label, dispatch_queue_attr_t attr) {
    dispatch_queue_t queue = dispatch_queue_create(label, attr);
    dispatch_queue_set_specific(queue, &kPSPDFQueueKey, queue, NULL);
    return queue;
}

inline void pspdf_dispatch_sync_reentrant(dispatch_queue_t queue, dispatch_block_t block) {
    dispatch_queue_t savedQueue = (dispatch_queue_t)dispatch_get_specific(&kPSPDFQueueKey);
    savedQueue == queue ? block() : dispatch_sync(queue, block);
}

inline void pspdf_dispatch_async_if(dispatch_queue_t queue, BOOL async, dispatch_block_t block) {
    async ? dispatch_async(queue, block) : pspdf_dispatch_sync_reentrant(queue, block);
}

BOOL PSPDFShouldAnimate(void) {
    return (!PSPDFIsCrappyDevice() && kPSPDFAnimateOption == PSPDFAnimateModernDevices) || kPSPDFAnimateOption == PSPDFAnimateEverywhere;
}

// http://blakespot.com/ios_device_specifications_grid.html
BOOL PSPDFIsCrappyDevice(void) {
    static BOOL isCrappyDevice = YES;

    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        BOOL isIPad2 = (PSIsIpad() && [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]);

        // Needed to detect the iPod touch (4G).
        mach_port_t host_port = mach_host_self();
        mach_msg_type_number_t host_size = sizeof(vm_statistics_data_t) / sizeof(integer_t);
        vm_size_t pagesize;
        host_page_size(host_port, &pagesize);
        vm_statistics_data_t vm_stat;
        if (host_statistics(host_port, HOST_VM_INFO, (host_info_t)&vm_stat, &host_size) != KERN_SUCCESS)
            PSPDFLogError(@"Failed to fetch vm statistics");

        // Stats in bytes
        natural_t mem_used = (vm_stat.active_count + vm_stat.inactive_count + vm_stat.wire_count) * pagesize;
        natural_t mem_free = vm_stat.free_count * pagesize;
        natural_t mem_total = mem_used + mem_free;
        PSPDFLogVerbose(@"used: %u free: %u total: %u", mem_used, mem_free, mem_total);
        BOOL isLowOnMemory = mem_total <= 200 * powf(1024, 2); // Less or equal to 256MB is bad.

        BOOL hasRetina = [[UIScreen mainScreen] scale] > 1.f;
        if (!isLowOnMemory && (isIPad2 || hasRetina || PSIsSimulator())) isCrappyDevice = NO;
        else PSPDFLog(@"Old device detected. Reducing animations.");
    });

    return isCrappyDevice;
}

NSString *PSPDFVersionString(void) {
    return [NSString stringWithFormat:@"PSPDFKit %@ (%d)", GIT_VERSION, GIT_COMMIT_COUNT];
}

// for localization
#define kPSPDFKitBundleName @"PSPDFKit.bundle"
NSBundle *PSPDFKitBundle(void) {
    static NSBundle *bundle = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        NSString* path = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:kPSPDFKitBundleName];
        bundle = [NSBundle bundleWithPath:path];
    });

    if (!bundle) PSPDFLogError(@"Error! PSPDFKit.bundle not found. Search and annotations will not work properly.");
    return bundle;
}

static NSString *PSPDFPreferredLocale(void) {
    static NSString *locale = nil;
    if (!locale) {
        NSArray *locales = [NSLocale preferredLanguages];
        if ([locales count]) {
            locale = [locales[0] copy];
        }else {
            PSPDFLogWarning(@"No preferred language? [NSLocale preferredLanguages] returned nil. Defaulting to english.");
            locale = @"en";
        }
    }
    return locale;
}

__strong static NSDictionary *pspdfLocalizationDict_ = nil;
NSString *PSPDFLocalize(NSString *stringToken) {
    // load language from bundle
    NSString *localization = NSLocalizedStringFromTableInBundle(stringToken, @"PSPDFKit", PSPDFKitBundle(), @"");
    if (!localization) localization = stringToken;

    // try loading from the global translation dict
    NSString *replLocale = nil;
    if (pspdfLocalizationDict_) {
        NSString *language = PSPDFPreferredLocale();
        replLocale = pspdfLocalizationDict_[language][stringToken];
        if (!replLocale && !pspdfLocalizationDict_[language] && ![language isEqualToString:@"en"]) {
            replLocale = pspdfLocalizationDict_[@"en"][stringToken];
        }
    }

    return replLocale ?: localization;
}

void PSPDFSetLocalizationDictionary(NSDictionary *localizationDict) {
    if (localizationDict != pspdfLocalizationDict_) {
        pspdfLocalizationDict_ = [localizationDict copy];
    }
    PSPDFLogVerbose(@"new localization dictionary set. locale: %@; dict: %@", PSPDFPreferredLocale(), localizationDict);
}

BOOL PSPDFResolvePathNamesEnableLegacyBehavior = NO;
NSString *PSPDFResolvePathNames(NSString *path, NSString *fallbackPath) {
    if (!path) return nil;
    NSMutableString *mutableString = [NSMutableString stringWithString:path];
    PSPDFResolvePathNamesInMutableString(mutableString, fallbackPath, NULL);
    return [mutableString copy];
}

BOOL PSPDFIsRootPath(NSString *path) {
    if ([path rangeOfString:@"/var/mobile/Applications/" options:NSCaseInsensitiveSearch].length > 0) return YES;
    PSPDF_IF_SIMULATOR(if([path rangeOfString:@"/iPhone Simulator/" options:NSCaseInsensitiveSearch].length > 0) return YES;)
    return NO;
}

BOOL PSPDFResolvePathNamesInMutableString(NSMutableString *mutableString, NSString *fallbackPath, NSString *(^resolveUnknownPathBlock)(NSString *unknownPath)) {
    if (PSPDFResolvePathNamesEnableLegacyBehavior) fallbackPath = nil;

    // if this is a full path; don't try to replace any parts.
    if (PSPDFIsRootPath(mutableString)) return NO;

    // replace Documents
    NSString *documentsDir = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
    BOOL replaced = PSPDFReplaceFirstOccurenceOfStringWithString(mutableString, @"/Documents", documentsDir);

    // replace Cache
    if (!replaced) {
        NSString *cachesDir = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES)[0];
        replaced = PSPDFReplaceFirstOccurenceOfStringWithString(mutableString, @"/Cache", cachesDir);
    }

    // replace Library
    if (!replaced) {
        NSString *cachesDir = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES)[0];
        replaced = PSPDFReplaceFirstOccurenceOfStringWithString(mutableString, @"/Library", cachesDir);
    }

    // replace Bundle
    if (!replaced) {
        NSString *bundlePath = [[NSBundle mainBundle] bundlePath];
        replaced = PSPDFReplaceFirstOccurenceOfStringWithString(mutableString, @"/Bundle", bundlePath);

        // if no replacements could be found, use local bundle
        if (!replaced && resolveUnknownPathBlock) {
            // first check if the pdf view controller wishes to handle special directory paths
            NSString *specialDirIdentifier = [mutableString componentsSeparatedByString:@"/"][1];
            if ([specialDirIdentifier length]) {
                NSString *resolvedPath = resolveUnknownPathBlock(specialDirIdentifier);
                if ([resolvedPath length]) {
                    NSString *specialPathIdentifier = [NSString stringWithFormat:@"/%@", specialDirIdentifier];
                    replaced = PSPDFReplaceFirstOccurenceOfStringWithString(mutableString, specialPathIdentifier, resolvedPath);
                }
            }
        }
        if (!replaced) {
            // use fallbackPath; but only if file at current path doesn't exist.
            if (![[NSFileManager defaultManager] fileExistsAtPath:mutableString]) {
                BOOL addSlash = ![mutableString hasPrefix:@"/"];
                [mutableString insertString:[NSString stringWithFormat:@"%@%@", fallbackPath ?: bundlePath, addSlash ? @"/" : @""] atIndex:0];
            }
        }
    }

    return replaced;
}

UIView *PSPDFGetViewInsideView(UIView *view, NSString *classNamePrefix) {
    if (!view || [classNamePrefix length] == 0) return nil;

    UIView *theView = nil;
    for (UIView *subview in view.subviews) {
        if ([NSStringFromClass([subview class]) hasPrefix:classNamePrefix] || [NSStringFromClass([subview superclass]) hasPrefix:classNamePrefix]) {
            return subview;
        }else {
            if ((theView = PSPDFGetViewInsideView(subview, classNamePrefix))) break;
        }
    }
    return theView;
}

NSString *PSPDFStripPDFFileType(NSString *pdfFileName) {
    if (pdfFileName) {
        pdfFileName = [pdfFileName stringByReplacingOccurrencesOfString:@".pdf" withString:@"" options:NSCaseInsensitiveSearch | NSBackwardsSearch range:NSMakeRange(0, [pdfFileName length])];
    }
    return pdfFileName;
}

// The keyboard is initialized lazily. This will prevent the load delay.
void PSPDFCacheKeyboard(void) {
    PSPDF_IF_PRE_IOS6(return;) // breaks UITableView on iOS 5.0
    UITextField *textField = [UITextField new];
    [[[UIApplication sharedApplication] keyWindow] addSubview:textField];
    [textField becomeFirstResponder];
    [textField resignFirstResponder];
    [textField removeFromSuperview];
}

Class PSPDFClassForClassInDictionary(Class originalClass, NSDictionary *overrideClassNames) {
    if (!originalClass) return nil;
    
    Class overrideClassObject = overrideClassNames[originalClass];
    if (!overrideClassObject) {
        // add legacy support to work with strings of class names
        NSString *overriddenClassName = overrideClassNames[NSStringFromClass(originalClass)];
        if (overriddenClassName) {
            if (!(overrideClassObject = NSClassFromString(overriddenClassName))) {
                PSPDFLogError(@"Error! Couldn't find class %@ in runtime. Using default %@ instead.", overriddenClassName, NSStringFromClass(originalClass));
            }
        }
    }
    // string detection support.
    if ([overrideClassObject respondsToSelector:@selector(cStringUsingEncoding:)]) {
        overrideClassObject = NSClassFromString((NSString *)overrideClassObject);
    }
    return overrideClassObject ?: originalClass;
}

void PSPDFCheckOverrideClassNames(NSDictionary *overrideClassNames) {
    // tricky, as we support both Class and NSString.
    [overrideClassNames enumerateKeysAndObjectsUsingBlock:^(Class overriddenClass, Class overrideClass, BOOL *stop) {
        overriddenClass = [overriddenClass respondsToSelector:@selector(cStringUsingEncoding:)] ? NSClassFromString((id)overriddenClass) : overriddenClass;
        overrideClass = [overrideClass respondsToSelector:@selector(cStringUsingEncoding:)] ? NSClassFromString((id)overrideClass) : overrideClass;
        
        if ([overrideClass isEqual:overriddenClass]) {
            PSPDFLogWarning(@"Override entry %@ with same class will be ignored.", NSStringFromClass(overrideClass));
        }
        Class parentOverrideClass = overrideClass;
        while (parentOverrideClass && ![overriddenClass isEqual:parentOverrideClass]) {
            parentOverrideClass = class_getSuperclass(parentOverrideClass);
        }
        if (!parentOverrideClass) {
            @throw [NSException exceptionWithName:NSInvalidArgumentException reason:[NSString stringWithFormat:@"%@ isn't a subclass of %@. (The PSPDF* class comes first, e.g. @{(id)[PSPDFCloseBarButtonItem class] : [MyCustomButtonSubclass class]})", NSStringFromClass(overrideClass), NSStringFromClass(overriddenClass)] userInfo:nil];
        }
    }];
}

CGFloat PSPDFSimulatorAnimationDragCoefficient(void) {
    static CGFloat (*UIAnimationDragCoefficient)(void) = NULL;
#if TARGET_IPHONE_SIMULATOR
#import <dlfcn.h>
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        void *UIKit = dlopen([[[NSBundle bundleForClass:[UIApplication class]] executablePath] fileSystemRepresentation], RTLD_LAZY);
        UIAnimationDragCoefficient = (CGFloat (*)(void))dlsym(UIKit, "UIAnimationDragCoefficient");
    });
#endif
    return UIAnimationDragCoefficient ? UIAnimationDragCoefficient() : 1.f;
}

CATransition *PSPDFFadeTransition(void) {
    return PSPDFFadeTransitionWithDuration(0.25f);
}

CATransition *PSPDFFadeTransitionWithDuration(CGFloat duration) {
    CATransition *transition = [CATransition animation];
    transition.duration = duration * PSPDFSimulatorAnimationDragCoefficient();
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionFade;
    return transition;
}

UIActionSheetStyle PSPDPFActionSheetStyleForBarButtonStyle(UIBarStyle barStyle, BOOL translucent) {
    switch (barStyle) {
        case UIBarStyleBlack: return translucent ? UIActionSheetStyleBlackTranslucent : UIActionSheetStyleBlackOpaque;
        case UIBarStyleBlackTranslucent: return UIActionSheetStyleBlackTranslucent; // deprecated
        default: return UIActionSheetStyleDefault; // UIBarStyleDefault
    }
}

CGFloat PSPDFToolbarHeightForOrientation(UIInterfaceOrientation orientation) {
    return PSPDFToolbarHeight(UIInterfaceOrientationIsLandscape(orientation));
}
CGFloat PSPDFToolbarHeight(BOOL isSmall) {
    return isSmall && !PSIsIpad() ? 33.f : 44.f;
}

inline CGFloat psrangef(float minRange, float value, float maxRange) {
    return fminf(fmaxf(value, minRange), maxRange);
}

BOOL PSPDFReplaceFirstOccurenceOfStringWithString(NSMutableString *mutableString, NSString *target, NSString *replacement) {
    BOOL foundTarget = NO;
    // scanner are not case sensitive by default.
	NSScanner *scanner = [[NSScanner alloc] initWithString:mutableString];
	[scanner scanUpToString:target intoString:NULL];
	if (![scanner isAtEnd]) {
		int targetLocation = [scanner scanLocation];
		[mutableString deleteCharactersInRange:NSMakeRange(targetLocation, [target length])] ;
		[mutableString insertString:replacement
				   atIndex:targetLocation];
		foundTarget = YES;
	}
	return foundTarget;
}

NSError *PSPDFError(NSInteger errorCode, NSString *description, NSError **errorToSet) {
    PSPDFLogError(@"Error %d: %@", errorCode, description);
    return PSPDFErrorWithUnderlyingError(errorCode, description, nil, errorToSet);
}

NSError *PSPDFErrorWithUnderlyingError(NSInteger errorCode, NSString *description, NSError *underlyingError, NSError **errorToSet) {
    NSMutableDictionary *errorDict = [NSMutableDictionary dictionary];
    if (description) errorDict[NSLocalizedDescriptionKey] = description;
    if (underlyingError) errorDict[NSUnderlyingErrorKey] = underlyingError;
    NSError *error = [NSError errorWithDomain:kPSPDFErrorDomain code:errorCode userInfo:errorDict];
    if (errorToSet) *errorToSet = error;
    return error;
}

NSError *PSPDFErrorWithException(NSInteger errorCode, NSString *description, NSException *exception, NSError **errorToSet) {
    PSPDFLogError(@"Error %d: %@ exception: %@", errorCode, description, exception);
    return PSPDFErrorWithUnderlyingError(errorCode, description, (NSError *)exception, errorToSet);
}

NSString *PSPDFTrimString(NSString *string) {
    NSString *searchTerm = [string stringByTrimmingCharactersInSet:[NSCharacterSet punctuationCharacterSet]];
    searchTerm = [searchTerm stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    searchTerm = [searchTerm stringByReplacingOccurrencesOfString:@"-\n" withString:@""];
    searchTerm = [searchTerm stringByReplacingOccurrencesOfString:@"\n" withString:@" "];
    searchTerm = [searchTerm stringByReplacingOccurrencesOfString:@"’s" withString:@""];
    searchTerm = [searchTerm stringByReplacingOccurrencesOfString:@"'s" withString:@""];
    searchTerm = [searchTerm stringByReplacingOccurrencesOfString:@"<`s" withString:@""];
    return searchTerm;
}

BOOL PSPDFIsControllerClassInPopover(UIPopoverController *popoverController, Class controllerClass) {
    UIViewController *contentController = popoverController.contentViewController;
    if ([contentController isKindOfClass:[UINavigationController class]]) {
        contentController = [(UINavigationController *)contentController topViewController];
    }

    BOOL classInPopover = [contentController isKindOfClass:controllerClass];
    return classInPopover;
}

BOOL PSPDFIsIncorrectPath(NSString *path) {
    return [path hasPrefix:@"file://localhost"];
}

NSString *PSPDFFixIncorrectPath(NSString *path) {
    // if string is wrongly converted from an NSURL internally (via description and not path), fix this problem silently.
    if (PSPDFIsIncorrectPath(path)) path = [[NSURL URLWithString:path] path];
    return path;
}

NSString *PSPDFAppName(void) {
    NSString *appName = [[NSBundle mainBundle] localizedInfoDictionary][@"CFBundleDisplayName"];
    if ([appName length] == 0) {
        appName = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleDisplayName"] ?: [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleName"];
    }
    return appName;
}

NSString *PSPDFFirstMatchInRegexString(NSString *regexString, NSString *string) {
    if (![regexString length] || ![string length]) return nil;
    
    NSError *error = nil;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:regexString options:NSRegularExpressionCaseInsensitive error:&error];
    if (!regex) { PSPDFLogWarning(@"Failed to compile regex: %@", [error localizedDescription]); return nil; }

    NSString *captureGroup = nil;
    NSTextCheckingResult *result = [regex firstMatchInString:string options:0 range:NSMakeRange(0, string.length)];
    if (result.numberOfRanges > 1) {
        captureGroup = [string substringWithRange:[result rangeAtIndex:1]];
    }
    return captureGroup;
}

NSIndexSet *PSPDFIndexSetFromArray(NSArray *array) {
    NSMutableIndexSet *indexSet = [NSMutableIndexSet indexSet];
    for (NSNumber *number in array) {
        [indexSet addIndex:[number unsignedIntegerValue]];
    }
    return [indexSet copy];
}

NSURL *PSPDFTempFileURLWithPathExtension(NSString *prefix, NSString *pathExtension) {
    if (pathExtension && ![pathExtension hasPrefix:@"."]) pathExtension = [NSString stringWithFormat:@".%@", pathExtension];
    if (!pathExtension) pathExtension = @"";

    CFUUIDRef udid = CFUUIDCreate(NULL);
    NSString *udidString = (NSString *)CFBridgingRelease(CFUUIDCreateString(NULL, udid));
    CFRelease(udid);
    if (prefix) udidString = [NSString stringWithFormat:@"_%@", udidString];

    NSURL *tempURL = [[NSURL fileURLWithPath:NSTemporaryDirectory()] URLByAppendingPathComponent:[NSString stringWithFormat:@"%@%@%@", prefix, udidString, pathExtension]];
    return tempURL;
}

bool PSPDFIsViewClassInsideViewHierarchy(UIView *theView, NSArray *classes) {
    UIView *view = theView;
    do { if ([classes containsObject:[view class]]) return YES; }
    while ((view = view.superview));
    return NO;
}

static CFStringRef CF_RETURNS_NOT_RETAINED __PSPDFObjectCopyDescription(const void *value)  { return (__bridge CFStringRef)[[(__bridge id)value description] copy]; }
static const void * __PSPDFObjectRetain(CFAllocatorRef alloc, const void *value) { return CFRetain(value); }
static void __PSPDFObjectRelease(CFAllocatorRef alloc, const void *value) { CFRelease(value); }
const CFSetCallBacks PSPDFNSObjectPointerSetCallBacks = { 0, __PSPDFObjectRetain, __PSPDFObjectRelease, __PSPDFObjectCopyDescription, NULL, NULL };
NSMutableSet *PSPDFCreateMutablePointerCompareSet(void) {
    return CFBridgingRelease(CFSetCreateMutable(kCFAllocatorDefault, 0, &PSPDFNSObjectPointerSetCallBacks));
}

inline bool PSPDFSizeIsEmpty(CGSize size) {
    return size.width == 0 || size.height == 0;
}
inline void PSPDFViewFrameSetX(UIView *view, CGFloat x) {
    CGRect frame = view.frame; frame.origin.x = x; view.frame = frame;
}
inline void PSPDFViewFrameSetY(UIView *view, CGFloat y) {
    CGRect frame = view.frame; frame.origin.y = y; view.frame = frame;
}
inline void PSPDFViewFrameSetWidth(UIView *view, CGFloat width) {
    CGRect frame = view.frame; frame.size.width = width; view.frame = frame;
}
inline void PSPDFViewFrameSetHeight(UIView *view, CGFloat height) {
    CGRect frame = view.frame; frame.size.height = height; view.frame = frame;
}

// Benchmark feature. Returns time in nanoseconds. (nsec/1E9 = seconds)
#include <stdint.h>
#include <mach/mach_time.h>
double PSPDFPerformAndTrackTime(dispatch_block_t block, BOOL trackTime) {
#ifndef DEBUG
    trackTime = NO;
#endif

    if (!trackTime) { block(); return 0; }

    static double ticksToNanoseconds = 0.0;
    uint64_t startTime = mach_absolute_time();
    block();
    uint64_t endTime = mach_absolute_time();

    // Elapsed time in mach time units
    uint64_t elapsedTime = endTime - startTime;

    // The first time we get here, ask the system
    // how to convert mach time units to nanoseconds
    if (0.0 == ticksToNanoseconds) {
        mach_timebase_info_data_t timebase;
        // to be completely pedantic, check the return code of this next call.
        mach_timebase_info(&timebase);
        ticksToNanoseconds = (double)timebase.numer / timebase.denom;
    }

    double elapsedTimeInNanoseconds = elapsedTime * ticksToNanoseconds;
    NSLog(@"seconds: %f", elapsedTimeInNanoseconds/1E9);
    return elapsedTimeInNanoseconds;
}


const char kPSPDFObjectTrackerToken;
@implementation PSPDFObjectTracker

+ (void)trackObject:(id)object withCallback:(PSPDFObjectTrackerCallback)callback {
    if (!object) return;
    
    PSPDFObjectTracker *tracker = objc_getAssociatedObject(object, &kPSPDFObjectTrackerToken);
    BOOL isCreatingNew = NO;
    if (!tracker || ![tracker isKindOfClass:[PSPDFObjectTracker class]]) {
        isCreatingNew = YES;
        tracker = [self new];
    }
    tracker.callback = callback;
    tracker.trackedObject = object;
    if (isCreatingNew) {
        objc_setAssociatedObject(object, &kPSPDFObjectTrackerToken, tracker, OBJC_ASSOCIATION_RETAIN);
    }
}

- (void)dealloc {
    if (_callback) _callback(_trackedObject);
}

@end

@implementation NSArray (PSPDFArrayAccess)
- (id)ps_firstObject {
    return [self count] ? self[0] : nil;
}
@end

@implementation PSPDFHangDetector
+ (void)startHangDetector {
#ifdef DEBUG
    NSThread *hangDetectionThread = [[NSThread alloc] initWithTarget:self selector:@selector(deadThreadMain) object:nil];
    [hangDetectionThread start];
#endif
}

#ifdef DEBUG
static volatile NSInteger DEAD_SIGNAL = 0;
+ (void)deadThreadTick {
    if (DEAD_SIGNAL == 1) {
        NSLog(@"Main Thread doesn't answer...");
    }
    DEAD_SIGNAL = 1;
    dispatch_async(dispatch_get_main_queue(), ^{DEAD_SIGNAL = 0;});
}

+ (void)deadThreadMain {
    [NSThread currentThread].name = @"PSPDFHangDetection";
    @autoreleasepool {
        [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(deadThreadTick) userInfo:nil repeats:YES];
        [[NSRunLoop currentRunLoop] runUntilDate:[NSDate distantFuture]];
    }
}
#endif

@end

#ifdef DEBUG
@implementation PSPDFFrameTrackingView
- (void)setFrame:(CGRect)frame {
    if (!CGRectEqualToRect(frame, self.frame)) {
        [super setFrame:frame];
        NSLog(@"Frame on %@ changed to %@", self, NSStringFromCGRect(frame));
    }
}
@end
#endif
