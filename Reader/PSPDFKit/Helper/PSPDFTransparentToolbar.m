//
//  PSPDFTransparentToolbar.m
//  PSPDFKit
//
//  Copyright 2011-2012 Peter Steinberger. All rights reserved.
//

#import "PSPDFTransparentToolbar.h"
#import "PSPDFKitGlobal.h"

@implementation PSPDFTransparentToolbar

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (id)init {
    if ((self = [super init])) {
        [self applyTranslucentBackground];
    }
	return self;
}

- (id)initWithFrame:(CGRect)frame {
    if ((self = [super initWithFrame:frame])) {
        [self applyTranslucentBackground];
    }
	return self;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UIVIew

// Override draw rect to avoid background coloring
- (void)drawRect:(CGRect)rect {
    // do nothing in here
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Private

// Set properties to make background translucent.
- (void)applyTranslucentBackground {
	self.backgroundColor = [UIColor clearColor];
	self.opaque = NO;
	self.translucent = YES;
}

@end
