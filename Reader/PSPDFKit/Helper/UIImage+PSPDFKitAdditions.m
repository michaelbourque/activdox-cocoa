//
//  UIImage+PSPDFKitAdditions.m
//  PSPDFKit
//
//  Copyright 2011-2012 Peter Steinberger. All rights reserved.
//

#import "PSPDFKit.h"
#import "UIImage+PSPDFKitAdditions.h"
#import <ImageIO/ImageIO.h>

@implementation UIImage (PSPDFKitAdditions)

// Some code here is based on MGImageUtilities and properly licensed to allow a non-attribution license.
- (UIImage *)pspdf_imageToFitSize:(CGSize)fitSize method:(PSPDFImageResizingMethod)resizeMethod honorScaleFactor:(BOOL)honorScaleFactor opaque:(BOOL)opaque {
	float imageScaleFactor = 1.f;
    if (honorScaleFactor) {
        imageScaleFactor = [self scale];
    }

    float sourceWidth = [self size].width * imageScaleFactor;
    float sourceHeight = [self size].height * imageScaleFactor;
    float targetWidth = fitSize.width;
    float targetHeight = fitSize.height;
    BOOL cropping = !(resizeMethod == PSPDFImageResizeScale);

    // adapt rect based on source image size
    switch (self.imageOrientation) {
        case UIImageOrientationLeft:    // button top
        case UIImageOrientationRight:   // button bottom
        case UIImageOrientationLeftMirrored:
        case UIImageOrientationRightMirrored: {
            ps_swapf(sourceWidth, sourceHeight);
            ps_swapf(targetWidth, targetHeight);
        }break;

        case UIImageOrientationUp:     // button left
        case UIImageOrientationDown:   // button right
        default: {             // works in default
        }break;
    }

    // Calculate aspect ratios
    float sourceRatio = sourceWidth / sourceHeight;
    float targetRatio = targetWidth / targetHeight;

    // Determine what side of the source image to use for proportional scaling
    BOOL scaleWidth = (sourceRatio <= targetRatio);
    // Deal with the case of just scaling proportionally to fit, without cropping
    scaleWidth = (cropping) ? scaleWidth : !scaleWidth;

    // Proportionally scale source image
    CGFloat scalingFactor, scaledWidth, scaledHeight;
    if (scaleWidth) {
        scalingFactor = 1.f / sourceRatio;
        scaledWidth = targetWidth;
        scaledHeight = round(targetWidth * scalingFactor);
    } else {
        scalingFactor = sourceRatio;
        scaledWidth = round(targetHeight * scalingFactor);
        scaledHeight = targetHeight;
    }
    float scaleFactor = scaledHeight / sourceHeight;

    // Calculate compositing rectangles
    CGRect sourceRect, destRect;
    if (cropping) {
        destRect = CGRectMake(0, 0, targetWidth, targetHeight);
        float destX = 0, destY = 0;
        if (resizeMethod == PSPDFImageResizeCrop) {
            // Crop center
            destX = round((scaledWidth - targetWidth) / 2.f);
            destY = round((scaledHeight - targetHeight) / 2.f);
        } else if (resizeMethod == PSPDFImageResizeCropStart) {
            // Crop top or left (prefer top)
            if (scaleWidth) {
                // Crop top
                destX = 0.f;
                destY = 0.f;
            } else {
                // Crop left
                destX = 0.f;
                destY = round((scaledHeight - targetHeight) / 2.f);
            }
        } else if (resizeMethod == PSPDFImageResizeCropEnd) {
            // Crop bottom or right
            if (scaleWidth) {
                // Crop bottom
                destX = round((scaledWidth - targetWidth) / 2.f);
                destY = round(scaledHeight - targetHeight);
            } else {
                // Crop right
                destX = round(scaledWidth - targetWidth);
                destY = round((scaledHeight - targetHeight) / 2.f);
            }
        }
        sourceRect = CGRectMake(destX / scaleFactor, destY / scaleFactor,
                                targetWidth / scaleFactor, targetHeight / scaleFactor);
    } else {
        sourceRect = CGRectMake(0.f, 0.f, sourceWidth, sourceHeight);
        destRect = CGRectMake(0.f, 0.f, scaledWidth, scaledHeight);
    }

    // Create appropriately modified image.
    UIImage *image = nil;
    UIGraphicsBeginImageContextWithOptions(destRect.size, opaque, honorScaleFactor ? 0.f : 1.f); // 0.0f for scale means "correct scale for device's main screen".
    CGImageRef sourceImg = CGImageCreateWithImageInRect([self CGImage], sourceRect); // cropping happens here.
    image = [UIImage imageWithCGImage:sourceImg scale:0.f orientation:self.imageOrientation]; //  create cropped UIImage.
    //PSELog(@"image size: %@", NSStringFromCGSize(image.size));
    [image drawInRect:destRect]; // the actual scaling happens here, and orientation is taken care of automatically.
    CGImageRelease(sourceImg);
    image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();

    return image;
}

- (id)initWithContentsOfResolutionIndependentFile_pspdf:(NSString *)path {
    if ([path pathExtension].length == 0) {
        path = [path stringByAppendingString:@".png"];
    }

    if ([UIScreen mainScreen].scale > 1) {
        NSString *path2x = [[path stringByDeletingLastPathComponent]
                            stringByAppendingPathComponent:[NSString stringWithFormat:@"%@@2x.%@",
                                                            [[path lastPathComponent] stringByDeletingPathExtension],
                                                            [path pathExtension]]];

        if ([[NSFileManager defaultManager] fileExistsAtPath:path2x]) {
            return [self initWithContentsOfFile:path2x];
        }
    }

    return [self initWithContentsOfFile:path];
}

+ (UIImage *)pspdf_animatedGIFWithPath:(NSString *)path {
    // be forgiving and add gif if missing
    if ([path pathExtension].length == 0) {
        path = [path stringByAppendingString:@".gif"];
    }

    if ([UIScreen mainScreen].scale > 1) {
        NSString *path2x = [[path stringByDeletingLastPathComponent]
                            stringByAppendingPathComponent:[NSString stringWithFormat:@"%@@2x.%@",
                                                            [[path lastPathComponent] stringByDeletingPathExtension],
                                                            [path pathExtension]]];

        NSData *data = [NSData dataWithContentsOfFile:path2x];
        if (data) {
            return [UIImage pspdf_animatedGIFWithData:data];
        }
    }
    // fallback
    NSData *data = [NSData dataWithContentsOfFile:path];
    if (data) {
        return [UIImage pspdf_animatedGIFWithData:data];
    }

    // last resort; try regular image loading
    return [[UIImage alloc] initWithContentsOfResolutionIndependentFile_pspdf:path];
}

// compatible with iOS5 upwards.
+ (UIImage *)pspdf_animatedGIFWithData:(NSData *)data {
    CGImageSourceRef source = CGImageSourceCreateWithData((__bridge CFDataRef)data, NULL);

    // get properties
    NSDictionary *properties = CFBridgingRelease(CGImageSourceCopyProperties(source, NULL));
    NSDictionary *gifProperties = properties[(NSString *)kCGImagePropertyGIFDictionary];

    // get frames
    size_t count = CGImageSourceGetCount(source);
    NSMutableArray *images = [NSMutableArray array];

    // build image array
    for (size_t i = 0; i < count; i++) {
        CGImageRef image = CGImageSourceCreateImageAtIndex(source, i, NULL);
        [images addObject:[UIImage imageWithCGImage:image]];
        CGImageRelease(image);
    }

    // get duration
    NSTimeInterval duration = [gifProperties[(NSString *)kCGImagePropertyGIFDelayTime] doubleValue];
    if (!duration) {
        duration = (1.0f/10.0f)*count;
    }
    CFRelease(source);

    // finally create image
    return [UIImage animatedImageWithImages:images duration:duration];
}

CGColorSpaceRef pspdf_colorSpace;
__attribute__((constructor)) static void initialize_colorSpace() {
    pspdf_colorSpace = CGColorSpaceCreateDeviceRGB();
}
__attribute__((destructor)) static void destroy_colorSpace() {
    CFRelease(pspdf_colorSpace);
}

// advanced trickery: http://stackoverflow.com/questions/5266272/non-lazy-image-loading-in-ios
+ (UIImage *)pspdf_preloadedImageWithContentsOfFile:(NSString *)path {

    // this *really* loads the image (imageWithContentsOfFile is lazy)
    CGImageRef image = NULL;
    CGDataProviderRef dataProvider = CGDataProviderCreateWithFilename([path cStringUsingEncoding:NSUTF8StringEncoding]);
    if (!dataProvider) {
        PSPDFLogWarning(@"Could not open %@!", path);
        return nil;
    }

    if ([[path lowercaseString] hasSuffix:@"jpg"]) {
        image = CGImageCreateWithJPEGDataProvider(dataProvider, NULL, false, kCGRenderingIntentDefault);
    }else {
        image = CGImageCreateWithPNGDataProvider(dataProvider, NULL, false, kCGRenderingIntentDefault);
    }
    CGDataProviderRelease(dataProvider);

    // make a bitmap context of a suitable size to draw to, forcing decode
    size_t width = CGImageGetWidth(image);
    size_t height = CGImageGetHeight(image);

    CGContextRef imageContext =  CGBitmapContextCreate(NULL, width, height, 8, width*4, pspdf_colorSpace, kCGImageAlphaNoneSkipLast | kCGBitmapByteOrder32Little);
    if (!imageContext) {
        PSPDFLogWarning(@"Failed to create image context.");
        CGImageRelease(image);
        return nil;
    }

    // draw the image to the context, release it
    CGContextDrawImage(imageContext, CGRectMake(0.f, 0.f, width, height), image);
    CGImageRelease(image);

    // now get an image ref from the context
    CGImageRef outputImage = CGBitmapContextCreateImage(imageContext);
    UIImage *cachedImage = [UIImage imageWithCGImage:outputImage scale:[UIScreen mainScreen].scale orientation:UIImageOrientationUp];

    // clean up
    CGImageRelease(outputImage);
    CGContextRelease(imageContext);
    return cachedImage;
}

- (UIImage *)pspdf_preloadedImage {
    CGImageRef imageRef = self.CGImage;

    // make a bitmap context of a suitable size to draw to, forcing decode
    size_t width = CGImageGetWidth(imageRef);
    size_t height = CGImageGetHeight(imageRef);

    CGContextRef imageContext = CGBitmapContextCreate(NULL, width, height, 8, width * 4, CGImageGetColorSpace(imageRef), kCGImageAlphaPremultipliedFirst | kCGBitmapByteOrder32Little);

    // draw the image to the context, release it
    CGContextDrawImage(imageContext, CGRectMake(0, 0, width, height), imageRef);

    // now get an image ref from the context
    CGImageRef outputImage = CGBitmapContextCreateImage(imageContext);
    UIImage *cachedImage = [UIImage imageWithCGImage:outputImage scale:[UIScreen mainScreen].scale orientation:UIImageOrientationUp];

    // clean up
    CGImageRelease(outputImage);
    CGContextRelease(imageContext);
    return cachedImage;
}

+ (UIImage *)pspdf_preloadedImageWithData:(NSData *)data {
    UIImage *cachedImage = [[UIImage imageWithData:data] pspdf_preloadedImage];
    return cachedImage;
}

- (UIImage *)pdpdf_imageTintedWithColor:(UIColor *)color fraction:(CGFloat)fraction {
	if (color) {
        CGRect rect = (CGRect){CGPointZero, self.size};
        UIGraphicsBeginImageContextWithOptions(self.size, NO, 0.0);
		[color set];
		UIRectFill(rect);
		[self drawInRect:rect blendMode:kCGBlendModeDestinationIn alpha:1.0];

		if (fraction > 0.0) {
			[self drawInRect:rect blendMode:kCGBlendModeSourceAtop alpha:fraction];
		}
		UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
		UIGraphicsEndImageContext();
		return image;
	}
	return self;
}

@end

PSPDF_FIX_CATEGORY_BUG(UIImagePSPDFKitAdditions)
