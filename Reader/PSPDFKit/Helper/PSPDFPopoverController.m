//
//  PSPDFPopoverController.m
//  PSPDFKit
//
//  Copyright (c) 2012 Peter Steinberger. All rights reserved.
//

#import "PSPDFPopoverController.h"

@implementation PSPDFPopoverController

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public

- (void)notifyContentControllerAboutDismissalAnimated:(BOOL)animated {
    // find topmost viewController and send message
    UIViewController *contentController = self.contentViewController;
    if ([contentController isKindOfClass:[UINavigationController class]]) {
        contentController = ((UINavigationController *)contentController).topViewController;
    }
    if ([contentController conformsToProtocol:@protocol(PSPDFPopoverControllerDismissable)]) {
        [(id<PSPDFPopoverControllerDismissable>)(contentController) willDismissPopover:self animated:animated];
    }
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Private

- (void)dismissPopoverAnimated:(BOOL)animated {
    [self notifyContentControllerAboutDismissalAnimated:animated];
    [super dismissPopoverAnimated:animated];
}

@end
