//
//  PSPDFNavigationAppearanceSnapshot.m
//  PSPDFKit
//
//  Copyright (c) 2012 Peter Steinberger. All rights reserved.
//

#import "PSPDFNavigationAppearanceSnapshot.h"
#import "PSPDFKitGlobal.h"
#import <QuartzCore/QuartzCore.h>

@interface PSPDFNavigationAppearanceSnapshot()

/// UINavigationBar's hidden property.
@property (nonatomic, readonly, assign) BOOL navBarHidden;

/// UINavigationBar's translucent property.
@property (nonatomic, readonly, assign) BOOL navBarTranslucent;

/// UINavigationBar's alpha property.
@property (nonatomic, readonly, assign) CGFloat navBarAlpha;

/// UINavigationBar's barStyle property.
@property (nonatomic, readonly, assign) UIBarStyle navBarStyle;

/// UIApplication's statusBarStyle property.
@property (nonatomic, readonly, assign) UIStatusBarStyle statusBarStyle;

/// UIApplication's statusBarHidden property.
@property (nonatomic, readonly, assign) BOOL statusBarHidden;

/// UINavigationBar's tintColor property.
@property (nonatomic, readonly, strong) UIColor *navBarTintColor;

/// UINavigationBar's UIBarMetricsDefault backgroundImage property.
@property (nonatomic, readonly, strong) UIImage *navBarDefaultImage;

/// UINavigationBar's UIBarMetricsLandscapePhone backgroundImage property.
@property (nonatomic, readonly, strong) UIImage* navBarLandscapePhoneImage;

@end

@implementation PSPDFNavigationAppearanceSnapshot

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (id)initForNavigationController:(UINavigationController *)navigationController {
    if ((self = [super init])) {
        // statusbar
        _statusBarStyle = [UIApplication sharedApplication].statusBarStyle;
        _statusBarHidden = [UIApplication sharedApplication].statusBarHidden;

        // navbar
        _navBarHidden = navigationController.navigationBarHidden;
        _navBarStyle = navigationController.navigationBar.barStyle;
        _navBarTranslucent = navigationController.navigationBar.translucent;
        _navBarAlpha = navigationController.navigationBar.alpha;
        _navBarTintColor = navigationController.navigationBar.tintColor;

        _navBarDefaultImage = [navigationController.navigationBar
                               backgroundImageForBarMetrics:UIBarMetricsDefault];
        _navBarLandscapePhoneImage = [navigationController.navigationBar
                                      backgroundImageForBarMetrics:UIBarMetricsLandscapePhone];
    }
    return self;
}

- (void)restoreStatusBarStateAnimated:(BOOL)animated {
    [self restoreStatusBarStateWithAnimation:animated ? UIStatusBarAnimationFade : UIStatusBarAnimationNone];
}

- (void)restoreStatusBarStateWithAnimation:(UIStatusBarAnimation)animation {
    [[UIApplication sharedApplication] setStatusBarStyle:self.statusBarStyle animated:animation != UIStatusBarAnimationNone];
    [[UIApplication sharedApplication] setStatusBarHidden:self.statusBarHidden withAnimation:animation];
}

- (BOOL)shouldAnimateAgainstNavigationController:(UINavigationController *)navigationController {
    return navigationController.navigationBar.barStyle != _navBarStyle || navigationController.navigationBar.translucent != self.navBarTranslucent;
}

- (void)restoreForNavigationController:(UINavigationController *)navigationController animated:(BOOL)animated {
    // hide/unhide navBar
    [navigationController setNavigationBarHidden:self.navBarHidden animated:animated];

    // simple fade animation - UIKit doesn't allow animated property changing on the navigation bar.
    if (animated) {
        [navigationController.navigationBar.layer addAnimation:PSPDFFadeTransition() forKey:nil];
    }

    // this might produce a frame change notification
    navigationController.navigationBar.barStyle = self.navBarStyle;

    navigationController.navigationBar.translucent = self.navBarTranslucent;
    navigationController.navigationBar.alpha = self.navBarAlpha;
    navigationController.navigationBar.tintColor = self.navBarTintColor;

    [navigationController.navigationBar setBackgroundImage:self.navBarDefaultImage
                                             forBarMetrics:UIBarMetricsDefault];
    [navigationController.navigationBar setBackgroundImage:self.navBarLandscapePhoneImage
                                             forBarMetrics:UIBarMetricsLandscapePhone];
}

@end
