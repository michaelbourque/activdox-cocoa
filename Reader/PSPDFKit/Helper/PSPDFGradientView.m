//
//  PSPDFGradientView.m
//  PSPDFKit
//
//  Copyright (c) 2012 Peter Steinberger. All rights reserved.
//  This class is inspired by SSToolkit from Sam Soffes (MIT licensed). Thanks, Sam!
//

#import "PSPDFGradientView.h"
#import <QuartzCore/QuartzCore.h>

@implementation PSPDFGradientView {
    CGGradientRef _gradient;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (void)commonInit {
    self.layer.needsDisplayOnBoundsChange = YES; // ensure we redraw the background after frame change.
    _direction = PSPDFGradientViewDirectionVertical;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
	if ((self = [super initWithCoder:aDecoder])) {
        [self commonInit];
	}
	return self;
}

- (id)initWithFrame:(CGRect)frame {
	if ((self = [super initWithFrame:frame])) {
        self.backgroundColor = [UIColor clearColor];
        [self commonInit];
	}
	return self;
}

- (void)dealloc {
	CGGradientRelease(_gradient);
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UIView

- (void)drawRect:(CGRect)rect {
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextAddPath(context, [UIBezierPath bezierPathWithRoundedRect:rect cornerRadius:self.layer.cornerRadius].CGPath);
    CGContextClip(context);

	if (_gradient) {
		CGPoint start = CGPointMake(0.f, 0.f);
		CGPoint end = (_direction == PSPDFGradientViewDirectionVertical ? CGPointMake(0.f, rect.size.height) : CGPointMake(rect.size.width, 0.f));
		CGContextDrawLinearGradient(context, _gradient, start, end, 0);
	}
    // add simple code path for one color
    else if ([self.colors count] == 1) {
        [[self.colors ps_firstObject] setFill];
        CGContextFillRect(context, rect);
    }

	[super drawRect:rect];
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Properties

- (void)setColors:(NSArray *)colors {
	_colors = colors;
	[self refreshGradient];
}

- (void)setLocations:(NSArray *)locations {
	_locations = locations;
	[self refreshGradient];
}

- (void)setDirection:(PSPDFGradientViewDirection)direction {
	_direction = direction;
	[self setNeedsDisplay];
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Private

- (void)refreshGradient {
	CGGradientRelease(_gradient); _gradient = nil;

	NSUInteger colorsCount = [_colors count];
	if (colorsCount < 2) return;

	CGColorSpaceRef colorSpace = CGColorGetColorSpace([_colors[0] CGColor]);

	CGFloat *gradientLocations = NULL;
	NSUInteger locationsCount = [_locations count];
	if (locationsCount == colorsCount) {
		gradientLocations = (CGFloat *)malloc(sizeof(CGFloat) * locationsCount);
		for (NSUInteger i = 0; i < locationsCount; i++) {
			gradientLocations[i] = [_locations[i] floatValue];
		}
	}

	NSMutableArray *gradientColors = [[NSMutableArray alloc] initWithCapacity:colorsCount];
	[_colors enumerateObjectsUsingBlock:^(id object, NSUInteger index, BOOL *stop) {
		[gradientColors addObject:(id)[(UIColor *)object CGColor]];
	}];

	_gradient = CGGradientCreateWithColors(colorSpace, (__bridge CFArrayRef)gradientColors, gradientLocations);

	if (gradientLocations) free(gradientLocations);

	[self setNeedsDisplay];
}

@end
