//
//  UIColor+PSPDFKitAdditions.m
//  PSPDFKit
//
//  Copyright (c) 2011-2012 Peter Steinberger. All rights reserved.
//

#import "UIColor+PSPDFKitAdditions.h"
#import "PSPDFConverter.h"
#import <QuartzCore/QuartzCore.h>

@implementation UIColor (PSPDFKitAdditions)

- (id)initWithCGPDFDictionary:(CGPDFDictionaryRef)dictionary {
    CGPDFArrayRef colorArray = NULL;
    if (CGPDFDictionaryGetArray(dictionary, "C", &colorArray)) {
        self = [self initWithCGPDFArray:colorArray];
    }else {
        self = nil;
    }
    return self;
}

- (id)initWithCGPDFArray:(CGPDFArrayRef)arrayRef {
//    The number of array elements determines the color space in which the color is defined:
//    0 No color; transparent
//    1 DeviceGray
//    3 DeviceRGB
//    4 DeviceCMYK
    
    // if we have a valid arg,
    if (arrayRef) {
        size_t count = CGPDFArrayGetCount(arrayRef);
        
        if (count == 0) {
            self = [self initWithWhite:0.0 alpha:0.0];
            
        } else if (count == 1) {
            CGPDFReal real;
            if (CGPDFArrayGetNumber(arrayRef, 0, &real)) {
                self = [self initWithWhite:real alpha:0.0];
            } else {
                // likely an invalid array
                self = nil;
            }
            
        } else if ((count == 3) || (count == 4)) {
            CGPDFReal components[count];
            size_t i;
            
            for (i = 0; i < count; i++) {
                if (!CGPDFArrayGetNumber(arrayRef, i, components + i)) {
                    // ruh roh, likely a malformed array
                    break;
                }
            }
            
            //if the real array was processed entirely,
            if (i == count) {
                // if rgb color,
                if (i == 3) {
                    self = [self initWithRed:components[0] green:components[1] blue:components[2] alpha:1.0];
                    
                } else if (i == 4) {
                    // don't have a CMYK convenience intializer so we need to invoke CGColor* gods
                    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceCMYK();
                    CGColorRef colorRef = CGColorCreate(colorSpace, components);
                    self = [self initWithCGColor:colorRef];
                    CGColorRelease(colorRef);
                    CGColorSpaceRelease(colorSpace);
                    
                } else {
                    // invalid argument
                    self = nil;
                }
            } else {
                //invalid argument
                self = nil;
            }
        } else {
            self = nil;
        }
    } else {
        // no argument implies no color; transparent
        self = [self initWithWhite:0.0 alpha:0.0];
    }
    
    return self;
}

- (BOOL)pspdf_canProvideRGBComponents {
	switch (self.pspdf_colorSpaceModel) {
		case kCGColorSpaceModelRGB:
		case kCGColorSpaceModelMonochrome:
			return YES;
		default:
			return NO;
	}
}

- (CGColorSpaceModel)pspdf_colorSpaceModel {
	return CGColorSpaceGetModel(CGColorGetColorSpace(self.CGColor));
}

- (BOOL)pspdf_canProvideRGBColor {
	return (([self pspdf_colorSpaceModel] == kCGColorSpaceModelRGB) || ([self pspdf_colorSpaceModel] == kCGColorSpaceModelMonochrome));
}

- (CGFloat)pspdf_redComponent {
	NSAssert ([self pspdf_canProvideRGBColor], @"Must be a RGB color to use -red, -green, -blue");
	const CGFloat *c = CGColorGetComponents(self.CGColor);
	return c[0];
}

- (CGFloat)pspdf_greenComponent {
	NSAssert ([self pspdf_canProvideRGBColor], @"Must be a RGB color to use -red, -green, -blue");
	const CGFloat *c = CGColorGetComponents(self.CGColor);
	if ([self pspdf_colorSpaceModel] == kCGColorSpaceModelMonochrome) return c[0];
	return c[1];
}

- (CGFloat)pspdf_blueComponent {
	NSAssert ([self pspdf_canProvideRGBColor], @"Must be a RGB color to use -red, -green, -blue");
	const CGFloat *c = CGColorGetComponents(self.CGColor);
	if ([self pspdf_colorSpaceModel] == kCGColorSpaceModelMonochrome) return c[0];
	return c[2];
}

- (CGFloat)pspdf_alphaComponent {
	const CGFloat *c = CGColorGetComponents(self.CGColor);
	return c[CGColorGetNumberOfComponents(self.CGColor)-1];
}

- (CGFloat)pspdf_whiteComponent {
	NSAssert([self pspdf_colorSpaceModel] == kCGColorSpaceModelMonochrome, @"Must be a Monochrome color to use -white");
	const CGFloat *c = CGColorGetComponents(self.CGColor);
	return c[0];
}

- (UIColor *)pspdf_colorByMultiplyingBy:(CGFloat)f {
	return [self pspdf_colorByMultiplyingByRed:f green:f blue:f alpha:1.0f];
}

- (BOOL)red:(CGFloat *)red green:(CGFloat *)green blue:(CGFloat *)blue alpha:(CGFloat *)alpha {
	const CGFloat *components = CGColorGetComponents(self.CGColor);

	CGFloat r,g,b,a;

	switch (self.pspdf_colorSpaceModel) {
		case kCGColorSpaceModelMonochrome:
			r = g = b = components[0];
			a = components[1];
			break;
		case kCGColorSpaceModelRGB:
			r = components[0];
			g = components[1];
			b = components[2];
			a = components[3];
			break;
		default:	// We don't know how to handle this model
			return NO;
	}

	if (red) *red = r;
	if (green) *green = g;
	if (blue) *blue = b;
	if (alpha) *alpha = a;

	return YES;
}

- (UIColor *)pspdf_colorByMultiplyingByRed:(CGFloat)red green:(CGFloat)green blue:(CGFloat)blue alpha:(CGFloat)alpha {
	NSAssert(self.pspdf_canProvideRGBComponents, @"Must be a RGB color to use arithmatic operations");

	CGFloat r,g,b,a;
	if (![self red:&r green:&g blue:&b alpha:&a]) return nil;

	return [UIColor colorWithRed:fmaxf(0.0, fminf(1.0, r * red))
						   green:fmaxf(0.0, fminf(1.0, g * green))
							blue:fmaxf(0.0, fminf(1.0, b * blue))
						   alpha:fmaxf(0.0, fminf(1.0, a * alpha))];
}

- (NSString *)pspdf_closestAnnotationColorName {
	NSDictionary *localizedColorNames = @{@"hl-yellow": PSPDFLocalize(@"yellow"),
    @"hl-green": PSPDFLocalize(@"green"),
    @"hl-blue": PSPDFLocalize(@"blue"),
    @"hl-pink": PSPDFLocalize(@"pink"),

    @"note-yellow": PSPDFLocalize(@"yellow"),
    @"note-orange": PSPDFLocalize(@"orange"),
    @"note-red": PSPDFLocalize(@"red"),
    @"note-purple": PSPDFLocalize(@"purple"),
    @"note-blue": PSPDFLocalize(@"blue"),
    @"note-green": PSPDFLocalize(@"green"),

    @"ul-green": PSPDFLocalize(@"green"),
    @"ul-blue": PSPDFLocalize(@"blue"),
    @"ul-red": PSPDFLocalize(@"red")};

	NSDictionary *colorNames = @{@"hl-yellow": [UIColor colorWithRed:0.988 green:0.929 blue:0.549 alpha:1.000],
    @"hl-green": [UIColor colorWithRed:0.812 green:0.961 blue:0.651 alpha:1.0],
    @"hl-blue": [UIColor colorWithRed:0.761 green:0.871 blue:1.000 alpha:1.000],
    @"hl-pink": [UIColor colorWithRed:1.000 green:0.749 blue:1.000 alpha:1.000],

    // note colors
    @"note-yellow": [UIColor colorWithRed:0.95 green:0.87 blue:0.28 alpha:1.000],
    @"note-orange": [UIColor colorWithRed:1.000 green:0.5 blue:0.000 alpha:1.0],
    @"note-red": [UIColor colorWithRed:1.0 green:0.200 blue:0.000 alpha:1.000],
    @"note-purple": [UIColor colorWithRed:0.9 green:0.200 blue:1.000 alpha:1.000],
    @"note-blue": [UIColor colorWithRed:0.000 green:0.600 blue:1.000 alpha:1.000],
    @"note-green": [UIColor colorWithRed:0.45 green:1.000 blue:0.000 alpha:1.000],

    // underline colors
    @"ul-green": [UIColor colorWithRed:0.102 green:0.702 blue:0.349 alpha:1.0],
    @"ul-blue": [UIColor colorWithRed:0.000 green:0.251 blue:1.000 alpha:1.0],
    @"ul-red": [UIColor colorWithRed:0.969 green:0.380 blue:0.329 alpha:1.0],

    // standard colors
    PSPDFLocalize(@"red"): [UIColor colorWithRed:1.000 green:0.000 blue:0.000 alpha:1.000],
    PSPDFLocalize(@"green"): [UIColor colorWithRed:0.000 green:1.000 blue:0.000 alpha:1.000],
    PSPDFLocalize(@"blue"): [UIColor colorWithRed:0.000 green:0.000 blue:1.000 alpha:1.000],
    PSPDFLocalize(@"yellow"): [UIColor colorWithRed:1.000 green:1.000 blue:0.000 alpha:1.000],
    PSPDFLocalize(@"orange"): [UIColor colorWithRed:1.000 green:0.502 blue:0.000 alpha:1.000],
    PSPDFLocalize(@"cyan"): [UIColor colorWithRed:0.000 green:1.000 blue:1.000 alpha:1.000],
    PSPDFLocalize(@"pink"): [UIColor colorWithRed:1.000 green:0.000 blue:1.000 alpha:1.000],
    PSPDFLocalize(@"gray"): [UIColor colorWithRed:0.500 green:0.500 blue:0.500 alpha:1.000],
    PSPDFLocalize(@"white"): [UIColor colorWithRed:1.000 green:1.000 blue:1.000 alpha:1.000],
    PSPDFLocalize(@"black"): [UIColor colorWithRed:0.000 green:0.000 blue:0.000 alpha:1.000]};
	NSString *closestColorName = [self pspdf_closestColorNameFromSelection:colorNames];
	if (localizedColorNames[closestColorName]) {
		closestColorName = localizedColorNames[closestColorName];
	}
	return closestColorName;
}

- (NSString *)pspdf_closestColorNameFromSelection:(NSDictionary *)colorNames {
	const CGFloat *components = CGColorGetComponents(self.CGColor);
	CGFloat rRef = components[0];
	CGFloat gRef = components[1];
	CGFloat bRef = components[2];
	float minRGBDistance = MAXFLOAT;
	NSString *minDistanceColorName = nil;
	for (NSString *colorName in colorNames) {
		UIColor *color = colorNames[colorName];
		const CGFloat *otherComponents = CGColorGetComponents(color.CGColor);
		CGFloat r = otherComponents[0];
		CGFloat g = otherComponents[1];
		CGFloat b = otherComponents[2];
		float hRef, sRef, vRef;
		PSPDFRGBtoHSV(r, g, b, &hRef, &sRef, &vRef);
		float rDist = fabsf(r - rRef);
		float gDist = fabsf(g - gRef);
		float bDist = fabsf(b - bRef);
		float rgbDist = sqrtf(rDist * rDist + gDist * gDist + bDist * bDist);
		if (rgbDist < minRGBDistance) {
			minDistanceColorName = colorName;
			minRGBDistance = rgbDist;
		}
	}
	return minDistanceColorName;
}

+ (CGFloat)pspdf_selectionAlpha {
    return 0.2;
}

+ (UIColor *)pspdf_selectionColor {
    float selectionAlpha = [self pspdf_selectionAlpha];
    UIColor *selectionColor = [UIColor colorWithRed:(0.8 - (1.0-selectionAlpha)) / selectionAlpha
                                              green:(0.87 - (1.0-selectionAlpha)) / selectionAlpha
                                               blue:(0.93 - (1.0-selectionAlpha)) / selectionAlpha
                                              alpha:1.0];
    return selectionColor;
}

+ (UIColor *)pspdf_colorWithHexString:(NSString *)stringToConvert {
    if (!stringToConvert) return nil;
	NSScanner *scanner = [NSScanner scannerWithString:stringToConvert];
	unsigned hexNum;
	if (![scanner scanHexInt:&hexNum]) return nil;
	return [UIColor pspdf_colorWithRGBHex:hexNum allowTransparancy:NO];
}

+ (UIColor *)pspdf_colorWithRGBHex:(UInt32)hex allowTransparancy:(BOOL)allowTransparancy {
    int a = allowTransparancy ? (hex >> 24) & 0xFF : 255.f;
    int r = (hex >> 16) & 0xFF;
    int g = (hex >> 8) & 0xFF;
    int b = (hex) & 0xFF;
    return [UIColor colorWithRed:r / 255.0f green:g / 255.0f blue:b / 255.0f alpha:a / 255.0f];
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Property List support

+ (UIColor *)pspdf_colorFromPropertyRepresentation:(id)colorObject {
	UIColor *color = nil;
	if ([colorObject isKindOfClass:[NSString class]]) {
		color = [UIColor pspdf_colorFromString:colorObject];
		if (!color) {
			color = [UIColor pspdf_colorFromWebColorString:colorObject];
		}
	} else if ([colorObject isKindOfClass:[UIColor class]]){
		color = colorObject;
	}
	return color;
}

- (id)pspdf_propertyRepresentation {
	NSString *colorString = [self pspdf_stringFromColor];
	if (colorString) return colorString;

	return nil;
}

- (NSString *)pspdf_webColorString {
	if (![self pspdf_canProvideRGBColor]) return nil;

	return [NSString stringWithFormat:@"#%02X%02X%02X", ((NSUInteger)([self pspdf_redComponent] * 255)),
			((NSUInteger)([self pspdf_greenComponent] * 255)), ((NSUInteger)([self pspdf_blueComponent] * 255))];
}

- (NSString *)pspdf_stringFromColor {
	NSAssert ([self pspdf_canProvideRGBColor], @"Must be a RGB color to use -red, -green, -blue");

	NSString *result;
	switch ([self pspdf_colorSpaceModel]) {
		case kCGColorSpaceModelRGB:
			result = [NSString stringWithFormat:@"{%0.3f, %0.3f, %0.3f, %0.3f}", [self pspdf_redComponent], [self pspdf_greenComponent], [self pspdf_blueComponent], [self pspdf_alphaComponent]];
			break;
		case kCGColorSpaceModelMonochrome:
			result = [NSString stringWithFormat:@"{%0.3f, %0.3f}", [self pspdf_whiteComponent], [self pspdf_alphaComponent]];
			break;
		default:
			result = nil;
	}
	return result;
}

+ (UIColor *)pspdf_colorFromString:(NSString *)colorString {
	NSScanner *scanner = [NSScanner scannerWithString:colorString];
	if (![scanner scanString:@"{" intoString:NULL]) return nil;
	const NSUInteger kMaxComponents = 4;
	CGFloat c[kMaxComponents];
	NSUInteger i = 0;
	if (![scanner scanFloat:&c[i++]]) return nil;
	while (1) {
		if ([scanner scanString:@"}" intoString:NULL]) break;
		if (i >= kMaxComponents) return nil;
		if ([scanner scanString:@"," intoString:NULL]) {
			if (![scanner scanFloat:&c[i++]]) return nil;
		} else {
			// either we're at the end of there's an unexpected character here
			// both cases are error conditions
			return nil;
		}
	}
	if (![scanner isAtEnd]) return nil;
	UIColor *color;
	switch (i) {
		case 2: // monochrome
			color = [UIColor colorWithWhite:c[0] alpha:c[1]];
			break;
		case 4: // RGB
			color = [UIColor colorWithRed:c[0] green:c[1] blue:c[2] alpha:c[3]];
			break;
		default:
			color = nil;
	}
	return color;
}

+ (UIColor *)pspdf_colorFromWebColorString:(NSString *)colorString {
	NSUInteger length = [colorString length];
	if (length > 0) {
		// remove prefixed #
		colorString = [colorString stringByTrimmingCharactersInSet: [NSCharacterSet characterSetWithCharactersInString:@"#"]];
		length = [colorString length];

		// calculate substring ranges of each color
		// FFF or FFFFFF
		NSRange redRange, blueRange, greenRange;
		if (length == 3) {
			redRange = NSMakeRange(0, 1);
			greenRange = NSMakeRange(1, 1);
			blueRange = NSMakeRange(2, 1);
		} else if (length == 6) {
			redRange = NSMakeRange(0, 2);
			greenRange = NSMakeRange(2, 2);
			blueRange = NSMakeRange(4, 2);
		} else {
			return nil;
		}

		// extract colors
		NSUInteger redComponent, greenComponent, blueComponent;
		BOOL valid = YES;
		NSScanner *scanner = [NSScanner scannerWithString:[colorString substringWithRange:redRange]];
		valid = [scanner scanHexInt:&redComponent];

		scanner = [NSScanner scannerWithString:[colorString substringWithRange:greenRange]];
		valid = ([scanner scanHexInt:&greenComponent] && valid);

		scanner = [NSScanner scannerWithString:[colorString substringWithRange:blueRange]];
		valid = ([scanner scanHexInt:&blueComponent] && valid);

		if (valid) {
			return [UIColor colorWithRed:redComponent/255.0 green:greenComponent/255.0 blue:blueComponent/255.0 alpha:1.0f];
		}
	}
	return nil;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Derived Colors

- (UIColor *)pspdf_lightenedColor {
    CGFloat delta = 0.1f;
    if (![self pspdf_canProvideRGBColor]) return self;

    CGFloat redComponent = MIN([self pspdf_redComponent]+delta,0);
    CGFloat greenComponent = MIN([self pspdf_greenComponent]+delta,0);
    CGFloat blueComponent = MIN([self pspdf_blueComponent]+delta,0);
    CGFloat alphaComponent = [self pspdf_alphaComponent];

    UIColor *lightenedColor = [UIColor colorWithRed:redComponent green:greenComponent blue:blueComponent alpha:alphaComponent];
    return lightenedColor;
}

- (UIColor *)pspdf_darkenedColor {
    CGFloat delta = 0.1f;
    if (![self pspdf_canProvideRGBColor]) return self;

    CGFloat redComponent = MAX([self pspdf_redComponent]-delta,0);
    CGFloat greenComponent = MAX([self pspdf_greenComponent]-delta,0);
    CGFloat blueComponent = MAX([self pspdf_blueComponent]-delta,0);
    CGFloat alphaComponent = [self pspdf_alphaComponent];

    UIColor *darkenedColor = [UIColor colorWithRed:redComponent green:greenComponent blue:blueComponent alpha:alphaComponent];
    return darkenedColor;
}

- (UIColor *)pspdf_colorInRGBColorSpace {
    UIColor *newColor = self;
    
    // convert UIDeviceWhiteColorSpace to UIDeviceRGBColorSpace.
    if (CGColorGetNumberOfComponents(self.CGColor) < 4) {
        const CGFloat *components = CGColorGetComponents(self.CGColor);
        newColor = [UIColor colorWithRed:components[0] green:components[0] blue:components[0] alpha:components[1]];
    }
    
    return newColor;
}

@end

// Generates two colors that differ in their brightness.
NSArray *PSPDFGradientColorsForColor(UIColor *color) {
    // A nil/clear color doesn't work with gradients; and is not what we want.
    if (!color || [color isEqual:[UIColor clearColor]]) {
        color = [UIColor blackColor]; // will change to yellow later
    }

    // ensure we're in RGB colorspace
    color = [color pspdf_colorInRGBColorSpace];
    const CGFloat *colorComponents = CGColorGetComponents(color.CGColor);
	float r = colorComponents[0], g = colorComponents[1], b = colorComponents[2];

    // if color is black, revert to yellow
	if (r == 0 && g == 0 && b == 0) {r = 1.0; g = 0.85; b = 0.25; }

    float h1, s1, v1, r1, g1, b1, r2, g2, b2;
	PSPDFRGBtoHSB(r, g, b, &h1, &s1, &v1);
	s1 = 0.25; v1 = 1.0;
	PSPDFHSBtoRGB(&r1, &g1, &b1, h1, s1, v1);
	float h2 = h1, s2 = 0.55, v2 = 0.95;
	PSPDFHSBtoRGB(&r2, &g2, &b2, h2, s2, v2);

    return @[[UIColor colorWithRed:r1 green:g1 blue:b1 alpha:1.f], [UIColor colorWithRed:r2 green:g2 blue:b2 alpha:1.f]];
}
