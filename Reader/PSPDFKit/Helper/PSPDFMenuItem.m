//
//  PSPDFMenuItem.m
//  PSPDFKit
//
//  Copyright (c) 2012 Peter Steinberger. All rights reserved.
//

#import "PSPDFMenuItem.h"
#import "PSPDFPatches.h"
#import <objc/runtime.h>
#import <objc/message.h>
#import "PSPDFKitGlobal.h"
#import "PSPDFKitGlobal+Internal.h"

// imp_implementationWithBlock changed it's type in iOS6.
#if __IPHONE_OS_VERSION_MAX_ALLOWED < 60000
#define PSPDFBlockImplCast (__bridge void *)
#else
#define PSPDFBlockImplCast
#endif


NSString *kMenuItemTrailer = @"pspdf_performMenuItem";

@interface PSPDFMenuItem();
@property (nonatomic, assign) SEL customSelector;
@end

@implementation PSPDFMenuItem

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Static

static BOOL PSPDFIsMenuItemSelector(SEL selector) {
    return [NSStringFromSelector(selector) hasPrefix:kMenuItemTrailer];
}

/*
 This might look scary, but it's actually not that bad.
 We hook into the three methods of UIResponder and NSObject to capture calls to our custom created selector.
 Then we find the UIMenuController and search for the corresponding PSMenuItem.
 If the kMenuItemTrailer is not detected, we call the original implementation.

 This all wouldn't be neccesasry if UIMenuController would call our selectors with the UIMenuItem as sender.
 */
+ (void)installMenuHandlerForObject:(id)object {
    @autoreleasepool {
        @synchronized(self) {
            // object can be both a class or an instance of a class.
            Class objectClass = class_isMetaClass(object_getClass(object)) ? object : [object class];

            // check if menu handler has been already installed.
            SEL canPerformActionSEL = NSSelectorFromString(@"pspdf_canPerformAction:withSender:");
            if (!class_getInstanceMethod(objectClass, canPerformActionSEL)) {

                // add canBecomeFirstResponder if it is not returning YES. (or if we don't know)
                if (object == objectClass || ![object canBecomeFirstResponder]) {
                    SEL canBecomeFRSEL = NSSelectorFromString(@"pspdf_canBecomeFirstResponder");
                    IMP canBecomeFRIMP = imp_implementationWithBlock(PSPDFBlockImplCast(^(id _self) {
                        return YES;
                    }));
                    PSPDFReplaceMethod(objectClass, @selector(canBecomeFirstResponder), canBecomeFRSEL, canBecomeFRIMP);
                }

                // swizzle canPerformAction:withSender: for our custom selectors.
                // Queried before the UIMenuController is shown.
                IMP canPerformActionIMP = imp_implementationWithBlock(PSPDFBlockImplCast(^(id _self, SEL action, id sender) {
                    return PSPDFIsMenuItemSelector(action) ? YES : ((BOOL (*)(id, SEL, SEL, id))objc_msgSend)(_self, canPerformActionSEL, action, sender);
                }));
                PSPDFReplaceMethod(objectClass, @selector(canPerformAction:withSender:), canPerformActionSEL, canPerformActionIMP);

                // swizzle methodSignatureForSelector:.
                SEL methodSignatureSEL = NSSelectorFromString(@"pspdf_methodSignatureForSelector:");
                IMP methodSignatureIMP = imp_implementationWithBlock(PSPDFBlockImplCast(^(id _self, SEL selector) {
                    if (PSPDFIsMenuItemSelector(selector)) {
                        return [NSMethodSignature signatureWithObjCTypes:"v@:@"]; // fake it.
                    }else {
                        return (NSMethodSignature *)objc_msgSend(_self, methodSignatureSEL, selector);
                    }
                }));
                PSPDFReplaceMethod(objectClass, @selector(methodSignatureForSelector:), methodSignatureSEL, methodSignatureIMP);

                // swizzle forwardInvocation:
                SEL forwardInvocationSEL = NSSelectorFromString(@"pspdf_forwardInvocation:");
                IMP forwardInvocationIMP = imp_implementationWithBlock(PSPDFBlockImplCast(^(id _self, NSInvocation *invocation) {
                    if (PSPDFIsMenuItemSelector([invocation selector])) {
                        BOOL didPerformBlock = NO;
                        for (PSPDFMenuItem *menuItem in [UIMenuController sharedMenuController].menuItems) {
                            if ([menuItem isKindOfClass:[PSPDFMenuItem class]] && sel_isEqual([invocation selector], menuItem.customSelector)) {
                                didPerformBlock = YES;
                                [menuItem performBlock]; break; // find corresponding MenuItem and forward
                            }
                        }
                        if (!didPerformBlock) {
                            PSPDFLogWarning(@"Failed to find matching menuItem in UIMenuController for %@", NSStringFromSelector([invocation selector]));
                        }
                    }else {
                        objc_msgSend(_self, forwardInvocationSEL, invocation);
                    }
                }));
                PSPDFReplaceMethod(objectClass, @selector(forwardInvocation:), forwardInvocationSEL, forwardInvocationIMP);
            }
        }
    }
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (id)initWithTitle:(NSString *)title block:(void(^)())block {
    return [self initWithTitle:title block:block identifier:nil];
}

- (id)initWithTitle:(NSString *)title block:(void(^)())block identifier:(NSString *)identifier {
    NSParameterAssert([title isKindOfClass:[NSString class]]);
    
    // Create a unique, still debuggable selector unique per PSMenuItem.
    NSString *strippedTitle = [[[title componentsSeparatedByCharactersInSet:[[NSCharacterSet letterCharacterSet] invertedSet]] componentsJoinedByString:@""] lowercaseString];
    CFUUIDRef uuid = CFUUIDCreate(kCFAllocatorDefault);
    NSString *uuidString = CFBridgingRelease(CFUUIDCreateString(kCFAllocatorDefault, uuid));
    CFRelease(uuid);
    SEL customSelector = NSSelectorFromString([NSString stringWithFormat:@"%@_%@_%@:", kMenuItemTrailer, strippedTitle, uuidString]);

    if ((self = [super initWithTitle:title action:customSelector])) {
        self.customSelector = customSelector;
        _enabled = YES;
        _block = [block copy];
        _identifier = [identifier copy];
    }
    return self;
}

- (NSString *)description {
    return [NSString stringWithFormat:@"<%@: %p identifier:%@>", NSStringFromClass([self class]), self, self.identifier];
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public

// Nils out action selector if we get disabled; auto-hides it from the UIMenuController.
- (void)setEnabled:(BOOL)enabled {
    _enabled = enabled;
    self.action = enabled ? self.customSelector : NULL;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Private

// Trampuline executor.
- (void)performBlock {
    if (_block) _block();
}

@end
