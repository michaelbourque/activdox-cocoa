//
//  PSPDFWindow.h
//  PSPDFKit
//
//  Copyright (c) 2012 Peter Steinberger. All rights reserved.
//

// Subclass that automatically rotates and overlays the status bar.
@interface PSPDFWindow : UIWindow

@end
