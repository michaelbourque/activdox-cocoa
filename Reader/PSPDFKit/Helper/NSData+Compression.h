//
//  NSData+Compression.h
//  PSPDFKit
//
//  Copyright 2012 Peter Steinberger. All rights reserved.
//

#import "PSPDFKitGlobal.h"

@interface NSData (PSPDFCompression)

- (NSData *)pspdf_zlibInflate; // decompress
@end
