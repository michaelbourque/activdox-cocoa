//
//  PSPDFWindow.m
//  PSPDFKit
//
//  Copyright (c) 2012 Peter Steinberger. All rights reserved.
//

#import "PSPDFWindow.h"
#import "PSPDFKitGlobal+Internal.h"
#import "PSPDFConverter.h"

static CGAffineTransform PSPDFTransformForOrientation(UIInterfaceOrientation orientation) {
    switch (orientation) {
        case UIInterfaceOrientationLandscapeLeft:
            return CGAffineTransformMakeRotation(-PSPDFDegreesToRadians(90));
        case UIInterfaceOrientationLandscapeRight:
            return CGAffineTransformMakeRotation(PSPDFDegreesToRadians(90));
        case UIInterfaceOrientationPortraitUpsideDown:
            return CGAffineTransformMakeRotation(PSPDFDegreesToRadians(180));
        case UIInterfaceOrientationPortrait:
        default:
            return CGAffineTransformMakeRotation(PSPDFDegreesToRadians(0));
    }
}

@implementation PSPDFWindow

- (id)init {
    if ((self = [self initWithFrame:[UIScreen mainScreen].bounds])) {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(statusBarDidChangeFrame:) name:UIApplicationDidChangeStatusBarFrameNotification object:nil];
    }
    return self;
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidChangeStatusBarFrameNotification object:nil];
}

- (id)initWithFrame:(CGRect)frame {
    if ((self = [super initWithFrame:frame])) {
        self.windowLevel = UIWindowLevelStatusBar;
        self.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        self.backgroundColor = [UIColor clearColor];
        self.transform = PSPDFTransformForOrientation([UIApplication sharedApplication].statusBarOrientation);
        self.userInteractionEnabled = NO;
    }
    return self;
}

- (void)statusBarDidChangeFrame:(NSNotification *)notification {
    self.transform = PSPDFTransformForOrientation([UIApplication sharedApplication].statusBarOrientation);
}
@end
