//
//  PSPDFAESCryptoDataProvider.m
//  PSPDFKit
//
//  Copyright (c) 2012 Peter Steinberger. All rights reserved.
//

#import "PSPDFAESCryptoDataProvider.h"
#include <CommonCrypto/CommonKeyDerivation.h> // PBKDF2
#import <CommonCrypto/CommonCryptor.h>
#import <objc/runtime.h>

// micro optimizations
#define likely(x)   __builtin_expect(!!(x), 1)
#define unlikely(x) __builtin_expect(!!(x), 0)

// The higher the better, but takes longer
// Also must be equal to number used at encryption time
#define PBKDFNumberOfRounds 50000
const size_t kBlockSize = kCCBlockSizeAES128;

@interface PSPDFAESCryptoDataProvider() {
    CCCryptorRef _cryptor;
    FILE *_fin;
    NSUInteger _blockCount;
    size_t _onDiskFileSize;
    size_t _decryptedFileSize;
    char _currentIV[kBlockSize];
    NSInteger _lastBlockRead;
}
@property (nonatomic, readwrite) CGDataProviderRef dataProvider;
@end

const void *kPSPDFAssociatedCryptoHandler;

size_t PSPDFGetBytesAtPosition(void *info, void *buffer, off_t position, size_t count);
void PSPDFReleaseProvider(void *info);
CGDataProviderDirectCallbacks pspdf_providerCallbacks = { 0, NULL, NULL, PSPDFGetBytesAtPosition, PSPDFReleaseProvider };

@implementation PSPDFAESCryptoDataProvider

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (id)initWithURL:(NSURL *)URL passphrase:(NSString *)passphrase salt:(NSString *)salt {
#ifndef PSPDFKIT_DISABLE_TEXT_SELECTION_FEATURES
    if ((self = [super init])) {
        NSParameterAssert(URL);
        NSParameterAssert(passphrase);
        NSParameterAssert(salt);

        if (![URL isFileURL]) {
            PSPDFLogError(@"You need to initialize the document with a *file* URL. (NSURL fileURLWithPath:)"); return nil;
        }

        // load file
        const char *finPath = [[URL path] cStringUsingEncoding:NSUTF8StringEncoding];
        _fin = fopen(finPath, "rb");
        if (!_fin) {
            PSPDFLogError(@"%s: Can't open input file\n", finPath);
            return nil;
        }

        NSData *passphraseData = [passphrase dataUsingEncoding:NSUTF8StringEncoding];
        NSData *passphraseSaltData = [salt dataUsingEncoding:NSUTF8StringEncoding];

        NSMutableData *key = [NSMutableData dataWithLength:kCCKeySizeAES256];
        CCKeyDerivationPBKDF(kCCPBKDF2,
                             [passphraseData bytes],
                             [passphraseData length],
                             [passphraseSaltData bytes],
                             [passphraseSaltData length],
                             kCCPRFHmacAlgSHA256,
                             PBKDFNumberOfRounds,
                             [key mutableBytes],
                             [key length]);

        CCCryptorStatus status = CCCryptorCreate(kCCDecrypt, kCCAlgorithmAES128, 0, [key bytes], [key length], NULL, &_cryptor);
        if (status != kCCSuccess) {
            PSPDFLogError(@"Failed to init cryptor: %d", status);
            return nil;
        }

        // get size of file on disk
        fseek(_fin, 0L, SEEK_END);
        _onDiskFileSize = ftell(_fin);

        if (_onDiskFileSize < kBlockSize) {
            PSPDFLogError(@"Error: The file is too small, it doesn't even contain the IV vector.");
            return nil;
        }
        if (_onDiskFileSize % kBlockSize != 0) {
            PSPDFLogError(@"Error: The file isn't aligned to an AES block.");
            return nil;
        }

        // subtract one block since the first block is the IV
        _blockCount = (_onDiskFileSize / kBlockSize) - 1;
        _lastBlockRead = -1;

        PSPDFLog(@"File has %u blocks (each with %zd bytes)", _blockCount, kBlockSize);

        // decrypt the last block(s) to determine the real file size
        char buffer[kBlockSize];
        if (![self decryptBlock:(_blockCount - 1) buffer:buffer]) { PSPDFLogWarning(@"wanted to read last block but failed"); return nil; }
        NSUInteger paddingLength = buffer[kBlockSize - 1];
        
        if (!(paddingLength > 0 && paddingLength <= kBlockSize)) {
            PSPDFLogError(@"The file appears to be invalid. Wrong passphrase/salt?");
            return nil;
        }
        _decryptedFileSize = _onDiskFileSize - kBlockSize /* don't count the IV block */ - paddingLength /* remove the padding bytes */;

        // create the data provider
        self.dataProvider = CGDataProviderCreateDirect((__bridge void *)(self), _decryptedFileSize, &pspdf_providerCallbacks);

        // build a retain cycle to retain the class as long as the data provider exists.
        objc_setAssociatedObject((__bridge id)(_dataProvider), kPSPDFAssociatedCryptoHandler, self, OBJC_ASSOCIATION_RETAIN);
    }

    return self;
#else
    PSPDFLogError(@"Only available in PSPDFKit Annotate.");
    return nil;
#endif
}

- (void)dealloc {
    fclose(_fin);
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Private

- (BOOL)decryptBlock:(NSUInteger)block buffer:(void *)buffer {
    assert(block < _blockCount);
    size_t readCount;

    // set IV for block
    if (_lastBlockRead < 0 || _lastBlockRead != (block - 1)) {
//        PSPDFLogVerbose(@"Reset IV in order to decrypt block %u", block);
        fseek(_fin, offsetForIVForBlock(block), SEEK_SET);
        readCount = fread(_currentIV, 1, kBlockSize, _fin);
        if (readCount != kBlockSize) { PSPDFLogWarning(@"wanted to read IV (one block: %zd bytes) but got %zd bytes", kBlockSize, readCount); return NO; }
        CCCryptorReset(_cryptor, _currentIV);

        fseek(_fin, offsetForBlock(block), SEEK_SET);
    }

    // Read encrypted Block
    readCount = fread(buffer, 1, kBlockSize, _fin);
    if (readCount != kBlockSize) { PSPDFLogWarning(@"wanted to read one block (%u, %zd bytes) but got %zd bytes", block, kBlockSize, readCount); return NO; }

    CCCryptorStatus status = CCCryptorUpdate(_cryptor, buffer, kBlockSize, buffer, kBlockSize, &readCount);
    if (kCCSuccess != status) { PSPDFLogError(@"Crypto error decryption block %d file: %d", block, status); return NO; }
    assert(readCount == kBlockSize);

    _lastBlockRead = block;

    return YES;
}

// In order to decrypt block x we need to read block (x-1) as the IV
static inline long offsetForIVForBlock(NSUInteger block) {
    return block * kBlockSize;
};
static inline long offsetForBlock(NSUInteger block) {
    return (block+1) * kBlockSize;
};

- (size_t)bytesAtOffset:(off_t)requestedOffset length:(size_t)requestedLength buffer:(void *)responseBuffer {
#ifndef PSPDFKIT_DISABLE_TEXT_SELECTION_FEATURES
    // align position to 16-byte AES block boundary
    size_t leadingBytes = requestedOffset % kBlockSize;
    size_t trailingBytes = (kBlockSize - (requestedOffset + requestedLength) % kBlockSize) % kBlockSize;

    off_t offset = requestedOffset - leadingBytes;
    size_t length = requestedLength + leadingBytes + trailingBytes;
    assert(offset % kBlockSize == 0);
    assert(length % kBlockSize == 0);

    NSUInteger blockCount = length / kBlockSize;
    NSUInteger firstBlock = (NSUInteger)(offset / kBlockSize);
    NSUInteger lastBlock = firstBlock + blockCount - 1;

    //NSLog(@"requesting %zd bytes at offset %lld (-> read %u blocks starting at %u (%zd leading bytes, %zd trailing bytes))", requestedLength, requestedOffset, blockCount, firstBlock, leadingBytes, trailingBytes);

    char buffer[kBlockSize];
    size_t totalReadLength = 0;
    for (NSUInteger block = firstBlock; block <= lastBlock; block++) {
        if (![self decryptBlock:block buffer:buffer]) { PSPDFLogWarning(@"wanted to read block %u but failed", block); return totalReadLength; }
        void *copyStart = buffer;
        size_t copyLength = kBlockSize;
        if (unlikely(block == firstBlock)) {
            // First block: Move the offset leadingBytes to the right and reduce length of memcpy'd buffer accordingly
            copyStart += leadingBytes;
            copyLength -= leadingBytes;
        }
        if (unlikely(block == lastBlock)) {
            // Last block: Reduce length of memcpy'd buffer accordingly
            copyLength -= trailingBytes;
        }
        memcpy(responseBuffer + totalReadLength, copyStart, copyLength);
        totalReadLength += copyLength;
    }
    assert(requestedLength == totalReadLength);
    return totalReadLength;
#else
    return 0;
#endif
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Direct

size_t PSPDFGetBytesAtPosition(void *info, void *buffer, off_t position, size_t count) {
    // call back into class method
    PSPDFAESCryptoDataProvider *cryptoWrapper = (__bridge PSPDFAESCryptoDataProvider *)(info);
    return [cryptoWrapper bytesAtOffset:position length:count buffer:buffer];
}

void PSPDFReleaseProvider(void *info) {
    PSPDFLogVerbose(@"Release PSPDFCryptoDataProvider");
    objc_setAssociatedObject((__bridge id)info, kPSPDFAssociatedCryptoHandler, nil, OBJC_ASSOCIATION_RETAIN);
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Static

+ (BOOL)isAESCryptoDataProvider:(CGDataProviderRef)dataProviderRef {
    PSPDFAESCryptoDataProvider *cryptoDataProvider = objc_getAssociatedObject((__bridge id)dataProviderRef, kPSPDFAssociatedCryptoHandler);
    return [cryptoDataProvider isKindOfClass:self];
}

+ (BOOL)isAESCryptoFeatureAvailable {
#ifndef PSPDFKIT_DISABLE_TEXT_SELECTION_FEATURES
    return YES;
#else
    return NO;
#endif
}

@end
