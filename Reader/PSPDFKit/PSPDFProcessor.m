//
//  PSPDFProcessor.m
//  PSPDFKit
//
//  Copyright (c) 2012 Peter Steinberger. All rights reserved.
//

#import "PSPDFProcessor.h"
#import "PSPDFDocument.h"
#import "PSPDFPageInfo.h"
#import "PSPDFConverter.h"
#import "PSPDFKitGlobal+Internal.h"
#import <CoreGraphics/CoreGraphics.h>
#import <QuartzCore/QuartzCore.h>
#import <objc/runtime.h>

@interface PSPDFConversionOperation() <UIWebViewDelegate> {
    dispatch_semaphore_t _webViewWaiter;
}
@property (nonatomic, strong) NSURL *inputURL;
@property (nonatomic, strong) NSURL *outputFileURL;
@property (nonatomic, copy) NSDictionary *options;
@property (nonatomic, strong) UIWebView *webView;
@property (nonatomic, strong) NSError *error;
@end

@interface PSPDFPrintPageRenderer : UIPrintPageRenderer
@property (nonatomic, assign) BOOL generatingPDF;
@property (nonatomic, assign) NSInteger heightCount;
@property (nonatomic, assign) UIEdgeInsets borderMargin;
- (NSData *)printToPDFWithPageCount:(NSInteger)height pageSizeDict:(NSDictionary *)pageSizeDict;
@end

NSString *const kPSPDFProcessorAnnotationTypes = @"kPSPDFProcessorAnnotationTypes";
NSString *const kPSPDFProcessorAnnotationDict = @"kPSPDFProcessorAnnotationDict";

NSString *const kPSPDFProcessorPageRect = @"kPSPDFProcessorPageRect";
NSString *const kPSPDFProcessorNumberOfPages = @"kPSPDFProcessorNumberOfPages";
NSString *const kPSPDFProcessorPageBorderMargin = @"kPSPDFProcessorPageBorderMargin";
NSString *const kPSPDFProcessorAdditionalDelay = @"kPSPDFProcessorAdditionalDelay";
NSString *const kPSPDFProcessorDocumentTitle = @"kPSPDFProcessorDocumentTitle";

@interface PSPDFProcessor()
@end

@implementation PSPDFProcessor

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public

+ (instancetype)defaultProcessor {
    static dispatch_once_t onceToken;
    __strong static PSPDFProcessor *_processor;
    dispatch_once(&onceToken, ^{
        _processor = [PSPDFProcessor new];
    });
    return _processor;
}

+ (NSOperationQueue *)conversionOperationQueue {
    static dispatch_once_t onceToken;
    __strong static NSOperationQueue *_queue;
    dispatch_once(&onceToken, ^{
        _queue = [NSOperationQueue new];
        _queue.maxConcurrentOperationCount = 1;
        _queue.name = @"PSPDFConversionOperationQueue";
    });
    return _queue;
}

- (BOOL)generatePDFFromDocument:(PSPDFDocument *)document pageRange:(NSIndexSet *)pageRange outputFileURL:(NSURL *)fileURL options:(NSDictionary *)options {
    NSParameterAssert(document); NSParameterAssert(pageRange); NSParameterAssert(fileURL);
    if (!document || ![document isValid] || [pageRange count] == 0 || !fileURL || ![fileURL isFileURL]) {
        PSPDFLogWarning(@"Called with invalid arguments. Stopping here."); return NO;
    }

    PSPDFPageInfo *pageInfo = [document pageInfoForPage:[pageRange firstIndex]];
    NSDictionary *documentInfo = [self documentInfoForDocument:document];
    UIGraphicsBeginPDFContextToFile([fileURL path], pageInfo.pageRect, documentInfo);
    [self renderDocumentInContext:document pageRange:pageRange options:options];
    UIGraphicsEndPDFContext();
    return YES;
}

- (NSData *)generatePDFFromDocument:(PSPDFDocument *)document pageRange:(NSIndexSet *)pageRange options:(NSDictionary *)options {
    NSParameterAssert(document); NSParameterAssert(pageRange);
    if (!document || ![document isValid] || [pageRange count] == 0) {
        PSPDFLogWarning(@"Called with invalid arguments. Stopping here."); return nil;
    }

    NSMutableData *data = [NSMutableData data];
    PSPDFPageInfo *pageInfo = [document pageInfoForPage:[pageRange firstIndex]];
    NSDictionary *documentInfo = [self documentInfoForDocument:document];
    UIGraphicsBeginPDFContextToData(data, (CGRect){.size=pageInfo.rotatedPageRect.size}, documentInfo);
    [self renderDocumentInContext:document pageRange:pageRange options:options];
    UIGraphicsEndPDFContext();
    return data;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public / Website printing

#define kPSPDFPageWidth 595
#define kPSPDFPageHeight 842
#define kPSPDFNumberOfPagesDefault 10
- (BOOL)generatePDFFromHTMLString:(NSString *)html outputFileURL:(NSURL *)fileURL options:(NSDictionary *)options {
    NSParameterAssert(html); NSParameterAssert(fileURL);
    if (!html || !fileURL || ![fileURL isFileURL]) {
        PSPDFLogWarning(@"Called with invalid arguments. Stopping here."); return NO;
    }

    // get options
    CGRect pageRect = options[kPSPDFProcessorPageRect] ? [options[kPSPDFProcessorPageRect] CGRectValue] : CGRectMake(0, 0, kPSPDFPageWidth, kPSPDFPageHeight);
    NSUInteger numberOfPages = options[kPSPDFProcessorNumberOfPages] ? fmaxf([options[kPSPDFProcessorNumberOfPages] unsignedIntegerValue], 1) : kPSPDFNumberOfPagesDefault;
    UIEdgeInsets edgeInsets = options[kPSPDFProcessorPageBorderMargin] ? [options[kPSPDFProcessorPageBorderMargin] UIEdgeInsetsValue] : UIEdgeInsetsMake(5, 5, 5, 5);
    NSString *title = options[kPSPDFProcessorDocumentTitle];

    // open PDF context
    NSDictionary *documentInfo = title ? [self documentInfoWithTitle:title] : [self documentInfoForDocument:nil];
    UIGraphicsBeginPDFContextToFile([fileURL path], pageRect, documentInfo);

    // Render the html into a PDF
    PSPDFPrintPageRenderer *renderer = [PSPDFPrintPageRenderer new];
    renderer.borderMargin = edgeInsets;
    UIMarkupTextPrintFormatter *formatter = [[UIMarkupTextPrintFormatter alloc] initWithMarkupText:html];
    [renderer addPrintFormatter:formatter startingAtPageAtIndex:0];
    [renderer printToPDFWithPageCount:numberOfPages pageSizeDict:nil];

    UIGraphicsEndPDFContext();
    return YES;
}

- (PSPDFConversionOperation *)generatePDFFromURL:(NSURL *)URL outputFileURL:(NSURL *)outputURL options:(NSDictionary *)options completionBlock:(PSPDFCompletionBlockWithError)completionBlock {
    NSParameterAssert(URL); NSParameterAssert(outputURL);
    if (!URL || !outputURL || ![outputURL isFileURL]) {
        PSPDFLogWarning(@"Called with invalid arguments. Stopping here.");
        if (completionBlock) completionBlock(outputURL, PSPDFError(PSPDFErrorCodeFailedToConvertToPDF, @"Called with invalid arguments. Stopping here.", nil)); return nil;
    }

    PSPDFConversionOperation *operation = [[PSPDFConversionOperation alloc] initWithURL:URL outputFileURL:outputURL options:options completionBlock:completionBlock];

    [[[self class] conversionOperationQueue] addOperation:operation]; // enqueue

    return operation;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Private

- (NSMutableDictionary *)baseDocumentInfo {
    return [NSMutableDictionary dictionaryWithObject:PSPDFVersionString() forKey:(id)kCGPDFContextCreator];
}

// Document might be nil, that's ok.
- (NSDictionary *)documentInfoForDocument:(PSPDFDocument *)document {
    return [self documentInfoWithTitle:document.title];
}

- (NSDictionary *)documentInfoWithTitle:(NSString *)title {
    NSMutableDictionary *documentInfo = [self baseDocumentInfo];
    if (title) documentInfo[(id)kCGPDFContextTitle] = title;
    return [documentInfo copy];
}

// actually does all the work
- (void)renderDocumentInContext:(PSPDFDocument *)document pageRange:(NSIndexSet *)pageRange options:(NSDictionary *)options {
    CGContextRef context = UIGraphicsGetCurrentContext();
    if (!context) { PSPDFLogError(@"Called without context! Breaking here."); return; }

    PSPDFAnnotationType annotationTypes = [options[kPSPDFProcessorAnnotationTypes] integerValue];
    NSDictionary *annotationDict = options[kPSPDFProcessorAnnotationDict];

    // iterate over all pages
    NSUInteger pageIndex = [pageRange firstIndex];
    while (pageIndex != NSNotFound) {
        PSPDFPageInfo *pageInfo = [document pageInfoForPage:pageIndex];

        UIGraphicsBeginPDFPageWithInfo((CGRect){.size=pageInfo.rotatedPageRect.size}, nil);

        // fetch annotations
        NSArray *annotations = annotationDict[@(pageIndex)];
        if (!annotations && annotationTypes != PSPDFAnnotationTypeNone) {
            annotations = [document annotationsForPage:pageIndex type:annotationTypes];
        }

        // Todo: Note annotations!
        [document renderPage:pageIndex inContext:context withSize:pageInfo.rotatedPageRect.size clippedToRect:(CGRect){.size=pageInfo.rotatedPageRect.size} withAnnotations:annotations options:nil];

        pageIndex = [pageRange indexGreaterThanIndex:pageIndex];
    }
}

// Hacky way to strip empty pages of a printed PDF.
static BOOL PSPDFStripEmptyPagesOfPDFAtURL(NSOperation *operation, NSURL *fileURL) {
    CGPDFDocumentRef documentRef = CGPDFDocumentCreateWithURL((__bridge CFURLRef)(fileURL));
    NSUInteger pageCount = CGPDFDocumentGetNumberOfPages(documentRef);
    if (pageCount == 0) {
        PSPDFLogError(@"Zero pages, stopping."); CGPDFDocumentRelease(documentRef); return NO;
    }

    // create temp file
    NSURL *tempURL = PSPDFTempFileURLWithPathExtension([fileURL lastPathComponent], @"pdf");

    CGPDFPageRef pageRefOfFirstPage = CGPDFDocumentGetPage(documentRef, 1);
    CGRect mediaBox = CGPDFPageGetBoxRect(pageRefOfFirstPage, kCGPDFCropBox);
    NSDictionary *metadata = PSPDFConvertPDFDictionary(CGPDFDocumentGetInfo(documentRef));
    CGContextRef context = CGPDFContextCreateWithURL((__bridge CFURLRef)(tempURL), &mediaBox, (__bridge CFDictionaryRef)(metadata));
    for (int pageIndex = 0; pageIndex < pageCount; pageIndex++) {
        if (operation.isCancelled) break;

        CGPDFPageRef pageRef = CGPDFDocumentGetPage(documentRef, pageIndex+1);

        // check if pagestream is so small that we practically have an empty page
        CGPDFDictionaryRef pageDict = CGPDFPageGetDictionary(pageRef);
        CGPDFStreamRef pageStream;
        if (CGPDFDictionaryGetStream(pageDict, "Contents", &pageStream)) {
            CGPDFDictionaryRef streamDict = CGPDFStreamGetDictionary(pageStream);
            unsigned long streamLength = [PSPDFConvertPDFDictionary(streamDict)[@"Length"] unsignedLongValue];
            if (streamLength < 12) {
                PSPDFLogVerbose(@"Stripping %d pages.", pageCount-pageIndex); break;
            }
        }

        CGPDFContextBeginPage(context, NULL);
        CGContextDrawPDFPage(context, pageRef);
        CGRect cropBox = CGPDFPageGetBoxRect(pageRef, kCGPDFCropBox);
        CGContextTranslateCTM(context, 0, CGRectGetHeight(cropBox));
        CGContextScaleCTM(context, 1.0, -1.0);
        PSPDFDrawHelper(context, 1);
        CGPDFContextEndPage(context);
    }
    CGPDFContextClose(context);
    CGContextRelease(context);
    CGPDFDocumentRelease(documentRef);

    // move to new location
    if (!operation.isCancelled) {
        NSError *error;
        if (![[NSFileManager defaultManager] removeItemAtURL:fileURL error:&error]) {
            PSPDFLogWarning(@"Failed to remove file: %@", [error localizedDescription]);
        }
        if (![[NSFileManager defaultManager] moveItemAtURL:tempURL toURL:fileURL error:&error]) {
            PSPDFLogWarning(@"Failed to move file: %@", [error localizedDescription]);
        }
    }

    return YES;
}

@end


@implementation PSPDFPrintPageRenderer

- (CGRect)paperRect {
    if (!_generatingPDF) return [super paperRect];
    return UIGraphicsGetPDFContextBounds();
}

- (CGRect)printableRect {
    if (!_generatingPDF) return [super printableRect];
    return UIEdgeInsetsInsetRect(self.paperRect, self.borderMargin);
}

- (NSInteger)numberOfPages {
    if (!_generatingPDF) return [super numberOfPages];
    return self.heightCount;
}

- (NSData *)printToPDFWithPageCount:(NSInteger)height pageSizeDict:(NSDictionary *)pageSizeDict {
    self.heightCount = height;
    _generatingPDF = YES;

    [self prepareForDrawingPages:NSMakeRange(0, self.numberOfPages)];

    CGRect bounds = UIGraphicsGetPDFContextBounds();
    for (int i=0 ; i < self.numberOfPages; i++) {
        CGSize pageSize = [pageSizeDict[@(i)] CGSizeValue];
        if (PSPDFSizeIsEmpty(pageSize)) {
            UIGraphicsBeginPDFPage();
        }else {
            UIGraphicsBeginPDFPageWithInfo(CGRectMake(0, 0, pageSize.width, pageSize.height), nil);
        }

        [self drawPageAtIndex:i inRect:bounds];
    }

    _generatingPDF = NO;
    return nil;
}

@end

@implementation PSPDFConversionOperation

- (id)initWithURL:(NSURL *)inputURL outputFileURL:(NSURL *)outputFileURL options:(NSDictionary *)options completionBlock:(PSPDFCompletionBlockWithError)completionBlock {
#ifndef PSPDFKIT_DISABLE_TEXT_SELECTION_FEATURES
    if ((self = [super init])) {
        _inputURL = inputURL;
        _outputFileURL = outputFileURL;
        _options = options;
        _webViewWaiter = dispatch_semaphore_create(0);
        [self setCompletionBlock:^{
            dispatch_async(dispatch_get_main_queue(), ^{
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-retain-cycles"
                if (completionBlock) completionBlock(outputFileURL, self.error);
                self.completionBlock = nil; // nil out block
#pragma clang diagnostic pop
            });
        }];
    }
    return self;
#else
    PSPDFLogError(@"Only supported in PSPDFKit Annotate");
    return nil;
#endif
}

- (NSString *)description {
	return [NSString stringWithFormat:@"<%@ inputURL:%@ outputFileURL:%@ options:%@>", NSStringFromClass([self class]), self.inputURL, self.outputFileURL, self.options];
}

- (void)dealloc {
    PSPDFDispatchRelease(_webViewWaiter);
}

- (void)main {
    if ([NSThread isMainThread]) {
        PSPDFLogError(@"Don't run this operation on the main thread! Stopping now."); return;
    }

    // create webview and fire up request
    dispatch_async(dispatch_get_main_queue(), ^{
        UIWebView *webView = [[UIWebView alloc] initWithFrame:[UIScreen mainScreen].bounds];
        webView.delegate = self;
        // iOS6+
        if ([webView respondsToSelector:@selector(setSuppressesIncrementalRendering:)]) {
            webView.suppressesIncrementalRendering = YES;
        }
        webView.scalesPageToFit = YES;
        [webView loadRequest:[NSURLRequest requestWithURL:self.inputURL]];
        self.webView = webView;
    });

    dispatch_semaphore_wait(_webViewWaiter, DISPATCH_TIME_FOREVER);

    // cleanup
    _webView.delegate = nil;
    [_webView stopLoading];
    _webView = nil;

    // if conversion was a success, perform last step.
    if (!self.error && !self.isCancelled) PSPDFStripEmptyPagesOfPDFAtURL(self, self.outputFileURL);
}

- (void)cancel {
    [super cancel];
    // instantly wake up
    dispatch_semaphore_signal(_webViewWaiter);
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UIWebViewDelegate

- (void)renderPDFFromWebView:(UIWebView *)webView {
    // get options
    BOOL isDefaultPageRect = NO;
    CGRect pageRect;
    if (_options[kPSPDFProcessorPageRect]) {
        pageRect = [_options[kPSPDFProcessorPageRect] CGRectValue];
    }else {
        pageRect = CGRectMake(0, 0, kPSPDFPageWidth, kPSPDFPageHeight);
        isDefaultPageRect = YES;
    }
    NSUInteger numberOfPages = _options[kPSPDFProcessorNumberOfPages] ? fmaxf([_options[kPSPDFProcessorNumberOfPages] unsignedIntegerValue], 1) : kPSPDFNumberOfPagesDefault;
    
    BOOL isDefaultEdgeInsets = NO;
    UIEdgeInsets edgeInsets;
    if (_options[kPSPDFProcessorPageBorderMargin]) {
        edgeInsets = [_options[kPSPDFProcessorPageBorderMargin] UIEdgeInsetsValue];
    }else {
        edgeInsets = UIEdgeInsetsMake(5, 5, 5, 5);
        isDefaultEdgeInsets = YES;
    }
    
    // get title
    NSString *title = [webView stringByEvaluatingJavaScriptFromString:@"document.title"];
    title = title ?: [webView.request.URL absoluteString];
    title = _options[kPSPDFProcessorDocumentTitle] ?: title;
    
    // try to get rects for each page.
    NSMutableDictionary *pageSizeDict = [NSMutableDictionary dictionary];
#ifndef PSPDFKIT_DONT_USE_OBFUSCATED_PRIVATE_API
    // hack into the UIWebView to get coordinates from pages documents
    // keynote seems to work with a UIWebBrowserView - harder to peek into.
    @try {
        Class pdfPageView = NSClassFromString([NSString stringWithFormat:@"U%@geView", @"IPDFPa"]);
        if (pdfPageView) {
            for (UIView *view in PSPDFGetViewInsideView(webView, [NSString stringWithFormat:@"%@WebPDF", @"UI"]).subviews) {
                if ([view isKindOfClass:pdfPageView]) {
                    CGSize pageSize = [[view valueForKey:@"cropBox"] CGRectValue].size;
                    if (!PSPDFSizeIsEmpty(pageSize)) {
                        NSUInteger pageIndex = [[view valueForKey:@"pageIndex"] unsignedIntegerValue];
                        pageSizeDict[@(pageIndex)] = BOXED(pageSize);
                    }
                }
            }
        }
    }
    @catch (NSException *exception) {
        PSPDFLogWarning(@"Failed to get rects (noncritical).%@", exception);
    }
#endif
    
    // variant that actually makes text selectable
    PSPDFPrintPageRenderer *renderer = [PSPDFPrintPageRenderer new];
    [renderer addPrintFormatter:[webView viewPrintFormatter] startingAtPageAtIndex:0];
    
    // if we have a default rect and we found page sizes, use them.
    if (isDefaultPageRect && [pageSizeDict count] > 0) {
        NSNumber *firstPage = [[[pageSizeDict allKeys] sortedArrayUsingSelector:@selector(compare:)] ps_firstObject];
        CGSize firstSize = [pageSizeDict[firstPage] CGSizeValue];
        pageRect = CGRectMake(0, 0, firstSize.width, firstSize.height);
    }
    
    // special handling for pure images
    BOOL isSingleImage = [[webView stringByEvaluatingJavaScriptFromString:@"document.body.childElementCount == 1 && document.body.firstChild.tagName == 'IMG' && document.body.firstChild.childElementCount == 0"] boolValue];
    if (isDefaultPageRect && isSingleImage) {
        CGFloat width = [[webView stringByEvaluatingJavaScriptFromString: @"document.body.firstChild.width"] floatValue];
        CGFloat height = [[webView stringByEvaluatingJavaScriptFromString: @"document.body.firstChild.height"] floatValue];
        
        // No idea why we can't just use the full size for this.
        pageRect = CGRectMake(0, 0, width/1.6, height/1.6);
        if (isDefaultEdgeInsets) edgeInsets = UIEdgeInsetsZero;
    }
    
    BOOL isKeynote = [[webView stringByEvaluatingJavaScriptFromString:@"document.getElementById('BGSlide-0').id == 'BGSlide-0'"] boolValue];
    if (isDefaultPageRect && isKeynote) {
        CGFloat width = [[webView stringByEvaluatingJavaScriptFromString:@"window.getComputedStyle(document.getElementById('slideId_1')).width"] floatValue];
        CGFloat height = [[webView stringByEvaluatingJavaScriptFromString:@"window.getComputedStyle(document.getElementById('slideId_1')).height"] floatValue];
        
        // remove margins from body and slides
        [webView stringByEvaluatingJavaScriptFromString:@"document.body.style.margin = 0;var myObj=document.getElementsByClassName('slideStyle');for(var i=0; i<myObj.length; i++){myObj[i].style.margin = 0;}"];
        
        pageRect = CGRectMake(0, 0, width/1.6, height/1.6);
        if (isDefaultEdgeInsets) edgeInsets = UIEdgeInsetsZero;
    }
    
    renderer.borderMargin = edgeInsets;
    
    if (self.isCancelled) return;
    
    // Render the html into a PDF
    UIGraphicsBeginPDFContextToFile([self.outputFileURL path], pageRect, [[PSPDFProcessor defaultProcessor] documentInfoWithTitle:title]);
    // ensure we have a context!
    if (!UIGraphicsGetCurrentContext()) {
        self.error = PSPDFError(PSPDFErrorCodeFailedToConvertToPDF, [NSString stringWithFormat:@"Context is nil, file location %@ could not be opened.", [self.outputFileURL path]], nil);
    }else {
        [renderer printToPDFWithPageCount:numberOfPages pageSizeDict:pageSizeDict];
        UIGraphicsEndPDFContext();
    }
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
#ifndef PSPDFKIT_DISABLE_TEXT_SELECTION_FEATURES
    CGFloat delayInSeconds = _options[kPSPDFProcessorAdditionalDelay] ? [_options[kPSPDFProcessorAdditionalDelay] integerValue] : 0.05f;

    // need to be called from main :/
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^{
        if (self.isCancelled) return;

        [self renderPDFFromWebView:webView];
        dispatch_semaphore_signal(_webViewWaiter);
    });
#endif
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    PSPDFLogWarning(@"Failed to convert: %@", [error localizedDescription]);
    self.error = error;
    dispatch_semaphore_signal(_webViewWaiter);
}

@end
