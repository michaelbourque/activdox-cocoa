//
//  PSPDFConverter.m
//  PSPDFKit
//
//  Copyright (c) 2012 Peter Steinberger. All rights reserved.
//

#import "PSPDFConverter.h"
#import "PSPDFKitGlobal.h"
#import "PSPDFStream.h"

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Coordinate conversion functions

CGFloat PSPDFScaleForSizeWithinSize(CGSize targetSize, CGSize boundsSize) {
    return PSPDFScaleForSizeWithinSizeWithOptions(targetSize, boundsSize, YES, NO);
}

CGFloat PSPDFScaleForSizeWithinSizeWithOptions(CGSize targetSize, CGSize boundsSize, BOOL zoomMinimalSize, BOOL fitToWidthEnabled) {
    // don't calculate if imageSize is nil
    if (CGSizeEqualToSize(targetSize, CGSizeZero)) return 1.0;

    // improve speed - this is called often.
    static CGFloat pspdf_screenScale;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        pspdf_screenScale = [[UIScreen mainScreen] scale];
    });

    // set up our content size and min/max zoomscale
    CGFloat xScale = boundsSize.width / targetSize.width;    // the scale needed to perfectly fit the image width-wise
    CGFloat yScale = boundsSize.height / targetSize.height;  // the scale needed to perfectly fit the image height-wise
    CGFloat minScale = fitToWidthEnabled ? xScale : MIN(xScale, yScale); // use minimum of these to allow the image to become fully visible
    
    // on high resolution screens we have double the pixel density,
    // so we will be seeing every pixel if we limit the maximum zoom scale to 0.5.
    CGFloat maxScale = 1.0 / pspdf_screenScale;
    
    // don't let minScale exceed maxScale.
    if (!zoomMinimalSize && minScale > maxScale) {
        minScale = maxScale;
    }
    
    if (minScale > 10.0) {
        PSPDFLogWarning(@"Ridiculous high scale detected, limiting.");
        minScale = 10.0;
    }
    return minScale;
}

CGSize PSPDFSizeForScale(CGSize size, CGFloat scale) {
	CGSize newSize = CGSizeMake(ceilf(size.width*scale), ceilf(size.height*scale));
    return newSize;
}

CGRect PSPDFAlignSizeWithinRectWithOffset(CGSize targetSize, CGRect bounds, CGFloat widthOffset, CGFloat heightOffset, PSPDFRectAlignment alignment) {
    CGFloat sizeFactor = PSPDFScaleForSizeWithinSizeWithOptions(targetSize, bounds.size, NO, NO);
    CGSize newSize = PSPDFSizeForScale(targetSize, sizeFactor);
    CGRect newRect = PSPDFAlignRectangles(CGRectMake(0, 0, newSize.width, newSize.height), bounds, alignment);
    newRect.origin.x += widthOffset * sizeFactor;
    newRect.origin.y += heightOffset * sizeFactor;
    return CGRectIntegral(newRect);
}

CGRect PSPDFAlignRectangles(CGRect alignee, CGRect aligner, PSPDFRectAlignment alignment) {
    switch (alignment) {
        case PSPDFRectAlignTop:
            alignee.origin.x = aligner.origin.x + (CGRectGetWidth(aligner)/2.f - CGRectGetWidth(alignee)/2.f);
            alignee.origin.y = aligner.origin.y + CGRectGetHeight(aligner) - CGRectGetHeight(alignee);
            break;
            
        case PSPDFRectAlignTopLeft:
            alignee.origin.x = aligner.origin.x;
            alignee.origin.y = aligner.origin.y + CGRectGetHeight(aligner) - CGRectGetHeight(alignee);
            break;
            
        case PSPDFRectAlignTopRight:
            alignee.origin.x = aligner.origin.x + CGRectGetWidth(aligner) - CGRectGetWidth(alignee);
            alignee.origin.y = aligner.origin.y + CGRectGetHeight(aligner) - CGRectGetHeight(alignee);
            break;
            
        case PSPDFRectAlignLeft:
            alignee.origin.x = aligner.origin.x;
            alignee.origin.y = aligner.origin.y + (CGRectGetHeight(aligner)/2.f - CGRectGetHeight(alignee)/2.f);
            break;
            
        case PSPDFRectAlignBottomLeft:
            alignee.origin.x = aligner.origin.x;
            alignee.origin.y = aligner.origin.y;
            break;
            
        case PSPDFRectAlignBottom:
            alignee.origin.x = aligner.origin.x + (CGRectGetWidth(aligner)/2.f - CGRectGetWidth(alignee)/2.f);
            alignee.origin.y = aligner.origin.y;
            break;
            
        case PSPDFRectAlignBottomRight:
            alignee.origin.x = aligner.origin.x + CGRectGetWidth(aligner) - CGRectGetWidth(alignee);
            alignee.origin.y = aligner.origin.y;
            break;
            
        case PSPDFRectAlignRight:
            alignee.origin.x = aligner.origin.x + CGRectGetWidth(aligner) - CGRectGetWidth(alignee);
            alignee.origin.y = aligner.origin.y + (CGRectGetHeight(aligner)/2.f - CGRectGetHeight(alignee)/2.f);
            break;
            
        case PSPDFRectAlignCenter:
        default:
            alignee.origin.x = aligner.origin.x + (CGRectGetWidth(aligner)/2.f - CGRectGetWidth(alignee)/2.f);
            alignee.origin.y = aligner.origin.y + (CGRectGetHeight(aligner)/2.f - CGRectGetHeight(alignee)/2.f);
            break;
    }
    return CGRectIntegral(alignee);
}

inline NSUInteger PSPDFNormalizeRotation(NSInteger rotation) {
    rotation %= 360;
    if (rotation < 0) rotation += 360;
    return rotation;
}

CGRect PSPDFApplyRotationToRect(CGRect pageRect, NSInteger rotation) {
    rotation = PSPDFNormalizeRotation(rotation);
    if (rotation == 90 || rotation == 270) {
        ps_swapf(pageRect.size.width, pageRect.size.height);
        ps_swapf(pageRect.origin.x, pageRect.origin.y);
    }
    return pageRect;
}

CGAffineTransform PSPDFGetTransformFromPageRectAndRotation(CGRect pageRect, NSInteger rotation) {
    rotation = PSPDFNormalizeRotation(rotation);
    CGAffineTransform pageRotationTransform = CGAffineTransformIdentity;
    if (rotation == 90) {
        pageRotationTransform = CGAffineTransformConcat(CGAffineTransformMakeTranslation(0, -pageRect.size.height), CGAffineTransformMakeRotation(M_PI/2));
    } else if (rotation == 270) {
        pageRotationTransform = CGAffineTransformConcat(CGAffineTransformConcat(CGAffineTransformMakeTranslation(-pageRect.size.width - pageRect.origin.y - pageRect.origin.x, 0), CGAffineTransformMakeRotation(-M_PI/2)), CGAffineTransformMakeTranslation(0, -pageRect.origin.y + pageRect.origin.x));
    } else if (rotation == 180) {
        pageRotationTransform = CGAffineTransformConcat(CGAffineTransformMakeTranslation(-pageRect.origin.x - pageRect.size.width, -pageRect.origin.y - pageRect.size.height), CGAffineTransformMakeRotation(-M_PI));
    }
    return pageRotationTransform;
}

//  Code to calculate the pdf points, *based* on code by Sorin Nistor. Many thanks!
//
//  Copyright (c) 2011-2012 Sorin Nistor. All rights reserved. This software is provided 'as-is', without any express or implied warranty.
//  In no event will the authors be held liable for any damages arising from the use of this software.
//  Permission is granted to anyone to use this software for any purpose, including commercial applications,
//  and to alter it and redistribute it freely, subject to the following restrictions:
//  1. The origin of this software must not be misrepresented; you must not claim that you wrote the original software.
//     If you use this software in a product, an acknowledgment in the product documentation would be appreciated but is not required.
//  2. Altered source versions must be plainly marked as such, and must not be misrepresented as being the original software.
//  3. This notice may not be removed or altered from any source distribution.
CGPoint PSPDFConvertViewPointToPDFPoint(CGPoint viewPoint, CGRect cropBox, NSUInteger rotation, CGRect bounds) {
    if (CGRectIsEmpty(bounds) || CGRectIsEmpty(cropBox)) return CGPointZero; // don't return NaN!

    CGPoint pdfPoint = CGPointMake(0, 0);
    switch (rotation) {
        case 90:
        case -270:
            pdfPoint.x = cropBox.size.width * (viewPoint.y - bounds.origin.y) / bounds.size.height;
            pdfPoint.y = cropBox.size.height * (viewPoint.x - bounds.origin.x) / bounds.size.width;
            break;
        case 180:
        case -180:
            pdfPoint.x = cropBox.size.width * (bounds.size.width - (viewPoint.x - bounds.origin.x)) / bounds.size.width;
            pdfPoint.y = cropBox.size.height * (viewPoint.y - bounds.origin.y) / bounds.size.height;
            break;
        case -90:
        case 270:
            pdfPoint.x = cropBox.size.width * (bounds.size.height - (viewPoint.y - bounds.origin.y)) / bounds.size.height;
            pdfPoint.y = cropBox.size.height * (bounds.size.width - (viewPoint.x - bounds.origin.x)) / bounds.size.width;
            break;
        case 0:
        default:
            pdfPoint.x = cropBox.size.width * (viewPoint.x - bounds.origin.x) / bounds.size.width;
            pdfPoint.y = cropBox.size.height * (bounds.size.height - (viewPoint.y - bounds.origin.y)) / bounds.size.height;
            break;
    }
    pdfPoint.x = pdfPoint.x + cropBox.origin.x;
    pdfPoint.y = pdfPoint.y + cropBox.origin.y;
    
    return pdfPoint;
}

CGPoint PSPDFConvertPDFPointToViewPoint(CGPoint pdfPoint, CGRect cropBox, NSUInteger rotation, CGRect bounds) {
    if (CGRectIsEmpty(bounds) || CGRectIsEmpty(cropBox)) return CGPointZero; // don't return NaN!

    CGPoint viewPoint = CGPointMake(0, 0);
    switch (rotation) {
        case 90:
        case -270:
            viewPoint.x = bounds.size.width * (pdfPoint.y - cropBox.origin.y) / cropBox.size.height;
            viewPoint.y = bounds.size.height * (pdfPoint.x - cropBox.origin.x) / cropBox.size.width;
            break;
        case 180:
        case -180:
            viewPoint.x = bounds.size.width * (cropBox.size.width - (pdfPoint.x - cropBox.origin.x)) / cropBox.size.width;
            viewPoint.y = bounds.size.height * (pdfPoint.y - cropBox.origin.y) / cropBox.size.height;
            break;
        case -90:
        case 270:
            viewPoint.x = bounds.size.width * (cropBox.size.height - (pdfPoint.y - cropBox.origin.y)) / cropBox.size.height;
            viewPoint.y = bounds.size.height * (cropBox.size.width - (pdfPoint.x - cropBox.origin.x)) / cropBox.size.width;
            break;
        case 0:
        default:
            viewPoint.x = bounds.size.width * (pdfPoint.x - cropBox.origin.x) / cropBox.size.width;
            viewPoint.y = bounds.size.height * (cropBox.size.height - (pdfPoint.y - cropBox.origin.y)) / cropBox.size.height;
            break;
    }
    
    viewPoint.x = viewPoint.x + bounds.origin.x;
    viewPoint.y = viewPoint.y + bounds.origin.y;
    
    return viewPoint;
}

CGRect PSPDFConvertPDFRectToViewRect(CGRect pdfRect, CGRect cropBox, NSUInteger rotation, CGRect bounds) {
    if (CGRectIsEmpty(bounds) || CGRectIsEmpty(cropBox)) return CGRectZero; // don't return NaN!

    CGPoint topLeft = PSPDFConvertPDFPointToViewPoint(pdfRect.origin, cropBox, rotation, bounds);
    CGPoint bottomRight = PSPDFConvertPDFPointToViewPoint(CGPointMake(CGRectGetMaxX(pdfRect), CGRectGetMaxY(pdfRect)), cropBox, rotation, bounds);
    CGRect viewRect = CGRectMake(topLeft.x, topLeft.y, bottomRight.x - topLeft.x, bottomRight.y - topLeft.y);
    return PSPDFNormalizeRect(viewRect);
}

CGRect PSPDFConvertViewRectToPDFRect(CGRect viewRect, CGRect cropBox, NSUInteger rotation, CGRect bounds) {
    if (CGRectIsEmpty(bounds) || CGRectIsEmpty(cropBox)) return CGRectZero; // don't return NaN!
    
    CGPoint topLeft = PSPDFConvertViewPointToPDFPoint(viewRect.origin, cropBox, rotation, bounds);
    CGPoint bottomRight = PSPDFConvertViewPointToPDFPoint(CGPointMake(CGRectGetMaxX(viewRect), CGRectGetMaxY(viewRect)), cropBox, rotation, bounds);
    CGRect pdfRect = CGRectMake(topLeft.x, topLeft.y, bottomRight.x - topLeft.x, bottomRight.y - topLeft.y);
    return PSPDFNormalizeRect(pdfRect);
}

CGRect PSPDFNormalizeRect(CGRect rect) {
    if (rect.size.height < 0) {
        rect.size.height *= -1;
        rect.origin.y -= rect.size.height;
    }
    
    if (rect.size.width < 0) {
        rect.size.width *= -1;
        rect.origin.x -= rect.size.width;
    }
    return rect;
}

CGRect PSPDFCGRectFromPoints(CGPoint p1, CGPoint p2) {
    return CGRectMake(MIN(p1.x, p2.x),
                      MIN(p1.y, p2.y),
                      fabs(p1.x - p2.x),
                      fabs(p1.y - p2.y));
}


#include <math.h>

#define MAX3(a,b,c) (a > b ? (a > c ? a : c) : (b > c ? b : c))
#define MIN3(a,b,c) (a < b ? (a < c ? a : c) : (b < c ? b : c))

void PSPDFRGBtoHSV(float r, float g, float b, float* h, float* s, float* v) {
	float min, max, delta;
	min = MIN3(r, g, b);
	max = MAX3(r, g, b);
	*v = max;        // v
	delta = max - min;
	if( max != 0 ) {
		*s = delta / max;    // s
	} else {
		// r = g = b = 0    // s = 0, v is undefined
		*s = 0;
		*h = -1;
		return;
	}
	if (r == max) {
		*h = ( g - b ) / delta;    // between yellow & magenta
	} else if (g == max) {
		*h = 2 + ( b - r ) / delta;  // between cyan & yellow
	} else {
		*h = 4 + ( r - g ) / delta;  // between magenta & cyan
	}
	*h *= 60;        // degrees
	if (*h < 0) {
		*h += 360;
	}
}

// Special thanks to Ole Begemann for providing those.
void PSPDFRGBtoHSB(float r, float g, float b, float *h, float *s, float *v) {
	float min, max, delta;
	min = fminf( r, fminf(g, b) );
	max = fmaxf( r, fmaxf(g, b) );
	*v = max;				// v
	delta = max - min;
	if( max != 0 )
		*s = delta / max;		// s
	else {
		// r = g = b = 0		// s = 0, v is undefined
		*s = 0;
		*h = -1;
		return;
	}
	if( r == max )
		*h = ( g - b ) / delta;		// between yellow & magenta
	else if ( g == max )
		*h = 2 + ( b - r ) / delta;	// between cyan & yellow
	else
		*h = 4 + ( r - g ) / delta;	// between magenta & cyan
	*h *= 60;				// degrees
	if( *h < 0 )
		*h += 360;
    
    // Convert degrees to 0.0...1.0
    *h /= 360;
}


void PSPDFHSBtoRGB(float *r, float *g, float *b, float h, float s, float v) {
    // Convert from 0.0...1.0 to degrees
    h *= 360;
    
	int i;
	float f, p, q, t;
	if( s == 0 ) {
		// achromatic (grey)
		*r = *g = *b = v;
		return;
	}
	h /= 60;			// sector 0 to 5
	i = floor( h );
	f = h - i;			// factorial part of h
	p = v * ( 1 - s );
	q = v * ( 1 - s * f );
	t = v * ( 1 - s * ( 1 - f ) );
	switch( i ) {
		case 0:
			*r = v;
			*g = t;
			*b = p;
			break;
		case 1:
			*r = q;
			*g = v;
			*b = p;
			break;
		case 2:
			*r = p;
			*g = v;
			*b = t;
			break;
		case 3:
			*r = p;
			*g = q;
			*b = v;
			break;
		case 4:
			*r = t;
			*g = p;
			*b = v;
			break;
		default:		// case 5:
			*r = v;
			*g = p;
			*b = q;
			break;
	}
}

UIBezierPath *PSPDFSplinePathFromPoints(CGPoint p1, CGPoint p2, CGPoint p3, CGPoint p4, int divisions) {
	float distance = sqrt((p3.x - p2.x)*(p3.x - p2.x) + (p3.y - p2.y)*(p3.y - p2.y));
	divisions = (distance > 2) ? distance : 1;

	float a[4];
	float b[4];
	a[0] = ( -p1.x + 3*p2.x - 3*p3.x + p4.x)/6.0;
	a[1] = ( 3*p1.x - 6*p2.x + 3*p3.x )/6.0;
	a[2] = (-3*p1.x + 3*p3.x )/6.0;
	a[3] = ( p1.x + 4*p2.x + p3.x )/6.0;
	b[0] = ( -p1.y + 3*p2.y - 3*p3.y + p4.y)/6.0;
	b[1] = ( 3*p1.y - 6*p2.y + 3*p3.y )/6.0;
	b[2] = (-3*p1.y + 3*p3.y )/6.0;
	b[3] = ( p1.y + 4*p2.y + p3.y )/6.0;
    CGPoint *spline = malloc((divisions+1) * sizeof(CGPoint));
    spline[0] = CGPointMake(0, 0); // to satisfy the clang analyzer

	for (int i = 0; i < divisions+1; i++) {
		float t = ((float) i) / ((float) divisions);
		float xi = a[3] + t*(a[2] + t*(a[1] + t*a[0]));
		float yi = b[3] + t*(b[2] + t*(b[1] + t*b[0]));
		spline[i] = CGPointMake(xi, yi);
	}

	UIBezierPath *splinePath = [UIBezierPath bezierPath];
	[splinePath moveToPoint:spline[0]];

    for (int i=0; i < divisions+1; i++) {
		[splinePath addLineToPoint:spline[i]];
    }
    free(spline);
	return splinePath;
}

UIBezierPath *PSPDFSplineWithPointArray(NSArray *pointArray, CGFloat lineWidth) {
	UIBezierPath *completeSpline = [UIBezierPath bezierPath];

	@autoreleasepool {
		if ([pointArray count] < 2) {
			CGPoint lastPoint = [[pointArray lastObject] CGPointValue];
			pointArray = [pointArray arrayByAddingObject:[NSValue valueWithCGPoint:CGPointMake(lastPoint.x+0.001,lastPoint.y+0.001)]];
		}

		CGPoint p1 = [pointArray[0] CGPointValue];
		CGPoint p2 = [pointArray[1] CGPointValue];
		CGPoint p0 = CGPointMake(p1.x - (p2.x - p1.x), p1.y - (p2.y - p1.y));

		CGPoint pn = [pointArray[[pointArray count]-1] CGPointValue];
		CGPoint pn_1 = [pointArray[[pointArray count]-2] CGPointValue];
		CGPoint plast = CGPointMake(pn.x - (pn_1.x - pn.x), pn.y - (pn_1.y - pn.y));

		NSMutableArray *allPoints = [pointArray mutableCopy];
		[allPoints insertObject:[NSValue valueWithCGPoint:p0] atIndex:0];
		[allPoints addObject:[NSValue valueWithCGPoint:plast]];

		completeSpline.lineJoinStyle = kCGLineJoinRound;
		completeSpline.lineCapStyle = kCGLineCapRound;

		int splineCount = [pointArray count] - 1;
		int i = 0;
		for (; i < splineCount; i++) {
			CGPoint sp1 = [allPoints[i] CGPointValue];
			CGPoint sp2 = [allPoints[i+1] CGPointValue];
			CGPoint sp3 = [allPoints[i+2] CGPointValue];
			CGPoint sp4 = [allPoints[i+3] CGPointValue];

			UIBezierPath *splinePart = PSPDFSplinePathFromPoints(sp1, sp2, sp3, sp4, 5);
			[splinePart setLineWidth:lineWidth];
			[splinePart setLineCapStyle:kCGLineCapRound];
			[splinePart setLineJoinStyle:kCGLineJoinRound];
			[completeSpline appendPath:splinePart];
		}

		return completeSpline;
	}
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - CGPDF* conversion functions

CGColorSpaceRef PSPDFCreateColorSpaceFromPDFArray(CGPDFArrayRef colorSpaceArray);
CGFloat *PSPDFCreateDecodedValuesFromImageDictionary(CGPDFDictionaryRef dict, CGColorSpaceRef cgColorSpace, NSInteger bitsPerComponent);
UIImage *PSPDFCopyImageFromCGPDFStream(CGPDFStreamRef myStream);

// prototypes for ignore set
id PSPDFConvertPDFObjectWithIgnoreSet(CGPDFObjectRef objectRef, NSMutableSet *ignoreSet);
NSArray *PSPDFConvertPDFArrayWithIgnoreSet(CGPDFArrayRef pdfArray, NSMutableSet *ignoreSet);
NSDictionary *PSPDFConvertPDFDictionaryWithIgnoreSet(CGPDFDictionaryRef pdfDict, NSMutableSet *ignoreSet);

static id PSPDFConvertPDFObjectOrNSNull(CGPDFObjectRef objectRef, NSMutableSet *ignoreSet) {
    if ([ignoreSet containsObject:BOXED(objectRef)]) return nil;

    return PSPDFConvertPDFObjectWithIgnoreSet(objectRef, ignoreSet) ?: [NSNull null];
}

id PSPDFConvertPDFObject(CGPDFObjectRef objectRef) {
    return PSPDFConvertPDFObjectWithIgnoreSet(objectRef, [NSMutableSet set]);
}

id PSPDFConvertPDFObjectWithIgnoreSet(CGPDFObjectRef objectRef, NSMutableSet *ignoreSet) {
    if (!objectRef) return nil; // return early if nil
    
    // check ignore set and add object if not found
    if ([ignoreSet containsObject:BOXED(objectRef)]) return nil;
    [ignoreSet addObject:BOXED(objectRef)];

    id result = nil;
    CGPDFObjectType objectType = CGPDFObjectGetType(objectRef);
    switch (objectType) {
        case kCGPDFObjectTypeString: {
            CGPDFStringRef objectString;
            if (CGPDFObjectGetValue(objectRef, objectType, &objectString)) {
                result = (NSString *)CFBridgingRelease(CGPDFStringCopyTextString(objectString));
            }
        }break;
        case kCGPDFObjectTypeInteger: {
            CGPDFInteger objectInteger;
            if (CGPDFObjectGetValue(objectRef, objectType, &objectInteger)) {
                result = @(objectInteger);
            }
        }break;
        case kCGPDFObjectTypeBoolean: {
            CGPDFBoolean objectBool;
            if (CGPDFObjectGetValue(objectRef, objectType, &objectBool)) {
                result = @(objectBool);
            }
        }break;
        case kCGPDFObjectTypeArray: {
            CGPDFArrayRef objectArray;
            if (CGPDFObjectGetValue(objectRef, objectType, &objectArray)) {
                result = PSPDFConvertPDFArrayWithIgnoreSet(objectArray, ignoreSet);
            }
        }break;
        case kCGPDFObjectTypeNull: {
            result = [NSNull null];
        }break;
        case kCGPDFObjectTypeName: {
            const char *objectName = nil;
            if (CGPDFObjectGetValue(objectRef, objectType, &objectName)) {
                result = @(objectName);
            }
        }break;
        case kCGPDFObjectTypeReal: {
            CGPDFReal objectReal;
            if (CGPDFObjectGetValue(objectRef, objectType, &objectReal)) {
                result = @(objectReal);
            }
        }break;
        case kCGPDFObjectTypeDictionary: {
            CGPDFDictionaryRef objectDictionary;
            if (CGPDFObjectGetValue(objectRef, objectType, &objectDictionary)) {
                result = PSPDFConvertPDFDictionaryWithIgnoreSet(objectDictionary, ignoreSet);
            }
        }break;
        case kCGPDFObjectTypeStream: {
            CGPDFStreamRef streamRef;
            if (CGPDFObjectGetValue(objectRef, objectType, &streamRef)) {
                result = [[PSPDFStream alloc] initWithStream:streamRef];
                /*
                CGPDFDictionaryRef streamDict = CGPDFStreamGetDictionary(streamRef);
                // try to convert as image, even if it's unlikely to succeed.
                //result = PSPDFCopyImageFromCGPDFStream(streamRef);
                if (!result) {
                    result = PSPDFConvertPDFDictionaryWithIgnoreSet(streamDict, ignoreSet);
                }
                
                 CGPDFDataFormat dataFormat;
                 CFDataRef streamData = CGPDFStreamCopyData(streamRef, &dataFormat);
                 
                 CFIndex length = CFDataGetLength(streamData);
                 const UInt8* bytes = CFDataGetBytePtr(streamData);
                 
                 NSStringEncoding encoding = (length > 1 && bytes[0] == 0xFE && bytes[1] == 0xFF) ? NSUnicodeStringEncoding : NSUTF8StringEncoding;
                 result = [[NSString alloc] initWithBytes:bytes length:length encoding:encoding];
                 */
            }
        }
    }
    return result;
}

// only convert if it's a string.
NSString *PSPDFConvertPDFObjectAsString(CGPDFObjectRef objectRef) {
    NSString *result = nil;
    CGPDFObjectType objectType = CGPDFObjectGetType(objectRef);
    switch (objectType) {
        case kCGPDFObjectTypeString: {
            CGPDFStringRef objectString;
            if (CGPDFObjectGetValue(objectRef, objectType, &objectString)) {
                result = (NSString *)CFBridgingRelease(CGPDFStringCopyTextString(objectString));
            }
        }break;
        case kCGPDFObjectTypeName: {
            const char *objectName = nil;
            if (CGPDFObjectGetValue(objectRef, objectType, &objectName)) {
                result = @(objectName);
            }
        }break;
        default: break;
    }
    return result;
}


NSArray *PSPDFConvertPDFArray(CGPDFArrayRef pdfArray) {
    return PSPDFConvertPDFArrayWithIgnoreSet(pdfArray, [NSMutableSet setWithObject:BOXED(pdfArray)]);
}

NSArray *PSPDFConvertPDFArrayWithIgnoreSet(CGPDFArrayRef pdfArray, NSMutableSet *ignoreSet) {
    NSMutableArray *array = [NSMutableArray array];
    size_t pdfArrayCount = CGPDFArrayGetCount(pdfArray);
    for(int i=0; i<pdfArrayCount; i++) {
        CGPDFObjectRef objectRef;
        CGPDFArrayGetObject(pdfArray, i, &objectRef);
        if ((CGPDFObjectRef)pdfArray != objectRef) {
            id arrayObject = PSPDFConvertPDFObjectOrNSNull(objectRef, ignoreSet);
            if (arrayObject) [array addObject:arrayObject];
        }
    }
    return array;
}

PSPDFStream *PSPDFDictionaryGetStreamForPath(CGPDFDictionaryRef pdfDict, NSString *keyPath) {
    id anObject = PSPDFDictionaryGetObjectForPath(pdfDict, keyPath);
    return [anObject isKindOfClass:[PSPDFStream class]] ? anObject : nil;
}

id PSPDFDictionaryGetObjectForPath(CGPDFDictionaryRef pdfDict, NSString *keyPath) {
    void *containerRef = pdfDict;
    CGPDFObjectRef objectRef = NULL;
    CGPDFDictionaryRef streamDictRef = NULL;
    NSArray *keys = [keyPath componentsSeparatedByString:@"."];
    for (NSString *key in keys) {
        if ([key hasPrefix:@"#"]) {
            NSUInteger index = [[key substringFromIndex:1] integerValue];
            // add support for stream extraction
            if (CGPDFObjectGetType(objectRef) == kCGPDFObjectTypeStream) {
                streamDictRef = containerRef = CGPDFStreamGetDictionary(containerRef);
            }else {
                streamDictRef = nil;
                if (!CGPDFArrayGetObject(containerRef, index, &objectRef)) break;
            }
        }else {
            streamDictRef = nil;
            if (!CGPDFDictionaryGetObject(containerRef, [key UTF8String], &objectRef)) break;
        }

        // get collection
        if (!streamDictRef) {
            CGPDFObjectType objectType = CGPDFObjectGetType(objectRef);
            if (objectType == kCGPDFObjectTypeDictionary || objectType == kCGPDFObjectTypeArray || objectType == kCGPDFObjectTypeStream) {
                if (!CGPDFObjectGetValue(objectRef, objectType, &containerRef)) break;
            }else break;
        }
    }
    return streamDictRef ? PSPDFConvertPDFDictionary(streamDictRef) : PSPDFConvertPDFObject(objectRef);
}


// just temporary for the dictionary apply function
#define kPSPDFIgnoreObjectsSet @"PSPDFIgnoreObjectsSet"

static void PSPDFCopyDictionaryValuesApplyFunction(const char *keyCStr, CGPDFObjectRef object, void *info) {
    NSMutableDictionary *dict = (__bridge NSMutableDictionary *)(info);
    NSMutableSet *ignoreObjectsSet = dict[kPSPDFIgnoreObjectsSet];
    id resultObject;
    if (![ignoreObjectsSet containsObject:BOXED(object)]) {
        resultObject = PSPDFConvertPDFObjectOrNSNull(object, ignoreObjectsSet);
    }else {
        // if we don't break the recursion, we will crash with a stack overflow at some point.
        resultObject = @"<Stopped recursion>";
    }
    NSString *key = @(keyCStr);
    dict[key] = resultObject;
}

NSDictionary *PSPDFConvertPDFDictionary(CGPDFDictionaryRef pdfDict) {
    return PSPDFConvertPDFDictionaryWithIgnoreSet(pdfDict, [NSMutableSet setWithObject:BOXED((pdfDict))]);
}

NSDictionary *PSPDFConvertPDFDictionaryWithIgnoreSet(CGPDFDictionaryRef pdfDict, NSMutableSet *ignoreSet) {
    if (!pdfDict) return nil;
    
    size_t dictionaryCount = CGPDFDictionaryGetCount(pdfDict);
    NSMutableDictionary *convertedDict = [NSMutableDictionary dictionaryWithCapacity:dictionaryCount];
    convertedDict[kPSPDFIgnoreObjectsSet] = ignoreSet;
    CGPDFDictionaryApplyFunction(pdfDict, PSPDFCopyDictionaryValuesApplyFunction, (__bridge void *)convertedDict);
    [convertedDict removeObjectForKey:kPSPDFIgnoreObjectsSet]; // remove dict again
    return convertedDict;
}

inline NSString *PSPDFDictionaryGetString(CGPDFDictionaryRef pdfDict, NSString *key) {
    NSString *string = nil;
    CGPDFObjectRef objectRef = NULL;
    if (CGPDFDictionaryGetObject(pdfDict, [key UTF8String], &objectRef)) {
        string = PSPDFConvertPDFObjectAsString(objectRef);
    }
    return string;
}

inline NSString *PSPDFArrayGetString(CGPDFArrayRef pdfArray, size_t index) {
    NSString *string = nil;
    CGPDFObjectRef objectRef = NULL;
    if (CGPDFArrayGetObject(pdfArray, index, &objectRef)) {
        string = PSPDFConvertPDFObjectAsString(objectRef);
    }
    return string;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - PDF Date conversion

/// Converts a string formatted after PDF Reference 7.9.4 to an NSDate.
/*
 Date values used in a PDF shall conform to a standard date format, which closely follows that of the international standard ASN.1 (Abstract Syntax Notation One), defined in ISO/IEC 8824. A date shall be a text string of the form
 ( D : YYYYMMDDHHmmSSOHH ' mm ) where:
 YYYY shall be the year
 MM shall be the month (01–12) DD shall be the day (01–31) HH shall be the hour (00–23)
 mm shall be the minute (00–59) SS shall be the second (00–59)
 
 O shall be the relationship of local time to Universal Time (UT), and shall be denoted by one of the characters PLUS SIGN (U+002B) (+), HYPHEN-MINUS (U+002D) (-), or LATIN CAPITAL LETTER Z (U+005A) (Z) (see below)
 HH followed by APOSTROPHE (U+0027) (') shall be the absolute value of the offset from UT in hours (00–23)
 mm shall be the absolute value of the offset from UT in minutes (00–59)
 The prefix D: shall be present, the year field (YYYY) shall be present and all other fields may be present but only if all of their preceding fields are also present. The APOSTROPHE following the hour offset field (HH) shall only be present if the HH field is present. The minute offset field (mm) shall only be present if the APOSTROPHE following the hour offset field (HH) is present. The default values for MM and DD shall be both 01; all other numerical fields shall default to zero values. A PLUS SIGN as the value of the O field signifies that local time is later than UT, a HYPHEN-MINUS signifies that local time is earlier than UT, and the LATIN CAPITAL LETTER Z signifies that local time is equal to UT. If no UT information is specified, the relationship of the specified time to UT shall be considered to be GMT. Regardless of whether the time zone is specified, the rest of the date shall be specified in local time.
 EXAMPLE For example, December 23, 1998, at 7:52 PM, U.S. Pacific Standard Time, is represented by the string D : 199812231952 - 08 ' 00
 */
#include <time.h>
#include <xlocale.h>
#define PSPDFDATE_MAX_LEN 25
NSDate *PSPDFDateFromString(NSString *pdfDateString) {
    if (!pdfDateString) return nil;

    // get C string
    const char *str = [pdfDateString cStringUsingEncoding:NSUTF8StringEncoding];
    size_t len = strlen(str);
    if (len < 6) return nil; // absolute minimum is D:YYYY

    // check for header D:
    size_t dateStart = 2; // (D:)
    if (!str[0] == 'D') { PSPDFLogWarning(@"Invalid date: %@", pdfDateString); return nil; }
    while (dateStart<len && str[dateStart-1] == ' ') dateStart++;
    if (!str[dateStart-1] == ':') { PSPDFLogWarning(@"Invalid date: %@", pdfDateString); return nil; }
    while (dateStart<len && str[dateStart] == ' ') dateStart++;

    // build new string
    char newStr[PSPDFDATE_MAX_LEN];
    bzero(newStr, PSPDFDATE_MAX_LEN);

    // 10 is the minimum block length (e.g. 1998122319) w/o timezone data
    // mimimum time zone length is 1(Z), but there might be whitespaces.
    size_t timeZoneStart = dateStart+10;
    if (len > timeZoneStart+1) {

        // minutes / seconds are optional
        while (timeZoneStart<len && isdigit(str[timeZoneStart])) timeZoneStart++;
        
        // ignore whitespace
        while (timeZoneStart<len && str[timeZoneStart] == ' ') timeZoneStart++;

        // check local/UTC time relationship marker
        if (str[timeZoneStart] == 'Z') {
            memcpy(newStr, str+dateStart, timeZoneStart);
            strncpy(newStr + timeZoneStart, "+0000\0", 6);
        }
        else if (str[timeZoneStart] == '+' || str[timeZoneStart] == '-') {

            // copy date and time
            memcpy(newStr, str+dateStart, timeZoneStart-dateStart);

            // full up remaining missing time data with 0's.
            for (int i=timeZoneStart; i<14; i++) {
                newStr[i] = 0;
            }

            // set time zone start symbol (+ or -)
            newStr[strlen(newStr)] = str[timeZoneStart];

            // get and copy timezone hour location
            size_t timezone_hour_loc = timeZoneStart+1;
            if (timezone_hour_loc+2<len) {
                while (timezone_hour_loc<len && str[timezone_hour_loc] == ' ') timezone_hour_loc++;
                memcpy(newStr+strlen(newStr), str + timezone_hour_loc, MIN(len-timezone_hour_loc, 2));

                // look for timezone minutes
                size_t timezone_minute_location = timezone_hour_loc+2;
                if (timezone_minute_location+2<len) {
                    // allow spaces and '
                    while (timezone_minute_location<len && (str[timezone_minute_location] == ' ' || str[timezone_minute_location] == '\'')) timezone_minute_location++;
                    memcpy(newStr+strlen(newStr), str + timezone_minute_location, MIN(len-timezone_minute_location, 2));
                }
            }
        }else {
           PSPDFLogWarning(@"Invalid date: %@", pdfDateString); return nil;
        }
    }
    // Fallback: date was already well-formatted OR any other case (bad-formatted)
    else {
        memcpy(newStr, str, len > PSPDFDATE_MAX_LEN - 1 ? PSPDFDATE_MAX_LEN - 1 : len);
    }

    // Add null terminator
    newStr[sizeof(newStr) - 1] = 0;

    // If the format string does not contain enough conversion specifications to completely specify the resulting struct tm, the unspecified members of tm are left untouched.  For example, if format is ``%H:%M:%S'', only tm_hour, tm_sec and tm_min will be modified.  If time relative to today is desired, initialize the tm structure with today's date before passing it to strptime().
    struct tm tm = {
        .tm_sec = 0,
        .tm_min = 0,
        .tm_hour = 0,
        .tm_mday = 0,
        .tm_mon = 0,
        .tm_year = 0,
        .tm_wday = 0,
        .tm_yday = 0,
        .tm_isdst = -1,
    };

    if (strptime_l(newStr, "%Y%m%d%H%M%S%z", &tm, NULL) == NULL) { // %FT%T%z
        PSPDFLogWarning(@"Invalid date: %@", pdfDateString); return nil;
    }

    return [NSDate dateWithTimeIntervalSince1970:mktime(&tm)];
}

NSString *PSPDFStringFromDate(NSDate *date) {
    if (!date) return nil;
    
	time_t rawtime = (time_t)[date timeIntervalSince1970];
	struct tm *timeinfo = gmtime(&rawtime);

    char buffer[80];
	strftime(buffer, 80, "D:%Y%m%d%H%M%S+00'00", timeinfo);

	return [NSString stringWithCString:buffer encoding:NSUTF8StringEncoding];
}
