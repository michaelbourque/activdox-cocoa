//
//  PSPDFScrollView.m
//  PSPDFKit
//
//  Copyright 2011-2012 Peter Steinberger. All rights reserved.
//

#import "PSPDFKit.h"
#import "PSPDFViewController+Internal.h"
#import "PSPDFLoupeView.h"
#import "PSPDFTextSelectionView.h"
#import "PSPDFTextBlock.h"
#import "PSPDFTextParser.h"
#import "PSPDFKitGlobal+Internal.h"
#import "PSPDFAnnotationController.h"
#import "PSPDFYouTubeAnnotationView.h"
#import <QuartzCore/QuartzCore.h>
#import <MessageUI/MessageUI.h>

@interface PSPDFCompoundView : UIView @end @implementation PSPDFCompoundView @end
@interface PSPDFLeftPageView : PSPDFPageView @end @implementation PSPDFLeftPageView @end
@interface PSPDFRightPageView : PSPDFPageView @end @implementation PSPDFRightPageView @end

@interface PSPDFScrollView() {
    struct {
        BOOL scrollViewIsDecelerating:1;
        BOOL manuallyCenteredFrame:1;
        BOOL isShowingRightPage:1;
        BOOL isSettingUpPage:1;
        BOOL menuWillHideCalled:1;
        BOOL menuDidHideDuringThisTouchEvent:1;
        BOOL blockLayoutSubviewCentering:1;
        BOOL isZoomingProgrammatically:1;
    }_flags;
}
@property (atomic, strong) PSPDFDocument *document;
@property (nonatomic, strong) UIView *compoundView;
@property (nonatomic, strong) PSPDFPageView *leftPage;
@property (nonatomic, strong) PSPDFPageView *rightPage;
@property (nonatomic, strong) PSPDFLoupeView *loupeView;
@property (nonatomic, assign, getter=isRotationActive) BOOL rotationActive;
@property (nonatomic, assign, getter=isAnimatingZoomIn) BOOL animatingZoomIn;
@property (nonatomic, strong) UITapGestureRecognizer *singleTapGesture;
@property (nonatomic, strong) UITapGestureRecognizer *doubleTapGesture;
@property (nonatomic, strong) UILongPressGestureRecognizer *longPressGesture;
@end

@implementation PSPDFScrollView

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (id)initWithFrame:(CGRect)frame {
    if ((self = [super initWithFrame:frame])) {
        self.showsVerticalScrollIndicator = kPSPDFDebugScrollViews;
        self.showsHorizontalScrollIndicator = kPSPDFDebugScrollViews;
        _shadowStyle = PSPDFShadowStyleFlat;
        self.directionalLockEnabled = YES; // like iBooks
        self.decelerationRate = UIScrollViewDecelerationRateFast;
        self.backgroundColor = [UIColor clearColor];
        _zoomingEnabled = YES;

        if (kPSPDFDebugScrollViews) {
            self.backgroundColor = [[UIColor redColor] colorWithAlphaComponent:0.8];
            self.alpha = 0.6f;
        }
        self.delegate = self;

        NSNotificationCenter *dnc = [NSNotificationCenter defaultCenter];
        [dnc addObserver:self selector:@selector(menuWillHide:) name:UIMenuControllerWillHideMenuNotification object:nil];
        [dnc addObserver:self selector:@selector(menuDidHide:) name:UIMenuControllerDidHideMenuNotification object:nil];

        // for rotation - view resizes itself
        self.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;

        // needed for shadows in subviews
        self.clipsToBounds = NO;

        // disable scrolling to top
        self.scrollsToTop = NO;

        // zooming in
        _doubleTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(doubleTapped:)];
        _doubleTapGesture.numberOfTapsRequired = 2;
        _doubleTapGesture.delegate = self;
        [self addGestureRecognizer:_doubleTapGesture];

        // HUD
        _singleTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTapped:)];
        _singleTapGesture.numberOfTapsRequired = 1;
        _singleTapGesture.delaysTouchesBegan = NO;
        _singleTapGesture.delaysTouchesEnded = NO;
        _singleTapGesture.delegate = self;
        [_singleTapGesture requireGestureRecognizerToFail:_doubleTapGesture];
        [self addGestureRecognizer:_singleTapGesture];

        // selection, annotations
        _longPressGesture = [[PSPDFLongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPress:)];
        _longPressGesture.minimumPressDuration = 0.35; // default is 0.5
        _longPressGesture.delegate = self;
        [self addGestureRecognizer:_longPressGesture];

        // create compound view
        _compoundView = [[PSPDFCompoundView alloc] initWithFrame:CGRectZero];
        _compoundView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        _compoundView.opaque = YES;
        self.compoundView.backgroundColor = [UIColor clearColor];
        [self addSubview:self.compoundView];
    }
    return self;
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    self.delegate = nil;
    _pdfController = nil;
}

- (NSString *)description {
    return [NSString stringWithFormat:@"%@ contentSize:%@ - page: %d>", [super description], NSStringFromCGSize(self.contentSize), self.page];
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Override layoutSubviews to center content

// also called upon rotation events
- (void)layoutSubviews {
    [super layoutSubviews];

    // don't use .bounds here; as the compountView has a transform set.
    self.compoundView.frame = (CGRect){.size=self.compoundView.frame.size};
    _flags.manuallyCenteredFrame = NO;


    // center the image as it becomes smaller than the size of the screen
    CGSize boundsSize = [self boundSize];

    CGRect frameToCenter = self.compoundView.frame;
    if (frameToCenter.size.width == 0 || frameToCenter.size.height == 0) {
        // only warn with document set, else it's expected to have a zero frame
        if (self.document.isValid) {
            PSPDFLogWarning(@"Compount frame is zero, aborting!");
        }
        return;
    }

    // This centering code works, but will breat the zoom to a specific block on non-perfect-sized documents (documents that don't match widht and height ratio of the device)
    // We will only use this for centering when we zoom out programmatically to work around an UIKit bug where the scrollView becomes unresponsive. (DTS running)
    if (_flags.blockLayoutSubviewCentering) {
        // center horizontally
        if (frameToCenter.size.width < boundsSize.width)
            frameToCenter.origin.x = roundf((boundsSize.width - frameToCenter.size.width) / 2);
        else frameToCenter.origin.x = 0;

        // center vertically
        if (frameToCenter.size.height < boundsSize.height)
            frameToCenter.origin.y = roundf((boundsSize.height - frameToCenter.size.height) / 2);
        else frameToCenter.origin.y = 0;

        self.compoundView.frame = frameToCenter;
    }

    // ensure view is centered
    self.contentOffset = self.contentOffset;
    [self configureShadow];

    // we zoomed programmatically, but without animation
    if (_flags.isZoomingProgrammatically) _flags.blockLayoutSubviewCentering = NO;
}


// Rather than the default behaviour of a {0,0} offset when an image is too small to fill the UIScrollView we're going to return an offset that centres the image in the UIScrollView instead.
- (void)setContentOffset:(CGPoint)offset {
    if (self.compoundView != nil && !_flags.blockLayoutSubviewCentering) {
        CGSize zoomViewSize = self.compoundView.frame.size;
        CGSize scrollViewSize = self.bounds.size;

        if (zoomViewSize.width < scrollViewSize.width) {
            offset.x = -roundf((scrollViewSize.width - zoomViewSize.width) / 2.0);
        }

        if (zoomViewSize.height < scrollViewSize.height) {
            offset.y = -roundf((scrollViewSize.height - zoomViewSize.height) / 2.0);
        }
    }

    if (!CGPointEqualToPoint(self.contentOffset, offset)) {
        super.contentOffset = offset;
    }
}

- (void)ensureContentIsCentered {
    self.contentOffset = self.contentOffset;
    [self setNeedsLayout];
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public / Properties

// lazy-init
- (PSPDFPageView *)leftPage {
    if (!_leftPage) {
        Class pageViewClass = [self.pdfController classForClass:[PSPDFPageView class]];
        pageViewClass = ![pageViewClass isEqual:[PSPDFPageView class]] ? pageViewClass : [PSPDFLeftPageView class];
        _leftPage = [[pageViewClass alloc] initWithFrame:self.bounds pdfController:self.pdfController];
        [self.compoundView addSubview:_leftPage];
    }
    return _leftPage;
}

// lazy-init
- (PSPDFPageView *)rightPage {
    if (!_rightPage) {
        Class pageViewClass = [self.pdfController classForClass:[PSPDFPageView class]];
        pageViewClass = ![pageViewClass isEqual:[PSPDFPageView class]] ? pageViewClass : [PSPDFRightPageView class];
        _rightPage = [[pageViewClass alloc] initWithFrame:self.bounds pdfController:self.pdfController];
        _rightPage.hidden = YES; // default-hide
        [self.compoundView addSubview:_rightPage];
    }
    return _rightPage;
}

- (void)setZoomingEnabled:(BOOL)zoomingEnabled {
    _zoomingEnabled = zoomingEnabled;
    self.pinchGestureRecognizer.enabled = zoomingEnabled;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UIScrollView

// will not be called when the user zooms via gestures.
- (void)setZoomScale:(float)scale animated:(BOOL)animated {
    // only set if we zoom IN, as we don't wanna block tiling when zooming out
    self.animatingZoomIn = scale > self.zoomScale;
    _flags.isZoomingProgrammatically = YES;

    // blockLayoutSubviewCentering is a workaround for a UIKit bug.
    _flags.blockLayoutSubviewCentering = scale < self.zoomScale;
    [super setZoomScale:scale animated:animated];
    _flags.isZoomingProgrammatically = animated;

    // don't forward initial zoomScale of 1
    if (!_flags.isSettingUpPage) {
        // this scrollview delegate doesn't actually exist, but it's useful so we extend it.
        for (PSPDFPageView *pageView in [self visiblePageViews]) {
            [pageView pspdf_scrollView:self willZoomToScale:scale animated:animated];
        }
    }
}

- (void)zoomToRect:(CGRect)rect animated:(BOOL)animated {
    // calculate current rect
    CGRect visibleRect = (CGRect){self.contentOffset,self.bounds.size};
    float theScale = 1.0 / self.zoomScale;
    visibleRect.origin.x *= theScale;
    visibleRect.origin.y *= theScale;
    visibleRect.size.width *= theScale;
    visibleRect.size.height *= theScale;
    self.animatingZoomIn = fabs(visibleRect.size.width * visibleRect.size.height) > fabs(rect.size.width * rect.size.height);

    [super zoomToRect:rect animated:animated];

    // if this is set without animation, ensure render views are updated.
    if (!animated) {
        for (PSPDFPageView *pageView in [self visiblePageViews]) {
            [pageView updateRenderView];
        }
    }
}

- (void)setContentOffset:(CGPoint)contentOffset animated:(BOOL)animated {
    [super setContentOffset:contentOffset animated:animated];

    // if this is set without animation, ensure render views are updated.
    if (!animated) {
        for (PSPDFPageView *pageView in [self visiblePageViews]) {
            [pageView updateRenderView];
        }
    }
}

// Debug helper
#if 0
- (void)setContentSize:(CGSize)contentSize {
    if (!CGSizeEqualToSize(contentSize, self.contentSize)) {
        //NSLog(@"new: %@ old: %@", NSStringFromCGSize(self.contentSize), NSStringFromCGSize(contentSize));
        [super setContentSize:contentSize];
    }
}
#endif

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Configure scrollView to display new pdf page

- (void)releaseDocument {
    [_leftPage prepareForReuse];
    [_rightPage prepareForReuse];

    // remove references
    self.document = nil;
    self.pdfController = nil;
}

// used with rotation
- (void)switchPages {
    PSPDFPageView *tmpPage = _leftPage;
    _leftPage = _rightPage;
    _rightPage = tmpPage;
}

- (void)layoutPages {
    [_leftPage updateRenderView];
    [_rightPage updateRenderView];

    NSUInteger pageId = self.page;
    NSInteger leftPage = pageId;
    NSInteger rightPage = 0;

    // used for delegates
    BOOL newLeftPage = NO, newRightPage = NO;

    // only show double pages!
    if (self.isDoublePageMode) {
        pageId *= 2; // we show two pages at once!

        if (self.doublePageModeOnFirstPage) {
            leftPage = pageId;
            rightPage = pageId+1;
        }else {
            leftPage = pageId-1;
            rightPage = pageId;
            // compensate for first page single in double page mode
            if (leftPage < 0) {
                leftPage++;
                rightPage = -1;
            }
        }
    }else {
        rightPage = -1;
    }

    // destroy left page if invalid (first page in double page mode)
    if (leftPage < 0) {
        [_leftPage removeFromSuperview];
        [_leftPage prepareForReuse];
    }

    // if we're displaying the *right* page and switch to double page mode, and they snap, exchange.
    if (_leftPage.page == rightPage) {
        [self switchPages];
    }

    CGRect compountViewRect;
    CGRect leftPageRect = CGRectZero;
    if (leftPage >= 0) {
        leftPageRect = [self.document rectBoxForPage:leftPage];
    }
    CGSize compountRawPdfSize = leftPageRect.size;
    CGRect rightPageRect = CGRectZero;
    if (self.isDoublePageMode && rightPage < [self.document pageCount]) {
        rightPageRect = [self.document rectBoxForPage:rightPage];
        compountRawPdfSize = CGSizeMake(floorf(leftPageRect.size.width+rightPageRect.size.width-1), floorf(fmaxf(leftPageRect.size.height, rightPageRect.size.height)));
    }

    CGSize boundSize = [self boundSize];
    boundSize.width -= self.pdfController.padding.width;
    boundSize.height -= self.pdfController.padding.height;
    CGFloat scale = PSPDFScaleForSizeWithinSizeWithOptions(compountRawPdfSize, boundSize, self.isZoomingSmallDocumentsEnabled, self.isFitToWidthEnabled);

    // don't animate if we're reloading the view.
    BOOL delayPageAnnotations = ![self.pdfController isViewWillAppearing];

    if (leftPage >= 0) {
        if (!self.leftPage.document || self.leftPage.page != leftPage) {newLeftPage = YES;}
        [self.leftPage displayDocument:self.document page:leftPage pageRect:leftPageRect scale:scale delayPageAnnotations:delayPageAnnotations pdfController:_pdfController];
    }

    _flags.isShowingRightPage = [self isDoublePageMode] && rightPage < [self.document pageCount] && rightPage >= 0;
    if (_flags.isShowingRightPage) {
        if (!self.rightPage.document || self.rightPage.page != rightPage) {newRightPage = YES;}
        [self.rightPage displayDocument:self.document page:rightPage pageRect:rightPageRect scale:scale delayPageAnnotations:delayPageAnnotations pdfController:_pdfController];

        CGFloat leftPageHeight = _leftPage ? _leftPage.frame.size.height : 0.f;
        CGFloat rightPageHeight = _rightPage ? _rightPage.frame.size.height : 0.f;
        CGFloat leftPageWidth = _leftPage ? _leftPage.frame.size.width : 0.f;
        CGFloat rightPageWidth = _rightPage ? _rightPage.frame.size.width : 0.f;

        // subtract one against hair lines
        compountViewRect = [self roundCompoundViewSize:CGRectMake(0, 0, roundf(leftPageWidth + rightPageWidth - 1), floorf(fmaxf(leftPageHeight, rightPageHeight)))];

        _rightPage.alpha = 1.f;
        _leftPage.alpha = 1.f;
        _rightPage.hidden = NO;
    }else {
        compountViewRect = [self roundCompoundViewSize:_leftPage.frame];

        // Stop re-rendering of content for the page that fades out. (requestRenderedFullPageImage will return early)
        self.rightPage.suspendUpdate = YES;

        // hack: create right page to support super-smooth rotations (in exchange to some minor overhead)
        self.rightPage.frame = self.leftPage.frame;
        // don't hide if rotation is active - we want a nice effect.
        self.rightPage.hidden = !self.rotationActive;

        // if there is a rightPage (due to rotation) move it (but don't move if we use the right page in portrait)
        if ([self.pdfController isRightPageInDoublePageMode:_leftPage.page]) {
            CGRect frame = _rightPage.frame;
            frame.origin.x = _leftPage.frame.origin.x - _leftPage.frame.size.width;
            _rightPage.frame = frame;
        }else {
            CGRect frame = _rightPage.frame;
            frame.origin.x = _leftPage.frame.origin.x + _leftPage.frame.size.width;
            _rightPage.frame = frame;
        }

        // if we have documents that are not full-width, fade the page out
        if (_leftPage.frame.size.width < self.frame.size.width) {
            _rightPage.alpha = 0.f;
        }
    }
    // set rect for compound image view
    self.compoundView.frame = compountViewRect;

    // setup UIScrollView contentSize
    self.contentSize = self.compoundView.frame.size;

    CGSize size = PSPDFSizeForScale(leftPageRect.size, scale);
    _leftPage.frame = CGRectMake(0, 0, size.width, size.height); // make size equal

    if (self.isDoublePageMode) {
        size = PSPDFSizeForScale(rightPageRect.size, scale);
        // move one pixel to compensate ugly hair lines that show up on zooming.
        _rightPage.frame = CGRectMake(_leftPage.frame.size.width-1, 0, size.width, size.height); // make size equal
    }

    // send delegate events
    if (newLeftPage)  { [self.pdfController delegateDidLoadPageView:_leftPage]; }
    if (newRightPage) { [self.pdfController delegateDidLoadPageView:_rightPage]; }
}

// because we use ceil() on size calulations we might be larger than we should.
- (CGRect)roundCompoundViewSize:(CGRect)compountViewRect {
    // if we get rounding errors, allow up to two pixels of variants to be removed
    CGFloat maxHeight = self.superview.bounds.size.height + self.superview.frame.origin.y*2;
    CGFloat heightDiff = compountViewRect.size.height - maxHeight;
    if (heightDiff > 0 && heightDiff < 3) {
        compountViewRect.size.height = fminf(compountViewRect.size.height, maxHeight);
    }
    CGFloat maxWidth = self.superview.bounds.size.width + self.superview.frame.origin.x*2;
    CGFloat widthDiff = compountViewRect.size.width - maxWidth;
    if (widthDiff > 0 && widthDiff < 3) {
        compountViewRect.size.width = fminf(compountViewRect.size.width, maxWidth);
    }
    return compountViewRect;
}

// not used in pageCurl mode
- (void)displayDocument:(PSPDFDocument *)document withPage:(NSUInteger)page {
    PSPDFLogVerbose(@"%@ - %d", document.title, page);
    _flags.isSettingUpPage = YES;
    self.document = document;
    self.page = page;
    float maxZoomScale = self.pdfController.maximumZoomScale;
    self.maximumZoomScale = maxZoomScale > 0 ? maxZoomScale : 5.f;
    self.minimumZoomScale = self.pdfController.minimumZoomScale;

    // if scrollsToTop isn't yet used, enable it here
    self.scrollsToTop = self.pdfController.scrollDirection == PSPDFScrollDirectionHorizontal && self.pdfController.pageTransition != PSPDFPageScrollContinuousTransition;

    // reset state
    self.zoomScale = 1.f;
    self.contentOffset = CGPointZero;
    self.contentInset = UIEdgeInsetsZero;

    // setup pages & calculate new center
    [self layoutPages];
    [self setNeedsLayout];
    _flags.isSettingUpPage = NO;
}

// control special events needed for rotation
- (void)setRotationActive:(BOOL)rotationActive {
    _rotationActive = rotationActive;
    _leftPage.hidden = NO;
    _rightPage.hidden = NO;

    // hide right page after rotation is finished (rotationActive = NO)
    if (!rotationActive) {
        BOOL displayRightPage = _flags.isShowingRightPage && !rotationActive;
        _rightPage.hidden = !displayRightPage;

        // update shadow
        [self setNeedsLayout];
    }
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public

- (void)setShadowEnabled:(BOOL)shadowEnabled {
    if (shadowEnabled != _shadowEnabled) {
        _shadowEnabled = shadowEnabled;
        [self configureShadow];
    }
}

- (PSPDFLoupeView *)loupeView {
    if (!_loupeView) {
        _loupeView = [[PSPDFLoupeView alloc] initWithReferenceView:self];
        _loupeView.alpha = 0.0f;
    }
    return _loupeView;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Private

// returns all currently visible pageViews.
- (NSArray *)visiblePageViews {
    return [self.pdfController visiblePageViews];
}

// Returns the pageView behind point, or nil if not found.
- (PSPDFPageView *)pageViewForPoint:(CGPoint)point {
    PSPDFPageView *affectedPageView = nil;
    for (PSPDFPageView *pageView in [self visiblePageViews]) {

        // calculate actual pageRect (differs between scroll and pageCurl
        CGRect pageViewRect = [self convertRect:pageView.bounds fromView:pageView];
        if (CGRectContainsPoint(pageViewRect, point)) {
            affectedPageView = pageView; break;
        }
    }

    return affectedPageView;
}

- (void)setCurrentTouchEventAsProcessed {
    if (self.singleTapGesture.enabled) {
        self.singleTapGesture.enabled = NO;
        self.singleTapGesture.enabled = YES;
    }
}

// HUD and link-handling
- (void)singleTapped:(UITapGestureRecognizer *)recognizer {
    PSPDFViewController *pdfController = self.pdfController;

    // get frontmost view, handle touches to a link annotation
    CGPoint touchPoint = [recognizer locationInView:self];
    UIView *topmost = [self hitTest:touchPoint withEvent:nil];
    if ([topmost isKindOfClass:[PSPDFLinkAnnotationView class]]) {
        PSPDFLinkAnnotationView *annotationView = (PSPDFLinkAnnotationView *)topmost;
        PSPDFPageView *pageView = (PSPDFPageView *)annotationView.superview;
        if ([pageView isKindOfClass:[PSPDFPageView class]]) {
            // manually send event
            [annotationView flashBackground];
            CGPoint viewPoint = [recognizer locationInView:pageView];
            CGPoint annotationPoint = [pageView convertViewPointToPDFPoint:[recognizer locationInView:annotationView]];

            // if the delegate did not handle the tap on the annotation,
            if (![pdfController delegateDidTapOnAnnotation:annotationView.annotation annotationPoint:annotationPoint annotationView:annotationView pageView:pageView viewPoint:viewPoint]) {
                // invoke default behaviour for annotation
                [pdfController.annotationController handleTouchUpForAnnotationIgnoredByDelegate:annotationView];
            }
        }
        return;
    }

    PSPDFPageView *pageView = [self pageViewForPoint:touchPoint];

    __block BOOL tapHandled = NO;
    BOOL touchSemiProcessed = NO; // don't show/hide HUD, else continue

    // prepare check for pdf annotations
    if (!tapHandled) {
        for (PSPDFPageView *visiblePageView in [self visiblePageViews]) {
            if ([visiblePageView.selectionView.selectedGlyphs count] > 0) {
                [visiblePageView.selectionView discardSelection];
                tapHandled = YES;
            }
            if (visiblePageView.selectedAnnotation) {
                visiblePageView.selectedAnnotation = nil;
                // allows to directly click-through select another annotation.
                touchSemiProcessed = YES;
            }

            // forward tap to handle annotations
            if (!tapHandled) {
                tapHandled = [visiblePageView singleTapped:recognizer];
            }
        }
    }

    // If menu was hidden during this touch event, mark the touch as handled.
    if (!tapHandled && _flags.menuDidHideDuringThisTouchEvent) {
        _flags.menuDidHideDuringThisTouchEvent = NO;
        tapHandled = YES;
    }

    // If menu isn't hidden automatically, make it so!
    if (!tapHandled && [UIMenuController sharedMenuController].isMenuVisible) {
        [[UIMenuController sharedMenuController] setMenuVisible:NO animated:YES];
        tapHandled = YES;
    }

    // process delegate
    if (!tapHandled) {
        CGPoint tapPosition = [recognizer locationInView:pageView];
        tapHandled = [pdfController delegateDidTapOnPageView:pageView atPoint:tapPosition];
    }

    // if touch is not used, show/hide HUD
    if (!tapHandled && !touchSemiProcessed) {
        CGPoint screenPoint = [self.pdfController.view convertPoint:touchPoint fromView:self];

        BOOL shouldToggle = NO;
        if (self.zoomScale == 1.f) {
            CGFloat scrollOnTapPageMargin = pdfController.scrollOnTapPageEndMargin;
            CGFloat deviceWidth = pdfController.view.bounds.size.width;
            if (screenPoint.x < scrollOnTapPageMargin && self.isScrollOnTapPageEndEnabled) {
                if (![pdfController scrollToPreviousPageAnimated:YES]) {
                    [pdfController showControls];
                }else {
                    [pdfController hideControls];
                }
            }
            else if (deviceWidth-screenPoint.x < scrollOnTapPageMargin && self.isScrollOnTapPageEndEnabled) {
                if (![pdfController scrollToNextPageAnimated:YES]) {
                    [pdfController showControls];
                }else {
                    [pdfController hideControls];
                }
            }else {
                shouldToggle = YES;
            }
        }else {
            shouldToggle = YES;
        }

        if (shouldToggle) {
            if (![pdfController toggleControls]) {
                // give controls a chance to show they locked the HUD (e.g. annotation toolbar)
                [[NSNotificationCenter defaultCenter] postNotificationName:PSPDFHUDNotHiddenNotification object:self];
            }
        }
    }
}

// trick to know if the menu was disappeared throught the current touch or not.
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [super touchesBegan:touches withEvent:event];
    _flags.menuDidHideDuringThisTouchEvent = NO;

    if (_flags.menuWillHideCalled) {
        _flags.menuWillHideCalled = NO;
        for (PSPDFPageView *pageView in [self visiblePageViews]) {
            [pageView.selectionView discardSelection];
        }
    }
}

- (void)menuWillHide:(NSNotification *)notification {
    _flags.menuWillHideCalled = YES;
}

- (PSPDFAnnotation *)selectedAnnotation {
    for (PSPDFPageView *pageView in [self visiblePageViews]) {
        if (pageView.selectedAnnotation) return pageView.selectedAnnotation;
    }
    return nil;
}

// clear selected annotation once menu bar hides.
// Called AFTER the touchesBegan, thus we know that we just hid the menu and will check for the
// flag set here in the UITapGestureRecognizer handler
- (void)menuDidHide:(NSNotification *)notification {
    if ([self selectedAnnotation]) _flags.menuDidHideDuringThisTouchEvent = YES;
}

// zoom in
#define kZoomSize (PSIsIpad() ? 350 : 100)
- (void)doubleTapped:(UITapGestureRecognizer *)recognizer {
    if (!self.zoomingEnabled) return;

    CGRect zoomRect = CGRectZero;
    CGRect textBlockRect = CGRectZero;
    CGPoint touchPoint = [recognizer locationInView:self];
    PSPDFPageView *pageView = [self pageViewForPoint:touchPoint];

    // Smart Zoom - look for columns and zoom into.
    // don't lock UI if parser hasn't been loaded yet. we lazy-load that elsehwere.
    BOOL hasLoadedTextParser = [self.document hasLoadedTextParserForPage:pageView.page];
    if (self.pdfController.isSmartZoomEnabled && hasLoadedTextParser) {
        if (pageView) {
            CGPoint pointInPageView = [recognizer locationInView:pageView];
            CGPoint pdfPoint = [pageView convertViewPointToPDFPoint:pointInPageView];
            NSArray *textBlocks = [self.document objectsAtPDFPoint:pdfPoint page:pageView.page options:@{kPSPDFObjectsTextBlocks : @YES, kPSPDFObjectsTextBlocksIgnoreLarge : @YES, kPSPDFObjectsSmartSort: @YES}][kPSPDFTextBlocks];

            if ([textBlocks count]) {
                PSPDFTextBlock *block = textBlocks[0];
                CGFloat border = 10;
                zoomRect = [pageView convertPDFRectToViewRect:block.frame];
                zoomRect = CGRectInset(zoomRect, -border, -border);
                textBlockRect = zoomRect;

                // If the height of the zoomRect is significantly larger, we use the screenHeight and the touch point as center.
                // This improves usage for very large textblocks that span almost the whole page, zooming them to fully visibility would make the "zoom" not very useful.
                if (zoomRect.size.height/zoomRect.size.width > 2) {
                    CGFloat widthFactor = self.bounds.size.width / zoomRect.size.width;
                    zoomRect.size.height = self.bounds.size.height / widthFactor;
                    zoomRect.origin.y = pointInPageView.y - zoomRect.size.height/2;
                }

                // compensate pageView position for all cases (pageCurl, continuous, etc)
                CGRect pageFrame = [self convertRect:pageView.bounds fromView:pageView];
                pageFrame = CGRectIntegral(CGRectApplyAffineTransform(pageFrame, CGAffineTransformMakeScale(1/self.zoomScale, 1/self.zoomScale)));
                zoomRect.origin.x +=  pageFrame.origin.x;
                zoomRect.origin.y += pageFrame.origin.y;
            }
        }
    }

    // if this is the correct zoomRect or no rect was detected, zoom out
    CGRect currentZoomRect = (CGRect){.origin=self.contentOffset, .size=self.bounds.size};
    float theScale = 1.0 / self.zoomScale;
    currentZoomRect.origin.x *= theScale;
    currentZoomRect.origin.y *= theScale;
    currentZoomRect.size.width *= theScale;
    currentZoomRect.size.height *= theScale;
    CGRect intersectRect = CGRectIntersection(zoomRect, currentZoomRect);

    // faily tricky stuff to be clever what the double-tap should do.
    if (self.zoomScale > 1.1 && (CGRectIsEmpty(zoomRect) || (!PSPDFSizeIsEmpty(intersectRect.size) && intersectRect.size.width > zoomRect.size.width*0.8 && intersectRect.size.height > zoomRect.size.height*0.6))) {
        [self zoomOut];
    }else {
        // mark and zoom in/move
        // if no block was found, fall back to default zoom
        if (CGRectIsEmpty(zoomRect)) {
            NSUInteger touchPointX = touchPoint.x < kZoomSize/2 ? 0 : touchPoint.x-kZoomSize/2;
            NSUInteger touchPointY = touchPoint.y < kZoomSize/2 ? 0 : touchPoint.y-kZoomSize/2;
            zoomRect = CGRectMake(touchPointX, touchPointY, kZoomSize, kZoomSize);
        }else {
            // quickly show target rect
            UIView *hintView = [[UIView alloc] initWithFrame:textBlockRect];
            hintView.layer.cornerRadius = 5.f;
            hintView.backgroundColor = [[UIColor yellowColor] colorWithAlphaComponent:0.05f];
            [pageView addSubview:hintView];
            [UIView animateWithDuration:0.25f delay:0.25f options:UIViewAnimationOptionAllowUserInteraction animations:^{
                hintView.alpha = 0.f;
            } completion:^(BOOL finished) {
                [hintView removeFromSuperview];
            }];
        }

        PSPDFLog(@"pointed: %@ zooming to rect: %@", NSStringFromCGPoint(touchPoint), NSStringFromCGRect(zoomRect));
        [self zoomToRect:zoomRect animated:YES];
    }
}

- (void)longPress:(UILongPressGestureRecognizer *)recognizer {
    PSPDFLogVerbose(@"longPress: %@", NSStringFromCGPoint([recognizer locationInView:self]));
    BOOL handled = NO;

    NSArray *visiblePageViews = [self visiblePageViews];

    // special priority handling for annotation dragging (to allow dragging above pageView borders)
    for (PSPDFPageView *pageView in visiblePageViews) {
        handled = pageView.selectedAnnotation && [pageView longPress:recognizer];
        if (handled) break;
    }

    // forward long press to all visible pageViews (unless priority selectedAnnotation handling is active)
    if (!handled) {
        for (PSPDFPageView *pageView in visiblePageViews) {
            handled |= [pageView longPress:recognizer];
        }
    }

    // if no one handled the touch, make sure the loupe is hidden when the gesture completes
    // (might happen if we tap in a gap between PSPDFPageView's)
    if (!handled) {
        // allow loupe movement even if we're not on a page (else loupe would get stuck between pages)
        if (recognizer.state == UIGestureRecognizerStateChanged) {
            CGPoint locationForLoupe = [recognizer locationInView:_loupeView.superview];
            self.loupeView.center = CGPointMake(locationForLoupe.x, locationForLoupe.y - 64);
        }
    }

    // ensure loupe is always hidden once the gesture is finished.
    if ((recognizer.state == UIGestureRecognizerStateCancelled || recognizer.state == UIGestureRecognizerStateEnded)) {
        [self.loupeView hideLoupeAnimated:YES];
    }
}

- (void)zoomOut {
    if (self.zoomScale > 1.f) {
        [self setZoomScale:1.f animated:YES];
    }
}

// creates a CGPath for shadows. Can be overridden.
- (id)pathShadowForView:(UIView *)imgView {
    CGSize size = imgView.bounds.size;
    UIBezierPath *path = nil;

    switch (_shadowStyle) {
        case PSPDFShadowStyleFlat: {
            CGFloat moveShadow = -12;
            path = [UIBezierPath bezierPathWithRect:CGRectMake(moveShadow, moveShadow, size.width+fabs(moveShadow/2), size.height+fabs(moveShadow/2))];
        }break;

        case PSPDFShadowStyleCurl: {
            CGFloat curlFactor = 15.0f;
            CGFloat shadowDepth = 5.0f;
            path = [UIBezierPath bezierPath];
            [path moveToPoint:CGPointMake(0.0f, 0.0f)];
            [path addLineToPoint:CGPointMake(size.width, 0.0f)];
            [path addLineToPoint:CGPointMake(size.width, size.height + shadowDepth)];
            [path addCurveToPoint:CGPointMake(0.0f, size.height + shadowDepth)
                    controlPoint1:CGPointMake(size.width - curlFactor, size.height + shadowDepth - curlFactor)
                    controlPoint2:CGPointMake(curlFactor, size.height + shadowDepth - curlFactor)];
        }break;

        default:
            PSPDFLogError(@"Invalid style set for page shadow: %d", _shadowStyle); break;
    }

    // copy path, else ARC instantly deallocates the UIBezierPath backing store
    return path ? CFBridgingRelease(CGPathCreateCopy(path.CGPath)) : nil;
}

- (void)configureShadow {
    CALayer *backgroundLayer = self.compoundView.layer;

    // only ever show shadow if document is valid.
    if (self.isShadowEnabled && [self.document isValid]) {
        backgroundLayer.shadowColor = [UIColor blackColor].CGColor;
        backgroundLayer.shadowOffset = PSIsIpad() ? CGSizeMake(10.0f, 10.0f) : CGSizeMake(8.0f, 8.0f);
        backgroundLayer.shadowRadius = 4.0f;
        backgroundLayer.masksToBounds = NO;

        BOOL shouldUpdatePath = YES;
        if (self.isRotationActive && backgroundLayer.shadowPath) {
            CGRect currentRect = CGPathGetPathBoundingBox(backgroundLayer.shadowPath);
            CGRect newRect = CGPathGetPathBoundingBox((__bridge CGPathRef)[self pathShadowForView:self.compoundView]);

            if (newRect.size.width > currentRect.size.width || newRect.size.height > currentRect.size.height) {
                shouldUpdatePath = NO;
            }
        }
        if (shouldUpdatePath) {
            if (backgroundLayer.shadowOpacity == 0.f && backgroundLayer.shadowPath) {
                CABasicAnimation *theAnimation = [CABasicAnimation animationWithKeyPath:@"shadowOpacity"];
                theAnimation.duration = 0.1f * PSPDFSimulatorAnimationDragCoefficient();
                theAnimation.fromValue = @0.0f;
                theAnimation.toValue = @0.7f;
                [backgroundLayer addAnimation:theAnimation forKey:@"shadowOpacity"];
            }
            backgroundLayer.shadowOpacity = 0.7f;
            backgroundLayer.shadowPath = (__bridge CGPathRef)[self pathShadowForView:self.compoundView];
        }else {
            backgroundLayer.shadowOpacity = 0.f;
        }
    }else {
        backgroundLayer.shadowOpacity = 0.f;
        backgroundLayer.shadowPath = nil;
    }
}

- (CGSize)boundSize {
    CGSize parentBoundsSize = CGSizeZero;

    // TODO: why so complicated? Why do we need the controller here?
    PSPDFViewController *controller = self.pdfController;
    if (controller) {
        if (controller.pageTransition > PSPDFPageScrollContinuousTransition) {
            parentBoundsSize = UIEdgeInsetsInsetRect(self.bounds, controller.margin).size;
        }else {
            parentBoundsSize = UIEdgeInsetsInsetRect(controller.view.bounds, controller.margin).size;
        }
    }

    return parentBoundsSize;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UIGestureRecognizerDelegate / PSPDFLongPressGestureRecognizerDelegate

// check if this gesture is PSPDFKit internal.
- (BOOL)isInternalGestureRecognizer:(UIGestureRecognizer *)gestureRecognizer {
    return gestureRecognizer == self.singleTapGesture || gestureRecognizer == self.doubleTapGesture || gestureRecognizer == self.longPressGesture;
}

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer {
    // disable long press gesture recognizer if text selection is disabled.
    if (!self.pdfController.isTextSelectionEnabled && [gestureRecognizer isKindOfClass:[PSPDFLongPressGestureRecognizer class]]) {
        return NO;
    }

    // block all multi-tap recognizers if viewLock is enabled
    if (self.pdfController.isViewLockEnabled && [gestureRecognizer isKindOfClass:[UITapGestureRecognizer class]] && [(UITapGestureRecognizer *)gestureRecognizer numberOfTapsRequired] >= 2) return NO;

    // disable all internal tap gestures?
    if (!self.pdfController.internalTapGesturesEnabled && [self isInternalGestureRecognizer:gestureRecognizer]) return NO;

    return YES;
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    if (([gestureRecognizer isKindOfClass:[UITapGestureRecognizer class]] || [gestureRecognizer isKindOfClass:[PSPDFLongPressGestureRecognizer class]]) && [touch.view isKindOfClass:[UIControl class]]) {
        return NO;
    }

    // don't allow text selection above videos!
    if ([gestureRecognizer isKindOfClass:[PSPDFLongPressGestureRecognizer class]] && PSPDFIsViewClassInsideViewHierarchy(touch.view, @[[PSPDFVideoAnnotationView class], [PSPDFYouTubeAnnotationView class]])) {
        return NO;
    }

    // disable all internal tap gestures?
    if (!self.pdfController.internalTapGesturesEnabled && [self isInternalGestureRecognizer:gestureRecognizer]) return NO;

    return YES;
}

// PSPDFLongPressGestureRecognizerDelegate
- (BOOL)pressRecognizerShouldHandlePressImmediately:(PSPDFLongPressGestureRecognizer *)recognizer {
    for (PSPDFPageView *pageView in [self visiblePageViews]) {
        if ([pageView pressRecognizerShouldHandlePressImmediately:recognizer]) {
            return YES;
        }
    }
    return NO;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UIScrollViewDelegate (relays events to PSPDFPageView's)

- (UIScrollView *)parentScrollView {
    UIScrollView *scrollView = (UIScrollView *)self.superview;
    while (scrollView && ![scrollView isKindOfClass:[UIScrollView class]]) {
        scrollView = (UIScrollView *)scrollView.superview;
    }

    return scrollView;
}

- (void)setParentScrollViewScrollPagingEnabled:(BOOL)enable {
    UIScrollView *parentScrollView = [self parentScrollView];
    parentScrollView.scrollEnabled = enable && self.pdfController.scrollingEnabled;
}

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView {
    return self.compoundView;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    // ensure menu is hidden
    [[UIMenuController sharedMenuController] setMenuVisible:NO animated:YES];

    [[self visiblePageViews] makeObjectsPerformSelector:@selector(scrollViewDidScroll:) withObject:scrollView];
}

// called on start of dragging (may require some time and or distance to move)
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    [[self visiblePageViews] makeObjectsPerformSelector:@selector(scrollViewWillBeginDragging:) withObject:scrollView];
}

- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset {
    // call delegate
    _flags.scrollViewIsDecelerating = !CGPointEqualToPoint(velocity, CGPointZero);
    [self.pdfController delegateDidEndPageDragging:scrollView willDecelerate:_flags.scrollViewIsDecelerating withVelocity:velocity targetContentOffset:targetContentOffset];

    for (PSPDFPageView *pageView in [self visiblePageViews]) {
        [pageView scrollViewWillEndDragging:scrollView withVelocity:velocity targetContentOffset:targetContentOffset];
    }
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    for (PSPDFPageView *pageView in [self visiblePageViews]) {
        [pageView scrollViewDidEndDragging:scrollView willDecelerate:decelerate];
    }
}

- (void)scrollViewWillBeginDecelerating:(UIScrollView *)scrollView {
    [[self visiblePageViews] makeObjectsPerformSelector:@selector(scrollViewWillBeginDecelerating:) withObject:scrollView];
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    [[self visiblePageViews] makeObjectsPerformSelector:@selector(scrollViewDidEndDecelerating:) withObject:scrollView];

    if (_flags.scrollViewIsDecelerating) {
        _flags.scrollViewIsDecelerating = NO;
        [self.pdfController delegateDidEndPageDragging:scrollView willDecelerate:NO withVelocity:CGPointZero targetContentOffset:nil];
    }
}

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView {
    self.animatingZoomIn = NO;
    [[self visiblePageViews] makeObjectsPerformSelector:@selector(scrollViewDidEndScrollingAnimation:) withObject:scrollView];

    [self.pdfController delegateDidEndPageScrollingAnimation:(PSPDFScrollView *)scrollView];
}

- (void)scrollViewWillBeginZooming:(UIScrollView *)scrollView withView:(UIView *)view {
    [self.pdfController hideControls];

    for (PSPDFPageView *pageView in [self visiblePageViews]) {
        [pageView scrollViewWillBeginZooming:scrollView withView:view];
    }

    // disable pan
    [self setParentScrollViewScrollPagingEnabled:NO];
}

// manually relay every zoom level!
- (void)scrollViewDidZoom:(UIScrollView *)scrollView {
    [[self visiblePageViews] makeObjectsPerformSelector:@selector(scrollViewDidZoom:) withObject:scrollView];
}

- (void)scrollViewDidEndZooming:(UIScrollView *)scrollView withView:(UIView *)view atScale:(float)scale {
    self.animatingZoomIn = NO;
    for (PSPDFPageView *pageView in [self visiblePageViews]) {
        [pageView scrollViewDidEndZooming:scrollView withView:view atScale:scale];
    }

    // re-enable pan
    [self setParentScrollViewScrollPagingEnabled:YES];
    [self.compoundView setNeedsLayout];

    // update centering method (work around UIKit bug)
    _flags.blockLayoutSubviewCentering = NO;
    _flags.isZoomingProgrammatically = NO;

    [self.pdfController delegateDidEndZooming:scrollView withView:view atScale:scale];
}

- (void)scrollViewDidScrollToTop:(UIScrollView *)scrollView {
    [[self visiblePageViews] makeObjectsPerformSelector:@selector(scrollViewDidScrollToTop:) withObject:scrollView];
}

@end
