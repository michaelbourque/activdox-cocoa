//
//  PSPDFColorTableViewCell.h
//  PSPDFKit
//
//  Copyright 2012 Peter Steinberger. All rights reserved.
//

#import "PSPDFKitGlobal.h"

@class PSPDFColorView;

@interface PSPDFColorTableViewCell : UITableViewCell

@property (nonatomic, strong) PSPDFColorView *colorView;

@end
