//
//  PSPDFColorTableViewCell.m
//  PSPDFKit
//
//  Copyright 2012 Peter Steinberger. All rights reserved.
//  Based on code by Markus Müller of Mindnote Touch. Thanks, Markus!
//

#import "PSPDFColorTableViewCell.h"
#import "PSPDFColorView.h"

@implementation PSPDFColorTableViewCell

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (id)initWithCellIdentifier:(NSString *)cellID style:(UITableViewCellStyle)style {
    if ((self = [super initWithStyle:style reuseIdentifier:cellID])) {
        self.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        self.selectionStyle = UITableViewCellSelectionStyleBlue;

        self.colorView = [[PSPDFColorView alloc] initWithFrame:CGRectZero];
        self.colorView.userInteractionEnabled = NO;
        self.accessoryView = self.colorView;
        [self.colorView sizeToFit];
    }
    return self;
}

@end
