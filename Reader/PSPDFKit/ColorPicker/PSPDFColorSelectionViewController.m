//
//  PSPDFColorSelectionViewController.m
//  PSPDFKit
//
//  Copyright 2012 Peter Steinberger. All rights reserved.
//  Based on code by Markus Müller of Mindnote Touch. Thanks, Markus!
//

#import "PSPDFColorSelectionViewController.h"
#import "PSPDFColorView.h"
#import "UIColor+PSPDFKitAdditions.h"
#import "PSPDFGradientView.h"

@interface PSPDFColorSelectionViewController ()

@property (nonatomic, copy) NSArray *colors;
@property (nonatomic, strong) NSMutableArray *colorViews;

@end

@implementation PSPDFColorSelectionViewController

__strong static NSArray *_defaultColorArrays = nil;

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Static

+ (NSArray *)defaultColorArrays {
    if (!_defaultColorArrays) {

        NSMutableArray *colorArrays = [NSMutableArray array];
        NSArray *fileNames = @[@"ColorsRainbow", @"ColorsModern", @"ColorsVintage"];
        for (NSString *fileName in fileNames) {
            
            NSArray *colors = [self colorsFromPalletURL:[PSPDFKitBundle() URLForResource:fileName withExtension:@"plist"] addDarkenedVariants:YES];
            if (colors) [colorArrays addObject:colors];
        }

        NSArray *blackwhite = [self colorsFromPalletURL:[PSPDFKitBundle() URLForResource:@"ColorsBlackWhite" withExtension:@"plist"] addDarkenedVariants:NO];
        if (blackwhite) [colorArrays addObject:blackwhite];

        _defaultColorArrays = [colorArrays copy];
    }
    return _defaultColorArrays;
}

+ (void)setDefaultColorArrays:(NSArray *)defaultColorArrays {
    _defaultColorArrays = [defaultColorArrays copy];
}

+ (PSPDFSimplePageViewController *)defaultColorPickerWithTitle:(NSString *)title delegate:(id<PSPDFColorSelectionViewControllerDelegate>)delegate {
    PSPDFSimplePageViewController *pageViewController = nil;

    NSString *viewControllerTitle = title ?: PSPDFLocalize(@"Choose Color");
    NSMutableArray *viewControllers = [NSMutableArray array];

    PSPDFColorSelectionViewController *rainbowViewController = [PSPDFColorSelectionViewController rainbowSelectionViewController];
    rainbowViewController.title = viewControllerTitle;
    if (rainbowViewController) [viewControllers addObject:rainbowViewController];
    rainbowViewController.delegate = delegate;

    PSPDFColorSelectionViewController *modernViewController = [PSPDFColorSelectionViewController modernColorsSelectionViewController];
    modernViewController.title = viewControllerTitle;
    if (modernViewController) [viewControllers addObject:modernViewController];
    modernViewController.delegate = delegate;

    PSPDFColorSelectionViewController *vintageViewController = [PSPDFColorSelectionViewController vintageColorsSelectionViewController];
    vintageViewController.title = viewControllerTitle;
    if (vintageViewController) [viewControllers addObject:vintageViewController];
    vintageViewController.delegate = delegate;

    PSPDFColorSelectionViewController *monochromeViewController = [PSPDFColorSelectionViewController monoChromeSelectionViewController];
    monochromeViewController.title = viewControllerTitle;
    if (monochromeViewController) [viewControllers addObject:monochromeViewController];
    monochromeViewController.delegate = delegate;

    // Navigation logic may go here. Create and push another view controller.
    if ([viewControllers count]) {
        pageViewController = [[PSPDFSimplePageViewController alloc] initWithViewControllers:viewControllers];
        pageViewController.contentSizeForViewInPopover = CGSizeMake(320.f, 460.f);

        PSPDFGradientView *gradient = [[PSPDFGradientView alloc] initWithFrame:pageViewController.view.bounds];
        gradient.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        gradient.colors = @[
        [UIColor colorWithRed:(0xE2 / 255.0) green:(0xE5 / 255.0) blue:(0xE9 / 255.0) alpha:1.0],
        [UIColor colorWithRed:(0xD0 / 255.0) green:(0xD2 / 255.0) blue:(0xD7 / 255.0) alpha:1.0]];
        [pageViewController.view insertSubview:gradient atIndex:0];
    }
    return pageViewController;
}

+ (NSArray *)colorsFromPalletURL:(NSURL *)palletURL addDarkenedVariants:(BOOL)darkenedVariants {
    NSArray *colorsArray = [NSArray arrayWithContentsOfURL:palletURL];
    if (!colorsArray || ![colorsArray count]) return nil;

    NSMutableArray *colorList = [NSMutableArray arrayWithCapacity:[colorsArray count]];
    for (id currentColorRepresentation in colorsArray) {

        UIColor *currentColor = [UIColor pspdf_colorFromPropertyRepresentation:currentColorRepresentation];
        if (!currentColor) continue;
        [colorList addObject:currentColor];

        if (!darkenedVariants) continue;
        for (NSUInteger index = 0; index < 3; index++) {
            currentColor = [currentColor pspdf_darkenedColor];
            [colorList addObject:currentColor];
        }
    }
    return colorList;
}

+ (instancetype)selectionViewControllerWithPallet:(NSString *)pallet addDarkenedVariants:(BOOL)darkenedVariants {
    NSArray *colorList = [self colorsFromPalletURL:[PSPDFKitBundle() URLForResource:pallet withExtension:@"plist"] addDarkenedVariants:darkenedVariants];
    PSPDFColorSelectionViewController *colorsViewController = [[[self class] alloc] initWithColors:colorList];
    return colorsViewController;
}

+ (instancetype)monoChromeSelectionViewController {
    PSPDFColorSelectionViewController *colorsViewController = [[self class] selectionViewControllerWithPallet:@"ColorsBlackWhite" addDarkenedVariants:NO]; 
    colorsViewController.title = PSPDFLocalize(@"Monochrome");
    return colorsViewController;
}

+ (instancetype)modernColorsSelectionViewController {
    PSPDFColorSelectionViewController *colorsViewController = [[self class] selectionViewControllerWithPallet:@"ColorsModern" addDarkenedVariants:YES];
    colorsViewController.title = PSPDFLocalize(@"Vintage");
    return colorsViewController;
}    
 
+ (instancetype)vintageColorsSelectionViewController {
    PSPDFColorSelectionViewController *colorsViewController = [[self class] selectionViewControllerWithPallet:@"ColorsVintage" addDarkenedVariants:YES]; 
    colorsViewController.title = PSPDFLocalize(@"Vintage");
    return colorsViewController;
}

+ (instancetype)rainbowSelectionViewController {
    PSPDFColorSelectionViewController *colorsViewController = [[self class] selectionViewControllerWithPallet:@"ColorsRainbow" addDarkenedVariants:NO];
    colorsViewController.title = PSPDFLocalize(@"Rainbow");
    return colorsViewController;
}

+ (instancetype)colorSelectionViewControllerFromColors:(NSArray *)colorsArray addDarkenedVariants:(BOOL)darkenedVariants {    
    NSMutableArray *colorList = [NSMutableArray arrayWithCapacity:[colorsArray count]];
    for (id currentColorRepresentation in colorsArray) {
        
        UIColor *currentColor = [UIColor pspdf_colorFromPropertyRepresentation:currentColorRepresentation];
        if (!currentColor) continue;
        [colorList addObject:currentColor];
        
        if (!darkenedVariants) continue;
        for (NSUInteger index = 0; index < 3; index++) {
            currentColor = [currentColor pspdf_darkenedColor];
            [colorList addObject:currentColor];
        }
    }
    
    if (![colorsArray count]) return nil;
    
    PSPDFColorSelectionViewController *colorsViewController = [[[self class] alloc] initWithColors:colorList];
    return colorsViewController;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (id)initWithColors:(NSArray *)colors {
    if ((self = [super initWithNibName:nil bundle:nil])) {
        NSAssert((([colors count] <= 6) || (([colors count] % 4) == 0)), @"If you provide more than 6 colors, the colors need to be mod 4!");
        self.colors = colors;
    }
    return self;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UIView

// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView {
    self.view = [[UIView alloc] initWithFrame:CGRectZero];
    
    NSUInteger colorCount = [self.colors count];
    self.colorViews = [NSMutableArray arrayWithCapacity:colorCount];
    if (colorCount <= 6) {
        [self loadSingleNodeViews];
    } else {
        [self loadGroupedNodeViews];
    }
}

- (void)loadSingleNodeViews {
    NSUInteger colorCount = [self.colors count];
    for (NSUInteger index = 0; index < colorCount; index++) {
        
        PSPDFColorView *colorView = [PSPDFColorView colorViewWithColor:(self.colors)[index] borderStyle:PSPDFColorViewBorderStyleSingle];
        [self.view addSubview:colorView];
        [self.colorViews addObject:colorView];
        [colorView addTarget:self action:@selector(didTapOnColorView:) forControlEvents:UIControlEventTouchUpInside];
    }
}

- (void)loadGroupedNodeViews {
    NSUInteger colorCount = [self.colors count];
    for (NSUInteger index = 0; index < colorCount / 4; index++) {
        for (NSUInteger innerIndex = 0; innerIndex < 4; innerIndex++) {
            PSPDFColorViewBorderStyle borderStyle;
            switch (innerIndex) {
                case 0:
                    borderStyle = PSPDFColorViewBorderStyleTop;
                    break;
                case 1:
                case 2:
                    borderStyle = PSPDFColorViewBorderStyleMiddle;
                    break;
                case 3:
                default:
                    borderStyle = PSPDFColorViewBorderStyleBottom;
                    break;
            }
            NSUInteger currentIndex = index*4+innerIndex;
            PSPDFColorView *colorView = [PSPDFColorView colorViewWithColor:(self.colors)[currentIndex] borderStyle:borderStyle];
            [self.view addSubview:colorView];
            [self.colorViews addObject:colorView];
            [colorView addTarget:self action:@selector(didTapOnColorView:) forControlEvents:UIControlEventTouchUpInside];
        }
    }
}

- (void)viewDidUnload {
    [super viewDidUnload];
    
    for (PSPDFColorView *currentView in self.colorViews) {
        [currentView removeFromSuperview];
    }
    self.colorViews = nil;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self layoutViewsForPortrait:UIInterfaceOrientationIsPortrait(self.interfaceOrientation)];
    UIColor *selectedColor = [self.delegate colorSelectionControllerSelectedColor:self];
    for (NSUInteger index = 0; index < [self.colors count]; index++) {
        UIColor *currentColor = (self.colors)[index];
        [(self.colorViews)[index] setSelected:([currentColor isEqual:selectedColor]) animated:NO];
    }
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation duration:(NSTimeInterval)duration {
    [self layoutViewsForPortrait:UIInterfaceOrientationIsPortrait(interfaceOrientation)];
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Layout

- (void)layoutViewsForPortrait:(BOOL)isPortrait {
    BOOL twoRows = self.view.frame.size.height > 300.f;
    CGFloat offset = 4;
    NSUInteger itemsInRow = twoRows ? 3 : 6;
    CGFloat minPaddingX = 8.0f;
    CGFloat minPaddingY = 8.0f;
    NSUInteger itemSpacings = itemsInRow - 1;
    
    CGFloat width = ceilf((CGRectGetWidth(self.view.frame) - itemSpacings * offset - 2 * minPaddingX) / itemsInRow);
    CGFloat height = ceilf(CGRectGetHeight(self.view.frame) / (twoRows ? 2 : 1) - 2 * minPaddingY) ;
    CGFloat paddingX = (CGRectGetWidth(self.view.frame) - itemsInRow * width - itemSpacings * offset) / 2.0f;
    CGFloat paddingY = ((CGRectGetHeight(self.view.frame) - (twoRows ? 2 : 1) * height) / 2.0f);

    CGRect colorRect = CGRectMake(0, 0, width, height);
    NSUInteger viewCount = [self.colorViews count];
    
    if (viewCount <= 6) {
        for (NSUInteger item = 0; item < viewCount; item++) {
            colorRect.origin.x = paddingX + (width+offset)*(item % itemsInRow);
            colorRect.origin.y = paddingY + (height+offset)*(NSUInteger)(item / itemsInRow);
            [(self.colorViews)[item] setFrame:colorRect];
        }
    } else {
        for (NSUInteger item = 0; item < viewCount / 4; item++) {
            colorRect.origin.x = paddingX + (width+offset)*(item % itemsInRow);
            colorRect.origin.y = paddingY + (height+offset)*(NSUInteger)(item / itemsInRow);
            
            for (NSUInteger innerIndex = 0; innerIndex < 4; innerIndex++) {
                CGRect frame = CGRectOffset(colorRect, 0, CGRectGetHeight(colorRect) * innerIndex / 4.0f);
                frame.size.height = CGRectGetHeight(frame) / 4.0f;
                [(self.colorViews)[item*4+innerIndex] setFrame:frame];
            }
        }
    }

    // Redraw color swatches in case orientation has changed
    for (NSUInteger index = 0; index < [self.colors count]; index++)
        [(self.colorViews)[index] setNeedsDisplay];
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Actions

- (void)didTapOnColorView:(PSPDFColorView *)colorView {
    [self.colorViews enumerateObjectsUsingBlock:^(PSPDFColorView *view, NSUInteger idx, BOOL *stop){
        [view setSelected:NO];
    }];    
    
    colorView.selected = YES;
    [self.delegate colorSelectionController:self didSelectedColor:colorView.color];
}

- (UINavigationController *)navigationController {
    UINavigationController *navigationController = super.navigationController;
    if (navigationController) return navigationController;
    
    UIResponder *responder = self;
    while (responder && ![responder isKindOfClass:[UINavigationController class]]) {
        responder = [responder nextResponder];
    }
    return (UINavigationController *)responder;
}

@end
