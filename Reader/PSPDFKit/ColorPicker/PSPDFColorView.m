//
//  PSPDFColorView.m
//  PSPDFKit
//
//  Copyright 2012 Peter Steinberger. All rights reserved.
//

#import "PSPDFColorView.h"
#import <QuartzCore/QuartzCore.h>

@interface PSPDFColorView ()

@property (nonatomic, strong) CAShapeLayer *touchDownLayer;
@property (nonatomic, strong) CALayer *checkmarkLayer;
+ (CGPathRef)newPathForBorderStyle:(PSPDFColorViewBorderStyle)borderStyle boundingRect:(CGRect)rect;

@end

static NSUInteger cornerRadius = 6.0f;
static NSUInteger inset = 1.0f;
static CGFloat kCheckmarkWidth = 22;

// KVO
static NSString *PSPDFColorViewColorKey = @"color";
static NSString *PSPDFColorViewRedrawKey = @"redraw";
static NSString *PSPDFColorViewRedrawObservationContext = @"PSPDFColorViewRedrawObservationContext";


@implementation PSPDFColorView

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

+ (id)colorViewWithColor:(UIColor *)color borderStyle:(PSPDFColorViewBorderStyle)borderStyle {
    PSPDFColorView *control = [[[self class] alloc] initWithFrame:CGRectZero];
    
    control.color = color;
    control.borderStyle = borderStyle;
    
    return control;
}

- (id)initWithFrame:(CGRect)frame  {
    self = [super initWithFrame:frame];
    if (!self) return nil;
    
    self.color = [UIColor blackColor];
    self.opaque = NO;
    self.backgroundColor = [UIColor clearColor];
    self.color = [UIColor whiteColor];

    
    self.touchDownLayer = [CAShapeLayer layer];
    self.touchDownLayer.opacity = 0.0f;
    [self.layer addSublayer:self.touchDownLayer];
    
    self.checkmarkLayer = [CALayer layer];
    self.checkmarkLayer.hidden = YES;
    self.checkmarkLayer.contents = (id)[UIImage imageNamed:@"PSPDFKit.bundle/ColorViewCheckmark"].CGImage;
    self.checkmarkLayer.bounds = CGRectMake(0, 0, kCheckmarkWidth, kCheckmarkWidth);
    [self.layer addSublayer:self.checkmarkLayer];

    [self addObserver:self forKeyPath:PSPDFColorViewRedrawKey options:0 context:(__bridge void *)PSPDFColorViewRedrawObservationContext];
    
    return self;
}

- (void)dealloc  {
    [self removeObserver:self forKeyPath:PSPDFColorViewRedrawKey];    
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Custom Properties

- (void)setSelected:(BOOL)selected {
    [super setSelected:selected];
    self.checkmarkLayer.hidden = !selected;
}

- (CGSize)sizeThatFits:(CGSize)size {
    return CGSizeMake(80, 30);
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    if (animated) {
        [self setSelected:selected];
    } else {
        [CATransaction begin];
        [CATransaction setValue:@0.000f forKey:kCATransactionAnimationDuration];
        [self setSelected:selected];
        [CATransaction commit];
    }
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Drawing

static CGColorRef PSPDFCGColorCreateGenericGray(CGFloat gray, CGFloat alpha) {
	CGColorSpaceRef space = CGColorSpaceCreateDeviceGray();
	const CGFloat components[] = {gray, alpha};
	CGColorRef colorRef = CGColorCreate(space, components);
	CGColorSpaceRelease(space);
	return colorRef;
}

- (void)drawRect:(CGRect)rect {
    CGColorRef color = self.color.CGColor;
    PSPDFColorViewBorderStyle borderStyle = self.borderStyle;
    rect = CGRectInset(rect, 0.5, 0.5);
        
    CGContextRef c = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(c, color);
    CGColorRef strokeColor = PSPDFCGColorCreateGenericGray(0.6f, 1.0f);
    CGContextSetStrokeColorWithColor(c, strokeColor);
    CGColorRelease(strokeColor);
    CGContextSetLineWidth(c, 1);
    
    CGPathRef path = [[self class] newPathForBorderStyle:self.borderStyle boundingRect:self.bounds];
    CGContextAddPath(c, path);
    CGContextDrawPath(c, kCGPathFill);
    CGPathRelease(path);
    
    CGContextSetBlendMode(c, kCGBlendModeMultiply);
    
    
    CGFloat minx = CGRectGetMinX(rect) , midx = CGRectGetMidX(rect), maxx = CGRectGetMaxX(rect) ;
    CGFloat miny = CGRectGetMinY(rect) , midy = CGRectGetMidY(rect) , maxy = CGRectGetMaxY(rect) ;

    minx = minx + inset;
    maxx = maxx - inset;

    if (borderStyle == PSPDFColorViewBorderStyleSingle) {
        miny = miny + inset;        
        maxy = maxy - inset;

        CGContextMoveToPoint(c, minx, midy);
        CGContextAddArcToPoint(c, minx, miny, midx, miny, cornerRadius);
        CGContextAddArcToPoint(c, maxx, miny, maxx, midy, cornerRadius);
        CGContextAddLineToPoint(c, maxx, midy);
        CGContextDrawPath(c, kCGPathStroke);
        
        CGContextMoveToPoint(c, minx, midy);
        CGContextAddArcToPoint(c, minx, maxy, midx, maxy, cornerRadius);
        CGContextAddArcToPoint(c, maxx, maxy, maxx, midy, cornerRadius);
        CGContextAddLineToPoint(c, maxx, midy);
        CGContextSetShadowWithColor(c, CGSizeMake(0, 1.5), 0,[[UIColor whiteColor] colorWithAlphaComponent:0.8f].CGColor); 
        CGContextDrawPath(c, kCGPathStroke);
    } else if (borderStyle == PSPDFColorViewBorderStyleTop) {
        miny = miny + inset;        
        maxy = maxy + inset;

        CGContextMoveToPoint(c, minx, maxy);
        CGContextAddArcToPoint(c, minx, miny, midx, miny, cornerRadius);
        CGContextAddArcToPoint(c, maxx, miny, maxx, maxy, cornerRadius);
        CGContextAddLineToPoint(c, maxx, maxy);
        CGContextDrawPath(c, kCGPathStroke);
    } else if (borderStyle == PSPDFColorViewBorderStyleBottom) {
        miny = miny - inset;        
        maxy = maxy - inset;
        CGContextMoveToPoint(c, minx, miny);
        CGContextAddArcToPoint(c, minx, maxy, midx, maxy, cornerRadius);
        CGContextAddArcToPoint(c, maxx, maxy, maxx, miny, cornerRadius);
        CGContextAddLineToPoint(c, maxx, miny);
        CGContextSetShadowWithColor(c, CGSizeMake(0, 1.5), 0,[[UIColor whiteColor] colorWithAlphaComponent:0.8f].CGColor); 
        CGContextDrawPath(c, kCGPathStroke);
    } else if (borderStyle == PSPDFColorViewBorderStyleMiddle) {
        miny = miny - inset;        
        maxy = maxy + inset;

        CGContextMoveToPoint(c, minx, miny);
        CGContextAddLineToPoint(c, minx, maxy);
        CGContextMoveToPoint(c, maxx, miny);
        CGContextAddLineToPoint(c, maxx, maxy);
        CGContextSetShadowWithColor(c, CGSizeMake(0, 1.5), 0,[[UIColor whiteColor] colorWithAlphaComponent:0.8f].CGColor); 
        CGContextDrawPath(c, kCGPathStroke);
    }
    CGContextSetBlendMode(c, kCGBlendModeNormal);

}

- (void)layoutSublayersOfLayer:(CALayer *)layer {
    CGPathRef path = [[self class] newPathForBorderStyle:self.borderStyle boundingRect:self.bounds];
    self.touchDownLayer.path = path;
    CGPathRelease(path);
    
    
    CGRect frame = self.bounds;
    self.checkmarkLayer.position = CGPointMake(CGRectGetMaxX(frame)-kCheckmarkWidth/2-4,CGRectGetMaxY(frame)-kCheckmarkWidth/2-4);

}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Touch Handling

- (BOOL)beginTrackingWithTouch:(UITouch *)touch withEvent:(UIEvent *)event {
    BOOL flag = [super beginTrackingWithTouch:touch withEvent:event];
    
    [CATransaction begin];
    [CATransaction setValue:@0.001f forKey:kCATransactionAnimationDuration];
    self.touchDownLayer.opacity = 0.2f;
    [CATransaction commit];

    return flag;
}

- (void)endTrackingWithTouch:(UITouch *)touch withEvent:(UIEvent *)event {
    self.touchDownLayer.opacity = 0.0;
    [super endTrackingWithTouch:touch withEvent:event];
}

- (void)cancelTrackingWithEvent:(UIEvent *)event {
    self.touchDownLayer.opacity = 0.0;
    [super cancelTrackingWithEvent:event];
}


+ (CGPathRef)newPathForBorderStyle:(PSPDFColorViewBorderStyle)borderStyle boundingRect:(CGRect)rect {
    // Drawing code in parts from http://stackoverflow.com/questions/400965/how-to-customize-the-background-border-colors-of-a-grouped-table-view
//    rect = CGRectInset(rect, 0.5, 0.5);
    CGMutablePathRef path = CGPathCreateMutable();
    
    if (borderStyle == PSPDFColorViewBorderStyleTop) {
        
        CGFloat minx = CGRectGetMinX(rect) , midx = CGRectGetMidX(rect), maxx = CGRectGetMaxX(rect) ;
        CGFloat miny = CGRectGetMinY(rect) , maxy = CGRectGetMaxY(rect) ;
        minx = minx + inset;
        miny = roundf(miny + inset);
        
        maxx = maxx - inset;
        maxy = maxy - inset;
        
        CGPathMoveToPoint(path, NULL, minx, maxy);
        CGPathAddArcToPoint(path, NULL, minx, miny, midx, miny, cornerRadius);
        CGPathAddArcToPoint(path, NULL, maxx, miny, maxx, maxy, cornerRadius);
        CGPathAddLineToPoint(path, NULL, maxx, maxy);
    } else if (borderStyle == PSPDFColorViewBorderStyleBottom) {
        
        CGFloat minx = CGRectGetMinX(rect) , midx = CGRectGetMidX(rect), maxx = CGRectGetMaxX(rect) ;
        CGFloat miny = CGRectGetMinY(rect) , maxy = CGRectGetMaxY(rect) ;
        minx = minx + inset;
        miny = miny + inset;
        
        maxx = maxx - inset;
        maxy = roundf(maxy - inset);
        
        CGPathMoveToPoint(path, NULL, minx, miny);
        CGPathAddArcToPoint(path, NULL, minx, maxy, midx, maxy, cornerRadius);
        CGPathAddArcToPoint(path, NULL, maxx, maxy, maxx, miny, cornerRadius);
        CGPathAddLineToPoint(path, NULL, maxx, miny);
    } else if (borderStyle == PSPDFColorViewBorderStyleMiddle) {
        CGFloat minx = CGRectGetMinX(rect) , maxx = CGRectGetMaxX(rect) ;
        CGFloat miny = CGRectGetMinY(rect) , maxy = CGRectGetMaxY(rect) ;
        minx = minx + inset;
        miny = roundf(miny + inset);
        
        maxx = maxx - inset;
        maxy = roundf(maxy - inset);
        
        CGPathMoveToPoint(path, NULL, minx, miny);
        CGPathAddLineToPoint(path, NULL, maxx, miny);
        CGPathAddLineToPoint(path, NULL, maxx, maxy);
        CGPathAddLineToPoint(path, NULL, minx, maxy);
    } else if (borderStyle == PSPDFColorViewBorderStyleSingle) {
        CGFloat minx = CGRectGetMinX(rect) , midx = CGRectGetMidX(rect), maxx = CGRectGetMaxX(rect) ;
        CGFloat miny = CGRectGetMinY(rect) , midy = CGRectGetMidY(rect) , maxy = CGRectGetMaxY(rect) ;
        minx = minx + inset;
        miny = miny + inset;
        
        maxx = maxx - inset;
        maxy = maxy - inset;
                
        CGPathMoveToPoint(path, NULL, minx, midy);
        CGPathAddArcToPoint(path, NULL, minx, miny, midx, miny, cornerRadius);
        CGPathAddArcToPoint(path, NULL, maxx, miny, maxx, midy, cornerRadius);
        CGPathAddArcToPoint(path, NULL, maxx, maxy, midx, maxy, cornerRadius);
        CGPathAddArcToPoint(path, NULL, minx, maxy, minx, midy, cornerRadius);
    }
    return path;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Observing

+ (NSSet *)keyPathsForValuesAffectingRedraw  {
    return [NSSet setWithObject:PSPDFColorViewColorKey];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context  {
    if (context == (__bridge void *)PSPDFColorViewRedrawObservationContext) {
        [self setNeedsDisplay];
    } else {
        [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
    }
}

@end
