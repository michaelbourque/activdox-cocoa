//
//  PSPDFSimplePageViewController.m
//  PSPDFKit
//
//  Copyright 2012 Peter Steinberger. All rights reserved.
//

#import "PSPDFSimplePageViewController.h"

@interface PSPDFSimplePageViewController ()

@property (nonatomic, copy) NSArray *viewControllers;
@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) UIPageControl *pageControl;
@property (nonatomic, assign) BOOL pageControlUsed;
@property (nonatomic, assign) BOOL isRotating;

@end

@implementation PSPDFSimplePageViewController

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (id)initWithViewControllers:(NSArray *)viewControllers {
    if ((self = [super initWithNibName:nil bundle:nil])) {
        self.viewControllers = viewControllers;
        self.pageControlUsed = NO;
    }
    return self;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - View lifecycle

// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView {
    self.view = [[UIView alloc] initWithFrame:CGRectZero];
    self.view.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;

    self.scrollView = [[UIScrollView alloc] initWithFrame:CGRectZero];
    self.scrollView.showsHorizontalScrollIndicator = NO;
    self.scrollView.showsVerticalScrollIndicator = NO;
    self.scrollView.scrollsToTop = NO;
    self.scrollView.delegate = self;
    self.scrollView.pagingEnabled = YES;
    self.scrollView.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;

    [self.view addSubview:self.scrollView];

    self.pageControl = [[UIPageControl alloc] initWithFrame:CGRectZero];
    self.pageControl.numberOfPages = [self.viewControllers count];
    self.pageControl.currentPage = 0;
    self.pageControl.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;
    [self.pageControl addTarget:self action:@selector(changePage:) forControlEvents:UIControlEventValueChanged];
    
    if ([self.pageControl respondsToSelector:@selector(pageIndicatorTintColor)]) {
        self.pageControl.currentPageIndicatorTintColor = [UIColor darkGrayColor];
        self.pageControl.pageIndicatorTintColor = [UIColor lightGrayColor];
    }
    [self.view addSubview:self.pageControl];
}

- (void)viewDidUnload {
    [super viewDidUnload];
    self.view = nil;
    self.scrollView = nil;
    self.pageControl = nil;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self relayoutViews];

    self.title = [(self.viewControllers)[0] title];
}

- (void)relayoutViews {
    // scrollview
    static CGFloat pageControlHeight = 18;
    CGRect frame = self.view.bounds;
    frame.size.height -= pageControlHeight;
    self.scrollView.frame = frame;
    self.scrollView.contentSize = CGSizeMake(frame.size.width * [self.viewControllers count], frame.size.height);

    // page control
    frame.origin.y = frame.size.height;
    frame.size.height = pageControlHeight;
    self.pageControl.frame = frame;

    for (NSInteger page = 0; page < [self.viewControllers count]; page++)
        [self loadScrollViewWithPage:page];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return (interfaceOrientation != UIDeviceOrientationPortraitUpsideDown);
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    self.isRotating = YES;
    [self setPagesHidden:YES exceptPages:@[@(self.pageControl.currentPage)]];
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
    self.isRotating = NO;
    [self setPagesHidden:NO exceptPages:nil];
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation duration:(NSTimeInterval)duration {
    [self relayoutViews];
    [self changePage:self]; // make sure we are on a page
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Internal

- (void)loadScrollViewWithPage:(NSUInteger)page {
    if (page >= [self.viewControllers count]) return;

    // replace the placeholder if necessary
    UIViewController *controller = (self.viewControllers)[page];

    // add the controller's view to the scroll view
    CGRect frame = self.scrollView.frame;
    frame.origin.x = frame.size.width * page;
    frame.origin.y = 0;
    controller.view.frame = frame;

    if (![self.childViewControllers containsObject:controller]) {
        [self addChildViewController:controller];
        [self.scrollView addSubview:controller.view];
        [controller didMoveToParentViewController:self];
    }
}

- (void)unloadPage:(NSUInteger)page {
    if (page >= [self.viewControllers count]) return;

    UIViewController *controller = (self.viewControllers)[page];
    [controller willMoveToParentViewController:nil];
    [controller removeFromParentViewController];
    [controller.view removeFromSuperview];
}

- (void)scrollViewDidScroll:(UIScrollView *)sender {
    if (self.pageControlUsed || self.isRotating) {
        // do nothing - the scroll was initiated from the page control, not the user dragging
        return;
    }

    // Switch the indicator when more than 50% of the previous/next page is visible
    CGFloat pageWidth = self.scrollView.frame.size.width;
    NSInteger currentPage = floor((self.scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    self.pageControl.currentPage = currentPage;

    if (currentPage < [self.viewControllers count]) {
        self.title = [(self.viewControllers)[currentPage] title];
    }
}

- (void)setPagesHidden:(BOOL)hidden exceptPages:(NSArray *)exceptPages {
    for (NSInteger page = 0; page < [self.viewControllers count]; page++) {
        UIViewController *childVC = (self.viewControllers)[page];
        if (childVC.isViewLoaded && ![exceptPages containsObject:@(page)])
            childVC.view.hidden = hidden;
    }
}

// At the begin of scroll dragging, reset the boolean used when scrolls originate from the UIPageControl
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    self.pageControlUsed = NO;
    [self updateControllers];
}

// At the end of scroll animation, reset the boolean used when scrolls originate from the UIPageControl
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    self.pageControlUsed = NO;
}

// This is a quick hack. We could implement proper containment calling willAppear/disappear, but this works well enough for our color controller.
- (void)updateControllers {
    for (UIViewController *viewController in self.childViewControllers) {
        [viewController viewWillAppear:NO];
    }
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Actions

- (NSUInteger)page {
    return roundf(self.scrollView.bounds.origin.x / self.scrollView.frame.size.width);
}

- (void)setPage:(NSUInteger)page { [self setPage:page animated:NO]; }
- (void)setPage:(NSUInteger)page animated:(BOOL)animated {
    // update the scroll view to the appropriate page
    CGRect frame = self.scrollView.frame;
    frame.origin.x = frame.size.width * page;
    frame.origin.y = 0;
    [self.scrollView scrollRectToVisible:frame animated:animated && !self.isRotating];
    [self updateControllers];
    
    // update title
    if (page < [self.viewControllers count]) {
        self.title = [(self.viewControllers)[0] title];
    }
}

- (IBAction)changePage:(id)sender {
    NSUInteger page = self.pageControl.currentPage;
    [self setPage:page animated:YES];

	// Set the boolean used when scrolls originate from the UIPageControl. See scrollViewDidScroll: above.
    self.pageControlUsed = YES;
}

@end
