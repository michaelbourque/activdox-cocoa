//
//  PSPDFIconGenerator.m
//  PSPDFKit
//
//  Copyright (c) 2012 Peter Steinberger. All rights reserved.
//

#import "PSPDFIconGenerator.h"
#import "PSPDFKitGlobal.h"

@interface PSPDFIconGenerator() {
    dispatch_queue_t _imageCacheQueue;
    NSMutableDictionary *_imageCache;
}
@end

@implementation PSPDFIconGenerator

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Singleton

+ (PSPDFIconGenerator *)sharedGenerator {
    __strong static PSPDFIconGenerator *_sharedGenerator = nil;
    static dispatch_once_t pred;
    dispatch_once(&pred, ^{ _sharedGenerator = [NSClassFromString(kPSPDFIconGeneratorClassName) new]; });
    return _sharedGenerator;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (id)init {
    if ((self = [super init])) {
        _imageCache = [[NSMutableDictionary alloc] initWithCapacity:12];
        _imageCacheQueue = pspdf_dispatch_queue_create("com.PSPDFKit.imageCacheQueue", 0);
    }
    return self;
}

- (void)dealloc {
    PSPDFDispatchRelease(_imageCacheQueue);
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public

// Generates in-code images.
- (UIImage *)iconForType:(PSPDFIconType)iconType {
    return [self iconForType:iconType shadowOffset:CGSizeZero shadowColor:nil];
}

- (UIImage *)iconForType:(PSPDFIconType)iconType shadowOffset:(CGSize)shadowOffset shadowColor:(UIColor *)shadowColor {
    __block UIImage *iconImage = nil;
    dispatch_sync(_imageCacheQueue, ^{

        // try to access the cache
        BOOL shouldDrawShadow = !CGSizeEqualToSize(shadowOffset, CGSizeZero);
        if (!shouldDrawShadow) {
            iconImage = _imageCache[@(iconType)];
        }

        dispatch_block_t afterContextBlock = ^{
            if (shouldDrawShadow) CGContextSetShadowWithColor(UIGraphicsGetCurrentContext(), shadowOffset, 0, shadowColor.CGColor);
        };

        
        if (!iconImage) {
            BOOL unknownType = NO;
            switch (iconType) {
                case PSPDFIconTypeOutline: {
                    UIGraphicsBeginImageContextWithOptions(CGSizeMake(22.f, 22.f), NO, 0.0f);
                    afterContextBlock();
                    [[UIColor whiteColor] setFill];
                    [[UIBezierPath bezierPathWithRect:CGRectMake(2, 6,   3, 3)] fill];
                    [[UIBezierPath bezierPathWithRect:CGRectMake(2, 12,  3, 3)] fill];
                    [[UIBezierPath bezierPathWithRect:CGRectMake(2, 18,  3, 3)] fill];
                    [[UIBezierPath bezierPathWithRect:CGRectMake(7, 6,  17, 3)] fill];
                    [[UIBezierPath bezierPathWithRect:CGRectMake(7, 12, 17, 3)] fill];
                    [[UIBezierPath bezierPathWithRect:CGRectMake(7, 18, 17, 3)] fill];
                }break;

                case PSPDFIconTypePage: {
                    UIGraphicsBeginImageContextWithOptions(CGSizeMake(23.f, 24.f), NO, 0.0f);
                    afterContextBlock();
                    [[UIColor whiteColor] setStroke];
                    UIBezierPath* rectanglePath = [UIBezierPath bezierPathWithRect:CGRectMake(7, 5, 10, 14)];
                    rectanglePath.lineWidth = 2;
                    [rectanglePath stroke];
                }break;

                case PSPDFIconTypeThumbnails: {
                    UIGraphicsBeginImageContextWithOptions(CGSizeMake(26.f, 24.f), NO, 0.0f);
                    afterContextBlock();
                    [[UIColor whiteColor] setFill];
                    [[UIBezierPath bezierPathWithRect:CGRectMake(6,   4, 5, 6)] fill];
                    [[UIBezierPath bezierPathWithRect:CGRectMake(14,  4, 5, 6)] fill];
                    [[UIBezierPath bezierPathWithRect:CGRectMake(6,  13, 5, 6)] fill];
                    [[UIBezierPath bezierPathWithRect:CGRectMake(14, 13, 5, 6)] fill];
                }break;

                case PSPDFIconTypeBackArrow: {
                    UIGraphicsBeginImageContextWithOptions(CGSizeMake(27.f, 22.f), NO, 0.f);
                    afterContextBlock();
                    CGContextRef context = UIGraphicsGetCurrentContext();
                    CGContextSetFillColor(context, CGColorGetComponents([UIColor blackColor].CGColor));
                    CGContextBeginPath(context);
                    CGContextMoveToPoint(context, 8.0f, 13.0f);
                    CGContextAddLineToPoint(context, 24.0f, 4.0f);
                    CGContextAddLineToPoint(context, 24.0f, 22.0f);
                    CGContextClosePath(context);
                    CGContextFillPath(context);
                }break;

                case PSPDFIconTypeBackArrowSmall: {
                    UIGraphicsBeginImageContextWithOptions(CGSizeMake(20.f, 20.f), NO, 0.f);
                    afterContextBlock();
                    CGContextRef context = UIGraphicsGetCurrentContext();
                    CGContextSetFillColor(context, CGColorGetComponents([UIColor blackColor].CGColor));
                    CGContextBeginPath(context);
                    CGContextMoveToPoint(context, 8.0f, 13.0f);
                    CGContextAddLineToPoint(context, 20.0f, 6.0f);
                    CGContextAddLineToPoint(context, 20.0f, 20.0f);
                    CGContextClosePath(context);
                    CGContextFillPath(context);
                }break;

                case PSPDFIconTypeForwardArrow: {
                    UIGraphicsBeginImageContextWithOptions(CGSizeMake(44.f, 44.f), NO, 0.f);
                    afterContextBlock();
                    UIBezierPath* bezierPath = [UIBezierPath bezierPath];
                    [bezierPath moveToPoint:CGPointMake(18, 14.5)];
                    [bezierPath addLineToPoint:CGPointMake(18, 29.5)];
                    [bezierPath addLineToPoint:CGPointMake(28, 22)];
                    [bezierPath addLineToPoint:CGPointMake(18, 14.5)];
                    [bezierPath closePath];
                    [[UIColor darkGrayColor] setFill];
                    [bezierPath fill];
                }break;

                case PSPDFIconTypePrint: {
                    UIGraphicsBeginImageContextWithOptions(CGSizeMake(20.f, 22.f), NO, 0.f);
                    afterContextBlock();
                    [[UIColor whiteColor] setFill];
                    [[UIBezierPath bezierPathWithRoundedRect:CGRectMake(0, 9, 20, 7) cornerRadius:1] fill];
                    [[UIBezierPath bezierPathWithRect:CGRectMake( 3,  3, 14,  5)] fill];
                    [[UIBezierPath bezierPathWithRect:CGRectMake( 2, 10,  1,  1)] fillWithBlendMode:kCGBlendModeClear alpha:1];
                    [[UIBezierPath bezierPathWithRect:CGRectMake( 4, 10,  1,  1)] fillWithBlendMode:kCGBlendModeClear alpha:1];
                    [[UIBezierPath bezierPathWithRect:CGRectMake( 2, 13, 16,  1)] fillWithBlendMode:kCGBlendModeClear alpha:1];
                    [[UIBezierPath bezierPathWithRect:CGRectMake( 2, 14,  1,  1)] fillWithBlendMode:kCGBlendModeClear alpha:1];
                    [[UIBezierPath bezierPathWithRect:CGRectMake(17, 14,  1,  1)] fillWithBlendMode:kCGBlendModeClear alpha:1];
                    [[UIBezierPath bezierPathWithRect:CGRectMake( 3, 16, 14,  3)] fill];
                    [[UIBezierPath bezierPathWithRect:CGRectMake( 4, 15, 12,  1)] fillWithBlendMode:kCGBlendModeClear alpha:1];
                    [[UIBezierPath bezierPathWithRect:CGRectMake( 4, 17, 12,  1)] fillWithBlendMode:kCGBlendModeClear alpha:1];
                }break;

                case PSPDFIconTypeEmail: {
                    UIGraphicsBeginImageContextWithOptions(CGSizeMake(24, 22.f), NO, 0.f);
                    afterContextBlock();
                    [[UIColor whiteColor] setFill];
                    [[UIBezierPath bezierPathWithRoundedRect:CGRectMake(0, 4, 24, 14) cornerRadius:1] fill];
                    UIBezierPath* lowerBezierPath = [UIBezierPath bezierPath];
                    [lowerBezierPath moveToPoint:CGPointMake(0, 18)];
                    [lowerBezierPath addLineToPoint:CGPointMake(12, 5)];
                    [lowerBezierPath addLineToPoint:CGPointMake(24, 18)];
                    [lowerBezierPath strokeWithBlendMode:kCGBlendModeClear alpha:1];
                    UIBezierPath* upperBezierPath = [UIBezierPath bezierPath];
                    [upperBezierPath moveToPoint:CGPointMake(0, 4)];
                    [upperBezierPath addLineToPoint:CGPointMake(12, 12)];
                    [upperBezierPath addLineToPoint:CGPointMake(24, 4)];
                    [upperBezierPath fill];
                    [upperBezierPath strokeWithBlendMode:kCGBlendModeClear alpha:1];
                }break;

                case PSPDFIconTypeAnnotations: {
                    UIGraphicsBeginImageContextWithOptions(CGSizeMake(24, 24.f), NO, 0.f);
                    afterContextBlock();
                    UIBezierPath* roundedRectanglePath = [UIBezierPath bezierPathWithRoundedRect: CGRectMake(1.5, 2.5, 14, 11) cornerRadius: 3];
                    [[UIColor blackColor] setFill];
                    [roundedRectanglePath fill];

                    [[UIColor blackColor] setStroke];
                    roundedRectanglePath.lineWidth = 1;
                    [roundedRectanglePath stroke];

                    UIBezierPath* bezierPath = [UIBezierPath bezierPath];
                    [bezierPath moveToPoint: CGPointMake(5.5, 13.5)];
                    [bezierPath addLineToPoint: CGPointMake(8.5, 16.5)];
                    [bezierPath addLineToPoint: CGPointMake(11.5, 13.5)];
                    [bezierPath addLineToPoint: CGPointMake(5.5, 13.5)];
                    [bezierPath closePath];
                    [[UIColor blackColor] setFill];
                    [bezierPath fill];

                    [[UIColor blackColor] setStroke];
                    bezierPath.lineWidth = 1;
                    [bezierPath stroke];

                    UIBezierPath* bezier2Path = [UIBezierPath bezierPath];
                    [bezier2Path moveToPoint: CGPointMake(12.5, 18.5)];
                    [bezier2Path addCurveToPoint: CGPointMake(21.5, 8.5) controlPoint1: CGPointMake(21.5, 8.5) controlPoint2: CGPointMake(21.5, 8.5)];
                    [bezier2Path addLineToPoint: CGPointMake(23.5, 10.5)];
                    [bezier2Path addLineToPoint: CGPointMake(14.5, 20.5)];
                    [[UIColor blackColor] setFill];
                    [bezier2Path fill];

                    [[UIColor blackColor] setStroke];
                    bezier2Path.lineWidth = 1;
                    [bezier2Path stroke];

                    UIBezierPath* bezier4Path = [UIBezierPath bezierPath];
                    [bezier4Path moveToPoint: CGPointMake(11.5, 19.5)];
                    [bezier4Path addLineToPoint: CGPointMake(10.5, 22.5)];
                    [bezier4Path addLineToPoint: CGPointMake(13.5, 21.5)];
                    [bezier4Path addLineToPoint: CGPointMake(11.5, 19.5)];
                    [bezier4Path closePath];
                    [[UIColor blackColor] setFill];
                    [bezier4Path fill];

                    [[UIColor blackColor] setStroke];
                    bezier4Path.lineWidth = 1;
                    [bezier4Path stroke];
                }break;

                case PSPDFIconTypeBookmarkActive:
                case PSPDFIconTypeBookmark: {
                    UIGraphicsBeginImageContextWithOptions(CGSizeMake(24.f, 24.f), NO, 0.f);
                    afterContextBlock();
                    UIBezierPath* bezierPath = [UIBezierPath bezierPath];
                    [bezierPath moveToPoint: CGPointMake(17.5, 20.5)];
                    [bezierPath addLineToPoint: CGPointMake(12.5, 13.5)];
                    [bezierPath addLineToPoint: CGPointMake(7.65, 20.5)];
                    [bezierPath addLineToPoint: CGPointMake(7.5, 1.5)];
                    [bezierPath addLineToPoint: CGPointMake(17.5, 1.5)];
                    [bezierPath addLineToPoint: CGPointMake(17.5, 20.5)];
                    [bezierPath closePath];

                    if (iconType == PSPDFIconTypeBookmarkActive) {
                        [[UIColor blackColor] setFill];
                        [bezierPath fill];
                    }

                    [[UIColor blackColor] setStroke];
                    bezierPath.lineWidth = 2;
                    [bezierPath stroke];
                }break;

                case PSPDFIconTypeBrightness: {
                    UIGraphicsBeginImageContextWithOptions(CGSizeMake(24.f, 24.f), NO, 0.f);
                    afterContextBlock();
                    
                    UIBezierPath* ovalPath = [UIBezierPath bezierPathWithOvalInRect: CGRectMake(6.5, 6.5, 11, 11)];
                    [[UIColor blackColor] setStroke];
                    ovalPath.lineWidth = 2;
                    [ovalPath stroke];

                    UIBezierPath* bezier5Path = [UIBezierPath bezierPath];
                    [bezier5Path moveToPoint: CGPointMake(3, 3)];
                    [bezier5Path addCurveToPoint: CGPointMake(6.5, 6.5) controlPoint1: CGPointMake(6.5, 6.5) controlPoint2: CGPointMake(6.5, 6.5)];
                    [[UIColor blackColor] setStroke];
                    bezier5Path.lineWidth = 1.5;
                    [bezier5Path stroke];

                    UIBezierPath* bezier6Path = [UIBezierPath bezierPath];
                    [bezier6Path moveToPoint: CGPointMake(17.5, 17.5)];
                    [bezier6Path addCurveToPoint: CGPointMake(21, 21) controlPoint1: CGPointMake(21, 21) controlPoint2: CGPointMake(21, 21)];
                    [[UIColor blackColor] setStroke];
                    bezier6Path.lineWidth = 1.5;
                    [bezier6Path stroke];

                    UIBezierPath* bezier7Path = [UIBezierPath bezierPath];
                    [bezier7Path moveToPoint: CGPointMake(21, 3)];
                    [bezier7Path addCurveToPoint: CGPointMake(17.5, 7) controlPoint1: CGPointMake(17.5, 7) controlPoint2: CGPointMake(17.5, 7)];
                    [[UIColor blackColor] setStroke];
                    bezier7Path.lineWidth = 1.5;
                    [bezier7Path stroke];

                    UIBezierPath* bezier8Path = [UIBezierPath bezierPath];
                    [bezier8Path moveToPoint: CGPointMake(6.5, 17)];
                    [bezier8Path addCurveToPoint: CGPointMake(3, 21) controlPoint1: CGPointMake(3, 21) controlPoint2: CGPointMake(3, 21)];
                    [[UIColor blackColor] setStroke];
                    bezier8Path.lineWidth = 1.5;
                    [bezier8Path stroke];

                    UIBezierPath* bezierPath = [UIBezierPath bezierPath];
                    [[UIColor blackColor] setStroke];
                    bezierPath.lineWidth = 1;
                    [bezierPath stroke];

                    UIBezierPath* bezier9Path = [UIBezierPath bezierPath];
                    [bezier9Path moveToPoint: CGPointMake(12, 0.5)];
                    [bezier9Path addCurveToPoint: CGPointMake(12, 5) controlPoint1: CGPointMake(12, 5.5) controlPoint2: CGPointMake(12, 5)];
                    [[UIColor blackColor] setStroke];
                    bezier9Path.lineWidth = 2;
                    [bezier9Path stroke];
                    
                    UIBezierPath* bezier2Path = [UIBezierPath bezierPath];
                    [bezier2Path moveToPoint: CGPointMake(12, 19)];
                    [bezier2Path addCurveToPoint: CGPointMake(12, 23.5) controlPoint1: CGPointMake(12, 24) controlPoint2: CGPointMake(12, 23.5)];
                    [[UIColor blackColor] setStroke];
                    bezier2Path.lineWidth = 2;
                    [bezier2Path stroke];
                    
                    UIBezierPath* bezier4Path = [UIBezierPath bezierPath];
                    [bezier4Path moveToPoint: CGPointMake(0, 12)];
                    [bezier4Path addCurveToPoint: CGPointMake(5, 12) controlPoint1: CGPointMake(5, 12) controlPoint2: CGPointMake(4.72, 12)];
                    [[UIColor blackColor] setStroke];
                    bezier4Path.lineWidth = 2;
                    [bezier4Path stroke];
                    
                    UIBezierPath* bezier10Path = [UIBezierPath bezierPath];
                    [bezier10Path moveToPoint: CGPointMake(19, 12)];
                    [bezier10Path addCurveToPoint: CGPointMake(24, 12) controlPoint1: CGPointMake(24, 12) controlPoint2: CGPointMake(23.72, 12)];
                    [[UIColor blackColor] setStroke];
                    bezier10Path.lineWidth = 2;
                    [bezier10Path stroke];
                    
                }break;

                default:
                    PSPDFLogWarning(@"Unknown type: %d", iconType);
                    unknownType = YES;
            }
            if (!unknownType) {
                iconImage = UIGraphicsGetImageFromCurrentImageContext();
                UIGraphicsEndImageContext();
                
                // save in cache
                if (!shouldDrawShadow) {
                    _imageCache[@(iconType)] = iconImage;
                }
            }
        }
    });
    return iconImage;
}

@end
