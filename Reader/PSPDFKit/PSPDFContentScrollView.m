//
//  PSPDFContentScrollView.m
//  PSPDFKit
//
//  Copyright (c) 2011-2012 Peter Steinberger. All rights reserved.
//

#import "PSPDFContentScrollView.h"
#import "PSPDFTransitionHelper.h"
#import "PSPDFViewController.h"

@interface PSPDFScrollView (PSPDFContentScrollViewInternal)
@property (nonatomic, strong) PSPDFDocument *document;
@end

@interface PSPDFContentScrollView ()
@property (nonatomic, strong, readwrite) UIViewController<PSPDFTransitionProtocol> *contentController;
@end

@implementation PSPDFContentScrollView

static char kPSPDFContentScrollViewContext;

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Private

// properly deregister KVO and pointers.
- (void)deregisterPageController {
    [_contentController removeObserver:self forKeyPath:NSStringFromSelector(@selector(pdfController))];
    _contentController.scrollView = nil;
    _contentController = nil;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (id)initWithTransitionViewController:(UIViewController<PSPDFTransitionProtocol> *)viewController {
    if ((self = [super initWithFrame:viewController.view.bounds])) {
        // already registered as sub-object
        _contentController = viewController;
        self.pdfController = viewController.pdfController;
        self.document = viewController.pdfController.document;
        self.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        self.delegate = self;
        self.maximumZoomScale = viewController.pdfController.maximumZoomScale;
        self.scrollOnTapPageEndEnabled = self.pdfController.scrollOnTapPageEndEnabled;
        self.shadowEnabled = NO; // shadow is drawn by pages themselves
        viewController.scrollView = self;
        [self addSubview:viewController.view];
        
        // use KVO to detect if PSPDFViewController gets deallocated.
        [viewController addObserver:self forKeyPath:NSStringFromSelector(@selector(pdfController)) options:0 context:&kPSPDFContentScrollViewContext];
    }
    return self;
}

- (void)dealloc {
    [self deregisterPageController];
    self.delegate = nil;
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    if (context == &kPSPDFContentScrollViewContext) {
        if (self.contentController.pdfController == nil) {
            [self deregisterPageController];
        }
    }else {
        [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
    }
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - PSPDFScrollView

- (UIView *)compoundView {
    return _contentController.view;
}

@end
