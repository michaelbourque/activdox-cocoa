//
//  PSPDFSearchResult.m
//  PSPDFKit
//
//  Copyright 2011-2012 Peter Steinberger. All rights reserved.
//

#import "PSPDFSearchResult.h"

@implementation PSPDFSearchResult

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (NSString *)description {
    return [NSString stringWithFormat:@"<%@ page:%d selection:%@ previewText:%@>", NSStringFromClass([self class]), _pageIndex, _selection, _previewText];
}

- (NSUInteger)hash {
    return ((_range.location * 31) + _range.length) * 31 + _pageIndex;
}

- (BOOL)isEqual:(id)other {
    if ([other isKindOfClass:[self class]]) {
        return [self isEqualToSearchResult:(PSPDFSearchResult *)other];
    }
    return NO;
}

- (BOOL)isEqualToSearchResult:(PSPDFSearchResult *)otherSearchResult {
        if (self.pageIndex == otherSearchResult.pageIndex && NSEqualRanges(self.range, otherSearchResult.range) && self.document == otherSearchResult.document) {
            return YES;
        }
    return NO;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSCopying

- (id)copyWithZone:(NSZone *)zone {
    PSPDFSearchResult *searchResult = [[[self class] alloc] init];
    searchResult.document = self.document;
    searchResult.pageIndex = self.pageIndex;
    searchResult.previewText = self.previewText;
    searchResult.selection = self.selection;
    searchResult.range = self.range;
    searchResult.rangeInPreviewText = self.rangeInPreviewText;
    searchResult.cachedOutlineTitle = self.cachedOutlineTitle;
    return searchResult;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSCoding

- (id)initWithCoder:(NSCoder *)decoder {
	if((self = [super init])) {
        _pageIndex = [decoder decodeIntegerForKey:NSStringFromSelector(@selector(pageIndex))];
        _previewText = [decoder decodeObjectForKey:NSStringFromSelector(@selector(previewText))];
        _selection = [decoder decodeObjectForKey:NSStringFromSelector(@selector(selection))];
        _range = [[decoder decodeObjectForKey:NSStringFromSelector(@selector(range))] rangeValue];
        _rangeInPreviewText = [[decoder decodeObjectForKey:NSStringFromSelector(@selector(rangeInPreviewText))] rangeValue];
    }
	return self;
}

- (void)encodeWithCoder:(NSCoder *)coder {
    [coder encodeInteger:_pageIndex forKey:NSStringFromSelector(@selector(pageIndex))];
    [coder encodeObject:_previewText forKey:NSStringFromSelector(@selector(previewText))];
    [coder encodeObject:_selection forKey:NSStringFromSelector(@selector(selection))];
    [coder encodeObject:BOXED(_range) forKey:NSStringFromSelector(@selector(range))];
    [coder encodeObject:BOXED(_rangeInPreviewText) forKey:NSStringFromSelector(@selector(rangeInPreviewText))];
}

@end
