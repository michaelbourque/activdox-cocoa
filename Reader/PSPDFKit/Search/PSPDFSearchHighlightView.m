//
//  PSPDFSearchHighlightView.m
//  PSPDFKit
//
//  Copyright (c) 2011-2012 Peter Steinberger. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "PSPDFPageInfo.h"
#import "PSPDFSearchHighlightView.h"
#import "PSPDFSearchResult.h"
#import "PSPDFDocument.h"
#import "PSPDFConverter.h"
#import "PSPDFWord.h"
#import "PSPDFTextBlock.h"

// Slightly increase the calculated height to make finding the highlight easier
#define kPSPDFSearchHighlightIncreaseHeight 1

@interface PSPDFSingleSearchHighlightView : UIView @end
@implementation PSPDFSingleSearchHighlightView @end

@interface PSPDFSearchHighlightView ()
@property (nonatomic, strong) NSMutableArray *searchViews;
@end

@implementation PSPDFSearchHighlightView

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (id)initWithSearchResult:(PSPDFSearchResult *)searchResult {
    if ((self = [super initWithFrame:CGRectZero])) {
        self.selectionBackgroundColor = [[UIColor yellowColor] colorWithAlphaComponent:0.5f];
        self.searchViews = [NSMutableArray new];
        self.searchResult = searchResult;
        [self setNeedsLayout];
    }
    return self;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UIView

- (void)layoutSubviews {
    [super layoutSubviews];
    
    if (self.superview) {
        PSPDFTextBlock *selection = _searchResult.selection;
        PSPDFPageInfo *pageInfo = [_searchResult.document pageInfoForPage:_searchResult.pageIndex];
        CGRect frame = PSPDFConvertPDFRectToViewRect(selection.frame, pageInfo.rotatedPageRect, 0, self.superview.bounds);

        NSArray *words = self.searchResult.selection.words;
        if ([words count] == [self.searchViews count]) {
            [words enumerateObjectsUsingBlock:^(PSPDFWord *word, NSUInteger idx, BOOL *stop) {
                UIView *subview = self.searchViews[idx];
                CGRect wordFrame = PSPDFConvertPDFRectToViewRect(word.frame, pageInfo.rotatedPageRect, 0, self.superview.bounds);
                CGRect subviewFrame = CGRectMake(wordFrame.origin.x-frame.origin.x, wordFrame.origin.y-frame.origin.y, wordFrame.size.width, wordFrame.size.height);

                // increase size
                subviewFrame.origin.y -= kPSPDFSearchHighlightIncreaseHeight;
                subviewFrame.size.height += kPSPDFSearchHighlightIncreaseHeight * 2;
                subview.frame = subviewFrame;
            }];
        }
        self.frame = frame;
    }
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public

- (void)popupAnimation {
    [self.searchViews enumerateObjectsUsingBlock:^(UIView *subview, NSUInteger idx, BOOL *stop) {
        subview.transform = CGAffineTransformMakeScale(2, 2);
    }];
    [UIView animateWithDuration:0.6f delay:0.f options:UIViewAnimationOptionAllowUserInteraction animations:^{
        [self.searchViews enumerateObjectsUsingBlock:^(UIView *subview, NSUInteger idx, BOOL *stop) {
            subview.transform = CGAffineTransformIdentity;
        }];
    } completion:nil];
}

- (void)setSearchResult:(PSPDFSearchResult *)searchResult {
    if (searchResult != _searchResult) {
        _searchResult = searchResult;

        // create new set of internal views
        [self.searchViews makeObjectsPerformSelector:@selector(removeFromSuperview)];
        for (PSPDFWord *word in self.searchResult.selection.words) {
            UIView *subview = [[PSPDFSingleSearchHighlightView alloc] initWithFrame:CGRectZero];
            subview.backgroundColor = self.selectionBackgroundColor;
            subview.layer.cornerRadius = 4.f;
            [self addSubview:subview];
            [self.searchViews addObject:subview];
        }

        // reposition everything
        [self setNeedsLayout];
    }
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - PSPDFAnnotationView

- (void)didChangePageFrame:(CGRect)frame {
    [self setNeedsLayout];
}

@end
