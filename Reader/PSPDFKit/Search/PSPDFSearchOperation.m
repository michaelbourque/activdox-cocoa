//
//  PSPDFSearchOperation.m
//  PSPDFKit
//
//  Copyright (c) 2011-2012 Peter Steinberger. All rights reserved.
//

#import "PSPDFKit.h"
#import "PSPDFSearchOperation.h"
#import "PSPDFTextSearch.h"
#import "PSPDFTextParser.h"
#import "PSPDFOutlineParser.h"
#import "PSPDFWord.h"
#import "PSPDFTextBlock.h"

// tune the text preview algorithm
#define kSearchPreviewSubStringStart 20
#define kSearchPreviewSubStringEnd 70

@interface PSPDFSearchOperation() {
    NSMutableArray *_searchResults;
    NSMutableDictionary *_pageTextDict;
}
@property (nonatomic, copy) NSString *searchTerm;
@property (nonatomic, copy) NSArray *searchResults;
@property (nonatomic, weak) PSPDFDocument *document;
// internally only
@property (nonatomic, strong) NSMutableSet *selectionsUsed;

// deprecation support
@property (nonatomic, copy) NSArray *selectionSearchPages __attribute__ ((deprecated("Use pageRanges instead.")));
@property (nonatomic, copy) NSArray *searchPages __attribute__ ((deprecated("Use pageRanges instead.")));
@end

@implementation PSPDFSearchOperation

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (id)initWithDocument:(PSPDFDocument *)document searchTerm:(NSString *)sarchTerm {
    if ((self = [super init])) {
        _document = document; // weak
        _searchTerm = [sarchTerm copy];
        _selectionsUsed = [NSMutableSet new];
        _searchResults = [NSMutableArray new];
        _pageTextDict = [[NSMutableDictionary alloc] initWithCapacity:[document pageCount]];
    }
    return self;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSOperation

// thread entry point
- (void)main {
    if ([_delegate respondsToSelector:@selector(willStartSearchOperation:forString:isFullSearch:)]) {
        [_delegate willStartSearchOperation:self forString:self.searchTerm isFullSearch:_searchPages == nil];
    }

    // ensure outline is parsed - we use outline in the controller for titles,
    // and if it's not parsed in the background we would freeze the UI.
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        [[_document outlineParser] outline];
    });

    // priorize _pageRanges
    [_pageRanges enumerateIndexesUsingBlock:^(NSUInteger page, BOOL *stop) {
        if ([self isCancelled]) { return; } // stop search on cancel
        NSArray *results = [self searchPage:page withSelection:YES];
        [_delegate didUpdateSearchOperation:self forString:self.searchTerm newSearchResults:results forPage:page];
    }];

    // search all pages
    if (self.shouldSearchAllPages) {
        for (int page = 0; page < [_document pageCount]; page++) {
            if ([self isCancelled]) return; // stop search on cancel
            if ([_pageRanges containsIndex:page]) continue; // stop if already searched

            NSArray *results = [self searchPage:page withSelection:YES];
            [_delegate didUpdateSearchOperation:self forString:self.searchTerm newSearchResults:results forPage:page];
        }
    }
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public

- (void)setPageTextDict:(NSDictionary *)pageTextDict {
    if ([self isExecuting]) return; // only allow setting before operation starts.
    if (pageTextDict != _pageTextDict) {
        _pageTextDict = [pageTextDict mutableCopy];
    }
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Private

// get preview on a specific NSRange from pageText
- (NSString *)previewTextForRange:(NSRange)range pageText:(NSString *)pageText rangeInPreviewText:(NSRange *)rangeInPreviewText {
    NSRange previewRange = range;
    if (previewRange.location > kSearchPreviewSubStringStart) {
        previewRange.location -= kSearchPreviewSubStringStart;
    }else {
        previewRange.location = 0;
    }

    if (previewRange.location + previewRange.length + kSearchPreviewSubStringEnd < [pageText length]) {
        previewRange.length += kSearchPreviewSubStringEnd;
    }else {
        previewRange.length = [pageText length] - previewRange.location; // right to the end
    }

    // prevent cutting in *between* words
    NSRange originalRange = previewRange;
    while (previewRange.length > 0 && previewRange.location > 0 && previewRange.location + previewRange.length < [pageText length] && previewRange.location < range.location) {
        unichar character = [pageText characterAtIndex:previewRange.location-1];
        if (character == ' ' || character == '\n') break; // if space

        previewRange.length--;
        previewRange.location++;
    }

    // we failed (one really long word?) restore original range
    if (previewRange.length == 0) previewRange = originalRange;

    // Replace newlines with spaces, and fix double spaces that we might just added because of that.
    NSMutableString *previewText = [[pageText substringWithRange:previewRange] mutableCopy];
    [previewText replaceOccurrencesOfString:@"\n" withString:@" " options:self.compareOptions range:NSMakeRange(0, [previewText length])];

    // check before search term
    NSUInteger replacementsBeforeSearchTerm = [previewText replaceOccurrencesOfString:@"  " withString:@" " options:self.compareOptions range:NSMakeRange(0, range.location - previewRange.location)];
    previewRange.length -= replacementsBeforeSearchTerm;

    // check after search term
    NSUInteger positionAfterSearchTerm = range.location - previewRange.location + range.length;
    if ([previewText length] > positionAfterSearchTerm) {
        NSUInteger replacementsAfterSearchTerm = [previewText replaceOccurrencesOfString:@"  " withString:@" " options:self.compareOptions range:NSMakeRange(positionAfterSearchTerm, [previewText length] - positionAfterSearchTerm)];
        previewRange.length -= replacementsAfterSearchTerm;
    }

    // return preview range
    if (rangeInPreviewText) {
        *rangeInPreviewText = NSMakeRange(range.location - previewRange.location - replacementsBeforeSearchTerm, range.length);
    }

    return previewText;
}

// search a page for .searchText, with or without withSelection. Returns partial results.
- (NSArray *)searchPage:(NSUInteger)page withSelection:(BOOL)withSelection {
    PSPDFLogVerbose(@"Extracting text from %@, page %d withSelection:%d", _document.title, page, withSelection);
    NSMutableArray *results = [NSMutableArray array];

    @autoreleasepool {
        NSUInteger lastFoundLoc = 0;

        // get page content and save it into dict. first try document function
        NSString *pageText = [_document pageContentForPage:page];

        // if selection is not required, try loading already parsed text from the dictionary.
        if (!withSelection || [pageText length] == 0) {
            pageText = _pageTextDict[@(page)];
        }

        // if we need selection, go parse the page
        BOOL needLoadRef = !pageText || (withSelection && _searchMode > PSPDFSearchModeBasic);
        PSPDFTextParser *textParser = nil;
        if (needLoadRef) {
            textParser = [_document textParserForPage:page];
            pageText = textParser.text;
            if (pageText) {
                _pageTextDict[@(page)] = pageText;
            }
        }

        // prepare text to find ignoring hyphenations and newlines if NSRegularExpressionSearch is enabled.
        NSString *searchPageText = pageText;
        NSString *searchTerm = self.searchTerm;

        if (self.compareOptions & NSDiacriticInsensitiveSearch) {
            NSMutableString *transformedSearchTerm = [searchTerm mutableCopy];
            CFStringTransform((__bridge CFMutableStringRef)(transformedSearchTerm), NULL, kCFStringTransformStripCombiningMarks, NO);
            searchTerm = transformedSearchTerm;
        }

        if (self.compareOptions & NSRegularExpressionSearch) {
            NSMutableString *newSearchTerm = [NSMutableString string];
            [searchTerm enumerateSubstringsInRange:NSMakeRange(0, [searchTerm length]) options:NSStringEnumerationByComposedCharacterSequences usingBlock:^(NSString *substring, NSRange substringRange, NSRange enclosingRange, BOOL *stop) {

                // allow searching for quotation marks.
                // http://en.wikipedia.org/wiki/Quotation_mark_glyphs
                if ([substring isEqualToString:@"'"]) [newSearchTerm appendString:@"[‘’']"];
                else if ([substring isEqualToString:@"\""]) [newSearchTerm appendString:@"[\"“”]"];
                else {
                    // If you think WTF, read here. Unicode is a bitch. http://en.wikipedia.org/wiki/Hyphen
                    if ([newSearchTerm length]) [newSearchTerm appendString:@"([-‑⁃‒–—]\\n)?"]; // skip first letter
                    [newSearchTerm appendString:substring];
                }
            }];
            searchTerm = newSearchTerm;

            // do manually, since this will be ignored when NSRegularExpressionSearch is set.
            if (self.compareOptions & NSDiacriticInsensitiveSearch) {
                NSMutableString *transformedPageText = [searchPageText mutableCopy];
                CFStringTransform((__bridge CFMutableStringRef)(transformedPageText), NULL, kCFStringTransformStripCombiningMarks, NO);
                searchPageText = transformedPageText;
            }
        }

        NSRange range;
        do {
            range = [searchPageText rangeOfString:searchTerm options:self.compareOptions range:NSMakeRange(lastFoundLoc, [searchPageText length] - lastFoundLoc)];
            if (range.length > 0) {
                PSPDFSearchResult *searchResult = [[PSPDFSearchResult alloc] init];
                searchResult.pageIndex = page;
                searchResult.document = _document;
                searchResult.range = range;
                NSRange rangeInPreviewText;
                searchResult.previewText = [self previewTextForRange:range pageText:searchPageText rangeInPreviewText:&rangeInPreviewText];
                searchResult.rangeInPreviewText = rangeInPreviewText;
                if (textParser && _searchMode > PSPDFSearchModeBasic) {
                    PSPDFTextBlock *textBlock = [[PSPDFTextBlock alloc] initWithGlyphs:[textParser.glyphs subarrayWithRange:range]];
                    searchResult.selection = textBlock;
                }
                lastFoundLoc = range.location + range.length;
                [results addObject:searchResult];
            }
        }while (range.length > 0);
    }
    [_searchResults addObjectsFromArray:results];
    return results;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Deprecation support

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-declarations"

// merge both searchPages and selectionSearchPages and set new pageRange.
- (void)checkAndUpdatePageRanges {
    if (self.searchPages && self.selectionSearchPages) {
        NSIndexSet *selectionIndexSet = PSPDFIndexSetFromArray(self.selectionSearchPages);
        NSMutableIndexSet *searchPagesIndexSet = [PSPDFIndexSetFromArray(self.searchPages) mutableCopy];
        [searchPagesIndexSet addIndexes:selectionIndexSet];
        self.pageRanges = searchPagesIndexSet;
    }
}

- (void)setSelectionSearchPages:(NSArray *)selectionSearchPages {
    if (selectionSearchPages != _selectionSearchPages) {
        _selectionSearchPages = selectionSearchPages;
        [self checkAndUpdatePageRanges];
    }
}

- (void)setSearchPages:(NSArray *)searchPages {
    if (searchPages != _searchPages) {
        _searchPages = searchPages;
        [self checkAndUpdatePageRanges];
    }
}

__attribute__ ((deprecated)) const NSUInteger PSPDFBasicSearch = PSPDFSearchModeBasic;
__attribute__ ((deprecated)) const NSUInteger PSPDFSearchWithHighlighting = PSPDFSearchModeHighlighting;

#pragma clang diagnostic pop

@end
