//
//  PSPDFDocumentSearch.m
//  PSPDFKit
//
//  Copyright 2011-2012 Peter Steinberger. All rights reserved.
//

#import "PSPDFKit.h"
#import "PSPDFTextSearch.h"
#import "PSPDFSearchOperation.h"
#import "PSPDFTextParser.h"
#import "PSPDFViewController+Internal.h"

@interface PSPDFTextSearch()
@property (nonatomic, strong) NSOperationQueue *searchQueue;
@end

@implementation PSPDFTextSearch

static char kPSPDFKVOToken;

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (id)initWithDocument:(PSPDFDocument *)document {
    if ((self = [super init])) {
        _searchMode = PSPDFSearchModeHighlighting;
        _document = document;
        _compareOptions = NSCaseInsensitiveSearch | NSDiacriticInsensitiveSearch | NSWidthInsensitiveSearch | NSRegularExpressionSearch;
        _searchQueue = [[NSOperationQueue alloc] init];
        _searchQueue.name = @"SearchQueue";
        _searchQueue.maxConcurrentOperationCount = 1;
        [_searchQueue addObserver:self forKeyPath:@"operationCount" options:0 context:&kPSPDFKVOToken];
    }
    return self;
}

- (void)dealloc {
    _delegate = nil;
    [self cancelAllOperationsAndWait];
    [_searchQueue removeObserver:self forKeyPath:@"operationCount" context:&kPSPDFKVOToken];
    [[PSPDFCache sharedCache] resumeCachingForService:self];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    // KVO to dynamically stop caching when we're searching
    if (context == &kPSPDFKVOToken) {
        if (object == _searchQueue && [keyPath isEqualToString:@"operationCount"]) {
            if (PSPDFIsCrappyDevice()) {
                if (_searchQueue.operationCount == 0) {
                    [[PSPDFCache sharedCache] resumeCachingForService:self];
                }else {
                    [[PSPDFCache sharedCache] pauseCachingForService:self];
                }
            }
        }
    }else {
        [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
    }
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public

- (void)cancelAllOperationsAndWait {
    [_searchQueue cancelAllOperations];
    [_searchQueue waitUntilAllOperationsAreFinished];
}

- (void)searchForString:(NSString *)searchTerm {
    [self searchForString:searchTerm inRanges:nil rangesOnly:NO];
}

- (void)searchForString:(NSString *)searchTerm inRanges:(NSIndexSet *)ranges rangesOnly:(BOOL)onlyVisible {
    PSPDFSearchOperation *searchOperation = [[[self.document classForClass:[PSPDFSearchOperation class]] alloc] initWithDocument:_document searchTerm:searchTerm];
    searchOperation.compareOptions = self.compareOptions;
    searchOperation.pageRanges = ranges;
    searchOperation.shouldSearchAllPages = !onlyVisible;
    searchOperation.delegate = self;
    searchOperation.searchMode = _searchMode;

    // copy over page text dict if not yet parsed
    __weak PSPDFSearchOperation *weakSearchOperation = searchOperation;
    [searchOperation setCompletionBlock:^{
        // call back delegate
        dispatch_sync(dispatch_get_main_queue(), ^{
            PSPDFSearchOperation *strongSearchOperation = weakSearchOperation;
            if (strongSearchOperation.isFinished) {
                if ([self.delegate respondsToSelector:@selector(didFinishSearch:forTerm:searchResults:isFullSearch:)]) {
                    [self.delegate didFinishSearch:self forTerm:strongSearchOperation.searchTerm searchResults:strongSearchOperation.searchResults isFullSearch:!onlyVisible];
                }
            }else {
                if ([self.delegate respondsToSelector:@selector(didCancelSearch:forTerm:isFullSearch:)]) {
                    [self.delegate didCancelSearch:self forTerm:strongSearchOperation.searchTerm isFullSearch:!onlyVisible];
                }
            }
        });
    }];
    // start search
    [_searchQueue cancelAllOperations];
    [_searchQueue addOperation:searchOperation];
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSCopying

- (id)copyWithZone:(NSZone *)zone {
    PSPDFTextSearch *textSearch = [[[self class] alloc] initWithDocument:self.document];
    textSearch.searchMode = self.searchMode;
    textSearch.compareOptions = self.compareOptions;
    return textSearch;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - PSPDFSearchOperationDelegate

- (void)willStartSearchOperation:(PSPDFSearchOperation *)operation forString:(NSString *)searchString isFullSearch:(BOOL)isFullSearch {
    dispatch_async(dispatch_get_main_queue(), ^{
        if (!operation.isCancelled) {
            if ([self.delegate respondsToSelector:@selector(willStartSearch:forTerm:isFullSearch:)]) {
                [self.delegate willStartSearch:self forTerm:searchString isFullSearch:isFullSearch];
            }
        }
    });
}

- (void)didUpdateSearchOperation:(PSPDFSearchOperation *)operation forString:(NSString *)searchString newSearchResults:(NSArray *)searchResults forPage:(NSUInteger)page {
    dispatch_async(dispatch_get_main_queue(), ^{
        if (!operation.isCancelled) {
            if ([self.delegate respondsToSelector:@selector(didUpdateSearch:forTerm:newSearchResults:forPage:)]) {
                [self.delegate didUpdateSearch:self forTerm:searchString newSearchResults:searchResults forPage:page];
            }
        }
    });
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Deprecated

- (BOOL)hasTextForPage:(NSUInteger)page {
    return [_document hasLoadedTextParserForPage:page];
}

- (NSString *)textForPage:(NSUInteger)page {
    return [_document textParserForPage:page].text;
}

- (void)searchForString:(NSString *)searchTerm visiblePages:(NSArray *)visiblePages onlyVisible:(BOOL)onlyVisible {
    [self searchForString:searchTerm inRanges:PSPDFIndexSetFromArray(visiblePages) rangesOnly:onlyVisible];
}

@end
