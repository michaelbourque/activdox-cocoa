//
//  PSPDFSearchViewController.m
//  PSPDFKit
//
//  Copyright 2011-2012 Peter Steinberger. All rights reserved.
//

#import "PSPDFSearchViewController.h"
#import "PSPDFDocument.h"
#import "PSPDFSearchResult.h"
#import "PSPDFViewController.h"
#import "PSPDFHighlightAnnotationView.h"
#import "PSPDFSearchHighlightView.h"
#import "PSPDFSearchStatusCell.h"
#import "PSPDFOutlineParser.h"
#import "PSPDFOutlineElement.h"
#import "PSPDFTextSearch.h"
#import "PSPDFBarButtonItem.h"
#import "PSPDFViewController+Internal.h"
#import <CoreText/CoreText.h>

// used in UITableViewCell
#define kPSPDFAttributedLabelTag 25633

NSUInteger kPSPDFMinimumSearchLength = 2;

@interface PSPDFSearchViewController() {    
    NSMutableArray *_searchResults;
}

@property (nonatomic, strong) PSPDFTextSearch *textSearch;
@property (nonatomic, strong) PSPDFDocument *document;
@property (nonatomic, copy) NSArray *searchResults;
@property (nonatomic, strong) UISearchBar *searchBar;
@property (nonatomic, assign) PSPDFSearchStatus searchStatus;
@property (nonatomic, weak) PSPDFViewController *pdfController;
@end

@implementation PSPDFSearchViewController

// Our controller get destroyed after viewing
static __strong NSString *_lastSearchString;
static __strong NSString *_lastDocumentUID;

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (id)initWithDocument:(PSPDFDocument *)document pdfController:(PSPDFViewController *)pdfController {
    if ((self = [super init])) {
        _document = document;
        _pdfController = pdfController; // weak
        _maximumNumberOfSearchResultsDisplayed = 600;

        // early initialize searchBar to make it customizable
        _searchBar = [[UISearchBar alloc] initWithFrame:CGRectZero];
        _searchBar.tintColor = self.pdfController.tintColor;
        _searchBar.showsCancelButton = _showsCancelButton;
        _searchBar.placeholder = PSPDFLocalize(@"Search Document");

        // initialize private copy of textParser
        _textSearch = [document.textSearch copy];
        _textSearch.delegate = self;

        [self updateContentSize];
    }
    return self;
}

- (void)dealloc {
    // cancel search async (but don't in any case retain self here, or BOOM.)
    PSPDFTextSearch *textSearch = _textSearch;
    textSearch.delegate = nil;
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        [textSearch cancelAllOperationsAndWait];
    });
    [[PSPDFCache sharedCache] removeDelegate:self];
    _searchBar.delegate = nil;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UIView

- (void)viewDidLoad {
    [super viewDidLoad];
    self.searchBar.frame = CGRectMake(0.f, 0.f, self.view.bounds.size.width, 0.f);
    [self.searchBar sizeToFit];
    self.searchBar.delegate = self;
    self.tableView.tableHeaderView = self.searchBar;
    self.tableView.scrollEnabled = NO; // disable scroll initially
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

    // initially, search is idle
    self.searchStatus = PSPDFSearchStatusIdle;

    // restore last search string if document is the same
    if (_lastSearchString && (!_lastDocumentUID || [_lastDocumentUID isEqual:[_document UID]])) {
        // only set if different, else we start a new search
        if (![self.searchBar.text isEqualToString:_lastSearchString]) {
            self.searchBar.text = _lastSearchString;
        }
    }

    // only show keyboard if there is no searchText set externally
    if ([self.searchText length] == 0) {
        [self.searchBar becomeFirstResponder];
    }
    [[PSPDFCache sharedCache] addDelegate:self];

    // manually call search bar delegate if set in viewWillAppear
    if ([self.searchBar.text length] && [self.searchResults count] == 0) {
        [self searchBar:self.searchBar textDidChange:self.searchBar.text];
    }else if ([self.searchBar.text length] == 0) {
        // start a demo search; just to fire up the caching machinery
        [self.textSearch searchForString:@"###PRECACHE###"];
    }

    // manually enable cancelButton when text is entered manually. (workaround)
    for (UIButton *subView in self.searchBar.subviews) {
        if ([subView isKindOfClass:[UIButton class]]) {
            subView.enabled = YES;
            // manually localize cancel button for people who mis-use the localization dict
            // instead of properly using NSLocale to set the language.
            [subView setTitle:PSPDFLocalize(@"Cancel") forState:UIControlStateNormal];
            break;
        }
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];

    // preserve last search string and document
    _lastSearchString = [self.searchBar.text copy];
    _lastDocumentUID = [[_document UID] copy];

    [[PSPDFCache sharedCache] removeDelegate:self];

    // instantly resign first responder, so we animate keyboard out while alpha animates popover
    [self.searchBar resignFirstResponder];

    if (_clearHighlightsWhenClosed) {
        [_pdfController clearHighlightedSearchResults];
    }
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation {
    return PSIsIpad() ? YES : toInterfaceOrientation != UIInterfaceOrientationPortraitUpsideDown;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public

// relay to searchBar
- (void)setShowsCancelButton:(BOOL)showsCancelButton {
    _showsCancelButton = showsCancelButton;
    _searchBar.showsCancelButton = showsCancelButton;
}

- (void)setSearchText:(NSString *)searchText {
    self.searchBar.text = searchText;
    _lastSearchString = searchText; // set so that we don't reset the search term.
    [self searchBar:self.searchBar textDidChange:searchText];
}

- (NSString *)searchText {
    return self.searchBar.text;
}

- (void)setSearchResults:(NSArray *)searchResults {
    if (searchResults != _searchResults) {
        _searchResults = [searchResults mutableCopy];
    }
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Private

- (PSPDFSearchResult *)searchResultsForIndexPath:(NSIndexPath *)indexPath {
    return (self.searchResults)[indexPath.row];
}

// only used on iPad
- (void)updateContentSize {
    CGFloat totalHeight = [self tableView:self.tableView numberOfRowsInSection:0] * 44.f;

    // performance tweak. contentSizeForViewInPopover is not cheap.
    if (self.contentSizeForViewInPopover.height > 2000 && totalHeight > 2000) return;

    self.contentSizeForViewInPopover = CGSizeMake(300, totalHeight + 44);
}

- (void)setSearchStatus:(PSPDFSearchStatus)searchStatus updateTable:(BOOL)updateTable {
    if (searchStatus != _searchStatus) {
        PSPDFSearchStatus oldStatus = _searchStatus;
        [self willChangeValueForKey:@"searchStatus"];
        _searchStatus = searchStatus;
        [self didChangeValueForKey:@"searchStatus"];

        if (updateTable) {
            [self.tableView beginUpdates];
            if (searchStatus != PSPDFSearchStatusIdle) {
                self.tableView.scrollEnabled = YES;
                if (oldStatus == PSPDFSearchStatusIdle) {
                    [self.tableView insertRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:[_searchResults count] inSection:0]] withRowAnimation:UITableViewRowAnimationFade];
                }else {
                    [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:[_searchResults count] inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
                }
            }else {
                [self.tableView deleteRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:[_searchResults count] inSection:0]] withRowAnimation:UITableViewRowAnimationFade];
                // scroll to start, disable scrolling
                [self.tableView setContentOffset:CGPointMake(0, 0) animated:YES];
                self.tableView.scrollEnabled = NO;
            }
            [self.tableView endUpdates];
        }
    }
}

- (void)setSearchStatus:(PSPDFSearchStatus)searchStatus {
    [self setSearchStatus:searchStatus updateTable:NO];
}

- (void)filterContentForSearchText:(NSString *)searchText scope:(NSString *)scope {
    // clear search
    self.searchResults = nil; // First clear the filtered array.
    [self.tableView reloadData];

    // start searching the html documents
    if ([searchText length] >= kPSPDFMinimumSearchLength) {
        NSArray *visiblePageNumbers = self.searchVisiblePagesFirst ? [_pdfController visiblePageNumbers] : nil;
        [self.textSearch searchForString:searchText inRanges:PSPDFIndexSetFromArray(visiblePageNumbers) rangesOnly:NO];
    }else {
        [self setSearchStatus:PSPDFSearchStatusIdle updateTable:YES];
        [_pdfController clearHighlightedSearchResults];
    }
    [self updateContentSize];
}

static void PSPDFMutableAttributedStringSetFontAtRange(NSMutableAttributedString *aString, UIFont *font, NSRange range) {
	CTFontRef aFont = CTFontCreateWithName((__bridge CFStringRef)font.fontName, font.pointSize, NULL);
	if (!aFont) return;
	[aString removeAttribute:(NSString *)kCTFontAttributeName range:range]; // Work around for Apple leak
	[aString addAttribute:(NSString *)kCTFontAttributeName value:(__bridge id)aFont range:range];
	CFRelease(aFont);
}

- (void)updateResultCell:(UITableViewCell *)cell searchResult:(PSPDFSearchResult *)searchResult {
    PSPDFAttributedLabel *attributedLabel = (PSPDFAttributedLabel *)[cell viewWithTag:kPSPDFAttributedLabelTag];
    if (!attributedLabel) {
        cell.textLabel.font = [UIFont boldSystemFontOfSize:14];
        cell.textLabel.textColor = [UIColor darkGrayColor];
        cell.detailTextLabel.text = @" "; // dummy content for cell to make space

        attributedLabel = [[PSPDFAttributedLabel alloc] init];
        attributedLabel.font = [UIFont systemFontOfSize:14];
        attributedLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        attributedLabel.tag = kPSPDFAttributedLabelTag;
        attributedLabel.backgroundColor = [UIColor clearColor];
        [cell.contentView addSubview:attributedLabel];
    }

    // setup regular cell
    if ([searchResult.previewText length]) {
        NSMutableAttributedString *mas = [[NSMutableAttributedString alloc] initWithString:searchResult.previewText];
        PSPDFMutableAttributedStringSetFontAtRange(mas, [UIFont systemFontOfSize:14], NSMakeRange(0, [searchResult.previewText length]));
        PSPDFMutableAttributedStringSetFontAtRange(mas, [UIFont boldSystemFontOfSize:14], searchResult.rangeInPreviewText);        
        attributedLabel.text = mas;
    }else {
        // don't init with empty string!
        attributedLabel.text = [[NSMutableAttributedString alloc] initWithString:@""];
    }

    // do we have a page title here? only use outline if we have > 1 element (some documents just have page title, not what we want)
    NSString *outlineTitle = searchResult.cachedOutlineTitle;
    if (!outlineTitle && [self.document.outlineParser.outline.children count] > 1) {
        outlineTitle = [self.document.outlineParser outlineElementForPage:searchResult.pageIndex exactPageOnly:NO].title;
        outlineTitle = [outlineTitle substringWithRange:NSMakeRange(0, MIN(25, [outlineTitle length]))]; // limit
        searchResult.cachedOutlineTitle = outlineTitle ?: @""; // use empty string to mark "parsed"
    }

    NSString *pageTitle = [NSString stringWithFormat:PSPDFLocalize(@"Page %d"), searchResult.pageIndex+1];
    NSString *cellText = [outlineTitle length] > 0 ? [NSString stringWithFormat:@"%@, %@", outlineTitle, pageTitle] : pageTitle;
    cell.textLabel.text = cellText;
    cell.imageView.image = [[PSPDFCache sharedCache] cachedImageForDocument:self.document page:searchResult.pageIndex size:PSPDFSizeTiny];

    // wait for imageView resizing (quite a hack, could be replaced with custom cell)
    dispatch_async(dispatch_get_main_queue(), ^{
        CGRect attributedFrame = CGRectMake(cell.textLabel.frame.origin.x, 20.f, cell.frame.size.width - cell.textLabel.frame.origin.x, 20.f);
        attributedLabel.frame = attributedFrame;
    });
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - PSPDFStatusBarStyleHint

- (UIStatusBarStyle)preferredStatusBarStyle {
    switch (self.searchBar.barStyle) {
        case UIBarStyleBlack:
        case UIBarStyleBlackTranslucent: return UIStatusBarStyleBlackOpaque;
        case UIBarStyleDefault:
        default: return UIStatusBarStyleDefault;
    }
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - PSPDFTextSearchDelegate

- (void)willStartSearch:(PSPDFTextSearch *)textSearch forTerm:(NSString *)searchTerm isFullSearch:(BOOL)isFullSearch {
    if (!isFullSearch || ![searchTerm isEqualToString:self.searchBar.text]) {
        return; // ignore partial search updates or different text
    }

    PSPDFLog(@"Starting search for %@.", searchTerm);
    [self setSearchStatus:PSPDFSearchStatusActive updateTable:YES];
    self.searchResults = [NSMutableArray array];
    [self.tableView reloadData];

    // relay
    if ([_pdfController respondsToSelector:@selector(willStartSearch:forTerm:isFullSearch:)]) {
        [_pdfController willStartSearch:textSearch forTerm:searchTerm isFullSearch:isFullSearch];
    }
}

- (void)didUpdateSearch:(PSPDFTextSearch *)textSearch forTerm:(NSString *)searchTerm newSearchResults:(NSArray *)searchResults forPage:(NSUInteger)page {
    PSPDFLogVerbose(@"Updated search status for %@, page %d, results:%@", searchTerm, page, searchResults);

    // ignore if filtered list content is not set (new search is already in progress?)
    if (!_searchResults || ![searchTerm isEqualToString:self.searchBar.text]) {
        PSPDFLogVerbose(@"skipping search update..."); return;
    }

    // TODO: show the state if we limit search results
    if ([_searchResults count] > self.maximumNumberOfSearchResultsDisplayed) {
        PSPDFLogVerbose(@"Skipping further search results."); return;
    }

    if ([searchResults count] > 0) {
        NSMutableArray *indexPaths = [NSMutableArray arrayWithCapacity:[searchResults count]];
        for (PSPDFSearchResult *searchResult in searchResults) {
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[_searchResults count] inSection:0];
            [indexPaths addObject:indexPath];
            [_searchResults addObject:searchResult];
        }
        if ([indexPaths count]) {
            [self.tableView beginUpdates];
            [self.tableView insertRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationFade];
            [self.tableView endUpdates];
            [self updateContentSize];
        }

        // relay
        if ([_pdfController respondsToSelector:@selector(didUpdateSearch:forTerm:newSearchResults:forPage:)]) {
            [_pdfController didUpdateSearch:textSearch forTerm:searchTerm newSearchResults:searchResults forPage:page];
        }
    }
}

- (void)didFinishSearch:(PSPDFTextSearch *)textSearch forTerm:(NSString *)searchTerm searchResults:(NSArray *)searchResults isFullSearch:(BOOL)isFullSearch {
    if (!isFullSearch || ![searchTerm isEqualToString:self.searchBar.text]) {
        return; // ignore partial search updates
    }

    [self setSearchStatus:PSPDFSearchStatusFinished updateTable:YES];

    // update content size.
    [self updateContentSize];

    // relay
    if ([_pdfController respondsToSelector:@selector(didFinishSearch:forTerm:searchResults:isFullSearch:)]) {
        [_pdfController didFinishSearch:textSearch forTerm:searchTerm searchResults:searchResults isFullSearch:isFullSearch];
    }
}

- (void)didCancelSearch:(PSPDFTextSearch *)textSearch forTerm:(NSString *)searchTerm isFullSearch:(BOOL)isFullSearch {
    if (!isFullSearch || ![searchTerm isEqualToString:self.searchBar.text]) {
        return; // ignore partial search updates
    }

    PSPDFLog(@"Search for %@ cancelled.", searchTerm);
    [self setSearchStatus:PSPDFSearchStatusCancelled updateTable:YES];

    // relay
    if ([_pdfController respondsToSelector:@selector(didCancelSearch:forTerm:isFullSearch:)]) {
        [_pdfController didCancelSearch:textSearch forTerm:searchTerm isFullSearch:isFullSearch];
    }
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row >= [self.searchResults count]) return; // user pressed on search status cell

    PSPDFViewController *pdfController = self.pdfController;
    PSPDFSearchResult *searchResult = [self searchResultsForIndexPath:indexPath];

    if (searchResult.selection) {
        [pdfController animateSearchHighlight:searchResult];
    }else {
        if (self.textSearch.searchMode == PSPDFSearchModeHighlighting) {
            // if we don't have selection data, perform a new, limited search.
            // we have to manually find out what page set we're getting (which is pretty chumbersome)
            NSMutableArray *visiblePages = [NSMutableArray arrayWithObject:@(searchResult.pageIndex)];
            if ([pdfController isDoublePageMode]) {
                if ([pdfController isRightPageInDoublePageMode:searchResult.pageIndex] && searchResult.pageIndex > 0) {
                    [visiblePages addObject:@(searchResult.pageIndex-1)];
                }else if (searchResult.pageIndex+1 < [self.document pageCount]) {
                    [visiblePages addObject:@(searchResult.pageIndex+1)];
                }
            }
        }
    }

    // hide controller
    if (PSIsIpad()) {
        [self.searchBar resignFirstResponder];
        [self.pdfController.popoverController dismissPopoverAnimated:YES];
        self.pdfController.popoverController = nil;
    }else {
        [[pdfController masterViewController] dismissViewControllerAnimated:YES completion:NULL];
    }

    [pdfController setPage:searchResult.pageIndex animated:NO];
    pdfController.viewMode = PSPDFViewModeDocument; // ensure that we show documents

    // add highlight results
    [pdfController addHighlightSearchResults:_searchResults];
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSUInteger listCount = [_searchResults count];
    if (listCount || _searchStatus != PSPDFSearchStatusIdle) {
        listCount++; // add one entry for the status cell (activty/search ended)
    }

    return listCount;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *pageCellID = @"PSPDFSearchTableViewCell";
    static NSString *searchCellID = @"PSPDFSearchActivityTableCell";
    BOOL searchCell = (_searchStatus != PSPDFSearchStatusIdle) && indexPath.row == [_searchResults count];
    NSString *cellID = searchCell ? searchCellID : pageCellID;
    UITableViewCell *cell = nil;

	// step 1: is there a reusable cell?
	cell = [tableView dequeueReusableCellWithIdentifier:cellID];

	// step 2: no? -> create new cell
	if (cell == nil) {
        if (searchCell) {
            cell = [[[_pdfController classForClass:[PSPDFSearchStatusCell class]] alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellID];
        }else {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellID];
        }
    }

    if (searchCell) {
        PSPDFSearchStatusCell *searchStatusCell = (PSPDFSearchStatusCell *)cell;
        [searchStatusCell.spinner startAnimating]; // iOS6 needs this
        [searchStatusCell updateCellWithSearchStatus:_searchStatus results:[_searchResults count]];
    }else {
        PSPDFSearchResult *searchResult = _searchResults[indexPath.row];
        [self updateResultCell:cell searchResult:searchResult];
    }

    return cell;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UISearchBarDelegate

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    [self filterContentForSearchText:searchText scope:nil];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    [[self.pdfController masterViewController] dismissViewControllerAnimated:YES completion:NULL]; // showsCancelButton is set for modal view
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [searchBar resignFirstResponder];
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - PSPDFCacheDelegate

- (void)didCachePageForDocument:(PSPDFDocument *)document page:(NSUInteger)page image:(UIImage *)cachedImage size:(PSPDFSize)size {
    if (size != PSPDFSizeTiny) return;

    NSMutableArray *indexPaths = [NSMutableArray array];
    for (PSPDFSearchResult *searchResult in _searchResults) {
        if (searchResult.pageIndex == page && searchResult.document == document) {
            NSUInteger searchIndex = [_searchResults indexOfObject:searchResult];
            [indexPaths addObject:[NSIndexPath indexPathForRow:searchIndex inSection:0]];
        }
    }
    if ([indexPaths count]) {
        [self.tableView reloadRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationNone];
    }
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - PSPDFPopoverControllerDismissable

- (void)willDismissPopover:(UIPopoverController *)popover animated:(BOOL)animated {
    // lock current size (so that it doesn't change while animating out keyboard
    popover.contentViewController.contentSizeForViewInPopover = popover.contentViewController.view.bounds.size;
    // instantly resign keyboard, don't wait for viewWillDisappear which is sent AFTER complete popover animation
    [self.searchBar resignFirstResponder];
}

@end

__attribute__ ((deprecated)) const NSUInteger PSPDFSearchIdle = PSPDFSearchStatusIdle;
__attribute__ ((deprecated)) const NSUInteger PSPDFSearchActive = PSPDFSearchStatusActive;
__attribute__ ((deprecated)) const NSUInteger PSPDFSearchFinished = PSPDFSearchStatusFinished;
__attribute__ ((deprecated)) const NSUInteger PSPDFSearchCancelled = PSPDFSearchStatusCancelled;
