//
//  PSPDFBookmarkViewController.m
//  PSPDFKit
//
//  Copyright (c) 2012 Peter Steinberger. All rights reserved.
//

#import "PSPDFBookmarkViewController.h"
#import "PSPDFDocument.h"
#import "PSPDFBookmarkParser.h"
#import "PSPDFBookmark.h"

@implementation PSPDFBookmarkViewController

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (instancetype)initWithDocument:(PSPDFDocument *)document {
    if ((self = [super initWithNibName:nil bundle:nil])) {
        _document = document;
        self.title = PSPDFLocalize(@"Bookmarks");
        [self updatePopoverSize];
    }
    return self;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UIViewController

// Deprecated in iOS6, but new default is the same as this.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation {
    return PSIsIpad() ? YES : toInterfaceOrientation != UIInterfaceOrientationPortraitUpsideDown;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self createBarButtonItems];
    [self updateView];

    // for some reason, changes to contentSizeForViewInPopover aren't recognized while we're within the appearance-callbacks. (as of iOS5)
    dispatch_async(dispatch_get_main_queue(), ^{ [self updatePopoverSize]; });
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public

- (void)setDocument:(PSPDFDocument *)document {
    if (document != _document) {
        _document = document;
        [self.tableView reloadData];
    }
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Private

- (void)createBarButtonItems {
    if (self.isInPopover) {
        self.navigationItem.leftBarButtonItem = self.editButtonItem;
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(addBookmarkAction:)];
    }else {
        self.navigationItem.leftBarButtonItems = @[[[UIBarButtonItem alloc] initWithTitle:PSPDFLocalize(@"Close") style:UIBarButtonItemStyleDone target:self action:@selector(doneAction:)]];
        self.navigationItem.rightBarButtonItems = @[[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(addBookmarkAction:)], self.editButtonItem];
    }
}

- (void)updatePopoverSize {
    CGFloat totalHeight = 44 * [self.document.bookmarkParser.bookmarks count];
    self.contentSizeForViewInPopover = CGSizeMake(self.contentSizeForViewInPopover.width, totalHeight);
}

- (void)updateView {
    NSUInteger currentPage = [self.delegate currentPageForBookmarkViewController:self];
    self.navigationItem.rightBarButtonItem.enabled = [self.document.bookmarkParser bookmarkForPage:currentPage] == nil;
}

- (void)addBookmarkAction:(id)sender {
    NSUInteger currentPage = [self.delegate currentPageForBookmarkViewController:self];
    [self.tableView beginUpdates];
    [self.document.bookmarkParser addBookmarkForPage:currentPage];
    [self.tableView insertRowsAtIndexPaths:@[[NSIndexPath indexPathForItem:[self.document.bookmarkParser.bookmarks count]-1 inSection:0]] withRowAnimation:UITableViewRowAnimationAutomatic];
    [self.tableView endUpdates];
    [self updatePopoverSize];
    [self updateView];
}

- (void)doneAction:(id)sender {
    [self dismissViewControllerAnimated:YES completion:NULL];
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.document.bookmarkParser.bookmarks count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"PSPDFBookmarkCell";
    
    PSPDFBookmarkTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[PSPDFBookmarkTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    cell.bookmark = self.document.bookmarkParser.bookmarks[indexPath.row];
    return cell;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [self.tableView beginUpdates];
        PSPDFBookmark *bookmark = self.document.bookmarkParser.bookmarks[indexPath.row];
        if ([self.document.bookmarkParser removeBookmarkForPage:bookmark.page]) {
            [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
        }
        [self.tableView endUpdates];
        [self updatePopoverSize];
        [self updateView];
    }
}

- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)sourceIndexPath toIndexPath:(NSIndexPath *)destinationIndexPath {
    NSMutableArray *bookmarks = [self.document.bookmarkParser.bookmarks mutableCopy];
    PSPDFBookmark *bookmark = bookmarks[sourceIndexPath.row];
    [bookmarks removeObjectAtIndex:sourceIndexPath.row];
    [bookmarks insertObject:bookmark atIndex:destinationIndexPath.row];
    self.document.bookmarkParser.bookmarks = bookmarks;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    PSPDFBookmark *bookmark = self.document.bookmarkParser.bookmarks[indexPath.row];
    [self.delegate bookmarkViewController:self didSelectBookmark:bookmark];
}

@end

@implementation PSPDFBookmarkTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if ((self = [super initWithStyle:style reuseIdentifier:reuseIdentifier])) {
        _textField = [[UITextField alloc] initWithFrame:CGRectZero];
        _textField.returnKeyType = UIReturnKeyDone;
        _textField.textColor = self.textLabel.textColor;
        _textField.delegate = self;
        [[self contentView] addSubview:_textField];
    }
    return self;
}

- (void)updateTextFieldFrame {
    self.textField.frame = UIEdgeInsetsInsetRect(self.contentView.bounds, UIEdgeInsetsMake(10, 10, 10, 10));
}

- (void)updateLabels {
    self.textLabel.text = [self.bookmark pageOrNameString];
    self.textField.text = [self.bookmark pageOrNameString];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    if ([self isEditing]) {
        self.textField.font = self.textLabel.font;
        [self updateTextFieldFrame];
    }
    self.textField.hidden = ![self isEditing];
    self.textLabel.hidden = [self isEditing];
}

- (void)setBookmark:(PSPDFBookmark *)bookmark {
    if (bookmark != _bookmark) {
        _bookmark = bookmark;
        [self updateLabels];
        [self setNeedsLayout];
    }
}

- (void)setEditing:(BOOL)editing animated:(BOOL)animated {
    [super setEditing:editing animated:animated];
    [self updateTextFieldFrame]; // needs to be here + layoutSubviews for a nice animation.
    if (!editing) [_textField resignFirstResponder];
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    return [self isEditing];
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    self.bookmark.name = textField.text;
    [self updateLabels];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self.textField resignFirstResponder];
    return YES;
}

@end
