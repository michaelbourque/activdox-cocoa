//
//  PSPDFTabbedViewController.m
//  PSPDFKit
//
//  Copyright (c) 2012 Peter Steinberger. All rights reserved.
//

#import "PSPDFTabbedViewController.h"
#import "PSPDFViewController.h"
#import "PSPDFTabBarView.h"
#import "PSPDFHUDView.h"

@interface PSPDFTabbedViewController () <PSPDFTabBarViewDelegate, PSPDFTabBarViewDataSource> {
    PSPDFTabBarView *_tabBar;
    NSMutableDictionary *_viewStateDictionary;
}
@property (nonatomic, strong) PSPDFTabBarView *tabBar;
@end

@implementation PSPDFTabbedViewController

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (void)commonInitWithPDFController:(PSPDFViewController *)pdfController {
    _minTabWidth = 100.f;
    _statePersistanceKey = @"kPSPDFTabbedDocumentsPersistKey";
    _enableAutomaticStatePersistance = NO;
    _viewStateDictionary = [NSMutableDictionary new];
    _pdfController = pdfController ?: [[PSPDFViewController alloc] initWithDocument:nil];
    _pdfController.useParentNavigationBar = YES;
    _pdfController.documentLabelEnabled = NO; // would overlap the tabbar
}

- (id)init { return [self initWithPDFViewController:nil]; }
- (id)initWithPDFViewController:(PSPDFViewController *)pdfController {
    if ((self = [super initWithNibName:nil bundle:nil])) {
        [self commonInitWithPDFController:pdfController];
    }
    return self;
}

// Interface Builder/Storyboarding. (calling super:initWithCoder is important!)
- (id)initWithCoder:(NSCoder *)decoder {
    if ((self = [super initWithCoder:decoder])) {
        [self commonInitWithPDFController:nil];
    }
    return self;
}

// this class is not designed to be used with a nib file.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    return [self initWithPDFViewController:nil];
}

- (void)dealloc {
    if (self.enableAutomaticStatePersistance) {
        [self persistState];
    }
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UIViewController

- (void)updateTabBarFrame {
    // increased high to make touches easier.
    CGFloat navigationBarHeight = [self.pdfController contentRect].origin.y;
    // 64 = hard coded increased touch target
    self.tabBar.frame = CGRectMake(0.f, navigationBarHeight, self.view.bounds.size.width, 64.f);
}

- (void)viewDidLoad {
    [super viewDidLoad];
    PSPDFViewController *pdfController = self.pdfController;
    
    [self addChildViewController:pdfController];
    [pdfController didMoveToParentViewController:self];
    pdfController.view.frame = self.view.bounds;
    pdfController.view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [self.view addSubview:pdfController.view];
    self.wantsFullScreenLayout = pdfController.wantsFullScreenLayout;
    
    // remove document item set from PSPDFViewController.
    self.navigationItem.leftBarButtonItem = nil;
    
    // add custom tabbar
    self.tabBar = [[PSPDFTabBarView alloc] initWithFrame:CGRectZero];
    _tabBar.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    _tabBar.delegate = self;
    _tabBar.dataSource = self;
    _tabBar.minTabWidth = self.minTabWidth;
    [self updateTabBarFrame];
    [pdfController.HUDView addSubview:_tabBar];
    [_tabBar reloadData];
    [self selectCurrentDocumentInTabBarAnimated:NO scrollToPosition:YES];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

    // navigationBar will only be changed in the PSPDFViewController's viewWillAppear.
    dispatch_async(dispatch_get_main_queue(), ^{
        [self updateTabBarFrame];
    });
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];

    // pre-iOS6 only
    if (![self isViewLoaded]) {
        [_pdfController willMoveToParentViewController:nil];
        [_pdfController removeFromParentViewController];
        self.tabBar.delegate = nil;
        self.tabBar.dataSource = nil;
        self.tabBar = nil;
    }
}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    [self updateTabBarFrame];
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    [super willAnimateRotationToInterfaceOrientation:toInterfaceOrientation duration:duration];
    [self updateTabBarFrame];
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public

- (void)addDocuments:(NSArray *)documents atIndex:(NSUInteger)index animated:(BOOL)animated {
    NSMutableArray *newDocuments = [self.documents mutableCopy];
    index = MIN(index, [newDocuments count]);

    NSArray *documentsToInsert = [documents filteredArrayUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(id evaluatedObject, NSDictionary *bindings) {
        return ![newDocuments containsObject:evaluatedObject];
    }]];

    NSIndexSet *indexSet = [NSIndexSet indexSetWithIndexesInRange:NSMakeRange(index, [documentsToInsert count])];
    [newDocuments insertObjects:documentsToInsert atIndexes:indexSet];
    [self setDocuments:newDocuments animated:animated];
}

- (void)removeDocuments:(NSArray *)documents animated:(BOOL)animated {
    NSMutableArray *newDocuments = [self.documents mutableCopy];
    for (PSPDFDocument *document in documents) {
        if ([newDocuments containsObject:document]) {
            [newDocuments removeObject:document];
        }else {
            PSPDFLogWarning(@"Ignoring document %@, not in self.documents array.", document);
        }
    }
    [self setDocuments:newDocuments animated:animated];
}

- (void)selectCurrentDocumentInTabBarAnimated:(BOOL)animated scrollToPosition:(BOOL)scrollToPosition {
    NSUInteger activeIndex = [self.documents indexOfObject:self.visibleDocument];
    if (activeIndex != NSNotFound) {
        [_tabBar selectTabAtIndex:activeIndex animated:animated];
        if (scrollToPosition) {
            [_tabBar scrollToTabAtIndex:activeIndex animated:animated];
        }
    }
}

- (void)setEnableAutomaticStatePersistance:(BOOL)enableAutomaticStatePersistance {
    if (enableAutomaticStatePersistance != _enableAutomaticStatePersistance) {
        _enableAutomaticStatePersistance = enableAutomaticStatePersistance;
        if (enableAutomaticStatePersistance) {
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(persistState) name:UIApplicationDidEnterBackgroundNotification object:nil];
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(persistState) name:UIApplicationWillTerminateNotification object:nil];
        }else {
            [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidEnterBackgroundNotification object:nil];
            [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationWillTerminateNotification object:nil];
        }
    }
}

- (void)persistState {
    PSPDFLogVerbose(@"persisting state.");
    NSMutableDictionary *persistDict = [NSMutableDictionary dictionaryWithCapacity:2];
    if (self.documents) {
        persistDict[NSStringFromSelector(@selector(documents))] = self.documents;
    }
    if (self.visibleDocument) {
        persistDict[NSStringFromSelector(@selector(visibleDocument))] = self.visibleDocument;
    }
    [self persistViewStateForCurrentVisibleDocument];
    persistDict[@"viewStateDictionary"] = _viewStateDictionary;
    NSData *archive = [NSKeyedArchiver archivedDataWithRootObject:persistDict];
    [[NSUserDefaults standardUserDefaults] setObject:archive forKey:self.statePersistanceKey];
}

- (BOOL)restoreState {
    return [self restoreStateAndMergeWithDocuments:nil];
}

- (BOOL)restoreStateAndMergeWithDocuments:(NSArray *)documents {
    // load documents fron user defaults
    NSData *archiveData = [[NSUserDefaults standardUserDefaults] objectForKey:self.statePersistanceKey];
    NSDictionary *persistDict = nil;
    if (archiveData) {
        PSPDFLogVerbose(@"restoring state.");
        @try {
            // This method raises an NSInvalidArchiveOperationException if data is not a valid archive.
            persistDict = [NSKeyedUnarchiver unarchiveObjectWithData:archiveData];
        }
        @catch (NSException *exception) {
            PSPDFLogError(@"Failed to restore persist state: %@", exception);
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:self.statePersistanceKey];
        }
    }

    NSArray *newDocuments = nil;
    PSPDFDocument *newVisibleDocument = nil;

    // set new properties
    if (persistDict) {
        newDocuments = persistDict[NSStringFromSelector(@selector(documents))];
        newVisibleDocument = persistDict[NSStringFromSelector(@selector(visibleDocument))];
        _viewStateDictionary = persistDict[@"viewStateDictionary"] ?: _viewStateDictionary;
        [self restoreViewStateForCurrentVisibleDocument];
    }

    // merge documents with current state
    // ensure that any document that is given in the array will override the loaded ones
    // (so we don't destroy subclasses of PSPDFDocument)
    NSMutableArray *mergedDocuments = [documents mutableCopy] ?: [NSMutableArray array];
    for (PSPDFDocument *document in newDocuments) {
        if (![documents containsObject:document]) {
            [mergedDocuments addObject:document];
        }
    }
    self.documents = mergedDocuments;
    if ([documents count] > 0) {
        newVisibleDocument = documents[0];
    }
    self.visibleDocument = newVisibleDocument;

    return persistDict != nil;
}

- (void)setDocuments:(NSArray *)documents {
    [self setDocuments:documents animated:NO];
}

- (void)setDocuments:(NSArray *)documents animated:(BOOL)animated {
    if (documents != _documents) {
        
        BOOL allowChange = YES;
        if ([_delegate respondsToSelector:@selector(tabbedPDFController:shouldChangeDocuments:)]) {
            allowChange = [_delegate tabbedPDFController:self shouldChangeDocuments:documents];
        }
        
        if (allowChange) {
            NSArray *oldDocuments = _documents;
            _documents = documents;
            [_tabBar reloadData];
            [self selectCurrentDocumentInTabBarAnimated:animated scrollToPosition:YES];

            // replace visibleDocument?
            if ((self.visibleDocument && [documents indexOfObject:self.visibleDocument] == NSNotFound) || !self.visibleDocument) {
                if ([documents count]) {
                    self.visibleDocument = documents[0];
                    [_tabBar selectTabAtIndex:0 animated:NO];
                }else {
                    self.visibleDocument = nil;
                }
            }
            
            // remove view cache data
            NSMutableSet *oldDocumentsSet = [NSMutableSet setWithArray:oldDocuments];
            [oldDocumentsSet minusSet:[NSSet setWithArray:documents]];
            [_viewStateDictionary removeObjectsForKeys:[[oldDocumentsSet valueForKeyPath:@"self.UID"] allObjects]];
            
            if ([_delegate respondsToSelector:@selector(tabbedPDFController:didChangeDocuments:)]) {
                [_delegate tabbedPDFController:self didChangeDocuments:oldDocuments];
            }
        }
    }
}

- (PSPDFDocument *)visibleDocument {
    return _pdfController.document;
}

- (void)restoreViewStateForCurrentVisibleDocument {
    PSPDFViewState *viewState = _viewStateDictionary[self.visibleDocument.UID];
    if (viewState) {
        [_pdfController setViewState:viewState animated:NO];
    }
}

- (void)persistViewStateForCurrentVisibleDocument {
    if (_pdfController.document.UID) {
        PSPDFViewState *viewState = _pdfController.viewState;
        _viewStateDictionary[_pdfController.document.UID] = viewState;
    }
}

- (void)setVisibleDocument:(PSPDFDocument *)visibleDocument {
    [self setVisibleDocument:visibleDocument animated:NO scrollToPosition:YES];
}

- (void)setVisibleDocument:(PSPDFDocument *)visibleDocument animated:(BOOL)animated scrollToPosition:(BOOL)scrollToPosition {
    if (_pdfController.document != visibleDocument) {
        [self persistViewStateForCurrentVisibleDocument];
        
        _pdfController.document = visibleDocument;
        [self restoreViewStateForCurrentVisibleDocument];

        [self selectCurrentDocumentInTabBarAnimated:animated scrollToPosition:scrollToPosition];
    }
}

- (void)setMinTabWidth:(CGFloat)minTabWidth {
    if (minTabWidth != _minTabWidth) {
        _minTabWidth = minTabWidth;
        self.tabBar.minTabWidth = minTabWidth;
        [self.tabBar reloadData];
    }
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - PSPDFTabBarViewDelegate

- (void)tabBarView:(PSPDFTabBarView *)tabBarView didSelectTabAtIndex:(NSUInteger)index {
    PSPDFDocument *newDocument = (self.documents)[index];
    BOOL allowChange = YES;

    if ([_delegate respondsToSelector:@selector(tabbedPDFController:shouldChangeVisibleDocument:)]) {
        allowChange = [_delegate tabbedPDFController:self shouldChangeVisibleDocument:newDocument];
    }
    
    // only set if allowed
    if (allowChange) {
        PSPDFDocument *oldDocument = self.visibleDocument;
        [self setVisibleDocument:newDocument animated:YES scrollToPosition:NO];
        
        if ([_delegate respondsToSelector:@selector(tabbedPDFController:didChangeVisibleDocument:)]) {
            [_delegate tabbedPDFController:self didChangeVisibleDocument:oldDocument];
        }
    }
}

- (void)tabBarView:(PSPDFTabBarView *)tabBarView didSelectCloseButtonOfTabAtIndex:(NSUInteger)index {
    NSMutableArray *newDocuments = [self.documents mutableCopy];
    PSPDFDocument *closedDocument = self.documents[index];
    [newDocuments removeObjectAtIndex:index];
    
    BOOL allowChange = YES;
    if ([_delegate respondsToSelector:@selector(tabbedPDFController:shouldChangeDocuments:)]) {
        allowChange = [_delegate tabbedPDFController:self shouldChangeDocuments:newDocuments];
    }

    if (allowChange) {
        if ([_delegate respondsToSelector:@selector(tabbedPDFController:shouldCloseDocument:)]) {
            allowChange = [_delegate tabbedPDFController:self shouldCloseDocument:closedDocument];
        }
    }

    // only set if allowed
    if (allowChange) {
        NSArray *oldDocumentsSet = self.documents;
        self.documents = newDocuments;
        
        if ([_delegate respondsToSelector:@selector(tabbedPDFController:didChangeDocuments:)]) {
            [_delegate tabbedPDFController:self didChangeDocuments:oldDocumentsSet];
        }

        // also call delegate that notifies what document has been closed.
        if ([_delegate respondsToSelector:@selector(tabbedPDFController:didCloseDocument:)]) {
            [_delegate tabbedPDFController:self didCloseDocument:closedDocument];
        }
    }
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - PSPDFTabBarViewDataSource

- (NSInteger)numberOfTabsInTabBarView:(PSPDFTabBarView *)tabBarView {
    return [self.documents count];
}

- (NSString *)tabBarView:(PSPDFTabBarView *)tabBarView titleForTabAtIndex:(NSUInteger)index {
    PSPDFDocument *document = (self.documents)[index];
    // ensure we have something to show for the tab
    if (document.isTitleLoaded && [[document.title stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0) {
        return document.title;
    }else {
        // if we would use title here, we might get some delays while the system parses the whole PDF document to get the metadata.
        return PSPDFStripPDFFileType([document.fileURL lastPathComponent]);
    }
}

@end
