//
//  PSPDFGlyph.m
//  PSPDFKit
//
//  Copyright 2012 Peter Steinberger. All rights reserved.
//

#import "PSPDFGlyph.h"
#import "PSPDFFontInfo.h"

@implementation PSPDFGlyph

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (NSUInteger)hash {
    return ((int)_frame.origin.x ^ (int)_frame.origin.y ^ (int)_frame.size.width ^ (int)_frame.size.height) + [self.content hash];
}

- (BOOL)isEqual:(id)other {
    if ([other isKindOfClass:[self class]]) {
        return [self isEqualToGlyph:(PSPDFGlyph *)other];
    }
    return NO;
}

- (BOOL)isEqualToGlyph:(PSPDFGlyph *)otherGlyph {
    if (CGRectEqualToRect(self.frame, otherGlyph.frame) && [self.content isEqual:otherGlyph.content]) {
        return YES;
    }
    return NO;
}

- (NSString *)description {
    NSString *lineBreakerStr = _lineBreaker ? @" LineBreak" : @"";
	return [NSString stringWithFormat:@"<%@ %@ (%@)%@>", NSStringFromClass([self class]), NSStringFromCGRect(_frame), _content, lineBreakerStr];
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public

- (NSComparisonResult)compareByLayout:(PSPDFGlyph *)glyph {
	if ([self isOnSameLineAs:glyph]) {
		return (glyph.frame.origin.x + glyph.frame.size.width < _frame.origin.x + _frame.size.width) ? NSOrderedDescending : NSOrderedAscending;
	} else {
		return (glyph.frame.origin.y + glyph.frame.size.height < _frame.origin.y + _frame.size.height) ? NSOrderedAscending : NSOrderedDescending;
	}
}

- (NSComparisonResult)compareByXPosition:(PSPDFGlyph *)glyph {
	if (CGRectGetMinX(glyph.frame) < CGRectGetMinX(_frame)) {
		return NSOrderedDescending;
	} else if (CGRectGetMinX(glyph.frame) > CGRectGetMinX(_frame)) {
		return NSOrderedAscending;
	}
	return NSOrderedSame;
}

- (BOOL)isOnSameLineAs:(PSPDFGlyph *)glyph {
    CGRect unionRect = CGRectUnion(glyph.frame, _frame);
    CGRect smallerFrame = _frame.size.height < glyph.frame.size.height ? _frame : glyph.frame;
    BOOL sameLine = (fabsf(unionRect.size.height) < fabsf(smallerFrame.size.height * 1.5));
    return sameLine;
}

// calculate fontHeight based on the actual height minus the default font height.
- (CGFloat)fontHeight {
    CGFloat height = _font.ascent / 1000.0 - _font.descent / 1000.0;
    CGFloat fontHeight = _frame.size.height/height;
    return fontHeight;
}

- (BOOL)isOnSameLineSegmentAs:(PSPDFGlyph *)glyph {
    CGFloat unionHeight = CGRectUnion(glyph.frame, _frame).size.height;
    CGFloat overlapHeight = unionHeight - fabsf(CGRectGetMinY(_frame) - CGRectGetMinY(glyph.frame)) - fabsf(CGRectGetMaxY(_frame) - CGRectGetMaxY(glyph.frame));

    CGFloat minHeight = fminf(CGRectGetHeight(_frame), CGRectGetHeight(glyph.frame));
    CGFloat miniumVerticalWordOverlapFactor = 0.4;

    BOOL overlap = overlapHeight > miniumVerticalWordOverlapFactor * minHeight;

    CGFloat fontSizeDifferenceFactor = 0.4;
    BOOL similarFont = fabs(self.fontHeight - glyph.fontHeight) < fontSizeDifferenceFactor;

    CGFloat maximumHorizontalMergeSpaceBetweenWordsFactor = 0.6;
    CGFloat dij = fmaxf(CGRectGetMinX(_frame), CGRectGetMinX(glyph.frame)) - fminf(CGRectGetMaxX(_frame), CGRectGetMaxX(glyph.frame));
    CGFloat minWidth = fminf(CGRectGetWidth(_frame), CGRectGetWidth(glyph.frame));
    BOOL space = dij < maximumHorizontalMergeSpaceBetweenWordsFactor * minWidth;

    BOOL sameLine = overlap && similarFont && space;
    return sameLine;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Static

NSArray *PSPDFRectsFromGlyphs(NSArray *glyphs, CGAffineTransform t, CGRect *boundingBox) {
    NSMutableArray *rects = [NSMutableArray array];
    PSPDFGlyph *prevGlyph = nil;
    CGRect currentRect = CGRectNull;
    float minX = MAXFLOAT;
    float maxX = -MAXFLOAT;
    float minY = MAXFLOAT;
    float maxY = -MAXFLOAT;
    for (PSPDFGlyph *glyph in glyphs) {
        if (CGRectGetMinX(glyph.frame) < minX) minX = CGRectGetMinX(glyph.frame);
        if (CGRectGetMaxX(glyph.frame) > maxX) maxX = CGRectGetMaxX(glyph.frame);
        if (CGRectGetMinY(glyph.frame) < minY) minY = CGRectGetMinY(glyph.frame);
        if (CGRectGetMaxY(glyph.frame) > maxY) maxY = CGRectGetMaxY(glyph.frame);

        if (prevGlyph) {
            if ([glyph isOnSameLineAs:prevGlyph]) {
                currentRect = CGRectUnion(currentRect, glyph.frame);
            } else {
                currentRect = CGRectApplyAffineTransform(currentRect, t);
                [rects addObject:[NSValue valueWithCGRect:currentRect]];
                currentRect = glyph.frame;
            }
        } else {
            currentRect = glyph.frame;
        }
        prevGlyph = glyph;
    }
    currentRect = CGRectApplyAffineTransform(currentRect, t);
    [rects addObject:[NSValue valueWithCGRect:currentRect]];

    *boundingBox = CGRectMake(minX, minY, maxX - minX, maxY - minY);
    *boundingBox = CGRectApplyAffineTransform(*boundingBox, t);

    return [rects copy];
}

CGRect PSPDFBoundingBoxFromGlyphs(NSArray *glyphs, CGAffineTransform t) {
    float minX = MAXFLOAT;
    float maxX = -MAXFLOAT;
    float minY = MAXFLOAT;
    float maxY = -MAXFLOAT;
    for (PSPDFGlyph *glyph in glyphs) {
        if (CGRectGetMinX(glyph.frame) < minX) minX = CGRectGetMinX(glyph.frame);
        if (CGRectGetMaxX(glyph.frame) > maxX) maxX = CGRectGetMaxX(glyph.frame);
        if (CGRectGetMinY(glyph.frame) < minY) minY = CGRectGetMinY(glyph.frame);
        if (CGRectGetMaxY(glyph.frame) > maxY) maxY = CGRectGetMaxY(glyph.frame);
    }
    return CGRectApplyAffineTransform(CGRectMake(minX, minY, maxX - minX, maxY - minY), t);
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSCopying

- (id)copyWithZone:(NSZone *)zone {
    PSPDFGlyph *glyph = [[[self class] alloc] init];
    glyph.font = self.font;
    glyph.lineBreaker = self.lineBreaker;
    glyph.indexOnPage = self.indexOnPage;
    glyph.frame = self.frame;
    glyph.content = self.content;
    return glyph;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSCoding

- (id)initWithCoder:(NSCoder *)decoder {
	if((self = [super init])) {
        _font = [decoder decodeObjectForKey:NSStringFromSelector(@selector(font))];
        _lineBreaker = [decoder decodeBoolForKey:NSStringFromSelector(@selector(lineBreaker))];
        _indexOnPage = [decoder decodeIntegerForKey:NSStringFromSelector(@selector(indexOnPage))];
        _frame = [decoder decodeCGRectForKey:NSStringFromSelector(@selector(frame))];
        _content = [decoder decodeObjectForKey:NSStringFromSelector(@selector(content))];
    }
	return self;
}

- (void)encodeWithCoder:(NSCoder *)coder {
    [coder encodeObject:_font forKey:NSStringFromSelector(@selector(font))];
    [coder encodeBool:_lineBreaker forKey:NSStringFromSelector(@selector(lineBreaker))];
    [coder encodeInteger:_indexOnPage forKey:NSStringFromSelector(@selector(indexOnPage))];
    [coder encodeCGRect:_frame forKey:NSStringFromSelector(@selector(frame))];
    [coder encodeObject:_content forKey:NSStringFromSelector(@selector(content))];
}

@end
