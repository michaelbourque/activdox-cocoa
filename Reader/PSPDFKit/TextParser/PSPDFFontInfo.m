//
//  PSPDFFontInfo.m
//  PSPDFKit
//
//  Copyright 2012 Peter Steinberger. All rights reserved.
//

#import "PSPDFFontInfo.h"
#import "PSPDFTextParser.h"
#import "PSPDFConverter.h"
#import <CoreText/CoreText.h>

@interface PSPDFFontInfo ()
@property (nonatomic, copy) NSString *name;
@property (nonatomic, assign) CGFloat ascent;
@property (nonatomic, assign) CGFloat descent;
@property (nonatomic, copy) NSArray *encodingArray;
@property (nonatomic, copy) NSDictionary *toUnicodeMap;
@end

@interface PDFSimpleFontInfo : PSPDFFontInfo {
	CGFloat widths[256];
}
+ (NSArray *)baseArrayForEncoding:(CFStringEncoding)stringEncoding;
@end

@interface PDFCompositeFontInfo : PSPDFFontInfo {
	CGFloat widths[65536];
}
@end

@implementation PSPDFFontInfo

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (id)initWithFontDictionary:(CGPDFDictionaryRef)font {
    //PSPDFLogVerbose(@"font:%@", PSPDFConvertPDFDictionary(font));
    NSString *fontType = PSPDFDictionaryGetString(font, @"Subtype");
    NSString *name = PSPDFDictionaryGetString(font, @"BaseFont");

    // TODO: add full support for MMType1 fonts.
	if ([fontType isEqual:@"Type1"] || [fontType isEqual:@"MMType1"] || [fontType isEqual:@"TrueType"] || [fontType isEqual:@"Type3"]) {
		self = [[PDFSimpleFontInfo alloc] initWithFontDictionary:font];
	}
	else if ([fontType isEqual:@"Type0"]) {
		self = [[PDFCompositeFontInfo alloc] initWithFontDictionary:font];
	}
	else {
        PSPDFLogWarning(@"Font type not implemented: %@", fontType);
		self = nil;
		return self;
	}
    self.name = name;

	return self;
}

- (NSString *)description {
    NSMutableString *description = [NSMutableString string];
    [description appendFormat:@"<%@ name:%@ ascend:%.1f descent:%.1f", NSStringFromClass([self class]), self.name, self.ascent, self.descent];

    if ([self.encodingArray count]) {
        [description appendFormat:@", encodingArray:#%d", [self.encodingArray count]];
    }
    if ([self.toUnicodeMap count]) {
        [description appendFormat:@", toUnicodeMap:#%d", [self.toUnicodeMap count]];
    }
    [description appendString:@">"];
    return [description copy];
}

- (NSUInteger)hash {
    return [self.name hash] * 31 + self.ascent + self.descent + [self.encodingArray hash] + [self.toUnicodeMap hash];
}

- (BOOL)isEqual:(id)other {
    if ([other isKindOfClass:[self class]]) {
        return [self isEqualToFontInfo:(PSPDFFontInfo *)other];
    }
    return NO;
}

- (BOOL)isEqualToFontInfo:(PSPDFFontInfo *)otherFontInfo {
    if ([self.name isEqual:otherFontInfo.name] && (self.encodingArray == otherFontInfo.encodingArray || [self.encodingArray isEqual:otherFontInfo.encodingArray]) && (self.toUnicodeMap == otherFontInfo.toUnicodeMap || [self.toUnicodeMap isEqual:otherFontInfo.toUnicodeMap])) {
        return YES;
    }
    return NO;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public

- (CGFloat)widthForCharacter:(uint16_t)character {
	return 0.0;
}

- (BOOL)isMultiByteFont {
	return NO;
}

// predefined widths for default fonts
+ (NSDictionary *)standardFontWidths {
	static NSDictionary *sharedStandardFontWidths = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
		sharedStandardFontWidths = [[NSDictionary alloc] initWithContentsOfFile:[PSPDFKitBundle() pathForResource:@"StandardFonts" ofType:@"plist"]];
    });
	return sharedStandardFontWidths;
}

// dict to convert glyph names into the actual characters
+ (NSDictionary *)glyphNames {
	static NSDictionary *sharedGlyphNames = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
		sharedGlyphNames = [[NSDictionary alloc] initWithContentsOfFile:[PSPDFKitBundle() pathForResource:@"glyphs" ofType:@"plist"]];
    });
	return sharedGlyphNames;
}

// more generalized tokenize parser (not all CMaps have whitespace between the <xxx> token.
- (NSArray *)tokensFromMappingString:(NSString *)mapping {
    NSMutableArray *tokens = [NSMutableArray array];
    NSScanner *mappingTokenScanner = [NSScanner scannerWithString:mapping];
    while (![mappingTokenScanner isAtEnd]) {
        NSString *singleToken;
        [mappingTokenScanner scanUpToString:@"<" intoString:NULL];
        [mappingTokenScanner scanUpToString:@">" intoString:&singleToken];
        if ([singleToken length]) {
            [tokens addObject:singleToken];
        }
    }
    return tokens;
}

- (void)parseToUnicodeMapWithString:(NSString *)cmapString {
	NSMutableDictionary *toUnicodeMap = [NSMutableDictionary dictionary];

	NSCharacterSet *bracketCharacterSet = [NSCharacterSet characterSetWithCharactersInString:@"<>"];

	NSMutableArray *bfCharMappings = [NSMutableArray array];
	NSScanner *bfcharScanner = [NSScanner scannerWithString:cmapString];
	while (![bfcharScanner isAtEnd]) {
		[bfcharScanner scanUpToString:@"beginbfchar" intoString:NULL];
		[bfcharScanner scanString:@"beginbfchar" intoString:NULL];
		NSString *bfcharMapping = nil;
		[bfcharScanner scanUpToString:@"endbfchar" intoString:&bfcharMapping];
		if (bfcharMapping) {
			[bfCharMappings addObject:bfcharMapping];
		}
	}

	for (NSString *mapping in bfCharMappings) {
        NSArray *tokens = [self tokensFromMappingString:mapping];

		for (int i=0; i<[tokens count]; i+=2) {
			if (i+1 >= [tokens count]) break;
			NSString *fromNumber = tokens[i];
			NSString *toString = tokens[i+1];
			fromNumber = [fromNumber stringByTrimmingCharactersInSet:bracketCharacterSet];
			toString = [toString stringByTrimmingCharactersInSet:bracketCharacterSet];
			unsigned int fromValue = 0;
			NSScanner *hexScanner = [NSScanner scannerWithString:fromNumber];
			[hexScanner scanHexInt:&fromValue];

            unsigned int toValue = 0;
            int charLen = 4;

            // are we handling a ligature?
            if ([toString length] > charLen) {
                NSMutableString *characters = [NSMutableString string];
                NSRange range;
                NSString *nextToString;
                for (int offset = 0; offset < [toString length]/4; offset++) {
                    range = NSMakeRange(offset * charLen, charLen);
                    nextToString = [toString substringWithRange:range];
                    hexScanner = [NSScanner scannerWithString:nextToString];
                    [hexScanner scanHexInt:&toValue];

                    if (toValue != 0) {
                        NSString *unicode = [NSString stringWithCharacters:(const unichar *)&toValue length:1];
                        if (unicode) [characters appendString:unicode];
                    }
                }
                if ([characters length]) toUnicodeMap[@(fromValue)] = [characters copy];
            }else {
                hexScanner = [NSScanner scannerWithString:toString];
                [hexScanner scanHexInt:&toValue];
                if (toValue != 0) {
                    NSString *unicode = [NSString stringWithCharacters:(const unichar *)&toValue length:1];
                    if (unicode) toUnicodeMap[@(fromValue)] = unicode;
                }
            }
		}
	}

	NSMutableArray *bfRangeMappings = [NSMutableArray array];
	NSScanner *bfRangeScanner = [NSScanner scannerWithString:cmapString];
	while (![bfRangeScanner isAtEnd]) {
		[bfRangeScanner scanUpToString:@"beginbfrange" intoString:NULL];
		[bfRangeScanner scanString:@"beginbfrange" intoString:NULL];
		NSString *bfcharMapping = nil;
		[bfRangeScanner scanUpToString:@"endbfrange" intoString:&bfcharMapping];
		if (bfcharMapping) {
			[bfRangeMappings addObject:bfcharMapping];
		}
	}

	for (NSString *mapping in bfRangeMappings) {
        NSArray *tokens = [self tokensFromMappingString:mapping];
		for (int i=0; i<[tokens count]; i+=3) {
			if (i+1 >= [tokens count]) break;
			if (i+2 >= [tokens count]) break;

			NSString *fromRangeToken = tokens[i];
			NSString *toRangeToken = tokens[i+1];
			NSString *codePointToken = tokens[i+2];
			fromRangeToken = [fromRangeToken stringByTrimmingCharactersInSet:bracketCharacterSet];
			toRangeToken = [toRangeToken stringByTrimmingCharactersInSet:bracketCharacterSet];
			codePointToken = [codePointToken stringByTrimmingCharactersInSet:bracketCharacterSet];

			unsigned int fromRangeValue = 0;
			unsigned int toRangeValue = 0;
			unsigned int codePointValue = 0;
			NSScanner *hexScanner = [NSScanner scannerWithString:fromRangeToken];
			[hexScanner scanHexInt:&fromRangeValue];
			hexScanner = [NSScanner scannerWithString:toRangeToken];
			[hexScanner scanHexInt:&toRangeValue];
			hexScanner = [NSScanner scannerWithString:codePointToken];
			[hexScanner scanHexInt:&codePointValue];

			int j = 0;
			for (int k=fromRangeValue; k<=toRangeValue; k++) {
				unichar charValue = codePointValue + j;
				if (charValue != 0) {
					NSString *unicode = [NSString stringWithCharacters:(const unichar *)&charValue length:1];
					if (unicode) {
						toUnicodeMap[@(k)] = unicode;
					}
				}
				j++;
			}
		}
	}
    self.toUnicodeMap = toUnicodeMap;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSCopying

- (id)copyWithZone:(NSZone *)zone {
    PSPDFFontInfo *fontInfo = [[[self class] alloc] init];
    fontInfo.name = self.name;
    fontInfo.ascent = self.ascent;
    fontInfo.descent = self.descent;
    fontInfo.encodingArray = self.encodingArray;
    fontInfo.toUnicodeMap = self.toUnicodeMap;
    return fontInfo;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSCoding

- (id)initWithCoder:(NSCoder *)decoder {
	if((self = [super init])) {
        _name = [decoder decodeObjectForKey:NSStringFromSelector(@selector(name))];
        _ascent = [decoder decodeFloatForKey:NSStringFromSelector(@selector(ascent))];
        _descent = [decoder decodeFloatForKey:NSStringFromSelector(@selector(descent))];
        _encodingArray = [decoder decodeObjectForKey:NSStringFromSelector(@selector(encodingArray))];
        _toUnicodeMap = [decoder decodeObjectForKey:NSStringFromSelector(@selector(toUnicodeMap))];
    }
	return self;
}

- (void)encodeWithCoder:(NSCoder *)coder {
    [coder encodeObject:_name forKey:NSStringFromSelector(@selector(name))];
    [coder encodeFloat:_ascent forKey:NSStringFromSelector(@selector(ascent))];
    [coder encodeFloat:_descent forKey:NSStringFromSelector(@selector(descent))];
    [coder encodeObject:_encodingArray forKey:NSStringFromSelector(@selector(encodingArray))];
    [coder encodeObject:_toUnicodeMap forKey:NSStringFromSelector(@selector(toUnicodeMap))];
}

@end


@implementation PDFSimpleFontInfo

- (id)initWithFontDictionary:(CGPDFDictionaryRef)font {
    if ((self = [super init])) {
        NSString *fontType = PSPDFDictionaryGetString(font, @"Subtype");
        BOOL isType3Font = [fontType isEqual:@"Type3"];

        CGPDFDictionaryRef fontDescriptor = NULL;
        CGPDFDictionaryGetDictionary(font, "FontDescriptor", &fontDescriptor);

        NSString *encodingNameString = PSPDFDictionaryGetString(font, @"Encoding");
        if ([encodingNameString length]) {
            if ([encodingNameString isEqual:@"MacRomanEncoding"]) {
                self.encodingArray = [PDFSimpleFontInfo baseArrayForEncoding:kCFStringEncodingMacRoman];
            } else {
                self.encodingArray = [PDFSimpleFontInfo baseArrayForEncoding:kCFStringEncodingWindowsLatin1];
            }
        } else {
            // parse CMap?
            CGPDFStreamRef unicodeMap;
            bool fontContainsToUnicodeStream = CGPDFDictionaryGetStream(font, "ToUnicode", &unicodeMap);
            if (fontContainsToUnicodeStream) {
                self.encodingArray = nil;
                CGPDFDataFormat format = CGPDFDataFormatRaw;
                CFDataRef streamData = CGPDFStreamCopyData(unicodeMap, &format);
                NSString *string = [[NSString alloc] initWithData:(NSData *)CFBridgingRelease(streamData) encoding:NSASCIIStringEncoding];
                [self parseToUnicodeMapWithString:string];
            }

            // Get Encoding dictionary instead
            if (!self.toUnicodeMap) {
                CGPDFDictionaryRef encodingDict;
                bool fontContainsEncodingDict = CGPDFDictionaryGetDictionary(font, "Encoding", &encodingDict);
                if (fontContainsEncodingDict) {
                    NSString *baseEncoding = PSPDFDictionaryGetString(encodingDict, @"BaseEncoding");

                    NSDictionary *glyphNames = [PSPDFFontInfo glyphNames];
                    NSMutableArray *encoding;
                    if ([baseEncoding length]) {
                        if ([baseEncoding isEqual:@"MacRomanEncoding"]) {
                            encoding = [NSMutableArray arrayWithArray:[PDFSimpleFontInfo baseArrayForEncoding:kCFStringEncodingMacRoman]];
                        } else {
                            encoding = [NSMutableArray arrayWithArray:[PDFSimpleFontInfo baseArrayForEncoding:kCFStringEncodingWindowsLatin1]];
                        }
                        // MacExpert would be another (rare) case, but there is no MacExpert encoding in CFString...
                    } else {
                        // no base encoding
                        // If no base encoding is given, the built-in encoding of the font program should be used.
                        // Haven't yet found out how to determine this built-in encoding, so we use MacRoman in this case...
                        encoding = [NSMutableArray arrayWithArray:[PDFSimpleFontInfo baseArrayForEncoding:kCFStringEncodingMacRoman]];
                    }

                    CGPDFArrayRef differences;
                    bool hasDifferences = CGPDFDictionaryGetArray(encodingDict, "Differences", &differences);
                    if (hasDifferences) {
                        int charCode = 0;
                        for (int i=0; i<CGPDFArrayGetCount(differences); i++) {
                            CGPDFInteger code;
                            const char *charName;
                            bool isCode = CGPDFArrayGetInteger(differences, i, &code);
                            if (isCode) {
                                charCode = code - 1;
                            } else {
                                CGPDFArrayGetName(differences, i, &charName);
                                NSString *glyphName = @(charName);
                                NSString *glyph = glyphNames[glyphName];
                                if (glyph) {
                                    encoding[charCode] = glyph;
                                } else {
                                    //this will probably only work for very few fonts, parse the glyph name as "Gxx" (xx = hex byte)
                                    glyphName = [glyphName stringByTrimmingCharactersInSet:[[NSCharacterSet characterSetWithCharactersInString:@"ABCDEFabcdef0123456789"] invertedSet]];
                                    NSScanner *hexScanner = [NSScanner scannerWithString:glyphName];
                                    unsigned int hexValue = 0;
                                    [hexScanner scanHexInt:&hexValue];
                                    CFStringRef glyphStringRef = CFStringCreateWithBytes(NULL, (const UInt8 *)&hexValue, 1, kCFStringEncodingMacRoman, false);
                                    if (glyphStringRef != NULL) {
                                        encoding[charCode] = (__bridge NSString *)glyphStringRef;
                                        CFRelease(glyphStringRef);
                                    }
                                }
                            }
                            charCode++;
                        }
                    }
                    self.encodingArray = [NSArray arrayWithArray:encoding];
                }
            }

            // last fallback option for encoding.
            if (!self.toUnicodeMap && !self.encodingArray) {
                self.encodingArray = [PDFSimpleFontInfo baseArrayForEncoding:kCFStringEncodingMacRoman];
            }
        }

        _ascent = 1000.0; _descent = 0.0;
        CGPDFDictionaryGetNumber(fontDescriptor, "Ascent", &_ascent);
        CGPDFDictionaryGetNumber(fontDescriptor, "Descent", &_descent);
        if (_descent > 0) _descent *= -1;

        CGPDFArrayRef widthsArray = NULL;
        bool hasWidths = CGPDFDictionaryGetArray(font, "Widths", &widthsArray);

        long firstChar = 0;
        long lastChar = 0;
        CGPDFDictionaryGetInteger(font, "FirstChar", &firstChar);
        CGPDFDictionaryGetInteger(font, "LastChar", &lastChar);

        CGAffineTransform type3FontMatrix = CGAffineTransformIdentity;
        if (isType3Font) {
            CGPDFArrayRef fontMatrixArray = NULL;
            if (CGPDFDictionaryGetArray(font, "FontMatrix", &fontMatrixArray)) {
                if (CGPDFArrayGetCount(fontMatrixArray) == 6) {
                    CGPDFArrayGetNumber(fontMatrixArray, 0, &type3FontMatrix.a);
                    CGPDFArrayGetNumber(fontMatrixArray, 1, &type3FontMatrix.b);
                    CGPDFArrayGetNumber(fontMatrixArray, 2, &type3FontMatrix.c);
                    CGPDFArrayGetNumber(fontMatrixArray, 3, &type3FontMatrix.d);
                    CGPDFArrayGetNumber(fontMatrixArray, 4, &type3FontMatrix.tx);
                    CGPDFArrayGetNumber(fontMatrixArray, 5, &type3FontMatrix.ty);
                }
            }
        }

        // font bounding box is defined as a rectangle. Currently unused.
        /*
         CGRect fontBBox = CGRectMake(0, 0, 1, 1);
         if (isType3Font) {
         CGPDFArrayRef fontBBoxArray;
         if (CGPDFDictionaryGetArray(font, "FontBBox", &fontBBoxArray)) {
         if (CGPDFArrayGetCount(fontBBoxArray) == 4) {
         // corrdiantes are two diagonally opposite points.
         CGFloat llx, lly, urx, ury;
         CGPDFArrayGetNumber(fontBBoxArray, 0, &llx);
         CGPDFArrayGetNumber(fontBBoxArray, 1, &lly);
         CGPDFArrayGetNumber(fontBBoxArray, 2, &urx);
         CGPDFArrayGetNumber(fontBBoxArray, 3, &ury);
         fontBBox.origin.x = fminf(llx, urx);
         fontBBox.origin.y = fminf(lly, ury);
         fontBBox.size.width = fabs(urx-llx);
         fontBBox.size.height = fabs(ury-lly);
         }
         }
         }
         */

        int count = 0;
        if (hasWidths) {
            count = CGPDFArrayGetCount(widthsArray);
        }

        if (!hasWidths || count != (lastChar - firstChar + 1)) {
            NSString *standardFontName = PSPDFDictionaryGetString(font, @"BaseFont");

            if ([standardFontName isEqual:@"Symbol"]) {
                self.encodingArray = [PDFSimpleFontInfo baseArrayForEncoding:kCFStringEncodingMacSymbol];
            }

            NSDictionary *standardFontWidths = [PSPDFFontInfo standardFontWidths];
            NSDictionary *fontInfo = standardFontWidths[standardFontName];

            int i = 0;
            if (fontInfo) {
                NSDictionary *fontWidths = fontInfo[@"widths"];
                _ascent = [fontInfo[@"ascent"] floatValue];
                _descent = [fontInfo[@"descent"] floatValue];
                for (NSString *encodedGlyph in self.encodingArray) {
                    NSNumber *widthNumber = fontWidths[encodedGlyph];
                    if (widthNumber) {
                        widths[i] = [widthNumber floatValue];
                    } else {
                        widths[i] = 500.0;
                    }
                    i++;
                }
            } else {
                for (int j=0; j<256; j++) {
                    widths[j] = 500.0;
                }
                // could not find widths for standard font, using 500 for all glyphs as fallback.
            }
        } else {
            for (int i=0; i<256; i++) {
                if (i >= firstChar && i <= lastChar) {
                    CGFloat width = 0.0;
                    CGPDFArrayGetNumber(widthsArray, i-firstChar, &width);
                    widths[i] = width;
                    if (isType3Font) {
                        CGRect rect = CGRectMake(0,0,width,1);
                        CGRect transformedRect = CGRectApplyAffineTransform(rect, type3FontMatrix);
                        width = transformedRect.size.width * 1000.0;
                        widths[i] = width;
                    }
                }
                else {
                    widths[i] = 500.0;
                }
            }
        }
    }
	return self;
}

+ (NSArray *)baseArrayForEncoding:(CFStringEncoding)stringEncoding {
	NSMutableArray *encoding = [NSMutableArray array];
	for (int i=0; i<256; i++) {
		CFStringRef cfStr = CFStringCreateWithBytes(NULL, (const uint8_t *)&i, 1, stringEncoding, NO);
		NSString *c = (NSString *)CFBridgingRelease(cfStr);
		if (c) {
			[encoding addObject:c];
		} else {
			[encoding addObject:@""];
		}
	}

	return [NSArray arrayWithArray:encoding];
}

- (CGFloat)widthForCharacter:(uint16_t)c {
	return widths[(unsigned char)c];
}

static void PSPDFFloatCArrayFromArray(CGFloat *cArray, NSArray *array) {
    [array enumerateObjectsUsingBlock:^(NSNumber *floatValue, NSUInteger idx, BOOL *stop) {
        if (sizeof(cArray)/sizeof(CGFloat) < idx) {
            cArray[idx] = [floatValue floatValue];
        }else {
            *stop = YES;
        }
    }];
}

static NSArray *PSPDFArrayFromFloatCArray(CGFloat *cArray) {
    NSMutableArray *array = [NSMutableArray arrayWithCapacity:sizeof(array)/sizeof(CGFloat)+1];
    for (int i=0; i<sizeof(cArray)/sizeof(CGFloat); i++) {
        [array addObject:@(cArray[i])];
    }
    return array;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSCopying

- (id)copyWithZone:(NSZone *)zone {
    PDFSimpleFontInfo *fontInfo = [super copyWithZone:zone];
    if (fontInfo) {
        for (int i=0; i<sizeof(fontInfo->widths)/sizeof(CGFloat); i++) {
            fontInfo->widths[i] = self->widths[i];
        }
    }
    return fontInfo;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSCoding

- (id)initWithCoder:(NSCoder *)decoder {
	if((self = [super init])) {
        NSArray *widthArray = [decoder decodeObjectForKey:@"widths"];
        PSPDFFloatCArrayFromArray(widths, widthArray);
    }
	return self;
}

- (void)encodeWithCoder:(NSCoder *)coder {
    [coder encodeObject:PSPDFArrayFromFloatCArray(widths) forKey:@"widths"];
}

@end


@implementation PDFCompositeFontInfo

- (id)initWithFontDictionary:(CGPDFDictionaryRef)font {
	if((self = [super init])) {
        CGPDFArrayRef descendantFonts = NULL;
        CGPDFDictionaryGetArray(font, "DescendantFonts", &descendantFonts);
        CGPDFDictionaryRef cidFont = NULL;
        CGPDFArrayGetDictionary(descendantFonts, 0, &cidFont);

        CGPDFDictionaryRef fontDescriptor = NULL;
        CGPDFDictionaryGetDictionary(cidFont, "FontDescriptor", &fontDescriptor);
        _ascent = 1000.0;
        CGPDFDictionaryGetNumber(fontDescriptor, "Ascent", &_ascent);
        _descent = 0.0;
        CGPDFDictionaryGetNumber(fontDescriptor, "Descent", &_descent);

        CGPDFStreamRef unicodeMap = NULL;
        bool hasUnicodeMap = CGPDFDictionaryGetStream(font, "ToUnicode", &unicodeMap);
        if (hasUnicodeMap) {
            CGPDFDataFormat format = CGPDFDataFormatRaw;
            CFDataRef streamData = CGPDFStreamCopyData(unicodeMap, &format);
            NSString *string = [[NSString alloc] initWithData:(NSData *)CFBridgingRelease(streamData) encoding:NSASCIIStringEncoding];
            [self parseToUnicodeMapWithString:string];

        }

        if (!hasUnicodeMap) {
            NSString *cMapNameString = PSPDFDictionaryGetString(font, @"Encoding");
            /*
             const char *cmapName;
             bool hasNamedCMap = CGPDFDictionaryGetName(font, "Encoding", &cmapName);
             if (hasNamedCMap) {
             NSString *cmapNameString = @(cmapName);
             if ([cmapNameString isEqual:@"Identity-H"] || [cmapNameString isEqual:@"Identity-V"]) {
             // Identity mapping... (other mappings are currently unsupported!)
             }
             }*/

            CGPDFDictionaryRef cidSystemInfo = NULL;
            CGPDFDictionaryGetDictionary(cidFont, "CIDSystemInfo", &cidSystemInfo);
            if (cidSystemInfo != NULL) {
                //NSLog(@"cidFont:%@", PSPDFConvertPDFDictionary(cidFont));
                CGPDFStringRef registry = NULL;
                CGPDFStringRef ordering = NULL;
                CGPDFDictionaryGetString(cidSystemInfo, "Registry", &registry);
                CGPDFDictionaryGetString(cidSystemInfo, "Ordering", &ordering);
                // A matching /Supplement entry is not required for CIDFont and CMap resource compatibility.
                NSString *registryString = CFBridgingRelease(CGPDFStringCopyTextString(registry));
                NSString *orderingString = CFBridgingRelease(CGPDFStringCopyTextString(ordering));

                // TODO! is this correct???
                // first try to load CMap of encoding name
                BOOL cMapFound = NO;
                if ([cMapNameString length]) {
                    NSString *cmapPath = [[[PSPDFKitBundle() bundlePath] stringByAppendingPathComponent:@"CMaps"] stringByAppendingPathComponent:cMapNameString];
                    NSString *cmap = [[NSString alloc] initWithContentsOfFile:cmapPath encoding:NSUTF8StringEncoding error:NULL];
                    if (cmap) {
                        [self parseToUnicodeMapWithString:cmap];
                        cMapFound = YES;
                        // cMapFound = [self.toUnicodeMap count] > 0;
                    }
                }

                if ([cMapNameString length] == 0 || [cMapNameString isEqual:@"Identity-H"] || [cMapNameString isEqual:@"Identity-V"]) {
                    cMapFound = NO;
                }

                // Construct a second CMap name by concatenating the registry and ordering obtained in step (b) in
                // the format registry–ordering–UCS2 (for example, Adobe–Japan1–UCS2).
                if (!cMapFound) {
                    NSString *cmapNameString = [NSString stringWithFormat:@"%@-%@-UCS2", registryString, orderingString];
                    NSString *cmapPath = [[[PSPDFKitBundle() bundlePath] stringByAppendingPathComponent:@"CMaps"] stringByAppendingPathComponent:cmapNameString];
                    NSString *cmap = [[NSString alloc] initWithContentsOfFile:cmapPath encoding:NSUTF8StringEncoding error:NULL];
                    if (cmap) {
                        [self parseToUnicodeMapWithString:cmap];
                    }
                }
            }
        }

        CGPDFReal defaultWidth = 0.0;
        CGPDFDictionaryGetNumber(cidFont, "DW", &defaultWidth);

        for (int i=0; i<sizeof(widths)/sizeof(CGFloat); i++) {
            widths[i] = defaultWidth;
        }

        CGPDFArrayRef cidWidths = NULL;
        CGPDFDictionaryGetArray(cidFont, "W", &cidWidths);
        if (cidWidths != NULL) {
            int count = CGPDFArrayGetCount(cidWidths);
            int i = 0;
            do {
                CGPDFInteger n1, n2;
                CGPDFArrayGetInteger(cidWidths, i, &n1);
                bool secondElementIsNumber = CGPDFArrayGetInteger(cidWidths, i+1, &n2);

                if (!secondElementIsNumber) {
                    // First variant, two elements:
                    // c [w1, w2, w3 ...]
                    // c shall be an integer specifying a starting CID value; it shall be followed by an
                    // array of n numbers that shall specify the widths for n consecutive CIDs, starting
                    // with c.
                    CGPDFArrayRef widthsList;
                    bool isArray = CGPDFArrayGetArray(cidWidths, i+1, &widthsList);
                    if (isArray) {
                        int listCount = CGPDFArrayGetCount(widthsList);
                        for (int j=0; j<listCount; j++) {
                            CGPDFReal width;
                            CGPDFArrayGetNumber(widthsList, j, &width);
                            widths[n1+j] = width;
                        }
                    }
                    i += 2;
                }
                else {
                    // Second variant, three elements:
                    // cFirst cLast w
                    // Defines the same width w for all CIDs in the range cFirst to cLast
                    CGPDFReal w;
                    bool hasW = CGPDFArrayGetNumber(cidWidths, i+2, &w);
                    if (hasW) {
                        for (int j=0; j<=(n2-n1); j++) {
                            widths[n1+j] = w;
                        }
                    }
                    i += 3;
                }
            } while (i < count);
        }
	}
	return self;
}

- (CGFloat)widthForCharacter:(uint16_t)c {
	return widths[c];
}

- (BOOL)isMultiByteFont {
	return YES;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSCopying

- (id)copyWithZone:(NSZone *)zone {
    PDFCompositeFontInfo *fontInfo = [super copyWithZone:zone];
    if (fontInfo) {
        for (int i=0; i<sizeof(fontInfo->widths)/sizeof(CGFloat); i++) {
            fontInfo->widths[i] = self->widths[i];
        }
    }
    return fontInfo;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSCoding

- (id)initWithCoder:(NSCoder *)decoder {
	if((self = [super init])) {
        NSArray *widthArray = [decoder decodeObjectForKey:@"widths"];
        PSPDFFloatCArrayFromArray(widths, widthArray);
    }
	return self;
}

- (void)encodeWithCoder:(NSCoder *)coder {
    [coder encodeObject:PSPDFArrayFromFloatCArray(widths) forKey:@"widths"];
}

@end
