//
//  PSPDFTextBlock.m
//  PSPDFKit
//
//  Copyright (c) 2012 Peter Steinberger. All rights reserved.
//

#import "PSPDFTextBlock.h"
#import "PSPDFWord.h"
#import "PSPDFGlyph.h"

@implementation PSPDFTextBlock

@synthesize words = _words;

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (id)initWithGlyphs:(NSArray *)glyphs {
    if ((self = [super init])) {
        self.glyphs = glyphs;
    }
    return self;
}

- (NSString *)description {
	return [NSString stringWithFormat:@"<%@ %@ '%@'>", NSStringFromClass([self class]), NSStringFromCGRect(_frame), self.content];
}

- (NSUInteger)hash {
    return ((int)_frame.origin.x ^ (int)_frame.origin.y ^ (int)_frame.size.width ^ (int)_frame.size.height) + [self.glyphs count];
}

- (BOOL)isEqual:(id)other {
    if ([other isKindOfClass:[self class]]) {
        return [self isEqualToTextBlock:(PSPDFTextBlock *)other];
    }
    return NO;
}

- (BOOL)isEqualToTextBlock:(PSPDFTextBlock *)otherBlock {
    // font is cached, thus the same object -> use == for pointer comparison
    if (CGRectEqualToRect(self.frame, otherBlock.frame) && [self.glyphs count] == [otherBlock.glyphs count]) {
        return YES;
    }
    return NO;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public

- (void)setGlyphs:(NSArray *)glyphs {
    if (glyphs != _glyphs) {
        _glyphs = [glyphs copy];
        _frame = [[glyphs lastObject] frame];
        _words = nil; // clear cache

        // TODO: may be optimized?
        for (PSPDFGlyph *glyph in _glyphs) {
            _frame = CGRectUnion(_frame, glyph.frame);
        }
    }
}

- (NSString *)content {
    NSMutableString *content = [NSMutableString string];
	for (PSPDFGlyph *glyph in _glyphs) {
		[content appendString:glyph.content];
	}
    return [NSString stringWithString:content];
}

- (NSArray *)words {
    if (!_words) {

#if 0
        NSMutableString *content = [NSMutableString string];
        for (PSPDFGlyph *glyph in _glyphs) {
            [content appendString:glyph.content];
            if ([glyph.content isEqual:@"\n"] || glyph.lineBreaker) {
                [content appendString:@"<NEWLINE>"];
            }
        }
        NSLog(@"content: %@", content);
#endif

        // Detect word boundaries from single glyphs. Doesn't yet support parenthesis characters.
        NSMutableArray *words = [NSMutableArray array];
        NSMutableArray *currentWord = [NSMutableArray array];
        for (PSPDFGlyph *glyph in _glyphs) {
            NSString *content = glyph.content;
            BOOL isGlyphLineBreaker = [content isEqualToString:@"\n"];
            BOOL isCurrentGlyphWordBoundary = [content isEqualToString:@" "] || isGlyphLineBreaker;
            BOOL isWordBoundary = isCurrentGlyphWordBoundary || glyph.lineBreaker;
            if (isWordBoundary) {
                if (!isCurrentGlyphWordBoundary) {
                    [currentWord addObject:glyph];
                }
                if ([currentWord count] != 0) {
                    PSPDFWord *completeWord = [[PSPDFWord alloc] initWithGlyphs:currentWord];
                    completeWord.lineBreaker = isGlyphLineBreaker || glyph.lineBreaker;
                    [words addObject:completeWord];

                    currentWord = [NSMutableArray array];
                }
            }else {
                [currentWord addObject:glyph];
            }
        }
        if ([currentWord count] != 0) {
            PSPDFWord *lastWord = [[PSPDFWord alloc] initWithGlyphs:currentWord];
            [words addObject:lastWord];
        }
        _words = words;
    }
    return _words;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSCopying

- (id)copyWithZone:(NSZone *)zone {
    PSPDFTextBlock *textBlock = [[[self class] alloc] initWithGlyphs:self.glyphs];
    return textBlock;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSCoding

- (id)initWithCoder:(NSCoder *)coder {
    NSArray *glyphs = [coder decodeObjectForKey:NSStringFromSelector(@selector(glyphs))];
    self = [self initWithGlyphs:glyphs];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)coder {
    [coder encodeObject:_glyphs forKey:NSStringFromSelector(@selector(glyphs))];
}

@end
