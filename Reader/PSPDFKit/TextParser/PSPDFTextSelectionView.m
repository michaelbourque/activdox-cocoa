//
//  PSPDFTextSelectionView.m
//  PSPDFKit
//
//  Copyright 2012 Peter Steinberger. All rights reserved.
//

#import "PSPDFTextSelectionView.h"
#import "PSPDFTextParser.h"
#import "PSPDFWord.h"
#import "PSPDFGlyph.h"
#import "PSPDFLoupeView.h"
#import "PSPDFPageView.h"
#import "PSPDFHighlightAnnotation.h"
#import "PSPDFLinkAnnotation.h"
#import "PSPDFNoteAnnotation.h"
#import "PSPDFInkAnnotation.h"
#import "PSPDFLongPressGestureRecognizer.h"
#import "PSPDFDocument.h"
#import "PSPDFScrollView.h"
#import "PSPDFPageInfo.h"
#import "UIImage+PSPDFKitAdditions.h"
#import "PSPDFKitGlobal.h"
#import "PSPDFTextBlock.h"
#import "PSPDFAnnotationParser.h"
#import "PSPDFViewController.h"
#import "PSPDFWebViewController.h"
#import "PSPDFMenuItem.h"
#import "PSPDFSearchViewController.h"
#import "PSPDFConverter.h"
#import "PSPDFViewController+Internal.h"
#import "UIColor+PSPDFKitAdditions.h"
#import "PSPDFResizableView.h"
#import "PSPDFGenericAnnotationView.h"
#import "PSPDFImageInfo.h"
#import "PSPDFSelectionView.h"
#import "PSPDFAlertView.h"
#import <QuartzCore/QuartzCore.h>
#import <objc/runtime.h>

@interface PSPDFTextSelectionView ()
@property (nonatomic, strong) PSPDFResizableView *annotationSelectionView;
@property (nonatomic, strong) PSPDFLoupeView *loupeView;
/// Used while we touch down.
@property (nonatomic, strong) PSPDFWord *selectedWord;
@property (nonatomic, assign) CGRect firstLineRect;
@property (nonatomic, assign) CGRect lastLineRect;
@property (nonatomic, assign) CGRect selectionRect;
@end

@implementation PSPDFTextSelectionView {
	UIImageView *_startHandle;
	UIImageView *_endHandle;

	UIView *_startDelimiter;
	UIView *_endDelimiter;

	CGPoint _startPoint;
	CGPoint _endPoint;

	int _draggingHandle; // -1 for not dragging, 0 for start handle, 1 for end handle.

	UIView *_firstLineView;
	UIView *_lastLineView;
	UIView *_bodyView;

	UIView *_wordSelectionView;
    BOOL _cachedViewRectIsValid;

    PSPDFSelectionView *_debugTextView;
    BOOL _showTextFlowData;
}

static char *kPSPDFTextBlockKey;

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (id)initWithFrame:(CGRect)frame {
	if ((self = [super initWithFrame:frame])) {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(discardSelection) name:kPSPDFHidePageHUDElements object:nil];

		_startPoint = CGPointZero;
		_endPoint = CGPointZero;
		_draggingHandle = -1;
		self.opaque = NO;

        [PSPDFMenuItem installMenuHandlerForObject:self];
	}
    return self;
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UIView

- (void)setFrame:(CGRect)newFrame {
	[super setFrame:newFrame];
	_startPoint = _startHandle.center;
	_endPoint = _endHandle.center;
    [self updateSelectionFrame];

    if (_showTextFlowData) [self updateTextBlockViews];
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public

// Returns string that is optimized for searching.
- (NSString *)trimmedSelectedText {
    return PSPDFTrimString(self.selectedText);
}

- (void)updateTextBlockViews {
    for (UIView *subview in self.subviews) {
        if ([subview.layer.name isEqualToString:NSStringFromSelector(@selector(showTextFlowData:animated:))]) {
            PSPDFTextBlock *textBlock = objc_getAssociatedObject(subview, &kPSPDFTextBlockKey);
            subview.frame = PSPDFConvertPDFRectToViewRect(textBlock.frame, self.pageView.pageInfo.rotatedPageRect, 0, self.pageView.bounds);
        }
    }
}

// show PSPDFTextBlock positioning in an animated way
- (void)showTextFlowData:(BOOL)show animated:(BOOL)animated {
    _showTextFlowData = show;
    
    // clear old text position views
    dispatch_block_t removeViews = ^{
        for (UIView *subview in self.subviews) {
            if ([subview.layer.name isEqualToString:NSStringFromSelector(@selector(showTextFlowData:animated:))]) {
                [subview removeFromSuperview];
            }
        }
        _debugTextView.hidden = YES;
    };

    if (animated) {
        [UIView animateWithDuration:0.25f delay:0.f options:UIViewAnimationOptionAllowUserInteraction animations:^{
            for (UIView *subview in self.subviews) {
                if ([subview.layer.name isEqualToString:NSStringFromSelector(@selector(showTextFlowData:animated:))]) {
                    subview.alpha = 0.f;
                }
            }
        }completion:^(BOOL finished) { if (finished) removeViews(); }];
    }else {
        removeViews();
    }

    if (show) {
        // show overlay of all detected words
        if (!_debugTextView) {
            _debugTextView = [[PSPDFSelectionView alloc] initWithFrame:self.bounds delegate:nil];
            _debugTextView.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
            _debugTextView.userInteractionEnabled = NO;
            [self addSubview:_debugTextView];
        }
        _debugTextView.hidden = NO;

        // convert to screen space
        NSArray *words = [self.pageView.document textParserForPage:self.pageView.page].words;
        CGRect *rawRects = NULL;
        if ([words count]) {
            rawRects = malloc([words count] * sizeof(CGRect)); // malloc(0) is not defined
            [words enumerateObjectsUsingBlock:^(PSPDFWord *word, NSUInteger idx, BOOL *stop) {
                rawRects[idx] = [self.pageView convertGlyphRectToViewRect:word.frame];
            }];
        }
        [_debugTextView setRawRects:rawRects count:[words count]];

        CGFloat animationDelay = 1;
        for (PSPDFTextBlock *textBlock in _pageView.textParser.textBlocks) {
            // if ([textBlock.content length] < 10) { continue; }
            UIView *coordiateView = [[UIView alloc] initWithFrame:CGRectZero];
            coordiateView.transform = _pageView.pageInfo.pageRotationTransform;
            coordiateView.frame = PSPDFConvertPDFRectToViewRect(textBlock.frame, self.pageView.pageInfo.rotatedPageRect, 0, self.pageView.bounds);
            coordiateView.layer.borderWidth = 2.f;
            coordiateView.layer.cornerRadius = 5;
            coordiateView.layer.name = NSStringFromSelector(@selector(showTextFlowData:animated:));
            objc_setAssociatedObject(coordiateView, &kPSPDFTextBlockKey, textBlock, OBJC_ASSOCIATION_RETAIN_NONATOMIC);

            float r = arc4random() % 12;
            UIColor *randomColor = [UIColor colorWithHue:(30*r)/360 saturation:0.5f brightness:0.8f alpha:1.0f];
            coordiateView.layer.borderColor = randomColor.CGColor;
            coordiateView.backgroundColor = [randomColor colorWithAlphaComponent:0.1];
            [self addSubview:coordiateView];

            if (animated) {
                coordiateView.alpha = 0.f;
                [UIView animateWithDuration:0.25f delay:animationDelay options:0 animations:^{
                    coordiateView.alpha = 1.f;
                } completion:nil];
                animationDelay += 0.25f;
            }
        }
    }
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Private

// lazily add views only when we first need them.
- (void)lazyInit {
    if (!_firstLineView) {
#ifndef PSPDFKIT_DISABLE_TEXT_SELECTION_FEATURES
        float selectionAlpha = [UIColor pspdf_selectionAlpha];
        UIColor *selectionColor = [UIColor pspdf_selectionColor];

        UIViewAutoresizing resizeMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;

        _firstLineView = [[UIView alloc] initWithFrame:CGRectZero];
        _firstLineView.backgroundColor = selectionColor;
        _firstLineView.alpha = selectionAlpha;
        _firstLineView.autoresizingMask = resizeMask;
        [self addSubview:_firstLineView];

        _lastLineView = [[UIView alloc] initWithFrame:CGRectZero];
        _lastLineView.backgroundColor = selectionColor;
        _lastLineView.alpha = selectionAlpha;
        _lastLineView.autoresizingMask = resizeMask;
        [self addSubview:_lastLineView];

        _bodyView = [[UIView alloc] initWithFrame:CGRectZero];
        _bodyView.backgroundColor = selectionColor;
        _bodyView.alpha = selectionAlpha;
        _bodyView.autoresizingMask = resizeMask;
        [self addSubview:_bodyView];

        _startHandle = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"PSPDFKit.bundle/SelectionHandle"]];
        [self addSubview:_startHandle];

        _startDelimiter = [[UIView alloc] initWithFrame:CGRectZero];
        _startDelimiter.alpha = 0.75;
        _startDelimiter.backgroundColor = [UIColor colorWithRed:0.259 green:0.420 blue:0.949 alpha:1.0];
        [self addSubview:_startDelimiter];

        _endDelimiter = [[UIView alloc] initWithFrame:CGRectZero];
        _endDelimiter.alpha = 0.75;
        _endDelimiter.backgroundColor = [UIColor colorWithRed:0.259 green:0.420 blue:0.949 alpha:1.0];
        [self addSubview:_endDelimiter];

        _endHandle = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"PSPDFKit.bundle/SelectionHandle"]];
        [self addSubview:_endHandle];

        _wordSelectionView = [[UIView alloc] initWithFrame:CGRectZero];
        _wordSelectionView.backgroundColor = selectionColor;
        _wordSelectionView.alpha = selectionAlpha;
        [self addSubview:_wordSelectionView];

#endif
        [self discardSelection];
        [self hideSelection];
    }
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Touch Event Handling

- (BOOL)longPress:(PSPDFLongPressGestureRecognizer *)recognizer {
    // return early if feature is disabled
    if (!self.pageView.pdfController.isTextSelectionEnabled) return NO;

    BOOL didShowMenu = NO;

    // calculate loupe position
    [self.loupeView prepareShow];
	CGPoint locationForLoupe = [recognizer locationInView:self.loupeView.superview];
	CGPoint loupeCenter = CGPointMake(locationForLoupe.x, locationForLoupe.y - 64);

	UIGestureRecognizerState state = [recognizer state];
	if (state == UIGestureRecognizerStateBegan) {
        // hide menu while we perform a new action
        [[UIMenuController sharedMenuController] setMenuVisible:NO animated:YES];

        if (_draggingHandle < 0) {

            // always discard old selection
            [self discardSelection];

            CGPoint location = [recognizer locationInView:self];
            [self updateWordSelectionWithLocation:location];

            // update loupe position
            self.loupeView.mode = PSPDFLoupeViewModeCircular;
            self.loupeView.center = loupeCenter;
            [self.loupeView showLoupeAnimated:YES];
        }

        // initially, calculate the view frame of every glyph.
        _cachedViewRectIsValid = YES;
        PSPDFPageInfo *pageInfo = self.pageView.pageInfo;
        CGRect pageViewBounds = self.pageView.bounds;
        for (PSPDFGlyph *glyph in _pageView.textParser.glyphs) {
            // don't use [self.pageView convertGlyphRectToViewRect:] for performance reasons
            glyph.cachedViewRect = PSPDFConvertPDFRectToViewRect(glyph.frame, pageInfo.rotatedPageRect, 0, pageViewBounds);
        }

    }else if (state == UIGestureRecognizerStateChanged) {
        if (_draggingHandle < 0) {

            // Dragging initial word selection loupe
            _loupeView.center = loupeCenter;
            CGPoint location = [recognizer locationInView:self];
            [self updateWordSelectionWithLocation:location];
            didShowMenu = YES;
        } else {
            // Dragging selection handle
            [self moveHandle:recognizer];
        }
    }else if (state == UIGestureRecognizerStateEnded || state == UIGestureRecognizerStateCancelled) {
        if (_draggingHandle < 0) {
            [_loupeView hideLoupeAnimated:YES];

            _wordSelectionView.hidden = YES;
            if (self.selectedWord) {
                self.selectedGlyphs = self.selectedWord.glyphs;
                didShowMenu = YES;
            }
            [self updateMenuAnimated:YES];
        } else if (_draggingHandle == 0 || _draggingHandle == 1) {
            // Ended dragging selection handle
            [_loupeView hideLoupeAnimated:YES];
            _draggingHandle = -1;
            // menu is handled one level up
        }
        _cachedViewRectIsValid = NO;
    }
    return didShowMenu;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Text Selection

/// Update word selection (finger is touched down)
- (void)updateWordSelectionWithLocation:(CGPoint)location {
    [self lazyInit];
    PSPDFPageView *pageView = self.pageView;
    NSDictionary *objects = [pageView objectsAtPoint:location options:@{kPSPDFObjectsFullWords : @YES, kPSPDFObjectsImages : @YES,
                             kPSPDFObjectsSmartSort : @YES, kPSPDFObjectsFindFirstOnly : @YES}];
    self.selectedWord = [objects[kPSPDFWords] ps_firstObject];
    self.selectedImage = nil;
    _wordSelectionView.hidden = self.selectedWord == nil;
    _wordSelectionView.frame = [pageView convertGlyphRectToViewRect:self.selectedWord.frame];

    if (!self.selectedWord && self.pageView.pdfController.isImageSelectionEnabled) {
        self.selectedImage = [objects[kPSPDFImages] ps_firstObject];
        if (self.selectedImage) {
            _wordSelectionView.hidden = NO;
            _wordSelectionView.frame = [self.pageView convertPDFRectToViewRect:self.selectedImage.boundingBox];
            PSPDFLogVerbose(@"Image id: %@\r\nWidth: %i\r\nHeight: %i\r\nHres: %f\r\nVres: %f",
                            self.selectedImage.imageID, self.selectedImage.pixelWidth, self.selectedImage.pixelHeight,
                            self.selectedImage.horizontalResolution, self.selectedImage.verticalResolution);
        }
    }
}

- (void)discardSelection {
    self.selectedGlyphs = nil;
}

- (BOOL)hasSelection {
    return _selectedGlyphs != nil;
}

- (BOOL)pressRecognizerShouldHandlePressImmediately:(PSPDFLongPressGestureRecognizer *)recognizer {
    if (self.selectedGlyphs) {
        CGPoint locationInStartHandle = [recognizer locationInView:_startHandle];
        CGPoint locationInEndHandle = [recognizer locationInView:_endHandle];
        if (CGRectContainsPoint(_startHandle.bounds, locationInStartHandle)) {
            // Dragging start handle
            _draggingHandle = 0;
            [self moveHandle:recognizer];
            return YES;
        } else if (CGRectContainsPoint(_endHandle.bounds, locationInEndHandle)) {
            // Dragging end handle
            _draggingHandle = 1;
            [self moveHandle:recognizer];
            return YES;
        }
    }

    return NO;
}

- (void)moveHandle:(UILongPressGestureRecognizer *)recognizer {
    CGPoint location = [recognizer locationInView:self];

    CGPoint startLoc = (_draggingHandle == 0) ? CGPointMake(location.x, location.y + 0.2 * _startHandle.bounds.size.height) : _startPoint;
    CGPoint endLoc = (_draggingHandle == 1) ? CGPointMake(location.x, location.y - 0.2 * _endHandle.bounds.size.height) : _endPoint;

    PSPDFTextParser *parser = _pageView.textParser;
    PSPDFGlyph *firstGlyph = nil;
    int i = 0;
    int indexOfFirstGlyph = 0;
    float minFirstGlyphDistance = MAXFLOAT;

    for (PSPDFGlyph *glyph in parser.glyphs) {
        CGRect glyphFrame = glyph.cachedViewRect;
        if (CGRectGetMidX(glyphFrame) > startLoc.x && CGRectGetMidY(glyphFrame) > startLoc.y) {
            CGPoint glyphCenter = CGPointMake(CGRectGetMidX(glyphFrame), CGRectGetMidY(glyphFrame));
            float distanceFromStartLoc = sqrtf(powf(fabsf(glyphCenter.x - startLoc.x), 2) + powf(fabsf(glyphCenter.y - startLoc.y), 2));
            if (distanceFromStartLoc < minFirstGlyphDistance) {
                firstGlyph = glyph;
                indexOfFirstGlyph = i;
                minFirstGlyphDistance = distanceFromStartLoc;
                //break;
            }
        }
        i++;
    }

    PSPDFGlyph *lastGlyph = nil;
    int indexOfLastGlyph = 0;
    i = [parser.glyphs count] - 1;
    float minLastGlyphDistance = MAXFLOAT;
    for (PSPDFGlyph *glyph in [parser.glyphs reverseObjectEnumerator]) {
        CGRect glyphFrame = glyph.cachedViewRect;
        if (CGRectGetMidX(glyphFrame) < endLoc.x && CGRectGetMidY(glyphFrame) < endLoc.y) {
            CGPoint glyphCenter = CGPointMake(CGRectGetMidX(glyphFrame), CGRectGetMidY(glyphFrame));
            float distanceFromEndLoc = sqrtf(powf(fabsf(glyphCenter.x - endLoc.x), 2) + powf(fabsf(glyphCenter.y - endLoc.y), 2));
            if (distanceFromEndLoc < minLastGlyphDistance) {
                lastGlyph = glyph;
                indexOfLastGlyph = i;
                minLastGlyphDistance = distanceFromEndLoc;
            }
        }
        i--;
    }

    if (firstGlyph && lastGlyph) {
        if (indexOfLastGlyph >= indexOfFirstGlyph) {
            NSArray *selectionCandidates = [parser.glyphs subarrayWithRange:NSMakeRange(indexOfFirstGlyph, indexOfLastGlyph - indexOfFirstGlyph + 1)];
            self.selectedGlyphs = [self reduceGlyphsToColumn:selectionCandidates];

            PSPDFGlyph *referenceGlyph = (_draggingHandle == 0) ? firstGlyph : lastGlyph;
            CGRect referenceGlyphFrame = referenceGlyph.cachedViewRect;

            CGPoint loupePoint = CGPointMake((_draggingHandle == 0) ? CGRectGetMinX(referenceGlyphFrame) : CGRectGetMaxX(referenceGlyphFrame), CGRectGetMinY(referenceGlyphFrame));
            loupePoint = [_loupeView.superview convertPoint:loupePoint fromView:self];
            loupePoint.y -= (_draggingHandle == 0) ? 50 : 35;

            float zoomScale = self.pageView.scrollView.zoomScale;
            CGSize targetSize = CGSizeMake(zoomScale * CGRectGetHeight(referenceGlyphFrame) * 114.0/35.0, zoomScale * CGRectGetHeight(referenceGlyphFrame));
            _loupeView.targetSize = targetSize;

            if (loupePoint.x != _loupeView.center.x || loupePoint.y != _loupeView.center.y) {
                _loupeView.center = loupePoint;
            }
            _loupeView.mode = (_draggingHandle == 0) ? PSPDFLoupeViewModeDetailTop : PSPDFLoupeViewModeDetailBottom;
            [_loupeView showLoupeAnimated:YES];
        }
    }
}

- (void)updateSelection {
    [self updateSelectionHandleSize];
}

- (void)updateSelectionHandleSize {
    if (!self.window || ![self.selectedGlyphs count]) return;

    //adjust the size of the selection handles to the current zoom scale:
    float zoomScale = _pageView.scrollView.zoomScale;
    float handleSize = 32.0 * (1.0/zoomScale);

    // relay zoomScale.
    _annotationSelectionView.zoomScale = self.pageView.scrollView.zoomScale; // important!

    _endHandle.frame = CGRectMake(_endPoint.x - handleSize/2, _endPoint.y - handleSize/2 + handleSize * 0.2, handleSize, handleSize);
    _startHandle.frame = CGRectMake(_startPoint.x - handleSize/2, _startPoint.y - handleSize/2 - handleSize * 0.2, handleSize, handleSize);

    float pointSize = _startHandle.bounds.size.height / 32.0;
    if (_selectedGlyphs && [_selectedGlyphs count] > 0) {
        CGRect firstGlyphFrame = [self.pageView convertGlyphRectToViewRect:[_selectedGlyphs[0] frame]];
        CGRect lastGlyphFrame = [self.pageView convertGlyphRectToViewRect:[[_selectedGlyphs lastObject] frame]];
        _startDelimiter.frame = CGRectMake(firstGlyphFrame.origin.x - pointSize, firstGlyphFrame.origin.y, 3.0 * pointSize, firstGlyphFrame.size.height);
        _endDelimiter.frame = CGRectMake(lastGlyphFrame.origin.x + lastGlyphFrame.size.width - pointSize, lastGlyphFrame.origin.y, 3.0 * pointSize, lastGlyphFrame.size.height);
    }
}

- (void)setSelectedGlyphs:(NSArray *)selectedGlyphs {
#ifdef PSPDFKIT_DISABLE_TEXT_SELECTION_FEATURES
    selectedGlyphs = nil;
#endif

    if (![selectedGlyphs isEqual:_selectedGlyphs]) {
        NSString *text = nil;
        CGRect textRect = CGRectZero;

        // call delegate
        BOOL shouldSelectText = YES;
        if ([selectedGlyphs count]) {
            PSPDFWord *word = [[PSPDFWord alloc] initWithGlyphs:selectedGlyphs];
            text = [word stringValue];
            textRect = [self.pageView convertGlyphRectToViewRect:PSPDFBoundingBoxFromGlyphs(selectedGlyphs, CGAffineTransformIdentity)];
            shouldSelectText = [self.pageView.pdfController delegateShouldSelectText:text withGlyphs:selectedGlyphs atRect:textRect onPageView:self.pageView];
        }

        if (shouldSelectText) {
            _selectedGlyphs = selectedGlyphs;
        }
        [self updateSelectionFrame];

        // call didSelectText
        if ([selectedGlyphs count] && shouldSelectText) {
            [self.pageView.pdfController delegateDidSelectText:text withGlyphs:selectedGlyphs atRect:textRect onPageView:self.pageView];
        }

        // clear cache
        _selectedText = nil;
    }
}

- (void)hideSelection {
    self.selectedWord = nil;
    self.selectedImage = nil;
    _startHandle.hidden = YES;
    _endHandle.hidden = YES;
    _startDelimiter.hidden = YES;
    _endDelimiter.hidden = YES;
    _firstLineView.hidden = YES;
    _bodyView.hidden = YES;
    _lastLineView.hidden = YES;
    _draggingHandle = -1;
    _startHandle.frame = CGRectZero;
    _endHandle.frame = CGRectZero;
    self.firstLineRect = CGRectZero;
    self.lastLineRect = CGRectZero;
    self.selectionRect = CGRectZero;
}

- (void)updateSelectionFrame {
    if (!self.window) return;

    _startHandle.hidden = NO;
    _endHandle.hidden = NO;
    _startDelimiter.hidden = NO;
    _endDelimiter.hidden = NO;
    _firstLineView.hidden = NO;
    _bodyView.hidden = NO;
    _lastLineView.hidden = NO;
    PSPDFPageInfo *pageInfo = self.pageView.pageInfo;

    if ([_selectedGlyphs count] > 0) {
        [self lazyInit];

        CGRect firstGlyphFrame = _cachedViewRectIsValid ? [_selectedGlyphs[0] cachedViewRect] : [self.pageView convertGlyphRectToViewRect:[_selectedGlyphs[0] frame]];
        CGRect lastGlyphFrame = _cachedViewRectIsValid ? [[_selectedGlyphs lastObject] cachedViewRect] : [self.pageView convertGlyphRectToViewRect:[[_selectedGlyphs lastObject] frame]];

        _startPoint = CGPointMake(CGRectGetMinX(firstGlyphFrame), CGRectGetMinY(firstGlyphFrame));
        _startHandle.center = CGPointMake(_startPoint.x, _startPoint.y - 0.2 * _startHandle.bounds.size.height);

        _endPoint = CGPointMake(CGRectGetMaxX(lastGlyphFrame), CGRectGetMaxY(lastGlyphFrame));
        _endHandle.center = CGPointMake(_endPoint.x, _endPoint.y + 0.2 * _endHandle.bounds.size.height);

        float pointSize = _startHandle.bounds.size.height / 32.0;
        _startDelimiter.frame = CGRectMake(firstGlyphFrame.origin.x - pointSize, firstGlyphFrame.origin.y, 3.0 * pointSize, firstGlyphFrame.size.height);
        _endDelimiter.frame = CGRectMake(lastGlyphFrame.origin.x + lastGlyphFrame.size.width - pointSize, lastGlyphFrame.origin.y, 3.0 * pointSize, lastGlyphFrame.size.height);

        float minX = MAXFLOAT;
        float maxX = -MAXFLOAT;
        float minY = MAXFLOAT;
        float maxY = -MAXFLOAT;
        for (PSPDFGlyph *selectedGlyph in _selectedGlyphs) {
            CGRect glyphFrame;
            if (_cachedViewRectIsValid) {
                glyphFrame = selectedGlyph.cachedViewRect;
            }else {
                // don't use [self.pageView convertGlyphRectToViewRect:] for performance reasons
                glyphFrame = PSPDFConvertPDFRectToViewRect(selectedGlyph.frame, pageInfo.rotatedPageRect, 0, self.pageView.bounds);
            }

            if (glyphFrame.origin.x < minX) minX = glyphFrame.origin.x;
            if (glyphFrame.origin.x + glyphFrame.size.width > maxX && glyphFrame.origin.x + glyphFrame.size.width < MAXFLOAT) maxX = glyphFrame.origin.x + glyphFrame.size.width;

            if (glyphFrame.origin.y < minY) minY = glyphFrame.origin.y;
            if (glyphFrame.origin.y + glyphFrame.size.height > maxY) maxY = glyphFrame.origin.y + glyphFrame.size.height;
        }

        CGRect firstLineRect = CGRectMake(firstGlyphFrame.origin.x, firstGlyphFrame.origin.y, maxX - firstGlyphFrame.origin.x, firstGlyphFrame.size.height);
        CGRect lastLineRect = CGRectMake(minX, maxY, lastGlyphFrame.origin.x + lastGlyphFrame.size.width - minX, -lastGlyphFrame.size.height);

        CGRect bodyRect = CGRectMake(CGRectGetMinX(lastLineRect),
                                     CGRectGetMaxY(firstLineRect),
                                     CGRectUnion(firstLineRect, lastLineRect).size.width,
                                     CGRectGetMinY(lastLineRect) - CGRectGetMaxY(firstLineRect));

        _firstLineView.frame = firstLineRect;
        _lastLineView.frame = lastLineRect;
        _bodyView.frame = bodyRect;
        self.firstLineRect = firstLineRect;
        self.lastLineRect = lastLineRect;
        self.selectionRect = bodyRect;

        if (CGRectIntersectsRect(firstLineRect, lastLineRect)) {
            _bodyView.hidden = YES;
        } else {
            _bodyView.hidden = NO;
        }
        _lastLineView.hidden = (CGRectEqualToRect(firstLineRect, lastLineRect));
    }else {
        [self hideSelection];
    }
}

// evaluate lazily
- (NSString *)selectedText {
    if (!_selectedText) {
        _selectedText = [[[PSPDFWord alloc] initWithGlyphs:self.selectedGlyphs] stringValue];
    }
    return _selectedText;
}

// TODO: this is slow.
- (NSArray *)reduceGlyphsToColumn:(NSArray *)glyphs {
    // 1.  Divide glyphs in lines
    // 2.  Divide lines in columns, interpreting a significant jump in indexOnPage as a gutter
    // 2b. store which of these columns contains the first and last glyph of the selection
    // 3.  Form the union rect of the frames of the columns from step 2
    // 4.  Iterate over all columns, discard those that don't intersect with the union rect from step 3

    PSPDFGlyph *firstGlyph = glyphs[0];
    PSPDFGlyph *lastGlyph = [glyphs lastObject];
    NSArray *columnWithFirstGlyph = nil;
    NSArray *columnWithLastGlyph = nil;

    NSMutableArray *lines = [NSMutableArray array];
    {
        NSMutableArray *currentLine = [NSMutableArray array];
        [lines addObject:currentLine];
        PSPDFGlyph *prevGlyph = nil;
        for (PSPDFGlyph *glyph in glyphs) {
            if (prevGlyph) {
                if (![glyph isOnSameLineAs:prevGlyph]) {
                    currentLine = [NSMutableArray array];
                    [lines addObject:currentLine];
                }
            }
            [currentLine addObject:glyph];
            prevGlyph = glyph;
        }
    }

    NSMutableArray *columnsByLine = [NSMutableArray array];
    for (NSArray *line in lines) {
        @autoreleasepool {
            NSMutableArray *currentLine = [NSMutableArray array];
            NSMutableArray *currentColumn = [NSMutableArray array];
            [currentLine addObject:currentColumn];
            PSPDFGlyph *prevGlyph = nil;
            for (PSPDFGlyph *glyph in line) {
                if (glyph.indexOnPage != -1 && prevGlyph) {
                    if (abs(prevGlyph.indexOnPage - glyph.indexOnPage) > 3) {
                        currentColumn = [NSMutableArray array];
                        [currentLine addObject:currentColumn];
                    }
                }
                if (glyph == firstGlyph) {
                    columnWithFirstGlyph = currentColumn;
                }
                if (glyph == lastGlyph) {
                    columnWithLastGlyph = currentColumn;
                }

                [currentColumn addObject:glyph];
                if (glyph.indexOnPage != -1) {
                    prevGlyph = glyph;
                }
            }
            [columnsByLine addObject:currentLine];
        }
    }

    PSPDFGlyph *firstGlyphInStartColumn = columnWithFirstGlyph[0];
    PSPDFGlyph *lastGlyphInStartColumn = [columnWithFirstGlyph lastObject];
    CGRect columnWithFirstGlyphFrame = CGRectUnion(firstGlyphInStartColumn.frame, lastGlyphInStartColumn.frame);

    PSPDFGlyph *firstGlyphInEndColumn = columnWithLastGlyph[0];
    PSPDFGlyph *lastGlyphInEndColumn = [columnWithLastGlyph lastObject];
    CGRect columnWithLastGlyphFrame = CGRectUnion(firstGlyphInEndColumn.frame, lastGlyphInEndColumn.frame);

    CGRect unionRect = CGRectUnion(columnWithFirstGlyphFrame, columnWithLastGlyphFrame);

    NSMutableArray *filteredGlyphs = [NSMutableArray array];
    for (NSArray *columnedLine in columnsByLine) {
        for (NSArray *column in columnedLine) {
            PSPDFGlyph *firstGlyphInColumn = column[0];
            PSPDFGlyph *lastGlyphInColumn = [column lastObject];
            CGRect columnFrame = CGRectUnion(firstGlyphInColumn.frame, lastGlyphInColumn.frame);
            if (CGRectIntersectsRect(columnFrame, unionRect)) {
                [filteredGlyphs addObjectsFromArray:column];
            }
        }
    }
    return filteredGlyphs;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Other Menu Actions

- (void)addAnnotationWithType:(NSString *)annotationType {
    CGAffineTransform t = _pageView.pageInfo.pageRotationTransform;
    CGRect boundingBox;
    NSArray *highlighedRects = PSPDFRectsFromGlyphs(self.selectedGlyphs, t, &boundingBox);
    PSPDFHighlightAnnotationType highlightType = [PSPDFHighlightAnnotation highlightTypeFromTypeString:annotationType];
    PSPDFHighlightAnnotation *annotation = [[[self.pageView.document classForClass:[PSPDFHighlightAnnotation class]] alloc] initWithHighlightType:highlightType];
    annotation.rects = highlighedRects;
    annotation.boundingBox = boundingBox;
    [self.pageView.document addAnnotations:@[annotation] forPage:self.pageView.page];

    [_pageView updateView];
    [self discardSelection];
}

- (void)dictionary:(id)sender {
    NSString *searchTerm = [self trimmedSelectedText];
    UIReferenceLibraryViewController *referenceLibraryViewController = [[UIReferenceLibraryViewController alloc] initWithTerm:searchTerm];

    if (PSIsIpad()) {
        UIPopoverController *popoverController = [[UIPopoverController alloc] initWithContentViewController:referenceLibraryViewController];
        self.pageView.pdfController.popoverController = popoverController;
        [popoverController presentPopoverFromRect:CGRectIntegral(_firstLineView.frame) inView:self permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    }else {
        [self.pageView.pdfController presentModalViewController:referenceLibraryViewController embeddedInNavigationController:NO withCloseButton:NO animated:YES];
    }
}

- (void)wikipedia {
    NSString *wikipediaLanguage = nil;

    // Map system language identifiers to valid wikipedia languages (1:1 except for zh-Hans):
    NSDictionary *wikipediaByLanguage = @{@"en": @"en", @"de": @"de", @"fr": @"fr", @"ja": @"ja", @"nl": @"nl", @"it": @"it", @"es": @"es", @"zh": @"zh-Hans", @"zh": @"zh", @"ru": @"ru"};

    // Find the first preferred language that is a valid wikipedia language:
    for (NSString *preferredLanguage in [NSLocale preferredLanguages]) {
        wikipediaLanguage = wikipediaByLanguage[preferredLanguage];
        if (wikipediaLanguage) break;
    }

    // Use English as default if no preferred language matches:
    wikipediaLanguage = wikipediaLanguage ?: @"en";

    NSString *searchTerm = [self trimmedSelectedText];
    NSString *URLString = [NSString stringWithFormat:@"http://%@.m.wikipedia.org/wiki/%@", wikipediaLanguage, [searchTerm stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];

    PSPDFViewController *pdfController = self.pageView.pdfController;
    PSPDFWebViewController *browser = [[[pdfController classForClass:[PSPDFWebViewController class]] alloc] initWithURL:[NSURL URLWithString:URLString]];
    browser.delegate = pdfController;
    UINavigationController *browserNavController = [[UINavigationController alloc] initWithRootViewController:browser];
    browser.contentSizeForViewInPopover = CGSizeMake(600, 500);

    CGRect targetRect = [self convertRect:_firstLineView.frame toView:self.pageView.pdfController.view];
    [self.pageView.pdfController presentViewControllerModalOrPopover:browserNavController embeddedInNavigationController:YES withCloseButton:YES animated:YES sender:nil options:@{PSPDFPresentOptionRect : BOXED(targetRect)}];
}

- (void)search {
    [self.pageView.pdfController searchForString:[self trimmedSelectedText] animated:YES];
    [self discardSelection];
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Text selection menu

- (NSArray *)menuItemsForImageSelection:(PSPDFImageInfo *)imageSelection {
    NSMutableArray *menuItems = [NSMutableArray array];

    if (imageSelection && _pageView.document.allowsCopying) {
        PSPDFMenuItem *copyItem = [[PSPDFMenuItem alloc] initWithTitle:PSPDFLocalize(@"Copy") block:^{
            UIImage *image = imageSelection.image;
            // Broken on iOS6!
            //[[UIPasteboard generalPasteboard] setImage:image];
            if (image) {
                NSData *imgData = UIImagePNGRepresentation(image);
                [[UIPasteboard generalPasteboard] setData:imgData forPasteboardType:UIPasteboardTypeListImage[0]];
            }
        } identifier:@"Copy"];
        [menuItems addObject:copyItem];

        PSPDFMenuItem *saveItem = [[PSPDFMenuItem alloc] initWithTitle:PSPDFLocalize(@"Save To Camera Roll") block:^{
            UIImage *image = imageSelection.image;
            if (image) UIImageWriteToSavedPhotosAlbum(image, nil, NULL, NULL);
        } identifier:@"Save To Camera Roll"];
        [menuItems addObject:saveItem];
    }
    return menuItems;
}

#define kMaxUsefulStringLength 100
- (NSArray *)menuItemsForTextSelection:(NSString *)selectedText {
    NSMutableArray *menuItems = [NSMutableArray array];

    if (_pageView.document.allowsCopying && [selectedText length]) {
        PSPDFMenuItem *copyItem = [[PSPDFMenuItem alloc] initWithTitle:PSPDFLocalize(@"Copy") block:^{
            [[UIPasteboard generalPasteboard] setString:selectedText];
            PSPDFLog(@"copied text: %@", self.selectedText);
            //[[[UIAlertView alloc] initWithTitle:@"Copied Text" message:self.selectedText delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
        } identifier:@"Copy"];
        [menuItems addObject:copyItem];
    }
    NSSet *editableAnnotationTypes = self.pageView.document.editableAnnotationTypes;
    if ([editableAnnotationTypes containsObject:PSPDFAnnotationTypeStringHighlight]) {
        PSPDFMenuItem *highlightItem = [[PSPDFMenuItem alloc] initWithTitle:PSPDFLocalize(@"Highlight") block:^{
            [self addAnnotationWithType:PSPDFAnnotationTypeStringHighlight];
            [self discardSelection];
        } identifier:PSPDFAnnotationTypeStringHighlight];
        [menuItems addObject:highlightItem];
    }

    // space is too rare on the iPhone.
    if (PSIsIpad()) {
        if ([editableAnnotationTypes containsObject:PSPDFAnnotationTypeStringUnderline]) {
            PSPDFMenuItem *underlineItem = [[PSPDFMenuItem alloc] initWithTitle:PSPDFLocalize(@"Underline") block:^{
                [self addAnnotationWithType:PSPDFAnnotationTypeStringUnderline];
                [self discardSelection];
            } identifier:PSPDFAnnotationTypeStringUnderline];
            [menuItems addObject:underlineItem];
        }

        if ([editableAnnotationTypes containsObject:PSPDFAnnotationTypeStringStrikeout]) {
            PSPDFMenuItem *strikeoutItem = [[PSPDFMenuItem alloc] initWithTitle:PSPDFLocalize(@"Strikeout") block:^{
                [self addAnnotationWithType:PSPDFAnnotationTypeStringStrikeout];
                [self discardSelection];
            } identifier:PSPDFAnnotationTypeStringStrikeout];
            [menuItems addObject:strikeoutItem];
        }
    }

    BOOL stringTooLong = [selectedText length] > kMaxUsefulStringLength;
    if (!stringTooLong) {
        BOOL dictEntryFound = NO;
        BOOL skipDictSearch = NO;
        if (!skipDictSearch && NSClassFromString(@"UIReferenceLibraryViewController") != nil) {
            NSString *searchTerm = [self trimmedSelectedText];
            if ([UIReferenceLibraryViewController dictionaryHasDefinitionForTerm:searchTerm]) {
                UIMenuItem *defineItem = [[UIMenuItem alloc] initWithTitle:PSPDFLocalize(@"Define") action:@selector(dictionary:)];
                [menuItems addObject:defineItem];
                dictEntryFound = YES;
            }
        }

        // fallback to Wikipedia for iOS4 or if the term couldn't be found.
        if (!dictEntryFound) {
            PSPDFMenuItem *wikipediaItem = [[PSPDFMenuItem alloc] initWithTitle:PSPDFLocalize(@"Wikipedia") block:^{
                [self wikipedia];
            } identifier:@"Wikipedia"];
            [menuItems addObject:wikipediaItem];
        }

        if ([selectedText length] >= kPSPDFMinimumSearchLength && !stringTooLong) {
            PSPDFMenuItem *searchItem = [[PSPDFMenuItem alloc] initWithTitle:PSPDFLocalize(@"Search") block:^{
                [self search];
            } identifier:@"Search"];
            [menuItems addObject:searchItem];
        }
    }

    if ([editableAnnotationTypes containsObject:PSPDFAnnotationTypeStringLink]) {
        PSPDFPageView *pageView = self.pageView;
        PSPDFMenuItem *createLinkItem = [[PSPDFMenuItem alloc] initWithTitle:PSPDFLocalize(@"Create Link...") block:^{
            PSPDFAlertView *linkAlertView = [[PSPDFAlertView alloc] initWithTitle:PSPDFLocalize(@"Link Destination") message:PSPDFLocalize(@"Enter page index or a http:// address to link to an URL.")];
            linkAlertView.alertViewStyle = UIAlertViewStylePlainTextInput;
            
            // TODO smart preselection!
//            [[linkAlertView textFieldAtIndex:0] setText:@"http://google.com"];

            [linkAlertView setCancelButtonWithTitle:PSPDFLocalize(@"Cancel") block:nil];
            __weak PSPDFAlertView *weakAlertView = linkAlertView;
            [linkAlertView addButtonWithTitle:PSPDFLocalize(@"Add Link") block:^{
                NSString *linkTarget = [weakAlertView textFieldAtIndex:0].text;
                if ([linkTarget length] == 0) {
                    PSPDFLogWarning(@"Will ignore creating link, empty target.");
                    //[[[UIAlertView alloc] initWithTitle:PSPDFLocalize((@"Link will not be created, empty target.") message:nil delegate:nil cancelButtonTitle:PSPDFLocalize(@"OK") otherButtonTitles:nil] show];
                }else {
                    PSPDFLinkAnnotation *linkAnnotation = [[PSPDFLinkAnnotation alloc] initWithLinkAnnotationType:PSPDFLinkAnnotationPage];

                    // calculate the boundingBox from the selected glyphs.
                    linkAnnotation.boundingBox = PSPDFBoundingBoxFromGlyphs(pageView.selectionView.selectedGlyphs, pageView.pageInfo.pageRotationTransform);

                    // add the URL or the page index
                    BOOL isHTTPLink = [linkTarget hasPrefix:@"http"] || [linkTarget hasPrefix:@"mailto"];
                    if (isHTTPLink) {
                        linkAnnotation.linkType = PSPDFLinkAnnotationWebURL;
                        linkAnnotation.siteLinkTarget = linkTarget;
                        linkAnnotation.URL = [NSURL URLWithString:linkTarget];
                    }else {
                        linkAnnotation.pageLinkTarget = [linkTarget integerValue];
                    }

                    // add the annotation
                    [pageView.pdfController.document addAnnotations:@[linkAnnotation] forPage:pageView.page];
                    [pageView addAnnotation:linkAnnotation animated:YES];

                    // clear selection (so we can actually see the new link)
                    [pageView.selectionView discardSelection];
                }
            }];
            [linkAlertView show];
        } identifier:@"Create Link..."];
        [menuItems addObject:createLinkItem];
    }
    return  menuItems;
}

// Show/Update menu for text selection
- (void)updateMenuAnimated:(BOOL)animated {
#ifndef PSPDFKIT_DISABLE_TEXT_SELECTION_FEATURES
    [self updateSelection];

    // show the editing menu
    UIMenuController *menu = [UIMenuController sharedMenuController];
    BOOL didShowMenu = NO;
    if ([_selectedGlyphs count] > 0) {

        CGRect firstLineFrameOnPageViewCoords = [_pageView convertRect:_firstLineView.frame fromView:self];
        CGRect lastLineFrameOnPageViewCoords = [_pageView convertRect:_lastLineView.frame fromView:self];
        if (firstLineFrameOnPageViewCoords.origin.y > 0 || lastLineFrameOnPageViewCoords.origin.y > 0) {

            CGRect targetRect = CGRectZero;
            if (CGRectIntersectsRect(_pageView.visibleRect, firstLineFrameOnPageViewCoords)) {
                targetRect = _firstLineView.frame;
            } else if (CGRectIntersectsRect(_pageView.visibleRect, lastLineFrameOnPageViewCoords)) {
                targetRect = _lastLineView.frame;
            }

            if (!CGRectIsEmpty(targetRect)) {
                NSArray *menuItems = [self menuItemsForTextSelection:self.selectedText];
                PSPDFViewController *pdfController = self.pageView.pdfController;
                NSArray *newMenuItems = [pdfController delegateShouldShowMenuItems:menuItems atSuggestedTargetRect:targetRect forSelectedText:self.selectedText inRect:PSPDFBoundingBoxFromGlyphs(self.selectedGlyphs, CGAffineTransformIdentity) onPageView:self.pageView];

                if ([newMenuItems count]) {
                    [self becomeFirstResponder];
                    [menu setMenuItems:newMenuItems];
                    [menu setTargetRect:targetRect inView:self];
                    [menu setMenuVisible:YES animated:animated];
                    didShowMenu = YES;
                }
            }
        }
    }
    else if (self.selectedImage) {
        CGRect targetRect = [self.pageView convertPDFRectToViewRect:self.selectedImage.boundingBox];
        if (!CGRectIsEmpty(targetRect)) {
            NSArray *menuItems = [self menuItemsForImageSelection:self.selectedImage];
            PSPDFViewController *pdfController = self.pageView.pdfController;
            menuItems = [pdfController delegateShouldShowMenuItems:menuItems atSuggestedTargetRect:targetRect forSelectedImage:self.selectedImage inRect:targetRect onPageView:self.pageView];

            if ([menuItems count]) {
                [self becomeFirstResponder];
                [menu setMenuItems:menuItems];
                [menu setTargetRect:targetRect inView:self];
                [menu setMenuVisible:YES animated:animated];
                didShowMenu = YES;
            }
        }
    }
    
    if (!didShowMenu) [menu setMenuVisible:NO animated:animated];
#endif
}

- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event {
    return nil;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - static

+ (BOOL)isTextSelectionFeatureAvailable {
#ifndef PSPDFKIT_DISABLE_TEXT_SELECTION_FEATURES
    return YES;
#else
    return NO;
#endif
}

@end
