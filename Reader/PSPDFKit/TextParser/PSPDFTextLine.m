//
//  PSPDFTextLine.m
//  PSPDFKit
//
//  Copyright (c) 2012 Peter Steinberger. All rights reserved.
//

#import "PSPDFTextLine.h"

@interface PSPDFTextLine ()
@property (nonatomic, weak) PSPDFTextLine *prevLine;
@property (nonatomic, weak) PSPDFTextLine *nextLine;
@end

@implementation PSPDFTextLine

@synthesize borderType = _borderType;

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (id)initWithGlyphs:(NSArray *)wordGlyphs {
    if ((self = [super initWithGlyphs:wordGlyphs])) {
        _blockID = -1;
    }
    return self;
}

- (NSString *)description {
	NSString *description = [NSString stringWithFormat:@"<(bid:%d,borderType:%d) %@ %@ %@>", _blockID, self.borderType, NSStringFromClass([self class]), [self stringValue], NSStringFromCGRect(self.frame)];
    return description;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public

- (PSPDFTextLineBorder)borderType {
    if (_borderType == PSPDFTextLineBorderUndefined) {
        if (!self.prevLine && self.nextLine) {
            _borderType = PSPDFTextLineBorderTopDown; // first line
        }else if (!self.nextLine && self.prevLine) {
            _borderType = PSPDFTextLineBorderBottomUp; // last line
        }else if (!self.nextLine && !self.prevLine) {
            _borderType = PSPDFTextLineBorderNone;
        }else {
            CGFloat verticalDistanceToPrev = CGRectGetMidY(_prevLine.frame) - CGRectGetMidY(self.frame);
            CGFloat verticalDistanceToNext = CGRectGetMidY(self.frame) - CGRectGetMidY(_nextLine.frame);

            CGFloat deltaDistance = fabsf(verticalDistanceToPrev-verticalDistanceToNext);
            CGFloat maximumRelativeLineSpaceForMergeFactor = 0.2;
            CGFloat maximumRelativeFontSizeDifferenceForMergeFactor = 0.25;
            CGFloat weigthForBoundaryOrientationFactor = 2.0;

            CGFloat fontHeightPrev = CGRectGetHeight(_prevLine.frame);
            CGFloat fontHeightSelf = CGRectGetHeight(self.frame);
            CGFloat fontHeightNext = CGRectGetHeight(_nextLine.frame);

            if (deltaDistance < maximumRelativeLineSpaceForMergeFactor && fabs(fontHeightSelf-fontHeightNext) < maximumRelativeFontSizeDifferenceForMergeFactor && fabsf(fontHeightPrev - fontHeightSelf) < maximumRelativeFontSizeDifferenceForMergeFactor) {
                _borderType = PSPDFTextLineBorderNone;
            }else if ((fabs(CGRectGetMidY(_prevLine.frame) - CGRectGetMidY(self.frame)) + weigthForBoundaryOrientationFactor * fabsf(fontHeightSelf-fontHeightPrev)) > fabsf(CGRectGetMidY(self.frame) - CGRectGetMidY(_nextLine.frame)) + weigthForBoundaryOrientationFactor * fabsf(fontHeightNext - fontHeightSelf)) {
                _borderType = PSPDFTextLineBorderTopDown;
            }else {
                _borderType = PSPDFTextLineBorderBottomUp;
            }
        }
    }
    return _borderType;
}

static inline CGFloat PSPDFTextLineDistanceY(PSPDFTextLine *line, PSPDFTextLine *otherLine) {
    if (!line || !otherLine) return CGFLOAT_MAX;
    return CGRectGetMidY(line.frame) - CGRectGetMidY(otherLine.frame);
}

inline void PSPDFSetNextLineIfCloserDistance(PSPDFTextLine *self, PSPDFTextLine *nextLine) {
    CGFloat newDistance = PSPDFTextLineDistanceY(self, nextLine);
    if (newDistance > 0 &&(!self.nextLine || newDistance < PSPDFTextLineDistanceY(self, self.nextLine))) {
        self.nextLine = nextLine;
    }
}

inline void PSPDFSetPrevLineIfCloserDistance(PSPDFTextLine *self, PSPDFTextLine *prevLine) {
    CGFloat newDistance = PSPDFTextLineDistanceY(prevLine, self);
    if (newDistance > 0 &&(!self.nextLine || newDistance < PSPDFTextLineDistanceY(self.prevLine, self))) {
        self.prevLine = prevLine;
    }
}

@end
