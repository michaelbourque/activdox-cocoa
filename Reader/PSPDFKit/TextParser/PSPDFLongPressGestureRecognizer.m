//
//  PSPDFLongPressGestureRecognizer.m
//  PSPDFKit
//
//  Copyright 2012 Peter Steinberger. All rights reserved.
//

#import "PSPDFLongPressGestureRecognizer.h"
#import <UIKit/UIGestureRecognizerSubclass.h>

@implementation PSPDFLongPressGestureRecognizer

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
	[super touchesBegan:touches withEvent:event];

	if ([self.delegate respondsToSelector:@selector(pressRecognizerShouldHandlePressImmediately:)]) {
		BOOL shouldHandleImmediately = [(id<PSPDFLongPressGestureRecognizerDelegate>)self.delegate pressRecognizerShouldHandlePressImmediately:self];
		if (shouldHandleImmediately) {
			self.state = UIGestureRecognizerStateBegan;
		}
	}
}


@end
