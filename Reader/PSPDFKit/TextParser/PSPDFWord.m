//
//  PSPDFWord.m
//  PSPDFKit
//
//  Copyright 2012 Peter Steinberger. All rights reserved.
//

#import "PSPDFWord.h"
#import "PSPDFGlyph.h"
#import "PSPDFKitGlobal.h"

@implementation PSPDFWord

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (id)initWithGlyphs:(NSArray *)wordGlyphs {
    if ((self = [super init])) {
        _glyphs = wordGlyphs;

        if ([_glyphs count] > 0) {
            CGRect firstGlyphFrame = [_glyphs[0] frame];
            CGRect lastGlyphFrame = [[_glyphs lastObject] frame];
            _frame = CGRectUnion(firstGlyphFrame, lastGlyphFrame);
        } else {
            _frame = CGRectZero;
        }
    }
	return self;
}

- (id)initWithFrame:(CGRect)wordFrame {
    if ((self = [super init])) {
        _glyphs = [NSArray new];
        _frame = wordFrame;
    }
	return self;
}

- (NSUInteger)hash {
    return ((int)_frame.origin.x ^ (int)_frame.origin.y ^ (int)_frame.size.width ^ (int)_frame.size.height) + [self.glyphs count];
}

- (BOOL)isEqual:(id)other {
    if ([other isKindOfClass:[self class]]) {
        return [self isEqualToWord:(PSPDFWord *)other];
    }
    return NO;
}

- (BOOL)isEqualToWord:(PSPDFWord *)otherWord {
    if (CGRectEqualToRect(self.frame, otherWord.frame) && [self.glyphs count] == [otherWord.glyphs count]) {
        return YES;
    }
    return NO;
}

- (NSString *)description {
    NSString *lineBreakerStr = _lineBreaker ? @" LineBreak" : @"";
	return [NSString stringWithFormat:@"<%@ %@ %@%@>", NSStringFromClass([self class]), [self stringValue], NSStringFromCGRect(_frame), lineBreakerStr];
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public

- (void)setGlyphs:(NSArray *)glyphs {
    if (glyphs != _glyphs) {
        _glyphs = [glyphs copy];
        // frame is not expected to be transformed.
        _frame = PSPDFBoundingBoxFromGlyphs(glyphs, CGAffineTransformIdentity);
    }
}

- (NSString *)stringValue {
    NSMutableString *content = [NSMutableString string];
    PSPDFGlyph *prevGlyph = nil;
	for (PSPDFGlyph *glyph in _glyphs) {
		[content appendString:glyph.content];

        // allow bigger text selections (not exactly words anymore)
        if (prevGlyph) {
            if (![glyph isOnSameLineAs:prevGlyph] && ![prevGlyph.content isEqual:@"\n"]) {
                [content appendString:@"\n"];
            }
        }
        prevGlyph = glyph;

	}
    return [NSString stringWithString:content];
}

- (BOOL)isOnSameColumn:(PSPDFWord *)glyph {
    CGRect unionRect = CGRectUnion(glyph.frame, self.frame);
    CGRect smallerFrame = self.frame.size.height < glyph.frame.size.height ? self.frame : glyph.frame;
    BOOL sameLine = (fabsf(unionRect.size.height) < fabsf(smallerFrame.size.height * 1.5));
    return sameLine;
}

// sort based on their geometric centerpoint
- (NSComparisonResult)compareByLayout:(PSPDFWord *)word {
    return CGRectGetMidX(word.frame)+CGRectGetMidY(self.frame) < CGRectGetMidY(self.frame) + CGRectGetMidX(word.frame) ? NSOrderedDescending : NSOrderedAscending;
}

- (BOOL)isOnSameLineAs:(PSPDFWord *)word {
    CGRect unionRect = CGRectUnion(word.frame, self.frame);
	CGRect smallerFrame = self.frame.size.height < word.frame.size.height ? self.frame : word.frame;
	BOOL sameLine = (fabsf(unionRect.size.height) < fabsf(smallerFrame.size.height * 1.5));
	return sameLine;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSCopying

- (id)copyWithZone:(NSZone *)zone {
    PSPDFWord *word = [[[self class] alloc] initWithGlyphs:self.glyphs];
    word.lineBreaker = self.lineBreaker;
    return word;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSCoding

- (id)initWithCoder:(NSCoder *)coder {
    NSArray *glyphs = [coder decodeObjectForKey:NSStringFromSelector(@selector(glyphs))];
    self = [self initWithGlyphs:glyphs];
    self.lineBreaker = [coder decodeBoolForKey:NSStringFromSelector(@selector(lineBreaker))];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)coder {
    [coder encodeObject:_glyphs forKey:NSStringFromSelector(@selector(glyphs))];
    [coder encodeBool:_lineBreaker forKey:NSStringFromSelector(@selector(lineBreaker))];
}

@end
