//
//  PSPDFLoupeView.m
//  PSPDFKit
//
//  Copyright 2012 Peter Steinberger. All rights reserved.
//

#import "PSPDFLoupeView.h"
#import "PSPDFWindow.h"
#import "UIImage+PSPDFKitAdditions.h"
#import "PSPDFConverter.h"
#import <QuartzCore/QuartzCore.h>

@implementation PSPDFLoupeView {
    UIWindow *_loupeWindow;
	UIView *_referenceView;
    UIImageView *_loupeBackground;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (id)initWithReferenceView:(UIView *)referenceView {
#ifdef PSPDFKIT_DISABLE_TEXT_SELECTION_FEATURES
    return nil;
#else
	if ((self = [super initWithFrame:CGRectMake(0, 0, 128, 128)])) {
		_referenceView = referenceView;
		self.backgroundColor = [UIColor clearColor];

        _loupeBackground = [[UIImageView alloc] initWithImage:[UIImage imageNamed:(_mode != PSPDFLoupeViewModeCircular) ? @"PSPDFKit.bundle/LoupeDetail" : @"PSPDFKit.bundle/Loupe"]];
        [self addSubview:_loupeBackground];
	}
	return self;
#endif
}

- (void)setMode:(PSPDFLoupeViewMode)mode {
    if (mode != _mode) {
        _mode = mode;
        _loupeBackground.image = [UIImage imageNamed:(mode != PSPDFLoupeViewModeCircular) ? @"PSPDFKit.bundle/LoupeDetail" : @"PSPDFKit.bundle/Loupe"];
    }
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UIView

#ifndef PSPDFKIT_DISABLE_TEXT_SELECTION_FEATURES
- (void)drawRect:(CGRect)rect  {
	float pixelSize = self.contentScaleFactor;

	CGContextRef c = UIGraphicsGetCurrentContext();
	CGRect frameInReferenceView = [self.superview convertRect:self.frame toView:_referenceView];
	CGPoint location = frameInReferenceView.origin;

	CGContextSaveGState(c);

	// Clip drawing
	if (_mode == PSPDFLoupeViewModeCircular) {
		CGContextAddEllipseInRect(c, CGRectMake(6, 7, 116, 114));
		CGContextClip(c);
	} else {
		CGContextAddRect(c, CGRectMake(7, 46, 114, 35));
		CGContextClip(c);
	}

	// Fill background
	CGContextSetFillColorWithColor(c, [UIColor lightGrayColor].CGColor);
	CGContextFillRect(c, self.bounds);

	CGSize contentLayerSize = (_mode == PSPDFLoupeViewModeCircular) ? CGSizeMake(100 * pixelSize, 100 * pixelSize) : CGSizeMake(_targetSize.width * pixelSize, _targetSize.height * pixelSize);
	CGLayerRef contentLayer = CGLayerCreateWithContext(c, contentLayerSize, NULL);
	CGContextRef c2 = CGLayerGetContext(contentLayer);

	// Transform CTM
    CGContextScaleCTM(c2, pixelSize, pixelSize);
	if (_mode == PSPDFLoupeViewModeCircular) {
        CGContextTranslateCTM(c2, -12, 4);
    }else {
        CGContextTranslateCTM(c2, -6, 0);
    }
	CGContextConcatCTM(c2, _referenceView.transform);

	if (_mode == PSPDFLoupeViewModeCircular) {
		CGContextTranslateCTM(c2, -location.x , -location.y - 84);
	} else if (_mode == PSPDFLoupeViewModeDetailBottom) {
		CGContextTranslateCTM(c2,
							  -location.x - 128.0/(_targetSize.width) * 0.5 * _targetSize.width + (_targetSize.width/128) * 64,
							  -location.y - 128.0/(_targetSize.height) * 0.5 * _targetSize.height - 36);
	} else if (_mode == PSPDFLoupeViewModeDetailTop) {
		CGContextTranslateCTM(c2,
							  -location.x - 128.0/(_targetSize.width) * 0.5 * _targetSize.width + (_targetSize.width/128) * 64,
							  -location.y - 128.0/(_targetSize.height) * 0.5 * _targetSize.height - 50);
	}

	// Render content
    self.hidden = YES;
    [_referenceView.layer renderInContext:c2];
    self.hidden = NO;
    
	CGRect contentLayerRect = CGRectInset(CGRectMake(5, 5, 116, 116), -15, -15);

	if (_mode != PSPDFLoupeViewModeCircular) {
        CGSize newSize = PSPDFSizeForScale(_targetSize, PSPDFScaleForSizeWithinSize(_targetSize, CGSizeMake(116, 116)));
		contentLayerRect = PSPDFAlignRectangles((CGRect){.size=newSize}, self.bounds, PSPDFRectAlignCenter);
	}

	CGContextDrawLayerInRect(c, contentLayerRect, contentLayer);
	CGLayerRelease(contentLayer);

	CGContextRestoreGState(c);
}

- (void)setCenter:(CGPoint)center {
    [super setCenter:center];
    [self setNeedsDisplay];
}

#endif

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public

- (CGAffineTransform)loupeViewMinimizedTransform {
    return CGAffineTransformConcat(CGAffineTransformMakeTranslation(0, 128), CGAffineTransformMakeScale(0.2, 0.2));
}

- (void)prepareShow {
    if (!_loupeWindow) {
        // we create a extra window so that the loupe is above the navigation and statusbar.
        _loupeWindow = [PSPDFWindow new];
        _loupeWindow.userInteractionEnabled = NO;
        _loupeWindow.hidden = NO;
        [_loupeWindow addSubview:self];
    }
}

- (void)showLoupeAnimated:(BOOL)animated {
    [self prepareShow];
    if (self.alpha == 1.0) return;

    self.transform = [self loupeViewMinimizedTransform];
    [UIView animateWithDuration:animated ? 0.2f : 0.f delay:0.f options:UIViewAnimationOptionBeginFromCurrentState | UIViewAnimationOptionAllowUserInteraction animations:^{
        self.alpha = 1.0;
        self.transform = CGAffineTransformIdentity;
    } completion:nil];
}

- (void)hideLoupeAnimated:(BOOL)animated {
    if (self.alpha == 0.0) return;

    self.transform = CGAffineTransformIdentity;
    [UIView animateWithDuration:animated ? 0.2f : 0.f delay:0.f options:UIViewAnimationOptionBeginFromCurrentState | UIViewAnimationOptionAllowUserInteraction animations:^{
        self.alpha = 0.0;
        self.transform = [self loupeViewMinimizedTransform];
    } completion:^(BOOL finished) {
        if (finished) _loupeWindow = nil;
    }];
}

@end
