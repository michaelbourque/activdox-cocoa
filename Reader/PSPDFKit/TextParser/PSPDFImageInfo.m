//
//  PSPDFImageInfo.m
//  PSPDFKit
//
//  Copyright (c) 2012 Peter Steinberger. All rights reserved.
//  Thanks to  Sorin Nistor for the initial sample code.
//

#import "PSPDFImageInfo.h"
#import "PSPDFStream.h"
#import "PSPDFDocument.h"
#import "PSPDFDocumentProvider.h"
#import "PSPDFConverter.h"

@implementation PSPDFImageInfo

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (id)init {
    if (self = [super init]) {
        _vertices = malloc(4 * sizeof(CGPoint));
    }
    return self;
}

- (void)dealloc {
    free(_vertices);
}

- (NSString *)description {
    return [NSString stringWithFormat:@"<%@: %p imageID:%@ pixelWidth:%d pixelHeight:%d bitsPerComponent:%d displayWidth:%.2f displayHeight: %.2f horizontalResolution: %.2f verticalResolution: %.2f, vertices: %@ %@ %@ %@>", NSStringFromClass([self class]), self, self.imageID, self.pixelWidth, self.pixelHeight, self.bitsPerComponent, self.displayWidth, self.displayHeight, self.horizontalResolution, self.verticalResolution, NSStringFromCGPoint(self.vertices[0]), NSStringFromCGPoint(self.vertices[1]), NSStringFromCGPoint(self.vertices[2]), NSStringFromCGPoint(self.vertices[3])];
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public

- (BOOL)isPointInImage:(CGPoint)point {
    int j = 3; // number of vertices - 1
    BOOL oddNodes = NO;

    for (int i=0; i < 4; i++) {
        if ((_vertices[i].y < point.y && _vertices[j].y >= point.y) ||
            (_vertices[j].y < point.y && _vertices[i].y >= point.y)) {
            if (_vertices[i].x + (point.y - _vertices[i].y) / (_vertices[j].y - _vertices[i].y) * (_vertices[j].x - _vertices[i].x) < point.x) {
                oddNodes = !oddNodes;
            }
        }
        j=i;
    }

    return oddNodes;
}

- (CGRect)boundingBox {
    CGPoint topLeft = CGPointZero;
    CGPoint bottomRight = CGPointZero;
    for (int i=0; i<4; i++) {
        CGPoint point = _vertices[i];
        if ( topLeft.x == 0 || topLeft.x > point.x ) topLeft.x = point.x;
        if ( topLeft.y == 0 || topLeft.y > point.y ) topLeft.y = point.y;
        if ( bottomRight.x < point.x ) bottomRight.x = point.x;
        if ( bottomRight.y < point.y ) bottomRight.y = point.y;
    }
    return CGRectMake(topLeft.x, topLeft.y, bottomRight.x - topLeft.x, bottomRight.y - topLeft.y);
}

- (UIImage *)image {
    PSPDFDocument *document = self.document;
    PSPDFDocumentProvider *provider = [document documentProviderForPage:self.page];
    CGPDFPageRef pageRef = [provider requestPageRefForPageNumber:[document pageNumberForPage:self.page]];
    CGPDFDictionaryRef pageDict = CGPDFPageGetDictionary(pageRef);
    NSString *path = [NSString stringWithFormat:@"Resources.XObject.%@", self.imageID];
    PSPDFStream *stream = PSPDFDictionaryGetStreamForPath(pageDict, path);
    UIImage *image = [stream image];
    [provider releasePageRef:pageRef];
    return image;
}

@end
