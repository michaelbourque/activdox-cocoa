//
//  PSPDFGraphicsState.m
//  PSPDFKit
//
//  Copyright 2012 Peter Steinberger. All rights reserved.
//

#import "PSPDFGraphicsState.h"
#import "PSPDFFontInfo.h"

@implementation PSPDFGraphicsState

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (id)init {
    if ((self = [super init])) {
        textMatrix = CGAffineTransformIdentity;
        lineMatrix = CGAffineTransformIdentity;
        ctm = CGAffineTransformIdentity;
        horizontalScaling = 1.0;
    }
	return self;
}

- (NSString *)description {
    return [NSString stringWithFormat:@"<%@: %p fontSize:%.2f charSpacing:%.2f wordSpacing:%.2f, leading:%.2f rise:%.2f ctm:%@ textMatrix:%@ lineMatrix:%@ renderingMode:%d>", NSStringFromClass([self class]), self, fontSize, characterSpacing, wordSpacing, leading, rise, NSStringFromCGAffineTransform(ctm), NSStringFromCGAffineTransform(textMatrix), NSStringFromCGAffineTransform(lineMatrix), renderingMode];
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSCopying

- (id)copyWithZone:(NSZone *)zone {
	PSPDFGraphicsState *copy = [PSPDFGraphicsState new];
	copy->characterSpacing = characterSpacing;
	copy->ctm = ctm;
	copy.font = _font;
	copy->fontSize = fontSize;
	copy->horizontalScaling = horizontalScaling;
	copy->leading = leading;
	copy->lineMatrix = lineMatrix;
	copy->rise = rise;
	copy->textMatrix = textMatrix;
	copy->wordSpacing = wordSpacing;
    copy->renderingMode = renderingMode;
	return copy;
}

@end
