//
//  PSPDFTextParser.m
//  PSPDFKit
//
//  Copyright 2012 Peter Steinberger. All rights reserved.
//

#import "PSPDFTextParser.h"
#import "PSPDFFontInfo.h"
#import "PSPDFGlyph.h"
#import "PSPDFWord.h"
#import "PSPDFTextLine.h"
#import "PSPDFTextBlock.h"
#import "PSPDFImageInfo.h"
#import "PSPDFGraphicsState.h"
#import "PSPDFHighlightAnnotation.h"
#import "PSPDFConverter.h"
#import "PSPDFDocument.h"
#import "PSPDFKitGlobal+Internal.h"

// uncomment to enable detailed text parser logging
//#define PSPDFTextLog(...) do { NSLog(__VA_ARGS__); }while(0)

#ifndef PSPDFTextLog
#define PSPDFTextLog(...)
#endif

// PDF Operator callbacks
void TstarCallback(CGPDFScannerRef scanner, void *userInfo);
void TcCallback(CGPDFScannerRef scanner, void *userInfo);
void TdCallback(CGPDFScannerRef scanner, void *userInfo);
void TDCallback(CGPDFScannerRef scanner, void *userInfo);
void TfCallback(CGPDFScannerRef scanner, void *userInfo);
void TjCallback(CGPDFScannerRef scanner, void *userInfo);
void TJCallback(CGPDFScannerRef scanner, void *userInfo);
void TLCallback(CGPDFScannerRef scanner, void *userInfo);
void TmCallback(CGPDFScannerRef scanner, void *userInfo);
void TsCallback(CGPDFScannerRef scanner, void *userInfo);
void TwCallback(CGPDFScannerRef scanner, void *userInfo);
void TzCallback(CGPDFScannerRef scanner, void *userInfo);
void TrCallback(CGPDFScannerRef scanner, void *userInfo);
void singleQuoteCallback(CGPDFScannerRef scanner, void *userInfo);
void doubleQuoteCallback(CGPDFScannerRef scanner, void *userInfo);
void BTCallback(CGPDFScannerRef scanner, void *userInfo);
void ETCallback(CGPDFScannerRef scanner, void *userInfo);
void cmCallback(CGPDFScannerRef scanner, void *userInfo);
void gsCallback(CGPDFScannerRef scanner, void *userInfo);
void qCallback(CGPDFScannerRef scanner, void *userInfo);
void QCallback(CGPDFScannerRef scanner, void *userInfo);
void DoCallback(CGPDFScannerRef scanner, void *userInfo);
void d0Callback(CGPDFScannerRef scanner, void *userInfo);
void d1Callback(CGPDFScannerRef scanner, void *userInfo);

void PSPDFFindXObject (const char *key, CGPDFObjectRef value, void *info);
void PSPDFAddBoundingBoxForCharacter(PSPDFTextParser *parser, uint16_t character);
void PSPDFAddString(PSPDFTextParser *parser, CGPDFStringRef pdfString);
void PSPDFParseXObjectResources(const char *key, CGPDFObjectRef value, void *info);
void PSPDFParseFont(const char *key, CGPDFObjectRef value, void *info);

@interface PSPDFXObjectStream : NSObject
@property (nonatomic, assign) CGPDFStreamRef stream;
@property (nonatomic, copy) NSString *name;
@end

@interface PSPDFTextParser() {
    dispatch_queue_t _parsingQueue;
@public
    NSMutableArray *_glyphs;
    NSMutableArray *_words;
    NSMutableArray *_images;
}
@property (nonatomic, assign) CGRect rotatedPageRect;
@property (nonatomic, assign) BOOL hideGlyphsOutsidePageRect;
@property (nonatomic, copy)   NSArray *textBlocks;
@property (nonatomic, strong) NSMutableArray *glyphs;
@property (nonatomic, strong) NSMutableArray *words;
@property (nonatomic, strong) NSMutableArray *images;
@property (atomic,    assign) BOOL wordAndColumnDetectionFinished;
@property (nonatomic, strong) NSMutableArray *graphicsStateStack;
@property (nonatomic, strong) PSPDFGraphicsState *graphicsState;
@property (nonatomic, assign) int formXObjectRecursionDepth;
@property (nonatomic, strong) NSMutableDictionary *fonts;
@property (nonatomic, assign) CGPDFPageRef pageRef;
@property (nonatomic, assign) CGAffineTransform pageRotationTransform;
@property (nonatomic, strong) NSMutableDictionary *fontCache;
@property (nonatomic, assign) NSUInteger page;
@end

@implementation PSPDFTextParser

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (id)initWithPDFPage:(CGPDFPageRef)pageRef page:(NSUInteger)page document:(PSPDFDocument *)document fontCache:(NSMutableDictionary *)fontCache hideGlyphsOutsidePageRect:(BOOL)hideGlyphsOutsidePageRect PDFBox:(CGPDFBox)PDFBox {
	if((self = [super init])) {
        _fontCache = fontCache;
        _hideGlyphsOutsidePageRect = hideGlyphsOutsidePageRect;
        self.pageRef = pageRef;
        self.page = page;
        self.document = document;
        _parsingQueue = pspdf_dispatch_queue_create("com.PSPDFKit.textParserQueue", 0);
        self.fonts = [NSMutableDictionary dictionary];
        self.glyphs = [NSMutableArray array];
        _images = [NSMutableArray new];

        self.graphicsStateStack = [NSMutableArray array];
        self.graphicsState = [PSPDFGraphicsState new];

        CGPDFDictionaryRef pageDict = CGPDFPageGetDictionary(pageRef);
        CGRect pageRect = CGPDFPageGetBoxRect(pageRef, PDFBox);
        int rotation = PSPDFNormalizeRotation(CGPDFPageGetRotationAngle(pageRef));
        _rotatedPageRect = PSPDFApplyRotationToRect(pageRect, rotation);
        _pageRotationTransform = PSPDFGetTransformFromPageRectAndRotation(_rotatedPageRect, rotation);

        if (rotation == 90) {
            _graphicsState->ctm = CGAffineTransformConcat(CGAffineTransformMakeRotation(-0.5 * M_PI),
                                                          CGAffineTransformMakeTranslation(0, pageRect.size.width + 2 * pageRect.origin.x));

        } else if (rotation == 270) {
            _graphicsState->ctm = CGAffineTransformConcat(CGAffineTransformMakeRotation(-1.5 * M_PI),
                                                          CGAffineTransformMakeTranslation(pageRect.size.height + 2 * pageRect.origin.y, 0));
        }

        bool hasParent = false;
        do {
            CGPDFDictionaryRef resources = NULL;
            CGPDFDictionaryGetDictionary(pageDict, "Resources", &resources);

            // xObjects might define fonts too
            CGPDFDictionaryRef xObject = NULL;
            if (CGPDFDictionaryGetDictionary(resources, "XObject", &xObject)) {
                CGPDFDictionaryApplyFunction(xObject, &PSPDFParseXObjectResources, (__bridge void *)self);
            }

            // those fonts take preference
            CGPDFDictionaryRef fontDict = NULL;
            CGPDFDictionaryGetDictionary(resources, "Font", &fontDict);
            CGPDFDictionaryApplyFunction(fontDict, &PSPDFParseFont, (__bridge void *)self);
            // walk up the page tree (fonts are inheritable)
            hasParent = CGPDFDictionaryGetDictionary(pageDict, "Parent", &pageDict);

        } while (hasParent);

        // finally parse content
        [self parseWithPageRef:pageRef];
    }
	return self;
}

- (NSString *)description {
    return [NSString stringWithFormat:@"<%@: %p page:%d fonts:%@ glyphs:#%d words:#%d textBlocks:#%d>", NSStringFromClass([self class]), self, self.page, self.fonts, [self.glyphs count], [self.words count], [self.textBlocks count]];
}

- (void)dealloc {
    PSPDFDispatchRelease(_parsingQueue);
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public

- (PSPDFGraphicsState *)graphicsState {
    return _graphicsState; // speed, don't retain/autorelease here.
}

- (NSString *)textWithGlyphs:(NSArray *)glyphs {
    if (![glyphs count]) return @"";

    NSUInteger minGlyphIndex = [self.glyphs count];
    NSUInteger maxGlyphIndex = 0;

    for (PSPDFGlyph *glyph in glyphs) {
        NSUInteger indexInArray = [self.glyphs indexOfObjectIdenticalTo:glyph];
        minGlyphIndex = MIN(minGlyphIndex, indexInArray);
        maxGlyphIndex = MAX(maxGlyphIndex, indexInArray);
    }

    return [self.text substringWithRange:NSMakeRange(fmaxf(minGlyphIndex, 0), fmaxf(maxGlyphIndex-minGlyphIndex, 0))];
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Static

+ (CGPDFOperatorTableRef)operatorTable {
    static dispatch_once_t onceToken;
    static CGPDFOperatorTableRef _operatorTable;
    dispatch_once(&onceToken, ^{
		_operatorTable = CGPDFOperatorTableCreate();

        // Special graphics state
        CGPDFOperatorTableSetCallback(_operatorTable, "q", qCallback); // Save the current graphics state.
		CGPDFOperatorTableSetCallback(_operatorTable, "Q", QCallback); // Restore the graphics state.
		CGPDFOperatorTableSetCallback(_operatorTable, "cm", cmCallback); // Modify current CTM by concatenating specified matrix.
		CGPDFOperatorTableSetCallback(_operatorTable, "gs", gsCallback); // Set the specified parameters in the graphics state.

        // Text objects
		CGPDFOperatorTableSetCallback(_operatorTable, "BT", BTCallback); // Begin a text object.
		CGPDFOperatorTableSetCallback(_operatorTable, "ET", ETCallback); // End a text object, discarding the text matrix.

        // Text state
		CGPDFOperatorTableSetCallback(_operatorTable, "Tc", TcCallback); // Character spacing.
		CGPDFOperatorTableSetCallback(_operatorTable, "Tw", TwCallback); // Word spacing.
		CGPDFOperatorTableSetCallback(_operatorTable, "Tz", TzCallback); // Set the horizontal scaling, Th, to (scale / 100).
        CGPDFOperatorTableSetCallback(_operatorTable, "TL", TLCallback); // Leading.
		CGPDFOperatorTableSetCallback(_operatorTable, "Tf", TfCallback); // Text font.
        CGPDFOperatorTableSetCallback(_operatorTable, "Tr", TrCallback); // Set the text rendering mode, Tmode, to render.
		CGPDFOperatorTableSetCallback(_operatorTable, "Ts", TsCallback); // Set the text rise, Trise, to rise.

        // Text positioning
		CGPDFOperatorTableSetCallback(_operatorTable, "Td", TdCallback); // Move to the start of the next. (with matrix)
		CGPDFOperatorTableSetCallback(_operatorTable, "TD", TDCallback); // Move to the start of the next .
		CGPDFOperatorTableSetCallback(_operatorTable, "Tm", TmCallback); // text matrix.
		CGPDFOperatorTableSetCallback(_operatorTable, "T*", TstarCallback); // Move to the start of the next line. (0 -Tl Td)

        // Type3 fonts (TODO)
		CGPDFOperatorTableSetCallback(_operatorTable, "d0", d0Callback); // Set width information for the glyph.
		CGPDFOperatorTableSetCallback(_operatorTable, "d1", d1Callback); // Set width and bounding box information for the glyph.

        // Text showing
		CGPDFOperatorTableSetCallback(_operatorTable, "Tj", TjCallback); // Show a text string.
		CGPDFOperatorTableSetCallback(_operatorTable, "TJ", TJCallback); // Show one or more text strings, allowing individual glyph positioning.
		CGPDFOperatorTableSetCallback(_operatorTable, "'", singleQuoteCallback);  // Move to the next line and show a text string.
		CGPDFOperatorTableSetCallback(_operatorTable, "\"", doubleQuoteCallback); // Move to the next line and show a text string.

        // XObjects
		CGPDFOperatorTableSetCallback(_operatorTable, "Do", DoCallback); // Perform XObject (Form, Image)

        // not listing for
        // - General graphics state (w, J, j, M, d, ri, i, gs)
        // - Path construction (m, l, c, v, y, h, re)
        // - Path painting (S, s, f, F, f*, B, B*, b, b*, n)
        // - Clipping paths (W, W*)
        // - Color (CS, cs, SC, SCN, sc, scn, G, g, RG, rg, K, k)
        // - Shading patterns (sh)
        // - Inline images (BI, ID, EI)
        // - Marked content (MP, DP, BMC, BDC, EMC)
        // - Compatibility (Begin/End compatibility region) (BX, EX)
    });
	return _operatorTable;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Parser

- (void)parseWithPageRef:(CGPDFPageRef)pageRef {
    @autoreleasepool {
        // parses the page, creates PSPDFGlyph's.
        CGPDFOperatorTableRef operatorTable = [[self class] operatorTable];
        CGPDFContentStreamRef contentStream = CGPDFContentStreamCreateWithPage(pageRef);
        CGPDFScannerRef scanner = CGPDFScannerCreate(contentStream, operatorTable, (__bridge void *)self);
        PSPDFTextLog(@"-- Starting parser (%@, %@, page: %d)", [self.document pageInfoForPage:self.page], self.document, self.page);
        CGPDFScannerScan(scanner);
        CGPDFScannerRelease(scanner);
        CGPDFContentStreamRelease(contentStream);
        contentStream = NULL;

        // Glyphs get numbered as they appear in the contextStream.
        {
            int i = 0;
            for (PSPDFGlyph *glyph in _glyphs) {
                glyph.indexOnPage = i;
                i++;
            }
        }

        NSArray *orderedGlyphs = _glyphs;

        // This step adds spaces and new-line (fake) PSPDFGlyph's into the stream so that it matches 1:1 with the extracted text.
        NSMutableString *mutableText = [NSMutableString string];
        __unsafe_unretained PSPDFGlyph *prevGlyph = nil; // __unsafe_unretained for performance
        // PSPDFGlyph *prevPrevGlyph = nil;
        NSMutableArray *processedGlyphs = [NSMutableArray array];
        for (PSPDFGlyph *glyph in orderedGlyphs) {
            if ([glyph.content length] == 0) continue;

            // remove shadow characters (overlapping glyphs)
            if (prevGlyph) {
                CGRect unionRect = CGRectUnion(glyph.frame, prevGlyph.frame);
                CGRect intersectionRect = CGRectIntersection(glyph.frame, prevGlyph.frame);
                if (CGRectGetWidth(intersectionRect) > CGRectGetWidth(glyph.frame)/2) {
                    // this looks like a shadow, skip...
                    prevGlyph = glyph;
                    continue;
                }

                CGRect smallerFrame = glyph.frame.size.height < prevGlyph.frame.size.height ? glyph.frame : prevGlyph.frame;
                BOOL sameLine = (fabsf(unionRect.size.height) < fabsf(smallerFrame.size.height * 1.5));
                if (!sameLine) {
                    [mutableText appendString:@"\n"]; // line break
                    PSPDFGlyph *lineBreakGlyph = [[PSPDFGlyph alloc] init];
                    lineBreakGlyph.frame = CGRectMake(prevGlyph.frame.origin.x + prevGlyph.frame.size.width, prevGlyph.frame.origin.y, 0, prevGlyph.frame.size.height);
                    lineBreakGlyph.content = @"\n";
                    lineBreakGlyph.font = prevGlyph.font;
                    lineBreakGlyph.indexOnPage = -1;
                    [processedGlyphs addObject:lineBreakGlyph];
                }
                if (!sameLine && unionRect.size.height > prevGlyph.frame.size.height * 3) {
                    [mutableText appendString:@"\n"]; // new paragraph
                    PSPDFGlyph *lineBreakGlyph = [[PSPDFGlyph alloc] init];
                    lineBreakGlyph.frame = CGRectMake(glyph.frame.origin.x, glyph.frame.origin.y, 0, glyph.frame.size.height);
                    lineBreakGlyph.content = @"\n";
                    lineBreakGlyph.font = prevGlyph.font;
                    lineBreakGlyph.indexOnPage = -1;
                    [processedGlyphs addObject:lineBreakGlyph];
                }
                // TODO: Make whitespace detection more accurate (use average/median spacing?)
                // a sequence of 'll' or 'il' etc. can trigger false positives at the moment.
                CGFloat spaceBetweenGlyphs = CGRectGetMinX(glyph.frame) - CGRectGetMaxX(prevGlyph.frame);
                CGFloat maxGlyphsWidth = MAX(CGRectGetWidth(prevGlyph.frame), CGRectGetWidth(glyph.frame))/4;

                // width seems wrong!
                // TODO: try some median with widths?
                BOOL ignoreSpaces = spaceBetweenGlyphs/maxGlyphsWidth > 100;

                if (sameLine && spaceBetweenGlyphs > maxGlyphsWidth && !ignoreSpaces) {
                    [mutableText appendString:@" "];
                    PSPDFGlyph *spaceGlyph = [[PSPDFGlyph alloc] init];
                    CGRect spaceFrame = CGRectMake(prevGlyph.frame.origin.x + prevGlyph.frame.size.width, prevGlyph.frame.origin.y, glyph.frame.size.width / 3, prevGlyph.frame.size.height);
                    spaceGlyph.frame = spaceFrame;
                    spaceGlyph.content = @" ";
                    spaceGlyph.font = glyph.font;
                    spaceGlyph.indexOnPage = -1;
                    [processedGlyphs addObject:spaceGlyph];
                }
            }
            [processedGlyphs addObject:glyph];
            [mutableText appendString:glyph.content];

            // for font ligatures or other characters that correspond to more than one string item, add filler glyphs.
            if ([glyph.content length] > 1) {
                for (int i=0; i < [glyph.content length]-1; i++) {
                    PSPDFGlyph *fillerGlyph = [[PSPDFGlyph alloc] init];
                    fillerGlyph.frame = CGRectMake(glyph.frame.origin.x + glyph.frame.size.width, glyph.frame.origin.y, 0, glyph.frame.size.height);
                    fillerGlyph.content = @"";
                    fillerGlyph.font = glyph.font;
                    fillerGlyph.indexOnPage = -1;
                    [processedGlyphs addObject:fillerGlyph];
                }
            }

            //prevPrevGlyph = prevGlyph; // DEBUG
            prevGlyph = glyph;
        }

        self.text = [NSString stringWithString:mutableText];
        self.glyphs = processedGlyphs;

        if ([self.text length] != [self.glyphs count]) {
            PSPDFLogWarning(@"Discrepancy between glyph count (%d) and text (%d) found. This is not good...", [self.glyphs count], [self.text length]);
        }
        PSPDFTextLog(@"-- Finished parsing page %d", self.page);
    }
}

- (void)parseTextBlocks {
    pspdf_dispatch_sync_reentrant(_parsingQueue, ^{
        if (!self.wordAndColumnDetectionFinished) {
            // grow the words into line segments
            NSMutableArray *lineSegments = [NSMutableArray array];

            NSMutableArray *currentLine = [NSMutableArray array];
            [lineSegments addObject:currentLine];
            PSPDFGlyph *prevGlyph = nil;

            // can be slow (CFSimpleMergeSort)
            NSArray *orderedGlyphs = [_glyphs sortedArrayUsingSelector:@selector(compareByLayout:)];
            for (PSPDFGlyph *glyph in orderedGlyphs) {
                if ([glyph.content isEqualToString:@"\n"]) {
                    prevGlyph.lineBreaker = YES;
                    continue;
                }
                if (prevGlyph) {
                    if (![glyph isOnSameLineSegmentAs:prevGlyph]) {
                        currentLine = [NSMutableArray array];
                        [lineSegments addObject:currentLine];
                    }
                }
                [currentLine addObject:glyph];
                prevGlyph = glyph;
            }

            // convert lines to words (for now...)
            NSMutableArray *lineWords = [NSMutableArray array];
            for (NSArray *line in lineSegments) {
                PSPDFTextLine *textLine = [[PSPDFTextLine alloc] initWithGlyphs:line];
                [lineWords addObject:textLine];
            }

            // sort lines in top-down and left-right based on geometric center point.
            [lineWords sortUsingSelector:@selector(compareByLayout:)];

            // set nearest lines above/below
            for (PSPDFTextLine *textLine in lineWords) {
                for (PSPDFTextLine *secondTextLine in lineWords) {
                    if (textLine == secondTextLine) continue;

                    CGFloat unionWidth = CGRectUnion(textLine.frame, secondTextLine.frame).size.width;
                    CGFloat overlapWidth = unionWidth - fabsf(CGRectGetMinX(textLine.frame) - CGRectGetMinX(secondTextLine.frame)) - fabsf(CGRectGetMaxX(textLine.frame) - CGRectGetMaxX(secondTextLine.frame));
                    if (overlapWidth > 0) {
                        PSPDFSetNextLineIfCloserDistance(textLine, secondTextLine);
                        PSPDFSetPrevLineIfCloserDistance(secondTextLine, textLine);
                    }
                }
            }

            // grow single lines into text blocks (by marking them with .bid)
            int blockID = 0;
            for(int i=0 ; i<[lineWords count] ; i++) {
                if ([lineWords[i] blockID] >= 0) continue;
                PSPDFGrowTextBlock(lineWords, i, blockID);
                blockID++;
            }

            // Sort the lines after bid number.
            NSArray *sortedLines = [lineWords sortedArrayWithOptions:0 usingComparator:^NSComparisonResult(id obj1, id obj2) {
                return [obj1 blockID] > [obj2 blockID];
            }];

#if 0
            NSLog(@"\n\n\n\n\n\n\n");
            for (PSPDFTextLine *textLine in lineWords) {
                NSLog(@"[%d](%d) %@ (next: '%@' prev: '%@')", [lineWords indexOfObject:textLine], textLine.borderType, [textLine stringValue], PSPDFPreviewString([textLine.nextLine stringValue]), PSPDFPreviewString([textLine.prevLine stringValue]));
            }
#endif

            // form text blocks from the marked lines
            blockID = 0;
            NSMutableArray *textBlocks = [NSMutableArray array];
            NSMutableArray *glyphsInBlock = [NSMutableArray array];
            @autoreleasepool {
                for (PSPDFTextLine *textLine in sortedLines) {
                    if (textLine.blockID != blockID) {
                        blockID = textLine.blockID;
                        PSPDFTextBlock *textBlock = [[PSPDFTextBlock alloc] initWithGlyphs:glyphsInBlock];
                        [textBlocks addObject:textBlock];
                        [glyphsInBlock removeAllObjects];
                    }
                    // set like breaker to not merge words between blocks
                    ((PSPDFGlyph *)[textLine.glyphs lastObject]).lineBreaker = YES;
                    [glyphsInBlock addObjectsFromArray:textLine.glyphs];
                }
                // add last object
                if ([glyphsInBlock count] > 0) {
                    PSPDFTextBlock *newTextBlock = [[PSPDFTextBlock alloc] initWithGlyphs:glyphsInBlock];
                    [textBlocks addObject:newTextBlock];
                }
            }

            // apply additional heurisics that merge single-line characters with the next block (text block starters)
            @autoreleasepool {
                for (int i=0; i< [textBlocks count]; i++) {
                    PSPDFTextBlock *block = textBlocks[i];
                    PSPDFTextBlock *nextBlock = i+1 < [textBlocks count] ? textBlocks[i+1] : nil;
                    if ([block.content length] == 1 && [nextBlock.content length] > 10) {
                        NSArray *mergedGlyphs = [block.glyphs arrayByAddingObjectsFromArray:nextBlock.glyphs];
                        PSPDFTextBlock *mergedBlock = [[PSPDFTextBlock alloc] initWithGlyphs:mergedGlyphs];
                        textBlocks[i] = mergedBlock;
                        [textBlocks removeObjectAtIndex:i+1];
                        PSPDFLogVerbose(@"(Column Find Optimizations) Merged block %@ with %@.", block, nextBlock);
                    }
                }
            }

            // merge blocks that are inside bigger blocks.
            // stop if text block count is crazy high!
            if ([textBlocks count] < 100) {
                // performance tweak: use set that does pointer equal.
                NSMutableSet *removedBlocks = PSPDFCreateMutablePointerCompareSet();
                for (int i=0; i<[textBlocks count]; i++) {
                    PSPDFTextBlock *block = textBlocks[i];
                    for (int j=0; j<[textBlocks count]; j++) {
                        if (i == j) continue;
                        PSPDFTextBlock *innerBlock = textBlocks[j];
                        if (![removedBlocks containsObject:block] && ![removedBlocks containsObject:innerBlock]) {
                            if (CGRectContainsRect(block.frame, innerBlock.frame)) {
                                block.glyphs = [block.glyphs arrayByAddingObjectsFromArray:innerBlock.glyphs];
                                [removedBlocks addObject:innerBlock];
                            }
                        }
                    }
                }
                [removedBlocks enumerateObjectsUsingBlock:^(id obj, BOOL *stop) {
                    [textBlocks removeObject:obj];
                }];
            }

            // sort by layout
            // commented out - it's better for most documents that we simply rely on the PDF ordering.
            /*
             [textBlocks sortUsingComparator:^NSComparisonResult(id obj1, id obj2) {
             return CGRectGetMinX([obj1 frame])*5+CGRectGetMinY([obj2 frame]) < CGRectGetMinY([obj1 frame]) + CGRectGetMinX([obj2 frame])*5 ? NSOrderedAscending : NSOrderedDescending;
             }];
             */
            _textBlocks = textBlocks;

            // fill the word array, sorted.
            NSMutableArray *words = [NSMutableArray array];
            for (PSPDFTextBlock *textBlock in textBlocks) {
                [words addObjectsFromArray:textBlock.words];
            }
            self.words = words;

#if 0
            // DEBUG - print text blocks
            NSLog(@"\n\n\n\n\n\n\n");
            for (PSPDFTextBlock *textBlock in textBlocks) {
                NSLog(@"%@\n---------------------------------\n", textBlock);
            }
#endif
            self.wordAndColumnDetectionFinished = YES;
        }
    });
}

- (void)ensureTextBlocksAreParsed {
    if (!self.wordAndColumnDetectionFinished) {
        [self parseTextBlocks];
    }
}

- (NSMutableArray *)words {
    [self ensureTextBlocksAreParsed];
    return _words;
}

- (NSArray *)textBlocks {
    [self ensureTextBlocksAreParsed];
    return _textBlocks;
}

static bool PSPDFKitTextLinesShouldMerge(PSPDFTextLine *topLine, PSPDFTextLine *bottomLine) {
    if (!topLine || !bottomLine) {
        return false;
    }

    if (topLine.borderType == PSPDFTextLineBorderNone && bottomLine.borderType == PSPDFTextLineBorderNone) {
        return true;
    }else if (topLine.borderType != PSPDFTextLineBorderNone && bottomLine.borderType != PSPDFTextLineBorderNone) {
        // both are boundaries
        BOOL merge = topLine.borderType == PSPDFTextLineBorderTopDown && bottomLine.borderType == PSPDFTextLineBorderBottomUp;
        if (merge) {
            return true;
        }else {
            return false;
        }
    }else {
        // one is boundary, one isn't.
        return topLine.borderType == PSPDFTextLineBorderTopDown || bottomLine.borderType == PSPDFTextLineBorderBottomUp;
    }
}

static inline void PSPDFGrowTextBlock(NSArray *lines, int seed, int blockID) {
    NSMutableArray *queue = [NSMutableArray arrayWithObject:@(seed)];
    ((PSPDFTextLine *)lines[seed]).blockID = blockID;
    while ([queue count]) {
        int i = [queue[0] intValue];
        [queue removeObjectAtIndex:0];

        PSPDFTextLine *referenceLine = lines[i];
        while (referenceLine.nextLine.blockID < 0 && PSPDFKitTextLinesShouldMerge(referenceLine, referenceLine.nextLine)) {
            referenceLine.nextLine.blockID = blockID;
            [queue addObject:@([lines indexOfObjectIdenticalTo:referenceLine.nextLine])];
            referenceLine = referenceLine.nextLine;
        }
        referenceLine = lines[i];
        while (referenceLine.prevLine.blockID < 0 && PSPDFKitTextLinesShouldMerge(referenceLine.prevLine, referenceLine)) {
            referenceLine.prevLine.blockID = blockID;
            [queue addObject:@([lines indexOfObjectIdenticalTo:referenceLine.prevLine])];
            referenceLine = referenceLine.prevLine;
        }
    }
}

- (NSString *)textForHighlightAnnotation:(PSPDFHighlightAnnotation *)annotation {
    NSMutableSet *highlightedGlyphs = PSPDFCreateMutablePointerCompareSet();
    for (NSValue *rectValue in annotation.rects) {
        CGRect rect = [rectValue CGRectValue];
        for (PSPDFGlyph *glyph in self.glyphs) {
            CGRect glyphFrame = glyph.frame;
            if (CGRectIntersectsRect(glyphFrame, rect)) {
                [highlightedGlyphs addObject:glyph];
            }
        }
    }
    NSArray *orderedGlyphs = [[highlightedGlyphs allObjects] sortedArrayUsingSelector:@selector(compareByLayout:)];
    NSMutableString *result = [NSMutableString string];
    for (PSPDFGlyph *glyph in orderedGlyphs) {
        [result appendString:glyph.content];
    }
    return [NSString stringWithString:result];
}

@end

// show the first 20 characters if any
static inline NSString *PSPDFPreviewString(NSString *string) {
    return string ? [string substringToIndex:MIN(20, [string length])] : nil;
}

static inline CGAffineTransform PSPDFParseTransformMatrix(CGPDFScannerRef scanner) {
    CGFloat a, b, c, d, tx, ty;
    if (CGPDFScannerPopNumber(scanner, &ty) && CGPDFScannerPopNumber(scanner, &tx) && CGPDFScannerPopNumber(scanner, &d) && CGPDFScannerPopNumber(scanner, &c) && CGPDFScannerPopNumber(scanner, &b) && CGPDFScannerPopNumber(scanner, &a)) {
        return CGAffineTransformMake(a, b, c, d, tx, ty);
    }
    return CGAffineTransformIdentity;
}

void PSPDFParseXObjectResources(const char *key, CGPDFObjectRef value, void *info) {
    CGPDFStreamRef object = NULL;
    BOOL isStream = CGPDFObjectGetValue(value, kCGPDFObjectTypeStream, &object);
    if (isStream) {
        CGPDFDictionaryRef dict = CGPDFStreamGetDictionary(object);
        CGPDFDictionaryRef resources = NULL;
        BOOL hasResources = CGPDFDictionaryGetDictionary(dict, "Resources", &resources);
        if (hasResources) {
            CGPDFDictionaryRef fontInfo = NULL;
            BOOL hasFontInfo = CGPDFDictionaryGetDictionary(resources, "Font", &fontInfo);
            if (hasFontInfo) {
                CGPDFDictionaryApplyFunction(fontInfo, &PSPDFParseFont, info);
            }
            CGPDFDictionaryRef xObject = NULL;
            if (CGPDFDictionaryGetDictionary(resources, "XObject", &xObject)) {
                // Other XObject found, parsing recursively...
                CGPDFDictionaryApplyFunction(xObject, &PSPDFParseXObjectResources, info);
            }
        }
    }
}

void PSPDFFindXObject(const char *key, CGPDFObjectRef value, void *info) {
    PSPDFXObjectStream *xObjectStream = (__bridge PSPDFXObjectStream *)info;
    CGPDFStreamRef stream = NULL;
    BOOL isStream = CGPDFObjectGetValue(value, kCGPDFObjectTypeStream, &stream);
    NSString *name = [[NSString alloc] initWithUTF8String:key];
    if (isStream) {
        if ([name isEqual:xObjectStream.name]) {
            // Found the object we want
            xObjectStream.stream = stream;
        } else {
            CGPDFDictionaryRef dict = CGPDFStreamGetDictionary(stream);
            CGPDFDictionaryRef resources = NULL;
            BOOL hasResources = CGPDFDictionaryGetDictionary(dict, "Resources", &resources);
            if (hasResources) {
                CGPDFDictionaryRef xObject = NULL;
                BOOL hasXObject = CGPDFDictionaryGetDictionary(resources, "XObject", &xObject);
                if (hasXObject) {
                    // Other XObject found, parsing recursively...
                    CGPDFDictionaryApplyFunction(xObject, &PSPDFFindXObject, info);
                }
            }
        }
    }
}

void PSPDFParseFont(const char *key, CGPDFObjectRef value, void *info) {
    if (!key) {
        PSPDFLogWarning(@"Font key cannot be nil"); return;
    }
    PSPDFTextParser *parser = (__bridge PSPDFTextParser *)info;
    NSString *fontIdentifier = @(key);

    CGPDFDictionaryRef font = NULL;
    if (CGPDFObjectGetValue(value, kCGPDFObjectTypeDictionary, &font)) {
        // first check fontCache
        PSPDFFontInfo *fontInfo = (parser.fontCache)[BOXED(font)];
        if (!fontInfo) {
            fontInfo = [[PSPDFFontInfo alloc] initWithFontDictionary:font];
            if (fontInfo) parser.fontCache[BOXED(font)] = fontInfo;
        }
        if (fontInfo) (parser.fonts)[fontIdentifier] = fontInfo;
    }
}

inline void PSPDFAddBoundingBoxForCharacter(PSPDFTextParser *parser, uint16_t character) {
    PSPDFFontInfo *font = parser.graphicsState.font;
    CGFloat width = [font widthForCharacter:character] / 1000.0;

    CGFloat height = font.ascent / 1000.0 - font.descent / 1000.0;
    CGRect unscaledFrame = CGRectMake(0, font.descent / 1000.0, width, height);

    // workaround for certain Type3 fonts, not sure if this is a good idea
    if (unscaledFrame.size.height == 0) {
        unscaledFrame.size.height = 1.0;
    }

    PSPDFGraphicsState *graphicsState = parser.graphicsState;
    CGAffineTransform textMatrix = graphicsState->textMatrix;
    CGFloat fontSize = graphicsState->fontSize;
    CGFloat horizontalScaling = graphicsState->horizontalScaling;
    CGFloat rise = graphicsState->rise;
    CGAffineTransform textTransform = CGAffineTransformMake(fontSize * horizontalScaling, 0, 0, fontSize, 0, rise);
    CGAffineTransform renderingMatrix = CGAffineTransformConcat(textTransform, textMatrix);
    renderingMatrix = CGAffineTransformConcat(renderingMatrix, graphicsState->ctm);
    CGRect letterFrame = CGRectApplyAffineTransform(unscaledFrame, renderingMatrix);

    // text that is not visible should not be extracted.
    if (parser.hideGlyphsOutsidePageRect && !CGRectContainsRect(parser.rotatedPageRect, letterFrame)) {
        PSPDFLogVerbose(@"skipping glyph at %@", NSStringFromCGRect(letterFrame));
    }else {
        PSPDFGlyph *glyph = [[PSPDFGlyph alloc] init];
        glyph.frame = letterFrame;
        PSPDFFontInfo *currentFont = graphicsState.font;
        glyph.font = currentFont;
        NSArray *encodingArray = [currentFont encodingArray];
        if (encodingArray && [encodingArray count] > character) {
            glyph.content = encodingArray[character];
        } else {
            if (currentFont.toUnicodeMap) {
                NSString *unicode = (currentFont.toUnicodeMap)[@(character)];
                if (unicode) {
                    glyph.content = unicode;
                } else {
                    unicode = [NSString stringWithCharacters:&character length:1];
                    glyph.content = unicode;
                }
            } else {
                glyph.content = [NSString stringWithCharacters:&character length:1];
            }
        }
        [parser->_glyphs addObject:glyph];
    }

    CGFloat spacing = graphicsState->characterSpacing;
    if (character == 32) spacing += graphicsState->wordSpacing; // 32 = space character.
    CGFloat tx = (width * fontSize + spacing) * horizontalScaling;
    CGAffineTransform translatedTextMatrix = CGAffineTransformTranslate(textMatrix, tx, 0);
    graphicsState->textMatrix = translatedTextMatrix;
}

void PSPDFAddString(PSPDFTextParser *parser, CGPDFStringRef pdfString)  {
    const unsigned char *bytes = CGPDFStringGetBytePtr(pdfString);
    size_t length = CGPDFStringGetLength(pdfString);

    if (![parser.graphicsState.font isMultiByteFont]) {
        for (int i=0; i<length; i++) {
            PSPDFAddBoundingBoxForCharacter(parser, bytes[i]);
        }
    }
    else {
        for (int i=0; i<length; i+=2) {
            unsigned char byte1 = bytes[i];
            unsigned char byte2 = bytes[i+1];
            uint16_t cid = (byte1 * 256) + byte2; // interpret as big-endian
            PSPDFAddBoundingBoxForCharacter(parser, cid);
        }
    }
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - PDF Operator callbacks

void d0Callback(CGPDFScannerRef scanner, void *userInfo) {
    PSPDFTextLog(@"d0 Set width information for the glyph and declare that the glyph description specifies both its shape and its colour.");

    // wx denotes the horizontal displacement in the glyph coordinate system; it shall be consistent with the corresponding width in the font’s Widths array. wy shall be 0 (see 9.2.4, "Glyph Positioning and Metrics").
    // This operator shall only be permitted in a content stream appearing in a Type 3 font’s CharProcs dictionary. It is typically used only if the glyph description executes operators to set the colour explicitly.

    // PSPDFTextParser *parser = (__bridge PSPDFTextParser *)userInfo;
    // PSPDFGraphicsState *graphicsState = parser.graphicsState;
    // TODO! Page 260ff
}

void d1Callback(CGPDFScannerRef scanner, void *userInfo) {
    PSPDFTextLog(@"d1 Set width and bounding box information for the glyph and declare that the glyph description specifies only shape, not colour.");
    // wx denotes the horizontal displacement in the glyph coordinate system; it shall be consistent with the corresponding width in the font’s Widths array. wy shall be 0 (see 9.2.4, "Glyph Positioning and Metrics").
    // llx and lly denote the coordinates of the lower-left corner, and urx and ury denote the upper-right corner, of the glyph bounding box.
    // The glyph bounding box is the smallest rectangle, oriented with the axes of the glyph coordinate system, that completely encloses all marks placed on the page as a result of executing the glyph’s description. The declared bounding box shall be correct—in other words, sufficiently large to enclose the entire glyph. If any marks fall outside this bounding box, the result is unpredictable.
    // A glyph description that begins with the d1 operator should not execute any operators that set the colour (or other colour-related parameters) in the graphics state; any use of such operators shall be ignored. The glyph description is executed solely to determine the glyph’s shape. Its colour shall be determined by the graphics state in effect each time this glyph is painted by a text-showing operator. For the same reason, the glyph description shall not include an image; however, an image mask is acceptable, since it merely defines a region of the page to be painted with the current colour.
    // This operator shall be used only in a content stream appearing in a Type 3 font’s CharProcs dictionary.

    // PSPDFTextParser *parser = (__bridge PSPDFTextParser *)userInfo;
    // PSPDFGraphicsState *graphicsState = parser.graphicsState;
    // TODO! Page 260ff
}

void TstarCallback(CGPDFScannerRef scanner, void *userInfo) {
    PSPDFTextLog(@"T* Move to start of next text line");

    // Move to the start of the next line. This operator has the same effect as the code
    // 0 -Tl Td
    // where Tl denotes the current leading parameter in the text state. The
    // negative of Tl is used here because Tl is the text leading expressed as a
    // positive number. Going to the next line entails decreasing the y coordinate.

    PSPDFTextParser *parser = (__bridge PSPDFTextParser *)userInfo;
    PSPDFGraphicsState *graphicsState = parser.graphicsState;

    CGFloat x = 0.0f;
    CGFloat y = -graphicsState->leading;

    CGAffineTransform t = CGAffineTransformIdentity;
    t.tx = x;
    t.ty = y;

    CGAffineTransform lineMatrix = graphicsState->lineMatrix;

    CGAffineTransform newMatrix = CGAffineTransformConcat(t, lineMatrix);
    graphicsState->textMatrix = newMatrix;
    graphicsState->lineMatrix = newMatrix;
}

void TcCallback(CGPDFScannerRef scanner, void *userInfo) {
    CGFloat spacing = 0.0;
    if (!CGPDFScannerPopNumber(scanner, &spacing)) return;
    PSPDFTextLog(@"Tc Set character spacing to %.2f", spacing);

    PSPDFTextParser *parser = (__bridge PSPDFTextParser *)userInfo;
    parser.graphicsState->characterSpacing = spacing;
}

void TdCallback(CGPDFScannerRef scanner, void *userInfo) {
    CGFloat x, y;
    if (!CGPDFScannerPopNumber(scanner, &y)) return;
    if (!CGPDFScannerPopNumber(scanner, &x)) return;

    PSPDFTextLog(@"Td Move text position to %.2f/%.2f", x, y);

    PSPDFTextParser *parser = (__bridge PSPDFTextParser *)userInfo;
    PSPDFGraphicsState *graphicsState = parser.graphicsState;

    CGAffineTransform t = CGAffineTransformIdentity;
    t.tx = x;
    t.ty = y;

    CGAffineTransform newMatrix = CGAffineTransformConcat(t, graphicsState->lineMatrix);
    graphicsState->textMatrix = newMatrix;
    graphicsState->lineMatrix = newMatrix;
}

void TDCallback(CGPDFScannerRef scanner, void *userInfo) {
    CGFloat x, y;
    if (!CGPDFScannerPopNumber(scanner, &y)) return;
    if (!CGPDFScannerPopNumber(scanner, &x)) return;
    PSPDFTextLog(@"TD Move text position and set leading: %.2f/%.2f", x, y);

    PSPDFTextParser *parser = (__bridge PSPDFTextParser *)userInfo;
    PSPDFGraphicsState *graphicsState = parser.graphicsState;

    CGAffineTransform t = CGAffineTransformIdentity;
    t.tx = x;
    t.ty = y;

    CGAffineTransform lineMatrix = graphicsState->lineMatrix;
    CGAffineTransform newMatrix = CGAffineTransformConcat(t, lineMatrix);
    graphicsState->textMatrix = newMatrix;
    graphicsState->lineMatrix = newMatrix;
    graphicsState->leading = -y;
}

// Set the text font, Tf, to font and the text font size, Tfs, to size. font shall be the name of a font resource in the Font subdictionary of the current resource dictionary; size shall be a number representing a scale factor. There is no initial value for either font or size; they shall be specified explicitly by using Tf before any text is shown.
void TfCallback(CGPDFScannerRef scanner, void *userInfo) {
    CGFloat fontSize;
    if (!CGPDFScannerPopNumber(scanner, &fontSize)) return;

    const char *f;
    if (!CGPDFScannerPopName(scanner, &f)) return;
    NSString *fontID = @(f);

    PSPDFTextLog(@"Tf Set text font %@ and size %.2f", fontID, fontSize);

    PSPDFTextParser *parser = (__bridge PSPDFTextParser *)userInfo;
    PSPDFGraphicsState *graphicsState = parser.graphicsState;
    graphicsState.font = (parser.fonts)[fontID];
    graphicsState->fontSize = fontSize;
}

void TjCallback(CGPDFScannerRef scanner, void *userInfo) {
    CGPDFStringRef pdfString = NULL;
    if (!CGPDFScannerPopString(scanner, &pdfString)) return;
    PSPDFTextLog(@"Tj Show text: %@", CFBridgingRelease(CGPDFStringCopyTextString(pdfString)));

    PSPDFTextParser *parser = (__bridge PSPDFTextParser *)userInfo;
    PSPDFAddString(parser, pdfString);
}

void TJCallback(CGPDFScannerRef scanner, void *userInfo) {
    PSPDFTextLog(@"TJ Show text, allowing individual glyph positioning");
    PSPDFTextParser *parser = (__bridge PSPDFTextParser *)userInfo;
    PSPDFGraphicsState *graphicsState = parser.graphicsState;

    CGPDFArrayRef array;
    if (!CGPDFScannerPopArray(scanner, &array)) return;
    CGFloat offset = 0.0;
    for(int i=0; i<CGPDFArrayGetCount(array); i++) {
        CGPDFStringRef pdfString;
        bool isString = CGPDFArrayGetString(array, i, &pdfString);
        if (isString) {
            PSPDFTextLog(@"-> string: %@", CFBridgingRelease(CGPDFStringCopyTextString(pdfString)));
            const unsigned char *bytes = CGPDFStringGetBytePtr(pdfString);
            size_t length = CGPDFStringGetLength(pdfString);
            if (![graphicsState.font isMultiByteFont]) {
                for (int j=0; j<length; j++) {
                    PSPDFAddBoundingBoxForCharacter(parser, bytes[j]);
                }
            }
            else {
                for (int j=0; j<length; j+=2) {
                    unsigned char byte1 = bytes[j];
                    unsigned char byte2 = bytes[j+1];
                    uint16_t cid = (byte1 * 256) + byte2; //interpret as big-endian
                    PSPDFAddBoundingBoxForCharacter(parser, cid);
                }
            }
        }
        else {
            CGPDFArrayGetNumber(array, i, &offset);
            PSPDFTextLog(@"-> offset: %.2f", offset);
            CGAffineTransform textMatrix = graphicsState->textMatrix;
            textMatrix = CGAffineTransformTranslate(textMatrix, -offset/1000.0 * graphicsState->fontSize, 0);
            graphicsState->textMatrix = textMatrix;
        }
    }
}

// Set the text leading, Tl, to leading, which shall be a number expressed in unscaled text space units. Text leading shall be used only by the T*, ', and " operators. Initial value: 0.
void TLCallback(CGPDFScannerRef scanner, void *userInfo) {
    CGFloat leading = 0.0;
    if (!CGPDFScannerPopNumber(scanner, &leading)) return;
    PSPDFTextLog(@"TL Set text leading to %.2f", leading);

    PSPDFTextParser *parser = (__bridge PSPDFTextParser *)userInfo;
    parser.graphicsState->leading = leading;
}

// Set the text matrix, Tm, and the text line matrix, Tlm:
void TmCallback(CGPDFScannerRef scanner, void *userInfo) {
    PSPDFTextParser *parser = (__bridge PSPDFTextParser *)userInfo;
    PSPDFGraphicsState *graphicsState = parser.graphicsState;
    CGAffineTransform matrix = PSPDFParseTransformMatrix(scanner);

    PSPDFTextLog(@"Tm Set text matrix and text line matrix: %@", NSStringFromCGAffineTransform(matrix));
    graphicsState->textMatrix = matrix;
    graphicsState->lineMatrix = matrix;
}

// // Set the text rise, Trise, to rise, which shall be a number expressed in unscaled text space units. Initial value: 0.
void TsCallback(CGPDFScannerRef scanner, void *userInfo) {
    CGFloat rise = 0.0;
    if (!CGPDFScannerPopNumber(scanner, &rise)) return;
    PSPDFTextLog(@"Ts Set text rise: %.2f", rise);

    PSPDFTextParser *parser = (__bridge PSPDFTextParser *)userInfo;
    parser.graphicsState->rise = rise;
}

void TwCallback(CGPDFScannerRef scanner, void *userInfo) {
    CGFloat spacing = 0.0;
    if (!CGPDFScannerPopNumber(scanner, &spacing)) return;
    PSPDFTextLog(@"Tw Set word spacing: %.2f", spacing);

    PSPDFTextParser *parser = (__bridge PSPDFTextParser *)userInfo;
    parser.graphicsState->wordSpacing = spacing;
}

// Set the horizontal scaling, Th, to (scale / 100). scale shall be a number specifying the percentage of the normal width. Initial value: 100 (normal width).
void TzCallback(CGPDFScannerRef scanner, void *userInfo) {
    CGFloat scaling = 0.0;
    if (!CGPDFScannerPopNumber(scanner, &scaling)) return;
    PSPDFTextLog(@"Tz Set horizontal text scaling: %.2f", scaling);

    PSPDFTextParser *parser = (__bridge PSPDFTextParser *)userInfo;
    parser.graphicsState->horizontalScaling = scaling/100.0;
}

// Set the text rendering mode, Tmode, to render, which shall be an integer. Initial value: 0.
void TrCallback(CGPDFScannerRef scanner, void *userInfo) {
    CGPDFInteger mode;
    if (!CGPDFScannerPopInteger(scanner, &mode)) return;
    PSPDFTextLog(@"Tr Set text rendering mode, Tmode: %ld", mode);
    PSPDFTextParser *parser = (__bridge PSPDFTextParser *)userInfo;
    parser.graphicsState->renderingMode = mode;
}

// Move to the next line and show a text string.
void singleQuoteCallback(CGPDFScannerRef scanner, void *userInfo) {
    PSPDFTextLog(@"'  Move to next line and show text");
    // This operator shall have the same effect as the code T* string Tj
    TstarCallback(scanner, userInfo);
    TjCallback(scanner, userInfo);
}

void doubleQuoteCallback(CGPDFScannerRef scanner, void *userInfo) {
    // Move to the next line and show a text string, using aw as the word spacing and ac as the character spacing
    // (setting the corresponding parameters in the text state). aw and ac shall be numbers expressed in unscaled
    // text space units. This operator shall have the same effect as this code:
    // aw Tw
    // ac Tc
    // string '

    PSPDFTextParser *parser = (__bridge PSPDFTextParser *)userInfo;
    PSPDFGraphicsState *graphicsState = parser.graphicsState;

    CGPDFStringRef pdfString = NULL;
    CGPDFReal ac = 0.0; CGPDFReal aw = 0.0;
    CGPDFScannerPopString(scanner, &pdfString);
    CGPDFScannerPopNumber(scanner, &ac);
    CGPDFScannerPopNumber(scanner, &aw);
    PSPDFTextLog(@"Set word and character spacing (%.2f %.2f), move to next line, and show text: %@", ac, aw, pdfString);

    graphicsState->wordSpacing = aw;
    graphicsState->characterSpacing = ac;
    TstarCallback(scanner, userInfo);
    PSPDFAddString(parser, pdfString);
}

// Begin a text object, initializing the text matrix, Tm, and the text line matrix, Tlm , to the identity matrix. Text objects shall not be nested; a second BT shall not appear before an ET.
void BTCallback(CGPDFScannerRef scanner, void *userInfo) {
    PSPDFTextParser *parser = (__bridge PSPDFTextParser *)userInfo;
    PSPDFGraphicsState *graphicsState = parser.graphicsState;
    PSPDFTextLog(@"BT Begin text object. (ctm: %@)", NSStringFromCGAffineTransform(graphicsState->ctm));

    graphicsState->textMatrix = CGAffineTransformIdentity;
    graphicsState->lineMatrix = CGAffineTransformIdentity;
}

// End a text object, discarding the text matrix.
void ETCallback(CGPDFScannerRef scanner, void *userInfo) {
    PSPDFTextParser *parser = (__bridge PSPDFTextParser *)userInfo;
    PSPDFGraphicsState *graphicsState = parser.graphicsState;
    PSPDFTextLog(@"ET End text object (ctm: %@)", NSStringFromCGAffineTransform(graphicsState->ctm));

    graphicsState->textMatrix = CGAffineTransformIdentity;
    graphicsState->lineMatrix = CGAffineTransformIdentity;
}

// Modify the current transformation matrix (CTM) by concatenating the specified matrix (see 8.3.2, "Coordinate Spaces"). Although the operands specify a matrix, they shall be written as six separate numbers, not as an array.
void cmCallback(CGPDFScannerRef scanner, void *userInfo) {
    PSPDFTextParser *parser = (__bridge PSPDFTextParser *)userInfo;
    PSPDFGraphicsState *graphicsState = parser.graphicsState;
    CGAffineTransform matrix = PSPDFParseTransformMatrix(scanner);
    graphicsState->ctm = CGAffineTransformConcat(matrix, graphicsState->ctm);
    PSPDFTextLog(@"cm Concatenate matrix to current transformation matrix: %@ -> %@", NSStringFromCGAffineTransform(matrix), NSStringFromCGAffineTransform(graphicsState->ctm));
}

// (PDF 1.2) Set the specified parameters in the graphics state. dictName shall be the name of a graphics state parameter dictionary in the ExtGState subdictionary of the current resource dictionary (see the next sub-clause).
void gsCallback(CGPDFScannerRef scanner, void *userInfo) {
    //PSPDFTextParser *parser = (__bridge PSPDFTextParser *)userInfo;
    //PSPDFGraphicsState *graphicsState = parser.graphicsState;
    //CGAffineTransform matrix = PSPDFParseTransformMatrix(scanner);
    //graphicsState->ctm = CGAffineTransformConcat(graphicsState->ctm, matrix);
    PSPDFTextLog(@"gs Set the specified parameters in the graphics state.");
    // TODO can change font!
}

void qCallback(CGPDFScannerRef scanner, void *userInfo) {
    PSPDFTextParser *parser = (__bridge PSPDFTextParser *)userInfo;
    PSPDFTextLog(@"q  Save graphics state (%d->%d)", [parser.graphicsStateStack count], [parser.graphicsStateStack count]+1);
    PSPDFGraphicsState *graphicsStateCopy = [parser.graphicsState copy];
    [parser.graphicsStateStack addObject:graphicsStateCopy];
}

void QCallback(CGPDFScannerRef scanner, void *userInfo) {
    PSPDFTextParser *parser = (__bridge PSPDFTextParser *)userInfo;
    PSPDFTextLog(@"Q  Restore graphics state (%d->%d)", [parser.graphicsStateStack count], [parser.graphicsStateStack count] > 0 ? [parser.graphicsStateStack count]-1 : 0);
    if ([parser.graphicsStateStack count] == 0) { PSPDFTextLog(@"-> No state to restore, stack empty!"); }
    PSPDFGraphicsState *restoredGraphicsState = [parser.graphicsStateStack lastObject];
    if (restoredGraphicsState) {
        parser.graphicsState = restoredGraphicsState;
        [parser.graphicsStateStack removeLastObject];
    }
}

void DoCallback(CGPDFScannerRef scanner, void *userInfo) {
    const char *name; // stream name
    if (!CGPDFScannerPopName(scanner, &name)) return;
    PSPDFTextLog(@"Do Invoke named XObject: %s -----------------------------------------------------------------------------", name);

    PSPDFTextParser *parser = (__bridge PSPDFTextParser *)userInfo;
    CGAffineTransform ctm = parser.graphicsState->ctm;

    //CGPDFDictionaryRef pageDict = CGPDFPageGetDictionary(parser.pageRef);
    //CGPDFDictionaryRef resources = NULL;
    //CGPDFDictionaryGetDictionary(pageDict, "Resources", &resources);

    //CGPDFDictionaryRef xObjectDictionary = NULL;
    //CGPDFDictionaryGetDictionary(resources, "XObject", &xObjectDictionary);

    CGPDFContentStreamRef cs = CGPDFScannerGetContentStream(scanner);
    CGPDFObjectRef imageOrFormObject = CGPDFContentStreamGetResource(cs, "XObject", name);
    CGPDFStreamRef xObjectStream;

    if (CGPDFObjectGetValue(imageOrFormObject, kCGPDFObjectTypeStream, &xObjectStream)) {
        CGPDFDictionaryRef xObjectDictionary = CGPDFStreamGetDictionary(xObjectStream);

        // get stream subtype
        const char *subtype = NULL;
        BOOL hasSubtype = CGPDFDictionaryGetName(xObjectDictionary, "Subtype", &subtype);
        if (hasSubtype && strcmp(subtype, "Image") == 0) {
            PSPDFImageInfo *imageInfo = [[PSPDFImageInfo alloc] init];
            imageInfo.imageID = @(name);

            // Transform the image coordinates into page coordinates based on current transformation matrix.
            ctm = CGAffineTransformConcat(ctm, parser.pageRotationTransform);
            imageInfo.vertices[0] = CGPointApplyAffineTransform(CGPointMake(0, 0), ctm); // lower left
            imageInfo.vertices[1] = CGPointApplyAffineTransform(CGPointMake(1, 0), ctm); // lower right
            imageInfo.vertices[2] = CGPointApplyAffineTransform(CGPointMake(1, 1), ctm); // upper right
            imageInfo.vertices[3] = CGPointApplyAffineTransform(CGPointMake(0, 1), ctm); // upper left

            CGPDFInteger pixelWidth;
            if (CGPDFDictionaryGetInteger(xObjectDictionary, "Width", &pixelWidth)) {
                imageInfo.pixelWidth = pixelWidth;
            }
            CGPDFInteger pixelHeight;
            if (CGPDFDictionaryGetInteger(xObjectDictionary, "Height", &pixelHeight)) {
                imageInfo.pixelHeight = pixelHeight;
            }

            CGPDFInteger bitsPerComponent;
            if (CGPDFDictionaryGetInteger(xObjectDictionary, "BitsPerComponent", &bitsPerComponent)) {
                imageInfo.bitsPerComponent = bitsPerComponent;
            }

            // Vertices 0 and 1 define the horizontal, vertices 1 and 2 define the vertical.
            imageInfo.displayWidth = sqrt((imageInfo.vertices[0].x - imageInfo.vertices[1].x) * (imageInfo.vertices[0].x - imageInfo.vertices[1].x) +
                                          (imageInfo.vertices[0].y - imageInfo.vertices[1].y) * (imageInfo.vertices[0].y - imageInfo.vertices[1].y));
            imageInfo.displayHeight = sqrt((imageInfo.vertices[1].x - imageInfo.vertices[2].x) * (imageInfo.vertices[1].x - imageInfo.vertices[2].x) +
                                           (imageInfo.vertices[1].y - imageInfo.vertices[2].y) * (imageInfo.vertices[1].y - imageInfo.vertices[2].y));

            imageInfo.horizontalResolution = abs(imageInfo.pixelWidth * 72 / imageInfo.displayWidth);
            imageInfo.verticalResolution = abs(imageInfo.pixelHeight * 72 / imageInfo.displayHeight);

            imageInfo.document = parser.document;
            imageInfo.page = parser.page;

            [parser->_images addObject:imageInfo];
            PSPDFTextLog(@"-> Image found: %@", imageInfo);
        }else if (hasSubtype && strcmp(subtype, "Form") == 0) {
            PSPDFTextLog(@"-> Form found.");

            // build stream object and find stream
            PSPDFXObjectStream *xObject = [[PSPDFXObjectStream alloc] init];
            xObject.name = [[NSString alloc] initWithUTF8String:name];
            xObject.stream = NULL;
            CGPDFDictionaryApplyFunction(xObjectDictionary, &PSPDFFindXObject, (__bridge void *)xObject);

            if (!xObject.stream) {
                xObject.stream = xObjectStream;
            }

            if (xObject.stream != NULL) {
                CGPDFDictionaryRef streamDictionary = CGPDFStreamGetDictionary(xObject.stream);
                if (parser.formXObjectRecursionDepth <= 4 && hasSubtype && strcmp(subtype, "Form") == 0) {

                    // push new graphics state
                    PSPDFGraphicsState *graphicsStateCopy = [parser.graphicsState copy];
                    [parser.graphicsStateStack addObject:graphicsStateCopy];

                    // Form XObject may have their own matrix that is concatenated with the current transformation matrix before the form XObject is drawn.
                    CGPDFArrayRef matrixArray;
                    if (CGPDFDictionaryGetArray(xObjectDictionary, "Matrix", &matrixArray)) {
                        if (CGPDFArrayGetCount(matrixArray) == 6) {
                            CGPDFReal m11 = -1, m12 = -1, m21 = -1, m22 = -1, tx = -1, ty = -1;
                            if (CGPDFArrayGetNumber(matrixArray, 0, &m11) &&
                                CGPDFArrayGetNumber(matrixArray, 1, &m12) &&
                                CGPDFArrayGetNumber(matrixArray, 2, &m21) &&
                                CGPDFArrayGetNumber(matrixArray, 3, &m22) &&
                                CGPDFArrayGetNumber(matrixArray, 4, &tx) &&
                                CGPDFArrayGetNumber(matrixArray, 5, &ty)) {
                                CGAffineTransform matrix = CGAffineTransformMake(m11, m12, m21, m22, tx, ty);
                                graphicsStateCopy->ctm = CGAffineTransformConcat(ctm, matrix);
                                PSPDFTextLog(@"-> Concatenating form matrix: %@, new state: %@", NSStringFromCGAffineTransform(matrix), NSStringFromCGAffineTransform(graphicsStateCopy->ctm));
                            }
                        }
                    }

                    // Avoid cyclic references of Form XObjects
                    parser.formXObjectRecursionDepth += 1;
                    PSPDFTextLog(@"-> Invoking XForm (depth: %d)", parser.formXObjectRecursionDepth);
                    CGPDFContentStreamRef localContent = CGPDFContentStreamCreateWithStream(xObject.stream, streamDictionary, CGPDFScannerGetContentStream(scanner));
                    CGPDFOperatorTableRef table = [[parser class] operatorTable];
                    CGPDFScannerRef localScanner = CGPDFScannerCreate(localContent, table, userInfo);
                    CGPDFScannerScan(localScanner);
                    CGPDFScannerRelease(localScanner);
                    CGPDFContentStreamRelease(localContent);
                    
                    parser.formXObjectRecursionDepth -= 1;
                    
                    // restore graphics state
                    PSPDFGraphicsState *restoredGraphicsState = [parser.graphicsStateStack lastObject];
                    parser.graphicsState = restoredGraphicsState;
                    [parser.graphicsStateStack removeLastObject];
                }
            } else {
                PSPDFLogVerbose(@"XObject %s not found", name);
            }
        }else {
            if (hasSubtype) {
                PSPDFLogVerbose(@"Subtype %s unknown.",  subtype);
            }
        }
    }
    
    PSPDFTextLog(@"Do  Finished invoking named XObject: %s ------------------------------------------------------------------------", name);
}

@implementation PSPDFXObjectStream @end
