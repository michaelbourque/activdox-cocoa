//
//  PSPDFGlobalLock.m
//  PSPDFKit
//
//  Copyright 2011-2012 Peter Steinberger. All rights reserved.
//

#import "PSPDFKit.h"
#import "PSPDFGlobalLock.h"
#import "PSPDFKitGlobal+Internal.h"
#import "PSPDFDocumentProvider.h"
#import <libkern/OSAtomic.h>

@interface PSPDFGlobalLock() {
    NSUInteger        _pageNumber;
    CGPDFPageRef      _pageRef;
    NSLock           *_pdfGlobalLock;
    OSSpinLock        _cacheAccessLock;
}
@property (nonatomic, strong) PSPDFDocumentProvider *documentProvider;
@property (nonatomic, assign, getter=isClearCacheRequested) BOOL clearCacheRequested;
@end

@implementation PSPDFGlobalLock

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Static

+ (PSPDFGlobalLock *)sharedGlobalLock {
    __strong static PSPDFGlobalLock *_sharedGlobalLock = nil;
    static dispatch_once_t pred;
    dispatch_once(&pred, ^{ _sharedGlobalLock = [[self class] new]; });
    return _sharedGlobalLock;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Private

- (void)clearCacheNoLock:(BOOL)forced {
    if (self.isClearCacheRequested || forced) {
        if (!forced) {
            PSPDFLog(@"Clear Cache was requested.");
        }
        _clearCacheRequested = NO;
        if (_pageRef) {
            [_documentProvider releasePageRef:_pageRef];
        }
        _pageRef = nil;
        _pageNumber = 0;
        self.documentProvider = nil;
    }
}

// internal - do not call from outside
- (void)clearCache:(BOOL)forced {
    OSSpinLockLock(&_cacheAccessLock);
    [self clearCacheNoLock:forced];
    OSSpinLockUnlock(&_cacheAccessLock);
}

// only enter after locking state!
- (CGPDFPageRef)openPDFWithDocument:(PSPDFDocument *)document page:(NSUInteger)page error:(NSError **)error {
    OSSpinLockLock(&_cacheAccessLock);
    PSPDFDocumentProvider *documentProvider = [document documentProviderForPage:page];
    NSUInteger pageNumber = [document pageNumberForPage:page];
    CGPDFPageRef pageRef = [documentProvider requestPageRefForPageNumber:pageNumber error:error];
    [self clearCacheNoLock:YES];
    self.documentProvider = documentProvider;

    _pageRef = pageRef;
    _pageNumber = pageNumber;
    OSSpinLockUnlock(&_cacheAccessLock);
    return pageRef;
}

- (void)requestClearCache {
    // on old devices, wait. they can't deal with the memory pressure
    BOOL shouldWait = PSPDFIsCrappyDevice();
    [self requestClearCacheAndWait:shouldWait];
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (id)init {
    if ((self = [super init])) {
        _pdfGlobalLock = [NSLock new];
        [_pdfGlobalLock setName:@"PDFGlobalLock"];
        NSNotificationCenter *dnc = [NSNotificationCenter defaultCenter];
        [dnc addObserver:self selector:@selector(requestClearCache) name:UIApplicationDidReceiveMemoryWarningNotification object:nil];
    }
    return self;
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [self clearCache:YES];
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public

- (void)requestClearCacheAndWait:(BOOL)waitFlag {
    BOOL allowedToClearCache = NO;
    
    // try lock if waiting is set to NO
    if (!waitFlag) {
        if (![_pdfGlobalLock tryLock]) {
            PSPDFLogVerbose(@"currently locked, requesting clear later!");
            _clearCacheRequested = YES;
        }else {
            allowedToClearCache = YES;
        }
    }else {
        // really wait
        PSPDFLogVerbose(@"waiting for clearcache...");
        [_pdfGlobalLock lock];
        allowedToClearCache = YES;
    }
    
    if (allowedToClearCache) {
        PSPDFLogVerbose(@"clearing internal pdf cache now!");
        [self clearCache:YES];
        [_pdfGlobalLock unlock];
    }
}

- (CGPDFPageRef)tryLockWithDocument:(PSPDFDocument *)document page:(NSUInteger)page error:(NSError **)error {
    BOOL isLocked = [_pdfGlobalLock tryLock];
    if (isLocked) {
        PSPDFLogVerbose(@"tryLock successful");
        CGPDFPageRef pageRef = [self openPDFWithDocument:document page:page error:error];
        if (!pageRef) {
            [_pdfGlobalLock unlock];
        }
        return pageRef;
    }else {
        PSPDFLogVerbose(@"tryLock failed to aquire lock");
        return nil;
    }
}

- (CGPDFPageRef)lockWithDocument:(PSPDFDocument *)document page:(NSUInteger)page error:(NSError **)error {
    [_pdfGlobalLock lock];
    return [self openPDFWithDocument:document page:page error:error];
}

- (void)freeWithPDFPageRef:(CGPDFPageRef)pdfPage {
    PSPDFLogVerbose(@"unlocking page");
    if (pdfPage != _pageRef) {
        PSPDFLogError(@"Returned invalid pdfPage! Only global lock can create pdf references here!");
    }
    
    // page, doc is release here if we run low on mem
    [self clearCache:NO];
    [_pdfGlobalLock unlock];
}

- (void)lockGlobal {
    PSPDFLogVerbose(@"Applying global lock");
    [_pdfGlobalLock lock];
    PSPDFLogVerbose(@"Applying global lock... done");
}

- (void)unlockGlobal {
    PSPDFLogVerbose(@"Freeing global lock");
    [_pdfGlobalLock unlock];
    PSPDFLogVerbose(@"Freeing global lock... done");
}

@end
