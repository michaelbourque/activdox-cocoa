//
//  PSPDFStream.m
//  PSPDFKit
//
//  Copyright (c) 2012 Peter Steinberger. All rights reserved.
//

#import "PSPDFStream.h"
#import "PSPDFDocument.h"
#import "PSPDFKitGlobal.h"
#import "PSPDFConverter.h"
#include <sys/mman.h>
#include <sys/fcntl.h>
#import <dlfcn.h>

@implementation PSPDFStream

static NSString *PSPDFDataFormatToString(CGPDFDataFormat dataFormat) {
    switch(dataFormat) {
        case CGPDFDataFormatRaw: return @"Raw";
        case CGPDFDataFormatJPEGEncoded: return @"JPEGEncoded";
        case CGPDFDataFormatJPEG2000: return @"JPEG2000";
        default: return @"Unknown";
    }
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (id)initWithStream:(CGPDFStreamRef)stream {
    if ((self = [super init])) {
        _stream = stream;
    }
    return self;
}

- (NSString *)description {
    @autoreleasepool {
        if ([self dataLength] < 40*1024*1024) {
            CGPDFDataFormat format;
            NSData *data = CFBridgingRelease(CGPDFStreamCopyData(_stream, &format));

            /*
            if (format == CGPDFDataFormatJPEGEncoded) {
                NSURL *URL = PSPDFTempFileURLWithPathExtension(@"image", @"jpg");
                UIImage *image = [self image];
                if (image) {
                    NSData *imageData = UIImageJPEGRepresentation(image, 0.9);
                    [imageData writeToURL:URL atomically:YES];
                    NSLog(@"%@", [URL path]);
                }
            }
             */

            return([NSString stringWithFormat:@"<%@: %p format: %@, length: %d, dictionary: %@>", NSStringFromClass([self class]), self,  PSPDFDataFormatToString(format), data.length, self.dictionary]);
        }else {
            return [NSString stringWithFormat:@"<%@: %p dictionary: %@>", NSStringFromClass([self class]), self, self.dictionary];
        }
    }
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public

- (NSDictionary *)dictionary {
    CGPDFDictionaryRef streamDictRef = CGPDFStreamGetDictionary(self.stream);
    NSDictionary *streamDict = PSPDFConvertPDFDictionary(streamDictRef);
    return streamDict;
}

- (size_t)dataLength {
    return [[self dictionary][@"Length"] integerValue];
}

- (NSData *)dataUsingFormat:(CGPDFDataFormat *)dataFormat {
    return CFBridgingRelease(CGPDFStreamCopyData(self.stream, dataFormat));
}

- (NSURL *)fileURLWithAssetName:(NSString *)assetName document:(PSPDFDocument *)document page:(NSUInteger)page {
    NSString *extension = [assetName pathExtension] ?: @"";
    NSString *cleanedAssetName = [[[assetName stringByDeletingPathExtension] componentsSeparatedByCharactersInSet:[[NSCharacterSet letterCharacterSet] invertedSet]] componentsJoinedByString:@""];
    NSString *path = [NSString stringWithFormat:@"%@_%@_%d.%@", cleanedAssetName, document.UID, page, extension];
    return [self fileURLWithName:path];
}

- (NSURL *)fileURLWithName:(NSString *)fileName {
    // create PSPDFKit temp path
    NSString *pspdfKitTempPath = [NSTemporaryDirectory() stringByAppendingPathComponent:@"PSPDFKit"];
    [[NSFileManager new] createDirectoryAtPath:pspdfKitTempPath withIntermediateDirectories:NO attributes:nil error:NULL];
    NSString *path = [pspdfKitTempPath stringByAppendingPathComponent:fileName];
    return [self fileURLWithPath:path];
}

- (NSURL *)fileURLWithPath:(NSString *)path {
    BOOL success = NO;

    @synchronized(self) {
        // if file exists, just relay it.
        if ([[NSFileManager new] fileExistsAtPath:path]) return [NSURL fileURLWithPath:path];

        // Get stream size
        size_t length = [self dataLength];

        BOOL shouldDoSequentialCopy = length > 10*1024*1024; // copy sequential for > 10MB sets.
        BOOL canDoStreamCopyData    = length > 40*1024*1024; // more than 40MB will crash the device.

#ifndef _PSPDFKIT_DONT_USE_OBFUSCATED_PRIVATE_API_
        if (shouldDoSequentialCopy) {

            // dynamically link CGPDFStreamGetData (private API) via opening the lib.
            // simply using "extern" could result in a crash on launch if Apple ever removes this API.
            // Although it's highly unlikely CGPDFStreamGetData ever gets changed.
            static CFStringRef (*streamGetData)(CGPDFStreamRef stream, void *data, int length) = nil;
            static dispatch_once_t onceToken;
            dispatch_once(&onceToken, ^{
                NSString *root = @"";
                PSPDF_IF_SIMULATOR(root = [[NSProcessInfo processInfo] environment][@"IPHONE_SIMULATOR_ROOT"];)
                void *CoreGraphics = dlopen([[root stringByAppendingPathComponent:@"/System/Library/Frameworks/CoreGraphics.framework/CoreGraphics"] fileSystemRepresentation], RTLD_LAZY);
                if (CoreGraphics) streamGetData = dlsym(CoreGraphics, [[NSString stringWithFormat:@"CGPDF%@ata", @"StreamGetD"] cStringUsingEncoding:NSUTF8StringEncoding]);
            });

            // use mmap to directly copy the stream data into a file.
            if (streamGetData) {
                int fileDescriptor = 0;
                if ((fileDescriptor = open([path UTF8String], O_RDWR|O_CREAT|O_TRUNC , S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP|S_IROTH|S_IWOTH)) < 0) {
                    PSPDFLogError(@"can't create %@ for writing", path); return nil;
                }
                void *destination = NULL;
                if ((destination = mmap(0, length, PROT_READ|PROT_WRITE, MAP_SHARED, fileDescriptor, 0)) == (caddr_t) -1) {
                    PSPDFLogError(@"mmap error for output"); goto cleanup;
                }
                // go to the location corresponding to the last byte
                if (lseek(fileDescriptor, length - 1, SEEK_SET) == -1) {
                    PSPDFLogError(@"lseek error"); goto cleanup;
                }
                // write a dummy byte at the last location
                if (write(fileDescriptor, "", 1) != 1) {
                    PSPDFLogError(@"write error"); goto cleanup;
                }
                // finally copy stream -> directly writes into file.
                if (!streamGetData(self.stream, destination, length)) {
                    PSPDFLogWarning(@"Failed to get stream data.");
                }else {
                    success = YES;
                }
            cleanup:
                if (fileDescriptor > 0) close(fileDescriptor);
                if (destination) munmap(destination, length);
            }
        }
#endif

        // fallback if private API usage is disabled or we just handle a small stream.
        if (!success && canDoStreamCopyData) {
            // Public API version: needs a lot of memory, only use for smaller streams.
            NSData *data = CFBridgingRelease(CGPDFStreamCopyData(self.stream, NULL));
            if (data) {
                [data writeToFile:path atomically:YES];
                success = YES;
            }
        }
    }
    return success ? [NSURL fileURLWithPath:path] : nil;
}

- (UIImage *)image {
    return PSPDFGetImageFromCGPDFStream(self.stream);
}


CGFloat *PSPDFCreateDecodedValuesFromImageDictionary(CGPDFDictionaryRef dict, CGColorSpaceRef cgColorSpace, NSInteger bitsPerComponent);
CGColorSpaceRef PSPDFCreateColorSpaceFromPDFArray(CGPDFArrayRef colorSpaceArray);

static UIImage *PSPDFGetImageFromCGPDFStream(CGPDFStreamRef myStream) {
    CGPDFArrayRef colorSpaceArray = NULL;
    CGPDFStreamRef dataStream;
    CGPDFDataFormat format;
    CGPDFDictionaryRef dict;
    CGPDFInteger width, height, bps, spp = 3;
    CGPDFBoolean interpolation = 0;
    CGColorSpaceRef cgColorSpace = NULL;
    const char *name = NULL, *colorSpaceName = NULL, *renderingIntentName = NULL;
    CFDataRef imageDataPtr = NULL;
    CGImageRef cgImage = NULL;
    CGImageRef sourceImage = NULL;
    CGDataProviderRef dataProvider;
    CGColorRenderingIntent renderingIntent;
    CGFloat *decodeValues = NULL;
    UIImage *image;

    if (myStream == NULL) return nil;

    dataStream = myStream;
    dict = CGPDFStreamGetDictionary(dataStream);

    // obtain basic image information
    if (!CGPDFDictionaryGetName(dict, "Subtype", &name))
        return nil;

    if (strcmp(name, "Image") != 0)
        return nil;

    if (!CGPDFDictionaryGetInteger(dict, "Width", &width))
        return nil;

    if (!CGPDFDictionaryGetInteger(dict, "Height", &height))
        return nil;

    if (!CGPDFDictionaryGetInteger(dict, "BitsPerComponent", &bps))
        return nil;

    if (!CGPDFDictionaryGetBoolean(dict, "Interpolate", &interpolation))
        interpolation = NO;

    if (!CGPDFDictionaryGetName(dict, "Intent", &renderingIntentName))
        renderingIntent = kCGRenderingIntentDefault;
    else {
        renderingIntent = kCGRenderingIntentDefault;
        //      renderingIntent = renderingIntentFromName(renderingIntentName);
    }

    imageDataPtr = CGPDFStreamCopyData(dataStream, &format);
    dataProvider = CGDataProviderCreateWithCFData(imageDataPtr);
    CFRelease(imageDataPtr);

    if (CGPDFDictionaryGetArray(dict, "ColorSpace", &colorSpaceArray)) {
        //cgColorSpace = CGColorSpaceCreateDeviceRGB();
        cgColorSpace = PSPDFCreateColorSpaceFromPDFArray(colorSpaceArray);
        spp = CGColorSpaceGetNumberOfComponents(cgColorSpace);
    } else if (CGPDFDictionaryGetName(dict, "ColorSpace", &colorSpaceName)) {
        if (strcmp(colorSpaceName, "DeviceRGB") == 0) {
            cgColorSpace = CGColorSpaceCreateDeviceRGB();
            spp = 3;
        } else if (strcmp(colorSpaceName, "DeviceCMYK") == 0) {
            cgColorSpace = CGColorSpaceCreateDeviceCMYK();
            spp = 4;
        } else if (strcmp(colorSpaceName, "DeviceGray") == 0) {
            cgColorSpace = CGColorSpaceCreateDeviceGray();
            spp = 1;
        } else if (bps == 1) { // if there's no colorspace entry, there's still one we can infer from bps
            cgColorSpace = CGColorSpaceCreateDeviceGray();
            spp = 1;
        } else {
            // fallback to RGB
            cgColorSpace = CGColorSpaceCreateDeviceRGB();
            spp = 3;
        }
    }
    // stop if there's no colorSpace.
    if (!cgColorSpace) {
        CGDataProviderRelease(dataProvider);
        return nil;
    }
    decodeValues = PSPDFCreateDecodedValuesFromImageDictionary(dict, cgColorSpace, bps);
    
    int rowBits = bps * spp * width;
    int rowBytes = rowBits / 8;
    // pdf image row lengths are padded to byte-alignment
    if (rowBits % 8 != 0)
        ++rowBytes;

    if (format == CGPDFDataFormatRaw) {
        sourceImage = CGImageCreate(width, height, bps, bps * spp, rowBytes, cgColorSpace, 0, dataProvider, decodeValues, interpolation, renderingIntent);
        cgImage = sourceImage;
    } else {
        if (format == CGPDFDataFormatJPEGEncoded) { // JPEG data requires a CGImage
            sourceImage = CGImageCreateWithJPEGDataProvider(dataProvider, decodeValues, interpolation, renderingIntent);
            cgImage = sourceImage;
        }
        // note that we could have handled JPEG with ImageIO as well
        else if (format == CGPDFDataFormatJPEG2000) { // JPEG2000 requires ImageIO
            CFDictionaryRef dictionary = CFDictionaryCreate(NULL, NULL, NULL, 0, NULL, NULL);
            sourceImage = CGImageCreateWithJPEGDataProvider(dataProvider, decodeValues, interpolation, renderingIntent);
            cgImage = sourceImage;
            CFRelease(dictionary);
        } // some format we don't know about or an error in the PDF
    }
    CGDataProviderRelease(dataProvider); // no crash on nil
    if (cgImage) image = [UIImage imageWithCGImage:cgImage];
    if (sourceImage) CGImageRelease(sourceImage);
    if (decodeValues) free(decodeValues);
    if (cgColorSpace) CFRelease(cgColorSpace);

    return image;
}

CGColorSpaceRef PSPDFCreateColorSpaceFromPDFArray(CGPDFArrayRef colorSpaceArray) {
    CGColorSpaceRef     cgColorSpace = NULL, alternateColorSpace = NULL;
    CGPDFStreamRef      stream;
    const char         *colorSpaceName = NULL, *alternateColorSpaceName = NULL;
    CGPDFInteger        numberOfComponents = 0;
    CGPDFDictionaryRef  dict;
    CGFloat            *range;
    CGPDFArrayRef       rangeArray;

    if (CGPDFArrayGetName(colorSpaceArray, 0, &colorSpaceName)) {
        if (strcmp(colorSpaceName, "ICCBased") == 0) {
            if (CGPDFArrayGetStream(colorSpaceArray, 1, &stream)) {
                dict = CGPDFStreamGetDictionary(stream);

                // First obtain the alternate color space if present
                if (CGPDFDictionaryGetName(dict, "Alternate",  &alternateColorSpaceName)) {
                    if (strcmp(alternateColorSpaceName, "DeviceRGB") == 0) {
                        alternateColorSpace = CGColorSpaceCreateDeviceRGB();
                    } else if (strcmp(alternateColorSpaceName, "DeviceGray") == 0) {
                        alternateColorSpace = CGColorSpaceCreateDeviceGray();
                    } else if (strcmp(alternateColorSpaceName, "DeviceCMYK") == 0) {
                        alternateColorSpace = CGColorSpaceCreateDeviceCMYK();
                    }
                }

                // Obtain the preferential color space
                CGPDFDataFormat dataFormat;
                CFDataRef colorSpaceDataPtr = CGPDFStreamCopyData(stream, &dataFormat);

                if (dataFormat == CGPDFDataFormatRaw) {
                    CGDataProviderRef profile = CGDataProviderCreateWithCFData(colorSpaceDataPtr);
                    CGPDFDictionaryGetInteger(dict, "N", &numberOfComponents);

                    // Deduce an alternate color space if we don't have one already
                    if (alternateColorSpace == NULL) {
                        switch (numberOfComponents) {
                            case 1: alternateColorSpace = CGColorSpaceCreateDeviceGray(); break;
                            case 3: alternateColorSpace = CGColorSpaceCreateDeviceRGB(); break;
                            case 4: alternateColorSpace = CGColorSpaceCreateDeviceCMYK(); break;
                            default: break;
                        }
                    }

                    range = malloc(numberOfComponents * 2 * sizeof(CGFloat));
                    if (!CGPDFDictionaryGetArray(dict, "Range", &rangeArray)) {
                        for (int i = 0; i < numberOfComponents * 2; i += 2) {
                            range[i] = (i % 2 == 0) ? 0.0 : 1.0;
                        }
                    } else {
                        size_t count = CGPDFArrayGetCount(rangeArray);
                        for (int i = 0; i < count; i++) {
                            CGPDFArrayGetNumber(rangeArray, i, &range[i]);
                        }
                    }

                    cgColorSpace = CGColorSpaceCreateICCBased(numberOfComponents, range, profile, alternateColorSpace);
                    CGDataProviderRelease(profile);
                    free(range);
                    if (!cgColorSpace) {
                        cgColorSpace = CGColorSpaceRetain(alternateColorSpace);
                    }

                } else if (dataFormat == CGPDFDataFormatJPEGEncoded) {
                    //
                } else if (dataFormat == CGPDFDataFormatJPEG2000) {
                    //
                }
                CGColorSpaceRelease(alternateColorSpace); // no crash on nil
                if (colorSpaceDataPtr) CFRelease(colorSpaceDataPtr);
            }
        } else if (strcmp(colorSpaceName, "Indexed") == 0) {
            CGColorSpaceRef baseSpace = NULL;
            CGPDFArrayRef    base = NULL;
            CGPDFInteger    highValue = 0;
            CGPDFStringRef    string;
            const unsigned char *chars = NULL;
            const char        *namedColorSpaceName;

            if (CGPDFArrayGetArray(colorSpaceArray, 1, &base)) {
                baseSpace = PSPDFCreateColorSpaceFromPDFArray(base);
            } else if (CGPDFArrayGetName(colorSpaceArray, 1, &namedColorSpaceName)) {
                if (strcmp(namedColorSpaceName, "DeviceRGB") == 0) {
                    baseSpace = CGColorSpaceCreateDeviceRGB();
                } else if (strcmp(namedColorSpaceName, "DeviceGray") == 0) {
                    baseSpace = CGColorSpaceCreateDeviceGray();
                } else if (strcmp(namedColorSpaceName, "DeviceCMYK") == 0) {
                    baseSpace = CGColorSpaceCreateDeviceCMYK();
                }
            }
            CGPDFArrayGetInteger(colorSpaceArray, 2, &highValue);

            CFDataRef streamData = NULL;
            if (CGPDFArrayGetStream(colorSpaceArray, 3, &stream)) {
                streamData = CGPDFStreamCopyData(stream, NULL);
                chars = CFDataGetBytePtr(streamData);
            } else if (CGPDFArrayGetString(colorSpaceArray, 3, &string)) {
                chars = CGPDFStringGetBytePtr(string);
            } else {
                // TODO: Raise some error state?
            }
            cgColorSpace = CGColorSpaceCreateIndexed(baseSpace, highValue, chars);
            CGColorSpaceRelease(baseSpace); // no crash on nil
            if (streamData) CFRelease(streamData);
        }
    }
    return cgColorSpace;
}

CGFloat *PSPDFCreateDecodedValuesFromImageDictionary(CGPDFDictionaryRef dict, CGColorSpaceRef cgColorSpace, NSInteger bitsPerComponent) {
    CGFloat *decodeValues = NULL;
    CGPDFArrayRef decodeArray = NULL;

    if (CGPDFDictionaryGetArray(dict, "Decode", &decodeArray)) {
        size_t count = CGPDFArrayGetCount(decodeArray);
        decodeValues = malloc(sizeof(CGFloat) * count);
        CGPDFReal realValue;
        int i;
        for (i = 0; i < count; i++) {
            CGPDFArrayGetNumber(decodeArray, i, &realValue);
            decodeValues[i] = realValue;
        }
    } else {
        size_t n;
        switch (CGColorSpaceGetModel(cgColorSpace)) {
            case kCGColorSpaceModelMonochrome:
                decodeValues = malloc(sizeof(CGFloat) * 2);
                decodeValues[0] = 0.0;
                decodeValues[1] = 1.0;
                break;
            case kCGColorSpaceModelRGB:
                decodeValues = malloc(sizeof(CGFloat) * 6);
                for (int i = 0; i < 6; i++) {
                    decodeValues[i] = i % 2 == 0 ? 0 : 1;
                }
                break;
            case kCGColorSpaceModelCMYK:
                decodeValues = malloc(sizeof(CGFloat) * 8);
                for (int i = 0; i < 8; i++) {
                    decodeValues[i] = i % 2 == 0 ? 0.0 :
                    1.0;
                }
                break;
            case kCGColorSpaceModelLab:
                // ????
                break;
            case kCGColorSpaceModelDeviceN:
                n =
                CGColorSpaceGetNumberOfComponents(cgColorSpace) * 2;
                decodeValues = malloc(sizeof(CGFloat) * (n *
                                                         2));
                for (int i = 0; i < n; i++) {
                    decodeValues[i] = i % 2 == 0 ? 0.0 :
                    1.0;
                }
                break;
            case kCGColorSpaceModelIndexed:
                decodeValues = malloc(sizeof(CGFloat) * 2);
                decodeValues[0] = 0.0;
                decodeValues[1] = pow(2.0, (double)bitsPerComponent) - 1;
                break;
            default:
                break;
        }
    }
    return decodeValues;
}

@end
