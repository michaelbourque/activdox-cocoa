//
//  PSPDFLabelParser.m
//  PSPDFKit
//
//  Copyright (c) 2012 Peter Steinberger. All rights reserved.
//
//  Special thanks to Cédric Luthi for providing the code.
//

#import "PSPDFLabelParser.h"
#import "PSPDFDocument.h"
#import "PSPDFDocumentProvider.h"
#import "PSPDFOrderedDictionary.h"

@interface PSPDFLabelParser ()
@property (nonatomic, weak) PSPDFDocumentProvider *documentProvider;
@property (nonatomic, copy) PSPDFOrderedDictionary *labels;
@end

@implementation PSPDFLabelParser

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (id)initWithDocumentProvider:(PSPDFDocumentProvider *)documentProvider {
    if ((self = [super init])) {
        _documentProvider = documentProvider;
    }
    return self;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public

- (NSDictionary *)labels {
    if (!_labels) _labels = [self parseDocument];
    return _labels;
}

- (NSDictionary *)parseDocument {
    if (![self.documentProvider.document isValid]) return nil;

    PSPDFOrderedDictionary *labels = [PSPDFOrderedDictionary dictionaryWithCapacity:[_documentProvider pageCount]+1];
    @autoreleasepool {
        CGPDFDocumentRef documentRef = [_documentProvider requestDocumentRefWithOwner:self];
        size_t numberOfPages = CGPDFDocumentGetNumberOfPages(documentRef);

        // See PDF Reference §8.3.1 (Page Labels)
        CGPDFDictionaryRef catalog = CGPDFDocumentGetCatalog(documentRef);
        CGPDFDictionaryRef pdfPageLabels = NULL;
        if (CGPDFDictionaryGetDictionary(catalog, "PageLabels", &pdfPageLabels) && pdfPageLabels) {
            CGPDFArrayRef pdfNums = NULL;
            if (CGPDFDictionaryGetArray(pdfPageLabels, "Nums", &pdfNums) && pdfNums) {
                size_t count = CGPDFArrayGetCount(pdfNums);
                for (size_t i = 0; i < count; i += 2) {
                    CGPDFInteger rangeStart = 0;
                    CGPDFInteger rangeEnd = 0;
                    NSString *prefix = nil;
                    NSString *style = nil;
                    CGPDFInteger pdfOffset = 1;

                    CGPDFArrayGetInteger(pdfNums, i, &rangeStart);
                    if ((i + 2) < count) {
                        CGPDFArrayGetInteger(pdfNums, i + 2, &rangeEnd);
                    }else {
                        rangeEnd = numberOfPages;
                    }

                    if (i + 1 < count) {
                        CGPDFDictionaryRef pdfLabelDictionary = NULL;
                        if (CGPDFArrayGetDictionary(pdfNums, i + 1, &pdfLabelDictionary)) {
                            CGPDFStringRef pdfPrefix = NULL;
                            if (CGPDFDictionaryGetString(pdfLabelDictionary, "P", &pdfPrefix) && pdfPrefix) {
                                prefix = CFBridgingRelease(CGPDFStringCopyTextString(pdfPrefix));
                            }
                            const char *pdfStyle = NULL;
                            if (CGPDFDictionaryGetName(pdfLabelDictionary, "S", &pdfStyle) && pdfStyle) {
                                style = @(pdfStyle);
                            }
                            CGPDFDictionaryGetInteger(pdfLabelDictionary, "St", &pdfOffset);
                        }
                    }

                    for (CGPDFInteger j = rangeStart; j < rangeEnd; j++) {
                        NSUInteger n = pdfOffset + j - rangeStart;
                        NSString *label = PSPDFPageLabel(n, prefix, style);
                        if (label) labels[@(n)] = label;
                    }
                }
            }
        }
        [_documentProvider releaseDocumentRef:documentRef withOwner:self];

        // sort after page index
        [labels.keyArray sortedArrayUsingSelector:@selector(compare:)];
    }
	return labels;
}

- (NSString *)pageLabelForPage:(NSUInteger)page {
    // returns nil if key is not found
    NSString *pageLabel = (self.labels)[@(page)];
    return pageLabel;
}

- (NSUInteger)pageForPageLabel:(NSString *)pageLabel partialMatching:(BOOL)partialMatching {
    __block NSUInteger foundPage = NSNotFound;
    NSMutableDictionary *partialMatches = [NSMutableDictionary dictionary];

    // iterate over all items and search for pageLabel.
    [self.labels enumerateKeysAndObjectsWithOptions:0 usingBlock:^(NSNumber *pageNumber, NSString *aPageLabel, BOOL *stop) {
        if ([pageLabel rangeOfString:aPageLabel options:NSCaseInsensitiveSearch].length > 0) {
            foundPage = [pageNumber unsignedIntegerValue];
            *stop = YES;
        }

     if (partialMatching && *stop == NO) {
         NSRange searchRange = [aPageLabel rangeOfString:pageLabel options:NSCaseInsensitiveSearch | NSDiacriticInsensitiveSearch];

         // array is sorted - if lenght is equal use the first match
         if (searchRange.length > 0 && !partialMatches[@(searchRange.length)]) {
             partialMatches[@(searchRange.length)] = pageNumber;
         }
     }
    }];

    if (foundPage == NSNotFound && [partialMatches count]) {
        NSNumber *mostLikelyKey = [[[partialMatches allKeys] sortedArrayUsingSelector:@selector(compare:)] ps_firstObject];
        foundPage = [partialMatches[mostLikelyKey] unsignedIntegerValue];
    }

    return foundPage;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Private

static NSString *PSPDFPageLabel(NSUInteger pageIndex, NSString *prefix, NSString *labelStyle) {
    // if there's no prefix and no labelStyle, don't bother creating a pageLabel.
    NSString *result = prefix ? [NSString stringWithFormat:@"%u", pageIndex] : nil;

    if ([[labelStyle lowercaseString] isEqualToString:@"r"]) {
        // Code from http://www.cocoabuilder.com/archive/cocoa/173503-roman-numerals-nsnumberformatter.html
        int i, deflateNumber = pageIndex;
        NSString *romanValue = @"";
        NSDictionary *pairs = @{
        @"1": @"i",
        @"10": @"x",
        @"100": @"c",
        @"1000": @"m",
        @"4": @"iv",
        @"40": @"xl",
        @"400": @"cd",
        @"5": @"v",
        @"50": @"l",
        @"500": @"d",
        @"9": @"ix",
        @"90": @"xc",
        @"900": @"cm"};

        NSArray *values = @[@"1000", @"900", @"500", @"400", @"100", @"90", @"50", @"40", @"10", @"9", @"5", @"4", @"1"];
        int itemCount = [values count], itemValue;

        for (i = 0; i < itemCount; i++) {
            itemValue = [values[i] intValue];

            while (deflateNumber >= itemValue) {
                romanValue = [romanValue stringByAppendingString:pairs[values[i]]];
                deflateNumber -= itemValue;
            }
        }

        result = romanValue;
    }else if ([[labelStyle lowercaseString] isEqualToString:@"a"]) {
        NSString *letterValue = @"";
        NSString *letter = [NSString stringWithFormat:@"%c", 'a' + ((pageIndex - 1) % 26)];
        NSUInteger length = 1 + ((pageIndex - 1) / 26);
        for (NSUInteger i = 0; i < length; i++)
            letterValue = [letterValue stringByAppendingString:letter];

        result = letterValue;
    }

    if ([labelStyle isEqualToString:@"R"] || [labelStyle isEqualToString:@"A"])
        result = [result uppercaseString];

    return prefix ? [prefix stringByAppendingString:result] : result;
}

@end
