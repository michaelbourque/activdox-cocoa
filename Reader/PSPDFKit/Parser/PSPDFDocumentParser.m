//
//  PSPDFDocumentParser.m
//  PSPDFKit
//
//  Copyright (c) 2012 Peter Steinberger. All rights reserved.
//

#import "PSPDFDocumentParser.h"
#import "PSPDFKitGlobal.h"
#import "PSPDFKitGlobal+Internal.h"
#import "NSData+Compression.h"
#import "PSPDFAnnotationParser.h"
#import "PSPDFAnnotation.h"
#import "PSPDFDocumentProvider.h"
#import "PSPDFDocument.h"
#import <Endian.h>

@interface PSPDFXRefEntry : NSObject
@property (nonatomic, assign) NSInteger objectNumber;
@property (nonatomic, assign) long byteOffset;
@property (nonatomic, assign) BOOL isCompressed;
@property (nonatomic, assign) NSInteger objectStreamNumber;
@property (nonatomic, assign) BOOL isDeleted;
@end

@interface PSPDFDocumentParser ()
@property (nonatomic, weak) PSPDFDocumentProvider *documentProvider;
@property (atomic, assign) BOOL needsParsing;
@end

@implementation PSPDFDocumentParser {
    NSLock *_parserLock;
    NSUInteger _prevXrefOffset;
	NSInteger _rootObjectNumber;
	NSInteger _encryptObjectNumber;
	NSInteger _infoObjectNumber;
    NSString *_documentID;
    NSMutableDictionary *_cachedObjects;
    NSDictionary *_crossReferenceTable;
    BOOL _containsObjectStreams;

    NSFileHandle *_handle; // if we are URL-based
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (id)initWithDocumentProvider:(PSPDFDocumentProvider *)documentProvider {
    if ((self = [super init])) {
        _parserLock = [NSLock new];
        [_parserLock setName:@"DocumentParserLock"];
        _needsParsing = YES;
        _documentProvider = documentProvider;
    }
    return self;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Data Fetching

- (unsigned long long)totalFileSize {
    unsigned long long fileSize = 0;
    if (_handle) {
        [_handle seekToEndOfFile];
        fileSize = [_handle offsetInFile];
    }else {
        fileSize = [self.documentProvider.data length];
    }
    return fileSize;
}

// abstracts file/data based PDFs.
- (NSData *)dataAtOffset:(unsigned long long)offset length:(NSUInteger)length {
    NSData *data = nil;
    if (_handle) {
        if ([_handle offsetInFile] != offset) {
            [_handle seekToFileOffset:offset];
        }
        if (length == NSUIntegerMax) {
            data = [_handle readDataToEndOfFile];
        }else {
            data = [_handle readDataOfLength:length];
        }
    }else if (self.documentProvider.data) {
        length = MIN([self.documentProvider.data length] - (NSUInteger)offset, length);
        data = [self.documentProvider.data subdataWithRange:NSMakeRange((NSUInteger)offset, length)];
    }else {
        PSPDFLogWarning(@"Annotation writing to CGDataProvider is not yet supported.");
    }
    return data;
}

- (BOOL)openFileHandleIfNeededWithError:(NSError **)error {
    NSError *localError = nil;

    // open the file if needed
    if (!_handle && self.documentProvider.fileURL) {
        _handle = [NSFileHandle fileHandleForReadingFromURL:_documentProvider.fileURL error:&localError];

        if (localError) {
            PSPDFLogError(@"Error while opening file handle: %@", [localError localizedDescription]);
        }
    }

    // only copy error over if error address is backed by an object.
    if (error) {
        *error = localError;
    }
    return localError == nil;
}

- (void)closeFileHandle {
    [_handle closeFile];
    _handle = nil;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Annotations

- (BOOL)saveAnnotations:(NSDictionary *)newAnnotations withError:(NSError **)error {
#ifndef PSPDFKIT_DISABLE_TEXT_SELECTION_FEATURES
    // Always parse document (as things change after saving!)
    [self parseDocumentWithError:error];
    _needsParsing = YES; // re-parse later on.

    [self openFileHandleIfNeededWithError:error];
    NSArray *sortedObjectNumbers = [[_crossReferenceTable allKeys] sortedArrayUsingSelector:@selector(compare:)];
	__block NSInteger numberForNewObject = [[sortedObjectNumbers lastObject] integerValue] + 1;

    NSMutableDictionary *updatedObjects = [NSMutableDictionary dictionary];

    // Resolve page object
    [newAnnotations enumerateKeysAndObjectsUsingBlock:^(NSNumber *pageNumber, NSArray *annotations, BOOL *stop) {
        NSUInteger page = [pageNumber unsignedIntegerValue];

        // don't execute if page is invalid (e.g. multi-page methods)
        if (page < [_pageObjectNumbers count]) {
            PSPDFXRefEntry *pageObjectEntry = _crossReferenceTable[_pageObjectNumbers[page]];
            NSString *pageObject = [self indirectObjectWithEntry:pageObjectEntry inTable:_crossReferenceTable];

            // find existing annotations array in page object:
            NSScanner *annotsScanner = [NSScanner scannerWithString:pageObject];
            [annotsScanner scanUpToString:@"/Annots" intoString:NULL];
            BOOL hasAnnots = [annotsScanner scanString:@"/Annots" intoString:NULL];
            NSMutableArray *existingAnnots = [NSMutableArray array];
            NSArray *previousAnnots = @[];

            // merge current annots with existing annots
            if (hasAnnots) {
                NSInteger annotsObjectNumber = 0;
                BOOL annotsIsIndirectObject = [annotsScanner scanInteger:&annotsObjectNumber];
                if (annotsIsIndirectObject) {
                    PSPDFXRefEntry *annotsEntry = _crossReferenceTable[@(annotsObjectNumber)];
                    NSString *annotsObject = [self indirectObjectWithEntry:annotsEntry inTable:_crossReferenceTable];
                    annotsScanner = [NSScanner scannerWithString:annotsObject];
                }
                BOOL annotsIsDirectArray = [annotsScanner scanString:@"[" intoString:NULL];
                if (annotsIsDirectArray) {
                    while (![annotsScanner isAtEnd]) {
                        NSInteger annotationObjectNumber = 0;
                        BOOL scannedAnnotationObjectNumber = [annotsScanner scanInteger:&annotationObjectNumber];
                        if (scannedAnnotationObjectNumber) {
                            [existingAnnots addObject:@(annotationObjectNumber)];
                        }
                        [annotsScanner scanInteger:NULL]; // generation number
                        if (![annotsScanner scanString:@"R" intoString:NULL]) break;
                    }

                    previousAnnots = [existingAnnots copy];
                    NSMutableIndexSet *indexSetToRemove = [NSMutableIndexSet indexSet];

                    for (PSPDFAnnotation *annotation in annotations) {
                        if (annotation.isDeleted && annotation.indexOnPage >= 0) {
                            [indexSetToRemove addIndex:(NSUInteger)annotation.indexOnPage];
                            if (annotation.popupIndex >= 0) {
                                [indexSetToRemove addIndex:(NSUInteger)annotation.popupIndex];
                            }
                        }
                    }
                    @try {
                        [existingAnnots removeObjectsAtIndexes:indexSetToRemove];
                    }
                    @catch (NSException *exception) {
                        PSPDFLogError(@"Inconsistent annotation data: %@", exception);
                    }
                }
            }

            NSMutableDictionary *newAnnotationsByObjectNumber = [NSMutableDictionary dictionary];
            NSMutableDictionary *newAnnotationDataByObjectNumber = [NSMutableDictionary dictionary];
            for (PSPDFAnnotation *newAnnotation in annotations) {
                if (newAnnotation.isDirty && !newAnnotation.isDeleted) {
                    NSMutableData *newAnnotationObject = [NSMutableData data];
                    [newAnnotationObject pspdf_appendDataString:[NSString stringWithFormat:@"\n\n%i 0 obj\n", numberForNewObject]];
                    NSData *newAnnotationData = [newAnnotation pdfDataRepresentation];
                    [newAnnotationObject appendData:newAnnotationData];
                    [newAnnotationObject pspdf_appendDataString:@"\nendobj"];

                    newAnnotationDataByObjectNumber[@(numberForNewObject)] = newAnnotationObject;
                    newAnnotationsByObjectNumber[@(numberForNewObject)] = newAnnotation;

                    updatedObjects[@(numberForNewObject)] = newAnnotationObject;
                    numberForNewObject++;
                    // after processing, set to not dirty.
                    newAnnotation.dirty = NO;
                }
            }

            NSMutableString *newAnnotsString = [NSMutableString stringWithString:@"/Annots ["];
            for (NSNumber *existingAnnotationObjectNumber in existingAnnots) {
                [newAnnotsString appendFormat:@"%@ 0 R ", existingAnnotationObjectNumber];
            }
            NSInteger i = 0;
            for (NSNumber *newAnnotationObjectNumber in [newAnnotationDataByObjectNumber allKeys]) {
                // Assign indexOnPage, so that the annotation itself is marked as saved
                PSPDFAnnotation *annotation = newAnnotationsByObjectNumber[newAnnotationObjectNumber];
                annotation.indexOnPage = i;

                [newAnnotsString appendFormat:@"%@ 0 R ", newAnnotationObjectNumber];
                i++;
            }
            [newAnnotsString appendString:@"]"];

            // Find range of /Annots array to replace it
            NSRange annotsRange;
            if (hasAnnots) {
                NSCharacterSet *endAnnotsCharacterSet = [NSCharacterSet characterSetWithCharactersInString:@"/>"]; // next key or end of page dictionary
                NSScanner *annotsRangeScanner = [NSScanner scannerWithString:pageObject];
                [annotsRangeScanner scanUpToString:@"/Annots" intoString:NULL];
                int rangeLocation = [annotsRangeScanner scanLocation];
                [annotsRangeScanner scanString:@"/Annots" intoString:NULL];
                [annotsRangeScanner scanUpToCharactersFromSet:endAnnotsCharacterSet intoString:NULL];
                int rangeLength = [annotsRangeScanner scanLocation] - rangeLocation;
                annotsRange = NSMakeRange(rangeLocation, rangeLength);
            } else {
                NSRange endDictRange = [pageObject rangeOfString:@">>" options:NSBackwardsSearch];
                annotsRange = NSMakeRange(endDictRange.location, 0);
            }
            // Replace or insert /Annots array
            NSString *updatedPageContent = [pageObject stringByReplacingCharactersInRange:annotsRange withString:newAnnotsString];
            NSString *updatedPageObject = [NSString stringWithFormat:@"\n%@ 0 obj\n%@\nendobj", (self.pageObjectNumbers)[page], updatedPageContent];
            updatedObjects[(self.pageObjectNumbers)[page]] = [updatedPageObject dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
        }
    }];

    unsigned long long offset = [self totalFileSize];
    NSMutableData *trailerData = [NSMutableData data];

    // add PSPDFKit marker as a PDF comment before adding the new annotation objects.
    NSData *pspdfkitTrailer = [self pspdfkitTrailerData];
    [trailerData appendData:pspdfkitTrailer];
    offset += [pspdfkitTrailer length];

    if (_containsObjectStreams) {
        // Generate xref update
        offset++;

        // Generate xref stream data for updated objects
        NSMutableData *newXrefStreamData = [NSMutableData data];
        unsigned long long progressingOffset = offset;
        for (NSNumber *updatedObjectNumber in [[updatedObjects allKeys] sortedArrayUsingSelector:@selector(compare:)]) {
            uint8_t objectType = 1;
            [newXrefStreamData appendBytes:&objectType length:1];
            // convert offset to big-endian
            NSUInteger bytes = EndianU32_NtoB(progressingOffset);
            [newXrefStreamData appendBytes:&bytes length:4];
            NSData *updatedObject = updatedObjects[updatedObjectNumber];
            progressingOffset += [updatedObject length];
        }
        // Append info for xref stream object itself
        uint8_t objectType = 1;
        [newXrefStreamData appendBytes:&objectType length:1];
        NSUInteger bytes = EndianU32_NtoB(progressingOffset);
        [newXrefStreamData appendBytes:&bytes length:4];

        // Generate xref stream dictionary
        NSMutableString *newXrefDictionary = [NSMutableString string];
        [newXrefDictionary appendFormat:@"\n%i 0 obj\n", numberForNewObject];
        numberForNewObject++;
        [newXrefDictionary appendFormat:@"<</Type /XRef /Size %i ", numberForNewObject];
        [newXrefDictionary appendFormat:@"/Prev %i ", _prevXrefOffset];
        [newXrefDictionary appendFormat:@"/Root %i 0 R ", _rootObjectNumber];
        if (_infoObjectNumber != 0) [newXrefDictionary appendFormat:@"/Info %i 0 R ", _infoObjectNumber];
        if (_encryptObjectNumber != 0) [newXrefDictionary appendFormat:@"/Encrypt %i 0 R ", _encryptObjectNumber];
        if (_documentID) [newXrefDictionary appendFormat:@"/ID %@ ", _documentID];
        // Use 1 byte for field 1 (object type), 4 bytes for field 2 (byte offset), use default value for field 3 (generation)
        [newXrefDictionary appendString:@"/W [1 4 0] "];
        [newXrefDictionary appendFormat:@"/Length %i ", [newXrefStreamData length]];

        // Generate index of objects in xref stream
        NSMutableString *xrefStreamIndex = [NSMutableString stringWithString:@"["];
        for (NSNumber *updatedObjectNumber in [[updatedObjects allKeys] sortedArrayUsingSelector:@selector(compare:)]) {
            [xrefStreamIndex appendFormat:@"%@ 1 ", updatedObjectNumber];
        }
        // Include the xref stream object in the index
        [xrefStreamIndex appendFormat:@"%i 1", numberForNewObject-1];
        [xrefStreamIndex appendString:@"]"];

        [newXrefDictionary appendFormat:@"/Index %@ ", xrefStreamIndex];
        [newXrefDictionary appendFormat:@">>"];

        // Concatenate updated objects
        NSMutableData *bodyUpdate = [NSMutableData data];
        for (NSNumber *updatedObjectNumber in [[updatedObjects allKeys] sortedArrayUsingSelector:@selector(compare:)]) {
            NSData *updatedObject = updatedObjects[updatedObjectNumber];
            [bodyUpdate appendData:updatedObject];
        }

        [trailerData appendData:bodyUpdate];
        [trailerData pspdf_appendDataString:newXrefDictionary];
        [trailerData pspdf_appendDataString:@"\rstream\n"];
        [trailerData appendData:newXrefStreamData];
        [trailerData pspdf_appendDataString:@"\nendstream\rendobj"];
        [trailerData pspdf_appendDataString:[NSString stringWithFormat:@"\n\nstartxref\n%llu\n%%%%EOF", progressingOffset]];
    } else {
        unsigned long long progressingOffset = offset;
        NSMutableString *newCrossReferenceSection = [NSMutableString stringWithString:@"\nxref\n"];
        for (NSNumber *updatedObjectNumber in [updatedObjects allKeys]) {
            NSString *subsection = [NSString stringWithFormat:@"%@ 1\n%010llu 00000 n \n", updatedObjectNumber, progressingOffset];
            [newCrossReferenceSection appendString:subsection];
            NSData *updatedObject = updatedObjects[updatedObjectNumber];
            progressingOffset += [updatedObject length];
        }

        NSMutableData *bodyUpdate = [NSMutableData data];
        for (NSData *updatedObject in [updatedObjects allValues]) {
            [bodyUpdate appendData:updatedObject];
        }

        // figure out total file offset (either for data or file based PDF's)
        unsigned long long newXrefOffset = offset + 1;
        [trailerData appendData:bodyUpdate];
        newXrefOffset += [bodyUpdate length];
        [trailerData pspdf_appendDataString:newCrossReferenceSection];

        NSMutableString *newTrailer = [NSMutableString stringWithString:@"\ntrailer\n<<"];
        [newTrailer appendFormat:@"/Size %i ", numberForNewObject];
        [newTrailer appendFormat:@"/Root %i 0 R ", _rootObjectNumber];
        if (_encryptObjectNumber != 0) [newTrailer appendFormat:@"/Encrypt %i 0 R ", _encryptObjectNumber];
        if (_documentID) [newTrailer appendFormat:@"/ID %@ ", _documentID];
        [newTrailer appendFormat:@"/Prev %i", _prevXrefOffset];
        [newTrailer appendFormat:@">>\nstartxref\n%llu\n%%%%EOF", newXrefOffset];
        [trailerData pspdf_appendDataString:newTrailer];
    }

    // finally write the new data.
    NSFileHandle *handleForWriting = nil;
    if (_documentProvider.fileURL) {
        //NSLog(@"Writing trailer: %@", [[NSString alloc] initWithData:trailerData encoding:NSUTF8StringEncoding]);

        handleForWriting = [NSFileHandle fileHandleForUpdatingURL:_documentProvider.fileURL error:error];
        if (!handleForWriting) {
            return NO;
        }
        [handleForWriting seekToEndOfFile];
    }

    if ([self delegateShouldAppendData:trailerData]) {
        [handleForWriting writeData:trailerData]; // might be nil for non file-based documents

        // append data
        if (_documentProvider.data) {
            NSData *originalData = _documentProvider.data;
            NSMutableData *pdfData = [originalData mutableCopy];
            [pdfData appendData:trailerData];
            _documentProvider.data = pdfData;
        }

        [self delegateDidAppendData:trailerData];

        PSPDFLog(@"Successfully written new trailer data of length %d into %@.", [trailerData length], self);
    }

    [self closeFileHandle];

    return YES;
#else
    PSPDFLogWarning(@"Saving annotations is only supported in PSPDFKit Annotate.");
    return NO;
#endif
}

// delegate handling.
- (BOOL)delegateShouldAppendData:(NSData *)data {
    BOOL shouldAppend = YES;
    if ([self.documentProvider.delegate respondsToSelector:@selector(documentProvider:shouldAppendData:)]) {
        shouldAppend = [self.documentProvider.delegate documentProvider:self.documentProvider shouldAppendData:data];
    }
    return shouldAppend;
}

// delegate handling.
- (void)delegateDidAppendData:(NSData *)data {
    if ([self.documentProvider.delegate respondsToSelector:@selector(documentProvider:didAppendData:)]) {
        [self.documentProvider.delegate documentProvider:self.documentProvider didAppendData:data];
    }
}

- (NSData *)pspdfkitTrailerData {
    // add PSPDFKit marker as a PDF comment before adding the new annotation objects.
    NSDictionary *infoDict = [[NSBundle mainBundle] infoDictionary];
    NSString *appName = PSPDFAppName();
    NSString *appIdentifier = infoDict[@"CFBundleIdentifier"];
    NSString *appVersion = infoDict[@"CFBundleVersion"];
    NSString *appVersionShortString = infoDict[@"CFBundleShortVersionString"];
    NSString *pdfMarker = [NSString stringWithFormat:@"\n\n%%PSPDFKit: Annotation trailer added by %@ (%@) %@ (%@) - %@.\n", appName, appIdentifier, appVersion, appVersionShortString, PSPDFVersionString()];
    PSPDFLogVerbose(@"Adding marker: %@", pdfMarker);
    return [pdfMarker dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Xref parsing

- (BOOL)parseDocumentWithError:(NSError **)error {
#ifndef PSPDFKIT_DISABLE_TEXT_SELECTION_FEATURES
    NSError *localError = nil;
    [self openFileHandleIfNeededWithError:&localError];

	[_parserLock lock];
    if (!self.needsParsing) {
        [_parserLock unlock];
        return NO;
    }

    // TODO: add support for data providers
    if (_documentProvider.dataProvider && !_documentProvider.data) {
        PSPDFLogWarning(@"Annotation saving is not yet supported for dataprovider-based documents.");
        self.needsParsing = NO;
        [_parserLock unlock];
        return NO;
    }

	_pageObjectNumbers = nil;
	_crossReferenceTable = nil;

	_cachedObjects = [NSMutableDictionary new];

	unsigned long long endOffset = [self totalFileSize];
	unsigned long long offset = MAX(0, endOffset - 500);

	// Read in last 500 bytes of file
	NSData *endData = [self dataAtOffset:offset length:NSUIntegerMax];
	NSString *endString = [[NSString alloc] initWithData:endData encoding:NSASCIIStringEncoding];

	// Find startxref
	NSInteger startXrefPos = 0;
	NSScanner *startXrefScanner = [NSScanner scannerWithString:endString];
	while (![startXrefScanner isAtEnd]) {
		[startXrefScanner scanUpToString:@"startxref" intoString:NULL];
		BOOL startxrefFound = [startXrefScanner scanString:@"startxref" intoString:NULL];
		if (startxrefFound) {
			[startXrefScanner scanInteger:&startXrefPos];
			_prevXrefOffset = startXrefPos;
		}
	}

	// Parse xref table(s) and trailer(s)
	NSMutableDictionary *xrefTable = [NSMutableDictionary dictionary];
	xrefTable[@"objects"] = [NSMutableDictionary dictionary];
	[self parseXrefAtOffset:startXrefPos inXrefTable:xrefTable];

	NSMutableDictionary *objects = xrefTable[@"objects"];
	if (xrefTable[@"encrypt"]) _encryptObjectNumber = [xrefTable[@"encrypt"] integerValue];
	if (xrefTable[@"id"]) _documentID = xrefTable[@"id"];
	if (xrefTable[@"info"]) _infoObjectNumber = [xrefTable[@"info"] integerValue];
	if (xrefTable[@"root"]) {
		_rootObjectNumber = [xrefTable[@"root"] integerValue];
		PSPDFXRefEntry *rootEntry = objects[@(_rootObjectNumber)];
		NSString *rootObject = [self indirectObjectWithEntry:rootEntry inTable:objects];
		_pageObjectNumbers = [self parseOrdered_pageObjectNumbersWithRootObject:rootObject xrefTable:objects];
		_crossReferenceTable = objects;
	} else {
		PSPDFLogWarning(@"No root object found in %@", self.documentProvider);
	}

	_cachedObjects = nil;

    // look for specific encryption filter like Adobe.APS (LifeCycle DRM, unsupported by iOS)
    NSString *encryptXRefString = [self indirectObjectWithEntry:_crossReferenceTable[@(_encryptObjectNumber)] inTable:_crossReferenceTable];

    if ([encryptXRefString length] > 0) {
        NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"/Filter/([^/]*)/" options:NSRegularExpressionCaseInsensitive error:NULL];
        NSTextCheckingResult *textResult = [regex firstMatchInString:encryptXRefString options:0 range:NSMakeRange(0, [encryptXRefString length])];
        if (textResult) {
            NSString *encryptionFilter = [encryptXRefString substringWithRange:[textResult rangeAtIndex:1]];
            if (![encryptionFilter isEqualToString:@"Standard"]) {
                _encryptionFilter = encryptionFilter;
                PSPDFLogWarning(@"Found encryption filter: %@", _encryptionFilter);
            }
        }
    }

	[_parserLock unlock];
    self.needsParsing = NO;

    [self closeFileHandle];

    if (error) {
        *error = localError;
    }

    return localError == nil;
#else
    return NO;
#endif
}

- (NSArray *)parseOrdered_pageObjectNumbersWithRootObject:(NSString *)rootObject xrefTable:(NSDictionary *)xrefTable {
	// Find root of page tree:
	NSInteger pagesObjectNumber = [self scanIntegerForKey:@"/Pages" inDictionary:rootObject defaultValue:0];
	if (pagesObjectNumber != 0) {
		NSMutableArray *pageObjects = [NSMutableArray array];
		[self parsePageNodeWithObjectNumber:pagesObjectNumber xrefTable:xrefTable inArray:pageObjects];
		return [pageObjects copy];
	}
	return @[];
}

- (void)parsePageNodeWithObjectNumber:(NSInteger)objectNumber xrefTable:(NSDictionary *)xrefTable inArray:(NSMutableArray *)pagesObjectNumbers {
	PSPDFXRefEntry *pagesObjectEntry = xrefTable[@(objectNumber)];
	if (!pagesObjectEntry || pagesObjectEntry.isDeleted) {
		// This page tree node was apparently deleted or doesn't exist (shouldn't really happen, but just in case...), bail out.
		return;
	}
	NSString *pagesObject = [self indirectObjectWithEntry:pagesObjectEntry inTable:xrefTable];

	NSInteger count = [self scanIntegerForKey:@"/Count" inDictionary:pagesObject defaultValue:0];
	if (count > 0) {
		NSScanner *kidsScanner = [NSScanner scannerWithString:pagesObject];
		[kidsScanner scanUpToString:@"/Kids" intoString:NULL];
		[kidsScanner scanString:@"/Kids" intoString:NULL];
		NSString *kidsArray = nil;
		[kidsScanner scanUpToString:@"[" intoString:NULL];
		[kidsScanner scanString:@"[" intoString:NULL];
		[kidsScanner scanUpToString:@"]" intoString:&kidsArray];

		NSMutableArray *kidObjectNumbers = [NSMutableArray array];

		NSScanner *kidsArrayScanner = [NSScanner scannerWithString:kidsArray];
		while (![kidsArrayScanner isAtEnd]) {
			NSInteger kidObjectNumber = 0;
			[kidsArrayScanner scanInteger:&kidObjectNumber];
			[kidObjectNumbers addObject:@(kidObjectNumber)];
			[kidsArrayScanner scanInteger:NULL];
			[kidsArrayScanner scanString:@"R" intoString:NULL];
		}

		if (count == [kidObjectNumbers count]) {
			// /Count is equal to the number of kids, all kids must be leaf nodes:
			[pagesObjectNumbers addObjectsFromArray:kidObjectNumbers];
		} else {
			//Parse all kids recursively:
			for (NSNumber *kidObjectNumber in kidObjectNumbers) {
				[self parsePageNodeWithObjectNumber:[kidObjectNumber integerValue] xrefTable:xrefTable inArray:pagesObjectNumbers];
			}
		}

	} else {
		// no /Count, probably a leaf node, check /Type to be sure (could be an orphan /Pages node):
		NSString *type = [self scanNameForKey:@"/Type" inDictionary:pagesObject];
		if ([type isEqualToString:@"Page"]) {
			[pagesObjectNumbers addObject:@(objectNumber)];
		}
	}
}

- (NSString *)indirectObjectWithByteOffset:(long)offset {
	NSMutableString *s = [NSMutableString string];
	NSData *dataChunk = nil;
	int endobjLocation = 0;
	do {
        dataChunk = [self dataAtOffset:offset length:1024];
        offset += 1024;

		NSString *textChunk = [[NSString alloc] initWithData:dataChunk encoding:NSASCIIStringEncoding];
		[s appendString:textChunk];
		endobjLocation = [s rangeOfString:@"endobj" options:NSBackwardsSearch].location;
		if (endobjLocation != NSNotFound) break;
	} while ([dataChunk length] > 0);
	NSString *objectContent = nil;
	NSScanner *objectScanner = [NSScanner scannerWithString:s];
	[objectScanner scanUpToString:@"obj" intoString:NULL];
	[objectScanner scanString:@"obj" intoString:NULL];
	[objectScanner scanUpToString:@"endobj" intoString:&objectContent];
	return objectContent;
}

- (NSData *)indirectObjectDataWithByteOffset:(long)offset {
	NSMutableString *s = [NSMutableString string];
	NSMutableData *dataBuffer = [NSMutableData data];
	NSData *dataChunk = nil;
	int endobjLocation = 0;
	do {
        dataChunk = [self dataAtOffset:offset length:1024];
        offset += 1024;
		[dataBuffer appendData:dataChunk];
		NSString *textChunk = [[NSString alloc] initWithData:dataChunk encoding:NSASCIIStringEncoding];
		[s appendString:textChunk];
		endobjLocation = [s rangeOfString:@"endobj" options:NSBackwardsSearch].location;
		if (endobjLocation != NSNotFound) break;
	} while ([dataChunk length] > 0);

	NSString *objectContent = nil;
	NSScanner *objectScanner = [NSScanner scannerWithString:s];
	[objectScanner scanUpToString:@"obj" intoString:NULL];
	[objectScanner scanString:@"obj" intoString:NULL];
	NSInteger objectStart = [objectScanner scanLocation];
	[objectScanner scanUpToString:@"endobj" intoString:&objectContent];
	NSInteger objectEnd = [objectScanner scanLocation];

	NSData *objectData = [dataBuffer subdataWithRange:NSMakeRange(objectStart, objectEnd - objectStart)];

	return objectData;
}

- (NSString *)indirectObjectWithEntry:(PSPDFXRefEntry *)entry inTable:(NSDictionary *)objects {
	if (!entry) return nil;

	NSString *cachedObject = _cachedObjects[@(entry.objectNumber)];
	if (cachedObject) return cachedObject;

	if (!entry.isCompressed && !entry.isDeleted) {
		long offset = entry.byteOffset;
		NSString *objectContent = [self indirectObjectWithByteOffset:offset];
		_cachedObjects[@(entry.objectNumber)] = objectContent;
		return objectContent;
	} else if (!entry.isDeleted) {
		PSPDFXRefEntry *objectStreamEntry = objects[@(entry.objectStreamNumber)];
		long offset = objectStreamEntry.byteOffset;
		NSData *objectStream = [self indirectObjectDataWithByteOffset:offset];

		NSString *objectStreamString = [[NSString alloc] initWithData:objectStream encoding:NSASCIIStringEncoding];
		NSInteger firstObjectOffset = [self scanIntegerForKey:@"/First" inDictionary:objectStreamString defaultValue:0];

		NSData *decodedStreamData = [self decodeStreamWithData:objectStream];

		NSData *objectData = [decodedStreamData subdataWithRange:NSMakeRange(firstObjectOffset, [decodedStreamData length] - firstObjectOffset)];

		NSData *objectIndexData = [decodedStreamData subdataWithRange:NSMakeRange(0, firstObjectOffset)];
		NSString *objectIndexString = [[NSString alloc] initWithData:objectIndexData encoding:NSASCIIStringEncoding];
		NSScanner *objectIndexScanner = [NSScanner scannerWithString:objectIndexString];

		NSMutableArray *objectNumbers = [NSMutableArray array];
		NSMutableArray *objectOffsets = [NSMutableArray array];
		while (![objectIndexScanner isAtEnd]) {
			NSInteger objectNumber, objectOffset;
			[objectIndexScanner scanInteger:&objectNumber];
			[objectIndexScanner scanInteger:&objectOffset];
			[objectNumbers addObject:@(objectNumber)];
			[objectOffsets addObject:@(objectOffset)];
		}
		NSMutableDictionary *objectsInStream = [NSMutableDictionary dictionary];
		for (int i=0; i<[objectNumbers count]; i++) {
			NSNumber *objectNumber = objectNumbers[i];
			NSNumber *objectOffset = objectOffsets[i];
			NSInteger objectEnd;
			if (i + 1 < [objectNumbers count]-1) {
				objectEnd = [objectOffsets[i+1] integerValue];
			} else {
				objectEnd = [objectData length];
			}
			NSRange objectRange = NSMakeRange([objectOffset integerValue], objectEnd - [objectOffset integerValue]);
			NSData *extractedObjectData = [objectData subdataWithRange:objectRange];

			objectsInStream[objectNumber] = [[NSString alloc] initWithData:extractedObjectData encoding:NSASCIIStringEncoding];
		}

		for (NSNumber *objectNumberInStream in [objectsInStream allKeys]) {
			NSString *objectContent = objectsInStream[objectNumberInStream];
			PSPDFXRefEntry *objectEntry = objects[objectNumberInStream];
			//check if the object is still associated with the object stream (it could be updated):
			if (objectEntry.objectStreamNumber == entry.objectStreamNumber) {
				_cachedObjects[objectNumberInStream] = objectContent;
			}
		}
		return objectsInStream[@(entry.objectNumber)];
	}
	return nil;
}


- (NSInteger)scanIntegerForKey:(NSString *)key inDictionary:(NSString *)dict defaultValue:(NSInteger)defaultValue {
    if (!dict || !key) return 0;
	NSScanner *scanner = [NSScanner scannerWithString:dict];
	[scanner scanUpToString:key intoString:NULL];
	[scanner scanString:key intoString:NULL];
	NSInteger value = defaultValue;
	[scanner scanInteger:&value];
	return value;
}

- (NSString *)scanNameForKey:(NSString *)key inDictionary:(NSString *)dict {
    if (!dict || !key) return nil;
	NSScanner *scanner = [NSScanner scannerWithString:dict];
	[scanner scanUpToString:key intoString:NULL];
	if ([scanner scanString:key intoString:NULL]) {
		if ([scanner scanString:@"/" intoString:NULL]) {
			NSString *name = nil;
			[scanner scanUpToCharactersFromSet:[NSCharacterSet characterSetWithCharactersInString:@" >/"] intoString:&name];
			return [name stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
		}
	}
	return nil;
}

- (NSData *)decodeStreamWithData:(NSData *)data {
    if (!data) return nil;
	NSString *streamString = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
	NSScanner *streamDataScanner = [NSScanner scannerWithString:streamString];
	[streamDataScanner setCharactersToBeSkipped:[NSCharacterSet characterSetWithCharactersInString:@""]];
	[streamDataScanner scanUpToString:@"stream" intoString:NULL];
	[streamDataScanner scanString:@"stream" intoString:NULL];
	[streamDataScanner scanCharactersFromSet:[NSCharacterSet newlineCharacterSet] intoString:NULL];
	NSUInteger streamStart = [streamDataScanner scanLocation];

	// Find out how long the stream is
	NSInteger streamLength = [self scanIntegerForKey:@"/Length" inDictionary:streamString defaultValue:0];

	// Extract actual stream data
	NSData *streamData = [data subdataWithRange:NSMakeRange(streamStart, streamLength)];

	// Find out if the stream data is compressed
	NSScanner *filterScanner = [NSScanner scannerWithString:streamString];
	[filterScanner scanUpToString:@"/Filter" intoString:NULL];
	NSString *filterName = nil;
	BOOL hasFilter = [filterScanner scanString:@"/Filter" intoString:NULL];
	if (hasFilter) {
		[filterScanner scanUpToString:@"/" intoString:NULL];
		[filterScanner scanString:@"/" intoString:NULL];
		[filterScanner scanUpToCharactersFromSet:[NSCharacterSet characterSetWithCharactersInString:@"> /\n]"] intoString:&filterName];
	}
	if ([filterName isEqualToString:@"FlateDecode"]) {
		streamData = [streamData pspdf_zlibInflate];

		NSInteger columns = [self scanIntegerForKey:@"/Columns" inDictionary:streamString defaultValue:1];
		NSInteger predictor = [self scanIntegerForKey:@"/Predictor" inDictionary:streamString defaultValue:1];

		if (predictor == 12) { //PNG Up
			NSInteger decodedLength = [streamData length] - ([streamData length] / (columns + 1));
			NSMutableData *decodedData = [NSMutableData dataWithLength:decodedLength];
			const uint8_t *decodedBytes = (const uint8_t *)[decodedData bytes];
			const uint8_t *streamBytes = (const uint8_t *)[streamData bytes];
			uint8_t predictedByte = 0;
			int indexInDecodedData = 0;
			for (int i=0; i<[streamData length]; i++) {
				if (i % (columns+1) == 0) {
					continue; //skip first byte in each row
				}
				if (indexInDecodedData - columns >= 0) {
					predictedByte = decodedBytes[indexInDecodedData-columns];
				}
				uint8_t byte = predictedByte + streamBytes[i];
				[decodedData replaceBytesInRange:NSMakeRange(indexInDecodedData,1) withBytes:&byte length:1];
				indexInDecodedData++;
			}
			streamData = [NSData dataWithData:decodedData];
		} else if (predictor != 1) {
			// Other predictors don't really make much sense here.
			PSPDFLogError(@"*** Unsupported predictor: %i", predictor);
			return nil;
		}
	}
    else if (filterName) {
		PSPDFLogError(@"*** Unsupported filter: '%@'", filterName);
        return nil;
	}
	return streamData;
}

- (void)parseXrefAtOffset:(NSInteger)offset inXrefTable:(NSMutableDictionary *)xrefTable {
    @autoreleasepool {
        NSMutableDictionary *objects = xrefTable[@"objects"];

        NSMutableString *s = [NSMutableString string];
        NSMutableData *xrefDataBuffer = [NSMutableData new];
        NSData *dataChunk = nil;
        int chunksRead = 0;
        do {
            dataChunk = [self dataAtOffset:offset length:10240]; //read xrefs in chunks of 10KB
            offset += 10240;
            [xrefDataBuffer appendData:dataChunk];
            NSString *textChunk = [[NSString alloc] initWithData:dataChunk encoding:NSASCIIStringEncoding];
            chunksRead++;
            [s appendString:textChunk];
            if ([s rangeOfString:@"%%EOF" options:NSBackwardsSearch].location != NSNotFound) {
                break;
            } else if ([s rangeOfString:@"endobj" options:NSBackwardsSearch].location != NSNotFound) {
                break;
            }
        } while ([dataChunk length] > 0);

        NSScanner *eofScanner = [NSScanner scannerWithString:s];
        [eofScanner scanUpToString:@"%%EOF" intoString:NULL];
        BOOL hasEof = [eofScanner scanString:@"%%EOF" intoString:NULL];
        if (!hasEof) {
            [eofScanner setScanLocation:0];
            [eofScanner scanUpToString:@"endobj" intoString:NULL];
        }

        NSString *xrefAndTrailer = [s substringToIndex:[eofScanner scanLocation]];
        NSData *xrefData = [xrefDataBuffer subdataWithRange:NSMakeRange(0, [eofScanner scanLocation])];

        // Parse xref table
        NSScanner *xrefScanner = [NSScanner scannerWithString:xrefAndTrailer];
        BOOL isXrefTable = [xrefScanner scanString:@"xref" intoString:NULL] || [xrefScanner scanString:@"ref" intoString:NULL];
        if (!isXrefTable) { // xref stream
            NSData *streamData = [self decodeStreamWithData:xrefData];

            // Find out byte widths for fields in stream data
            NSScanner *wScanner = [NSScanner scannerWithString:xrefAndTrailer];
            [wScanner scanUpToString:@"/W" intoString:NULL];
            [wScanner scanString:@"/W" intoString:NULL];
            [wScanner scanUpToString:@"[" intoString:NULL];
            [wScanner scanString:@"[" intoString:NULL];
            int w1 = 0; int w2 = 0; int w3 = 0;
            [wScanner scanInteger:&w1];
            [wScanner scanInteger:&w2];
            [wScanner scanInteger:&w3];

            NSMutableArray *indexNumbers = [NSMutableArray array];
            NSScanner *indexScanner = [NSScanner scannerWithString:xrefAndTrailer];
            [indexScanner scanUpToString:@"/Index" intoString:NULL];
            BOOL hasIndex = [indexScanner scanString:@"/Index" intoString:NULL];
            if (hasIndex) {
                [indexScanner scanUpToString:@"[" intoString:NULL];
                [indexScanner scanString:@"[" intoString:NULL];
                BOOL scannedNumber = NO;
                do {
                    NSInteger n;
                    scannedNumber = [indexScanner scanInteger:&n];
                    if (scannedNumber) {
                        [indexNumbers addObject:@(n)];
                    }
                } while (scannedNumber);
            } else {
                NSInteger size = [self scanIntegerForKey:@"/Size" inDictionary:xrefAndTrailer defaultValue:0];
                // generate default index [0 size]
                [indexNumbers addObject:@0];
                [indexNumbers addObject:@(size)];
            }

            // Pop the range of the first section from the index stack
            int firstObjectNumberInSection = [indexNumbers[0] intValue];
            [indexNumbers removeObjectAtIndex:0];
            int numberOfObjectsInSection = [indexNumbers[0] intValue];
            [indexNumbers removeObjectAtIndex:0];
            int indexInSection = 0;

            const uint8_t *streamBytes = (const uint8_t *)[streamData bytes];
            for (int i=0; i<[streamData length];) {
                int field1 = 1; // default value if w1 is 0
                if (w1 > 0) {
                    field1 = 0;
                    for (int j=(w1-1); j>=0; j--) {
                        uint8_t byte = streamBytes[i+(w1-j-1)];
                        field1 += powf(256.0f, (float)j) * (int)byte;
                    }
                    i += w1;
                }

                long field2 = 0;
                for (int j=(w2-1); j>=0; j--) {
                    uint8_t byte = streamBytes[i+(w2-j-1)];
                    field2 += powf(256.0f, (float)j) * (long)byte;
                }
                i += w2;

                long field3 = 0;
                if (w3 > 0) {
                    for (int j=(w3-1); j>=0; j--) {
                        uint8_t byte = streamBytes[i+(w3-j-1)];
                        field3 += powf(256.0f, (float)j) * (long)byte;
                    }
                    i += w3;
                }

                int objectNumber = firstObjectNumberInSection + indexInSection;
                NSNumber *objectNum = @(objectNumber);
                if (field1 == 1) { // normal object, not compressed
                    if (!objects[objectNum]) {
                        PSPDFXRefEntry *entry = [PSPDFXRefEntry new];
                        entry.byteOffset = field2;
                        entry.objectNumber = objectNumber;
                        objects[objectNum] = entry;
                    }
                } else if (field1 == 2) { // compressed object
                    PSPDFXRefEntry *entry = [PSPDFXRefEntry new];
                    entry.objectNumber = objectNumber;
                    entry.isCompressed = YES;
                    entry.objectStreamNumber = field2;
                    _containsObjectStreams = YES;
                    if (!objects[objectNum]) {
                        objects[objectNum] = entry;
                    }
                } else if (field1 == 0) { // deleted (free) object
                    if (!objects[objectNum]) {
                        PSPDFXRefEntry *entry = [PSPDFXRefEntry new];
                        entry.isDeleted = YES;
                        entry.objectNumber = objectNumber;
                        objects[objectNum] = entry;
                    }
                }

                indexInSection++;
                if (indexInSection > numberOfObjectsInSection-1) {
                    if ([indexNumbers count] > 0) {
                        // Pop the range of the next section from the index stack
                        indexInSection = 0;
                        firstObjectNumberInSection = [indexNumbers[0] intValue];
                        [indexNumbers removeObjectAtIndex:0];
                        numberOfObjectsInSection = [indexNumbers[0] intValue];
                        [indexNumbers removeObjectAtIndex:0];
                    } else break;
                }
            }
        } else { // classic xref table
            while (![xrefScanner isAtEnd]) {
                NSInteger startObjectNumber = 0;
                NSInteger subsectionLength = 0;
                BOOL isSubsection = [xrefScanner scanInteger:&startObjectNumber];
                if (!isSubsection) {
                    break;
                }
                [xrefScanner scanInteger:&subsectionLength];
                for (int i=0; i<subsectionLength; i++) {
                    NSInteger objectByteOffset = 0;
                    NSInteger objectGeneration = 0;
                    NSString *objectType = nil;
                    [xrefScanner scanInteger:&objectByteOffset];
                    [xrefScanner scanInteger:&objectGeneration];
                    [xrefScanner scanUpToCharactersFromSet:[NSCharacterSet newlineCharacterSet] intoString:&objectType];
                    NSInteger objectNumber = startObjectNumber + i;
                    NSNumber *existingEntry = objects[@(objectNumber)];
                    if ([objectType hasPrefix:@"n"]) {
                        if (!existingEntry) {
                            PSPDFXRefEntry *entry = [[PSPDFXRefEntry alloc] init];
                            entry.objectNumber = objectNumber;
                            entry.byteOffset = (long)objectByteOffset;
                            objects[@(objectNumber)] = entry;
                        }
                    } else {
                        if (!existingEntry) {
                            PSPDFXRefEntry *entry = [[PSPDFXRefEntry alloc] init];
                            entry.isDeleted = YES;
                            entry.objectNumber = objectNumber;
                            objects[@(objectNumber)] = entry;
                        }
                    }
                }
            }
        }
        
        // Find root (catalog) object
        NSInteger root = [self scanIntegerForKey:@"/Root" inDictionary:xrefAndTrailer defaultValue:0];
        if (root != 0) {
            if (![objects[@(root)] isDeleted]) {
                if (!xrefTable[@"root"]) {
                    // update catalog entry if not yet present
                    xrefTable[@"root"] = @(root);
                }
            }
        }
        
        NSInteger info = [self scanIntegerForKey:@"/Info" inDictionary:xrefAndTrailer defaultValue:0];
        if (info != 0) {
            if (!xrefTable[@"info"]) {
                // update catalog entry if not yet present
                xrefTable[@"info"] = @(info);
            }
        }
        
        // Find /ID
        NSScanner *idScanner = [NSScanner scannerWithString:xrefAndTrailer];
        [idScanner scanUpToString:@"/ID" intoString:NULL];
        BOOL hasID = [idScanner scanString:@"/ID" intoString:NULL];
        if (hasID) {
            NSString *docID = nil;
            [idScanner scanUpToString:@"]" intoString:&docID];
            if (!xrefTable[@"id"]) {
                xrefTable[@"id"] = [NSString stringWithFormat:@"%@]", docID];
            }
        }
        
        // Find /Encrypt
        NSScanner *encryptScanner = [NSScanner scannerWithString:xrefAndTrailer];
        [encryptScanner scanUpToString:@"/Encrypt" intoString:NULL];
        BOOL hasEncrypt = [encryptScanner scanString:@"/Encrypt" intoString:NULL];
        if (hasEncrypt) {
            NSInteger encrypt = 0;
            [encryptScanner scanInteger:&encrypt];
            if (encrypt != 0) {
                if (!xrefTable[@"encrypt"]) {
                    xrefTable[@"encrypt"] = @(encrypt);
                }
            }
        }
        
        // Find offset of previous xref section
        NSInteger prev = [self scanIntegerForKey:@"/Prev" inDictionary:xrefAndTrailer defaultValue:0];
        if (prev != 0 && prev != offset) {
            // Parse previous xref table/stream recursively
            @autoreleasepool {
                [self parseXrefAtOffset:prev inXrefTable:xrefTable];
            }
        }
    }
}

@end

@implementation PSPDFXRefEntry

- (NSString *)description {
    return [NSString stringWithFormat:@"<%@: %p #%d offset:%ld streamNumber:%d isCompressed:%d isDeleted:%d>", NSStringFromClass([self class]), self, _objectNumber, _byteOffset, _objectStreamNumber, _isCompressed, _isDeleted];
}

@end
