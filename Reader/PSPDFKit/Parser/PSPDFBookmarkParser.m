//
//  PSPDFBookmarkParser.m
//  PSPDFKit
//
//  Copyright (c) 2012 Peter Steinberger. All rights reserved.
//

#import "PSPDFBookmarkParser.h"
#import "PSPDFBookmark.h"
#import "PSPDFDocument.h"

NSString *const kPSPDFBookmarksChangedNotification = @"kPSPDFBookmarksChangedNotification";

@implementation PSPDFBookmarkParser

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (id)initWithDocument:(PSPDFDocument *)document {
    if ((self = [super init])) {
        _document = document;
        _bookmarkQueue = pspdf_dispatch_queue_create("com.PSPDFKit.bookmarkQueue", 0);
    }
    return self;
}

- (void)dealloc {
    PSPDFDispatchRelease(_bookmarkQueue);
}

- (NSString *)description {
    return [NSString stringWithFormat:@"<%@: %p path:%@ %@>", NSStringFromClass([self class]), self, self.bookmarkPath, self.bookmarks];
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public

// set and write bookmarks to disk
- (void)setBookmarks:(NSArray *)bookmarks {
    dispatch_async(_bookmarkQueue, ^{
        _bookmarks = [bookmarks mutableCopy];
        [self saveBookmarksWithError:nil];
    });
    [[NSNotificationCenter defaultCenter] postNotificationName:kPSPDFBookmarksChangedNotification object:self];
}

// read bookmarks from disk.
- (NSArray *)bookmarks {
    __block NSArray *bookmarks;
    pspdf_dispatch_sync_reentrant(_bookmarkQueue, ^{
        if (!_bookmarks) {
            _bookmarks = [[self loadBookmarksWithError:nil] mutableCopy];
        }
        bookmarks = _bookmarks;
    });
    return bookmarks;
}

- (BOOL)addBookmarkForPage:(NSUInteger)page {
    __block BOOL success = YES;
    dispatch_sync(_bookmarkQueue, ^{
        PSPDFBookmark *bookmark = [[PSPDFBookmark alloc] initWithPage:page];
        if ([self.bookmarks containsObject:bookmark] || page >= [self.document pageCount]) {
            success = NO;
        }else {
            [_bookmarks addObject:bookmark];
            dispatch_async(_bookmarkQueue, ^{
                [self saveBookmarksWithError:nil];
            });
        }
    });
    [[NSNotificationCenter defaultCenter] postNotificationName:kPSPDFBookmarksChangedNotification object:self];
    return success;
}

- (BOOL)removeBookmarkForPage:(NSUInteger)page {
    __block BOOL success = YES;
    dispatch_sync(_bookmarkQueue, ^{

        // find the bookmark we need
        PSPDFBookmark *bookmark = [[self.bookmarks filteredArrayUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(PSPDFBookmark *evaluatedBookmark, NSDictionary *bindings) {
            return evaluatedBookmark.page == page;
        }]] ps_firstObject];

        if (!bookmark) {
            success = NO;
        }else {
            [_bookmarks removeObject:bookmark];
            dispatch_async(_bookmarkQueue, ^{
                [self saveBookmarksWithError:nil];
            });
        }
    });
    [[NSNotificationCenter defaultCenter] postNotificationName:kPSPDFBookmarksChangedNotification object:self];
    return success;
}

- (BOOL)clearAllBookmarksWithError:(NSError **)error {
    dispatch_sync(_bookmarkQueue, ^{
        [_bookmarks removeAllObjects];
    });
    BOOL success = [[NSFileManager new] removeItemAtPath:[self bookmarkPath] error:error];
    [[NSNotificationCenter defaultCenter] postNotificationName:kPSPDFBookmarksChangedNotification object:self];
    return success;
}

- (PSPDFBookmark *)bookmarkForPage:(NSUInteger)page {
    __block PSPDFBookmark *bookmark = nil;
    dispatch_sync(_bookmarkQueue, ^{
        for (PSPDFBookmark *aBookmark in self.bookmarks) {
            if (aBookmark.page == page) {
                bookmark = aBookmark; break;
            }
        }
    });
    return bookmark;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Private

- (NSString *)bookmarkPath {
    return [self.document.cacheDirectory stringByAppendingPathComponent:@"bookmarks.plist"];
}

- (NSArray *)loadBookmarksWithError:(NSError **)error {
    NSMutableArray *newBookmarks = [NSMutableArray array];
    NSArray *bookmarksArray = [[NSArray alloc] initWithContentsOfFile:[self bookmarkPath]];
    for (NSNumber *pageNumber in bookmarksArray) {
        [newBookmarks addObject:[[PSPDFBookmark alloc] initWithPage:[pageNumber unsignedIntegerValue]]];
    }
    return newBookmarks;
}

- (BOOL)saveBookmarksWithError:(NSError **)error {
    NSString *bookmarkPath = [self bookmarkPath];
    __block NSError *localError = nil;
    dispatch_async(_bookmarkQueue, ^{
        if ([_bookmarks count]) {
            [self.document ensureCacheDirectoryExistsWithError:&localError];

            // we transform this into a clssic plist for better readability.
            NSMutableArray *bookmarkArray = [NSMutableArray arrayWithCapacity:[_bookmarks count]];
            for (PSPDFBookmark *bookmark in _bookmarks) {
                [bookmarkArray addObject:@(bookmark.page)];
            }

            NSData *data = [NSPropertyListSerialization dataWithPropertyList:bookmarkArray format:NSPropertyListBinaryFormat_v1_0 options:0 error:&localError];
            if (!data) {
                PSPDFLogWarning(@"Failed to serialize data: %@", localError);
            }else {
                if (![data writeToFile:bookmarkPath options:NSDataWritingAtomic error:&localError]) {
                    PSPDFLogWarning(@"Failed to write atomically to path %@: %@", bookmarkArray, localError);
                }
            }
        }else {
            // ignore error (maybe file doesn't exist at all)
            [[NSFileManager new] removeItemAtPath:bookmarkPath error:&localError];
        }
        if (error && localError) *error = localError;
    });
    
    return localError == nil;
}

@end
