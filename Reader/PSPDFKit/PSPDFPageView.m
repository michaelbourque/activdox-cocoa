//
//  PSPDFPageView.m
//  PSPDFKit
//
//  Copyright 2011-2012 Peter Steinberger. All rights reserved.
//

#import "PSPDFKit.h"
#import "PSPDFPageInfo.h"
#import "PSPDFViewController+Internal.h"
#import "PSPDFLinkAnnotationView.h"
#import "PSPDFHighlightAnnotationView.h"
#import "PSPDFTextSelectionView.h"
#import "PSPDFTextParser.h"
#import "PSPDFConverter.h"
#import "PSPDFNoteAnnotationView.h"
#import "PSPDFNoteAnnotationController.h"
#import "PSPDFMenuItem.h"
#import "PSPDFSimplePageViewController.h"
#import "PSPDFColorSelectionViewController.h"
#import "PSPDFKitGlobal+Internal.h"
#import "PSPDFLoupeView.h"
#import "PSPDFAnnotationController.h"
#import "PSPDFSinglePageViewController.h"
#import "PSPDFResizableView.h"
#import "PSPDFOrderedDictionary.h"
#import "PSPDFAnnotationToolbar.h"
#import "PSPDFAnnotationBarButtonItem.h"
#import <QuartzCore/QuartzCore.h>
#import <objc/runtime.h>

// subclasses just for naming things; easier debugging and subclassing.
@interface PSPDFRenderImageView  : UIImageView @end @implementation PSPDFRenderImageView @end
@interface PSPDFContentImageView : UIImageView @end @implementation PSPDFContentImageView @end
@interface PSPDFRenderStatusView : UIImageView @end @implementation PSPDFRenderStatusView @end

@interface PSPDFPageView() <PSPDFColorSelectionViewControllerDelegate, PSPDFNoteAnnotationControllerDelegate, UIAccessibilityReadingContent> {
    CFAbsoluteTime _lastScrollUpdate;
	CGPoint _lastScrollOffset;
    CGPoint _targetContentOffset;
    float _lastZoomScale;
    CGRect _currentClipRect;
    id _openAnnotationLinkTargetSheet;

    NSMutableDictionary *_annotationViews;
    NSMutableSet *_annotationViewsToBeRemoved;
    CGPDFDocumentRef _pdfDocument;
    CGPDFPageRef _pdfPage;
    struct {
        unsigned int hasLoadedAnnotationViews:1;
        unsigned int targetContentOffsetValid:1;
        unsigned int annotationSetToOverlayTemporarily:1;
        unsigned int isWithinDisplayDocumentMethod:1;
        unsigned int isCancellingLongPressGesture:1;
    } _flags;
}

@property (atomic,    assign) NSUInteger page;
@property (atomic,    strong) PSPDFDocument *document;
@property (atomic,    strong) NSBlockOperation *annotationLoadOperation;
@property (atomic,    strong) NSBlockOperation *textParserLoadOperation;
@property (atomic,    weak)   PSPDFViewController *pdfController;
@property (nonatomic, strong) PSPDFPageInfo *pageInfo;
@property (nonatomic, assign) CGFloat pdfScale;
@property (nonatomic, strong) UIImageView *contentView;
@property (nonatomic, assign, getter=isRendering)  BOOL rendering;
@property (nonatomic, assign) BOOL suspendUpdate;
@property (nonatomic, copy)   PSPDFOrderedDictionary *colorOptions;
@property (nonatomic, strong) PSPDFTextSelectionView *selectionView;
@property (nonatomic, strong) PSPDFAnnotation *selectedAnnotation;
@property (nonatomic, strong) PSPDFResizableView *annotationSelectionView;
@property (nonatomic, strong) UIView <PSPDFAnnotationView> *draggingAnnotationView;
@property (nonatomic, copy) NSSet *subviewsToKeep;

// mirror rendering properties on PSPDFViewController
@property (nonatomic, assign) PSPDFAnnotationType renderAnnotationTypes;
@property (nonatomic, strong) UIColor *renderBackgroundColor;
@property (nonatomic, assign) BOOL renderInvertEnabled;
@property (nonatomic, assign) CGFloat renderContentOpacity;
@end

NSString *const kPSPDFHidePageHUDElements = @"kPSPDFHidePageHUDElements";

@implementation PSPDFPageView

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (id)initWithFrame:(CGRect)frame {
    @throw [NSException exceptionWithName:NSInvalidArgumentException reason:@"You need to use initWithFrame:pdfController:." userInfo:nil];
}

- (id)initWithFrame:(CGRect)frame pdfController:(PSPDFViewController *)pdfController {
    if ((self = [super initWithFrame:frame])) {
        NSAssert([NSThread isMainThread], @"Must run on main thread (init PSPDFPageView)");
        self.isAccessibilityElement = YES;
        _pdfController = pdfController;
        NSNotificationCenter *dnc = [NSNotificationCenter defaultCenter];
        [dnc addObserver:self selector:@selector(willChangeInterfaceOrientationNotification:) name:PSPDFViewControllerWillChangeOrientationNotification object:nil];
        [dnc addObserver:self selector:@selector(annotationsChanged:) name:PSPDFAnnotationChangedNotification object:nil];
        [dnc addObserver:self selector:@selector(discardSelection) name:kPSPDFHidePageHUDElements object:nil];

        // cache for annotation objects
        _annotationViews = [NSMutableDictionary new];
        _annotationViewsToBeRemoved = [NSMutableSet new];

        // make transparent
        self.backgroundColor = [UIColor clearColor];
        self.opaque = NO;

        self.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        _shadowOpacity = 0.7f;

        // setup background image view
        _contentView = [[[pdfController classForClass:[PSPDFContentImageView class]] alloc] initWithImage:nil];
        _contentView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        _contentView.opaque = YES;
        _contentView.clipsToBounds = YES;
        _contentView.frame = self.bounds;
        [self addSubview:_contentView];

        CGFloat screenMax = fmaxf([UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height);
        CGFloat screenExtensionFactor = PSPDFIsCrappyDevice() ? 0 : 100;
        _renderSize = CGSizeMake(screenMax + screenExtensionFactor, screenMax + screenExtensionFactor);

        _renderView = [[[pdfController classForClass:[PSPDFRenderImageView class]] alloc] initWithFrame:CGRectMake(0, 0, _renderSize.width, _renderSize.height)];
		[_contentView addSubview:_renderView];

        // Show render margins if debug is enabled
        if (kPSPDFDebugScrollViews) {
            _renderView.layer.borderColor = [UIColor orangeColor].CGColor;
            _renderView.layer.borderWidth = 5.f;
        }

        // create selectionView
        _selectionView = [[[pdfController classForClass:[PSPDFTextSelectionView class]] alloc] initWithFrame:self.bounds];
        _selectionView.pageView = self;
		_selectionView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        _selectionView.frame = self.bounds;
		_selectionView.selectedGlyphs = nil;
		[self addSubview:_selectionView];

        // show small rendering status item
        _renderStatusView = [[[pdfController classForClass:[PSPDFRenderStatusView class]] alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
        _renderStatusView.hidden = YES;
        UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        [activityIndicator sizeToFit];
        activityIndicator.hidden = YES;
        [_renderStatusView addSubview:activityIndicator];
        _renderStatusView.backgroundColor = [UIColor clearColor];
        //_renderStatusView.backgroundColor = [UIColor blueColor]; // DEBUG
        [self addSubview:_renderStatusView];
        [activityIndicator startAnimating]; // expensive, do early
        _renderStatusViewOffset = 30;
        _centerRenderStatusView = NO;

        // remember all subviews that are there at creation time.
        _subviewsToKeep = [NSSet setWithArray:self.subviews];

        [PSPDFMenuItem installMenuHandlerForObject:self];
    }
    return self;
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [self recycleAnnotationViews];

    self.document = nil; // for queue image loading
    [self.renderStatusView removeFromSuperview]; // might be added to the parent
    [[PSPDFRenderQueue sharedRenderQueue] cancelRenderingForDelegate:self async:NO];

    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(startCachingDocument) object:nil];
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(loadPageAnnotationsWithAnimation) object:nil];
}

- (NSString *)description {
    NSString *defaultDescription = [super description]; // UIView's default description
    NSString *description = [NSString stringWithFormat:@"%@ isRendering:%@ page:%d document:%@>", defaultDescription, self.isRendering ? @"YES" : @"NO", self.page, self.document];
    return description;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public

- (void)setRenderSize:(CGSize)renderSize {
    if (CGSizeEqualToSize(renderSize, _renderSize)) {
        _renderSize = renderSize;
        [self setNeedsLayout];
    }
}

- (void)updateView {
    [self updateRenderViewForced:YES];
    [self requestRenderedFullPageImage];
}

- (void)setUpdateShadowBlock:(void (^)(PSPDFPageView *))updateShadowBlock {
    _updateShadowBlock = updateShadowBlock;
    [self setNeedsLayout];
}

- (CGRect)visibleRect {
    UIScrollView *scrollView = [self scrollView];
    CGRect visibleRect;
    visibleRect.origin = scrollView.contentOffset;
    visibleRect.size = scrollView.bounds.size;

    float theScale = 1.0 / scrollView.zoomScale;
    visibleRect.origin.x *= theScale;
    visibleRect.origin.y *= theScale;
    visibleRect.size.width *= theScale;
    visibleRect.size.height *= theScale;

    // hack to get the correct offset for right page.
    CGRect ownFrame = self.frame;
    if ([self.superview isKindOfClass:[PSPDFSinglePageView class]]) {
        ownFrame = self.superview.frame;
    }

    // compensate pageView offsets
    visibleRect.origin.x -= ownFrame.origin.x;
    visibleRect.origin.y -= ownFrame.origin.y;

    return visibleRect;
}

// don't use the renderView if zoomScale is smaller than 1.0 or exact 1.0.
#define kPSPDFZoomScale @"zoomScale"
- (NSDictionary *)renderOptionsDictWithZoomScale:(CGFloat)zoomScale {
    // auto-fetch annotations
    NSDictionary *renderOptions = @{
    kPSPDFAnnotationAutoFetchTypes : @(self.renderAnnotationTypes),
kPSPDFZoomScale: @(zoomScale),
    NSStringFromSelector(@selector(bounds)) : BOXED(self.bounds),
    kPSPDFPageColor : self.renderBackgroundColor,
    kPSPDFInvertRendering : @(self.renderInvertEnabled),
    kPSPDFContentOpacity : @(self.renderContentOpacity)
    };

    // add renferene of the pdfController to be able to call back the delegate later on.
    PSPDFDocument *document = self.document;
    if (document) {
        PSPDFViewController *pdfController = self.pdfController;
        if (pdfController) {
            NSMutableDictionary *mutableRenderOptions = [renderOptions mutableCopy];
            mutableRenderOptions[@"pdfController"] = pdfController;
            renderOptions = [mutableRenderOptions copy];
        }
    }

    return renderOptions;
}

- (void)updateRenderView {
    [self updateRenderViewForced:NO];
}

- (void)updateRenderViewForced:(BOOL)forced {
    UIScrollView *scrollView = [self scrollView];
    if (!scrollView || self.suspendUpdate || _renderSize.width == 0 || _contentView.frame.size.width == 0) return;

    // simply hide renderView if we are zoomed out
    if (scrollView.zoomScale <= 1.0f) { _renderView.hidden = YES; return; }

    CGPoint contentOffset = _flags.targetContentOffsetValid ? _targetContentOffset : scrollView.contentOffset;
    CGRect rectInScrollView = [self convertRect:self.bounds toView:[self scrollView]];

    // if there is a size given, ignore PageView frame and calculate from scrollView center.
    CGPoint center = CGPointMake(contentOffset.x - rectInScrollView.origin.x + scrollView.bounds.size.width/2,
                                 contentOffset.y - rectInScrollView.origin.y + scrollView.bounds.size.height/2);
    CGRect clipRect = CGRectMake(center.x - _renderSize.width / 2, center.y - _renderSize.height / 2, _renderSize.width, _renderSize.height);

    // stop here if clipRect is the same as before and renderView isn't hidden
    if (!forced && !_renderView.hidden && CGRectEqualToRect(_currentClipRect, clipRect)) return;

    CGRect pageRect = self.contentView.bounds;
	_currentClipRect = clipRect;
	CGSize fullSize = CGSizeMake(pageRect.size.width * scrollView.zoomScale, pageRect.size.height * scrollView.zoomScale);

	if (fullSize.width <= _contentView.frame.size.width+1 && fullSize.height <= _contentView.frame.size.height+1) {
		// no need to render, preview size is sufficient
		_renderView.hidden = YES;
	} else {
        PSPDFLogVerbose(@"rendering: fullSize:%@ clipRect:%@", NSStringFromCGSize(fullSize), NSStringFromCGRect(clipRect));

        // request to render us the new page
        [[PSPDFRenderQueue sharedRenderQueue] cancelRenderingForDelegate:self async:YES];
        if (self.document && self.pdfController) {
            NSDictionary *renderOptions = [self renderOptionsDictWithZoomScale:scrollView.zoomScale];
            [[PSPDFRenderQueue sharedRenderQueue] requestRenderedImageForDocument:self.document forPage:self.page withSize:fullSize clippedToRect:clipRect withAnnotations:nil options:renderOptions delegate:self];
        }
        self.rendering = YES;
	}
}

- (void)setShadowOpacity:(float)shadowOpacity {
    _shadowOpacity = shadowOpacity;
    [self setNeedsLayout]; // rebuild shadow
}

- (void)setRendering:(BOOL)rendering {
    if (rendering != _rendering) {
        _rendering = rendering;

        // UI stuff, needs to be on main.
        dispatch_async(dispatch_get_main_queue(), ^{
            BOOL hide = YES;
            if (self.document) {
                PSPDFViewController *pdfController = self.pdfController;
                hide = !pdfController.isRenderAnimationEnabled || (pdfController.pageTransition == PSPDFPageCurlTransition && !([pdfController isDoublePageMode] && [pdfController isRightPageInDoublePageMode:self.page]));
            }
            BOOL hideRenderView = !rendering || hide;
            self.renderStatusView.hidden = hideRenderView;

            // start/stop activity indicator
            UIActivityIndicatorView *activityIndicator = [_renderStatusView.subviews ps_firstObject];
            if ([activityIndicator isKindOfClass:[UIActivityIndicatorView class]]) {
                activityIndicator.hidden = hideRenderView;
            }
        });
    }
}

// Dynamically search for the parent scrollView.
- (PSPDFScrollView *)scrollView {
    PSPDFScrollView *scrollView = nil;

    if (self.pdfController.pageTransition == PSPDFPageScrollPerPageTransition || self.pdfController.pageTransition == PSPDFPageScrollContinuousTransition) {
        scrollView = (PSPDFScrollView *)self.superview;
        while (scrollView && ![scrollView isKindOfClass:[PSPDFScrollView class]]) {
            scrollView = (PSPDFScrollView *)scrollView.superview;
        }
    }else {
        // PSPDFPageCurlTransition or PSPDFPageFlipTarnsition have one single scrollView.
        scrollView = (PSPDFScrollView *)self.pdfController.pagingScrollView;
    }

    return scrollView;
}

- (void)requestRenderedFullPageImage {
    if (_suspendUpdate) return;

    PSPDFDocument *document = self.document; // get hard reference since document could vanish in the mean time.
    if (![document isValid] || CGRectIsEmpty(self.bounds)) return;

    self.rendering = YES;
    NSDictionary *renderDict = [self renderOptionsDictWithZoomScale:1.f];
    [[PSPDFRenderQueue sharedRenderQueue] requestRenderedImageForDocument:document
                                                                  forPage:_page
                                                                 withSize:self.bounds.size
                                                            clippedToRect:self.bounds
                                                          withAnnotations:nil // auto-fetched
                                                                  options:renderDict
                                                                 delegate:self];
}


static dispatch_queue_t _pageViewImageLoader;
- (dispatch_queue_t)pageViewImageLoader {
    static dispatch_once_t onceToken;
	dispatch_once(&onceToken, ^{
		_pageViewImageLoader = pspdf_dispatch_queue_create("com.PSPDFKit.pageViewImageLoader", NULL);
	});
	return _pageViewImageLoader;
}

- (void)displayDocument:(PSPDFDocument *)document page:(NSUInteger)page pageRect:(CGRect)pageRect scale:(CGFloat)scale delayPageAnnotations:(BOOL)delayPageAnnotations pdfController:(PSPDFViewController *)pdfController {
    BOOL skipImageLoading = document == self.document && page == self.page;
    self.document = document;
    self.page = page;
    self.pdfScale = scale;
    self.contentView.backgroundColor = [document backgroundColorForPage:page];
    self.pdfController = pdfController;
    self.pageInfo = [document pageInfoForPage:page]; // cache for faster usage later.

    // copy render properties
    self.renderAnnotationTypes = pdfController.renderAnnotationTypes;
    self.renderBackgroundColor = pdfController.renderBackgroundColor;
    self.renderContentOpacity = pdfController.renderContentOpacity;
    self.renderInvertEnabled = pdfController.renderInvertEnabled;

    // make sure to not be hidden!
    self.hidden = NO;

    // not suspending updates anymore
    self.suspendUpdate = NO;

    // prevents NaN-crashes
    if (pageRect.size.width < 10 || pageRect.size.height < 10) {
        if ([document pageCount]) {
            PSPDFLogWarning(@"Invalid page rect given: %@ (stopping rendering here)", NSStringFromCGRect(pageRect));
        }
        return;
    }

    _flags.isWithinDisplayDocumentMethod = YES;

    pageRect.size = PSPDFSizeForScale(pageRect.size, scale);
    self.frame = (CGRect){.size=pageRect.size}; // don't set custom offset here.

    // if full-size pageimage is in memory, use it -> insta-sharp!
    BOOL immediatelyRenderFullPage = YES;
    if (!skipImageLoading) {
        PSPDFPageRenderingMode renderingMode = pdfController.renderingMode;

        // try to load cached native-size image
        UIImage *backgroundImage = nil;
        if (renderingMode != PSPDFPageRenderingModeThumbnailThenRender && renderingMode != PSPDFPageRenderingModeRender) {
            backgroundImage = [[PSPDFCache sharedCache] imageForDocument:document page:page size:PSPDFSizeNative];
        }

        // fallback to thumbnail image, if it's on memory or on disk
        if (!backgroundImage) {
            // if this is enabled; try loading the full-size image.
            if (renderingMode == PSPDFPageRenderingModeFullPageBlocking) {
                backgroundImage = [[PSPDFCache sharedCache] cachedImageForDocument:document page:page size:PSPDFSizeNative preload:NO];
            }

            if (!backgroundImage) {
                // this may block the main thread for a bit, but we don't wanna flash-in the thumbnail as soon as its there.
                if (renderingMode == PSPDFPageRenderingModeThumbailThenFullPage || renderingMode == PSPDFPageRenderingModeThumbnailThenRender) {
                    backgroundImage = [[PSPDFCache sharedCache] cachedImageForDocument:document page:page size:PSPDFSizeThumbnail preload:NO];
                }

                // load pre-rendered image before we use our own renderer.
                if (renderingMode == PSPDFPageRenderingModeFullPage || renderingMode == PSPDFPageRenderingModeThumbailThenFullPage) {
                    self.rendering = YES;
                    if ([[PSPDFCache sharedCache] isImageCachedForDocument:document page:page size:PSPDFSizeNative]) {

                        // use a queue to pre-load images from the cache, only then start rendering the full page.
                        immediatelyRenderFullPage = NO;
                        dispatch_async([self pageViewImageLoader], ^{
                            // check early, check often. PSPDFPageView could change anytime.
                            if (self.document != document || self.page != page) return;
                            UIImage *fullBackgroundImage = [[PSPDFCache sharedCache] cachedImageForDocument:document page:page size:PSPDFSizeNative preload:YES];
                            if (self.document != document || self.page != page) return;
                            dispatch_async(dispatch_get_main_queue(), ^{
                                if (self.document != document || self.page != page) return;
                                BOOL animated = !PSPDFIsCrappyDevice() && [self.pdfController viewDidAppearCalled];
                                [self setBackgroundImage:fullBackgroundImage animated:animated];
                            });
                            [self requestRenderedFullPageImage];
                        });
                    }
                }
            }
        }
        [self setBackgroundImage:backgroundImage animated:NO];
    }

    // cancel renderings and render new full page.
    [[PSPDFRenderQueue sharedRenderQueue] cancelRenderingForDelegate:self async:YES];

    // directly start rendering
    if (immediatelyRenderFullPage) {
        [self requestRenderedFullPageImage];
    }

    // start caching document after two seconds
    [self performSelector:@selector(startCachingDocument) withObject:nil afterDelay:2.0];

    // add delay for annotation loading; improve scrolling speed (but reload instantly if already there)
    if ([_annotationViews count] > 0 || !delayPageAnnotations) {
        [self loadPageAnnotationsAnimated:NO];
    }else {
        [self performSelector:@selector(loadPageAnnotationsWithAnimation) withObject:nil afterDelay:kPSPDFInitialAnnotationLoadDelay];
    }

    [self setNeedsLayout];

#ifdef DEBUG
    BOOL showTextColumnFlowData = NO;
    if (showTextColumnFlowData) {
        [self.selectionView showTextFlowData:YES animated:YES];
    }
#endif

    _flags.isWithinDisplayDocumentMethod = NO;
}

// animated will be ignored, since it's too much of a performance hit.
- (void)setBackgroundImage:(UIImage *)image animated:(BOOL)animated {
    UIImageView *contentView = self.contentView;
    if (contentView.image != image) {

        /*
         if (animated && kPSPDFAnimationDuration > 0.f) {
         [contentView.layer addAnimation:PSPDFFadeTransitionWithDuration(kPSPDFAnimationDuration) forKey:@"image"];
         }
         */

        contentView.image = image;
        contentView.opaque = YES;
        contentView.clearsContextBeforeDrawing = NO;
    }
    contentView.backgroundColor = image ? [UIColor clearColor] : [self.document backgroundColorForPage:self.page];

    //CGRect rectInWindow = [self.backgroundImageView convertRect:self.backgroundImageView.frame toView:self.backgroundImageView.window];

    // if image is readonably close to target size, don't scale it (fixes blurry text)
    /*
     BOOL isFullSize = NO;
     if (image) {
     int widthDiff = abs(self.backgroundImageView.frame.size.width - image.size.width);
     int heightDiff = abs(self.backgroundImageView.frame.size.height - image.size.height);
     isFullSize = widthDiff < 2 && heightDiff < 2;
     }*/

    //if(isFullSize) {
    //    backgroundImageView_.contentMode = UIViewContentModeTopLeft; // doesn't stretch image
    //}else {
    contentView.contentMode = UIViewContentModeScaleToFill;
    //}
}

- (void)prepareForReuse {
    // if there's a pdfController attached, call delegate! (else just call to nil)
    [self.pdfController delegateWillUnloadPageView:self];

    // always recycle annotation views
    [self recycleAnnotationViews];

    // reset variables
    self.pdfController = nil;
    self.document = nil;
    self.page = 0;
    self.suspendUpdate = NO;

    // ensure any selection is gone
    self.selectedAnnotation = nil;
    [self discardSelection];
    [self.selectionView.loupeView hideLoupeAnimated:NO];

    // stop ongoing rendering requests
    [[PSPDFRenderQueue sharedRenderQueue] cancelRenderingForDelegate:self async:YES];
    self.rendering = NO;

    // stop ongoing cache requests
    [self.annotationLoadOperation cancel]; self.annotationLoadOperation = nil;
    [self.textParserLoadOperation cancel]; self.textParserLoadOperation = nil;

    // remove all other unknown views. (Only allow classes with PSPDF* has prefix)
    // this is to be compatible with PSPDFKit v1 that always destroyed the PSPDFPageView.
    NSMutableSet *unknownViewSet = [NSMutableSet setWithArray:self.subviews];
    [unknownViewSet minusSet:self.subviewsToKeep];
    [unknownViewSet makeObjectsPerformSelector:@selector(removeFromSuperview)];

    // cancel late selectors
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(startCachingDocument) object:nil];
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(loadPageAnnotationsWithAnimation) object:nil];
}

- (void)setHidden:(BOOL)hidden {
    if (hidden != self.hidden) {
        [super setHidden:hidden];
        [self callAnnotationVisibleDelegateToShow:!hidden];
    }
}

// detects when the controller/view goes offscreen - pause videos etc
- (void)willMoveToWindow:(UIWindow *)newWindow {
    PSPDFLogVerbose(@"new window for page: %@", newWindow);
    // inform annotations
    if (self.document) {
        [self callAnnotationVisibleDelegateToShow:newWindow != nil];
    }
}

// inform annotations that the parent size has changed
- (void)callChangePageFrameOnAnnotationViews {
    [[self visibleAnnotationViews] enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        if ([obj respondsToSelector:@selector(didChangePageFrame:)]) {
            [obj didChangePageFrame:self.frame];
        }
    }];
}

- (void)setFrame:(CGRect)frame {
    CGRect oldFrame = self.frame;
    [super setFrame:frame];

    if (self.pdfController) {

        // invalidate render view (else we get artefacts on rotation)
        if (!_flags.isWithinDisplayDocumentMethod) {
            if (!CGRectIsEmpty(frame) && (!CGRectEqualToRect(frame, oldFrame) || (self.contentView && !self.contentView.image))) {
                self.renderView.hidden = YES;
                [self requestRenderedFullPageImage];
                [self updateRenderView];
            }
        }

        // ensure selection is updated
        [self updateSelection];

        [self callChangePageFrameOnAnnotationViews];
    }
}

// TODO: animate shadow
- (void)updateShadowAnimated:(BOOL)animated {
    if (self.isShadowEnabled) {
        // TODO: make one library for shadow services (see PSPDFScrollView)
        CALayer *backgroundLayer = self.layer;
        backgroundLayer.shadowColor = [UIColor blackColor].CGColor;
        backgroundLayer.shadowOffset = PSIsIpad() ? CGSizeMake(10.0f, 10.0f) : CGSizeMake(8.0f, 8.0f);
        backgroundLayer.shadowRadius = 4.0f;
        backgroundLayer.masksToBounds = NO;
        CGSize size = self.bounds.size;
        CGFloat moveShadow = -12;
        UIBezierPath *path = [UIBezierPath bezierPathWithRect:CGRectMake(moveShadow, moveShadow, size.width+fabs(moveShadow/2), size.height+fabs(moveShadow/2))];
        backgroundLayer.shadowOpacity = _shadowOpacity;
        backgroundLayer.shadowPath = path.CGPath;

        if (_updateShadowBlock) {
            _updateShadowBlock(self);
        }
    }
}

// places the statusView
- (void)updateStatusView {
    PSPDFScrollView *scrollView = self.scrollView;
    CGRect rectInScrollView = [self convertRect:self.bounds toView:scrollView];
    BOOL isDualPage = [self.pdfController isDoublePageModeForPage:self.page];

    //NSLog(@"cOffset:%@ rectInS:%@ sBounds:%@ cView:%@", NSStringFromCGPoint(scrollView.contentOffset), NSStringFromCGRect(rectInScrollView), NSStringFromCGRect(scrollView.bounds), NSStringFromCGRect(_contentView.frame));

    CGPoint center;
    if (self.centerRenderStatusView) {
        center = CGPointMake(scrollView.contentOffset.x - rectInScrollView.origin.x + scrollView.bounds.size.width/2, scrollView.contentOffset.y - rectInScrollView.origin.y + scrollView.bounds.size.height/2);
    }else {
        CGPoint topRightGap = CGPointMake(roundf((scrollView.bounds.size.width-_contentView.frame.size.width)/2), roundf((scrollView.bounds.size.height-_contentView.frame.size.height)/2));
        if (topRightGap.x < 0) topRightGap.x = 0;
        if (topRightGap.y < 0) topRightGap.y = 0;

        CGPoint topRightOffsetGap = CGPointMake(roundf((scrollView.bounds.size.width-rectInScrollView.size.width)/2), roundf((scrollView.bounds.size.height-rectInScrollView.size.height)/2));
        if (topRightOffsetGap.x < 0) topRightOffsetGap.x = 0;
        if (topRightOffsetGap.y < 0) topRightOffsetGap.y = 0;

        CGFloat offset = self.renderStatusViewOffset;
        center = CGPointMake(scrollView.contentOffset.x - rectInScrollView.origin.x + (_contentView.frame.size.width/(isDualPage ? 1 : 2)) - offset + scrollView.bounds.size.width/2 - topRightGap.x + topRightOffsetGap.x, scrollView.contentOffset.y - rectInScrollView.origin.y - (_contentView.frame.size.height/2) + offset + scrollView.bounds.size.height/2 - topRightGap.y + topRightOffsetGap.y);
    }

    float zoomScale = scrollView.zoomScale;
    if (zoomScale > 1 || (zoomScale == 1 && !scrollView.zoomBouncing)) {
        _renderStatusView.center = CGPointMake(center.x / zoomScale, center.y / zoomScale);
        _renderStatusView.transform = CGAffineTransformMakeScale(1.0/zoomScale, 1.0/zoomScale);
    }
}

- (void)layoutSubviews {
    [super layoutSubviews];
    if (self.pdfController) {
        [self updateShadowAnimated:NO];
        [self updateRenderView];
        [self updateStatusView];
    }
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Private

// when we rotate, ensure the link target sheet is dismissed.
- (void)willChangeInterfaceOrientationNotification:(NSNotification *)notification {
    if ([_openAnnotationLinkTargetSheet isKindOfClass:[UIActionSheet class]]) {
        NSUInteger cancelButtonIndex = ((UIActionSheet *)_openAnnotationLinkTargetSheet).cancelButtonIndex;
        [_openAnnotationLinkTargetSheet dismissWithClickedButtonIndex:cancelButtonIndex animated:NO];
        _openAnnotationLinkTargetSheet = nil;
    }
}

- (void)startCachingDocument {
    // cache page, make it an async call
    PSPDFDocument *aDocument = self.document;
    if (aDocument) {
        [[PSPDFCache sharedCache] cacheDocument:aDocument startAtPage:self.page size:PSPDFSizeNative];
    }
}

+ (NSOperationQueue *)pageViewQueue {
    __strong static NSOperationQueue *pageViewQueue;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        pageViewQueue = [NSOperationQueue new];
        pageViewQueue.name = @"com.PSPDFKit.PageViewQueue";
        pageViewQueue.maxConcurrentOperationCount = 2;
    });
    return pageViewQueue;
}

// order loaded views after their zIndex.
- (void)orderLoadedAnnotationViews {
    NSMutableArray *annotationViews = [[_annotationViews allValues] mutableCopy];
    [annotationViews sortUsingComparator:^NSComparisonResult(UIView<PSPDFAnnotationView> *view1, UIView<PSPDFAnnotationView> *view2) {
        NSUInteger zIndex1 = [view1 respondsToSelector:@selector(zIndex)] ? [view1 zIndex] : 0;
        NSUInteger zIndex2 = [view2 respondsToSelector:@selector(zIndex)] ? [view2 zIndex] : 0;
        return zIndex1 > zIndex2 ? NSOrderedAscending : (zIndex1 < zIndex2 ? NSOrderedDescending : NSOrderedSame);
    }];
    // re-add the views again, this time with the correct zIndex.
    [annotationViews enumerateObjectsUsingBlock:^(UIView *annotationView, NSUInteger idx, BOOL *stop) {
        [self insertSubview:annotationView aboveSubview:self.contentView];
    }];
}

// load page annotations from the pdf.
- (void)loadPageAnnotationsAnimated:(BOOL)animated {
    PSPDFDocument *document = self.document;
    PSPDFViewController *pdfController = self.pdfController;
    if (![document isValid] || !pdfController || _flags.hasLoadedAnnotationViews) {
        return; // document removed? don't try to load annotations!
    }

    // ensure annotations are already loaded; else load then in a background thread
    // If we don't check for annotationParser, this could result in an endless loop!
    PSPDFAnnotationParser *annotationParser = [document annotationParserForPage:self.page];
    NSUInteger compensatedPage = [document compensatedPageForPage:self.page];
    if (annotationParser && ![annotationParser hasLoadedAnnotationsForPage:compensatedPage]) {

        // set off to process as operation and then continue.
        if (!self.annotationLoadOperation) {
            NSBlockOperation *annotationLoadOperation = [NSBlockOperation new];
            annotationLoadOperation.queuePriority = self.page == self.pdfController.page ? NSOperationQueuePriorityNormal : NSOperationQueuePriorityVeryLow;
            __weak NSOperation *weakAnnotationLoadOperaton = annotationLoadOperation;
            [annotationLoadOperation addExecutionBlock:^{
                if (![weakAnnotationLoadOperaton isCancelled]) {
                    [annotationParser annotationsForPage:compensatedPage type:PSPDFAnnotationTypeAll]; // no need to filter here.
                }
            }];

            // call loadPageAnnotationsAnimated: again after parsing has been finished.
            __weak typeof(self) weakSelf = self;
            [annotationLoadOperation setCompletionBlock:^{
                if (![weakAnnotationLoadOperaton isCancelled]) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        __strong typeof(self) strongSelf = weakSelf;
                        strongSelf.annotationLoadOperation = nil;
                        [strongSelf loadPageAnnotationsAnimated:YES];
                    });
                }
            }];
            [[[self class] pageViewQueue] addOperation:annotationLoadOperation];
            self.annotationLoadOperation = annotationLoadOperation;
        }
        return;
    }
    PSPDFLogVerbose(@"Adding annotations for page: %d", self.page);

    // filter annotations to only process overlays
    NSArray *annotations = [annotationParser annotationsForPage:compensatedPage type:pdfController.renderAnnotationTypes];
    for (PSPDFAnnotation *annotation in annotations) {
        if (annotation.isOverlay && !annotation.isDeleted) {
            [self addAnnotation:annotation animated:animated];
        }
    }

    // now we have all annotation views, re-order the view hierarchy after the zIndex.
    [self orderLoadedAnnotationViews];

    // inform all annotation views that implement didChangePageFrame:.
    [self callChangePageFrameOnAnnotationViews];

    // we use this time to prepare the pageText (for smart zoom)
    // this won't be invoked if we just scrolling "through" a page but only when we are inside.
    if (!self.textParserLoadOperation) {
        NSBlockOperation *textParserLoadOperation = [NSBlockOperation new];
        textParserLoadOperation.queuePriority = self.page == self.pdfController.page ? NSOperationQueuePriorityNormal : NSOperationQueuePriorityVeryLow;
        __weak NSOperation *weakTextParserLoadOperation = textParserLoadOperation;
        [textParserLoadOperation addExecutionBlock:^{
            if (![weakTextParserLoadOperation isCancelled]) {
                [[document textParserForPage:self.page] words]; // access words to trigger block-parsing (not only text extraction)
            }
        }];
        __weak typeof(self) weakSelf = self;
        [textParserLoadOperation setCompletionBlock:^{
            if (![weakTextParserLoadOperation isCancelled]) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    __strong typeof(self) strongSelf = weakSelf;
                    strongSelf.textParserLoadOperation = nil;
                });
            }
        }];
        [[[self class] pageViewQueue] addOperation:textParserLoadOperation];
        self.textParserLoadOperation = textParserLoadOperation;
    }
}

- (BOOL)removeAnnotationOnNextPageUpdate:(PSPDFAnnotation *)annotation {
    return [self removeAnnotation:annotation animated:NO removeOnNextPageUpdate:YES];
}

- (BOOL)removeAnnotation:(PSPDFAnnotation *)annotation animated:(BOOL)animated {
    return [self removeAnnotation:annotation animated:animated removeOnNextPageUpdate:NO];
}

- (BOOL)removeAnnotation:(PSPDFAnnotation *)annotation animated:(BOOL)animated removeOnNextPageUpdate:(BOOL)removeOnNextPageUpdate {
    NSParameterAssert(annotation); if (!annotation) return NO;
    NSAssert([NSThread isMainThread], @"Must be called on main thread.");
    UIView *annotationView = _annotationViews[annotation];
    if (!annotationView) return NO;

    // remove from view cache
    [_annotationViews removeObjectForKey:annotation];

    // animate out
    if (removeOnNextPageUpdate) {
        [_annotationViewsToBeRemoved addObject:annotationView];
        [self updateView];
    }else {
        CGFloat animationDuration = self.pdfController.annotationAnimationDuration;
        [UIView animateWithDuration:animated ? animationDuration : 0.f delay:0.f options:UIViewAnimationOptionAllowUserInteraction animations:^{
            annotationView.alpha = 0.f;
        } completion:^(BOOL finished) {
            [annotationView removeFromSuperview];
        }];
    }
    return YES;
}

- (void)updatePageAnnotationView:(UIView<PSPDFAnnotationView> *)annotationView usingBlock:(void (^)(PSPDFAnnotation *annotation))block {
    PSPDFAnnotation *oldAnnotation = annotationView.annotation;

    // remove original annotation cache
    [_annotationViews removeObjectForKey:oldAnnotation];

    // copy annotation if needed
    PSPDFAnnotation *changeableAnnotation = [oldAnnotation copyAndDeleteOriginalIfNeeded];
    if (block) block(changeableAnnotation);

    annotationView.annotation = changeableAnnotation;

    // re-add new view (annotation gets copied, thus we need to do this even if copyAndDeleteOriginalIfNeeded doesn't copy the annotation)
    _annotationViews[changeableAnnotation] = annotationView;

    // TODO: selector list!
    [[NSNotificationCenter defaultCenter] postNotificationName:PSPDFAnnotationChangedNotification object:changeableAnnotation userInfo:@{PSPDFAnnotationChangedNotificationKeyPathKey : @[NSStringFromSelector(@selector(boundingBox))], PSPDFAnnotationChangedNotificationOriginalAnnotationKey : oldAnnotation}];
}

- (void)addAnnotation:(PSPDFAnnotation *)annotation animated:(BOOL)animated {
    NSParameterAssert(annotation); if (!annotation) return;
    NSAssert([NSThread isMainThread], @"Must be called on main thread.");
    PSPDFViewController *pdfController = self.pdfController;
    if (!pdfController) return;

    BOOL shouldDisplay = [pdfController delegateShouldDisplayAnnotation:annotation onPageView:self];
    if (shouldDisplay) {
        CGRect annotationRect = [annotation rectForPageRect:self.bounds];
        PSPDFLogVerbose(@"anntation rect %@ (bounds: %@)", NSStringFromCGRect(annotationRect), NSStringFromCGRect(self.bounds));

        // check if the annotation is already created
        UIView <PSPDFAnnotationView> *annotationView = [self cachedAnnotationViewForAnnotation:annotation];
        if (annotationView) {
            annotationView.frame = annotationRect;
        }else {
            annotationView = [pdfController.annotationController prepareAnnotationViewForAnnotation:annotation frame:annotationRect pageView:self];
            if (annotationView) {
                // add to tracking dictionary
                _annotationViews[annotation] = annotationView;

                [pdfController delegateWillShowAnnotationView:annotationView onPageView:self];
                [self insertSubview:annotationView aboveSubview:self.contentView];

                // smooth annotation animation
                CGFloat animationDuration = pdfController.annotationAnimationDuration;
                if (animated && animationDuration > 0.01f) {
                    annotationView.alpha = 0.f;
                    [UIView animateWithDuration:animationDuration delay:0.f options:UIViewAnimationOptionAllowUserInteraction animations:^{
                        annotationView.alpha = 1.f;
                    } completion:^(BOOL finished) {
                        [pdfController delegateDidShowAnnotationView:annotationView onPageView:self];
                    }];
                }else {
                    // don't animate, call delegate directly
                    [pdfController delegateDidShowAnnotationView:annotationView onPageView:self];
                }
            }
        }

        // call up delegate
        if ([annotationView respondsToSelector:@selector(didShowPage:)]) {
            [(id<PSPDFAnnotationView>)annotationView didShowPage:self.page];
        }
    }
}

- (UIView<PSPDFAnnotationView> *)cachedAnnotationViewForAnnotation:(PSPDFAnnotation *)annotation {
    return _annotationViews[annotation];
}

// shorthand for performSelector
- (void)loadPageAnnotationsWithAnimation {
    [self loadPageAnnotationsAnimated:YES];
}

// iterate over all subviews and return the annotation views.
// we're not using annotationViews_ here to allow custom subview manipulation.
- (NSArray *)visibleAnnotationViews {
    NSMutableArray *annotationViews = [NSMutableArray arrayWithCapacity:[self.subviews count]];
    // iterates over all subviews that conform to PSPDFAnnotationView
    for (UIView *subview in self.subviews) {
        if ([subview conformsToProtocol:@protocol(PSPDFAnnotationView)]) {
            [annotationViews addObject:subview];
        }
    }
    return annotationViews;
}

- (void)callAnnotationVisibleDelegateToShow:(BOOL)show {
    // update show/hide info on all loaded annotations
    NSArray *annotationViews = [self visibleAnnotationViews];
    [annotationViews enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        if (!show) {
            // call up delegate
            if ([obj respondsToSelector:@selector(didHidePage:)]) {
                [(id<PSPDFAnnotationView>)obj didHidePage:self.page];
            }
        }else {
            // call up delegate
            if ([obj respondsToSelector:@selector(didShowPage:)]) {
                [(id<PSPDFAnnotationView>)obj didShowPage:self.page];
            }
        }
    }];
}

- (void)recycleAnnotationViews {
    _flags.hasLoadedAnnotationViews = NO;

    // recycle annotation views (only those we created ourselves)
    PSPDFAnnotationController *annotationController = self.pdfController.annotationController;
    [[_annotationViews allValues] enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        // send the hide page call
        if ([obj respondsToSelector:@selector(didHidePage:)]) {
            [obj didHidePage:self.page];
        }

        // enqueue in cache
        [annotationController recycleAnnotationView:obj];
    }];
    [_annotationViews removeAllObjects];
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - PDF Coordinate Conversion Helpers

- (CGPoint)convertViewPointToPDFPoint:(CGPoint)viewPoint {
    PSPDFPageInfo *pageInfo = self.pageInfo;
    return PSPDFConvertViewPointToPDFPoint(viewPoint, pageInfo.pageRect, pageInfo.pageRotation, self.bounds);
}

- (CGPoint)convertPDFPointToViewPoint:(CGPoint)pdfPoint {
    PSPDFPageInfo *pageInfo = self.pageInfo;
    return PSPDFConvertPDFPointToViewPoint(pdfPoint, pageInfo.pageRect, pageInfo.pageRotation, self.bounds);
}

- (CGRect)convertViewRectToPDFRect:(CGRect)viewRect {
    PSPDFPageInfo *pageInfo = self.pageInfo;
    return PSPDFConvertViewRectToPDFRect(viewRect, pageInfo.pageRect, pageInfo.pageRotation, self.bounds);
}

- (CGRect)convertPDFRectToViewRect:(CGRect)pdfRect {
    PSPDFPageInfo *pageInfo = self.pageInfo;
    return PSPDFConvertPDFRectToViewRect(pdfRect, pageInfo.pageRect, pageInfo.pageRotation, self.bounds);
}

- (CGRect)convertGlyphRectToViewRect:(CGRect)glyphRect {
    PSPDFPageInfo *pageInfo = self.pageInfo;
    return PSPDFConvertPDFRectToViewRect(glyphRect, pageInfo.rotatedPageRect, 0, self.bounds);
}

- (CGRect)convertViewRectToGlyphRect:(CGRect)viewRect {
    PSPDFPageInfo *pageInfo = self.pageInfo;
    return PSPDFConvertViewRectToPDFRect(viewRect, pageInfo.rotatedPageRect, 0, self.bounds);
}

- (NSDictionary *)objectsAtPoint:(CGPoint)viewPoint options:(NSDictionary *)options {
    CGPoint pdfPoint = [self convertViewPointToPDFPoint:viewPoint];
    return [self.document objectsAtPDFPoint:pdfPoint page:self.page options:options];
}

- (NSDictionary *)objectsAtRect:(CGRect)viewRect options:(NSDictionary *)options {
    CGRect pdfRect = [self convertViewRectToPDFRect:viewRect];
    return [self.document objectsAtPDFRect:pdfRect page:self.page options:options];
}

- (PSPDFTextParser *)textParser {
    return [self.document textParserForPage:self.page];
}

- (BOOL)isRightPage {
    return self.page > 0 && [self.pdfController isDoublePageMode] && [self.pdfController isRightPageInDoublePageMode:self.page];
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - PSPDFRenderDelegate

- (void)renderQueue:(PSPDFRenderQueue *)renderQueue jobDidFinish:(PSPDFRenderJob *)job {
    // maybe there are old jobs that slipped through the cancellation because of their async nature.
    CGRect origBounds = [job.options[NSStringFromSelector(@selector(bounds))] CGRectValue];
    CGRect currentBounds = self.bounds;
    BOOL documentDifferent = job.document != self.document;
    BOOL pageDifferent = job.page != self.page;
    BOOL frameDifferent = !CGRectEqualToRect(origBounds, currentBounds);
    if (documentDifferent || pageDifferent || frameDifferent) {
        return;
    }

    // remove views that are registered to be removed after a page update.
    [_annotationViewsToBeRemoved makeObjectsPerformSelector:@selector(removeFromSuperview)];
    [_annotationViewsToBeRemoved removeAllObjects];

    float zoomScale = [(job.options)[kPSPDFZoomScale] floatValue];
    if (zoomScale <= 1.f) {
        [self setBackgroundImage:job.renderedImage animated:NO];
        [self.pdfController delegateDidRenderPageView:self];
    }else {
        _renderView.hidden = NO;
        _renderView.center = CGPointMake(CGRectGetMidX(job.clipRect) / zoomScale, CGRectGetMidY(job.clipRect) / zoomScale);
        _renderView.transform = CGAffineTransformMakeScale(1.0/zoomScale, 1.0/zoomScale);
        _renderView.image = job.renderedImage;
        _renderView.opaque = YES;
        _renderView.clearsContextBeforeDrawing = NO;
    }

    self.rendering = [[PSPDFRenderQueue sharedRenderQueue] hasRenderJobsForDelegate:self];
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UIScrollViewDelegate Handling (relayed from external)

// any offset changes
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    PSPDFLogVerbose(@"scrollViewDidScroll:%@", scrollView);

	if (!scrollView.zooming && !scrollView.decelerating) {
		CGPoint offset = scrollView.contentOffset;
		float distanceFromLastOffset = sqrtf(powf(offset.x - _lastScrollOffset.x, 2.0) + powf(offset.y - _lastScrollOffset.y, 2.0));
		if (CFAbsoluteTimeGetCurrent() - _lastScrollUpdate > 0.2 || distanceFromLastOffset < 5) {
			[self updateRenderView];
		}
		_lastScrollOffset = scrollView.contentOffset;
		_lastScrollUpdate = CFAbsoluteTimeGetCurrent();
	}
    [self updateStatusView];
}

// any zoom scale changes
- (void)scrollViewDidZoom:(UIScrollView *)scrollView {
    PSPDFLogVerbose(@"scrollViewDidZoom:%@", scrollView);

	_suspendUpdate = NO;
	float zoomScale = scrollView.zoomScale;
	if (zoomScale > scrollView.minimumZoomScale && zoomScale < scrollView.maximumZoomScale && fabsf(_lastZoomScale - zoomScale) < 0.1) {
		[self updateRenderView];
	}
	_lastZoomScale = scrollView.zoomScale;
    [self updateStatusView];
}

// called on start of dragging (may require some time and or distance to move)
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    PSPDFLogVerbose(@"scrollViewWillBeginDragging:%@", scrollView);

    // hide any open menu.
    [[UIMenuController sharedMenuController] setMenuVisible:NO animated:YES];

    _flags.targetContentOffsetValid = NO;

    // delay initial scrollView update to allow a "flick" with only a single update.
    _lastScrollUpdate = CFAbsoluteTimeGetCurrent();
}

// Called on finger up if the user dragged. velocity is in points/second. targetContentOffset may be changed to adjust where the scroll view comes to rest. not called when pagingEnabled is YES
- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset {
    PSPDFLogVerbose(@"scrollViewWillEndDragging:%@ withVelocity:%@ targetContentOffset:%@", scrollView, NSStringFromCGPoint(velocity), NSStringFromCGPoint(*targetContentOffset));

    // save targetContentOffset for more efficient view calculation
    _targetContentOffset = *targetContentOffset;
    _flags.targetContentOffsetValid = YES;
    [self updateRenderView];
}

// called on finger up if the user dragged. decelerate is true if it will continue moving afterwards
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    PSPDFLogVerbose(@"scrollViewDidEndDragging:scrollView willDecelerate:%@", decelerate ? @"YES" : @"NO");

	if (!decelerate) {
        _flags.targetContentOffsetValid = NO;
		[self updateRenderView];
        [self showMenuIfSelectedAnimated:YES];
	}
}

// called on finger up as we are moving
- (void)scrollViewWillBeginDecelerating:(UIScrollView *)scrollView {
    PSPDFLogVerbose(@"scrollViewWillBeginDecelerating:%@", scrollView);
}

// called when scroll view grinds to a halt
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    PSPDFLogVerbose(@"scrollViewDidEndDecelerating:%@", scrollView);

    _flags.targetContentOffsetValid = NO;

    // only call on systems where we don't yet get the targetContentOffset.
    [self updateStatusView];
    [self showMenuIfSelectedAnimated:YES];
}

// called when setContentOffset/scrollRectVisible:animated: finishes. not called if not animating
- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView {
    PSPDFLogVerbose(@"scrollViewDidEndScrollingAnimation:%@", scrollView);

    [self updateRenderView];
    [self updateStatusView];
}

// called before the scroll view begins zooming its content
- (void)scrollViewWillBeginZooming:(UIScrollView *)scrollView withView:(UIView *)view {
    PSPDFLogVerbose(@"scrollViewWillBeginZooming:%@ withView:%@", scrollView, view);

    _flags.targetContentOffsetValid = NO;
    _suspendUpdate = YES;
}

// custom scrollview extension. Won't always get called if not animated.
- (void)pspdf_scrollView:(UIScrollView *)scrollView willZoomToScale:(float)scale animated:(BOOL)animated {
    // hack into zoomScale and animate our renderView for a nicer zoom animation transition
    if (scale <= 1.f) {
        if (!self.renderView.hidden) {
            if (animated) {
                [UIView animateWithDuration:0.25f delay:0.f options:UIViewAnimationOptionAllowUserInteraction animations:^{
                    self.renderView.alpha = 0.f;
                } completion:^(BOOL finished) {
                    if (finished) {
                        self.renderView.hidden = YES;
                    }
                    self.renderView.alpha = 1.f;
                }];
            }else {
                self.renderView.hidden = YES;
            }
        }
    }
}

// scale between minimum and maximum. called after any 'bounce' animations
- (void)scrollViewDidEndZooming:(UIScrollView *)scrollView withView:(UIView *)view atScale:(float)scale {
    PSPDFLogVerbose(@"scrollViewDidEndZooming:%@ withView:%@ atScale:%f", scrollView, view, scale);

    _suspendUpdate = NO;
	[self updateSelection];
	[self updateRenderView];
    [self updateStatusView];

    // if scrollview is still decellerating, don't already show the menu (else it would flicker)
    if (!scrollView.isDecelerating) {
        [self showMenuIfSelectedAnimated:YES];
    }
}

// called when scrolling animation finished. may be called immediately if already at top
- (void)scrollViewDidScrollToTop:(UIScrollView *)scrollView {
    PSPDFLogVerbose(@"scrollViewDidScrollToTop:%@", scrollView);

	[self updateRenderView];
    [self updateStatusView];
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Selection Handling

- (void)discardSelection {
    self.selectedAnnotation = nil;
    [self.selectionView discardSelection];
    [[UIMenuController sharedMenuController] setMenuVisible:NO animated:YES];
}

- (void)updateSelection {
    [self.selectionView updateSelection];
    [self updateAnnotationSelection];
}

// update annotation selection
- (void)updateAnnotationSelection {
    CGRect boundsInViewCoords = [self.selectedAnnotation rectForPageRect:self.bounds];
    _annotationSelectionView.frame = boundsInViewCoords;
    _annotationSelectionView.zoomScale = self.scrollView.zoomScale; // important!
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Touch Handling

- (BOOL)pressRecognizerShouldHandlePressImmediately:(PSPDFLongPressGestureRecognizer *)recognizer {
    if ([self.selectionView pressRecognizerShouldHandlePressImmediately:recognizer]) {
        return YES;
    }else if (self.selectedAnnotation && [_annotationSelectionView pressRecognizerShouldHandlePressImmediately:recognizer]) {
        return YES;
    }
    return NO;
}

- (BOOL)longPress:(UILongPressGestureRecognizer *)recognizer {
    PSPDFScrollView *scrollView = [self scrollView];
    PSPDFPageView *affectedPageView = nil;
    if (scrollView) {
        affectedPageView = [scrollView pageViewForPoint:[recognizer locationInView:self.scrollView]];
    }
    BOOL tapHandled = NO;

    // call out to delegate
    if (affectedPageView == self) {
        CGPoint pageViewPoint = [recognizer locationInView:self];
        tapHandled = [self.pdfController delegateDidLongPressOnPageView:self atPoint:pageViewPoint gestureRecognizer:recognizer];
    }

    // exit early if delegate processed the longPress.
    if (tapHandled) return YES;

    // regular long-press processing (note annotation dragging)
    switch ([recognizer state]) {
        case UIGestureRecognizerStateBegan: {
			for (PSPDFNoteAnnotationView *annotView in self.subviews) {
                if (![annotView isKindOfClass:[PSPDFNoteAnnotationView class]] || !annotView.annotation.isEditable) continue;

				CGPoint locationInNoteView = [recognizer locationInView:annotView];
				if (CGRectContainsPoint(annotView.bounds, locationInNoteView)) {
					[self bringSubviewToFront:annotView];
                    [UIView animateWithDuration:0.25f animations:^{
                        annotView.layer.transform = CATransform3DConcat(annotView.layer.transform, CATransform3DMakeScale(1.5, 1.5, 1.0));
                        annotView.alpha = 0.8;
                    }];
					self.draggingAnnotationView = annotView;
                    self.selectedAnnotation = nil;
                    tapHandled = YES;
					break;
				}
            }break;

        case UIGestureRecognizerStateChanged: {
            if (self.draggingAnnotationView) {
                self.draggingAnnotationView.center = [recognizer locationInView:self];
                tapHandled = YES;
            }
        }break;

        case UIGestureRecognizerStateCancelled: {
            // restore back to original position
            if (self.draggingAnnotationView) {
                CGRect annotationRect = [self.draggingAnnotationView.annotation rectForPageRect:self.bounds];
                self.draggingAnnotationView.frame = annotationRect;
                tapHandled = YES;
            }
        }

        case UIGestureRecognizerStateEnded: {
            // Ended dragging text note
            if (self.draggingAnnotationView) {
                [UIView animateWithDuration:0.25f animations:^{
                    self.draggingAnnotationView.layer.transform = CATransform3DConcat(self.draggingAnnotationView.layer.transform, CATransform3DMakeScale(1/1.5, 1/1.5, 1.0));
                    self.draggingAnnotationView.alpha = 1.f;
                }];

                // calculate new frame
                CGRect noteRect = [self convertViewRectToPDFRect:self.draggingAnnotationView.frame];
                [self updatePageAnnotationView:self.draggingAnnotationView usingBlock:^(PSPDFAnnotation *annotation) {
                    annotation.boundingBox = noteRect;
                }];
                self.draggingAnnotationView = nil;
                tapHandled = YES;
            }
        }break;
        default: break;
        }
    }

    // skip processing if point doesn't affect us
    if (!self.draggingAnnotationView) {
        CGPoint viewPoint = [recognizer locationInView:self];
        CGPoint pdfPoint = [self convertViewPointToPDFPoint:viewPoint];

        // show long press action sheet above link annotations.
        NSArray *annotations = [self.document annotationsForPage:affectedPageView.page type:PSPDFAnnotationTypeLink];
        for (PSPDFLinkAnnotation *linkAnnotation in annotations) {
            if (!linkAnnotation.isDeleted && CGRectContainsPoint(linkAnnotation.boundingBox, pdfPoint)) {
                // don't show menu for multimedia extensions
                if (linkAnnotation.showAsLinkView && recognizer.state == UIGestureRecognizerStateBegan) {

                    // position on the whole annotation rect, but only if we can ensure to be fully visible.
                    CGRect viewRect = CGRectMake(viewPoint.x, viewPoint.y, 1, 1);
                    if (scrollView.zoomScale == 1.0 && linkAnnotation.boundingBox.size.width < affectedPageView.pageInfo.rotatedPageRect.size.width * 0.7 && linkAnnotation.boundingBox.size.height < affectedPageView.pageInfo.rotatedPageRect.size.height * 0.7 ) {
                        viewRect = [affectedPageView convertPDFRectToViewRect:linkAnnotation.boundingBox];
                    }

                    id sheet = [self showLinkPreviewActionSheetForAnnotation:linkAnnotation fromRect:viewRect animated:YES];
                    if (sheet) {
                        _openAnnotationLinkTargetSheet = sheet;
                        tapHandled = YES;
                    }
                }
                break;
            }
        }

        // don't allow text selection if link target sheet is open.
        // Allows UIView and UIViewController classes.
        if (([_openAnnotationLinkTargetSheet respondsToSelector:@selector(window)] && [_openAnnotationLinkTargetSheet window]) || ([_openAnnotationLinkTargetSheet respondsToSelector:@selector(view)] && [[_openAnnotationLinkTargetSheet view] window])) tapHandled = YES;

        if (!tapHandled && scrollView) {

            // insta-select an annotation?
            // Only select new annototation if we're not currently within the annotation (and allow some space, since the selection kobs are partly outside the annotation)
            BOOL hitTestWithinAnnotation = CGRectContainsPoint(CGRectInset(self.selectedAnnotation.boundingBox, -20, -20), pdfPoint);
            if (recognizer.state == UIGestureRecognizerStateBegan && !hitTestWithinAnnotation) {
                CGPoint tapPoint = [self convertViewPointToPDFPoint:viewPoint];
                NSArray *tappableAnnotations = nil;
#ifndef PSPDFKIT_DISABLE_TEXT_SELECTION_FEATURES
                tappableAnnotations = [self tappableAnnotationsAtPoint:viewPoint];
#endif
                for (PSPDFAnnotation *annotation in tappableAnnotations) {
                    CGPoint annotationPoint = CGPointMake(tapPoint.x - annotation.boundingBox.origin.x, tapPoint.y - annotation.boundingBox.origin.y);
                    if (![self.pdfController delegateDidTapOnAnnotation:annotation annotationPoint:annotationPoint annotationView:nil pageView:self viewPoint:viewPoint]) {

                        // first call delegate, and invoke default handlers if delegate returns NO.
                        if ([self.pdfController delegateShouldSelectAnnotation:annotation onPageView:self]) {
                            [self.pdfController delegateDidSelectAnnotation:annotation onPageView:self];
                        }
                        self.selectedAnnotation = annotation;

                        // we're in moving mode!
                        if (annotation.isMovable) {
                            self.annotationSelectionView.activeKnobType = PSPDFSelectionBorderKnobTypeMove;
                        }else {
                            // show menu, e.g. for highlight annotations
                            [self showMenuIfSelectedAnimated:YES];

                            // disrupt gesture to finish selection.
                            // this will instantly stop the gesture recognized and call this method recursively.
                            _flags.isCancellingLongPressGesture = YES;
                            recognizer.enabled = NO; recognizer.enabled = YES;
                            _flags.isCancellingLongPressGesture = NO;
                        }
                    }
                    tapHandled = YES;
                    break;
                }
            }

            if (self == affectedPageView || self.selectedAnnotation) {
                // check if annotation selection knobs are touched.
                if (self.selectedAnnotation) {
                    tapHandled = [self.annotationSelectionView longPress:recognizer];

                    // if not handled, remove annotation selection
                    if (!tapHandled) {
                        self.selectedAnnotation = nil;
                    }
                }

                // re-show menu after long-press on annotation
                if (recognizer.state == UIGestureRecognizerStateEnded) {
                    [self showMenuIfSelectedAnimated:YES];
                }

                // if we don't check for the flag here, the menu that might just have been shot above will be dismissed instantly.
                if (!tapHandled && !_flags.isCancellingLongPressGesture) {
                    self.selectionView.loupeView = scrollView.loupeView;
                    if (![self.selectionView longPress:recognizer]) {

                        // show the annotation menu if we didn't stop at text/image
                        if (recognizer.state == UIGestureRecognizerStateEnded && self.pdfController.isCreateAnnotationMenuEnabled) {
                            NSArray *menuItems = [self menuItemsForNewAnnotationAtPoint:viewPoint];
                            if ([menuItems count]) {
                                UIMenuController *menu = [UIMenuController sharedMenuController];
                                CGRect targetRect = CGRectMake(viewPoint.x, viewPoint.y, 1, 1);
                                [menu setTargetRect:targetRect inView:self];

                                menuItems = [self.pdfController delegateShouldShowMenuItems:menuItems atSuggestedTargetRect:targetRect forAnnotation:nil inRect:CGRectNull onPageView:self];

                                if ([menuItems count]) {
                                    menu.menuItems = menuItems;
                                    [self becomeFirstResponder];
                                    [menu setMenuVisible:YES animated:YES];
                                }
                            }
                        }
                    }
                    tapHandled = YES;
                }
            }else {
                // discard selection created in another page.
                [self discardSelection];
            }
        }
    }
    return tapHandled;
}

- (id)showLinkPreviewActionSheetForAnnotation:(PSPDFLinkAnnotation *)annotation fromRect:(CGRect)viewRect animated:(BOOL)animated {
    // show popover that previews the target link
    PSPDFLinkAnnotationView *annotationView = (PSPDFLinkAnnotationView *)[self cachedAnnotationViewForAnnotation:annotation];
    PSPDFActionSheet *actionSheet = [[PSPDFActionSheet alloc] initWithTitle:nil];
    NSString *title = [annotation targetString];

    // if annotation is set as editable, handle it differently.
    if ([self.document.editableAnnotationTypes containsObject:PSPDFAnnotationTypeStringLink]) {
        actionSheet.title = title;

        NSString *actionTitle = annotation.siteLinkTarget ? PSPDFLocalize(@"Open Link") : [NSString stringWithFormat:PSPDFLocalize(@"Go to page %d"), annotation.pageLinkTarget];
        [actionSheet addButtonWithTitle:actionTitle block:^{
            [self.pdfController.annotationController handleTouchUpForAnnotationIgnoredByDelegate:annotationView];
        }];

        [actionSheet addButtonWithTitle:PSPDFLocalize(@"Edit Link") block:^{
            PSPDFAlertView *linkAlertView = [[PSPDFAlertView alloc] initWithTitle:PSPDFLocalize(@"Link Destination") message:PSPDFLocalize(@"Enter page index or a http:// address to link to an URL.")];
            linkAlertView.alertViewStyle = UIAlertViewStylePlainTextInput;
            [[linkAlertView textFieldAtIndex:0] setText:annotation.siteLinkTarget ?: [NSString stringWithFormat:@"%d", annotation.pageLinkTarget]];

            [linkAlertView setCancelButtonWithTitle:PSPDFLocalize(@"Cancel") block:nil];
            __weak PSPDFAlertView *weakAlertView = linkAlertView;
            [linkAlertView addButtonWithTitle:PSPDFLocalize(@"Save") block:^{
                NSString *linkTarget = [weakAlertView textFieldAtIndex:0].text;
                if ([linkTarget length] == 0) {
                    annotation.deleted = YES;
                    [[NSNotificationCenter defaultCenter] postNotificationName:PSPDFAnnotationChangedNotification object:annotation userInfo:@{PSPDFAnnotationChangedNotificationKeyPathKey : @[NSStringFromSelector(@selector(isDeleted))], PSPDFAnnotationChangedNotificationOriginalAnnotationKey : annotation}];

                }else {
                    PSPDFLinkAnnotation *linkAnnotation = [annotation copyAndDeleteOriginalIfNeeded];

                    // add the URL or the page index
                    BOOL isHTTPLink = [linkTarget hasPrefix:@"http"] || [linkTarget hasPrefix:@"mailto:"];
                    if (isHTTPLink) {
                        linkAnnotation.linkType = PSPDFLinkAnnotationWebURL;
                        linkAnnotation.siteLinkTarget = linkTarget;
                        linkAnnotation.URL = [NSURL URLWithString:linkTarget];
                    }else {
                        linkAnnotation.siteLinkTarget = nil;
                        linkAnnotation.URL = nil;
                        linkAnnotation.pageLinkTarget = [linkTarget integerValue];
                    }

                    // send update notification
                    [[NSNotificationCenter defaultCenter] postNotificationName:PSPDFAnnotationChangedNotification object:linkAnnotation userInfo:@{PSPDFAnnotationChangedNotificationKeyPathKey : @[NSStringFromSelector(@selector(pageLinkTarget)), NSStringFromSelector(@selector(siteLinkTarget))], PSPDFAnnotationChangedNotificationOriginalAnnotationKey : annotation}];

                    // update annotation in linkAnnotationView (since we might have to make a copy)
                    annotationView.annotation = linkAnnotation;
                }
            }];
            [linkAlertView show];
        }];

        [actionSheet addButtonWithTitle:PSPDFLocalize(@"Move Link") block:^{
            self.selectedAnnotation = annotation;
        }];

        [actionSheet setDestructiveButtonWithTitle:PSPDFLocalize(@"Remove Link") block:^{
            annotation.deleted = YES;
            [self removeAnnotation:annotation animated:YES];

            // send change event for deleted = YES.
            // This event is optional. (If you know it's going to be deleted, you can do your custom code here as well, but having one place is better - PSPDFKit will send change notifications on all changes)
            [[NSNotificationCenter defaultCenter] postNotificationName:PSPDFAnnotationChangedNotification object:annotation userInfo:@{PSPDFAnnotationChangedNotificationKeyPathKey : @[NSStringFromSelector(@selector(isDeleted))], PSPDFAnnotationChangedNotificationOriginalAnnotationKey : annotation}];
        }];

    }else {
        [actionSheet addButtonWithTitle:title block:^{
            // invoke default behaviour for annotation
            [self.pdfController.annotationController handleTouchUpForAnnotationIgnoredByDelegate:annotationView];
        }];
    }

    [actionSheet setCancelButtonWithTitle:PSPDFLocalize(@"Cancel") block:NULL];
    [actionSheet setDestroyBlock:^{
        _openAnnotationLinkTargetSheet = nil;
    }];

    // ensure all selections are discarded before showing the sheet.
    [self discardSelection];

    [actionSheet showFromRect:viewRect inView:self animated:YES];
    return actionSheet;
}

// Sorted after likeliness of being tapped.
- (NSArray *)tappableAnnotationsAtPoint:(CGPoint)viewPoint {
    NSMutableArray *tappableAnnotations = nil;
    // fetch all annotation types except links.
    CGPoint pdfPoint = [self convertViewPointToPDFPoint:viewPoint];
    NSDictionary *objects = [self.document objectsAtPDFPoint:pdfPoint page:self.page options:@{kPSPDFObjectsAnnotationTypes : @(PSPDFAnnotationTypeAll &~ PSPDFAnnotationTypeLink), kPSPDFObjectsAnnotationPageBounds : BOXED(self.bounds), kPSPDFObjectsSmartSort : @YES}];
    tappableAnnotations = [objects[kPSPDFAnnotations] mutableCopy];

    // filter out deleted and non-editable annotations and non-rendered annotations.
    NSSet *editableAnnotationTypes = self.document.editableAnnotationTypes;
    PSPDFAnnotationType renderAnnotationTypes = self.pdfController.renderAnnotationTypes;
    [tappableAnnotations filterUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(PSPDFAnnotation *annotation, NSDictionary *bindings) {
        BOOL isDeleted = annotation.isDeleted;
        BOOL isEditable = [editableAnnotationTypes containsObject:annotation.typeString];
        BOOL isRendered = renderAnnotationTypes & annotation.type;

        return !isDeleted && isEditable && isRendered;
    }]];

    return tappableAnnotations;
}

- (BOOL)singleTapped:(UITapGestureRecognizer *)tapGesture {
    CGPoint viewPoint = [tapGesture locationInView:self];
    return [self singleTappedAtViewPoint:viewPoint];
}

- (BOOL)singleTappedAtViewPoint:(CGPoint)viewPoint {
    BOOL tapHandled = NO;

    CGPoint tapPoint = [self convertViewPointToPDFPoint:viewPoint];
    NSUInteger compensatedPage = [self.document compensatedPageForPage:self.page];
    PSPDFAnnotationParser *annotationParser = [self.document annotationParserForPage:self.page];

    // don't lock up when we haven't even loaded annotations yet. Touch won't evaluate annotations in that case and they're evaluated lazily.
    if (![annotationParser hasLoadedAnnotationsForPage:compensatedPage]) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            [annotationParser annotationsForPage:compensatedPage type:PSPDFAnnotationTypeAll];
        });
    }else {
        NSArray *tappableAnnotations = nil;
#ifndef PSPDFKIT_DISABLE_TEXT_SELECTION_FEATURES
        tappableAnnotations = [self tappableAnnotationsAtPoint:viewPoint];
#endif
        for (PSPDFAnnotation *annotation in tappableAnnotations) {
            CGPoint annotationPoint = CGPointMake(tapPoint.x - annotation.boundingBox.origin.x, tapPoint.y - annotation.boundingBox.origin.y);
            if (![self.pdfController delegateDidTapOnAnnotation:annotation annotationPoint:annotationPoint annotationView:nil pageView:self viewPoint:viewPoint]) {

                // first call delegate, and invoke default handlers if delegate returns NO.
                if ([self.pdfController delegateShouldSelectAnnotation:annotation onPageView:self]) {
                    [self.pdfController delegateDidSelectAnnotation:annotation onPageView:self];

                    self.selectedAnnotation = annotation;
                    [self showMenuForAnnotation:annotation animated:YES];
                }
            }
            tapHandled = YES;
            break;
        }
    }

    return tapHandled;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Annotation Handling

- (PSPDFNoteAnnotationController *)showNoteControllerForAnnotation:(PSPDFAnnotation *)annotation showKeyboard:(BOOL)showKeyboard animated:(BOOL)animated {
    if (!annotation.isOverlay) {
        self.selectedAnnotation = annotation;
    }

    PSPDFNoteAnnotationController *noteController = [[[self.pdfController classForClass:[PSPDFNoteAnnotationController class]] alloc] initWithAnnotation:annotation editable:annotation.isEditable];
    noteController.delegate = self;

    CGRect targetRect = [self convertPDFRectToViewRect:annotation.boundingBox];
    targetRect = [self convertRect:targetRect toView:self.pdfController.view];
    [self.pdfController presentViewControllerModalOrPopover:noteController embeddedInNavigationController:YES withCloseButton:YES animated:animated sender:nil options:@{PSPDFPresentOptionRect : BOXED(targetRect)}];

    // show keyboard if set
    if (noteController.view.window && showKeyboard) {
        [noteController.textView becomeFirstResponder];
    }

    return noteController;
}

- (void)showColorPickerForAnnotation:(PSPDFAnnotation *)annotation {
    PSPDFSimplePageViewController *colorPicker = [[self.pdfController classForClass:[PSPDFColorSelectionViewController class]] defaultColorPickerWithTitle:nil delegate:self];
    if (colorPicker) {
        CGRect targetRect = [self convertPDFRectToViewRect:annotation.boundingBox];
        targetRect = [self convertRect:targetRect toView:self.pdfController.view];
        [self.pdfController presentViewControllerModalOrPopover:colorPicker embeddedInNavigationController:YES withCloseButton:YES animated:YES sender:nil options:@{PSPDFPresentOptionRect : BOXED(targetRect)}];
    }else {
        PSPDFLogError(@"Color picker can't be displayed. Is PSPDFKit.bundle missing?");
    }
}

- (PSPDFMenuItem *)opacityMenuItemForAnnotation:(PSPDFAnnotation *)annotation {
    PSPDFMenuItem *opacityItem = [[PSPDFMenuItem alloc] initWithTitle:PSPDFLocalize(@"Opacity...") block:^{
        NSMutableArray *opacityMenuItems = [NSMutableArray array];
        NSArray *opacityOptions = @[@(25), @(50), @(75), @(100)];
        for (NSNumber *opacityNumber in opacityOptions) {
            // skip the current choice
            //if (annotation.alpha == [opacityNumber floatValue]/100) continue;
            NSString *title = [NSString stringWithFormat:PSPDFLocalize(@"%@ %%"), opacityNumber];
            UIMenuItem *opacityChooseItem = [[PSPDFMenuItem alloc] initWithTitle:title block:^{
                PSPDFAnnotation *newAnnotation = [annotation copyAndDeleteOriginalIfNeeded];
                [newAnnotation setAlpha:[opacityNumber floatValue]/100];

                [[NSNotificationCenter defaultCenter] postNotificationName:PSPDFAnnotationChangedNotification object:newAnnotation userInfo:@{PSPDFAnnotationChangedNotificationKeyPathKey : @[NSStringFromSelector(@selector(alpha))], PSPDFAnnotationChangedNotificationOriginalAnnotationKey : annotation}];

                [self updateView];
            } identifier:title];
            [opacityMenuItems addObject:opacityChooseItem];
        }
        UIMenuController *menu = [UIMenuController sharedMenuController];
        NSArray *updatedOpacityMenuItems = [self.pdfController delegateShouldShowMenuItems:opacityMenuItems atSuggestedTargetRect:menu.menuFrame forAnnotation:self.selectedAnnotation inRect:self.selectedAnnotation.boundingBox onPageView:self];

        if ([updatedOpacityMenuItems count]) {
            menu.menuItems = updatedOpacityMenuItems;
            [menu setMenuVisible:YES animated:YES];
        }
    } identifier:@"Opacity..."];
    return opacityItem;
}

- (NSArray *)menuItemsForAnnotation:(PSPDFAnnotation *)annotation {
    NSMutableArray *menuItems = [NSMutableArray array];
#ifndef PSPDFKIT_DISABLE_TEXT_SELECTION_FEATURES
    if (!annotation.isDeleted && [self.document.editableAnnotationTypes containsObject:annotation.typeString]) {
        if (annotation.type == PSPDFAnnotationTypeHighlight) {
            if (annotation.isEditable) {
                if ([self availableHighlightAnnotationTypeCount] > 1) {
                    PSPDFMenuItem *highlightTypeItem = [[PSPDFMenuItem alloc] initWithTitle:PSPDFLocalize(@"Type...") block:^{
                        [self selectHighlightAnnotationType];
                    } identifier:@"Type..."];
                    [menuItems addObject:highlightTypeItem];
                }

                PSPDFMenuItem *colorItem = [[PSPDFMenuItem alloc] initWithTitle:PSPDFLocalize(@"Colors...") block:^{
                    [self selectHighlightColor];
                } identifier:@"Colors..."];
                [menuItems addObject:colorItem];

                [menuItems addObject:[self opacityMenuItemForAnnotation:annotation]];
            }
        }else if (annotation.type == PSPDFAnnotationTypeInk) {
            if (annotation.isEditable) {
                PSPDFMenuItem *colorItem = [[PSPDFMenuItem alloc] initWithTitle:PSPDFLocalize(@"Colors...") block:^{
                    [self showColorPickerForAnnotation:annotation];
                } identifier:@"Colors..."];
                [menuItems addObject:colorItem];

                [menuItems addObject:[self opacityMenuItemForAnnotation:annotation]];

                PSPDFMenuItem *thicknessItem = [[PSPDFMenuItem alloc] initWithTitle:PSPDFLocalize(@"Thickness...") block:^{

                    NSMutableArray *thicknessMenuItems = [NSMutableArray array];
                    NSArray *thicknessOptions = @[@(1), @(2), @(3), @(6), @(9)];
                    for (NSNumber *thicknessNumber in thicknessOptions) {

                        // skip the current choice
                        if (annotation.lineWidth == [thicknessNumber floatValue]) continue;

                        NSString *title = [NSString stringWithFormat:PSPDFLocalize(@"%@ pt"), thicknessNumber];
                        UIMenuItem *thicknessChooseItem = [[PSPDFMenuItem alloc] initWithTitle:title block:^{
                            PSPDFAnnotation *newAnnotation = [annotation copyAndDeleteOriginalIfNeeded];
                            [newAnnotation setLineWidth:[thicknessNumber floatValue]];

                            [[NSNotificationCenter defaultCenter] postNotificationName:PSPDFAnnotationChangedNotification object:newAnnotation userInfo:@{PSPDFAnnotationChangedNotificationKeyPathKey : @[NSStringFromSelector(@selector(lineWidth))], PSPDFAnnotationChangedNotificationOriginalAnnotationKey : annotation}];

                            [self updateView];
                        } identifier:title];
                        [thicknessMenuItems addObject:thicknessChooseItem];
                    }
                    UIMenuController *menu = [UIMenuController sharedMenuController];
                    NSArray *updatedThicknessMenuItems = [self.pdfController delegateShouldShowMenuItems:thicknessMenuItems atSuggestedTargetRect:menu.menuFrame forAnnotation:self.selectedAnnotation inRect:self.selectedAnnotation.boundingBox onPageView:self];

                    if ([updatedThicknessMenuItems count]) {
                        menu.menuItems = updatedThicknessMenuItems;
                        [menu setMenuVisible:YES animated:YES];
                    }
                } identifier:@"Thickness..."];
                [menuItems addObject:thicknessItem];
            }
        }

        // remove is the last entry
        if (annotation.isEditable) {

            // every annotation can contain text.
            PSPDFMenuItem *textItem = [[PSPDFMenuItem alloc] initWithTitle:PSPDFLocalize(@"Text...") block:^{
                [self showNoteControllerForAnnotation:annotation showKeyboard:[self.selectedAnnotation.contents length] == 0 animated:YES];
            } identifier:@"Text..."];
            [menuItems addObject:textItem];

            PSPDFMenuItem *removeAnnotationItem = [[PSPDFMenuItem alloc] initWithTitle:PSPDFLocalize(@"Remove") block:^{
                self.selectedAnnotation.deleted = YES;

                // send change event for deleted
                [[NSNotificationCenter defaultCenter] postNotificationName:PSPDFAnnotationChangedNotification object:self.selectedAnnotation userInfo:@{PSPDFAnnotationChangedNotificationKeyPathKey : @[NSStringFromSelector(@selector(isDeleted))]}];

                self.selectedAnnotation = nil;
                [self updateView];
            } identifier:@"Remove"];
            [menuItems addObject:removeAnnotationItem];
        }
    }
#endif
    return [menuItems copy];
}

- (NSArray *)menuItemsForNewAnnotationAtPoint:(CGPoint)point {
    NSMutableArray *menuItems = [NSMutableArray array];
#ifndef PSPDFKIT_DISABLE_TEXT_SELECTION_FEATURES
    if ([self.document.editableAnnotationTypes containsObject:PSPDFAnnotationTypeStringFreeText]) {
        PSPDFMenuItem *freeTextTypeItem = [[PSPDFMenuItem alloc] initWithTitle:PSPDFLocalize(@"Free Text") block:^{
            PSPDFAnnotation *annotation = [[self.pdfController classForClass:[PSPDFFreeTextAnnotation class]] new];

            // get unrotated point
            CGPoint pdfPoint = [self convertViewPointToPDFPoint:point];
            annotation.boundingBox = CGRectMake(pdfPoint.x, pdfPoint.y-80, 150, 80);

            // add annotation and redraw
            [self.document addAnnotations:@[annotation] forPage:self.page];
            [self addAnnotation:annotation animated:NO];
            [self showNoteControllerForAnnotation:annotation showKeyboard:YES animated:YES];
        } identifier:@"Free Text"];
        [menuItems addObject:freeTextTypeItem];

        PSPDFMenuItem *noteTypeItem = [[PSPDFMenuItem alloc] initWithTitle:PSPDFLocalize(@"Note") block:^{
            PSPDFAnnotation *annotation = [[self.pdfController classForClass:[PSPDFNoteAnnotation class]] new];

            // get unrotated point
            CGPoint pdfPoint = [self convertViewPointToPDFPoint:point];
            annotation.boundingBox = CGRectMake(pdfPoint.x - 16, pdfPoint.y - 16, 32, 32);

            // add annotation and redraw
            [self.document addAnnotations:@[annotation] forPage:self.page];
            [self addAnnotation:annotation animated:NO];
            [self showNoteControllerForAnnotation:annotation showKeyboard:YES animated:YES];
        } identifier:@"Note"];
        [menuItems addObject:noteTypeItem];

        PSPDFMenuItem *drawTypeItem = [[PSPDFMenuItem alloc] initWithTitle:PSPDFLocalize(@"Draw") block:^{
            // make sure we show the annotation toolbar
            if (!self.pdfController.annotationButtonItem.annotationToolbar.window) {
                [self.pdfController.annotationButtonItem action:self.pdfController.annotationButtonItem];
                self.pdfController.annotationButtonItem.annotationToolbar.hideAfterDrawingDidFinish = YES;
            }
            [self.pdfController.annotationButtonItem.annotationToolbar drawButtonPressed:nil];
        } identifier:@"Draw"];
        [menuItems addObject:drawTypeItem];

        PSPDFMenuItem *signatureTypeItem = [[PSPDFMenuItem alloc] initWithTitle:PSPDFLocalize(@"Signature") block:^{
            PSPDFSignatureViewController *signatureController = [[PSPDFSignatureViewController alloc] init];
            signatureController.delegate = self;
            signatureController.userInfo = @{@"targetPoint" : BOXED(point)};

            [self.pdfController presentViewControllerModalOrPopover:signatureController embeddedInNavigationController:YES withCloseButton:NO animated:YES sender:nil options:@{PSPDFPresentOptionAlwaysModal : @YES, PSPDFPresentOptionModalPresentationStyle : @(UIModalPresentationFormSheet)}];
            
        } identifier:@"Signature"];
        [menuItems addObject:signatureTypeItem];
    }
#endif
    return [menuItems copy];
}

- (void)showMenuForAnnotation:(PSPDFAnnotation *)annotation animated:(BOOL)animated {
    if (annotation.type == PSPDFAnnotationTypeNote) {
        [self showNoteControllerForAnnotation:(PSPDFNoteAnnotation *)annotation showKeyboard:NO animated:animated];
        return;
    }

    UIMenuController *menu = [UIMenuController sharedMenuController];
    CGRect targetRect = [self convertPDFRectToViewRect:annotation.boundingBox];

    PSPDFLogVerbose(@"Showing menu for %@", annotation);

    if ([annotation isKindOfClass:[PSPDFHighlightAnnotation class]]) {
        PSPDFLogVerbose(@"selected text: %@", [(PSPDFHighlightAnnotation *)annotation highlightedString]);
    }

    NSArray *menuItems = [self menuItemsForAnnotation:annotation];
    if ([menuItems count]) {
        [menu setTargetRect:targetRect inView:self];

        menuItems = [self.pdfController delegateShouldShowMenuItems:menuItems atSuggestedTargetRect:targetRect forAnnotation:annotation inRect:annotation.boundingBox onPageView:self];

        if ([menuItems count]) {
            menu.menuItems = menuItems;
            [self becomeFirstResponder];
            [menu setMenuVisible:YES animated:animated];
        }
    }
}

// listen for annotation changes to update selected annotation object.
- (void)annotationsChanged:(NSNotification *)notification {
    if (self.selectedAnnotation) {
        PSPDFAnnotation *annotation = notification.object;
        PSPDFAnnotation *originalAnnotation = notification.userInfo[PSPDFAnnotationChangedNotificationOriginalAnnotationKey];

        if (self.selectedAnnotation == originalAnnotation) {
            self.selectedAnnotation = annotation;
        }
    }
}

- (void)setSelectedAnnotation:(PSPDFAnnotation *)selectedAnnotation {
    // TODO: unify dragging!
    if ([selectedAnnotation isKindOfClass:[PSPDFNoteAnnotation class]]) selectedAnnotation = nil;

    PSPDFAnnotation *oldSelectedAnnotation = _selectedAnnotation;
    if (selectedAnnotation != _selectedAnnotation) {
        _selectedAnnotation = selectedAnnotation;

        // lazily init annotation selection view
        if (selectedAnnotation && !_annotationSelectionView) {
            _annotationSelectionView = [[PSPDFResizableView alloc] initWithFrame:CGRectZero];
            _annotationSelectionView.hidden = YES;
            _annotationSelectionView.delegate = self;
            [self addSubview:_annotationSelectionView];
        }

        // only show selection if movable
        if (selectedAnnotation && selectedAnnotation.isMovable) {
            _annotationSelectionView.hidden = NO;
            [self updateAnnotationSelection];
            [self addSubview:_annotationSelectionView]; // ensure we're at the top!

#ifndef PSPDFKIT_DISABLE_TEXT_SELECTION_FEATURES
            _annotationSelectionView.allowEditing = selectedAnnotation.isMovable;
#else
            _annotationSelectionView.allowEditing = NO;
#endif
            if (!selectedAnnotation.isOverlay) {
                selectedAnnotation.overlay = YES; // make the annotation an overlay as long as we're selected
                [self addAnnotation:selectedAnnotation animated:NO];
                [self updateView];
                _flags.annotationSetToOverlayTemporarily = YES;
            }
            _annotationSelectionView.trackedView = [self cachedAnnotationViewForAnnotation:selectedAnnotation];

        }else {
            if (oldSelectedAnnotation && oldSelectedAnnotation.isMovable) {
                if (_flags.annotationSetToOverlayTemporarily) {
                    _flags.annotationSetToOverlayTemporarily = NO;
                    [self removeAnnotationOnNextPageUpdate:oldSelectedAnnotation];
                    oldSelectedAnnotation.overlay = NO;
                    [self updateView];
                }
            }
            _annotationSelectionView.hidden = YES;
        }
    }
}

- (void)showMenuIfSelectedAnimated:(BOOL)animated {
    if (self.selectedAnnotation && CGRectIntersectsRect(self.visibleRect, [self convertPDFRectToViewRect:self.selectedAnnotation.boundingBox])) {
        [self showMenuForAnnotation:self.selectedAnnotation animated:animated];
    }else if (self.selectionView.selectedGlyphs) {
        [self.selectionView updateMenuAnimated:animated];
    }
}

- (PSPDFOrderedDictionary *)colorOptions {
    // lazy loaded
    if (!_colorOptions) {
        PSPDFOrderedDictionary *colorOptions = [PSPDFOrderedDictionary dictionary];
        colorOptions[@"Yellow"] = [UIColor colorWithRed:0.99 green:0.93 blue:0.55 alpha:1.f];
        colorOptions[@"Red"] = [UIColor colorWithRed:1.0 green:0.0 blue:0.0 alpha:1.f];
        colorOptions[@"Green"] = [UIColor colorWithRed:0.81 green:0.96 blue:0.65 alpha:1.f];
        colorOptions[@"Blue"] = [UIColor colorWithRed:0.76 green:0.87 blue:1.0 alpha:1.f];
        colorOptions[@"Pink"] = [UIColor colorWithRed:1.0 green:0.75 blue:1.0 alpha:1.f];
        colorOptions[@"Black"] = [UIColor blackColor];
        self.colorOptions = colorOptions;
    }

    return _colorOptions;
}

- (NSArray *)colorMenuItemsForAnnotation:(PSPDFAnnotation *)annotation {
    // build menu items
    NSMutableArray *menuItems = [NSMutableArray array];
    [self.colorOptions enumerateKeysAndObjectsUsingBlock:^(NSString *colorName, UIColor *color, BOOL *stop) {
        if (![annotation.color isEqual:color]) {
            UIMenuItem *menuItem = [[PSPDFMenuItem alloc] initWithTitle:PSPDFLocalize(colorName) block:^{
                [self changeHighlightColor:color];
            } identifier:colorName];
            [menuItems addObject:menuItem];
        }
    }];

    // add custom color picker option
    UIMenuItem *menuItem = [[PSPDFMenuItem alloc] initWithTitle:PSPDFLocalize(@"Custom...") block:^{
        [self showColorPickerForAnnotation:self.selectedAnnotation];
    } identifier:@"Custom..."];
    [menuItems addObject:menuItem];

    return menuItems;
}

- (void)selectHighlightColor {
    UIMenuController *menu = [UIMenuController sharedMenuController];
    NSArray *menuItems = [self colorMenuItemsForAnnotation:self.selectedAnnotation];
    menuItems = [self.pdfController delegateShouldShowMenuItems:menuItems atSuggestedTargetRect:menu.menuFrame forAnnotation:self.selectedAnnotation inRect:self.selectedAnnotation.boundingBox onPageView:self];

    if ([menuItems count]) {
        menu.menuItems = menuItems;
        [menu setMenuVisible:YES animated:YES];
    }
}

- (void)changeHighlightColor:(UIColor *)color {
    if (self.selectedAnnotation) {
        if (![self.selectedAnnotation.color isEqual:color]) {
            PSPDFAnnotation *newAnnotation = [self.selectedAnnotation copyAndDeleteOriginalIfNeeded];
            newAnnotation.color = color;
            [self.document addAnnotations:@[newAnnotation] forPage:self.page];

            // send change event for deleted
            [[NSNotificationCenter defaultCenter] postNotificationName:PSPDFAnnotationChangedNotification object:newAnnotation userInfo:@{PSPDFAnnotationChangedNotificationKeyPathKey : @[NSStringFromSelector(@selector(iconName))], PSPDFAnnotationChangedNotificationOriginalAnnotationKey : self.selectedAnnotation}];

            self.selectedAnnotation = newAnnotation;
        }
        [self updateView];
    }
}

- (NSUInteger)availableHighlightAnnotationTypeCount {
    NSUInteger count = 0;
    if ([self.document.editableAnnotationTypes containsObject:PSPDFAnnotationTypeStringHighlight]) count++;
    if ([self.document.editableAnnotationTypes containsObject:PSPDFAnnotationTypeStringUnderline]) count++;
    if ([self.document.editableAnnotationTypes containsObject:PSPDFAnnotationTypeStringStrikeout]) count++;
    return count;
}

// change highlight annotation type
- (void)selectHighlightAnnotationType {
    if ([self availableHighlightAnnotationTypeCount] < 2) return;
    
    PSPDFHighlightAnnotation *highlightAnnot = (PSPDFHighlightAnnotation *)self.selectedAnnotation;
    if (![highlightAnnot isKindOfClass:[PSPDFHighlightAnnotation class]]) {
        PSPDFLogWarning(@"Stopping here. %@ is not a PSPDFHighlightAnnotation.", highlightAnnot);
    }

    PSPDFMenuItem *highlightItem = [[PSPDFMenuItem alloc] initWithTitle:PSPDFLocalize(@"Highlight") block:^{
        [self changeHighlightAnnotationTypeTo:PSPDFHighlightAnnotationHighlight];
    } identifier:@"Highlight"];
    highlightItem.enabled = highlightAnnot.highlightType != PSPDFHighlightAnnotationHighlight;
    PSPDFMenuItem *underlineItem = [[PSPDFMenuItem alloc] initWithTitle:PSPDFLocalize(@"Underline") block:^{
        [self changeHighlightAnnotationTypeTo:PSPDFHighlightAnnotationUnderline];
    } identifier:@"Underline"];
    underlineItem.enabled = highlightAnnot.highlightType != PSPDFHighlightAnnotationUnderline;
    PSPDFMenuItem *strikeoutItem = [[PSPDFMenuItem alloc] initWithTitle:PSPDFLocalize(@"Strikeout") block:^{
        [self changeHighlightAnnotationTypeTo:PSPDFHighlightAnnotationStrikeOut];
    } identifier:@"Strikeout"];
    strikeoutItem.enabled = highlightAnnot.highlightType != PSPDFHighlightAnnotationStrikeOut;
    UIMenuController *menu = [UIMenuController sharedMenuController];

    NSArray *menuItems = @[highlightItem, underlineItem, strikeoutItem];
    menuItems = [self.pdfController delegateShouldShowMenuItems:menuItems atSuggestedTargetRect:menu.menuFrame forAnnotation:highlightAnnot inRect:highlightAnnot.boundingBox onPageView:self];

    if ([menuItems count]) {
        menu.menuItems = menuItems;
        [menu setMenuVisible:YES animated:YES];
    }
}

- (void)changeHighlightAnnotationTypeTo:(PSPDFHighlightAnnotationType)highlightType {
    PSPDFHighlightAnnotation *newAnnotation = (PSPDFHighlightAnnotation *)self.selectedAnnotation;
    newAnnotation = (PSPDFHighlightAnnotation *)[newAnnotation copyAndDeleteOriginalIfNeeded];
    newAnnotation.highlightType = highlightType;
    [[NSNotificationCenter defaultCenter] postNotificationName:PSPDFAnnotationChangedNotification object:newAnnotation userInfo:@{PSPDFAnnotationChangedNotificationKeyPathKey : @[NSStringFromSelector(@selector(highlightType))], PSPDFAnnotationChangedNotificationOriginalAnnotationKey : self.selectedAnnotation}];
    [self updateView];
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - PSPDFNoteAnnotationControllerDelegate

- (void)noteAnnotationController:(PSPDFNoteAnnotationController *)noteAnnotationController didDeleteAnnotation:(PSPDFAnnotation *)annotation {
    // remove annotation view
    if (annotation.isOverlay) {
        UIView *annotationView = [self cachedAnnotationViewForAnnotation:annotation];
        if (annotationView) {
            [_annotationViews removeObjectForKey:annotation];
            [UIView animateWithDuration:0.25f delay:0.f options:0 animations:^{
                annotationView.alpha = 0;
            } completion:^(BOOL finished) {
                [annotationView removeFromSuperview];
            }];
        }
    }else {
        [self updateView];
    }

    // dismiss controller
    [noteAnnotationController dismissViewControllerAnimated:YES completion:NULL];
    [self.pdfController.popoverController dismissPopoverAnimated:YES];
    self.pdfController.popoverController = nil;
}

- (void)noteAnnotationController:(PSPDFNoteAnnotationController *)noteAnnotationController didChangeAnnotation:(PSPDFAnnotation *)annotation originalAnnotation:(PSPDFAnnotation *)originalAnnotation {

    // update view
    if (annotation.isOverlay) {
        [self removeAnnotation:originalAnnotation animated:NO];
        [self addAnnotation:annotation animated:NO];
    }else {
        [self updateView];
    }
}

- (void)noteAnnotationController:(PSPDFNoteAnnotationController *)noteAnnotationController willDismissWithAnnotation:(PSPDFAnnotation *)annotation {
    // if we were selected, clear selection
    if (self.selectedAnnotation == annotation) {
        self.selectedAnnotation = nil;
    }
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - PSPDFColorSelectionViewControllerDelegate

- (UIColor *)colorSelectionControllerSelectedColor:(PSPDFColorSelectionViewController *)controller {
    return self.selectedAnnotation.color;
}

- (void)colorSelectionController:(PSPDFColorSelectionViewController *)controller didSelectedColor:(UIColor *)color {

    PSPDFAnnotation *newAnnotation = [self.selectedAnnotation copyAndDeleteOriginalIfNeeded];
    newAnnotation.color = color;

    [[NSNotificationCenter defaultCenter] postNotificationName:PSPDFAnnotationChangedNotification object:newAnnotation userInfo:@{PSPDFAnnotationChangedNotificationKeyPathKey : @[NSStringFromSelector(@selector(color))], PSPDFAnnotationChangedNotificationOriginalAnnotationKey : self.selectedAnnotation}];

    [self updateView];
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - PSPDFSelectionBorderViewDelegate

- (void)resizableViewDidBeginEditing:(PSPDFResizableView *)resizableView {
    [[UIMenuController sharedMenuController] setMenuVisible:NO animated:YES];
}

// Update annotation frame and selection.
- (void)resizableViewChangedFrame:(PSPDFResizableView *)selectionView {
    CGRect annotationBoundingBox = [self convertViewRectToPDFRect:selectionView.frame];
    //PSPDFLogVerbose(@"annotationBoundingBox: %@", NSStringFromCGRect(annotationBoundingBox));

    if ([selectionView.trackedView respondsToSelector:@selector(annotation)]) {
        [self updatePageAnnotationView:(UIView<PSPDFAnnotationView> *)selectionView.trackedView usingBlock:^(PSPDFAnnotation *annotation) {
            annotation.boundingBox = annotationBoundingBox;
        }];
    }
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - PSPDFSignatureViewControllerDelegate

- (void)signatureViewControllerDidSave:(PSPDFSignatureViewController *)signatureController {
    // get current page and pageView
    NSUInteger page = self.pdfController.page;
    PSPDFInkAnnotation *inkAnnotation = [[self.pdfController.document classForClass:[PSPDFInkAnnotation class]] new];

    // get lines and calculate the boundingBox
    NSArray *lines = PSPDFConvertViewLinesToPDFLines(signatureController.lines, self.pageInfo.pageRect, self.pageInfo.pageRotation, self.pageInfo.pageRect);
    inkAnnotation.boundingBox = PSPDFBoundingBoxFromLines(lines, signatureController.drawView.lineWidth);
    inkAnnotation.lines = lines;

    // initially, we want to center the newly inserted signature and reduze size by 50%
    CGFloat sizeReductionFactor = signatureController.view.frame.size.width / (self.bounds.size.width * 2);
    CGRect targetRect;
    if (signatureController.userInfo[@"targetPoint"]) {
        CGPoint viewTargetPoint = [signatureController.userInfo[@"targetPoint"] CGPointValue];
        CGPoint pdfTargetPoint = [self convertViewPointToPDFPoint:viewTargetPoint];
        CGSize boxSize = CGSizeMake(inkAnnotation.boundingBox.size.width*sizeReductionFactor, inkAnnotation.boundingBox.size.height*sizeReductionFactor);
        targetRect = CGRectMake(pdfTargetPoint.x-boxSize.width/2, pdfTargetPoint.y-boxSize.height/2, boxSize.width, boxSize.height);
    }else {
        CGRect boundingBox = CGRectMake(0.f, 0.f, inkAnnotation.boundingBox.size.width*sizeReductionFactor, inkAnnotation.boundingBox.size.height*sizeReductionFactor);
        targetRect = PSPDFAlignRectangles(boundingBox, self.pageInfo.rotatedPageRect, PSPDFRectAlignCenter);
    }
    inkAnnotation.boundingBox = targetRect; // will recalculate all lines

    // add and display the annotation
    [self.pdfController.document addAnnotations:@[inkAnnotation] forPage:page];
    [self addAnnotation:inkAnnotation animated:NO];
    self.selectedAnnotation = inkAnnotation;
    [self showMenuIfSelectedAnimated:YES];
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UIAccessibilityReadingContent

// Returns the line number given a point in the view's coordinate space.
- (NSInteger)accessibilityLineNumberForPoint:(CGPoint)point {
    return 0;
}

// Returns the content associated with a line number as a string.
- (NSString *)accessibilityContentForLineNumber:(NSInteger)lineNumber {
    return [self.document textParserForPage:self.page].text;
}

// Returns the on-screen rectangle for a line number.
- (CGRect)accessibilityFrameForLineNumber:(NSInteger)lineNumber {
    return [self convertRect:self.bounds toView:self.window];
}

// Returns a string representing the text displayed on the current page.
- (NSString *)accessibilityPageContent {
    return [self.document textParserForPage:self.page].text;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UIAccessibility

- (UIAccessibilityTraits)accessibilityTraits {
    return UIAccessibilityTraitCausesPageTurn;
}

- (BOOL)accessibilityScroll:(UIAccessibilityScrollDirection)direction {
    [self.pdfController hideControlsAndPageElements];
    
    if (direction == UIAccessibilityScrollDirectionNext) {
        return [self.pdfController scrollToNextPageAnimated:YES];
    }else {
        return [self.pdfController scrollToPreviousPageAnimated:YES];
    }
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Deprecated

- (void)loadPageAnnotation:(PSPDFAnnotation *)annotation animated:(BOOL)animated {
    [self addAnnotation:annotation animated:animated];
}

- (BOOL)removePageAnnotation:(PSPDFAnnotation *)annotation animated:(BOOL)animated {
    return [self removeAnnotation:annotation animated:animated];
}

@end
