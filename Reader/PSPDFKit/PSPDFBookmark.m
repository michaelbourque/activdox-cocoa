//
//  PSPDFBookmark.m
//  PSPDFKit
//
//  Copyright (c) 2012 Peter Steinberger. All rights reserved.
//

#import "PSPDFBookmark.h"

@implementation PSPDFBookmark

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (id)initWithPage:(NSUInteger)page {
    if ((self = [super init])) {
        _page = page;
    }
    return self;
}

- (NSString *)description {
    return [NSString stringWithFormat:@"<%@ page:%d name: %@>", NSStringFromClass([self class]), self.page, self.name];
}

- (NSUInteger)hash {
    return [self.name hash] + _page * 31;
}

- (BOOL)isEqual:(id)other {
    if ([other isKindOfClass:[self class]]) {
        return [self isEqualToBookmark:(PSPDFBookmark *)other];
    }
    return NO;
}

- (BOOL)isEqualToBookmark:(PSPDFBookmark *)otherBookmark {
    if ((self.name == otherBookmark.name || [self.name isEqualToString:otherBookmark.name]) && self.page == otherBookmark.page) {
        return YES;
    }
    return NO;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public

- (NSString *)pageOrNameString {
    return [self.name length] ? self.name : [NSString stringWithFormat:PSPDFLocalize(@"Page %d"), self.page+1];
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSCopying

- (id)copyWithZone:(NSZone *)zone {
    PSPDFBookmark *bookmark = [[[self class] alloc] initWithPage:self.page];
    bookmark.name = self.name;
    return bookmark;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSCoding

- (id)initWithCoder:(NSCoder *)coder {
    NSUInteger page = [coder decodeIntegerForKey:NSStringFromSelector(@selector(page))];
    self = [self initWithPage:page];
    self.name = [coder decodeObjectForKey:NSStringFromSelector(@selector(name))];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)coder {
    [coder encodeInteger:_page forKey:NSStringFromSelector(@selector(page))];
    [coder encodeObject:_name forKey:NSStringFromSelector(@selector(name))];
}

@end
