//
//  PSPDFFocusViewController.m
//  PSPDFKit
//
//  Copyright (c) 2012 Peter Steinberger. All rights reserved.
//

#import "PSPDFFocusViewController.h"
#import "PSPDFConverter.h"
#import "PSPDFWindow.h"

@interface PSPDFFocusViewController () {
    UIWindow *_focusWindow;
}
@end

@implementation PSPDFFocusViewController

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (id)initWithView:(UIView *)focusView {
    if ((self = [super initWithNibName:nil bundle:nil])) {
        _focusView = focusView;
    }
    return self;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UIViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.view.backgroundColor = [UIColor blackColor];
    self.view.alpha = 0.5f;

    [self.view addSubview:self.focusView];
    self.focusView.frame = PSPDFAlignRectangles(self.focusView.bounds, self.view.bounds, PSPDFRectAlignCenter);
}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    self.focusView.frame = PSPDFAlignRectangles(self.focusView.bounds, self.view.bounds, PSPDFRectAlignCenter);
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public

- (void)setFocusView:(UIView *)focusView {
    [_focusView removeFromSuperview];
    _focusView = focusView;
    [self.view addSubview:focusView];
}

- (void)presentAnimated:(BOOL)animated {
    // use extra window to dim statusbar.
    // UIWindow's are a strange bread. They'll get added to the application at init time.
    _focusWindow = [PSPDFWindow new];
    _focusWindow.hidden = NO;
    _focusWindow.userInteractionEnabled = YES;
    _focusWindow.rootViewController = self;
}

- (void)dismissAnimated:(BOOL)animated completion:(void (^)(void))completion; {
    [UIView animateWithDuration:animated ? 0.25 : 0.f delay:0.f options:UIViewAnimationOptionAllowUserInteraction animations:^{
        _focusView.alpha = 0.f;
    } completion:^(BOOL finished) {
        if (completion) completion();
        _focusWindow = nil;
    }];
}

@end
