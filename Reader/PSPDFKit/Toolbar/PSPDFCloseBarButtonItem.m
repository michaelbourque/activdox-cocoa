//
//  PSPDFCloseBarButtonItem.m
//  PSPDFKit
//
//  Copyright (c) 2012 Peter Steinberger. All rights reserved.
//

#import "PSPDFCloseBarButtonItem.h"
#import "PSPDFViewController.h"

@implementation PSPDFCloseBarButtonItem

- (BOOL)isAvailable {
    // always enable close button unless there's *really* nothing to close/dismiss.
    UIViewController *rootController = [[[UIApplication sharedApplication] keyWindow] rootViewController];
    BOOL isFlatHierarchy = rootController == self.pdfController.parentViewController;
    BOOL isFirstEntry = [rootController isKindOfClass:[UINavigationController class]] && [((UINavigationController *)rootController).viewControllers ps_firstObject] == self.pdfController;

    return !isFlatHierarchy || !isFirstEntry;
}

// always enable close button
- (void)updateBarButtonItem {
    self.enabled = YES;
}

- (NSString *)actionName {
    return PSPDFLocalize(@"Close");
}

- (UIBarButtonItemStyle)itemStyle {
    return UIBarButtonItemStyleBordered;
}

- (void)action:(PSPDFBarButtonItem *)sender {
    [[self class] dismissPopoverAnimated:YES];

    // try to be smart and pop if we are not displayed modally.
    BOOL shouldDismiss = YES;
    if (self.pdfController.navigationController) {
        UIViewController *topViewController = self.pdfController.navigationController.topViewController;
        UIViewController *parentViewController = self.pdfController.parentViewController;
        if ((topViewController == self.pdfController || topViewController == parentViewController) && [self.pdfController.navigationController.viewControllers count] > 1) {
            [self.pdfController.navigationController popViewControllerAnimated:YES];
            shouldDismiss = NO;
        }
    }

    if (shouldDismiss) [self.pdfController dismissViewControllerAnimated:YES completion:NULL];
}

@end
