//
//  PSPDFActivityBarButtonItem.m
//  PSPDFKit
//
//  Copyright (c) 2012 Peter Steinberger. All rights reserved.
//

#import "PSPDFActivityBarButtonItem.h"
#import "PSPDFViewController.h"
#import "PSPDFViewController+Internal.h"
#import "PSPDFViewController+Delegates.h"
#import "PSPDFDocument.h"

@interface PSPDFActivityBarButtonItem ()
@property (nonatomic, strong) UIActivityViewController *activityController;
@end

@implementation PSPDFActivityBarButtonItem

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - PSPDFBarButtonitem

- (id)initWithPDFViewController:(PSPDFViewController *)pdfController {
    if ((self = [super initWithPDFViewController:pdfController])) {
        _excludedActivityTypes = @[UIActivityTypeAssignToContact];
    }
    return self;
}

- (BOOL)isAvailable {
    PSPDF_IF_PRE_IOS6(return NO;)
    return [self.pdfController.document isValid];
}

- (NSString *)actionName {
    return PSPDFLocalize(@"Share...");
}

- (UIBarButtonSystemItem)systemItem {
    return UIBarButtonSystemItemAction;
}

- (id)presentAnimated:(BOOL)animated sender:(id)sender {
    PSPDFDocument *document = self.pdfController.document;
    if (!document.isValid) return nil;

    NSMutableArray *activityItems = [NSMutableArray array];
    [activityItems addObject:document.title];
    if (document.fileURL) [activityItems addObject:document.fileURL];

    // get page view, render if not available.
    UIImage *renderedPage = [self.pdfController pageViewForPage:self.pdfController.page].contentView.image;
    if (!renderedPage) renderedPage = [document renderImageForPage:self.pdfController.page withSize:CGSizeMake(600, 800) clippedToRect:CGRectZero withAnnotations:[document annotationsForPage:self.pdfController.page type:PSPDFAnnotationTypeAll] options:nil];
    if (renderedPage) [activityItems addObject:renderedPage];

    self.activityController = [[UIActivityViewController alloc] initWithActivityItems:activityItems applicationActivities:self.applicationActivities];
    self.activityController.excludedActivityTypes = self.excludedActivityTypes;
    
    __weak PSPDFActivityBarButtonItem *weakSelf = self;
    self.activityController.completionHandler = ^(NSString *activityType, BOOL completed) {
        [weakSelf didDismiss];
    };
    
    return [self.pdfController presentViewControllerModalOrPopover:self.activityController embeddedInNavigationController:NO withCloseButton:NO animated:animated sender:sender options:nil];
}

@end
