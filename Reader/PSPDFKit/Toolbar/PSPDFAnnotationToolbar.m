//
//  PSPDFAnnotationToolbar.m
//  PSPDFKit
//
//  Copyright (c) 2012 Peter Steinberger. All rights reserved.
//

#import "PSPDFAnnotationToolbar.h"
#import "PSPDFViewController.h"
#import "PSPDFDocument.h"
#import "PSPDFIconGenerator.h"
#import "PSPDFPageView.h"
#import "PSPDFScrollView.h"
#import "PSPDFPageInfo.h"
#import "PSPDFInkAnnotation.h"
#import "PSPDFAnnotationParser.h"
#import "PSPDFColorButton.h"
#import "PSPDFDrawView.h"
#import "UIImage+PSPDFKitAdditions.h"
#import "PSPDFViewController+Internal.h"
#import "PSPDFAlertView.h"
#import "PSPDFHighlightAnnotation.h"
#import "PSPDFNoteAnnotation.h"
#import "PSPDFFreeTextAnnotation.h"
#import "PSPDFGlyph.h"
#import "PSPDFColorSelectionViewController.h"
#import "PSPDFNoteAnnotationController.h"
#import "PSPDFSimplePageViewController.h"
#import "PSPDFGradientView.h"
#import "PSPDFConverter.h"
#import "PSPDFKitGlobal+Internal.h"
#import "PSPDFWord.h"
#import "PSPDFSignatureViewController.h"

@interface PSPDFAnnotationToolbar() <PSPDFColorSelectionViewControllerDelegate> {
	PSPDFColorButton *_strokeColorButton;
	UIBarButtonItem *_undoItem;
	UIBarButtonItem *_redoItem;
    NSArray *_originalItems;
    PSPDFHUDViewMode _savedHUDViewMode;

    NSDictionary *_barButtonToTypeDict;
    UIView *_toolbarOverlay;

    // improve rotation experience
    CGFloat _originalNavBarAlpha;

    UIBarButtonItem *_colorButtonItem;

    // page -> view
    NSMutableDictionary *_drawViews;
    NSMutableDictionary *_selectionViews;
    NSMutableDictionary *_selectionViewRectCache;

    // global undo over multiple drawViews
    NSMutableArray *_drawViewUndoStack;
    NSUInteger _drawViewUndoStackPosition;
    BOOL _isDoingUndoRedo:1;
    BOOL _origInternalTapGesturesEnabled:1;
}
@property (nonatomic, copy) NSDictionary *lastUsedColors;
@end

NSString *const kPSPDFLastUsedDrawingWidth = @"kPSPDFLastUsedDrawingWidth";
NSString *const kPSPDFLastUsedDrawingColor = @"kPSPDFLastUsedDrawingColor";

@implementation PSPDFAnnotationToolbar

@synthesize delegate = delegate_; // _delegate is already used in UIToolbar

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (id)initWithPDFController:(PSPDFViewController *)pdfController {
#ifdef PSPDFKIT_DISABLE_TEXT_SELECTION_FEATURES
    PSPDFLogError(@"Only available in PSPDFKit Annotate.");
    return nil;
#else
    if (self = [super initWithFrame:CGRectZero]) {
        // to be 100% sure everything's working in all cases, install the override handler on both classes
        // (UIToolbar for people who add the annotationBar e.g. to the botton toolbar)
        static dispatch_once_t onceToken;
        dispatch_once(&onceToken, ^{
            PSPDFEnsureAnnotationToolbarIsOnTopForClass([UINavigationBar class]);
            PSPDFEnsureAnnotationToolbarIsOnTopForClass([UIToolbar class]);
        });

        _pdfController = pdfController;
        _toolbarMode = PSPDFAnnotationToolbarNone;
        self.autoresizingMask = UIViewAutoresizingFlexibleWidth;

        self.drawColor = [self lastUsedColorForAnnotationTypeString:PSPDFAnnotationTypeStringInk] ?: [UIColor colorWithRed:0.121f green:0.35f blue:1.f alpha:1.f];
        self.lineWidth = [[NSUserDefaults standardUserDefaults] floatForKey:kPSPDFLastUsedDrawingWidth] ?: 3.f;

        _drawViews = [NSMutableDictionary new];
        _selectionViews = [NSMutableDictionary new];
        _selectionViewRectCache = [NSMutableDictionary new];
        _drawViewUndoStack = [NSMutableArray new];

        NSNotificationCenter *dnc = [NSNotificationCenter defaultCenter];
        [dnc addObserver:self selector:@selector(flashToolbar) name:PSPDFHUDNotHiddenNotification object:nil];
        [dnc addObserver:self selector:@selector(annotationChanged:) name:PSPDFAnnotationChangedNotification object:nil];

        UIBarButtonItem *flexSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];

        float topInset = 2.0f;
        NSMutableDictionary *barButtonToTypeDict = [NSMutableDictionary dictionaryWithCapacity:5];
        NSMutableArray *items = [NSMutableArray arrayWithObject:flexSpace];
        NSSet *editableAnnotationTypes = self.pdfController.document.editableAnnotationTypes;

        if ([editableAnnotationTypes containsObject:PSPDFAnnotationTypeStringHighlight]) {
            UIImage *highlightImage = [UIImage imageNamed:@"PSPDFKit.bundle/highlight"];
            UIBarButtonItem *highlightButton = [[UIBarButtonItem alloc] initWithImage:highlightImage style:UIBarButtonItemStylePlain target:self action:@selector(highlightButtonPressed:)];
            barButtonToTypeDict[@(PSPDFAnnotationToolbarHighlight)] = highlightButton;
            highlightButton.imageInsets = UIEdgeInsetsMake(topInset, 0.0f, -topInset, 0.0f);
            highlightButton.accessibilityLabel = PSPDFLocalize(@"Highlight");
            [items addObjectsFromArray:@[highlightButton, flexSpace]];
        }

        if ([editableAnnotationTypes containsObject:PSPDFAnnotationTypeStringStrikeout]) {
            UIImage *strikeoutImage = [UIImage imageNamed:@"PSPDFKit.bundle/strikeout"];
            UIBarButtonItem *strikeOutButton = [[UIBarButtonItem alloc] initWithImage:strikeoutImage style:UIBarButtonItemStylePlain target:self action:@selector(strikeOutButtonPressed:)];
            barButtonToTypeDict[@(PSPDFAnnotationToolbarStrikeOut)] = strikeOutButton;
            strikeOutButton.imageInsets = UIEdgeInsetsMake(topInset, 0.0f, -topInset, 0.0f);
            strikeOutButton.accessibilityLabel = PSPDFLocalize(@"Strikeout");
            [items addObjectsFromArray:@[strikeOutButton, flexSpace]];
        }

        if ([editableAnnotationTypes containsObject:PSPDFAnnotationTypeStringUnderline]) {
            UIImage *underlineImage = [UIImage imageNamed:@"PSPDFKit.bundle/underline"];
            UIBarButtonItem *underlineButton = [[UIBarButtonItem alloc] initWithImage:underlineImage style:UIBarButtonItemStylePlain target:self action:@selector(underlineButtonPressed:)];
            barButtonToTypeDict[@(PSPDFAnnotationToolbarUnderline)] = underlineButton;
            underlineButton.imageInsets = UIEdgeInsetsMake(topInset, 0.0f, -topInset, 0.0f);
            underlineButton.accessibilityLabel = PSPDFLocalize(@"Underline");
            [items addObjectsFromArray:@[underlineButton, flexSpace]];
        }

        if ([editableAnnotationTypes containsObject:PSPDFAnnotationTypeStringFreeText]) {
            UIImage *underlineImage = [UIImage imageNamed:@"PSPDFKit.bundle/freetext"];
            UIBarButtonItem *freeTextButton = [[UIBarButtonItem alloc] initWithImage:underlineImage style:UIBarButtonItemStylePlain target:self action:@selector(freeTextButtonPressed:)];
            barButtonToTypeDict[@(PSPDFAnnotationToolbarFreeText)] = freeTextButton;
            freeTextButton.imageInsets = UIEdgeInsetsMake(topInset, 0.0f, -topInset, 0.0f);
            freeTextButton.accessibilityLabel = PSPDFLocalize(@"Free Text");
            [items addObjectsFromArray:@[freeTextButton, flexSpace]];
        }

        if ([editableAnnotationTypes containsObject:PSPDFAnnotationTypeStringNote]) {
            UIImage *commentImage = [UIImage imageNamed:@"PSPDFKit.bundle/newcomment"];
            UIBarButtonItem *commentButton = [[UIBarButtonItem alloc] initWithImage:commentImage style:UIBarButtonItemStylePlain target:self action:@selector(noteButtonPressed:)];
            barButtonToTypeDict[@(PSPDFAnnotationToolbarNote)] = commentButton;
            commentButton.imageInsets = UIEdgeInsetsMake(topInset, 0.0f, -topInset, 0.0f);
            commentButton.accessibilityLabel = PSPDFLocalize(@"New Note");
            [items addObjectsFromArray:@[commentButton, flexSpace]];
        }

        if ([editableAnnotationTypes containsObject:PSPDFAnnotationTypeStringInk]) {
            UIImage *sketchImage = [UIImage imageNamed:@"PSPDFKit.bundle/sketch"];
            UIBarButtonItem *sketchButton = [[UIBarButtonItem alloc] initWithImage:sketchImage style:UIBarButtonItemStylePlain target:self action:@selector(drawButtonPressed:)];
            barButtonToTypeDict[@(PSPDFAnnotationToolbarDraw)] = sketchButton;
            sketchButton.imageInsets = UIEdgeInsetsMake(topInset, 0.0f, -topInset, 0.0f);
            sketchButton.accessibilityLabel = PSPDFLocalize(@"Sketch");
            [items addObjectsFromArray:@[sketchButton, flexSpace]];
        }

        if ([editableAnnotationTypes containsObject:PSPDFAnnotationTypeStringSignature]) {
            UIImage *sketchImage = [UIImage imageNamed:@"PSPDFKit.bundle/sketch"];
            UIBarButtonItem *signatureButton = [[UIBarButtonItem alloc] initWithImage:sketchImage style:UIBarButtonItemStylePlain target:self action:@selector(signatureButtonPressed:)];
            barButtonToTypeDict[@(PSPDFAnnotationToolbarSignature)] = signatureButton;
            signatureButton.imageInsets = UIEdgeInsetsMake(topInset, 0.0f, -topInset, 0.0f);
            signatureButton.accessibilityLabel = PSPDFLocalize(@"Signature");
            [items addObjectsFromArray:@[signatureButton, flexSpace]];
        }

        _barButtonToTypeDict = barButtonToTypeDict;

        UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneButtonPressed:)];
        [items addObject:backButton];

        _originalItems = [items copy];
        self.items = _originalItems;

        // for toolbar state
        _toolbarOverlay = [[UIView alloc] initWithFrame:CGRectZero];
        _toolbarOverlay.userInteractionEnabled = NO;
        _toolbarOverlay.backgroundColor = [UIColor whiteColor];
        _toolbarOverlay.alpha = 0.5;
        [self addSubview:_toolbarOverlay];

        // add warning for missing PSPDFKit.bundle
        PSPDF_IF_SIMULATOR(if(!PSPDFKitBundle()) {
            PSPDFAlertView *alert = [[PSPDFAlertView alloc] initWithTitle:@"Simulator Warning" message:@"PSPDFKit.bundle is not available. Please add it to your Xcode project. Annotation and Search will not work properly. See http://PSPDFKit.com/documentation.html for details."];
            [alert addButtonWithTitle:@"Open Mac Safari" block:^{
                system("open 'http://PSPDFKit.com/documentation.html'");
            }];
            [alert setCancelButtonWithTitle:@"Ok" block:NULL];
            [alert show];})
    }
    return self;
#endif
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [self removeDrawingViewAnimated:NO];
    [self removeSelectionAnnotationViewAnimated:NO];
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UIView

// automatically adapy to 30 px iPhone size.
- (void)setFrame:(CGRect)frame {
    [super setFrame:frame];

    // update size
    CGRect newFrame = frame;
    BOOL shouldBeSmall = UIInterfaceOrientationIsLandscape(self.pdfController.interfaceOrientation) && !PSIsIpad();
    newFrame.size.height = shouldBeSmall ? 30 : 44;

    // dispatch to prevent weird animation if items.
    // (this items animation can't be disabled with a CATransation...)
    if (!CGRectIsEmpty(frame) && !CGRectEqualToRect(newFrame, self.frame)) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self setFrame:newFrame];
        });
    }
}

// Ensure the drawing views are added after view has been reloaded (e.g. because the color picker has been displayed modally on the iPhone)
- (void)didMoveToWindow {
    if (self.window) {
        NSArray *pageViews = [self.pdfController visiblePageViews];
        for (PSPDFPageView *pageView in pageViews) {
            PSPDFDrawView *drawView = _drawViews[@(pageView.page)];
            if (drawView) [pageView addSubview:drawView];

            PSPDFSelectionView *selectionView = _selectionViews[@(pageView.page)];
            if (selectionView) [pageView addSubview:selectionView];
        }
    }
}

- (CGFloat)targetNavigationBarAlpha {
    return self.translucent ? 0.f : 1.f;
}

- (BOOL)shouldAnimateFromTopWithRect:(CGRect)rect {
    UIView *pdfView = self.pdfController.view;
    CGRect positionOnScreen = [pdfView convertRect:rect fromView:self.superview];
    CGRect screenRect = pdfView.bounds; // includes rotation
    if (positionOnScreen.size.width < positionOnScreen.size.height) {
        // landscape
        return CGRectGetMaxX(positionOnScreen) < CGRectGetWidth(screenRect)/2;
    }else {
        return CGRectGetMaxY(positionOnScreen) < CGRectGetHeight(screenRect)/2;
    }
}

- (BOOL)isInNavigationBar {
    // bar is either added to the navBar or the view directly, depending on the transparency style.
    return self.superview == self.pdfController.navigationController.navigationBar || self.superview == self.pdfController.navigationController.view;
}

- (void)layoutSubviews {
    [super layoutSubviews];

    // update indictor as frame changed.
    [self updateToolbarModeIndicatorView];
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public

- (void)setPdfController:(PSPDFViewController *)pdfController {
    _pdfController = pdfController;
    self.barStyle = [pdfController barStyle];
    self.translucent = [pdfController isTransparentHUD];
    self.tintColor = pdfController.tintColor;
}

- (void)flashToolbar {
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"backgroundColor"];
    animation.duration = 0.25 * PSPDFSimulatorAnimationDragCoefficient();
    animation.autoreverses = YES;
    animation.toValue = (id)[UIColor redColor].CGColor;
    [self.layer addAnimation:animation forKey:@"backgroundColor"];
}

- (void)showToolbarInRect:(CGRect)rect animated:(BOOL)animated {
    self.hidden = NO;
    _originalNavBarAlpha = self.pdfController.navigationController.navigationBar.alpha;

    // determine what kind of animation we should make (fade from top or bottom)
    BOOL shouldAnimateFromTop = [self shouldAnimateFromTopWithRect:rect];
    // only show navBar if we are actually inside (we might also be on any UIToolbar or e.g. at a bottom navBar-toolbar)
    BOOL isInNavBar = [self isInNavigationBar];
    dispatch_block_t animationAction = ^{
        self.frame = rect;
        self.alpha = 1.f;
    };

    // If we would set alpha = 0 on a default toolbar style, the pdf content would *jump*.
    if (animated) {
        CGRect preAnimationRect = rect;
        preAnimationRect.origin.y -= rect.size.height * (shouldAnimateFromTop ? 1 : -1);
        self.frame = preAnimationRect;
        self.alpha = 0.f;
        [UIView animateWithDuration:0.25f delay:0.f options:UIViewAnimationOptionAllowUserInteraction animations:^{
            animationAction();
        } completion:NULL];

        if (isInNavBar) {
            [UIView animateWithDuration:0.25f delay:0.f options:UIViewAnimationOptionAllowUserInteraction animations:^{
                self.pdfController.navigationController.navigationBar.alpha = [self targetNavigationBarAlpha];
            } completion:NULL];
        }

    }else {
        animationAction();
        if (isInNavBar) self.pdfController.navigationController.navigationBar.alpha = [self targetNavigationBarAlpha];
    }

    // Lock the HUD.
    _savedHUDViewMode = self.pdfController.HUDViewMode;

    // only lock if we're in the HUD
    if (isInNavBar) {
        self.pdfController.HUDViewMode = PSPDFHUDViewAlways;
    }

    UIAccessibilityPostNotification(UIAccessibilityLayoutChangedNotification, nil);
}

- (void)hideToolbarAnimated:(BOOL)animated completion:(dispatch_block_t)completionBlock {
    if (animated) {
        BOOL shouldAnimateFromTop = [self shouldAnimateFromTopWithRect:self.frame];
        CGRect preAnimationRect = self.frame;
        preAnimationRect.origin.y -= preAnimationRect.size.height * (shouldAnimateFromTop ? 1 : -1);
        [UIView animateWithDuration:0.25f delay:0.f options:0 animations:^{
            self.frame = preAnimationRect;
            self.alpha = 0.f;
        } completion:^(BOOL finished) {
            self.hidden = YES;
            if (completionBlock) completionBlock();

            if ([self.delegate respondsToSelector:@selector(annotationToolbarDidHide:)]) {
                [self.delegate annotationToolbarDidHide:self];
            }
        }];
    }else {
        self.hidden = YES;
        if (completionBlock) completionBlock();
    }

    // remove any draw/selectionViews
    [self removeDrawingViewAnimated:animated];
    [self removeSelectionAnnotationViewAnimated:animated];

    // unlock the HUD.
    self.pdfController.HUDViewMode = _savedHUDViewMode;
    self.pdfController.navigationController.navigationBar.alpha = _originalNavBarAlpha;

    // ensure color popover is invisible
    [self.pdfController.popoverController dismissPopoverAnimated:animated];
    self.pdfController.popoverController = nil;

    UIAccessibilityPostNotification(UIAccessibilityLayoutChangedNotification, nil);
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Private

- (void)lockPDFController {
    if ([self.pdfController isControllerLocked]) {
        PSPDFLogWarning(@"PDF Controller already locked."); return;
    }

    [self.pdfController lockController];
    [self.pdfController setHUDVisible:NO animated:YES];
}

- (void)unlockPDFControllerAndEnsureToStayOnTop:(BOOL)stayOnTop {
    if (![self.pdfController isControllerLocked]) {
        PSPDFLogWarning(@"PDF Controller is not locked."); return;
    }

    [self.pdfController unlockController];
    if ([self isInNavigationBar]) [self.pdfController showControls];

    // ensure annotation toolbar stays on top. (showControls invokes async, so wait a runloop)
    if (stayOnTop) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if (self.window && self.alpha) {
                [self.superview insertSubview:self atIndex:[self.superview.subviews count]];
                if ([self isInNavigationBar]) self.pdfController.navigationController.navigationBar.alpha = [self targetNavigationBarAlpha];
            }
        });
    }
}

- (void)setLineWidth:(CGFloat)lineWidth {
    _lineWidth = lineWidth;
    [[NSUserDefaults standardUserDefaults] setFloat:lineWidth forKey:kPSPDFLastUsedDrawingWidth];

    for (PSPDFDrawView *drawView in _drawViews) {
        drawView.lineWidth = lineWidth;
    }
}

- (void)setDrawColor:(UIColor *)drawColor {
    if (drawColor != _drawColor) {
        _drawColor = drawColor;
        _strokeColorButton.color = drawColor;
        [self setLastUsedColor:drawColor forAnnotationType:PSPDFAnnotationTypeStringInk];
        [[_drawViews allValues] makeObjectsPerformSelector:@selector(setStrokeColor:) withObject:drawColor];
    }
}

- (void)setLastUsedColor:(UIColor *)lastUsedDrawColor forAnnotationType:(NSString *)annotationType {
    NSParameterAssert(annotationType);
    NSMutableDictionary *lastUsedColors = [self.lastUsedColors mutableCopy] ?: [NSMutableDictionary dictionary];
    lastUsedColors[annotationType] = lastUsedDrawColor;
    self.lastUsedColors = lastUsedColors;

    [[NSUserDefaults standardUserDefaults] setObject:[NSKeyedArchiver archivedDataWithRootObject:lastUsedColors] forKey:kPSPDFLastUsedDrawingColor];
}

- (UIColor *)lastUsedColorForAnnotationTypeString:(NSString *)annotationType {
    NSData *lastUsedDrawColorsData = [[NSUserDefaults standardUserDefaults] objectForKey:kPSPDFLastUsedDrawingColor];
    NSDictionary *lastUsedColors = nil;
    if (lastUsedDrawColorsData) {
        @try {
            lastUsedColors = [NSKeyedUnarchiver unarchiveObjectWithData:lastUsedDrawColorsData];
        } @catch (NSException *exception) {}
    }

    return annotationType ? lastUsedColors[annotationType] : nil;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Rotation Handling

#import "PSPDFPatches.h"
#import <objc/runtime.h>
#import <objc/message.h>

// imp_implementationWithBlock changed it's type in iOS6.
#if __IPHONE_OS_VERSION_MAX_ALLOWED < 60000
#define PSPDFBlockImplCast (__bridge void *)
#else
#define PSPDFBlockImplCast
#endif

// Used when toolbar is not transparent so that the added toolbar stays on top.
static void PSPDFEnsureAnnotationToolbarIsOnTopForClass(Class overrideClass) {
    SEL layoutSEL = NSSelectorFromString(@"pspdf_layoutSubviews");
    IMP layoutIMP = imp_implementationWithBlock(PSPDFBlockImplCast(^(UINavigationBar *_self) {
        objc_msgSend(_self, layoutSEL); // call super

        // find any PSPDF* toolbar
        UIView *toolbar = [[[_self subviews] filteredArrayUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(UIView *view, NSDictionary *bindings) {
            return [view isKindOfClass:[UIToolbar class]] && [NSStringFromClass([view class]) hasPrefix:@"PSPDF"];
        }]] ps_firstObject];

        // if there is a toolbar, make sure it's on top.
        if (toolbar && [_self.subviews indexOfObject:toolbar] < [_self.subviews count]-1) {
            [_self bringSubviewToFront:toolbar];
            toolbar.frame = _self.bounds;
        }
    }));
    PSPDFReplaceMethod(overrideClass, @selector(layoutSubviews), layoutSEL, layoutIMP);
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Button Actions

- (void)toggleToolbarMode:(PSPDFAnnotationToolbarMode)toolbarMode {
    if (self.toolbarMode == toolbarMode) {
        [self removeSelectionAnnotationViewAnimated:YES];
        self.toolbarMode = PSPDFAnnotationToolbarNone;
    }else {
        if (self.toolbarMode == PSPDFAnnotationToolbarNone && toolbarMode != PSPDFAnnotationToolbarNone) {
            [self addSelectionAnnotationView];
        }
        self.toolbarMode = toolbarMode;
    }
}

- (void)noteButtonPressed:(id)sender {
    [self toggleToolbarMode:PSPDFAnnotationToolbarNote];
}

- (void)highlightButtonPressed:(id)sender {
    [self toggleToolbarMode:PSPDFAnnotationToolbarHighlight];
}

- (void)strikeOutButtonPressed:(id)sender {
    [self toggleToolbarMode:PSPDFAnnotationToolbarStrikeOut];
}

- (void)underlineButtonPressed:(id)sender {
    [self toggleToolbarMode:PSPDFAnnotationToolbarUnderline];
}

- (void)freeTextButtonPressed:(id)sender {
    [self toggleToolbarMode:PSPDFAnnotationToolbarFreeText];
}

- (void)signatureButtonPressed:(id)sender {
    PSPDFSignatureViewController *signatureController = [[PSPDFSignatureViewController alloc] init];
    signatureController.delegate = [self.pdfController pageViewForPage:self.pdfController.page];

    [self.pdfController presentViewControllerModalOrPopover:signatureController embeddedInNavigationController:YES withCloseButton:NO animated:YES sender:sender options:@{PSPDFPresentOptionAlwaysModal : @YES, PSPDFPresentOptionModalPresentationStyle : @(UIModalPresentationFormSheet)}];
}

// similar to PSPDFDrawView; detect touches, lock PDF UI.
- (void)addSelectionAnnotationView {
    [self lockPDFController];
    [self.pdfController hideControlsAndPageElements];

    for (PSPDFPageView *pageView in [self.pdfController visiblePageViews]) {
        PSPDFSelectionView *selectionView = [[PSPDFSelectionView alloc] initWithFrame:pageView.bounds delegate:self];
        [pageView addSubview:selectionView];
        _selectionViews[@(pageView.page)] = selectionView;
    }
}

- (void)removeSelectionAnnotationViewAnimated:(BOOL)animated {
    for (PSPDFSelectionView *selectionView in [_selectionViews allValues]) {
        if ([self.pdfController isControllerLocked]) [self unlockPDFControllerAndEnsureToStayOnTop:animated];

        [selectionView removeFromSuperview];
        selectionView.delegate = nil;
    }
    [_selectionViews removeAllObjects];
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Drawing

- (void)drawButtonPressed:(id)sender {
    self.toolbarMode = PSPDFAnnotationToolbarDraw;

    [self lockPDFController];
    [self.pdfController hideControlsAndPageElements];
    _origInternalTapGesturesEnabled = self.pdfController.internalTapGesturesEnabled;
    self.pdfController.internalTapGesturesEnabled = NO;

    // update toolbar for drawing.
    UIBarButtonItem *cancelItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancelDrawing)];
    UIBarButtonItem *doneItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneDrawing)];
    UIBarButtonItem *flexSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    UIBarButtonItem *space = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    space.width = 32.0;
    _undoItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemUndo target:self action:@selector(undoDrawing:)];
    _undoItem.enabled = NO;
    _redoItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemRedo target:self action:@selector(redoDrawing:)];
    _redoItem.enabled = NO;

    _strokeColorButton = [PSPDFColorButton buttonWithType:UIButtonTypeCustom];

    // as we lock orientation, button size needs to be checked only here.
    CGFloat buttonSize = roundf(PSPDFToolbarHeightForOrientation(self.pdfController.interfaceOrientation) * 0.75f);
    _strokeColorButton.frame = CGRectMake(0, 0, buttonSize, buttonSize);

    _strokeColorButton.color = self.drawColor;
    _strokeColorButton.displayAsEllipse = YES;
    [_strokeColorButton addTarget:self action:@selector(selectStrokeColor:) forControlEvents:UIControlEventTouchUpInside];
    _colorButtonItem = [[UIBarButtonItem alloc] initWithCustomView:_strokeColorButton];
    [self setItems:@[cancelItem, flexSpace, _colorButtonItem, flexSpace, _undoItem, _redoItem, flexSpace, doneItem] animated:YES];

    for (PSPDFPageView *pageView in [self.pdfController visiblePageViews]) {
        PSPDFDrawView *drawView = [[PSPDFDrawView alloc] initWithFrame:pageView.bounds];
        drawView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        drawView.delegate = self;
        drawView.alpha = 0.0;
        drawView.strokeColor = self.drawColor;
        drawView.lineWidth = self.lineWidth;
        [pageView addSubview:drawView];
        [UIView animateWithDuration:0.25 animations:^{
            drawView.alpha = 1.0;
        }];
        _drawViews[@(pageView.page)] = drawView;
    }
}

- (void)cancelDrawing { [self cancelDrawingAnimated:YES]; }
- (void)doneDrawing { [self doneDrawingAnimated:YES]; }

- (void)cancelDrawingAnimated:(BOOL)animated {
    [self.pdfController.popoverController dismissPopoverAnimated:animated];
    self.pdfController.popoverController = nil;
    [self finishDrawingAnimated:animated andSaveAnnotation:NO];
}

- (void)doneDrawingAnimated:(BOOL)animated {
    [self finishDrawingAnimated:animated andSaveAnnotation:YES];
    [self.pdfController.popoverController dismissPopoverAnimated:animated];
    self.pdfController.popoverController = nil;
}

- (void)finishDrawingAnimated:(BOOL)animated andSaveAnnotation:(BOOL)saveAnnotation {
    [self unlockPDFControllerAndEnsureToStayOnTop:YES];
    self.pdfController.internalTapGesturesEnabled = _origInternalTapGesturesEnabled;

    [_drawViews enumerateKeysAndObjectsUsingBlock:^(NSNumber *pageNumber, PSPDFDrawView *drawView, BOOL *stop) {
        PSPDFPageView *pageView = [self.pdfController pageViewForPage:[pageNumber unsignedIntegerValue]];
        if (!pageView) {
            PSPDFLogWarning(@"pageView not found, skipping %@", drawView);
        }else {
            if (saveAnnotation) {
                NSUInteger page = [[[_drawViews allKeysForObject:drawView] ps_firstObject] unsignedIntegerValue];
                [self addInkAnnotationWithLines:drawView.linesDictionaries bounds:[pageView convertRect:drawView.frame fromView:pageView] page:page];
            }
        }
    }];

    // reset original buttons (only restore once)
    if (self.toolbarMode != PSPDFAnnotationToolbarNone) {
        [self setItems:_originalItems animated:animated];
        [self removeDrawingViewAnimated:animated];
        self.toolbarMode = PSPDFAnnotationToolbarNone;
    }

    // auto-hide after drawing finished/cancelled if enabled.
    if (self.hideAfterDrawingDidFinish) {
        [self hideToolbarAnimated:NO completion:^{
            [self removeFromSuperview];
        }];
        self.hideAfterDrawingDidFinish = NO;
    }
}

- (void)removeDrawingViewAnimated:(BOOL)animated {
    if ([_drawViews count]) {
        if (animated) {
            [UIView animateWithDuration:0.25 delay:0.f options:UIViewAnimationOptionAllowUserInteraction animations:^{
                for (PSPDFDrawView *drawView in [_drawViews allValues]) {
                    drawView.alpha = 0.0;
                }
            } completion:^(BOOL finished) {
                [[_drawViews allValues] makeObjectsPerformSelector:@selector(removeFromSuperview)];
            }];
        }else {
            [[_drawViews allValues] makeObjectsPerformSelector:@selector(removeFromSuperview)];
        }
        [[_drawViews allValues] makeObjectsPerformSelector:@selector(setDelegate:) withObject:nil];
        [_drawViews removeAllObjects];
        [_drawViewUndoStack removeAllObjects];
        _drawViewUndoStackPosition = 0;
    }
}

- (PSPDFDrawView *)currentUndoDrawView {
    return _drawViewUndoStackPosition > 0 ? _drawViewUndoStack[_drawViewUndoStackPosition-1] : nil;
}

- (PSPDFDrawView *)currentRedoDrawView {
    return _drawViewUndoStackPosition < [_drawViewUndoStack count] ? _drawViewUndoStack[_drawViewUndoStackPosition] : nil;
}

- (void)undoDrawing:(id)sender {
    _isDoingUndoRedo = YES;
    if ([[self currentUndoDrawView] undo]) {
        _drawViewUndoStackPosition--;
    }
    [self updateDrawingToolbar];
    _isDoingUndoRedo = NO;
}

- (void)redoDrawing:(id)sender {
    _isDoingUndoRedo = YES;
    if ([[self currentRedoDrawView] redo]) {
        _drawViewUndoStackPosition++;
    }
    [self updateDrawingToolbar];
    _isDoingUndoRedo = NO;
}

- (void)updateDrawingToolbar {
    _undoItem.enabled = [[self currentUndoDrawView] canUndo];
	_redoItem.enabled = [[self currentRedoDrawView] canRedo];
}

- (void)selectStrokeColor:(id)sender {
    BOOL alreadyDisplayed = PSPDFIsControllerClassInPopover(self.pdfController.popoverController, [PSPDFSimplePageViewController class]);
    if (alreadyDisplayed) {
        [self.pdfController.popoverController dismissPopoverAnimated:YES];
        self.pdfController.popoverController = nil;
    }else {
        NSString *viewControllerTitle = PSPDFLocalize(@"Drawing Color");
        PSPDFSimplePageViewController *colorPicker = [[self.pdfController classForClass:[PSPDFColorSelectionViewController class]] defaultColorPickerWithTitle:viewControllerTitle delegate:self];
        if (colorPicker) {
            [self.pdfController presentViewControllerModalOrPopover:colorPicker embeddedInNavigationController:YES withCloseButton:YES animated:YES sender:_colorButtonItem options:@{PSPDFPresentOptionPassthroughViews : @[self]}];
        }else {
            PSPDFLogError(@"Color picker can't be displayed. Is PSPDFKit.bundle missing?");
        }
    }
}

- (void)doneButtonPressed:(id)sender {
    [self hideToolbarAnimated:YES completion:^{
        [self removeFromSuperview];
    }];
}

static UIView *PSPDFToolbarViewForBarButtonItem(UIToolbar *toolbar, UIBarButtonItem *barButtonItem) {
    UIView *barButtonView = nil;
    for (UIControl *subview in toolbar.subviews) {
        if ([NSStringFromClass([subview class]) hasPrefix:@"UIToolbarB"] && [subview isKindOfClass:[UIControl class]]) {
            for (UIBarButtonItem *aBarButtonItem in [subview allTargets]) {
                if (barButtonItem == aBarButtonItem) { barButtonView = subview; break; }
            }
        }
    }
    return barButtonView;
}

// set mode and mark in toolbar
- (void)setToolbarMode:(PSPDFAnnotationToolbarMode)toolbarMode {
    if (toolbarMode != _toolbarMode) {
        _toolbarMode = toolbarMode;
        [self updateToolbarModeIndicatorView];

        // call the delegate
        if ([self.delegate respondsToSelector:@selector(annotationToolbar:didChangeMode:)]) {
            [self.delegate annotationToolbar:self didChangeMode:toolbarMode];
        }
    }
}

// updates the white indicator view
- (void)updateToolbarModeIndicatorView {
    if (self.toolbarMode == PSPDFAnnotationToolbarNone || self.toolbarMode == PSPDFAnnotationToolbarDraw) {
        _toolbarOverlay.hidden = YES;
    }else {
        _toolbarOverlay.hidden = NO;

        UIView *barButtonView = PSPDFToolbarViewForBarButtonItem(self, _barButtonToTypeDict[@(self.toolbarMode)]);
        if (barButtonView) {
            CGFloat highlightWidth = floorf(self.frame.size.width/25);
            CGRect toolbarOverlayFrame = CGRectInset(barButtonView.frame, -highlightWidth, 0);
            // be pixel perfect. iPhone needs one pixel off.
            if (!PSIsIpad()) {
                toolbarOverlayFrame.size.height -= 1;
                toolbarOverlayFrame.origin.y += 1;
            }
            _toolbarOverlay.frame = toolbarOverlayFrame;
        }
    }
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Annotation Change Listener

// Remembers the last used color.
- (void)annotationChanged:(NSNotification *)notification {
    PSPDFAnnotation *annotation = notification.object;
    if ([annotation.typeString length] && annotation.color) {
        [self setLastUsedColor:annotation.color forAnnotationType:annotation.typeString];
    }
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - PSPDFDrawViewDelegate

- (void)drawView:(PSPDFDrawView *)drawView didChange:(PSPDFDrawAction *)drawAction {
    if (!_isDoingUndoRedo) {
        [_drawViewUndoStack addObject:drawView]; // update undo stack
        _drawViewUndoStackPosition = [_drawViewUndoStack count];
    }
    [self updateDrawingToolbar];
}

- (void)addInkAnnotationWithLines:(NSArray *)lines bounds:(CGRect)inkBounds page:(NSUInteger)page {
	if ([lines count] == 0) return;

	@autoreleasepool {
        PSPDFPageView *pageView = [self.pdfController pageViewForPage:page];
		float minX = MAXFLOAT;
		float maxX = -MAXFLOAT;
		float minY = MAXFLOAT;
		float maxY = -MAXFLOAT;

		UIColor *prevColor = nil;
		float prevLineWidth = 0.0;

		NSMutableArray *inkAnnotations = [NSMutableArray array];

		PSPDFInkAnnotation *currentAnnotation = nil;
		NSMutableArray *currentAnnotationLines = nil;

		for (NSDictionary *line in lines) {
			NSArray *points = line[kPSPDFPointsKey];
			float lineWidth = [line[kPSPDFWidthKey] floatValue];
			UIColor *color = line[kPSPDFColorKey];

			if (![color isEqual:prevColor] || lineWidth != prevLineWidth) {
				// Begin new ink annotation
				if (currentAnnotation) {
					currentAnnotation.boundingBox = CGRectMake(minX, minY, maxX-minX, maxY-minY);
                    currentAnnotation.lines = currentAnnotationLines;
					minX = MAXFLOAT;
					maxX = -MAXFLOAT;
					minY = MAXFLOAT;
					maxY = -MAXFLOAT;
				}

				currentAnnotation = [[self.pdfController.document classForClass:[PSPDFInkAnnotation class]] new];
				currentAnnotation.color = [color colorWithAlphaComponent:1.f];
				currentAnnotation.alpha = CGColorGetAlpha(color.CGColor);
				currentAnnotation.lineWidth = lineWidth;
				currentAnnotationLines = [NSMutableArray array];
                currentAnnotation.documentProvider = [pageView.document documentProviderForPage:pageView.page];
				[inkAnnotations addObject:currentAnnotation];
				prevColor = color;
				prevLineWidth = lineWidth;
			}

			NSMutableArray *currentLine = [NSMutableArray array];
			for (NSValue *pointValue in points) {
				CGPoint point = [pointValue CGPointValue];
                CGPoint scaledPoint = [pageView convertViewPointToPDFPoint:point];
				if (scaledPoint.x < minX) minX = scaledPoint.x;
				if (scaledPoint.y < minY) minY = scaledPoint.y;
				if (scaledPoint.x > maxX) maxX = scaledPoint.x;
				if (scaledPoint.y > maxY) maxY = scaledPoint.y;
				[currentLine addObject:[NSValue valueWithCGPoint:scaledPoint]];
			}
			[currentAnnotationLines addObject:currentLine];
		}
		currentAnnotation.boundingBox = CGRectMake(minX - currentAnnotation.lineWidth/2,
												   minY - currentAnnotation.lineWidth/2,
												   maxX-minX + currentAnnotation.lineWidth,
												   maxY-minY + currentAnnotation.lineWidth);

        // set at very last, since boundingBox would change lines.
        currentAnnotation.lines = currentAnnotationLines;

		for (PSPDFInkAnnotation *ink in inkAnnotations) {
			[ink rebuildPaths];
		}
        [pageView.document addAnnotations:inkAnnotations forPage:pageView.page];
        [pageView updateView];
	}
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - PSPDFSelectionViewDelegate

// handle comment creation
- (BOOL)selectionView:(PSPDFSelectionView *)selectionView shouldStartSelectionAtPoint:(CGPoint)point {
    [_selectionViewRectCache removeAllObjects]; // clear cache
    NSUInteger page = [[[_selectionViews allKeysForObject:selectionView] ps_firstObject] unsignedIntegerValue];
    PSPDFPageView *pageView = [self.pdfController pageViewForPage:page];
    pageView.selectedAnnotation = nil; // clear any selection

    PSPDFAnnotationToolbarMode toolbarMode = self.toolbarMode;
    BOOL handleSelection = NO;

    if (toolbarMode == PSPDFAnnotationToolbarNote || toolbarMode == PSPDFAnnotationToolbarFreeText) {
        BOOL isNote = toolbarMode == PSPDFAnnotationToolbarNote;

        // get unrotated point
        CGPoint pdfPoint = [pageView convertViewPointToPDFPoint:point];
        CGRect noteRect;
        if (isNote) {
            noteRect = CGRectMake(pdfPoint.x - 16, pdfPoint.y - 16, 32, 32);
        }else {
            noteRect = CGRectMake(pdfPoint.x, pdfPoint.y-80, 150, 80);
        }
        // rotate just the size component
        CGSize targetSize = CGSizeApplyAffineTransform(noteRect.size, pageView.pageInfo.pageRotationTransform);
        noteRect.size.width = fabs(targetSize.width); noteRect.size.height = fabs(targetSize.height);

        // create annotation
        Class annotationClass = [self.pdfController classForClass:isNote ? [PSPDFNoteAnnotation class] : [PSPDFFreeTextAnnotation class]];
        PSPDFAnnotation *annotation = [annotationClass new];
        annotation.boundingBox = noteRect;

        // add annotation and redraw
        [pageView.document addAnnotations:@[annotation] forPage:pageView.page];
        [pageView addAnnotation:annotation animated:NO];

        // disable toolbar mode
        [self removeSelectionAnnotationViewAnimated:YES];
        self.toolbarMode = PSPDFAnnotationToolbarNone;

        if (annotation.isOverlay) {
            [self removeSelectionAnnotationViewAnimated:YES];
        }else {
            [pageView updateView];
        }

        // open annotation controller and show keyboard
        [pageView showNoteControllerForAnnotation:annotation showKeyboard:YES animated:YES];
    }else {
        handleSelection = YES;
    }

    // disrupt current active tap gesture
    if (toolbarMode != PSPDFAnnotationToolbarNone) {
        [pageView.scrollView setCurrentTouchEventAsProcessed];
    }

    return handleSelection;
}

#define kPSPDFTapDetectorThreshold 5

// preview selected words
- (void)selectionView:(PSPDFSelectionView *)selectionView updateSelectedRect:(CGRect)rect {
    // find current page
    __block NSUInteger page;
    [_selectionViews enumerateKeysAndObjectsUsingBlock:^(NSNumber *pageNumber, PSPDFSelectionView *aSelectionView, BOOL *stop) {
        if (selectionView == aSelectionView) {
            page = [pageNumber unsignedIntegerValue]; *stop = YES;
        }
    }];

    // get affected objects
    PSPDFPageView *pageView = [self.pdfController pageViewForPage:page];
    NSDictionary *objects = [pageView objectsAtRect:rect options:@{kPSPDFObjectsFullWords : @YES}];
    NSArray *newWords = objects[kPSPDFWords];

    // use a cache to save some caclulation of the words stay the same
    NSArray *currentWords = _selectionViewRectCache[@(page)];
    if (![currentWords isEqual:newWords]) {
        if (newWords) _selectionViewRectCache[@(page)] = newWords;

        // convert to screen space
        CGRect *rawRects = NULL;
        if ([newWords count]) {
            rawRects = malloc([newWords count] * sizeof(CGRect)); // malloc(0) is not defined
            [newWords enumerateObjectsUsingBlock:^(PSPDFWord *word, NSUInteger idx, BOOL *stop) {
                rawRects[idx] = [pageView convertGlyphRectToViewRect:word.frame];
            }];
        }
        [selectionView setRawRects:rawRects count:[newWords count]];
    }
}

/// Called when a rect was selected successfully. (touchesEnded)
- (void)selectionView:(PSPDFSelectionView *)selectionView finishedWithSelectedRect:(CGRect)rect {
    [_selectionViewRectCache removeAllObjects]; // clear cache

    PSPDFHighlightAnnotationType highlightType;
    switch (self.toolbarMode) {
        case PSPDFAnnotationToolbarHighlight:
            highlightType = PSPDFHighlightAnnotationHighlight; break;
        case PSPDFAnnotationToolbarStrikeOut:
            highlightType = PSPDFHighlightAnnotationStrikeOut; break;
        case PSPDFAnnotationToolbarUnderline:
            highlightType = PSPDFHighlightAnnotationUnderline; break;
        default:
            PSPDFLogWarning(@"Invalid toolbar mode."); return;
    }

    // remove rect highlights
    selectionView.rects = nil;

    // detect objects
    NSUInteger page = [[[_selectionViews allKeysForObject:selectionView] ps_firstObject] unsignedIntegerValue];
    PSPDFPageView *pageView = [self.pdfController pageViewForPage:page];
    BOOL shouldAddNewAnnotation = YES;
    // make a hit-test if we might just have tapped on an annotation?
    if (rect.size.width < kPSPDFTapDetectorThreshold && rect.size.height < kPSPDFTapDetectorThreshold) {
        NSArray *tappableAnnotations = nil;
#ifndef PSPDFKIT_DISABLE_TEXT_SELECTION_FEATURES
        tappableAnnotations = [pageView tappableAnnotationsAtPoint:rect.origin];
#endif
        if ([tappableAnnotations count]) {
            shouldAddNewAnnotation = NO;

            // since the regular tap gesture is disrupted, we need to relay manually
            [pageView singleTappedAtViewPoint:rect.origin];
        }
    }

    if (shouldAddNewAnnotation) {
        NSDictionary *objects = [pageView objectsAtRect:rect options:@{kPSPDFObjectsGlyphs : @YES}];

        // build rects
        CGAffineTransform t = pageView.pageInfo.pageRotationTransform;
        CGRect boundingBox;
        NSArray *highlighedRects = PSPDFRectsFromGlyphs(objects[kPSPDFGlyphs], t, &boundingBox);

        // build annotation
        PSPDFHighlightAnnotation *annotation = [[[self.pdfController.document classForClass:[PSPDFHighlightAnnotation class]] alloc] initWithHighlightType:highlightType];
        annotation.boundingBox = boundingBox;
        annotation.rects = highlighedRects;
        // if theres's a color set, use it.
        UIColor *customColor = [self lastUsedColorForAnnotationTypeString:annotation.typeString];
        if (customColor) annotation.color = customColor;

        // add annotation and redraw
        [pageView.document addAnnotations:@[annotation] forPage:pageView.page];
        [pageView updateView];
    }
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - PSPDFColorSelectionViewControllerDelegate

- (UIColor *)colorSelectionControllerSelectedColor:(PSPDFColorSelectionViewController *)controller {
    return self.drawColor;
}

- (void)colorSelectionController:(PSPDFColorSelectionViewController *)controller didSelectedColor:(UIColor *)color {
    self.drawColor = color;
}
 
@end
