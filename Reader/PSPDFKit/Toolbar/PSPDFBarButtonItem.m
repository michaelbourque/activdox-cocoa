//
//  PSPDFBarButtonItem.m
//  PSPDFKit
//
//  Copyright (c) 2012 Peter Steinberger. All rights reserved.
//

#import "PSPDFBarButtonItem.h"
#import "PSPDFViewController.h"
#import "PSPDFMoreBarButtonItem.h"
#import "PSPDFDocument.h"
#import "PSPDFViewController+Internal.h"
#import "PSPDFTransparentToolbar.h"

// Allows to easily mix UIBarButtonItem and PSPDFBarButtonItem in rightBarButtonItems and leftBarButtonItems
@implementation UIBarButtonItem (PSPDFBarButtonItem)
- (BOOL)isAvailable { return YES; }
@end

@interface PSPDFBarButtonItem ()
@property (nonatomic, weak) PSPDFBarButtonItem *senderBarButtonItem;
@property (nonatomic, assign, getter=isLongPressActionActive) BOOL longPressActionActive;
@end

@implementation PSPDFBarButtonItem

static __strong PSPDFBarButtonItem *currentBarButtonItem = nil;

+ (void)dismissPopoverAnimated:(BOOL)animated {
    // prevent entering multiple times
    static BOOL isDismissingPopover = NO;
    if (!isDismissingPopover) {
        isDismissingPopover = YES;
        [currentBarButtonItem dismissAnimated:animated];
        [currentBarButtonItem didDismiss];
        isDismissingPopover = NO;
    }
}

+ (BOOL)isPopoverVisible {
    return currentBarButtonItem != nil;
}

- (id)initWithPDFViewController:(PSPDFViewController *)pdfController {
    _pdfController = pdfController;
    
    if (self.customView) {
        self = [super initWithCustomView:self.customView];
    }else if (self.image) {
        if (self.landscapeImagePhone && kCFCoreFoundationVersionNumber >= kCFCoreFoundationVersionNumber_iOS_5_0) {
            self = [super initWithImage:self.image landscapeImagePhone:self.landscapeImagePhone style:self.itemStyle target:self action:@selector(action:)];
        }else {
            self = [super initWithImage:self.image style:[self itemStyle] target:self action:@selector(action:)];
        }
    }else if (self.systemItem != (UIBarButtonSystemItem)-1) {
        self = [super initWithBarButtonSystemItem:self.systemItem target:self action:@selector(action:)];
    }else {
        self = [super initWithTitle:[self actionName] style:[self itemStyle] target:self action:@selector(action:)];
    }

    [self updateBarButtonItem];
    return self;
}

- (BOOL)isAvailable {
    return YES;
}

- (BOOL)isLongPressActionAvailable {
    return NO;
}

- (void)updateBarButtonItem {
    self.enabled = [_pdfController.document isValid];
}

- (UIView *)customView {
    return nil;
}

- (UIImage *)image {
    return nil;
}

- (UIImage *)landscapeImagePhone {
    return nil;
}

- (UIBarButtonSystemItem)systemItem {
    return (UIBarButtonSystemItem)-1;
}

- (NSString *)actionName {
    return nil;
}

// voiceover
- (NSString *)accessibilityLabel {
    return [self actionName];
}

- (UIBarButtonItemStyle)itemStyle {
    return UIBarButtonItemStylePlain;
}

- (id)presentAnimated:(BOOL)animated sender:(id)sender {
    [self doesNotRecognizeSelector:_cmd];
    return nil;
}

- (void)dismissAnimated:(BOOL)animated {
    // default implementation
    [self dismissModalOrPopoverAnimated:animated];
}

- (void)didDismiss {
    currentBarButtonItem.senderBarButtonItem = nil;
    currentBarButtonItem.pdfController.popoverController = nil;
    currentBarButtonItem = nil;
}

- (void)handleAction:(PSPDFBarButtonItem *)sender isLongPress:(BOOL)isLongPress {
    if (!self.pdfController) { PSPDFLogError(@"PDF controller is not set!"); return; }

    self.longPressActionActive = isLongPress;
    PSPDFBarButtonItem *originalButtonItem = sender;
    BOOL dismissOnly = sender == currentBarButtonItem.senderBarButtonItem && originalButtonItem == self;

    // now the tricky part. If we're invoked from an external source, don't be clever on HUD state.
    // we simple check if we're managed (thus, inside a PSPDFTransparentToolbar, not a regular UIToolbar or sth else)
    // nextResponder is not declared, but it works (and it makes sense for UIBarButtonItem to be in the responder chain)
    BOOL invokedExternally = ![sender respondsToSelector:@selector(nextResponder)] || ![[(UIResponder *)sender nextResponder] isKindOfClass:[PSPDFTransparentToolbar class]];

    // dismiss any active popovers from the barbutton and also any generic open popovers
    [[self class] dismissPopoverAnimated:dismissOnly];
    [self.pdfController.popoverController dismissPopoverAnimated:YES];
    self.pdfController.popoverController = nil;

    // never invoke presentAnimated if button is not available
    if ((!isLongPress && [self isAvailable]) || (isLongPress && [self isLongPressActionActive])) {
        if (!dismissOnly && (invokedExternally || !self.pdfController.isNavigationBarHidden)) {
            self.pdfController.popoverController = nil;
            id presentedObject = [self presentAnimated:YES sender:sender];
            if (presentedObject) {
                [self addPassthroughViewsToPopoverControllerForObject:presentedObject];
                currentBarButtonItem = self;
                currentBarButtonItem.senderBarButtonItem = sender;
            }
        }
    }
    self.longPressActionActive = NO;
}

- (void)action:(PSPDFBarButtonItem *)sender { [self handleAction:sender isLongPress:NO]; }
- (void)longPressAction:(PSPDFBarButtonItem *)sender { [self handleAction:sender isLongPress:YES]; }

// present controller modally or in popover - depending on the platform
- (id)presentModalOrInPopover:(UIViewController *)viewController sender:(id)sender {
    return [self.pdfController presentViewControllerModalOrPopover:viewController embeddedInNavigationController:YES withCloseButton:YES animated:YES sender:sender options:nil];
}

- (void)dismissModalOrPopoverAnimated:(BOOL)animated {
    if (PSIsIpad()) {
        [self.pdfController.popoverController dismissPopoverAnimated:animated];
        self.pdfController.popoverController = nil;
    }else {
        [self.pdfController dismissViewControllerAnimated:animated completion:NULL];
    }
}

// Hacky, but not critical if it fails.
+ (UIPopoverController *)popoverControllerForObject:(id)object {
    if (!PSIsIpad()) return nil;
    
    UIPopoverController *popoverController = object;
    
#ifndef PSPDFKIT_DONT_USE_OBFUSCATED_PRIVATE_API
    NSString *printInteractionControllerKeyPath = [NSString stringWithFormat:@"%1$@%2$@.%1$@%3$@%4$@%5$@.%6$@%5$@", @"print", @"State", @"Panel", @"View", @"Controller", @"pover"];
    @try {
        if ([object isKindOfClass:[UIPopoverController class]]) {
            popoverController = object;
        }else if ([object isKindOfClass:[UIPrintInteractionController class]]) {
            popoverController = [object valueForKeyPath:printInteractionControllerKeyPath];
        }else {
            // Works at least for UIDocumentInteractionController and UIActionSheet
            popoverController = [object valueForKey:NSStringFromSelector(@selector(popoverController))];
        }
    }
    @catch (NSException *exception) {
        @try {
            // If Apple fixes the typo in UIPrintPanelViewController and renames the _poverController ivar into _popoverController
            printInteractionControllerKeyPath = [printInteractionControllerKeyPath stringByReplacingOccurrencesOfString:@"pover" withString:@"popover"];
            popoverController = [object valueForKeyPath:printInteractionControllerKeyPath];
        }
        @catch (NSException *exception) {
            popoverController = nil;
        }
    }
#endif

    if ([popoverController isKindOfClass:[UIPopoverController class]]) return popoverController;
    else return nil;
}

// add new passthrough views and don't remove preexisting ones
- (void)addPassthroughViewsToPopoverControllerForObject:(id)object {
    if (self.pdfController.navigationController.navigationBar) {
        UIPopoverController *popoverController = [[self class] popoverControllerForObject:object];
        NSMutableArray *passthroughViews = [popoverController.passthroughViews mutableCopy] ?: [NSMutableArray array];
        [passthroughViews addObject:self.pdfController.navigationController.navigationBar];
        popoverController.passthroughViews = passthroughViews;
    }
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSCopying

- (id)copyWithZone:(NSZone *)zone {
    PSPDFBarButtonItem *barButtonItem = [[[self class] alloc] initWithPDFViewController:self.pdfController];
    return barButtonItem;
}

@end
