//
//  PSPDFOpenInBarButtonItem.m
//  PSPDFKit
//
//  Copyright (c) 2012 Peter Steinberger. All rights reserved.
//

#import "PSPDFOpenInBarButtonItem.h"
#import "PSPDFViewController.h"
#import "PSPDFDocument.h"
#import "PSPDFViewController+Internal.h"
#import "PSPDFProcessor.h"
#import "PSPDFProgressHUD.h"

@interface PSPDFOpenInBarButtonItem ()
@property (nonatomic, strong) UIDocumentInteractionController *documentInteractionController;
@end

BOOL kPSPDFCheckIfCompatibleAppsAreInstalled = NO;

@implementation PSPDFOpenInBarButtonItem

- (id)initWithPDFViewController:(PSPDFViewController *)pdfViewController {
    if ((self = [super initWithPDFViewController:pdfViewController])) {
        _allowFileMergingAndTempFiles = YES;
    }
    return self;
}

- (BOOL)isCompatibleDocument:(PSPDFDocument *)document {
    if (!self.allowFileMergingAndTempFiles) {
        return [document.files count] == 1 && [document.dataArray count] == 0 && document.dataProvider == 0;
    }else {
        return YES;
    }
}

- (BOOL)isAvailable {
    BOOL isAvailable;

    // this check is slow and ugly.
    if (kPSPDFCheckIfCompatibleAppsAreInstalled) {
        isAvailable = PSIsSimulator();
        PSPDFDocument *document = self.pdfController.document;
        @try {
            if ([UIDocumentInteractionController class] && document.fileURL) {
                UIDocumentInteractionController *docController = [UIDocumentInteractionController interactionControllerWithURL:document.fileURL];
                if (docController) {
                    // this throws an exception on the simulator, it doesn't kill the app, but it's annoying
                    // and we don't expect to find apps that support the openIn in the simulator anyway.
                    // Also, this check is SLOW.
                    static BOOL _documentInteractionControllerIsAvailable = NO;
                    static dispatch_once_t onceToken;
                    dispatch_once(&onceToken, ^{
                        PSPDF_IF_NOT_SIMULATOR(_documentInteractionControllerIsAvailable = [docController presentOpenInMenuFromRect:CGRectMake(0, 0, 1, 1) inView:[UIApplication sharedApplication].keyWindow animated:NO]; // Rect is not allowed to be CGRectZero
                                               [docController dismissMenuAnimated:NO];)
                    });
                    isAvailable = _documentInteractionControllerIsAvailable;
                }
            }
        }
        @catch (NSException *exception) {
            // this sometimes fires internal exceptions, in that case we disable that feature.
            isAvailable = NO;
        }
    }else {
        isAvailable = [self isCompatibleDocument:self.pdfController.document];
    }
    return isAvailable;
}

- (UIBarButtonSystemItem)systemItem {
    return UIBarButtonSystemItemAction;
}

- (NSString *)actionName {
    return PSPDFLocalize(@"Open in...");
}

- (id)presentAnimated:(BOOL)animated sender:(id)sender {
    PSPDFDocument *document = self.pdfController.document;
    if (![self isCompatibleDocument:document]) {
        PSPDFLogWarning(@"Document must consist of exactly one PDF file. (no data, no dataProviders)");
        return nil;
    }

    // at lest it doesn't crash anymore on iOS6...
    PSPDF_IF_PRE_IOS6(PSPDF_IF_SIMULATOR([[[UIAlertView alloc] initWithTitle:@"PSPDFKit Simulator Notice" message:@"Open In... doesn't properly work in the iOS Simulator. It's hard-locked to always show. On a device, we ask UIDocumentInteractionController if the document can be opened with other applications." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show]; return nil;))


#ifndef PSPDFKIT_DISABLE_TEXT_SELECTION_FEATURES
    if ([document canEmbedAnnotations]) {
        NSError *error = nil;
        if (![document saveChangedAnnotationsWithError:&error]) {
            PSPDFLogWarning(@"Error while trying to save for Open In...: %@", error);
        }
    }
#endif

    // can we use file or do we need to merge?
    NSURL *fileURL = [self fileURLForDocument:document];
    if (!fileURL) return nil;
    self.documentInteractionController = [self interactionControllerWithURL:fileURL];
    if (!self.documentInteractionController) return nil;
    
    BOOL success = NO;
    BOOL shouldShow = [self.pdfController delegateShouldShowController:_documentInteractionController embeddedInController:[[self class] popoverControllerForObject:_documentInteractionController] animated:animated];
    if (shouldShow) {
        if ([sender isKindOfClass:[UIBarButtonItem class]]) {
            success = [self.documentInteractionController presentOpenInMenuFromBarButtonItem:sender animated:animated];
        }else if ([sender isKindOfClass:[UIView class]]) {
            success = [self.documentInteractionController presentOpenInMenuFromRect:[sender bounds] inView:sender animated:animated];
        }else {
            PSPDFLogError(@"Cannot display controller - sender is neither a UIView or a UIBarButtonItem."); return nil;
        }
        if (!success) {
            [[[UIAlertView alloc] initWithTitle:PSPDFLocalize(@"Open in...") message:PSPDFLocalize(@"No Applications found") delegate:nil cancelButtonTitle:PSPDFLocalize(@"OK") otherButtonTitles:nil] show];
        }
    }
    return success ? self.documentInteractionController : nil;
}

- (void)dismissAnimated:(BOOL)animated {
    [self.documentInteractionController dismissMenuAnimated:animated];
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Private

- (NSURL *)fileURLForDocument:(PSPDFDocument *)document {
    NSURL *fileURL = document.fileURL;
    if ([document.files count] > 1 || document.data || document.dataProvider) {
        BOOL shouldShowProgress = [document pageCount] > 10;
        if (shouldShowProgress) [PSPDFProgressHUD showWithStatus:PSPDFLocalize(@"Preparing...")];

        NSError *error;
        NSString *basePath = [NSTemporaryDirectory() stringByAppendingPathComponent:document.UID];
        if (![[NSFileManager new] createDirectoryAtPath:basePath withIntermediateDirectories:YES attributes:nil error:&error]) {
            PSPDFLogError(@"Failed to create directory %@", basePath); return nil;
        }
        // create nice file name since other apps might work with that name.
        fileURL = [NSURL fileURLWithPath:[basePath stringByAppendingPathComponent:[document fileNameForPage:0]]];

        [[PSPDFProcessor defaultProcessor] generatePDFFromDocument:document pageRange:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(0, [document pageCount])] outputFileURL:fileURL options:nil];

        if (shouldShowProgress) [PSPDFProgressHUD dismiss];
    }
    return fileURL;
}

- (UIDocumentInteractionController *)interactionControllerWithURL:(NSURL *)fileURL {
    UIDocumentInteractionController *documentInteractionController = [UIDocumentInteractionController interactionControllerWithURL:fileURL];
    documentInteractionController.delegate = self;
    return documentInteractionController;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UIDocumentInteractionControllerDelegate

- (void)documentInteractionControllerDidDismissOpenInMenu:(UIDocumentInteractionController *)controller {
    [self didDismiss];
}

- (void)documentInteractionController:(UIDocumentInteractionController *)controller willBeginSendingToApplication:(NSString *)application {
    PSPDFLogVerbose(@"Sending document to application: %@", application);
}

- (void)documentInteractionController:(UIDocumentInteractionController *)controller didEndSendingToApplication:(NSString *)application {
    PSPDFLogVerbose(@"Sent document to application: %@", application);
    [self didDismiss];
}

@end
