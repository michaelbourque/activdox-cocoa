//
//  PSPDFMoreBarButtonItem.m
//  PSPDFKit
//
//  Copyright (c) 2012 Peter Steinberger. All rights reserved.
//

#import "PSPDFMoreBarButtonItem.h"
#import "PSPDFActionSheet.h"
#import "PSPDFViewController.h"
#import "PSPDFViewController+Internal.h"

@implementation PSPDFMoreBarButtonItem {
    BOOL _isDismissingSheet;
}

- (UIBarButtonSystemItem)systemItem {
    return UIBarButtonSystemItemAction;
}

- (id)presentAnimated:(BOOL)animated sender:(id)sender {
    PSPDFActionSheet *actionSheet = [[PSPDFActionSheet alloc] initWithTitle:nil];
    _isDismissingSheet = NO;
    
    for (PSPDFBarButtonItem *buttonItem in self.pdfController.additionalBarButtonItems) {
        if (![buttonItem isKindOfClass:[UIBarButtonItem class]] || !buttonItem.isAvailable) continue;
        if (!buttonItem.actionName) { PSPDFLogWarning(@"%@ does not have an action name.", buttonItem); continue; }
        
        [actionSheet addButtonWithTitle:buttonItem.actionName block:^{
            [buttonItem.target performSelector:@selector(action:) withObject:sender];
        }];
    }
    
    if ([actionSheet buttonCount] > 0) {
        actionSheet.destroyBlock = ^{
            // we don't get any delegates for the slide down animation on iPhone.
            // if we'd call dismissWithClickedButtonIndex again, the animation would be nil.
            _isDismissingSheet = YES;
            [self didDismiss];
        };
        [actionSheet setCancelButtonWithTitle:PSPDFLocalize(@"Cancel") block:NULL];
        BOOL shouldShow = [self.pdfController delegateShouldShowController:actionSheet embeddedInController:nil animated:animated];

        if (shouldShow) {
            if (PSIsIpad() && [sender isKindOfClass:[UIBarButtonItem class]]) {
                [actionSheet showFromBarButtonItem:sender animated:animated];
            }else if (PSIsIpad() && [sender isKindOfClass:[UIView class]]) {
                [actionSheet showFromRect:[sender bounds] inView:sender animated:animated];
            }else {
                [actionSheet showInView:[self.pdfController masterViewController].view];
            }
            self.actionSheet = actionSheet;
            return self.actionSheet;
        }else {
            return nil;
        }
    }else {
        PSPDFLogError(@"Not showing empty action sheet.");
        return nil;
    }
}

- (void)dismissAnimated:(BOOL)animated {
    if (_isDismissingSheet) return;
    [self.actionSheet dismissWithClickedButtonIndex:self.actionSheet.cancelButtonIndex animated:animated];
}

- (void)didDismiss {
    [super didDismiss];
    self.actionSheet = nil;
}

@end
