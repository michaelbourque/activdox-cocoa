//
//  PSPDFSearchBarButtonItem.m
//  PSPDFKit
//
//  Copyright (c) 2012 Peter Steinberger. All rights reserved.
//

#import "PSPDFSearchBarButtonItem.h"
#import "PSPDFSearchViewController.h"
#import "PSPDFViewController+Internal.h"

@implementation PSPDFSearchBarButtonItem

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - PSPDFBarButtonItem

- (BOOL)isAvailable {
    return [self.pdfController.document isValid] && !self.pdfController.document.isLocked;
}

- (UIBarButtonSystemItem)systemItem {
    return UIBarButtonSystemItemSearch;
}

- (NSString *)actionName {
    return PSPDFLocalize(@"Search");
}

- (id)presentAnimated:(BOOL)animated sender:(PSPDFBarButtonItem *)sender {
    Class viewControllerClass = [self.pdfController classForClass:[PSPDFSearchViewController class]];
    PSPDFSearchViewController *searchController = [[viewControllerClass alloc] initWithDocument:self.pdfController.document pdfController:self.pdfController];

    return [self.pdfController presentViewControllerModalOrPopover:searchController embeddedInNavigationController:NO withCloseButton:YES animated:animated sender:sender options:nil];
}

@end
