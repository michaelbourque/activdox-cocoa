//
//  PSPDFPrintBarButtonItem.m
//  PSPDFKit
//
//  Copyright (c) 2012 Peter Steinberger. All rights reserved.
//

#import "PSPDFPrintBarButtonItem.h"
#import "PSPDFIconGenerator.h"
#import "PSPDFViewController+Internal.h"
#import "PSPDFDocument.h"
#import "PSPDFDocumentProvider.h"

@implementation PSPDFPrintBarButtonItem {
    BOOL isAnimatingPrint_;
    BOOL isShowingPrint_;
}

- (BOOL)isAvailable {
    return [UIPrintInteractionController isPrintingAvailable] && self.pdfController.document.allowsPrinting;
}

- (UIImage *)image {
    return [[PSPDFIconGenerator sharedGenerator] iconForType:PSPDFIconTypePrint];
}

- (void)updateBarButtonItem {
    self.imageInsets = UIEdgeInsetsMake(2.f, 0.f, -2.f, 0.f);
}

- (NSString *)actionName {
    return PSPDFLocalize(@"Print");
}

- (id)presentAnimated:(BOOL)animated sender:(id)sender {
    UIPrintInteractionController *printController = [UIPrintInteractionController sharedPrintController];
    if (isAnimatingPrint_) return printController;

    UIPrintInfo *printInfo = [UIPrintInfo printInfo];
    printInfo.jobName = self.pdfController.document.title;
    printController.delegate = self;
    printController.printInfo = printInfo;

    // iterate over the documentProviders
    NSMutableArray *printItems = [NSMutableArray array];
    NSArray *documentProviders = [self.pdfController.document documentProviders];
    for (PSPDFDocumentProvider *documentProvider in documentProviders) {
        PSPDFLogVerbose(@"Looking for content in %@", documentProvider);
        if (documentProvider.fileURL) {
            [printItems addObject:documentProvider.fileURL];
        }else if (documentProvider.data) {
            [printItems addObject:documentProvider.data];
        }else {
            // basic support for CGDataProviderRef
            NSError *error = nil;
            NSData *data = [documentProvider dataRepresentationWithError:&error];
            if (!data) {
                PSPDFLogWarning(@"Ignoring documentProvider %@. Failed to get NSData. %@", documentProvider, [error localizedDescription]);
            }else {
                [printItems addObject:data];
            }
        }
    }

    if ([printItems count] == 1) {
        // pageRange works only if we use printingItem, not printingItems
        printController.printingItem = [printItems lastObject];
    }else if ([printItems count] > 1) {
        printController.printingItems = printItems;
    }else {
        PSPDFLogWarning(@"No printItems found. Aborting.");
        return nil;
    }

    printController.showsPageRange = YES;

    UIStatusBarStyle savedStatusBarStyle = [UIApplication sharedApplication].statusBarStyle;
    UIPrintInteractionCompletionHandler completionHandler = ^(UIPrintInteractionController *printInteractionController, BOOL completed, NSError *error) {
        if (!PSIsIpad()) [self.pdfController setStatusBarStyle:savedStatusBarStyle animated:animated];
        isShowingPrint_ = NO;
        isAnimatingPrint_ = NO;
        if (error) PSPDFLogError(@"Could not print document. %@", error);
        else PSPDFLogVerbose(@"Printing finished: %d", completed);
    };

    BOOL shouldShow = [self.pdfController delegateShouldShowController:printController embeddedInController:[[self class] popoverControllerForObject:printController] animated:animated];
    BOOL success = NO;

    if (shouldShow) {
        if (PSIsIpad() && [sender isKindOfClass:[UIBarButtonItem class]]) {
            success = [printController presentFromBarButtonItem:sender animated:animated completionHandler:completionHandler];
        }else if (PSIsIpad() && [sender isKindOfClass:[UIView class]]) {
            success = [printController presentFromRect:[sender bounds] inView:sender animated:animated completionHandler:completionHandler];
        }else {
            success = [printController presentAnimated:animated completionHandler:completionHandler];
            [self.pdfController setStatusBarStyle:UIStatusBarStyleDefault animated:animated];
        }
        isShowingPrint_ = success;
        if (!success) PSPDFLogWarning(@"UIPrintInteractionController returned NO when trying to display.");
    }

    return success ? printController : nil;
}

- (void)dismissAnimated:(BOOL)animated {
    if (isShowingPrint_ || isAnimatingPrint_) {
        // stupid UIPrintInteractionController. We need to block calls during animation
        // else it crashes on us with a "dealloc reached while still visible" exception.
        if (!isAnimatingPrint_) {
            [[UIPrintInteractionController sharedPrintController] dismissAnimated:animated];
            isAnimatingPrint_ = animated;
        }
    }
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UIPrintInteractionControllerDelegate

- (void)printInteractionControllerDidDismissPrinterOptions:(UIPrintInteractionController *)printInteractionController {
    isShowingPrint_ = NO;
    isAnimatingPrint_ = NO;
    [self didDismiss];
}

@end
