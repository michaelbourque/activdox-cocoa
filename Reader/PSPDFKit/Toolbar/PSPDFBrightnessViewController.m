//
//  PSPDFBrightnessViewController.m
//  PSPDFKit
//
//  Copyright (c) 2012 Peter Steinberger. All rights reserved.
//

#import "PSPDFBrightnessViewController.h"
#import "PSPDFGradientView.h"

@interface PSPDFBrightnessViewController ()
@property (nonatomic, strong) UISlider *slider;
@property (nonatomic, strong) PSPDFGradientView *gradient;
@end

#define kPSPDFBrightnessControlWidth 300.f
#define kPSPDFBrightnessControlMargin 10.f

@interface PSPDFDimmingWindow : UIWindow @end @implementation PSPDFDimmingWindow @end

@implementation PSPDFBrightnessViewController

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (id)init {
    if ((self = [super initWithNibName:nil bundle:nil])) {
        self.title = PSPDFLocalize(@"Brightness");
        self.contentSizeForViewInPopover = CGSizeMake(kPSPDFBrightnessControlWidth, 44.f);
        _wantsSoftwareDimming = YES;
        _wantsAdditionalSoftwareDimming = YES;
        _additionalBrightnessDimmingFactor = 0.3f;
    }
    return self;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UIViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    PSPDFGradientView *gradient = [[PSPDFGradientView alloc] initWithFrame:self.view.bounds];
    gradient.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    gradient.colors = @[[UIColor colorWithWhite:1.f alpha:1.f], [UIColor colorWithWhite:0.9f alpha:1.f]];
    self.gradient = gradient;
    [self.view addSubview:gradient];

    UISlider *slider = [[UISlider alloc] initWithFrame:CGRectMake(kPSPDFBrightnessControlMargin, 0, kPSPDFBrightnessControlWidth-kPSPDFBrightnessControlMargin*2, 44)];
    [UIScreen mainScreen].wantsSoftwareDimming = _wantsSoftwareDimming;
    [slider addTarget:self action:@selector(sliderChanged:) forControlEvents:UIControlEventValueChanged];
    self.slider = slider;
    [self updateSliderAnimated:NO];
    [self.view addSubview:slider];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(brightnessChangedNotification:) name:UIScreenBrightnessDidChangeNotification object:nil];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];

    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIScreenBrightnessDidChangeNotification object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];

    // pre iOS6 view unloading
    if (![self isViewLoaded]) {
        self.slider = nil;
    }
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Properites

- (void)setWantsSoftwareDimming:(BOOL)wantsSoftwareDimming {
    _wantsAdditionalSoftwareDimming = wantsSoftwareDimming;
    [UIScreen mainScreen].wantsSoftwareDimming = wantsSoftwareDimming;
}

- (void)setWantsAdditionalSoftwareDimming:(BOOL)wantsAdditionalSoftwareDimming {
    _wantsAdditionalSoftwareDimming = wantsAdditionalSoftwareDimming;
    if (self.slider) [self updateBrightnessWithValue:self.slider.value];
}

- (void)setAdditionalBrightnessDimmingFactor:(CGFloat)additionalBrightnessDimmingFactor {
    _additionalBrightnessDimmingFactor = additionalBrightnessDimmingFactor;
    [self dimmingView].additionalBrightnessDimmingFactor = _additionalBrightnessDimmingFactor;
    if (self.slider) [self updateBrightnessWithValue:self.slider.value];
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Private

// brightness is from the system. we might need to calculate software brightness in
- (void)updateSliderAnimated:(BOOL)animated {
    CGFloat brightness = [UIScreen mainScreen].brightness; // 0.0 ... 1.0
    
    if (_wantsAdditionalSoftwareDimming) {
        brightness = (brightness * (1-_additionalBrightnessDimmingFactor)); // 0.0 ... 0.7

        if ([self dimmingView]) {
            CGFloat dimViewValue = [self dimmingView].alpha; // 0.0 ... 1.0
            brightness += dimViewValue * _additionalBrightnessDimmingFactor; // 0.0 ... 0.3
        }else {
            brightness += _additionalBrightnessDimmingFactor;
        }
    }
    if (self.slider) [self.slider setValue:brightness animated:animated];
}

- (void)updateBrightnessWithValue:(CGFloat)brightness {
    CGFloat brightnessValue = brightness; // 0.0 ... 1.0
    
    if (_wantsAdditionalSoftwareDimming) {
        brightnessValue = fmaxf(brightness - _additionalBrightnessDimmingFactor, 0)/(1-_additionalBrightnessDimmingFactor); // 0.0 ... 1.0 (but 0.3 new min)

        if (brightness < _additionalBrightnessDimmingFactor) {
            CGFloat additionalBrightnessValue = brightness * 1/_additionalBrightnessDimmingFactor; // 0.0 ... 0.3 => 0.0 ... 1.0
            [self addDimmingView].alpha = fminf(1-additionalBrightnessValue, 0.95f);
        }else {
            [self removeDimmingView];
        }
    }
    [UIScreen mainScreen].brightness = brightnessValue;
}

- (PSPDFDimmingView *)dimmingView {
    PSPDFDimmingView *dimmingView = nil;
    for (UIWindow *window in [UIApplication sharedApplication].windows) {
        for (UIView *view in window.subviews) {
            if ([view isKindOfClass:[PSPDFDimmingView class]]) {
                dimmingView = (PSPDFDimmingView *)view; break;
            }
        }
    }
    return dimmingView;
}

- (PSPDFDimmingView *)addDimmingView {
    PSPDFDimmingView *dimmingView = [self dimmingView];
    if (!dimmingView) {
        dimmingView = [[PSPDFDimmingView alloc] initWithFrame:[UIScreen mainScreen].bounds];
        dimmingView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        dimmingView.additionalBrightnessDimmingFactor = self.additionalBrightnessDimmingFactor;
        dimmingView.backgroundColor = [UIColor blackColor];
        dimmingView.userInteractionEnabled = NO;

        // use extra window to dim statusbar.
        // UIWindow's are a strange bread. They'll get added to the application at init time.
        UIWindow *backgroundWindow = [[PSPDFDimmingWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
#ifndef __clang_analyzer__
        CFRetain((__bridge CFTypeRef)(backgroundWindow)); // needs to be retained, else will disappear automatically
#endif
        backgroundWindow.windowLevel = UIWindowLevelStatusBar;
        backgroundWindow.hidden = NO;
        backgroundWindow.userInteractionEnabled = NO;
        [backgroundWindow addSubview:dimmingView];
    }
    return dimmingView;
}

- (void)removeDimmingView {
    [[self dimmingView] removeFromSuperview];
    if ([[self dimmingView].window isKindOfClass:[PSPDFDimmingWindow class]]) {
#ifndef __clang_analyzer__
        CFRelease((__bridge CFTypeRef)([self dimmingView].window));
#endif
    }
}

- (void)sliderChanged:(UISlider *)slider {
    [self updateBrightnessWithValue:slider.value];
}

- (void)brightnessChangedNotification:(NSNotification *)notification {
    [self updateSliderAnimated:YES];
}

@end

// Dimming view removes itself on external brigtness changes
@implementation PSPDFDimmingView

- (id)initWithFrame:(CGRect)frame {
    if ((self = [super initWithFrame:frame])) {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(dimmingBrightnessChangedNotification:) name:UIScreenBrightnessDidChangeNotification object:nil];
    }
    return self;
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIScreenBrightnessDidChangeNotification object:nil];
}

- (void)dimmingBrightnessChangedNotification:(NSNotification *)notification {
    if ([UIScreen mainScreen].brightness > self.additionalBrightnessDimmingFactor) {
        [self removeFromSuperview];
    }
}

@end

