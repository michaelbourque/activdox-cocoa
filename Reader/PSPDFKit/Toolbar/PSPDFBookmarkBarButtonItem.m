//
//  PSPDFBookmarkBarButtonItem.m
//  PSPDFKit
//
//  Copyright (c) 2012 Peter Steinberger. All rights reserved.
//

#import "PSPDFBookmarkBarButtonItem.h"
#import "PSPDFViewController.h"
#import "PSPDFViewController+Internal.h"
#import "PSPDFIconGenerator.h"
#import "PSPDFTransparentToolbar.h"
#import "PSPDFDocument.h"
#import "PSPDFBookmarkParser.h"
#import "PSPDFBookmarkViewController.h"

@implementation PSPDFBookmarkBarButtonItem {
    BOOL _imageIsCalledAndNotOverridden;
    BOOL _showBookmark;
    BOOL _isUpdatingBookmark;
}

static char kPSPDFKVOToken;

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (id)initWithPDFViewController:(PSPDFViewController *)pdfController {
    if ((self = [super initWithPDFViewController:pdfController])) {
        _showBookmarkControllerOnLongPress = YES;
        _tapChangesBookmarkStatus = YES;
        [self.pdfController addObserver:self forKeyPath:NSStringFromSelector(@selector(page)) options:0 context:&kPSPDFKVOToken];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(bookmarksChanged:) name:kPSPDFBookmarksChangedNotification object:nil];
    }
    return self;
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    if (self.pdfController) [self.pdfController removeObserver:self forKeyPath:NSStringFromSelector(@selector(page)) context:&kPSPDFKVOToken];
}

- (void)setPdfController:(PSPDFViewController *)pdfController {
    // bar buttons are not designed for controller change, but this prevents a KVO error when zombines are enabled
    if (!pdfController) [self.pdfController removeObserver:self forKeyPath:NSStringFromSelector(@selector(page)) context:&kPSPDFKVOToken];
    [super setPdfController:pdfController];
}

// update on every page change
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    if (context == &kPSPDFKVOToken) {
        if ([keyPath isEqualToString:NSStringFromSelector(@selector(page))]) {
            [self updateBarButtonItemIfStateChanged];
        }
    }else {
        [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
    }
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - PSPDFBarButtonItem

- (BOOL)isAvailable {
    return [self.pdfController.document isValid] && self.pdfController.viewMode == PSPDFViewModeDocument;
}

- (void)updateBarButtonItemIfStateChanged {
    BOOL hasBookmark = [self bookmarkNumberForVisiblePages] != nil;
    if (hasBookmark != _showBookmark || !_imageIsCalledAndNotOverridden) {
        self.image = self.image; /// KVO workaround to actually update the image.
        [self.pdfController updateBarButtonItem:self animated:YES];
    }
}

- (void)updateBarButtonItem {
    [super updateBarButtonItem];
    self.imageInsets = UIEdgeInsetsMake(3.f, 0.f, -3.f, 0.f);
}

// this works because UIBarButtonitem uses exactly this name as an accessor.
- (UIImage *)image {
    _imageIsCalledAndNotOverridden = YES;
    BOOL hasBookmark = [self bookmarkNumberForVisiblePages] != nil;
    _showBookmark = hasBookmark;
    return [[PSPDFIconGenerator sharedGenerator] iconForType:hasBookmark ? PSPDFIconTypeBookmarkActive : PSPDFIconTypeBookmark];
}

- (NSString *)actionName {
    if (self.tapChangesBookmarkStatus) {
        return PSPDFLocalize([self bookmarkNumberForVisiblePages] ? @"Remove Bookmark" : @"Add Bookmark");
    }else {
        return PSPDFLocalize(@"Bookmarks");
    }
}

- (void)action:(PSPDFBarButtonItem *)sender {
    // if tap-to-change is disabled, use regular viewController display code.
    if (!self.tapChangesBookmarkStatus) { [super action:sender]; return; }
    
    // don't handle touches while we're showing the bookmarks popover
    BOOL longPressPopoverVisible = [self.pdfController.popoverController.contentViewController isKindOfClass:[UINavigationController class]] && [[((UINavigationController *)self.pdfController.popoverController.contentViewController) topViewController] isKindOfClass:[PSPDFBookmarkViewController class]] && [self.pdfController.popoverController isPopoverVisible];
    [[self class] dismissPopoverAnimated:YES];
    if (longPressPopoverVisible) return;

    PSPDFDocument *document = self.pdfController.document;
    NSUInteger page = self.pdfController.page;

    // support double page mode
    NSNumber *pageNumber = [self bookmarkNumberForVisiblePages];
    if (pageNumber) page = [pageNumber unsignedIntegerValue];

    // toogle bookmark
    _isUpdatingBookmark = YES;
    if ([document.bookmarkParser bookmarkForPage:page]) {
        // remove both bookmarks in landscape mode
        while ([self bookmarkNumberForVisiblePages]) {
            [document.bookmarkParser removeBookmarkForPage:[[self bookmarkNumberForVisiblePages] unsignedIntegerValue]];
        }
    }else {
        [document.bookmarkParser addBookmarkForPage:page];
    }
    _isUpdatingBookmark = NO;

    // update toolbar
    [self.pdfController updateBarButtonItem:self animated:YES];
}

- (BOOL)isLongPressActionAvailable {
    return self.tapChangesBookmarkStatus && self.showBookmarkControllerOnLongPress && [self.pdfController.document isValid];
}

// only used for long press
- (id)presentAnimated:(BOOL)animated sender:(id)sender {
    PSPDFBookmarkViewController *bookmarkController = [[PSPDFBookmarkViewController alloc] initWithDocument:self.pdfController.document];
    bookmarkController.delegate = self.pdfController;
    return [self.pdfController presentViewControllerModalOrPopover:bookmarkController embeddedInNavigationController:YES withCloseButton:NO animated:animated sender:sender options:nil];
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Private

- (NSNumber *)bookmarkNumberForVisiblePages {
    PSPDFDocument *document = self.pdfController.document;
    for (NSNumber *visiblePage in [self.pdfController calculatedVisiblePageNumbers]) {
        if ([document.bookmarkParser bookmarkForPage:[visiblePage unsignedIntegerValue]]) {
            return visiblePage;
        }
    }
    return nil;
}

// observe external changes on bookmarks
- (void)bookmarksChanged:(NSNotification *)notification {
    if (_isUpdatingBookmark || ![notification.object isKindOfClass:[PSPDFBookmarkParser class]]) return;
    [self.pdfController updateBarButtonItem:self animated:YES];
}

@end
