//
//  PSPDFBrightnessBarButtonItem.m
//  PSPDFKit
//
//  Copyright (c) 2012 Peter Steinberger. All rights reserved.
//

#import "PSPDFBrightnessBarButtonItem.h"
#import "PSPDFViewController.h"
#import "PSPDFViewController+Internal.h"
#import "PSPDFBrightnessViewController.h"
#import "PSPDFIconGenerator.h"

@implementation PSPDFBrightnessBarButtonItem

- (UIImage *)image {
    return [[PSPDFIconGenerator sharedGenerator] iconForType:PSPDFIconTypeBrightness];
}

- (NSString *)actionName {
    return PSPDFLocalize(@"Brightness");
}

- (void)updateBarButtonItem {
    [super updateBarButtonItem];
    self.imageInsets = UIEdgeInsetsMake(3.f, 0.f, -3.f, 0.f);
}

- (id)presentAnimated:(BOOL)animated sender:(id)sender {
    Class viewControllerClass = [self.pdfController classForClass:[PSPDFBrightnessViewController class]];
    PSPDFBrightnessViewController *brightnessController = [[viewControllerClass alloc] init];
    return [self.pdfController presentViewControllerModalOrPopover:brightnessController embeddedInNavigationController:NO withCloseButton:NO animated:animated sender:sender options:nil];
}

@end
