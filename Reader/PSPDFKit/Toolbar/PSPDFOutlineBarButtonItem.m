//
//  PSPDFOutlineBarButtonItem.m
//  PSPDFKit
//
//  Copyright (c) 2012 Peter Steinberger. All rights reserved.
//

#import "PSPDFOutlineBarButtonItem.h"
#import "PSPDFIconGenerator.h"
#import "PSPDFOutlineViewController.h"
#import "PSPDFViewController+Internal.h"
#import "PSPDFDocument.h"
#import "PSPDFOutlineParser.h"
#import "PSPDFOutlineElement.h"

@implementation PSPDFOutlineBarButtonItem

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - PSPDFBarButtonItem

- (BOOL)isAvailableBlocking {
    BOOL isAvailable = [self.pdfController.document isValid];
    return isAvailable && self.pdfController.document.outlineParser.isOutlineAvailable;
}

- (BOOL)isAvailable {
    BOOL isAvailable = [self.pdfController.document isValid];
    PSPDFOutlineParser *outlineParser = self.pdfController.document.outlineParser;
    if (outlineParser.isOutlineParsed) {
        isAvailable = isAvailable && outlineParser.isOutlineAvailable;
    }else if (outlineParser) {
        BOOL isLocked = self.pdfController.document.isLocked;
        isAvailable = !isLocked;
        if (!isLocked) {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
                [outlineParser outline]; // parse in background
                BOOL needsToolbarReload = outlineParser.isOutlineParsed && !outlineParser.isOutlineAvailable;

                // always send the block to work against the deallocation problem
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (needsToolbarReload) [self.pdfController createToolbarAnimated:NO]; // update toolbar buttons
                });
            });
        }
    }
    return isAvailable;
}

- (UIImage *)image {
    return [[PSPDFIconGenerator sharedGenerator] iconForType:PSPDFIconTypeOutline];
}

- (NSString *)actionName {
    return PSPDFLocalize(@"Table Of Contents");
}

- (id)presentAnimated:(BOOL)animated sender:(PSPDFBarButtonItem *)sender {
    Class viewControllerClass = [self.pdfController classForClass:[PSPDFOutlineViewController class]];
    PSPDFOutlineViewController *outlineViewController = [[viewControllerClass alloc] initWithDocument:self.pdfController.document delegate:self.pdfController];
    return [self presentModalOrInPopover:outlineViewController sender:sender];
}

@end
