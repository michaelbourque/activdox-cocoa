//
//  PSPDFViewModeBarButtonItem.m
//  PSPDFKit
//
//  Copyright (c) 2012 Peter Steinberger. All rights reserved.
//

#import "PSPDFViewModeBarButtonItem.h"
#import "PSPDFSegmentedControl.h"
#import "PSPDFIconGenerator.h"
#import "PSPDFViewController+Internal.h"
#import "UIImage+PSPDFKitAdditions.h"
#import "PSPDFDocument.h"

@implementation PSPDFViewModeBarButtonItem {
    BOOL _isUpdatingSegment;
    BOOL _createdTintedImages;
}

- (BOOL)shouldCreateTintedImages {
    return ![self.pdfController isDarkHUD] && PSIsIpad();
}

- (void)setupModeSegmentView {
    _viewModeSegment = [[PSPDFSegmentedControl alloc] initWithItems:@[@"", @""]];
    _viewModeSegment.segmentedControlStyle = UISegmentedControlStyleBar;
    [_viewModeSegment addTarget:self action:@selector(viewModeSegmentChanged:) forControlEvents:UIControlEventValueChanged];

    UIImage *pageImage, *thumbImage;

    if (!PSIsIpad()) {
        pageImage = [[PSPDFIconGenerator sharedGenerator] iconForType:PSPDFIconTypePage shadowOffset:CGSizeMake(0, -1) shadowColor:[UIColor colorWithWhite:0.f alpha:0.5f]];
        thumbImage = [[PSPDFIconGenerator sharedGenerator] iconForType:PSPDFIconTypeThumbnails shadowOffset:CGSizeMake(0, -1) shadowColor:[UIColor colorWithWhite:0.f alpha:0.5f]];
    }else {
        // TODO iPad shadow!
        pageImage = [[PSPDFIconGenerator sharedGenerator] iconForType:PSPDFIconTypePage shadowOffset:CGSizeMake(0, 0) shadowColor:[UIColor colorWithWhite:0.f alpha:0.5f]];
        thumbImage = [[PSPDFIconGenerator sharedGenerator] iconForType:PSPDFIconTypeThumbnails shadowOffset:CGSizeMake(0, 0) shadowColor:[UIColor colorWithWhite:0.f alpha:0.5f]];
    }

    if ([self shouldCreateTintedImages]) {
        _createdTintedImages = YES;
        UIColor *tintColor = [UIColor colorWithRed:106.f/255.f green:113.f/255.f blue:120.f/255.f alpha:1.f];
        pageImage = [pageImage pdpdf_imageTintedWithColor:tintColor fraction:0.f];
        thumbImage = [thumbImage pdpdf_imageTintedWithColor:tintColor fraction:0.f];
    }
    [_viewModeSegment setImage:pageImage forSegmentAtIndex:0];
    [_viewModeSegment setImage:thumbImage forSegmentAtIndex:1];

    if ([self shouldCreateTintedImages]) {
        UIColor *tintColor = [UIColor colorWithWhite:0.9 alpha:1.f];
        pageImage = [pageImage pdpdf_imageTintedWithColor:tintColor fraction:0.f];
        thumbImage = [thumbImage pdpdf_imageTintedWithColor:tintColor fraction:0.f];
    }
    [(PSPDFSegmentedControl *)_viewModeSegment setSelectedImage:pageImage forSegmentAtIndex:0];
    [(PSPDFSegmentedControl *)_viewModeSegment setSelectedImage:thumbImage forSegmentAtIndex:1];

    // accessibility support
    NSUInteger buttonCount = 0;
    for (UIView *aView in _viewModeSegment.subviews) {
        switch (buttonCount) {
            case 0: aView.accessibilityValue = PSPDFLocalize(@"Page"); break;
            case 1: aView.accessibilityValue = PSPDFLocalize(@"Grid"); break;
            default:break;
        }
        buttonCount++;
    }

    self.customView = _viewModeSegment;
}

- (BOOL)isAvailable {
    return [self.pdfController.document isValid] && !self.pdfController.document.isLocked;
}

- (void)updateBarButtonItem {
    [super updateBarButtonItem];

    BOOL needsRecreation = [self shouldCreateTintedImages] != _createdTintedImages;

    // If we don't recreate the segmented contol, it only shows a graphics artefact instead.
    // only shows up with appcelerator titanium; both iPhone and iPad. Gets fixed on iPhone on rotation.
    // Might be fixed in iOS6
    needsRecreation = needsRecreation || NSClassFromString(@"TiWindowProxy") != nil;

    if (!_viewModeSegment || needsRecreation) [self setupModeSegmentView];

    if (self.pdfController.tintColor) {
        _viewModeSegment.tintColor = self.pdfController.tintColor;
    }else {
        _viewModeSegment.tintColor = [self.pdfController isTransparentHUD] ? [UIColor colorWithWhite:0.2f alpha:1.0f] : nil;
    }

    _isUpdatingSegment = YES;
    _viewModeSegment.selectedSegmentIndex = (NSInteger)self.pdfController.viewMode;
    _isUpdatingSegment = NO;
    _viewModeSegment.enabled = [self.pdfController.document isValid];
}

- (UIView *)customView {
    if (!_viewModeSegment) [self updateBarButtonItem];
    return _viewModeSegment;
}

- (void)viewModeSegmentChanged:(PSPDFSegmentedControl *)sender {
    if (_isUpdatingSegment) return; // iOS4 workaround (calls event even if changed programmatically)

    [[self class] dismissPopoverAnimated:NO];
    
    NSUInteger selectedSegment = sender.selectedSegmentIndex;
    PSPDFLog(@"selected segment index: %d", selectedSegment);
    [self.pdfController setViewMode:selectedSegment == 0 ? PSPDFViewModeDocument : PSPDFViewModeThumbnails animated:YES];
}

@end
