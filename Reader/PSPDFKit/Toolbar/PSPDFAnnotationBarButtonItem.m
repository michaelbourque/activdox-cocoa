//
//  PSPDFAnnotationBarButtonItem.m
//  PSPDFKit
//
//  Copyright (c) 2012 Peter Steinberger. All rights reserved.
//

#import "PSPDFAnnotationBarButtonItem.h"
#import "PSPDFViewController.h"
#import "PSPDFDocument.h"
#import "PSPDFDocumentProvider.h"
#import "PSPDFIconGenerator.h"
#import "PSPDFPageView.h"
#import "PSPDFAnnotationToolbar.h"
#import "PSPDFViewController+Internal.h"
#import "PSPDFTransparentToolbar.h"

@interface PSPDFAnnotationBarButtonItem () <PSPDFAnnotationToolbarDelegate>
@property (nonatomic, strong) PSPDFAnnotationToolbar *annotationToolbar;
@end

@implementation PSPDFAnnotationBarButtonItem
///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (id)initWithPDFViewController:(PSPDFViewController *)pdfController {
    if ((self = [super initWithPDFViewController:pdfController])) {
        // compensate icon center
        float topInset = 2.0f;
        self.imageInsets = UIEdgeInsetsMake(topInset, 0.0f, -topInset, 0.0f);
    }
    return self;
}

- (void)setPdfController:(PSPDFViewController *)pdfController {
    [super setPdfController:pdfController];
    _annotationToolbar.pdfController = pdfController;
}

- (void)dealloc {
    [_annotationToolbar removeFromSuperview];
    _annotationToolbar.delegate = nil;
    _annotationToolbar.pdfController = nil;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public

- (PSPDFAnnotationToolbar *)annotationToolbar {
    // lazily create toolbar
    if (!_annotationToolbar) {
        _annotationToolbar = [[[self.pdfController classForClass:[PSPDFAnnotationToolbar class]] alloc] initWithPDFController:self.pdfController];
        _annotationToolbar.delegate = self;
    }
    return _annotationToolbar;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - PSPDFBarButtonItem

- (BOOL)isAvailableBlocking { return [self isAvailableAndBlock:YES]; }
- (BOOL)isAvailable { return [self isAvailableAndBlock:NO]; }

- (BOOL)isAvailableAndBlock:(BOOL)block {
#ifdef PSPDFKIT_DISABLE_TEXT_SELECTION_FEATURES
    return NO;
#else
    PSPDFAnnotationSaveMode annotationSaveMode = self.pdfController.document.annotationSaveMode;
    BOOL isDocumentParserLoaded = [[[self.pdfController.document documentProviders] ps_firstObject] isDocumentParserLoaded];
    __block BOOL canEmbedAnnotations = YES;
    if (isDocumentParserLoaded) {
        canEmbedAnnotations = self.pdfController.document.canEmbedAnnotations && (annotationSaveMode == PSPDFAnnotationSaveModeEmbeddedWithExternalFileAsFallback || annotationSaveMode == PSPDFAnnotationSaveModeEmbedded);

        // we only need to evaluate this if embedded is the only save mode choice.
    }else if (annotationSaveMode == PSPDFAnnotationSaveModeEmbedded) {
        // only evaluate if needed
        if ([self.pdfController.document.editableAnnotationTypes count]) {
            dispatch_block_t workerBlock = ^{
                // We assume that embedding annotations will work. If this is not the case; reload the toolbar.
                canEmbedAnnotations = [self.pdfController.document canEmbedAnnotations]; // just access to evaluate and parse the PDF.

                // always send the block to work against the deallocation problem
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (!canEmbedAnnotations) {
                        [self.pdfController updateBarButtonItem:self animated:YES];
                    }
                });
            };
            block ? workerBlock() : dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), workerBlock);
        }
    }
    BOOL canSaveToFile = annotationSaveMode == PSPDFAnnotationSaveModeExternalFile || annotationSaveMode == PSPDFAnnotationSaveModeEmbeddedWithExternalFileAsFallback;

    return self.pdfController.viewMode == PSPDFViewModeDocument && [self.pdfController.document.editableAnnotationTypes count] && (canEmbedAnnotations || canSaveToFile);
#endif
}

- (UIImage *)image {
    return [[PSPDFIconGenerator sharedGenerator] iconForType:PSPDFIconTypeAnnotations];
}

- (NSString *)actionName {
    return PSPDFLocalize(@"Annotations");
}

- (UIToolbar *)targetToolbarForBarButtonItem:(UIBarButtonItem *)barButtonItem {
    UIToolbar *toolbar = nil;
    if ([barButtonItem respondsToSelector:@selector(nextResponder)] && [[(UIResponder *)barButtonItem nextResponder] isKindOfClass:[UIToolbar class]]) {
        toolbar = (UIToolbar *)[(UIResponder *)barButtonItem nextResponder];
    }
    return toolbar;
}

- (void)action:(PSPDFBarButtonItem *)sender {
#ifndef PSPDFKIT_DISABLE_TEXT_SELECTION_FEATURES
    PSPDFViewController *pdfController = self.pdfController;

    // update tintColor etc
    self.annotationToolbar.pdfController = pdfController;

    // ensure popovers are gone
    [[self class] dismissPopoverAnimated:YES];
    [pdfController.popoverController dismissPopoverAnimated:YES];
    pdfController.popoverController = nil;

    UIToolbar *targetToolbar = [self targetToolbarForBarButtonItem:sender];
    CGRect targetRect;
    if ([targetToolbar isKindOfClass:[PSPDFTransparentToolbar class]] && pdfController.navigationController && !pdfController.navigationController.navigationBar.hidden) {
        if ([pdfController isTransparentHUD]) {
            targetRect = pdfController.navigationController.navigationBar.frame;
            [pdfController.navigationController.view addSubview:_annotationToolbar];
        }else {
            // If we're not transparent, content starts to jump when we rotate.
            // Thus we're adding the toolbar to the navBar instead to the parent view then.
            targetRect = pdfController.navigationController.navigationBar.bounds;
            [pdfController.navigationController.navigationBar addSubview:_annotationToolbar];
        }
        [_annotationToolbar showToolbarInRect:targetRect animated:YES];
    }else {
        if (targetToolbar) {
            targetRect = targetToolbar.bounds;
            [targetToolbar addSubview:_annotationToolbar];
            // match style
            _annotationToolbar.barStyle = targetToolbar.barStyle;
            _annotationToolbar.translucent = targetToolbar.translucent;
            _annotationToolbar.tintColor = targetToolbar.tintColor;
        }else {
            // try to hide controls (so that nothing overlaps our toolbar)
            [self.pdfController hideControlsAndPageElements];

            targetRect = CGRectMake(0, 0, pdfController.view.bounds.size.width, PSPDFToolbarHeightForOrientation(pdfController.interfaceOrientation));
            [self.pdfController.view addSubview:_annotationToolbar];
        }

        // height is auto-set.
        [_annotationToolbar showToolbarInRect:targetRect animated:YES];
    }
#endif
}

@end
