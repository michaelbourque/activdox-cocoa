//
//  PSPDFEmailBarButtonItem.m
//  PSPDFKit
//
//  Copyright (c) 2012 Peter Steinberger. All rights reserved.
//

#import "PSPDFEmailBarButtonItem.h"
#import "PSPDFIconGenerator.h"
#import "PSPDFViewController.h"
#import "PSPDFDocument.h"
#import "PSPDFProcessor.h"
#import "PSPDFActionSheet.h"
#import "PSPDFViewController+Internal.h"

@interface PSPDFEmailBarButtonItem() {
    BOOL _isDismissingSheet;
}
@property (nonatomic, strong) UIActionSheet *actionSheet;
@end

@implementation PSPDFEmailBarButtonItem

- (id)initWithPDFViewController:(PSPDFViewController *)pdfViewController {
    if ((self = [super initWithPDFViewController:pdfViewController])) {
        _sendOptions = PSPDFEmailSendVisiblePagesFlattened | PSPDFEmailSendMergedFilesIfNeeded;
    }
    return self;
}

- (BOOL)isAvailable {
    static BOOL pspdf_canSendMailCache;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        // this is much slower than it should be.
        // cache result, it won't change often, and we check again on action.
        pspdf_canSendMailCache = [MFMailComposeViewController canSendMail];
    });
    return pspdf_canSendMailCache;
}

- (UIImage *)image {
    return [[PSPDFIconGenerator sharedGenerator] iconForType:PSPDFIconTypeEmail];
}

- (void)updateBarButtonItem {
    self.imageInsets = UIEdgeInsetsMake(2.f, 0.f, -2.f, 0.f);
}

- (NSString *)actionName {
    return PSPDFLocalize(@"Email");
}

- (id)presentAnimated:(BOOL)animated sender:(id)sender {
    if (![MFMailComposeViewController canSendMail]) return nil;
    if (!self.pdfController) { PSPDFLogError(@"PDF controller is not set!"); return nil;}

    NSUInteger numberOfVisiblePages = fmaxf([self.pdfController.visiblePageNumbers count], 1);
    NSUInteger numberOfDocumentProviders = [self.pdfController.document.documentProviders count];
    PSPDFEmailSendOptions sendOptions = self.sendOptions;
    BOOL sendCurrentPageOnly = sendOptions & PSPDFEmailSendCurrentPageOnly;
    BOOL sendCurrentPageOnlyFlattened = sendOptions & PSPDFEmailSendCurrentPageOnlyFlattened;
    BOOL sendVisiblePages = sendOptions & PSPDFEmailSendVisiblePages;
    BOOL sendVisiblePagesFlattened = sendOptions & PSPDFEmailSendVisiblePagesFlattened;
    BOOL sendMergedFiles = sendOptions & PSPDFEmailSendMergedFilesIfNeeded;
    BOOL sendMergedFilesFlattened = sendOptions & PSPDFEmailSendMergedFilesIfNeededFlattened;
    BOOL sendOriginalFiles = sendOptions & PSPDFEmailSendOriginalDocumentFiles;

    // sanitize options (filter choices that are not logical)
    if (sendCurrentPageOnly && sendVisiblePages && numberOfVisiblePages == 1) {
        sendVisiblePages = NO;
        sendOptions &= ~PSPDFEmailSendVisiblePages;
    }
    if (sendCurrentPageOnlyFlattened && sendVisiblePagesFlattened && numberOfVisiblePages == 1) {
        sendVisiblePagesFlattened = NO;
        sendOptions &= ~PSPDFEmailSendVisiblePagesFlattened;
    }
    if (sendMergedFiles && sendOriginalFiles && [self.pdfController.document.documentProviders count] == 1) {
        sendMergedFiles = NO;
        sendOptions &= ~PSPDFEmailSendMergedFilesIfNeeded;
    }

    NSUInteger numberOfOptions = (sendCurrentPageOnly?1:0) + (sendCurrentPageOnlyFlattened?1:0) + (sendVisiblePages?1:0) + (sendVisiblePagesFlattened?1:0) + (sendMergedFiles?1:0) + (sendMergedFilesFlattened?1:0) + (sendOriginalFiles?1:0);

    if (numberOfOptions > 1) {
        PSPDFActionSheet *actionSheet = [[PSPDFActionSheet alloc] initWithTitle:nil];
        _isDismissingSheet = NO;

        if (sendCurrentPageOnly) {
            [actionSheet addButtonWithTitle:PSPDFLocalize(@"Email visible page") block:^{
                [self showEmailControllerWithSendOptions:PSPDFEmailSendCurrentPageOnly animated:animated];
            }];
        }
        if (sendVisiblePages) {
            [actionSheet addButtonWithTitle:PSPDFLocalize(numberOfVisiblePages > 1 ? @"Email visible pages" : @"Email visible page") block:^{
                [self showEmailControllerWithSendOptions:PSPDFEmailSendVisiblePages animated:animated];
            }];
        }
        if (sendCurrentPageOnlyFlattened) {
            [actionSheet addButtonWithTitle:PSPDFLocalize(@"Email flattened page") block:^{
                [self showEmailControllerWithSendOptions:PSPDFEmailSendCurrentPageOnlyFlattened animated:animated];
            }];
        }
        if (sendVisiblePagesFlattened) {
            [actionSheet addButtonWithTitle:PSPDFLocalize(numberOfVisiblePages > 1 ? @"Email flattened pages" : @"Email flattened page") block:^{
                [self showEmailControllerWithSendOptions:PSPDFEmailSendVisiblePagesFlattened animated:animated];
            }];
        }
        if (sendOriginalFiles) {
            [actionSheet addButtonWithTitle:PSPDFLocalize(numberOfDocumentProviders > 1 ? @"Email document files" : @"Email document") block:^{
                [self showEmailControllerWithSendOptions:PSPDFEmailSendOriginalDocumentFiles animated:animated];
            }];
        }
        if (sendMergedFiles) {
            [actionSheet addButtonWithTitle:PSPDFLocalize(@"Email document") block:^{
                [self showEmailControllerWithSendOptions:PSPDFEmailSendMergedFilesIfNeeded animated:animated];
            }];
        }
        if (sendMergedFilesFlattened) {
            [actionSheet addButtonWithTitle:PSPDFLocalize(@"Email flattened document") block:^{
                [self showEmailControllerWithSendOptions:PSPDFEmailSendMergedFilesIfNeededFlattened animated:animated];
            }];
        }

        actionSheet.destroyBlock = ^{
            // we don't get any delegates for the slide down animation on iPhone.
            // if we'd call dismissWithClickedButtonIndex again, the animation would be nil.
            _isDismissingSheet = YES;
            [[self class] dismissPopoverAnimated:YES];
        };
        [actionSheet setCancelButtonWithTitle:PSPDFLocalize(@"Cancel") block:NULL];
        BOOL shouldShow = [self.pdfController delegateShouldShowController:actionSheet embeddedInController:nil animated:animated];
        if (shouldShow) {
            if (PSIsIpad() && [sender isKindOfClass:[UIBarButtonItem class]]) {
                [actionSheet showFromBarButtonItem:sender animated:animated];
            }else if (PSIsIpad() && [sender isKindOfClass:[UIView class]]) {
                [actionSheet showFromRect:[sender bounds] inView:sender animated:animated];
            }else {
                [actionSheet showInView:[self.pdfController masterViewController].view];
            }
            self.actionSheet = actionSheet;
            return self.actionSheet;
        }else {
            return nil;
        }
    }

    return [self showEmailControllerWithSendOptions:sendOptions animated:animated];
}

- (void)dismissAnimated:(BOOL)animated {
    if (_isDismissingSheet) return;
    [self.actionSheet dismissWithClickedButtonIndex:self.actionSheet.cancelButtonIndex animated:animated];
}

- (void)didDismiss {
    [super didDismiss];
    self.actionSheet = nil;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Private

- (id)showEmailControllerWithSendOptions:(PSPDFEmailSendOptions)sendOptions animated:(BOOL)animated {
    PSPDFDocument *document = self.pdfController.document;
    MFMailComposeViewController *mailViewController = [MFMailComposeViewController new];
    [mailViewController setSubject:document.title];
    mailViewController.mailComposeDelegate = self;
    mailViewController.modalPresentationStyle = UIModalPresentationFormSheet;
    BOOL success = [self attachDocumentToMailController:mailViewController withMode:sendOptions];
    if (!success) {
        PSPDFLogError(@"Failed to attach any files.");
    }else {
        BOOL shouldShow = [self.pdfController delegateShouldShowController:mailViewController embeddedInController:mailViewController animated:YES];
        if (shouldShow) {
            [self.pdfController presentModalViewController:mailViewController embeddedInNavigationController:NO withCloseButton:NO animated:animated];
            return mailViewController;
        }
    }
    return nil;
}

- (BOOL)attachDocumentToMailController:(MFMailComposeViewController *)mailViewController withMode:(PSPDFEmailSendOptions)mode {
    PSPDFDocument *document = self.pdfController.document;

    // TODO: support annotation flattening
    if (mode == PSPDFEmailSendOriginalDocumentFiles) {
        NSDictionary *fileNamesWithDataDict = [document fileNamesWithDataDictionary];
        if ([fileNamesWithDataDict count] == 0) { PSPDFLogWarning(@"Could not attach any files"); return NO; }
        
        [fileNamesWithDataDict enumerateKeysAndObjectsUsingBlock:^(NSString *fileName, NSData *data, BOOL *stop) {
            [mailViewController addAttachmentData:data mimeType:@"application/pdf" fileName:fileName];
        }];
    }

    // merge and create new document
    if (mode == PSPDFEmailSendVisiblePages || mode == PSPDFEmailSendVisiblePagesFlattened || mode == PSPDFEmailSendCurrentPageOnly || mode == PSPDFEmailSendCurrentPageOnlyFlattened || mode == PSPDFEmailSendMergedFilesIfNeeded || mode == PSPDFEmailSendMergedFilesIfNeededFlattened) {
        NSIndexSet *visiblePages;
        if (mode == PSPDFEmailSendMergedFilesIfNeeded || mode == PSPDFEmailSendMergedFilesIfNeededFlattened) {
            visiblePages = [NSIndexSet indexSetWithIndexesInRange:NSMakeRange(0, [document pageCount])];
        }else if (mode == PSPDFEmailSendVisiblePages || mode == PSPDFEmailSendVisiblePagesFlattened) {
            visiblePages = PSPDFIndexSetFromArray(self.pdfController.visiblePageNumbers);
        }else {
            visiblePages = [NSIndexSet indexSetWithIndex:self.pdfController.page];
        }

        BOOL flatten = mode == PSPDFEmailSendVisiblePagesFlattened || mode == PSPDFEmailSendCurrentPageOnlyFlattened || mode == PSPDFEmailSendMergedFilesIfNeededFlattened;

        // merge all annotations except link (useless info)
        NSDictionary *options = flatten ? @{kPSPDFProcessorAnnotationTypes : @(PSPDFAnnotationTypeAll & ~PSPDFAnnotationTypeLink)} : nil;

        NSString *fileName = [self fileNameForPage:[visiblePages firstIndex] sendOptions:mode];
        NSData *data = [[PSPDFProcessor defaultProcessor] generatePDFFromDocument:document pageRange:visiblePages options:options];
        if (data) [mailViewController addAttachmentData:data mimeType:@"application/pdf" fileName:fileName];
        return data != nil;
    }
    
    return NO;
}

- (NSString *)fileNameForPage:(NSUInteger)pageIndex sendOptions:(PSPDFEmailSendOptions)sendOptions {
    return [self.pdfController.document fileNameForPage:pageIndex];
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - MFMailComposeViewControllerDelegate

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error {
    [[self.pdfController masterViewController] dismissViewControllerAnimated:YES completion:NULL];
}

@end

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Deprecated

const NSUInteger PSPDFEmailSendAskUser = PSPDFEmailSendVisiblePagesFlattened | PSPDFEmailSendMergedFilesIfNeeded;
