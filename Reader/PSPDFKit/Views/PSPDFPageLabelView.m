//
//  PSPDFPageLabelView.m
//  PSPDFKit
//
//  Copyright (c) 2011-2012 Peter Steinberger. All rights reserved.
//

#import "PSPDFPageLabelView.h"
#import "PSPDFViewController.h"
#import "PSPDFViewController+Internal.h"
#import "PSPDFIconGenerator.h"
#import "PSPDFDocument.h"

#define kThumbnailButtonTouchExpandArea 20.f
#define kThumbnailGridButtonWidth 25.f

@interface PSPDFPageLabelView ()
@property (nonatomic, strong) UIButton *thumbnailButton;
@end

@implementation PSPDFPageLabelView

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (id)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin;
        self.showThumbnailGridButton = NO;
        self.userInteractionEnabled = YES;

        // orientation change might change the calcultedPageNumbers (2 becomes 2-3) but won't fire a KVO notification since the actual page doesn't change.
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(orientationChanged:) name:PSPDFViewControllerWillAnimateChangeOrientationNotification object:nil];
    }
    return self;
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UIView

- (CGRect)expandedButtonFrame {
    return CGRectInset(self.thumbnailButton.frame, -kThumbnailButtonTouchExpandArea, -kThumbnailButtonTouchExpandArea);
}

// Only allow touches to the thumbnail button (and expand touch area)
- (BOOL)pointInside:(CGPoint)point withEvent:(UIEvent *)event {
    return CGRectContainsPoint([self expandedButtonFrame], point);
}

- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event {
    return CGRectContainsPoint([self expandedButtonFrame], point) ? self.thumbnailButton : nil;
}

- (void)layoutSubviews {
    [super layoutSubviews];

    UILabel *label = self.label;
    CGFloat labelMargin = self.labelMargin;
    [label sizeToFit];
    label.frame = CGRectMake(labelMargin*4, labelMargin, fminf(label.frame.size.width, self.superview.bounds.size.width - labelMargin*18), label.frame.size.height);
    CGRect bounds = CGRectMake(0, 0, label.frame.size.width+labelMargin*8 + (self.showThumbnailGridButton ? (kThumbnailGridButtonWidth) : 0.f), label.frame.size.height+labelMargin*2);
    self.bounds = bounds;
    self.frame = CGRectIntegral(self.frame); // don't subpixel align centered item!

    self.thumbnailButton.frame = CGRectMake(label.frame.size.width+labelMargin*6, 0, kThumbnailGridButtonWidth, bounds.size.height);
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - PSPDFLabelView

- (NSArray *)kvoValues {
    return @[NSStringFromSelector(@selector(document)), NSStringFromSelector(@selector(page)), NSStringFromSelector(@selector(viewMode)), @"viewModeAnimated"];
}

- (void)updateAnimated:(BOOL)animated {
    PSPDFViewController *pdfController = self.pdfController;

    BOOL showLabel = pdfController.viewMode == PSPDFViewModeDocument && [pdfController.document isValid];
    CGFloat targetAlpha = showLabel ? 1.f : 0.f;

    if (self.alpha != targetAlpha || [self.label.text length] == 0) {
        [UIView animateWithDuration:animated ? 0.25f : 0.f delay:0.f options:UIViewAnimationOptionAllowUserInteraction | UIViewAnimationOptionBeginFromCurrentState animations:^{
            self.alpha = targetAlpha;
        } completion:nil];
    }

    NSString *text = @"";
    if (pdfController.document.isValid) {
        NSString *pageLabel = [pdfController.document pageLabelForPage:pdfController.page substituteWithPlainLabel:NO];
        if (!pageLabel) {
            NSArray *calculatedPages = [self.pdfController calculatedVisiblePageNumbers];
            if ([calculatedPages count] == 2) {
                text = [NSString stringWithFormat:PSPDFLocalize(@"%d-%d of %d"), [calculatedPages[0] unsignedIntegerValue]+1, [calculatedPages[1] unsignedIntegerValue]+1, pdfController.document.pageCount];
            }else  {
                text = [NSString stringWithFormat:PSPDFLocalize(@"%d of %d"), pdfController.page+1, pdfController.document.pageCount];
            }
        }else {
            text = [NSString stringWithFormat:PSPDFLocalize(@"%@ (%d of %d)"), pageLabel, pdfController.page+1, pdfController.document.pageCount];
        }
    }

    // optimization: only update and recalculate frame if text changed
    if (![self.label.text isEqualToString:text]) {
        self.label.text = text;
        [self setNeedsLayout]; // recalculate outer frame
    }
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public

- (void)setShowThumbnailGridButton:(BOOL)showThumbnailGridButton {
    _showThumbnailGridButton = showThumbnailGridButton;

    if (showThumbnailGridButton) {
        if (!self.thumbnailButton) {
            UIButton *thumbnailButton = [UIButton buttonWithType:UIButtonTypeCustom];
            UIImage *image = [[PSPDFIconGenerator sharedGenerator] iconForType:PSPDFIconTypeThumbnails shadowOffset:CGSizeMake(0, 1) shadowColor:[UIColor blackColor]];
            [thumbnailButton setImage:image forState:UIControlStateNormal];
            [thumbnailButton addTarget:self action:@selector(thumbnailButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
            [thumbnailButton sizeToFit];
            thumbnailButton.contentMode = UIViewContentModeCenter;
            thumbnailButton.showsTouchWhenHighlighted = YES;
            thumbnailButton.contentEdgeInsets = UIEdgeInsetsMake(1.f, 0.f, -1.f, 0.f); // image is offset by a pixel
            self.thumbnailButton = thumbnailButton;
        }
        [self addSubview:self.thumbnailButton];
    }else {
        [self.thumbnailButton removeFromSuperview];
    }
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Private

- (void)thumbnailButtonTapped:(UIButton *)button {
    [self.pdfController setViewMode:PSPDFViewModeThumbnails animated:YES];
}

- (void)orientationChanged:(NSNotification *)notification {
    [self updateAnimated:YES];
}

@end
