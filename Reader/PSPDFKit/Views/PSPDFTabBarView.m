//
//  PSPDFTabBarView.m
//  PSPDFKit
//
//  Copyright (c) 2012 Peter Steinberger. All rights reserved.
//

#import "PSPDFTabBarView.h"
#import "PSPDFTabBarButton.h"

@interface PSPDFTabBarView ()
@property (nonatomic, strong) UIScrollView *scrollView;
@end

@implementation PSPDFTabBarView

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (id)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        _scrollView = [[UIScrollView alloc] initWithFrame:self.bounds];
        _scrollView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        _scrollView.scrollsToTop = NO;
        _scrollView.delegate = self;
        _scrollView.showsHorizontalScrollIndicator = NO;
        _scrollView.showsVerticalScrollIndicator = NO;
        // delaying content touches would break scrolling
        [self addSubview:_scrollView];
    }
    return self;
}

- (void)dealloc {
    _scrollView.delegate = nil;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UIView

// don't handle touches that are ouside of the tabbar content area
- (BOOL)pointInside:(CGPoint)point withEvent:(UIEvent *)event {
    CGRect contentRect = self.bounds;
    contentRect.size.width = fminf(_scrollView.contentSize.width + _scrollView.contentInset.left + _scrollView.contentInset.right, self.superview.frame.size.width);
    BOOL inside = CGRectContainsPoint(contentRect, point);
    return inside;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Private

- (void)iterateTabButtonsWithBlock:(void(^)(PSPDFTabBarButton *tabButton, BOOL *stop))block {
    BOOL stop = NO;
    for(UIView *subview in _scrollView.subviews) {
        if ([subview isKindOfClass:[PSPDFTabBarButton class]]) {
            block((PSPDFTabBarButton *)subview, &stop);
        }
        if (stop) break;
    }
}

- (void)buttonPressed:(PSPDFTabBarButton *)sender {
    [self selectTabAtIndex:sender.tag animated:NO callDelegate:YES];
}

- (void)closeButtonPressed:(PSPDFTabBarCloseButton *)sender {
    [_delegate tabBarView:self didSelectCloseButtonOfTabAtIndex:sender.superview.tag];
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public

- (void)selectTabAtIndex:(NSUInteger)index animated:(BOOL)animated callDelegate:(BOOL)callDelegate {
    [self iterateTabButtonsWithBlock:^(PSPDFTabBarButton *tabButton, BOOL *stop) {
        BOOL selected = index == tabButton.tag;
        [tabButton setSelected:selected animated:animated];
    }];

    if (callDelegate) {
        [_delegate tabBarView:self didSelectTabAtIndex:index];
    }
}

- (void)selectTabAtIndex:(NSUInteger)index animated:(BOOL)animated  {
    [self selectTabAtIndex:index animated:animated callDelegate:NO];
}

- (NSUInteger)selectedTabIndex {
    __block NSUInteger selectedTabIndex = NSNotFound;
    [self iterateTabButtonsWithBlock:^(PSPDFTabBarButton *tabButton, BOOL *stop) {
        if (tabButton.isSelected) {
            selectedTabIndex = tabButton.tag;
            *stop = YES;
        }
    }];
    return selectedTabIndex;
}

- (void)scrollToTabAtIndex:(NSUInteger)index animated:(BOOL)animated {
    [self iterateTabButtonsWithBlock:^(PSPDFTabBarButton *tabButton, BOOL *stop) {
        if (tabButton.tag == index) {
            [_scrollView scrollRectToVisible:tabButton.frame animated:animated];
            *stop = YES;
        }
    }];
}

- (void)reloadData {
    [_scrollView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    CGFloat contentOffset = 0.f;
    NSUInteger itemCount = [_dataSource numberOfTabsInTabBarView:self];
    for (NSUInteger index = 0; index < itemCount; index++) {
        PSPDFTabBarButton *button = [[PSPDFTabBarButton alloc] initWithFrame:CGRectZero];
        button.tag = index;
        [button addTarget:self action:@selector(buttonPressed:) forControlEvents:UIControlEventTouchUpInside];
        [button.closeButton addTarget:self action:@selector(closeButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
        NSString *title = [_dataSource tabBarView:self titleForTabAtIndex:index];
        [button setTitle:title forState:UIControlStateNormal];
        [button sizeToFit];
        CGRect frame = button.frame; frame.origin.x = contentOffset; button.frame = frame;
        contentOffset += button.frame.size.width;
        [_scrollView addSubview:button];
    }
    _scrollView.contentSize = CGSizeMake(contentOffset, self.frame.size.height);
}

- (void)setMinTabWidth:(CGFloat)minTabWidth {
    if (minTabWidth != _minTabWidth) {
        _minTabWidth = minTabWidth;
        [self reloadData];
    }
}

@end
