//
//  PSPDFSelectionBorderView.m
//  PSPDFKit
//
//  Copyright (c) 2012 Peter Steinberger. All rights reserved.
//

#import "PSPDFResizableView.h"
#import "UIColor+PSPDFKitAdditions.h"
#import "UIImage+PSPDFKitAdditions.h"
#import <QuartzCore/QuartzCore.h>

@interface PSPDFResizableView() {
    UIImageView *_knobTopLeft, *_knobTopMiddle, *_knobTopRight;
    UIImageView *_knobMiddleLeft, *_knobMiddleRight;
    UIImageView *_knobBottomLeft, *_knobBottomMiddle, *_knobBottomRight;
    NSArray *_knobs;
    CGPoint _firstTouchPoint;
    CGFloat _savedMinWidth, _savedMinHeight;
}

@property (nonatomic, strong) UIColor *selectionBorderColor;
@property (nonatomic, strong) UIImage *knobImage;
@end

@implementation PSPDFResizableView

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (id)initWithFrame:(CGRect)frame {
    if ((self = [super initWithFrame:frame])) {
        self.backgroundColor = [UIColor clearColor];
        self.clipsToBounds = NO;
        self.userInteractionEnabled = YES;
        _zoomScale = 1.f;

        self.selectionBorderColor = [[UIColor pspdf_selectionColor] colorWithAlphaComponent:0.6f];
        self.knobImage = [UIImage imageNamed:@"PSPDFKit.bundle/SelectionHandle"];

        // defaults
        _minHeight = _savedMinHeight = 44.f;
        _minWidth = _savedMinWidth = 44.f;
        _preventsPositionOutsideSuperview = YES;
        _allowEditing = YES;

        // images
        _knobTopLeft      = [[UIImageView alloc] initWithImage:self.knobImage];
        _knobTopMiddle    = [[UIImageView alloc] initWithImage:self.knobImage];
        _knobTopRight     = [[UIImageView alloc] initWithImage:self.knobImage];
        _knobMiddleLeft   = [[UIImageView alloc] initWithImage:self.knobImage];
        _knobMiddleRight  = [[UIImageView alloc] initWithImage:self.knobImage];
        _knobBottomLeft   = [[UIImageView alloc] initWithImage:self.knobImage];
        _knobBottomMiddle = [[UIImageView alloc] initWithImage:self.knobImage];
        _knobBottomRight  = [[UIImageView alloc] initWithImage:self.knobImage];

        _knobs = @[_knobTopLeft, _knobTopMiddle, _knobTopRight, _knobMiddleLeft, _knobMiddleRight, _knobBottomLeft, _knobBottomMiddle, _knobBottomRight];

        for (UIImageView *knob in _knobs) {
            [self addSubview:knob];
        }
    }
    return self;
}

- (id)initWithTrackedView:(UIView *)trackedView {
    if ((self = [super initWithFrame:trackedView.frame])) {
        _trackedView = trackedView;
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];

    float handleSize = 32.0 * (1.0/self.zoomScale);
    CGRect knobFrame = (CGRect){.size=CGSizeMake(handleSize, handleSize)};

    for (UIImageView *knob in _knobs) {
        knob.frame = knobFrame;
    }

    _knobTopLeft.center      = CGPointMake(0, 0);
    _knobTopMiddle.center    = CGPointMake(self.bounds.size.width/2, 0);
    _knobTopRight.center     = CGPointMake(self.bounds.size.width, 0);
    _knobMiddleLeft.center   = CGPointMake(0, self.bounds.size.height/2);
    _knobMiddleRight.center  = CGPointMake(self.bounds.size.width, self.bounds.size.height/2);
    _knobBottomLeft.center   = CGPointMake(0, self.bounds.size.height);
    _knobBottomMiddle.center = CGPointMake(self.bounds.size.width/2, self.bounds.size.height);
    _knobBottomRight.center  = CGPointMake(self.bounds.size.width, self.bounds.size.height);
}

- (void)setFrame:(CGRect)frame {
    CGRect oldFrame = self.frame;
    [super setFrame:frame];
    [self setNeedsLayout];
    
    // we only need to redraw if the size changes.
    if (!CGSizeEqualToSize(oldFrame.size, frame.size)) {
        [self setNeedsDisplay];
    }
}

- (void)updateMinHeightWidthForStartFrame {
    // min range can't be larger than the initial frame.
    _minWidth = fminf(self.trackedView.frame.size.width, self.minWidth);
    _minHeight = fminf(self.trackedView.frame.size.height, self.minHeight);
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UIView

- (void)drawRect:(CGRect)rect {
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetLineWidth(context, 2.f);
    CGContextSetStrokeColorWithColor(context, self.selectionBorderColor.CGColor);
    CGContextStrokeRect(context, self.bounds);
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public

- (void)setTrackedView:(UIView *)trackedView {
    if (trackedView != _trackedView) {
        _trackedView = trackedView;

        // restore saved min values
        _minWidth = _savedMinWidth;
        _minHeight = _savedMinHeight;

        // set limits so that they're not smaller than the current frame
        [self updateMinHeightWidthForStartFrame];
    }
}

- (void)setMinWidth:(CGFloat)minWidth {
    _minWidth = minWidth;
    _savedMinWidth = minWidth;
}

- (void)setMinHeight:(CGFloat)minHeight {
    _minHeight = minHeight;
    _savedMinHeight = minHeight;
}

- (void)setZoomScale:(CGFloat)zoomScale {
    _zoomScale = zoomScale;
    [self setNeedsLayout];
    [self setNeedsDisplay];
}

- (BOOL)longPress:(UILongPressGestureRecognizer *)recognizer {
    if (!self.allowEditing) return NO;
    BOOL handle = self.activeKnobType != PSPDFSelectionBorderKnobTypeNone;

    switch (recognizer.state) {
        case UIGestureRecognizerStateBegan:
            _firstTouchPoint = [recognizer locationInView:self];

            if ([self.delegate respondsToSelector:@selector(resizableViewDidBeginEditing:)]) {
                [self.delegate resizableViewDidBeginEditing:self];
            }
        break;

        case UIGestureRecognizerStateChanged: {
            CGPoint loc = [recognizer locationInView:self];
            //NSLog(@"%@", NSStringFromCGPoint(loc));

            CGRect newFrame = self.frame, f = self.frame;
            switch (self.activeKnobType) {
                case PSPDFSelectionBorderKnobTypeMove:
                    newFrame = CGRectMake(f.origin.x+loc.x-_firstTouchPoint.x, f.origin.y+loc.y-_firstTouchPoint.y, f.size.width, f.size.height); break;
                case PSPDFSelectionBorderKnobTypeTopLeft:
                    newFrame = CGRectMake(f.origin.x+loc.x, f.origin.y+loc.y, f.size.width-loc.x, f.size.height-loc.y); break;
                case PSPDFSelectionBorderKnobTypeTopMiddle:
                    newFrame = CGRectMake(f.origin.x, f.origin.y+loc.y, f.size.width, f.size.height-loc.y);  break;
                case PSPDFSelectionBorderKnobTypeTopRight:
                    newFrame = CGRectMake(f.origin.x, f.origin.y+loc.y, loc.x, f.size.height-loc.y); break;
                case PSPDFSelectionBorderKnobTypeMiddleLeft:
                    newFrame = CGRectMake(f.origin.x+loc.x, f.origin.y, f.size.width-loc.x, f.size.height); break;
                case PSPDFSelectionBorderKnobTypeMiddleRight:
                    newFrame = CGRectMake(f.origin.x, f.origin.y, loc.x, f.size.height); break;
                case PSPDFSelectionBorderKnobTypeBottomLeft:
                    newFrame = CGRectMake(f.origin.x+loc.x, f.origin.y, f.size.width-loc.x, loc.y); break;
                case PSPDFSelectionBorderKnobTypeBottomMiddle:
                    newFrame = CGRectMake(f.origin.x, f.origin.y, f.size.width, loc.y); break;
                case PSPDFSelectionBorderKnobTypeBottomRight:
                    newFrame = CGRectMake(f.origin.x, f.origin.y, loc.x, loc.y); break;
                default: break;
            }

            // ensure there's a minimum size
            if (newFrame.size.width < self.minWidth) {
                newFrame.size.width = self.minWidth;
                newFrame.origin.x = f.origin.x;
            }
            if (newFrame.size.height < self.minHeight) {
                newFrame.size.height = self.minHeight;
                newFrame.origin.y = f.origin.y;
            }

            if (self.preventsPositionOutsideSuperview) {
                //NSLog(@"newFrame: %@", NSStringFromCGRect(newFrame));
                CGRect parentBounds = self.superview.bounds;
                if (self.activeKnobType == PSPDFSelectionBorderKnobTypeMove) {
                    // limit move to the edges (top, left)
                    newFrame = CGRectApplyAffineTransform(newFrame, CGAffineTransformMakeTranslation(fmaxf(0,-newFrame.origin.x), fmaxf(0,-newFrame.origin.y)));
                    // limit move to the edges (bottom, right)
                    newFrame = CGRectApplyAffineTransform(newFrame, CGAffineTransformMakeTranslation(fminf(0, CGRectGetMaxX(parentBounds)-CGRectGetMaxX(newFrame)), fminf(0, CGRectGetMaxY(parentBounds)-CGRectGetMaxY(newFrame))));
                }else {
                    // limit resizing (only allow to page bounds)
                    if (CGRectGetMinX(newFrame) < 0) {newFrame.size.width+=newFrame.origin.x; newFrame.origin.x = 0; }
                    if (CGRectGetMinY(newFrame) < 0) { newFrame.size.height+=newFrame.origin.y; newFrame.origin.y = 0; }
                    if (CGRectGetMaxX(newFrame) > CGRectGetMaxX(parentBounds)) {newFrame.size.width = CGRectGetMaxX(parentBounds)-newFrame.origin.x; }
                    if (CGRectGetMaxY(newFrame) > CGRectGetMaxY(parentBounds)) {newFrame.size.height= CGRectGetMaxY(parentBounds)-newFrame.origin.y; }
                }
            }

            self.frame = newFrame;

            // change frame and notify delegate
            self.trackedView.frame = newFrame;
            if ([self.delegate respondsToSelector:@selector(resizableViewChangedFrame:)]) {
                [self.delegate resizableViewChangedFrame:self];
            }
        }break;

        case UIGestureRecognizerStateEnded:
        case UIGestureRecognizerStateFailed:
            self.activeKnobType = PSPDFSelectionBorderKnobTypeNone;

            if ([self.delegate respondsToSelector:@selector(resizableViewDidEndEditing:)]) {
                [self.delegate resizableViewDidEndEditing:self];
            }

        default: break;
    }

    return handle;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public

- (void)setAllowEditing:(BOOL)allowEditing {
    if (allowEditing != _allowEditing) {
        _allowEditing = allowEditing;

        for (UIImageView *knob in _knobs) {
            knob.hidden = !allowEditing;
        }
    }
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - PSPDFLongPressGestureRecognizerDelegate

- (BOOL)pressRecognizerShouldHandlePressImmediately:(PSPDFLongPressGestureRecognizer *)recognizer {
    if (!self.allowEditing) return NO;

    CGPoint touchPoint = [recognizer locationInView:self];

    if (CGRectContainsPoint(_knobTopLeft.frame, touchPoint)) {
        self.activeKnobType = PSPDFSelectionBorderKnobTypeTopLeft;
    }else if (CGRectContainsPoint(_knobTopMiddle.frame, touchPoint)) {
        self.activeKnobType = PSPDFSelectionBorderKnobTypeTopMiddle;
    }else if (CGRectContainsPoint(_knobTopRight.frame, touchPoint)) {
        self.activeKnobType = PSPDFSelectionBorderKnobTypeTopRight;
    }else if (CGRectContainsPoint(_knobMiddleLeft.frame, touchPoint)) {
        self.activeKnobType = PSPDFSelectionBorderKnobTypeMiddleLeft;
    }else if (CGRectContainsPoint(_knobMiddleRight.frame, touchPoint)) {
        self.activeKnobType = PSPDFSelectionBorderKnobTypeMiddleRight;
    }else if (CGRectContainsPoint(_knobBottomLeft.frame, touchPoint)) {
        self.activeKnobType = PSPDFSelectionBorderKnobTypeBottomLeft;
    }else if (CGRectContainsPoint(_knobBottomMiddle.frame, touchPoint)) {
        self.activeKnobType = PSPDFSelectionBorderKnobTypeBottomMiddle;
    }else if (CGRectContainsPoint(_knobBottomRight.frame, touchPoint)) {
        self.activeKnobType = PSPDFSelectionBorderKnobTypeBottomRight;
    }else if (CGRectContainsPoint(self.bounds, touchPoint)) {
        self.activeKnobType = PSPDFSelectionBorderKnobTypeMove;
    }else {
        self.activeKnobType = PSPDFSelectionBorderKnobTypeNone;
    }

    return self.activeKnobType != PSPDFSelectionBorderKnobTypeNone;
}

@end
