//
//  PSPDFDocumentLabelView.m
//  PSPDFKit
//
//  Copyright (c) 2012 Peter Steinberger. All rights reserved.
//

#import "PSPDFDocumentLabelView.h"
#import "PSPDFViewController.h"
#import "PSPDFDocument.h"
#import <QuartzCore/QuartzCore.h>

@implementation PSPDFDocumentLabelView

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (id)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleBottomMargin;
    }
    return self;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - PSPDFDocumentLabelView

- (NSArray *)kvoValues {
    return @[NSStringFromSelector(@selector(document)), NSStringFromSelector(@selector(viewMode)), @"viewModeAnimated"];
}

- (void)updateAnimated:(BOOL)animated {
    CGFloat targetAlpha = self.pdfController.viewMode == PSPDFViewModeDocument ? 1.f : 0.f;
    if (![self.pdfController.document isValid]) {
        targetAlpha = 0.f;
    }

    if (self.alpha != targetAlpha || [self.label.text length] == 0) {
        [UIView animateWithDuration:animated ? 0.25f : 0.f delay:0.f options:UIViewAnimationOptionAllowUserInteraction | UIViewAnimationOptionBeginFromCurrentState animations:^{
            self.alpha = targetAlpha;
        } completion:nil];
    }

    self.label.text = self.pdfController.document.title;
    [self setNeedsLayout]; // recalculate outer frame
}

@end
