//
//  PSPFScrobbleBar.m
//  PSPDFKit
//
//  Copyright 2011-2012 Peter Steinberger. All rights reserved.
//

#import "PSPDFKit.h"
#import "PSPDFViewController+Internal.h"
#import <QuartzCore/QuartzCore.h>

#define kPSPDFScrobbleThumbMarkerSizeMultiplikator 1.4
#define kPSPDFScrobbleThumbMargin 5
#define kPSPDFScrobblePointerAnimationDuration 0.3f

#define kPSPDFScrobbleThumbMarkerSize CGSizeMake(roundf([self scrobbleBarThumbSize].width*kPSPDFScrobbleThumbMarkerSizeMultiplikator), roundf([self scrobbleBarThumbSize].height*kPSPDFScrobbleThumbMarkerSizeMultiplikator))

#define kPSPDFPageImagesTagOffset 1000

@interface PSPDFScrobbleBar() {
    NSInteger _pageMarkerPage;
    NSUInteger _thumbCount;
    NSInteger _lastTouchedPage;
    UIImageView *_positionImage;
    UIImageView *_positionImage2;
    NSMutableDictionary *_imageViews;    // NSNumber (page) -> UIImageView
    BOOL _touchInProgress;
}
- (void)setPageInternal:(NSUInteger)page;
@property (nonatomic, assign, getter=isViewLocked) BOOL viewLocked; // for animation
@end

@implementation PSPDFScrobbleBar

static char kPSPDFKVOToken;

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (id)init {
    if ((self = [super init])) {
        self.isAccessibilityElement = YES;
        _imageViews = [NSMutableDictionary new];
        _page = 0;
        _pageMarkerPage = -1;
        _leftBorderMargin = 5.f;
        _rightBorderMargin = 5.f;
        
        // clip images, don't bleed out
        self.clipsToBounds = YES;
        self.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleWidth;

        // translucent black toolbar
        _toolbar = [UIToolbar new];
        _toolbar.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        _toolbar.alpha = 0.7;
        [self addSubview:_toolbar];
        
        // register master position marker
        _positionImage  = [self emptyThumbImageView];
        _positionImage2 = [self emptyThumbImageView];
        
        [self addSubview:_positionImage];
        [self addSubview:_positionImage2];
        // listen for cache hits
        [[PSPDFCache sharedCache] addDelegate:self];
    }
    return self;
}

- (void)dealloc {
    _pdfController = nil;
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(updateToolbarForced) object:nil];
    [[PSPDFCache sharedCache] removeDelegate:self];
}

- (NSArray *)kvoValues {
    // viewModeAnimated is a special token to inform us of an _animated_ change of the viewMode.
    return @[NSStringFromSelector(@selector(document)), NSStringFromSelector(@selector(page)), NSStringFromSelector(@selector(viewMode)), @"viewModeAnimated"];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    if (context == &kPSPDFKVOToken) {
        // performance: only update if visible!
        if (!self.hidden) {
            [self setPage:_pdfController.page];
            [self updatePageMarker];

            // no need to update toolbar position on a page change
            if (![keyPath isEqualToString:NSStringFromSelector(@selector(page))]) {
                BOOL animated = [keyPath isEqualToString:@"viewModeAnimated"];
                [self updateToolbarPositionAnimated:animated forced:NO];
                [self updateToolbar];
            }
        }
        // ensure bar is hidden if no document is set
        self.hidden = _pdfController.document == nil;
    }else {
        [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
    }
}

- (void)setPdfController:(PSPDFViewController *)pdfController {
    if (pdfController != _pdfController) {
        PSPDFViewController *oldController = _pdfController;
        _pdfController = pdfController;
        _toolbar.tintColor = pdfController.tintColor;
        _toolbar.barStyle = [pdfController barStyle];
        [[self kvoValues] enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            [oldController removeObserver:self forKeyPath:obj context:&kPSPDFKVOToken];
            [pdfController addObserver:self forKeyPath:obj options:0 context:&kPSPDFKVOToken];
        }];
        if (pdfController) {
            [self updateToolbarPositionAnimated:NO forced:YES];
            [self updateToolbar];
        }
    }
}

- (void)setHidden:(BOOL)hidden {
    [super setHidden:hidden];
    [self setPage:_pdfController.page];
    [self updatePageMarker];
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UIView

- (void)layoutSubviews {
    [super layoutSubviews];
    [self updatePageMarker];
}

- (void)setFrame:(CGRect)frame {
    [super setFrame:frame];

    if (!self.isViewLocked) {
        _pageMarkerPage = -1; // reset
        // don't update if controller is not even set!
        if (_pdfController) [self updateToolbar];
    }
}

- (CGSize)sizeThatFits:(CGSize)size {
    return [super sizeThatFits:size];
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public

// use queuing system to reduce calls
- (void)updateToolbar {
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(updateToolbarForced) object:nil];
    [self performSelector:@selector(updateToolbarForced) withObject:nil afterDelay:0.f];
}

// create the thumbnails!
- (void)updateToolbarForced {
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(updateToolbarForced) object:nil];
    [CATransaction begin];
    [CATransaction setDisableActions:YES];
    
    // clear old views
    for (UIImageView *imageView in [_imageViews allValues]) {
        [imageView removeFromSuperview];
    }
    [_imageViews removeAllObjects];
    
    // calculate how many thumbs we can display
    CGSize thumbSize = [self scrobbleBarThumbSize];
    NSUInteger pageCount = [self.pdfController.document pageCount];
    NSUInteger maxThumbCount = (self.frame.size.width-self.leftBorderMargin-self.rightBorderMargin) / (CGFloat)(thumbSize.width+kPSPDFScrobbleThumbMargin);
    _thumbCount = MIN(pageCount, maxThumbCount);
    PSPDFLogVerbose(@"Showing %d thumbnails.", _thumbCount);
    
    // build reversed, to have proper caching requests in FIFO stack
    NSInteger lastPage = pageCount;
    for (int i = _thumbCount-1; i >= 0; i--) {        
        // determine which page we show
        // specially handled for _thumbCount = 1 to not divide by zero
        NSInteger page = _thumbCount > 1 ? ceil(pageCount * (i/(CGFloat)(_thumbCount-1))) : 0;
        while (page >= lastPage && page > 0) {
            page--;
        }
        lastPage = page;

        // create new UIImageView if not yet in dict
        UIImageView *pageImage = _imageViews[@(page)];
        if (!pageImage) {
            pageImage = [self emptyThumbImageView];

            // may return nil, callback later with PSPDFCacheDelegate
            pageImage.image = [[PSPDFCache sharedCache] cachedImageForDocument:self.pdfController.document page:page size:PSPDFSizeTiny];
            pageImage.tag = i + kPSPDFPageImagesTagOffset; // trick to remember the page.

            // save in dict
            _imageViews[@(page)] = pageImage;
            [self addSubview:pageImage];
        }

        pageImage.frame = [self rectForThumb:thumbSize withImageSize:pageImage.image ? pageImage.image.size : CGSizeZero position:i];
    }

    // position marker has to be frontmost
    [self bringSubviewToFront:_positionImage2];
    [self bringSubviewToFront:_positionImage];
    [self updatePageMarkerForced:YES];
    [self updateToolbarPositionAnimated:NO forced:YES];
    [CATransaction commit];
}

- (void)setPageInternal:(NSUInteger)page {
    _page = page;
    // perform after slight delay, performance. only update if visible!
    if (!self.hidden) {
        [self setNeedsLayout];
    }
}

- (void)setPage:(NSUInteger)page {
    if (!_touchInProgress) [self setPageInternal:page];
}

- (void)animateImageView:(UIImageView *)imageView newImage:(UIImage *)image {
    [imageView.layer addAnimation:PSPDFFadeTransition() forKey:@"image"];

    // set new image
    imageView.image = image;
    imageView.backgroundColor = [UIColor clearColor]; // remove gray background
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Private

- (CGFloat)contentWidth {
    CGSize thumbSize = [self scrobbleBarThumbSize];
    CGFloat contentWidth = roundf(_thumbCount*thumbSize.width+(_thumbCount-1.f)*kPSPDFScrobbleThumbMargin);
    return contentWidth;
}

// availble width minus outer margins
- (CGFloat)availableWidth {
    CGFloat availableWidth = self.frame.size.width-self.leftBorderMargin-self.rightBorderMargin;
    return availableWidth;
}

- (CGFloat)leftBorder {
    NSUInteger leftBorderForCentering = 0;
    CGFloat contentWidth = [self contentWidth];
    CGFloat availableWidth = [self availableWidth];
    if (contentWidth < availableWidth) {
        leftBorderForCentering = floor((availableWidth - contentWidth)/2.f);
    }
    leftBorderForCentering += self.leftBorderMargin;
    return leftBorderForCentering;
}

- (BOOL)shouldShowTwoPageMarkerForPage:(NSUInteger)page {
    BOOL showTwo = NO;
    if ([self.pdfController isDoublePageMode]) {
        if (page > 0 && [self.pdfController isRightPageInDoublePageMode:page]) {
            page--;
        }
        showTwo = page+1 < [self.pdfController.document pageCount] && !(page == 0 && !self.pdfController.isDoublePageModeOnFirstPage);
    }
    return showTwo;
}

- (CGFloat)leftPosForThumbSize:(CGSize)thumbSize page:(NSUInteger)page {
    //    BOOL shouldShowTwoPageMarkerForPage = [self shouldShowTwoPageMarkerForPage:self.page]; // don't use page as this would return differently for a second page
    CGFloat thumbWidth = [self scrobbleBarThumbSize].width;
    NSUInteger pageCount = MAX(1, (NSInteger)[self.pdfController.document pageCount] - 1);
    CGFloat markerPos = roundf([self leftBorder] - kPSPDFScrobbleThumbMargin/2.f - 1.f + page/(CGFloat)pageCount * ([self contentWidth] - thumbWidth));
    return markerPos;
}

- (void)updatePageMarkerForced:(BOOL)forced {
    // don't update if we're not visible
    if (!forced && self.hidden) {
        return;
    }

    // check if we're on a right page, don't display wrong page combinations in double page mode
    NSInteger page = self.page;
    if ([self.pdfController isDoublePageMode] && [self.pdfController isRightPageInDoublePageMode:page] && page > 0) {
        page--;
    }
    CGSize thumbSize = [self scrobbleBarThumbSize];
    CGFloat scrobbleBarHeight = [self scrobbleBarHeight];
    if (_pageMarkerPage != self.page || forced) {
        NSUInteger pageCount = [self.pdfController.document pageCount];
        if (pageCount) {
            thumbSize = kPSPDFScrobbleThumbMarkerSize;
            UIImage *thumbImage = [[PSPDFCache sharedCache] cachedImageForDocument:self.pdfController.document page:page size:PSPDFSizeTiny];
            if (thumbImage) {
                CGFloat scale = PSPDFScaleForSizeWithinSize(thumbImage.size, thumbSize);
                thumbSize = CGSizeMake(roundf(thumbImage.size.width * scale), roundf(thumbImage.size.height * scale));
            }

            CGFloat markerPos = [self leftPosForThumbSize:thumbSize page:page];
            CGRect markerFrame = CGRectMake(markerPos,
                                            floorf((scrobbleBarHeight-thumbSize.height)/2.f),
                                            thumbSize.width,
                                            thumbSize.height);
            _positionImage.frame = markerFrame;
            _positionImage.image = thumbImage;
        }
        _positionImage.hidden = !pageCount;
        _pageMarkerPage = page;

        // do we have a second page?
        _positionImage2.hidden = YES;
        BOOL shouldShowTwoPageMarker = [self shouldShowTwoPageMarkerForPage:page];
        if (shouldShowTwoPageMarker) {
            UIImage *thumbImage = [[PSPDFCache sharedCache] cachedImageForDocument:self.pdfController.document page:page+1 size:PSPDFSizeTiny];
            if (thumbImage) {
                CGFloat scale = PSPDFScaleForSizeWithinSize(thumbImage.size, kPSPDFScrobbleThumbMarkerSize);
                thumbSize = CGSizeMake(roundf(thumbImage.size.width * scale), roundf(thumbImage.size.height * scale));
            }

            CGFloat markerPos = [self leftPosForThumbSize:thumbSize page:page];
            CGRect markerFrame = CGRectMake(markerPos + _positionImage.frame.size.width - 1.f,
                                            floorf((scrobbleBarHeight-thumbSize.height)/2.f),
                                            thumbSize.width,
                                            thumbSize.height);
            _positionImage2.frame = markerFrame;
            _positionImage2.image = thumbImage;
            _positionImage2.hidden = !pageCount;
        }
    }
}

- (void)updatePageMarker {
    [self updatePageMarkerForced:NO];
}

- (BOOL)isSmallToolbar {
    return !PSIsIpad() && UIInterfaceOrientationIsLandscape([UIApplication sharedApplication].statusBarOrientation);
}

- (CGFloat)scrobbleBarHeight {
    return [self isSmallToolbar] ? 32.f : 44.f;
}

- (CGSize)scrobbleBarThumbSize {
    return [self isSmallToolbar] ? CGSizeMake(14.f, 21.f) : CGSizeMake(18.f, 25.f);
}

- (void)updateToolbarPositionAnimated:(BOOL)animated forced:(BOOL)forced {
    BOOL isValidDocument = _pdfController.document.isValid;
    BOOL shouldShow = _pdfController.viewMode == PSPDFViewModeDocument && isValidDocument && (self.alpha < 0.99f || forced);
    BOOL shouldHide = (_pdfController.viewMode == PSPDFViewModeThumbnails || !isValidDocument) && (self.alpha > 0.f || forced);

    if (shouldShow || shouldHide) {
        self.viewLocked = YES;
        CGFloat scrobbleBarHeight = [self scrobbleBarHeight];

        dispatch_block_t dispatchBlock = ^{
            if (shouldShow) {
                self.alpha = 1.f;
                self.frame = CGRectMake(0, _pdfController.view.bounds.size.height-scrobbleBarHeight, _pdfController.view.bounds.size.width, scrobbleBarHeight);
            }else {
                self.alpha = 0.f;
                self.frame = CGRectMake(0, _pdfController.view.bounds.size.height, _pdfController.view.bounds.size.width, scrobbleBarHeight);
            }
            self.toolbar.frame = self.bounds;
            self.viewLocked = NO;
        };

        if (animated) {
            [UIView animateWithDuration:0.25f delay:0.f options:UIViewAnimationOptionAllowUserInteraction animations:^{
                dispatchBlock();
            } completion:nil];
        }else {
            dispatchBlock();
        }
        [self updatePageMarker];
    }
}

- (CGRect)rectForThumb:(CGSize)thumbSize withImageSize:(CGSize)imageSize position:(NSInteger)position {
    CGSize newThumbSize = thumbSize;
    CGSize scrobbleBarThumbSize = [self scrobbleBarThumbSize];
    if (!CGSizeEqualToSize(imageSize, CGSizeZero)) {
        CGFloat scale = PSPDFScaleForSizeWithinSize(imageSize, scrobbleBarThumbSize);
        newThumbSize = CGSizeMake(imageSize.width * scale, imageSize.height * scale);
    }

    CGFloat scrobbleBarHeight = [self scrobbleBarHeight];
    CGRect rect = CGRectMake([self leftBorder]+position*thumbSize.width+position*kPSPDFScrobbleThumbMargin,
                             floor((scrobbleBarHeight-newThumbSize.height)/2.f),
                             newThumbSize.width,
                             newThumbSize.height);
    return rect;
}

- (BOOL)processTouch:(UITouch *)touch animated:(BOOL)animated {
    PSPDFViewController *pdfController = self.pdfController;
    CGPoint tapPoint = [touch locationInView:self];
    NSUInteger pageCount = [pdfController.document pageCount];
    NSUInteger page = 0;
    CGSize scrobbleBarThumbSize = [self scrobbleBarThumbSize];
    CGFloat minimumLeft = [self leftBorder] + scrobbleBarThumbSize.width/2;
    if (tapPoint.x > minimumLeft) {
        page = floor((tapPoint.x - minimumLeft)/([self contentWidth] - scrobbleBarThumbSize.width) * pageCount);
    }
    if (page >= pageCount) {
        page = pageCount-1;
    }

    // only scroll if page has changed
    BOOL processed = NO;
    if (_lastTouchedPage != page) {
        // checks for double page mode
        BOOL doublePageMode = [pdfController isDoublePageMode];
        if (!doublePageMode || abs(_lastTouchedPage - page) > 1 ||
            ((_lastTouchedPage > page && !(doublePageMode && [pdfController isRightPageInDoublePageMode:page+1])) ||
             (_lastTouchedPage < page && !(doublePageMode && [pdfController isRightPageInDoublePageMode:page])))) {
                PSPDFLogVerbose(@"scrolling to page: %d", page);

                // don't animate if animations are *partially* blocked via PSPDFPageRenderingModeFullPageBlocking.
                // This fixes a issue with UIKit where you get blank space if you mix animation and non-amination scroll directions.
                BOOL blockingScrollTransition = pdfController.renderingMode == PSPDFPageRenderingModeFullPageBlocking && pdfController.pageTransition == PSPDFPageScrollPerPageTransition;
                BOOL shouldAnimate = (animated && PSPDFShouldAnimate()) && !blockingScrollTransition;
                processed = [pdfController setPage:page animated:shouldAnimate];
            }
        _lastTouchedPage = page;
    }
    return processed;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    _lastTouchedPage = -1;
    [self processTouch:[touches anyObject] animated:YES];
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    if ([self processTouch:[touches anyObject] animated:YES]) {
        _touchInProgress = YES;
        // set page internal with result, prevent animation
        [self setPageInternal:_lastTouchedPage];
    }
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    _touchInProgress = NO;
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event {
    _touchInProgress = NO;
}

// prepare a template UIImageView
- (UIImageView *)emptyThumbImageView {
    UIImageView *pageImage = [[UIImageView alloc] initWithFrame:CGRectZero];
    pageImage.backgroundColor = [UIColor colorWithRed:0.6f green:0.6f blue:0.6f alpha:0.7f];
    pageImage.layer.borderColor = [UIColor blackColor].CGColor;
    pageImage.layer.borderWidth = 1.f;
    return pageImage;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - PSPDFCacheDelegate

- (void)didCachePageForDocument:(PSPDFDocument *)document page:(NSUInteger)page image:(UIImage *)cachedImage size:(PSPDFSize)size {
    // only process if we're on a window and it's the tiny size
    CGSize scrobbleBarThumbSize = [self scrobbleBarThumbSize];
    if (self.window && size == PSPDFSizeTiny && document == self.pdfController.document) {
        UIImageView *imageView = _imageViews[@(page)];
        if (imageView) {
            [self animateImageView:imageView newImage:cachedImage];

            // potentially update frame as soon as image has been loaded properly
            imageView.frame = [self rectForThumb:scrobbleBarThumbSize withImageSize:cachedImage.size position:imageView.tag - kPSPDFPageImagesTagOffset];
        }

        // update marker
        if (self.page == page || self.page+1 == page || self.page-1 == page) {
            [self updatePageMarkerForced:YES];
        }
    }
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UIAccessibility

- (UIAccessibilityTraits)accessibilityTraits {
    return UIAccessibilityTraitAllowsDirectInteraction;
}

@end
