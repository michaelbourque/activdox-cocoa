//
//  PSPDFPasswordView.m
//  PSPDFKit
//
//  Copyright (c) 2012 Peter Steinberger. All rights reserved.
//

#import "PSPDFPasswordView.h"
#import "UIImage+PSPDFKitAdditions.h"
#import "PSPDFKitGlobal.h"
#import "PSPDFDocument.h"
#import "PSPDFGradientView.h"
#import "PSPDFConverter.h"
#import <QuartzCore/QuartzCore.h>

#define PSPDFPasswordViewCornerRadius 5.f

@interface PSPDFPasswordView () {
    UIImageView *_lockImage;
    UILabel *_statusLabel;
    UIButton *_unlockButton;
}
@property (nonatomic, strong) UITextField *passwordField;
@end

@implementation PSPDFPasswordView

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (id)initWithFrame:(CGRect)frame {
    if ( self = [super initWithFrame:frame]) {
        _shakeOnError = YES;

        self.backgroundColor = [UIColor clearColor];
        self.layer.cornerRadius = PSPDFPasswordViewCornerRadius;
        self.layer.shadowOffset = CGSizeMake(1, 1);
        self.layer.shadowColor = [UIColor blackColor].CGColor;
        self.layer.shadowOpacity = 1.f;
        self.layer.shadowRadius = self.layer.cornerRadius;

        PSPDFGradientView *gradient = [[PSPDFGradientView alloc] initWithFrame:self.bounds];
        gradient.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        gradient.colors = @[[UIColor colorWithWhite:1.f alpha:1.f], [UIColor colorWithWhite:0.9 alpha:1.f]];
        gradient.layer.cornerRadius = PSPDFPasswordViewCornerRadius; // can't use clipToBounds because of the shadow
        [self addSubview:gradient];

        _lockImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"PSPDFKit.bundle/lock"]];
        _lockImage.contentMode = UIViewContentModeScaleAspectFit;
        [self addSubview:_lockImage];
        
        _statusLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        _statusLabel.backgroundColor = [UIColor clearColor];
        _statusLabel.shadowColor  = [UIColor whiteColor];
        _statusLabel.shadowOffset = CGSizeMake(0, 1);
        _statusLabel.textAlignment = UITextAlignmentCenter;
        _statusLabel.numberOfLines = 2;
        _statusLabel.font = [UIFont systemFontOfSize:PSIsIpad() ? 16 : 14];
        [self addSubview:_statusLabel];
        
        _passwordField = [[UITextField alloc] initWithFrame:CGRectMake(0, 0, 200, 30)];
        [_passwordField addTarget:self action:@selector(passswordFieldChanged:) forControlEvents:UIControlEventEditingChanged];
        _passwordField.delegate = self;
        _passwordField.secureTextEntry = YES;
        _passwordField.autocorrectionType = UITextAutocorrectionTypeNo;
        _passwordField.autocapitalizationType = UITextAutocapitalizationTypeNone;
        _passwordField.borderStyle = UITextBorderStyleRoundedRect;
        [self addSubview:_passwordField];

        // TODO:
        _unlockButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        [_unlockButton setTitle:PSPDFLocalize(@"Unlock") forState:UIControlStateNormal];
        [_unlockButton addTarget:self action:@selector(unlockButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_unlockButton];
        
        [self registerForKeyboardNotifications];        
        [self updateView];
    }
    return self;
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    CGFloat labelMargin = 10;
    _lockImage.frame = PSPDFAlignSizeWithinRectWithOffset(_lockImage.image.size, CGRectInset(self.bounds, labelMargin*6, labelMargin*6), 0, -35, PSPDFRectAlignCenter);
    
    _statusLabel.frame = CGRectMake(labelMargin, self.bounds.size.height - 60, self.bounds.size.width-labelMargin*2, 50);
    _passwordField.frame = CGRectMake(labelMargin*3, self.bounds.size.height - (PSIsIpad() ? 90 : 80), self.bounds.size.width-labelMargin*2*3, 30);
    self.layer.shadowPath = [UIBezierPath bezierPathWithRoundedRect:self.bounds cornerRadius:self.layer.cornerRadius].CGPath;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public

- (void)didMoveToSuperview {
    [self centerInParentView];
}

- (BOOL)becomeFirstResponder {
    if (!_passwordField.hidden) {
        [_passwordField becomeFirstResponder];
        return YES;
    }
    return NO;
}

- (void)setDocument:(PSPDFDocument *)document {
    if (document != _document) {
        _document = document;
        _passwordField.text = @"";
        [self updateView];
    }
}

- (void)centerInParentView {
    CGRect newFrame = PSPDFAlignRectangles(self.bounds, self.superview.bounds, PSPDFRectAlignCenter);
    self.frame = newFrame;
}

- (void)updateView {
    BOOL canBeUnlocked = _document.isLocked && [_document.encryptionFilter length] == 0;
    _passwordField.hidden = !canBeUnlocked;
    _unlockButton.hidden = !canBeUnlocked;
    
    if (canBeUnlocked) {
        _statusLabel.text = PSPDFLocalize(@"Please enter the password.");
    }else {
        _statusLabel.text = [NSString stringWithFormat:PSPDFLocalize(@"This document cannot be unlocked. %@ is unsupported."), _document.encryptionFilter];
        [_passwordField resignFirstResponder];
    }

    [self centerInParentView];
    [self setNeedsLayout];
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Private

- (void)passswordFieldChanged:(id)sender {
    _unlockButton.enabled = [_passwordField.text length] > 0;
}

- (void)unlockButtonPressed:(id)sender {
    PSPDFLogVerbose(@"pressed unlock button");
    NSString *password = _passwordField.text;
    
    if ([_delegate respondsToSelector:@selector(passwordView:shouldlUnlockWithPassword:)]) {
        BOOL shouldUnlock = [_delegate passwordView:self shouldlUnlockWithPassword:password];
        if (!shouldUnlock) {
            return;
        }
    }
    if ([_delegate respondsToSelector:@selector(passwordView:willUnlockWithPassword:)]) {
        [_delegate passwordView:self willUnlockWithPassword:password];
    }
    
    // unlock and notify delegate
    BOOL unlockSuccess = [_document unlockWithPassword:password];
    if (unlockSuccess) {
        [_passwordField resignFirstResponder];
        [_delegate passwordView:self didUnlockWithPassword:password];
    }else {
        if ([_delegate respondsToSelector:@selector(passwordView:didFailToUnlockWithPassword:)]) {
            [_delegate passwordView:self didFailToUnlockWithPassword:password];
        }
        
        // add simple Mac OS X - inspired shake animation
        if (_shakeOnError) {
            CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"position"];
            [animation setDuration:0.07 * PSPDFSimulatorAnimationDragCoefficient()];
            [animation setRepeatCount:3];
            [animation setAutoreverses:YES];
            [animation setFromValue:[NSValue valueWithCGPoint:CGPointMake(self.center.x - 20.0f, self.center.y)]];
            [animation setToValue:[NSValue valueWithCGPoint:CGPointMake(self.center.x + 20.0f, self.center.y)]];
            [self.layer addAnimation:animation forKey:@"position"];
        }
    }
}

- (void)registerForKeyboardNotifications {
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeShown:)
                                                 name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
}

- (void)updateFrameForKeyboardWithNotification:(NSNotification *)aNotification keyboardShown:(BOOL)keyboardShown {
    NSDictionary *info = [aNotification userInfo];
    CGSize kbSize = [info[UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    CGFloat animationDuration = [info[UIKeyboardAnimationDurationUserInfoKey] floatValue];
    [UIView animateWithDuration:animationDuration delay:0.f options:UIViewAnimationOptionBeginFromCurrentState | UIViewAnimationOptionAllowUserInteraction animations:^{
        [self centerInParentView];
        if (keyboardShown) {
            CGRect newFrame = self.frame;
            newFrame.origin.y -= (fminf(kbSize.height, kbSize.width)/(PSIsIpad() ? 2.f : 1.5f));
            self.frame = newFrame;
        }
    } completion:nil];
}

// Called when the UIKeyboardWillShowNotification is sent.
- (void)keyboardWillBeShown:(NSNotification *)aNotification {
    [self updateFrameForKeyboardWithNotification:aNotification keyboardShown:YES];
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification {
    [self updateFrameForKeyboardWithNotification:aNotification keyboardShown:NO];
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self unlockButtonPressed:_unlockButton];
    return NO;
}

@end
