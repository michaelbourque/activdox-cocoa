//
//  PSPDFLabelView.m
//  PSPDFKit
//
//  Copyright (c) 2012 Peter Steinberger. All rights reserved.
//

#import "PSPDFGradientView.h"
#import "PSPDFLabelView.h"
#import "PSPDFViewController.h"
#import <QuartzCore/QuartzCore.h>

@implementation PSPDFLabelView

static char kPSPDFKVOToken;

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (id)initWithFrame:(CGRect)frame {
    if ((self = [super initWithFrame:frame])) {
        self.userInteractionEnabled = NO;
        _labelMargin = 2;
        self.opaque = NO;
        _label = [[UILabel alloc] init];
        _label.backgroundColor = [UIColor clearColor];
        _label.font = [UIFont boldSystemFontOfSize:14.f];
        _label.textColor = [UIColor whiteColor];
        _label.shadowColor = [UIColor blackColor];
        _label.shadowOffset = CGSizeMake(0, 1);
        [self addSubview:_label];
        self.labelStyle = PSPDFLabelStyleFlat;
    }
    return self;
}

- (void)dealloc {
    self.pdfController = nil; // removes KVO
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    if (context == &kPSPDFKVOToken) {
        BOOL animated = [keyPath isEqualToString:@"viewModeAnimated"];
        [self updateAnimated:animated];
    }else {
        [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
    }
}

- (NSArray *)kvoValues {
    return nil;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UIView

- (void)layoutSubviews {
    [super layoutSubviews];

    UILabel *label = self.label;
    CGFloat labelMargin = self.labelMargin;
    [label sizeToFit];
    label.frame = CGRectMake(labelMargin*4, labelMargin, fminf(label.frame.size.width, self.superview.bounds.size.width - labelMargin*18), label.frame.size.height);
    CGRect bounds = CGRectMake(0, 0, label.frame.size.width+labelMargin*8, label.frame.size.height+labelMargin*2);
    self.bounds = bounds;
    self.frame = CGRectIntegral(self.frame); // don't subpixel align centered item!
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public

- (void)setLabelStyle:(PSPDFLabelStyle)labelStyle {
    _labelStyle = labelStyle;

    // backgroundColor won't work because we overrided drawRect in PSPDFGradientView.
    self.backgroundColor = [UIColor clearColor];
    if (labelStyle == PSPDFLabelStyleFlat) {
        self.colors = nil;
        self.colors = @[[UIColor colorWithWhite:0.4f alpha:0.7f]];
        self.layer.cornerRadius = 3.f;
        self.layer.shadowRadius = 0.f;
    }else {
        self.labelMargin = 3.f;
        self.layer.cornerRadius = 6.f;
        self.colors = @[[UIColor colorWithWhite:0.5 alpha:0.7f], [UIColor colorWithWhite:0.1 alpha:0.7f]];
        self.layer.borderColor = [UIColor whiteColor].CGColor;
        self.layer.borderWidth = 1.f;
        self.layer.shadowColor = [UIColor blackColor].CGColor;
        self.layer.shadowOffset = CGSizeMake(0, 1);
        self.layer.shadowOpacity = 0.5f;
        self.layer.shadowRadius = 5.f;
        self.layer.shadowPath = [UIBezierPath bezierPathWithRoundedRect:self.bounds
                                                           cornerRadius:self.layer.cornerRadius].CGPath;
    }
}

- (void)setPdfController:(PSPDFViewController *)pdfController {
    if (pdfController != _pdfController) {
        PSPDFViewController *oldController = _pdfController;
        _pdfController = pdfController;
        [[self kvoValues] enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            [oldController removeObserver:self forKeyPath:obj context:&kPSPDFKVOToken];
            [pdfController addObserver:self forKeyPath:obj options:idx == 0 ? NSKeyValueObservingOptionInitial : 0 context:&kPSPDFKVOToken];
        }];
    }
}

- (void)updateAnimated:(BOOL)animated {
    // subclass
}

@end
