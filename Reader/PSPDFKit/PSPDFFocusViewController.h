//
//  PSPDFFocusViewController.h
//  PSPDFKit
//
//  Copyright (c) 2012 Peter Steinberger. All rights reserved.
//

#import "PSPDFBaseViewController.h"

@interface PSPDFFocusViewController : PSPDFBaseViewController

/// Initialize with a view that should be centered.
- (id)initWithView:(UIView *)focusView;

/// View that is centered in the focusViewController
@property (nonatomic, strong) UIView *focusView;

/// Presents view controller on a new window, on top of everything
- (void)presentAnimated:(BOOL)animated;

/// Dismiss the controller
- (void)dismissAnimated:(BOOL)animated completion:(void (^)(void))completion;

@end
