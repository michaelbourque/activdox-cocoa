//
//  PSPDFContinuousScrollViewController.m
//  PSPDFKit
//
//  Copyright (c) 2012 Peter Steinberger. All rights reserved.
//

#import "PSPDFContinuousScrollViewController.h"
#import "PSPDFViewController.h"
#import "PSPDFViewController+Internal.h"
#import "PSPDFKitGlobal+Internal.h"
#import "PSPDFDocument.h"
#import "PSPDFContentScrollView.h"
#import "PSPDFPageView.h"
#import "PSPDFPageInfo.h"
#import "PSPDFConverter.h"
#import "PSPDFViewState.h"

@interface PSPDFContinuousScrollViewController () {
    BOOL _viewIsVisible:1;
    BOOL _isUpdatingPagingContentSize:1;
    BOOL _targetPageSet:1;
    NSMutableDictionary *_pageSizeCacheDict;
    CGSize _currentCacheSize;
    CGRect *_currentCache;
    dispatch_queue_t _pagesQueue;
    CGSize _lastFrameSize;
}
@property (nonatomic, assign, getter=isRotationActive) BOOL rotationActive;
@property (nonatomic, assign, getter=isRotationAnimationActive) BOOL rotationAnimationActive;
@property (nonatomic, strong) NSMutableSet *recycledPages;
@property (nonatomic, strong) NSMutableSet *visiblePages;
@property (nonatomic, assign) NSUInteger targetPage;
@end

@implementation PSPDFContinuousScrollViewController

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (id)initWithPDFController:(PSPDFViewController *)pdfController {
    if ((self = [super initWithNibName:nil bundle:nil])) {
        _pagesQueue = pspdf_dispatch_queue_create("com.PSPDFKit.pagesQueue", 0);
        self.pdfController = pdfController;
        _pagePadding = pdfController.pagePadding;
        _pageSizeCacheDict = [NSMutableDictionary new];
        _recycledPages = [NSMutableSet new];
        _visiblePages  = [NSMutableSet new];

        // make sure cache is filled early and in a fast way - we need it there.
        [pdfController.document fillPageInfoCache];
    }
    return self;
}

- (void)dealloc {
    PSPDFDispatchRelease(_pagesQueue);
    [self invalidateCache];
    self.pdfController = nil;
    self.scrollView.delegate = nil;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UIViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    _viewIsVisible = YES;

    [self updatePagingContentSize];

    if (_targetPageSet) [self setPage:_targetPage animated:NO];
    [self tilePagesForced:YES];

    // mark size so we don't need to relayout again in viewWillLayoutSubviews.
    _lastFrameSize = self.scrollView.bounds.size;;
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    _viewIsVisible = NO;
    self.targetPage = self.pdfController.page; // save page in case we rotate off-screen
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];

    // remove all recycled pages
    pspdf_dispatch_sync_reentrant(_pagesQueue, ^{
        [self.recycledPages removeAllObjects];
    });
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    [super willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
    self.rotationActive = YES;
    self.targetPage = self.pdfController.page;
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    [super willAnimateRotationToInterfaceOrientation:toInterfaceOrientation duration:duration];

    // rotation is handled implicit via the setFrame-notification
    if ([self isViewLoaded] && self.view.window) {
        // TODO: scroll position should be preserved here, but this needs additional code.
        // As a hotfix, restore zoom scale to 1 on rotation.
        self.scrollView.zoomScale = 1.f;
        self.rotationAnimationActive = YES;
        [self updatePagingContentSize];
        [self setPage:self.targetPage animated:NO];
        [self tilePagesForced:YES];
    }
    self.rotationAnimationActive = NO;
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
    [super didRotateFromInterfaceOrientation:fromInterfaceOrientation];
    self.rotationActive = NO;
}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    
    // only use if not on screen or frame changed
    BOOL frameChanged = !CGSizeEqualToSize(self.scrollView.bounds.size, _lastFrameSize);
    if (frameChanged) {
        /// ||(!self.view.window && self.scrollView.zoomScale == 1.f)) {
        _lastFrameSize = self.scrollView.bounds.size;

        if (!self.isRotationActive) {
            [self updatePagingContentSize];
            [self tilePagesForced:YES];
        }
    }
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Properties

- (void)setTargetPage:(NSUInteger)targetPage {
    _targetPage = targetPage;
    _targetPageSet = YES;
}

- (BOOL)isHorizontal {
    return self.pdfController.scrollDirection == PSPDFScrollDirectionHorizontal;
}

- (void)setScrollView:(PSPDFContentScrollView *)scrollView {
    if (scrollView != _scrollView) {
        _scrollView = scrollView;

        // changing properties
        scrollView.scrollEnabled = self.pdfController.isScrollingEnabled;
        scrollView.pagingEnabled = NO;
        scrollView.decelerationRate = UIScrollViewDecelerationRateNormal;

        // use pageView for zooming if we're in continuous mode
        scrollView.minimumZoomScale = self.pdfController.minimumZoomScale;
        scrollView.maximumZoomScale = self.pdfController.maximumZoomScale;

        // allowed in vertical mode
        scrollView.scrollsToTop = ![self isHorizontal];

        // static stuff
        scrollView.backgroundColor = [UIColor clearColor];
        scrollView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        scrollView.showsVerticalScrollIndicator = NO;
        scrollView.showsHorizontalScrollIndicator = NO;
        scrollView.delegate = self;

        if (kPSPDFDebugScrollViews) {
            scrollView.backgroundColor = [UIColor colorWithRed:0.5f green:0.2f blue:0.f alpha:0.5f];
        }
    }
}

// scroll the pagingScrollView to certain page.
- (void)setPage:(NSUInteger)page animated:(BOOL)animated {
    PSPDFLogVerbose(@"Setting page: %d animated:%d", page, animated);
    
    // if we're not yet visible, delay this.
    if (!_viewIsVisible) {
        self.targetPage = page; return;
    }else {
        _targetPageSet = NO;
    }

    // dual page mode converts e.g. page 50 to index 25 then.
    NSUInteger actualPage = [self.pdfController actualPage:page];
    CGRect pageFrame = [self frameForViewAtIndex:actualPage];
    UIScrollView *scrollView = self.scrollView;
    CGPoint newOffset = pageFrame.origin;
    CGPoint maxOffset = CGPointMake(scrollView.contentSize.width-scrollView.bounds.size.width,
                                    scrollView.contentSize.height-scrollView.bounds.size.height);

    // compensate centering
    if ([self isHorizontal]) {
        newOffset.x = fminf(newOffset.x, maxOffset.x);
        newOffset.y = 0;
    }else {
        newOffset.x = 0;
        newOffset.y = fminf(newOffset.y, maxOffset.y);
    }

    // compensate for zoom
    newOffset.x *= scrollView.zoomScale;
    newOffset.y *= scrollView.zoomScale;

    //NSLog(@"Scrolling to offset: %@", NSStringFromCGPoint(newOffset));
    [scrollView setContentOffset:newOffset animated:animated];

    // if not animated, we have to manually tile pages.
    // also don't manually call when we're in the middle of rotation
    if (!animated && !self.isRotationAnimationActive) {
        [self tilePagesForced:NO];
    }
}

// Returns an array of NSNumber's for the current visible page numbers. (ordered)
- (NSArray *)visiblePageNumbers {
    NSMutableArray *visiblePageNumbers = [NSMutableArray array];
    pspdf_dispatch_sync_reentrant(_pagesQueue, ^{
        for (PSPDFPageView *pageView in self.visiblePages) {
            [visiblePageNumbers addObject:@(pageView.page)];
        }
    });
    return visiblePageNumbers;
}

// Get pageView for a specific page. Returns nil if page isn't currently loaded.
- (PSPDFPageView *)pageViewForPage:(NSUInteger)page {
    __block PSPDFPageView *thePageView = nil;
    pspdf_dispatch_sync_reentrant(_pagesQueue, ^{
        for (PSPDFPageView *pageView in self.visiblePages) {
            if (pageView.page == page) {
                thePageView = pageView;
            }
        }
    });
    return thePageView;
}

- (void)setPdfController:(PSPDFViewController *)pdfController {
    if (pdfController != _pdfController) {
        _pdfController = pdfController;
        pspdf_dispatch_sync_reentrant(_pagesQueue, ^{
            [self.visiblePages makeObjectsPerformSelector:@selector(setPdfController:) withObject:pdfController];
        });
        [self invalidateCache];
    }
}

- (void)setPagePadding:(CGFloat)pagePadding {
    if (pagePadding != _pagePadding) {
        _pagePadding = pagePadding;
        [self invalidateCache];
    }
}

- (CGPoint)compensatedContentOffset {
    NSUInteger page = self.pdfController.page;
    UIScrollView *scrollView = [self.pdfController pageViewForPage:page].scrollView;
    CGPoint pageOffset = [self frameForViewAtIndex:page].origin;
    CGPoint contentOffset = scrollView.contentOffset;
    contentOffset.x -= pageOffset.x;
    contentOffset.y -= pageOffset.y;
    return contentOffset;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Frame Calculations

static CGSize PSPDFScaledRectForPageIndex(PSPDFDocument *document, CGSize viewSize, NSUInteger pageIndex, BOOL zoomMinimalSize, BOOL fitToWidthEnabled) {
    PSPDFPageInfo *pageInfo = [document pageInfoForPage:pageIndex];
    CGFloat scale = PSPDFScaleForSizeWithinSizeWithOptions(pageInfo.rotatedPageRect.size, viewSize, zoomMinimalSize, fitToWidthEnabled);
    CGSize pageSize = PSPDFSizeForScale(pageInfo.rotatedPageRect.size, scale);
    return pageSize;
}

- (void)invalidateCache {
    // cache needs to be manually freed.
    for (NSValue *cacheArray in [_pageSizeCacheDict allValues]) {
        free([cacheArray pointerValue]);
    }
    [_pageSizeCacheDict removeAllObjects];
    _currentCache = nil;
    _currentCacheSize = CGSizeZero;
}

- (CGRect)frameForViewAtIndex:(NSUInteger)pageIndex {
    PSPDFViewController *pdfController = self.pdfController;
    PSPDFDocument *document = pdfController.document;
    CGSize parentViewSize = self.scrollView.bounds.size;

    if (pageIndex >= [document pageCount]) {
        PSPDFLogWarning(@"Invalid pageIndex: %d", pageIndex); return CGRectZero;
    }

    // cannot calculate on nil size.
    if (CGSizeEqualToSize(CGSizeZero, parentViewSize)) return CGRectZero;

    // super fast path
    if (CGSizeEqualToSize(parentViewSize, _currentCacheSize)) {
        CGRect pageFrame = _currentCache[pageIndex];
        return pageFrame;
    }else {
        // check if this size has been already cached (usually happens after a portrait/landscape transition)
        CGRect *cacheArray = [_pageSizeCacheDict[BOXED(parentViewSize)] pointerValue];
        if (cacheArray) {
            _currentCache = cacheArray;
            _currentCacheSize = parentViewSize;
            return cacheArray[pageIndex];
        }else {
            // build up cache
            // Clang doesn't understand that the malloc'ed memory is saved in an NSArray and complains. So silence him.
#ifndef __clang_analyzer__
            cacheArray = malloc([document pageCount] * sizeof(CGRect));
            BOOL zoomMinimalSize = pdfController.zoomingSmallDocumentsEnabled;
            BOOL fitToWidthEnabled = pdfController.fitToWidthEnabled;
            BOOL firstIsCover = !pdfController.doublePageModeOnFirstPage;
            BOOL doublePageMode = [pdfController isDoublePageMode];
            CGSize viewSize = self.scrollView.bounds.size;
            CGRect pageFrame = CGRectZero;
            NSUInteger pageCount = [document pageCount];

            for (NSUInteger i=0; i<pageCount; i++) {
                NSUInteger realPage = [pdfController landscapePage:i];
                CGRect pageRect = (CGRect){CGPointZero, PSPDFScaledRectForPageIndex(document, viewSize, realPage, zoomMinimalSize, fitToWidthEnabled)};

                // special case for first page
                if (i == 0) {
                    cacheArray[0] = PSPDFCenterFrame(pageRect, viewSize, [self isHorizontal]);
                }

                // we always process the NEXT page here. So break early.
                if (i+1 >= pageCount) break;

                if ([self isHorizontal]) {
                    pageFrame.origin.x += pageRect.size.width;

                    // if we have double pages, add the second page size.
                    if (doublePageMode && (i > 0 || !firstIsCover)) {
                        pageRect = (CGRect){CGPointZero, PSPDFScaledRectForPageIndex(document, viewSize, realPage+1, zoomMinimalSize, fitToWidthEnabled)};
                        pageFrame.origin.x += pageRect.size.width;
                    }

                    // always add padding at the end
                    pageFrame.origin.x += self.pagePadding;
                }else {
                    pageFrame.origin.y += pageRect.size.height;

                    // if we have double pages, add the second page size.
                    if (doublePageMode && (i > 0 || !firstIsCover)) {
                        pageRect = (CGRect){CGPointZero, PSPDFScaledRectForPageIndex(document, viewSize, realPage+1, zoomMinimalSize, fitToWidthEnabled)};
                        pageFrame.origin.y += pageRect.size.height;
                    }

                    // always add padding at the end
                    pageFrame.origin.y += self.pagePadding;
                }

                // final page frame, copy size.
                CGRect currentPageFrame = pageFrame;
                NSUInteger lastRealPage = [pdfController landscapePage:i+1];
                currentPageFrame.size = PSPDFScaledRectForPageIndex(document, viewSize, lastRealPage, zoomMinimalSize, fitToWidthEnabled);
                cacheArray[i+1] = PSPDFCenterFrame(currentPageFrame, viewSize, [self isHorizontal]);
            }
            _pageSizeCacheDict[BOXED(parentViewSize)] = BOXED(cacheArray);
            return cacheArray[pageIndex];
#endif
        }
    }
}

// Helper to center pages
static inline CGRect PSPDFCenterFrame(CGRect frameToCenter, CGSize viewSize, BOOL isHorizontal) {
    if (isHorizontal) {
        // calculate center, but don't go negative
        frameToCenter.origin.y = fmaxf(roundf((viewSize.height - frameToCenter.size.height)/2), 0);
    }else {
        frameToCenter.origin.x = fmaxf(roundf((viewSize.width - frameToCenter.size.width)/2), 0);
    }
    return frameToCenter;
}

- (void)updatePagingContentSize {
    _isUpdatingPagingContentSize = YES;
    
    PSPDFDocument *document = self.pdfController.document;
    NSUInteger pageCount = [self.pdfController actualPage:[document pageCount]];
    if ([self.pdfController isDoublePageMode] && (([document pageCount]%2==1 && self.pdfController.doublePageModeOnFirstPage) || ([document pageCount]%2==0 && !self.pdfController.doublePageModeOnFirstPage))) {
        pageCount++; // first page...
    }
    PSPDFLogVerbose(@"pageCount:%d, used page Count:%d", [document pageCount], pageCount);

    CGRect lastPageFrame = [self frameForViewAtIndex:pageCount-1];
    CGSize contentSize = CGSizeMake(CGRectGetMaxX(lastPageFrame), CGRectGetMaxY(lastPageFrame));

    // one side of the view is always complete; we center per page manually
    if ([self isHorizontal]) {
        contentSize.height = fmaxf(self.scrollView.bounds.size.height, contentSize.height);
    }else {
        contentSize.width = fmaxf(self.scrollView.bounds.size.width, contentSize.width);
    }

    PSPDFLogVerbose(@"old contentSize: %@, new: %@", NSStringFromCGSize(self.scrollView.contentSize), NSStringFromCGSize(contentSize));
    self.view.frame = (CGRect){CGPointZero, contentSize};
    self.scrollView.contentSize = contentSize;
    
    _isUpdatingPagingContentSize = NO;
}

- (void)recyclePage:(PSPDFPageView *)page {
    [page prepareForReuse];
    pspdf_dispatch_sync_reentrant(_pagesQueue, ^{
        [self.recycledPages addObject:page];
    });
}

- (PSPDFPageView *)dequeueRecycledPage {
    __block PSPDFPageView *page = nil;
    pspdf_dispatch_sync_reentrant(_pagesQueue, ^{
        page = [self.recycledPages anyObject];
        if (page) {
            [self.recycledPages removeObject:page];
        }
    });
    return page;
}

- (void)tilePagesForced:(BOOL)forceUpdate {
    PSPDFViewController *pdfController = self.pdfController;
    PSPDFDocument *document = pdfController.document;
    CGRect visibleBounds = self.scrollView.bounds;
    CGFloat zoomScale = self.scrollView.zoomScale;
    visibleBounds.origin.x /= zoomScale;
    visibleBounds.origin.y /= zoomScale;
    visibleBounds.size.height /= zoomScale;
    visibleBounds.size.width /= zoomScale;
    //PSPDFLogVerbose(@"%@ visibleBounds: %@", NSStringFromCGRect(self.scrollView.bounds), NSStringFromCGRect(visibleBounds));

    NSUInteger maxPageIndex = [pdfController isDoublePageMode] ? floorf([document pageCount]/2) : [document pageCount] - 1;
    if (![document isValid]) maxPageIndex = 0;

    // Calculate which pages are visible
    int firstNeededPageIndex = 0, lastNeededPageIndex = 0, primaryPageIndex = 0;
    BOOL firstPageFound = NO, completePageFound = NO;

    for (int i=0; i<=maxPageIndex; i++) {
        CGRect rect = [self frameForViewAtIndex:i];
        //NSLog(@"%i %@", i, NSStringFromCGRect(rect));
        BOOL completelyShowsPage = CGRectContainsRect(visibleBounds, rect);
        BOOL interectRect = completelyShowsPage || CGRectIntersectsRect(visibleBounds, rect); // optimization, if completelyShowsPage is true don't check intersection

        // find first page
        if (!firstPageFound && interectRect) {
            firstPageFound = YES;
            firstNeededPageIndex = i;
            if (!completePageFound) primaryPageIndex = i;
        }

        // find page that is fully shown = current page
        if (!completePageFound && completelyShowsPage) {
            completePageFound = YES;
            primaryPageIndex = i;
        }

        // last page
        if (interectRect) {
            lastNeededPageIndex = i;
        }

        // stop calculations
        if (firstPageFound && !interectRect) break;
    }

    // make sure indexes are within range.
    firstNeededPageIndex = MAX(firstNeededPageIndex, 0);
    lastNeededPageIndex  = MIN(lastNeededPageIndex, maxPageIndex);

    // set to zero if document isn't valid or if page size is zero.
    if (![document isValid] || CGRectIsEmpty(visibleBounds)) {
        firstNeededPageIndex = 0;
        lastNeededPageIndex = 0;
        primaryPageIndex = 0;
    }

    // estimate page that is mostly visible
    //NSLog(@"first:%d last:%d page:%d", firstNeededPageIndex, lastNeededPageIndex, primaryPageIndex);

    // Recycle no-longer-visible pages (or convert for re-use while rotation)
    pspdf_dispatch_sync_reentrant(_pagesQueue, ^{
        NSMutableSet *removedPages = [NSMutableSet set];
        for (PSPDFPageView *page in self.visiblePages) {
            if (page.page < firstNeededPageIndex || page.page > lastNeededPageIndex) {
                [self recyclePage:page];
                [removedPages addObject:page];

                // TODO: fixes wild movement of tiles with giving them a default position at the end.
                //page.frame = CGRectMake(self.scrollView.contentSize.width, self.scrollView.contentSize.height, 0, 0);
            }
        }
        [self.visiblePages minusSet:self.recycledPages];

        // add missing pages and don't animate that
        [CATransaction begin];
        [CATransaction setDisableActions:YES];
        NSMutableSet *updatedPages = [NSMutableSet set];
        Class pageViewClass = [pdfController classForClass:[PSPDFPageView class]];
        for (int pageIndex = firstNeededPageIndex; pageIndex <= lastNeededPageIndex; pageIndex++) {
            if (![self isDisplayingPageForIndex:pageIndex]) {
                PSPDFPageView *page = [self dequeueRecycledPage];
                if ([removedPages containsObject:page]) {
                    [removedPages removeObject:page];
                }
                page = page ?: [[pageViewClass alloc] initWithFrame:CGRectZero pdfController:self.pdfController];

                // add view
                [self.view addSubview:page];
                [self.visiblePages addObject:page];

                // configure it (also sends delegate events)
                [self configurePage:page forIndex:pageIndex];

                [updatedPages addObject:page];
            }
        }
        [removedPages makeObjectsPerformSelector:@selector(removeFromSuperview)];
        [CATransaction commit];

        // if forced, configure all pages (used for rotation events)
        if (forceUpdate) {
            for (PSPDFPageView *page in self.visiblePages) {
                if (![updatedPages containsObject:page]) {
                    [self configurePage:page forIndex:page.page];
                }
            }
        }});

    // finally, set new page
    if (pdfController.page != primaryPageIndex) [pdfController setPageInternal:primaryPageIndex];
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Private

// set properties within scrollview, update view
- (void)configurePage:(PSPDFPageView *)pageView forIndex:(NSUInteger)pageIndex {
    [pageView prepareForReuse];
    PSPDFViewController *pdfController = self.pdfController;
    PSPDFDocument *document = pdfController.document;
    PSPDFPageInfo *pageInfo = [document pageInfoForPage:pageIndex];
    CGRect pageRect = [document rectBoxForPage:pageIndex];
    // view has size of whole content - use superview for single page size
    CGFloat scale = PSPDFScaleForSizeWithinSizeWithOptions(pageInfo.pageRect.size, self.view.superview.bounds.size, pdfController.zoomingSmallDocumentsEnabled, pdfController.fitToWidthEnabled);
    [pageView displayDocument:document page:pageIndex pageRect:pageRect scale:scale delayPageAnnotations:YES pdfController:pdfController];
    pageView.shadowEnabled = self.pdfController.shadowEnabled;
    CGRect pageFrame = [self frameForViewAtIndex:pageIndex];
    pageView.frame = pageFrame;
    pageView.autoresizingMask = UIViewAutoresizingNone; // don't ever change the calculated frame!
    [self.pdfController delegateDidLoadPageView:pageView];
}

// be sure to destroy pages before d'allocating
- (void)setVisiblePages:(NSMutableSet *)visiblePages {
    pspdf_dispatch_sync_reentrant(_pagesQueue, ^{
        if (visiblePages != _visiblePages) {
            [self.visiblePages makeObjectsPerformSelector:@selector(setPdfController:) withObject:nil];
            _visiblePages = visiblePages;
        }
    });
}

// checks if a page is already displayed in the scrollview
- (BOOL)isDisplayingPageForIndex:(NSUInteger)pageIndex {
    __block BOOL foundPage = NO;
    pspdf_dispatch_sync_reentrant(_pagesQueue, ^{
        for (PSPDFPageView *page in self.visiblePages) {
            if (page.page == pageIndex) {
                foundPage = YES;
                break;
            }
        }
    });
    return foundPage;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UIScrollViewDelegate

static inline bool PSPDFShouldTilePages(UIScrollView *scrollView) {
    if (!scrollView.isZoomBouncing) {
        if ([scrollView isKindOfClass:[PSPDFScrollView class]]) {
            return !((PSPDFScrollView *)scrollView).isAnimatingZoomIn;
        }else {
            return YES;
        }
    }
    return NO;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    [_scrollView scrollViewDidScroll:scrollView];
    //NSLog(@"scrollViewDidScroll");

    // don't tile if view is not visible
    if (PSPDFShouldTilePages(scrollView) && _viewIsVisible && !self.isRotationAnimationActive && !_isUpdatingPagingContentSize) {
        [self tilePagesForced:NO];
    }
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    [_scrollView scrollViewWillBeginDragging:scrollView];

    if (self.pdfController.viewMode == PSPDFViewModeDocument) {
        [self.pdfController hideControls];
    }
}

- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset {
    [_scrollView scrollViewWillEndDragging:scrollView withVelocity:velocity targetContentOffset:targetContentOffset];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    [_scrollView scrollViewDidEndDragging:scrollView willDecelerate:decelerate];

    [self.pdfController hideControlsIfPageMode];

    if (!decelerate) {
        if ([self.pdfController shouldShowControls]) {
            [self.pdfController showControls];
        }
    }
}

- (void)scrollViewWillBeginDecelerating:(UIScrollView *)scrollView {
    [_scrollView scrollViewWillBeginDecelerating:scrollView];
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    [_scrollView scrollViewDidEndDecelerating:scrollView];

    [self.pdfController hideControlsIfPageMode];
    if ([self.pdfController shouldShowControls]) {
        [self.pdfController showControls];
    }
}

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView {
    [_scrollView scrollViewDidEndScrollingAnimation:scrollView];
    if (PSPDFShouldTilePages(scrollView)) {
        [self tilePagesForced:NO];
    }
}

- (void)scrollViewWillBeginZooming:(UIScrollView *)scrollView withView:(UIView *)view {
    [_scrollView scrollViewWillBeginZooming:scrollView withView:view];
}

- (void)scrollViewDidZoom:(UIScrollView *)scrollView {
    [_scrollView scrollViewDidZoom:scrollView];
    if (PSPDFShouldTilePages(scrollView)) {
        [self tilePagesForced:NO];
    }
}

- (void)scrollViewDidEndZooming:(UIScrollView *)scrollView withView:(UIView *)view atScale:(float)scale {
    [_scrollView scrollViewDidEndZooming:scrollView withView:view atScale:scale];
    if (PSPDFShouldTilePages(scrollView)) {
        [self tilePagesForced:NO];
    }
}

- (void)scrollViewDidScrollToTop:(UIScrollView *)scrollView {
    [_scrollView scrollViewDidScrollToTop:scrollView];
    [self tilePagesForced:NO];
}

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView {
    return [_scrollView viewForZoomingInScrollView:scrollView];
}

@end
