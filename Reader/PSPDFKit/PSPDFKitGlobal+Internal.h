//
//  PSPDFKitGlobal+Internal.h
//  PSPDFKit
//
//  Copyright (c) 2012 Peter Steinberger. All rights reserved.
//

#import "PSPDFPatches.h"

/// Optionally enable scrollbar debugging.
extern BOOL kPSPDFDebugScrollViews;

// Drawing helper.
extern inline void PSPDFDrawHelper(CGContextRef context, CGFloat magnifier);

// Helper to find a custom class in an override dictionary.
extern Class PSPDFClassForClassInDictionary(Class originalClass, NSDictionary *overrideClassNames);

// Helper to check the override class names dict for illegal entries.
// Throws an exception if illegal class is found.
extern void PSPDFCheckOverrideClassNames(NSDictionary *overrideClassNames);

// Replace first occurence of string with replacement string. Not case sensitive.
extern BOOL PSPDFReplaceFirstOccurenceOfStringWithString(NSMutableString *mutableString, NSString *target, NSString *replacement);

// Generate a NSError object in the kPSPDFErrorDomain.
extern NSError *PSPDFError(NSInteger errorCode, NSString *description, NSError **errorToSet);

// Generate NSError object with a sub-error.
extern NSError *PSPDFErrorWithUnderlyingError(NSInteger errorCode, NSString *description, NSError *underlyingError, NSError **errorToSet);

// Generate a NSError object in the kPSPDFErrorDomain from an exception.
extern NSError *PSPDFErrorWithException(NSInteger errorCode, NSString *description, NSException *exception, NSError **errorToSet);

// Fixes paths that are converted incorrectly via NSURL description instead of path.
extern BOOL PSPDFIsIncorrectPath(NSString *path);
extern NSString *PSPDFFixIncorrectPath(NSString *path);

// Returns YES if the path starts from the root directory.
extern BOOL PSPDFIsRootPath(NSString *path);

// Returns the first match in 'string'.
extern NSString *PSPDFFirstMatchInRegexString(NSString *regexString, NSString *string);

// Returns the (localized) name of the current app.
extern NSString *PSPDFAppName(void);

// Return true if `size' is empty (that is, if it has zero width or height),
// false otherwise. A null rect is defined to be empty.
extern bool PSPDFSizeIsEmpty(CGSize size);

// Will return a NSMutableSet that does pointer comparison (instead of isEqual:)
// Used for sets in tight loops -> faster (as long as we can guarrantee to only work with the same set of objects)
extern NSMutableSet *PSPDFCreateMutablePointerCompareSet(void);

// Check the view hiararchy for "classes". Return YES if found.
extern bool PSPDFIsViewClassInsideViewHierarchy(UIView *view, NSArray *classes);
// Atomic getting/setting of properties.
// See http://www.opensource.apple.com/source/objc4/objc4-371.2/runtime/Accessors.subproj/objc-accessors.h
extern void objc_setProperty(id self, SEL _cmd, ptrdiff_t offset, id newValue, BOOL atomic, BOOL shouldCopy);
extern id objc_getProperty(id self, SEL _cmd, ptrdiff_t offset, BOOL atomic);
extern void objc_copyStruct(void *dest, const void *src, ptrdiff_t size, BOOL atomic, BOOL hasStrong);
#define PSPDFAtomicRetainedSetToFrom(dest, source) objc_setProperty(self, _cmd, (ptrdiff_t)(&dest) - (ptrdiff_t)(self), source, YES, NO)
#define PSPDFAtomicCopiedSetToFrom(dest, source) objc_setProperty(self, _cmd, (ptrdiff_t)(&dest) - (ptrdiff_t)(self), source, YES, YES)
#define PSPDFAtomicAutoreleasedGet(source) objc_getProperty(self, _cmd, (ptrdiff_t)(&source) - (ptrdiff_t)(self), YES)
#define PSPDFAtomicStructToFrom(dest, source) objc_copyStruct(&dest, &source, sizeof(__typeof__(source)), YES, NO)

/// CGRect manipulators
extern void PSPDFViewFrameSetX(UIView *view, CGFloat x);
extern void PSPDFViewFrameSetY(UIView *view, CGFloat y);
extern void PSPDFViewFrameSetWidth(UIView *view, CGFloat width);
extern void PSPDFViewFrameSetHeight(UIView *view, CGFloat height);

// Object tracker; can be used instead of __weak. (Used to track UINavigationController)
typedef void (^PSPDFObjectTrackerCallback)(id trackedObject);
extern const char kPSPDFObjectTrackerToken;
@interface PSPDFObjectTracker : NSObject
@property (nonatomic, assign) id trackedObject;
@property (nonatomic, copy) PSPDFObjectTrackerCallback callback;
+ (void)trackObject:(id)object withCallback:(PSPDFObjectTrackerCallback)callback;
@end

#ifdef DEBUG
// Debug helper to track view frame changes.
// Usage: - (void)loadView {self.view = [PSPDFFrameTrackingView new]; }
@interface PSPDFFrameTrackingView : UIView @end
#endif
