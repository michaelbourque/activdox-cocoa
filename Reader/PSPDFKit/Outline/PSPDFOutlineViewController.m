//
//  PSPDFOutlineViewController.m
//  PSPDFKit
//
//  Copyright 2011-2012 Peter Steinberger. All rights reserved.
//

#import "PSPDFOutlineViewController.h"
#import "PSPDFDocument.h"
#import "PSPDFOutlineParser.h"
#import "PSPDFOutlineCell.h"
#import "PSPDFOutlineElement.h"
#import "PSPDFBarButtonItem.h"

@interface PSPDFOutlineViewController() <PSPDFOutlineCellDelegate>
@property (nonatomic, strong) PSPDFOutlineElement *outline; // Array of PSPDFOutlineElements
@property (nonatomic, copy) NSArray *visibleItems;
@end

@implementation PSPDFOutlineViewController

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (id)initWithDocument:(PSPDFDocument *)document delegate:(id<PSPDFOutlineViewControllerDelegate>)delegate {
    if ((self = [super init])) {
        _maximumNumberOfLines = 4;
        _outlineIntentLeftOffset = 32.f;
        _outlineIndentMultiplicator = 15.f;
        _document = document;
        _delegate = delegate;
        _allowCopy = YES;
        self.title = PSPDFLocalize(@"Table Of Contents");
        _outline = document.outlineParser.outline;
        _visibleItems = [_outline flattenedChildren];

        // if there are less than 3 root outline elements, expand them per default
        if ([_visibleItems count] < 3) {
            for (PSPDFOutlineElement *element in _outline.children) {
                if (element.children > 0) {
                    element.expanded = YES;
                }
            }
            _visibleItems = [_outline flattenedChildren];
        }
    }
    return self;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UIViewController

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation {
    return PSIsIpad() ? YES : toInterfaceOrientation != UIInterfaceOrientationPortraitUpsideDown;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

    // for some reason, changes to contentSizeForViewInPopover aren't recognized while we're within the appearance-callbacks. (as of iOS5)
    dispatch_async(dispatch_get_main_queue(), ^{ [self updatePopoverSize]; });

    // try to restore last position
    NSUInteger firstVisibleItem = self.document.outlineParser.firstVisibleElement;
    if (firstVisibleItem < [_visibleItems count]) {
        [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:firstVisibleItem inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:NO];
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self setTopVisibleCellItem];
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Private

- (void)updatePopoverSize {
    CGFloat totalHeight = 0;
    for(PSPDFOutlineElement *outlineElement in _visibleItems) {
        CGFloat width = self.maximumNumberOfLines == 0 ? MAXFLOAT : [PSPDFOutlineCell fontForOutlineElement:outlineElement].lineHeight * self.maximumNumberOfLines;
        totalHeight += [PSPDFOutlineCell heightForCellWithOutlineElement:outlineElement constrainedToSize:CGSizeMake(self.view.bounds.size.width, width) outlineIntentLeftOffset:self.outlineIntentLeftOffset outlineIntentMultiplicator:self.outlineIndentMultiplicator];
    }
    self.contentSizeForViewInPopover = CGSizeMake(self.contentSizeForViewInPopover.width, totalHeight);
}

- (void)setTopVisibleCellItem {
    NSUInteger firstElement = 0;
    NSArray *visibleCells = self.tableView.visibleCells;
    if ([visibleCells count]) {
        firstElement = [self.tableView indexPathForCell:visibleCells[0]].row;

        // or is the cell almost not visible anymore?
        CGRect firstCellRect = [visibleCells[0] frame];
        if ([visibleCells count] > 1 && (self.tableView.contentOffset.y + firstCellRect.size.height/2 > firstCellRect.origin.y + firstCellRect.size.height)) {
            firstElement++;
        }
    }
    self.document.outlineParser.firstVisibleElement = firstElement;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    PSPDFOutlineElement *outlineElement = _visibleItems[indexPath.row];
    BOOL processed = [_delegate outlineController:self didTapAtElement:outlineElement];
    if (!processed && [outlineElement.children count] > 0) {
        // try to expand/collapse
        [self outlineCellDidTapDisclosureButton:(PSPDFOutlineCell *)[tableView cellForRowAtIndexPath:indexPath]];
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
    }
}

// at some point animations are getting to expensive VS simple reloading.
#define kPSPDFTableItemAnimationTreshhold 1000

// Cell delegate - expand/shrink content
- (void)outlineCellDidTapDisclosureButton:(PSPDFOutlineCell *)cell {
	PSPDFOutlineElement *item = cell.outlineElement;
    if (item.children.count) {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[_visibleItems indexOfObjectIdenticalTo:item] inSection:0];

        BOOL needsFullReload = NO;

        if (item.isExpanded) {
            NSMutableArray *removedIndexPaths = [NSMutableArray array];
            NSArray *collapsedChildren = [item flattenedChildren];
            item.expanded = NO;
            for (int i=0; i<collapsedChildren.count; i++) {
                NSIndexPath *removedIndexPath = [NSIndexPath indexPathForRow:indexPath.row + i + 1 inSection:0];
                [removedIndexPaths addObject:removedIndexPath];
            }
            _visibleItems = [_outline flattenedChildren];

            // performance tweak: removing A LOT of rows is super slow.
            if ([removedIndexPaths count] > kPSPDFTableItemAnimationTreshhold) {
                needsFullReload = YES;
            }else {
                [self.tableView beginUpdates];
                [self.tableView deleteRowsAtIndexPaths:removedIndexPaths withRowAnimation:UITableViewRowAnimationTop];
                [self.tableView endUpdates];
            }
        }else {
            item.expanded = YES;
            NSArray *expandedChildren = [item flattenedChildren];
            NSMutableArray *addedIndexPaths = [NSMutableArray array];
            for (int i=0; i<expandedChildren.count; i++) {
                NSIndexPath *addedIndexPath = [NSIndexPath indexPathForRow:indexPath.row + i + 1 inSection:0];
                [addedIndexPaths addObject:addedIndexPath];
            }
            _visibleItems = [_outline flattenedChildren];
            if ([addedIndexPaths count] > kPSPDFTableItemAnimationTreshhold) {
                needsFullReload = YES;
            }else {
                [self.tableView beginUpdates];
                [self.tableView insertRowsAtIndexPaths:addedIndexPaths withRowAnimation:UITableViewRowAnimationTop];
                [self.tableView endUpdates];
            }
        }
        if (needsFullReload) {
            [self.tableView reloadData];
        }else {
            [UIView animateWithDuration:0.25f delay:0.f options:0 animations:^{
                [cell updateOutlineButton];
            } completion:nil];
        }
    }
    [self updatePopoverSize];
}

- (void)tableView:(UITableView*)tableView performAction:(SEL)action forRowAtIndexPath:(NSIndexPath*)indexPath withSender:(id)sender {
    if (action == @selector(copy:)) {
        [UIPasteboard generalPasteboard].string = [_visibleItems[indexPath.row] title];
    }
}

- (BOOL)tableView:(UITableView*)tableView canPerformAction:(SEL)action forRowAtIndexPath:(NSIndexPath*)indexPath withSender:(id)sender {
    return self.allowCopy && action == @selector(copy:);
}

- (BOOL)tableView:(UITableView*)tableView shouldShowMenuForRowAtIndexPath:(NSIndexPath*)indexPath {
    return self.allowCopy;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [_visibleItems count] > 0 ? 1 : 0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [_visibleItems count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    PSPDFOutlineElement *outlineElement = _visibleItems[indexPath.row];
    CGFloat width = self.maximumNumberOfLines == 0 ? MAXFLOAT : [PSPDFOutlineCell fontForOutlineElement:outlineElement].lineHeight * self.maximumNumberOfLines;
    CGFloat height = [PSPDFOutlineCell heightForCellWithOutlineElement:outlineElement constrainedToSize:CGSizeMake(self.view.bounds.size.width, width) outlineIntentLeftOffset:self.outlineIntentLeftOffset outlineIntentMultiplicator:self.outlineIndentMultiplicator];
    return height;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellID = @"PSPDFOutlineCell";

	PSPDFOutlineCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
	if (cell == nil) {
		cell = [[PSPDFOutlineCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }

    cell.outlineIntentLeftOffset = self.outlineIntentLeftOffset;
    cell.outlineIndentMultiplicator = self.outlineIndentMultiplicator;
    cell.outlineElement = _visibleItems[indexPath.row];

#if __IPHONE_OS_VERSION_MAX_ALLOWED < 60000
    cell.textLabel.lineBreakMode = UILineBreakModeTailTruncation; // deprecated in iOS6.
#else
    cell.textLabel.lineBreakMode = NSLineBreakByTruncatingTail;
#endif
    cell.textLabel.numberOfLines = self.maximumNumberOfLines;
    cell.delegate = self;
    return cell;
}

@end
