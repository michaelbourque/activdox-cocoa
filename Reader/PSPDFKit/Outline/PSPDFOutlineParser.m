//
//  PSPDFOutlineParser.m
//  PSPDFKit
//
//  Copyright 2011-2012 Peter Steinberger. All rights reserved.
//

#import "PSPDFKit.h"
#import "PSPDFKitGlobal+Internal.h"
#import "PSPDFDocumentProvider.h"
#import "PSPDFOutlineParser.h"
#import "PSPDFOutlineElement.h"
#import "PSPDFOrderedDictionary.h"

@interface PSPDFOutlineParser() {
    PSPDFOrderedDictionary *_outlinePageDict;
}
@property (nonatomic, weak) PSPDFDocumentProvider *documentProvider;
@property (nonatomic, strong) NSMutableDictionary *namedDestinations;
@end

@implementation PSPDFOutlineParser

@synthesize outline = _outline;

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (id)initWithDocumentProvider:(PSPDFDocumentProvider *)documentProvider {
    if ((self = [super init])) {
        _documentProvider = documentProvider;
    }
    return self;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public

- (BOOL)isOutlineParsed {
    return _outline != nil;
}

- (BOOL)isOutlineAvailable {
    [self parseOutline];
    return [_outline.children count] > 0;
}

- (PSPDFOutlineElement *)outline {
    if (!_outline) {
        [self parseOutline]; // ensure outline is parsed.
    }
    return _outline;
}

- (NSArray *)parseOutline {
    NSArray *outlineElements = nil;
    @synchronized(self) {
        if (!_outline) {
            if (![self.documentProvider isLocked]) {
                outlineElements = [self parseOutlineInternal];
                _outline = [[PSPDFOutlineElement alloc] initWithTitle:nil page:0 relativePath:nil children:outlineElements level:0];
            }
        }else {
            outlineElements = _outline.children;
        }
    }
    return outlineElements;
}

- (PSPDFOutlineElement *)outlineElementForPage:(NSUInteger)page exactPageOnly:(BOOL)exactPageOnly {
    [self parseOutline]; // ensure outline is parsed

    // don't show outline if it's just one entry (most likely just the PDF name)
    if ([_outlinePageDict count] <= 2) return nil;

    PSPDFOutlineElement *outline = _outlinePageDict[@(page)];

    // no outline, and not exact page needed? do a search
    if (!outline && !exactPageOnly && [_outlinePageDict count] > 0) {

        // iterate over sorted array of keys
        NSUInteger lastOutlinePage = 0;
        for (NSNumber *pageNumber in _outlinePageDict.keyArray) {
            NSUInteger currentPage = [pageNumber unsignedIntegerValue];

            if (currentPage > page) {
                break;
            }else {
                lastOutlinePage = currentPage;
            }
        }

        outline = _outlinePageDict[@(lastOutlinePage)];
    }

    return outline;
}

// parse outline (w/o locking and root outline element)
- (NSArray *)parseOutlineInternal {
    __block NSArray *outlineArray = nil;

    // capture documentProvider so we don't get deallocated while parsing.
    PSPDFDocumentProvider *documentProvider = _documentProvider;

    [documentProvider performBlock:^(PSPDFDocumentProvider *docProvider, CGPDFDocumentRef documentRef) {
        CGPDFDictionaryRef pdfDocDictionary = CGPDFDocumentGetCatalog(documentRef);

        // get outline & loop through dictionary...
        CGPDFDictionaryRef outlineRef;
        if (CGPDFDictionaryGetDictionary(pdfDocDictionary, "Outlines", &outlineRef)) {
            self.namedDestinations = [NSMutableDictionary dictionary]; // prepare named destinations dict
            outlineArray = [self parseOutline:outlineRef documentRef:documentRef];

            // named destinations are pretty complex - need to search corresponding page for destination string
            if ([_namedDestinations count] > 0) {
                __block BOOL resolvedAtLeastOnePage = NO;
                PSPDFLogVerbose(@"Named destinations found. Resolving...");

                NSDictionary *resolvedNames = [[self class] resolveDestNames:[NSSet setWithArray:[_namedDestinations allKeys]] documentRef:documentRef];
                [resolvedNames enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
                    PSPDFOutlineElement *outlineElement = _namedDestinations[key];
                    if (outlineElement) {
                        NSInteger destPage = [obj integerValue];
                        PSPDFLogVerbose(@"MATCH FOUND: %@ (%@) is page %d", key, outlineElement.title, destPage);
                        outlineElement.page = destPage;
                        resolvedAtLeastOnePage = YES;
                    }
                }];

                // sanity check. we don't want a *wrong* outline. worst case, remote it completely!
                if (!resolvedAtLeastOnePage) {
                    PSPDFLogWarning(@"Outline pages couldn't be resolved, will remove outline now.");
                    outlineArray = nil;
                }

                // restore memory
                self.namedDestinations = nil;
            }
        }else {
            PSPDFLogVerbose(@"No outline found.");
        }
        [self buildPageNameCache:outlineArray];
    }];

    return outlineArray ?: @[];
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Private

+ (CGPDFDictionaryRef)pageReferenceForObject:(CGPDFObjectRef)pageObjectRef {
    CGPDFDictionaryRef actualPageRef = nil;
    CGPDFObjectType pageObjectType = CGPDFObjectGetType(pageObjectRef);

    // first item in our array is the page dict
    if (pageObjectType == kCGPDFObjectTypeArray) {
        CGPDFArrayRef pageArrayRef;
        if (CGPDFObjectGetValue(pageObjectRef, pageObjectType, &pageArrayRef)) {
            CGPDFArrayGetDictionary(pageArrayRef, 0, &actualPageRef);
        }
    }else if (pageObjectType == kCGPDFObjectTypeDictionary) {
        CGPDFDictionaryRef pageDictRef;
        if (CGPDFObjectGetValue(pageObjectRef, pageObjectType, &pageDictRef)) {
            CGPDFArrayRef dRef;
            if (CGPDFDictionaryGetArray(pageDictRef, "D", &dRef)) {
                CGPDFArrayGetDictionary(dRef, 0, &actualPageRef);
            }
        }
    }else if (pageObjectType == kCGPDFObjectTypeNull) {
        // ignore
    }else {
        PSPDFLogError(@"Warning: Parsing error - unexpected type before actualPageRef: %d", pageObjectType);
    }

    return actualPageRef;
}

- (PSPDFOutlineElement *)parseOutlineElement:(CGPDFDictionaryRef)outlineElementRef level:(NSUInteger)level error:(NSError **)error documentRef:(CGPDFDocumentRef)documentRef {
    NSString *namedDestination = nil;
    int pageIndex = 0;
    NSString *relativePath = nil;

    // parse title
    NSString *outlineTitle = PSPDFDictionaryGetString(outlineElementRef, @"Title");
    PSPDFLogVerbose(@"outline title: %@", outlineTitle);
    if (!outlineTitle) { PSPDFError(PSPDFErrorCodeOutlineParser, @"No outline title found.", error); return nil; }

    CGPDFObjectRef destinationRef;
    if (CGPDFDictionaryGetObject(outlineElementRef, "Dest", &destinationRef)) {
        CGPDFObjectType destinationType = CGPDFObjectGetType(destinationRef);

        // named destination
        // http://stackoverflow.com/questions/4643489/how-do-i-retrieve-a-page-number-or-page-reference-for-an-outline-destination-in-a
        if (destinationType == kCGPDFObjectTypeString) {
            CGPDFStringRef destinationStrRef;
            if (CGPDFObjectGetValue(destinationRef, kCGPDFObjectTypeString, &destinationStrRef)) {
                namedDestination = CFBridgingRelease(CGPDFStringCopyTextString(destinationStrRef));
                //PSPDFLog(@"named destination [string]: %@", namedDestination);
            }
        }else if (destinationType == kCGPDFObjectTypeName) {
            const char *name = nil;
            if (CGPDFObjectGetValue(destinationRef, kCGPDFObjectTypeName, &name)) {
                namedDestination = @(name);
                //PSPDFLog(@"named destination [name]: %@", namedDestination);
            }
        }else if (destinationType == kCGPDFObjectTypeArray) {
            // we expect an array at index 0
            CGPDFArrayRef destinationArray;
            if (CGPDFObjectGetValue(destinationRef, destinationType, &destinationArray)) {
                CGPDFDictionaryRef pageRef;
                if (CGPDFArrayGetDictionary(destinationArray, 0, &pageRef)) {
                    pageIndex = [[self class] pageNumberForPageTreeNode:pageRef doc:documentRef];
                }else {
                    PSPDFLogWarning(@"Expected Dictionary at index 0");
                }
            }else {
                PSPDFLogWarning(@"Failed fetching destination array (unexpected).");
            }
        }else {
            PSPDFLogWarning(@"Unsupported destination of type %d.", destinationType);
        }
    }else {
        // parse A dict
        CGPDFDictionaryRef aRef;
        if (CGPDFDictionaryGetDictionary(outlineElementRef, "A", &aRef)) {
            PSPDFLogVerbose(@"A dict: %ld", (long int)CGPDFDictionaryGetCount(aRef));

            // check that it's a GoTo
            const char *action = nil;
            if (CGPDFDictionaryGetName(aRef, "S", &action)) {
                if (strcmp(action, "GoTo") != 0) {
                    if (strcmp(action, "GoToR") != 0) {
                        PSPDFLogVerbose(@"Invalid page reference, skipping entry.");
                    }else {
                        // outline to external document.
                        CGPDFArrayRef destArrayRef;
                        if (CGPDFDictionaryGetArray(aRef, "D", &destArrayRef)) {
                            NSArray *destArray = PSPDFConvertPDFArray(destArrayRef);
                            if ([destArray count] >= 3) {
                                // ensure that pageIndex is only set if we have a doc path.
                                relativePath = PSPDFDictionaryGetString(aRef, @"F");
                                if ([relativePath length]) {
                                    pageIndex = [destArray[2] integerValue];
                                }
                            }
                        }
                    }
                }else {
                    namedDestination = PSPDFDictionaryGetString(aRef, @"D");
                    if (namedDestination) {
                        PSPDFLogVerbose(@"destination: %@", namedDestination);
                    }else {
                        // can also be an array with page content
                        CGPDFObjectRef pageRefContainer;
                        CGPDFDictionaryGetObject(outlineElementRef, "A", &pageRefContainer);
                        CGPDFDictionaryRef pageRef = [[self class] pageReferenceForObject:pageRefContainer];
                        if (pageRef) {
                            // write to header pageIndex
                            pageIndex = [[self class] pageNumberForPageTreeNode:pageRef doc:documentRef];
                        }
                    }
                }
            }
        }else {
            if (CGPDFDictionaryGetDictionary(outlineElementRef, "SE", &aRef)) {
                PSPDFLogVerbose(@"SE dict: %ld", (long int)CGPDFDictionaryGetCount(aRef));
            }
        }
    }

    // parse descendants (child elements)
    NSArray *descendantOutlineElements = nil;
    CGPDFDictionaryRef descendantRef;
    if (CGPDFDictionaryGetDictionary(outlineElementRef, "First", &descendantRef)) {
        descendantOutlineElements = [self parseOutlineElements:descendantRef level:level+1 error:error documentRef:documentRef]; // return all following outline references
    }

    PSPDFOutlineElement *outlineElement = [[PSPDFOutlineElement alloc] initWithTitle:outlineTitle page:pageIndex relativePath:relativePath children:descendantOutlineElements level:level];

    // add to dict if this needs to be resolved later.
    if (namedDestination) {
        _namedDestinations[namedDestination] = outlineElement;
    }

    return outlineElement;
}

- (NSArray *)parseOutlineElements:(CGPDFDictionaryRef)outlineElementRef level:(NSUInteger)level error:(NSError **)error documentRef:(CGPDFDocumentRef)documentRef {

    // parse current outline element
    PSPDFOutlineElement *outlineElement = [self parseOutlineElement:outlineElementRef level:level error:error documentRef:documentRef];

    // parse next itemss
    NSMutableArray *nextOutlineElements = nil;
    CGPDFDictionaryRef nextRef = outlineElementRef;
    while (CGPDFDictionaryGetDictionary(nextRef, "Next", &nextRef)) {
        if (!nextOutlineElements) nextOutlineElements = [NSMutableArray array];
        
        PSPDFOutlineElement *childOutlineElement = [self parseOutlineElement:nextRef level:level error:error documentRef:documentRef];
        if (childOutlineElement) {
            [nextOutlineElements addObject:childOutlineElement];
        }
    }
    
    NSMutableArray *outlineElements = [NSMutableArray arrayWithObject:outlineElement];
    if (nextOutlineElements) {
        [outlineElements addObjectsFromArray:nextOutlineElements];
    }

    return outlineElements;
}

// start parsing outline
- (NSArray *)parseOutline:(CGPDFDictionaryRef)outlineRef documentRef:(CGPDFDocumentRef)documentRef {
    NSError *error = nil;

    CGPDFInteger elements;
    if (CGPDFDictionaryGetInteger(outlineRef, "Count", &elements)) {
        PSPDFLogVerbose(@"parsing outline: %ld elements.", (long int)elements);
    }else {
        PSPDFLogError(@"Error while parsing outline. No outlineRef?");
    }

    NSArray *outlineElements = nil;

    // don't trust Count. There are documents that have e.g. "-10".
    CGPDFDictionaryRef firstEntry;
    if (CGPDFDictionaryGetDictionary(outlineRef, "First", &firstEntry)) {
        outlineElements = [self parseOutlineElements:firstEntry level:0 error:&error documentRef:documentRef];
    }else {
        // only show warning if we would have expected a First entry.
        if (elements > 0) {
            PSPDFLogWarning(@"Error while parsing outline. First entry not found!");
        }
    }

    return outlineElements;
}

// crawl through outline and build the dictionary
- (void)recursiveOutlineCrawler:(PSPDFOutlineElement *)outlineElement {
    NSNumber *pageNumber = @(outlineElement.page);
    if (!_outlinePageDict[pageNumber]) {
        _outlinePageDict[pageNumber] = outlineElement;
    }

    for(PSPDFOutlineElement *childOutline in outlineElement.children) {
        [self recursiveOutlineCrawler:childOutline];
    }
}

- (void)buildPageNameCache:(NSArray *)outlineArray {
    _outlinePageDict = [PSPDFOrderedDictionary new];

    // crawl root outline, goes deep
    for(PSPDFOutlineElement *outlineElement in outlineArray) {
        [self recursiveOutlineCrawler:outlineElement];
    }

    // sort the dictionary
    [_outlinePageDict.keyArray sortUsingSelector:@selector(compare:)];
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Destination Names Resolver

+ (NSDictionary *)resolveDestNames:(NSSet *)destNames documentRef:(CGPDFDocumentRef)documentRef {
    NSMutableDictionary *resolvedDestNames = [NSMutableDictionary dictionary];
    for (NSString *destName in destNames) {
        int pageIndex = [self resolveNamedDestination:destName documentRef:documentRef];
        resolvedDestNames[destName] = @(pageIndex);
    }
    return resolvedDestNames;
}

// resolves a single named destination
+ (int)resolveNamedDestination:(NSString *)destinationName documentRef:(CGPDFDocumentRef)documentRef {
    CGPDFDictionaryRef catalog = CGPDFDocumentGetCatalog(documentRef);

    // Relatively rare case, deprecated with PDF 1.2, named destinations are defined as a /Dests dictionary in the catalog
    CGPDFDictionaryRef destsDict = NULL;
    CGPDFDictionaryGetDictionary(catalog, "Dests", &destsDict);
    if (destsDict != NULL) {
        CGPDFArrayRef destArray = NULL;
        bool destIsArray = CGPDFDictionaryGetArray(destsDict, [destinationName UTF8String], &destArray);
        if (!destIsArray) {
            CGPDFDictionaryRef destDDict = NULL;
            CGPDFDictionaryGetDictionary(destsDict, [destinationName UTF8String], &destDDict);
            if (destDDict != NULL) {
                destIsArray = CGPDFDictionaryGetArray(destDDict, "D", &destArray);
            }
        }
        if (destIsArray) {
            CGPDFDictionaryRef pageRef = NULL;
            CGPDFArrayGetDictionary(destArray, 0, &pageRef);
            if (pageRef != NULL) {
                int pageIndex = [self pageNumberForPageTreeNode:pageRef doc:documentRef];
                return pageIndex;
            } else {
                return 0;
            }
        }
    }

    CGPDFDictionaryRef namesRef = NULL;
    CGPDFDictionaryGetDictionary(catalog, "Names", &namesRef);
    if (namesRef != NULL) {

        CGPDFDictionaryRef destsRef = NULL;
        CGPDFDictionaryGetDictionary(namesRef, "Dests", &destsRef);

        while (destsRef != NULL) {
            CGPDFArrayRef names = NULL;
            CGPDFDictionaryGetArray(destsRef, "Names", &names);
            if (names != NULL) {
                // root or leaf node...
                for (int i=0; i<CGPDFArrayGetCount(names); i++) {
                    if (i % 2 == 0) {
                        // key
                        CGPDFStringRef keyRef = NULL;
                        CGPDFArrayGetString(names, i, &keyRef);
                        CFStringRef keyStringRef = CGPDFStringCopyTextString(keyRef);
                        if (CFStringCompare(keyStringRef, (__bridge CFStringRef)destinationName, 0) == kCFCompareEqualTo) {
                            CFRelease(keyStringRef);

                            CGPDFDictionaryRef valueRef = NULL;
                            CGPDFArrayGetDictionary(names, i+1, &valueRef);
                            if (valueRef != NULL) {
                                CGPDFArrayRef dRef = NULL;
                                CGPDFDictionaryGetArray(valueRef, "D", &dRef);
                                if (dRef != NULL) {
                                    CGPDFDictionaryRef pageRef = NULL;
                                    CGPDFArrayGetDictionary(dRef, 0, &pageRef);
                                    if (pageRef != NULL) {
                                        int pageIndex = [self pageNumberForPageTreeNode:pageRef doc:documentRef];
                                        return pageIndex;
                                    }
                                }
                            } else {
                                CGPDFArrayRef valueArrayRef = NULL;
                                bool valueIsArray = CGPDFArrayGetArray(names, i+1, &valueArrayRef);
                                if (valueIsArray) {
                                    CGPDFDictionaryRef pageRef = NULL;
                                    CGPDFArrayGetDictionary(valueArrayRef, 0, &pageRef);
                                    if (pageRef != NULL) {
                                        int pageIndex = [self pageNumberForPageTreeNode:pageRef doc:documentRef];
                                        return pageIndex;
                                    }
                                }
                            }
                            return [self resolveNamedDestinationByTraversingCompleteTree:destinationName documentRef:documentRef];

                        } else {
                            CFRelease(keyStringRef);
                        }
                    }
                }
                return [self resolveNamedDestinationByTraversingCompleteTree:destinationName documentRef:documentRef];
            } else {
                // intermediate node...
                CGPDFArrayRef kidsRef = NULL;
                CGPDFDictionaryGetArray(destsRef, "Kids", &kidsRef);
                destsRef = NULL;
                for (int i=0; i<CGPDFArrayGetCount(kidsRef); i++) {
                    CGPDFDictionaryRef kidRef = NULL;
                    CGPDFArrayGetDictionary(kidsRef, i, &kidRef);

                    CGPDFArrayRef limits = NULL;
                    CGPDFDictionaryGetArray(kidRef, "Limits", &limits);
                    if (limits != NULL) {
                        CGPDFStringRef limit1 = NULL;
                        CGPDFStringRef limit2 = NULL;
                        CGPDFArrayGetString(limits, 0, &limit1);
                        CGPDFArrayGetString(limits, 1, &limit2);
                        if (limit1 != NULL && limit2 != NULL) {
                            CFStringRef limit1String = CGPDFStringCopyTextString(limit1);
                            CFStringRef limit2String = CGPDFStringCopyTextString(limit2);

                            CFComparisonResult compare1 = CFStringCompare(limit1String, (__bridge CFStringRef)destinationName, 0);
                            CFComparisonResult compare2 = CFStringCompare(limit2String, (__bridge CFStringRef)destinationName, 0);

                            CFRelease(limit1String);
                            CFRelease(limit2String);

                            if (compare1 == kCFCompareLessThan || compare1 == kCFCompareEqualTo) {
                                if (compare2 == kCFCompareGreaterThan || compare2 == kCFCompareEqualTo) {
                                    destsRef = kidRef;
                                    break;
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    return [self resolveNamedDestinationByTraversingCompleteTree:destinationName documentRef:documentRef];
}

// resolves a pageTree node and returns the page number
+ (int)pageNumberForPageTreeNode:(CGPDFDictionaryRef)pageRef doc:(CGPDFDocumentRef)pdfDoc {
	CGPDFDictionaryRef currentNode = pageRef;
	CGPDFDictionaryRef parent = NULL;
	CGPDFDictionaryGetDictionary(currentNode, "Parent", &parent);

	int pageNumber = 1;
	while (parent != NULL) {
		CGPDFArrayRef kids = NULL;
		bool hasKids = CGPDFDictionaryGetArray(parent, "Kids", &kids);
		if (!hasKids) {
			return pageNumber;
		}
		BOOL kidFound = NO;
		for (int i=0; i<CGPDFArrayGetCount(kids); i++) {
			CGPDFDictionaryRef kid = NULL;
			CGPDFArrayGetDictionary(kids, i, &kid);
			if (kid == currentNode) {
				kidFound = YES;
				currentNode = parent;
				bool hasParent = CGPDFDictionaryGetDictionary(parent, "Parent", &parent);
				if (!hasParent) {
					return pageNumber;
				}
				break;
			} else {
				CGPDFInteger count = 1;
				CGPDFDictionaryGetInteger(kid, "Count", &count);
				pageNumber += count;
			}
		}
		if (!kidFound) {
			pageNumber = 0;
			break;
		}
	}

	// Workaround for iOS 5: (TODO: seems to be fixed in iOS6)?
	// Apparently, CGPDFDictionary cannot be reliably compared by memory address in iOS 5.
	// If the above (faster) method fails, we use the length of the content stream(s)
	// to find out the page number. This is slower and there's a (small) risk that two
	// pages have the same content stream length, but this is pretty unlikely and comparing
	// the actual content stream data would be much slower.
	if (pageNumber == 0) {
		CGPDFInteger pageRefContentStreamLength = [self lengthForContentStreamsInPageDict:pageRef];
		int n = CGPDFDocumentGetNumberOfPages(pdfDoc);
		for (int i = 1; i <= n; i++) {
			CGPDFPageRef p = CGPDFDocumentGetPage(pdfDoc, i);
			CGPDFDictionaryRef pageDict = CGPDFPageGetDictionary(p);
			CGPDFInteger contentStreamLength = [self lengthForContentStreamsInPageDict:pageDict];
			if (pageRefContentStreamLength == contentStreamLength) {
				return i;
			}
		}

        // !!!!!
        // Don't try to print out the pageRef. This is an arbitrary PDF pointer and might just be broken.
        // PSPDFLogWarning(@"Page for reference %@ not found", pageRef);
	}
	return pageNumber;
}

+ (CGPDFInteger)lengthForContentStreamsInPageDict:(CGPDFDictionaryRef)pageRef {
	CGPDFInteger contentLength = 0;
	CGPDFStreamRef contentStream = NULL;
	bool hasContentStream = CGPDFDictionaryGetStream(pageRef, "Contents", &contentStream);
	if (hasContentStream) {
		CGPDFDictionaryRef contentStreamDict = CGPDFStreamGetDictionary(contentStream);
		CGPDFInteger contentStreamLength = 0;
		CGPDFDictionaryGetInteger(contentStreamDict, "Length", &contentStreamLength);
		contentLength = contentStreamLength;
	} else {
		CGPDFArrayRef contentStreamArray = NULL;
		bool hasContentStreamArray = CGPDFDictionaryGetArray(pageRef, "Contents", &contentStreamArray);
		if (hasContentStreamArray) {
			CGPDFInteger numberOfContentStreams = CGPDFArrayGetCount(contentStreamArray);
			for (int i=0; i<numberOfContentStreams; i++) {
				CGPDFArrayGetStream(contentStreamArray, i, &contentStream);
				if (contentStream != NULL) {
					CGPDFDictionaryRef contentStreamDict = CGPDFStreamGetDictionary(contentStream);
					CGPDFInteger contentStreamLength = 0;
					CGPDFDictionaryGetInteger(contentStreamDict, "Length", &contentStreamLength);
					contentLength += contentStreamLength;
				}
			}
		}
	}
	return contentLength;
}

// Slower fallback, in case a named destination isn't found by searching the name tree,
// which likely happens because the limits of the tree nodes aren't correct. (should be rare)
+ (int)resolveNamedDestinationByTraversingCompleteTree:(NSString *)destinationName documentRef:(CGPDFDocumentRef)documentRef {
    CGPDFDictionaryRef catalog = CGPDFDocumentGetCatalog(documentRef);
    CGPDFDictionaryRef namesRef = NULL;
    CGPDFDictionaryGetDictionary(catalog, "Names", &namesRef);
    if (namesRef != NULL) {
        CGPDFDictionaryRef destsRef = NULL;
        CGPDFDictionaryGetDictionary(namesRef, "Dests", &destsRef);
        if (destsRef != NULL) {
            NSMutableDictionary *namedDestinations = [NSMutableDictionary dictionary];
            [self parseNamedDestinationsWithRoot:destsRef inDictionary:namedDestinations doc:documentRef];
            if (namedDestinations[destinationName]) {
                return [namedDestinations[destinationName] intValue];
            }
        }
    }
    return 0;
}

// Helper for resolveNamedDestinationByTraversingCompleteTree fallback.
+ (void)parseNamedDestinationsWithRoot:(CGPDFDictionaryRef)root inDictionary:(NSMutableDictionary *)pagesByDestName doc:(CGPDFDocumentRef)pdfDoc {
	CGPDFArrayRef namesRef = NULL;
	CGPDFDictionaryGetArray(root, "Names", &namesRef);
	if (namesRef != NULL) {
		NSString *key = nil;
		for (int i=0; i<CGPDFArrayGetCount(namesRef); i++) {
			if (i % 2 == 0) {
				// key
				CGPDFStringRef keyRef = NULL;
				CGPDFArrayGetString(namesRef, i, &keyRef);
				CFStringRef keyStringRef = CGPDFStringCopyTextString(keyRef);
				key = [NSString stringWithString:(__bridge NSString *)keyStringRef];
				CFRelease(keyStringRef);
			} else {
				// value
				CGPDFDictionaryRef valueRef = NULL;
				CGPDFArrayGetDictionary(namesRef, i, &valueRef);
				if (valueRef != NULL) {
					CGPDFArrayRef dRef = NULL;
					CGPDFDictionaryGetArray(valueRef, "D", &dRef);
					if (dRef != NULL) {
						CGPDFDictionaryRef pageRef = NULL;
						CGPDFArrayGetDictionary(dRef, 0, &pageRef);
						if (pageRef != NULL) {
							int pageIndex = [self pageNumberForPageTreeNode:pageRef doc:pdfDoc];
							pagesByDestName[key] = @(pageIndex);
						}
					}
				} else {
					CGPDFArrayRef valueArrayRef = NULL;
					bool valueIsArray = CGPDFArrayGetArray(namesRef, i, &valueArrayRef);
					if (valueIsArray) {
						CGPDFDictionaryRef pageRef = NULL;
						CGPDFArrayGetDictionary(valueArrayRef, 0, &pageRef);
						if (pageRef != NULL) {
							int pageIndex = [self pageNumberForPageTreeNode:pageRef doc:pdfDoc];
							pagesByDestName[key] = @(pageIndex);
						}
					}
				}
			}
		}
	}
	CGPDFArrayRef kidsRef = NULL;
	CGPDFDictionaryGetArray(root, "Kids", &kidsRef);
	for (int i=0; i<CGPDFArrayGetCount(kidsRef); i++) {
		CGPDFDictionaryRef kidRef = NULL;
		CGPDFArrayGetDictionary(kidsRef, i, &kidRef);
		[self parseNamedDestinationsWithRoot:kidRef inDictionary:pagesByDestName doc:pdfDoc];
	}
}

@end
