//
//  PSPDFOutlineElement.m
//  PSPDFKit
//
//  Copyright 2011-2012 Peter Steinberger. All rights reserved.
//

#import "PSPDFOutlineElement.h"

@implementation PSPDFOutlineElement

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (id)initWithTitle:(NSString *)title page:(NSUInteger)page relativePath:(NSString *)relativePath children:(NSArray *)children level:(NSUInteger)level {
    if ((self = [super initWithPage:page])) {
        _title = [title copy];
        _relativePath = relativePath;
        _level = level;
        _children = [children copy];
    }
    return self;
}

- (NSUInteger)hash {
    return [super hash] * 31 + [_title hash] * 31 + _level * 31 + [_relativePath hash] * 31;
}

- (BOOL)isEqualToBookmark:(PSPDFBookmark *)otherBookmark {
    if ([otherBookmark isKindOfClass:[self class]]) {
        PSPDFOutlineElement *otherOutlineElement = (PSPDFOutlineElement *)otherBookmark;
        if (![super isEqualToBookmark:otherOutlineElement]) {
            return NO;
        }

        if ([_title isEqual:otherOutlineElement.title] && _level == otherOutlineElement.level && [_children isEqual:otherOutlineElement.children] && (_relativePath == otherOutlineElement.relativePath || [_relativePath isEqual:otherOutlineElement.relativePath])) {
            return YES;
        }
    }
    return NO;
}

- (NSString *)description {
    return [NSString stringWithFormat:@"<%@ title:%@ page:%d level:%d children:#%d, relativeDocumentPath:%@>", NSStringFromClass([self class]), self.title, self.page, self.level, [self.children count], self.relativePath];
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public

- (void)addOpenChildren:(NSMutableArray *)list {
	[list addObject:self];
	if (_expanded) {
		for (PSPDFOutlineElement *child in _children) {
			[child addOpenChildren:list];
		}
	}
}

- (NSArray *)flattenedChildren {
	NSMutableArray *flatList = [NSMutableArray array];
	for (PSPDFOutlineElement *child in _children) {
		[child addOpenChildren:flatList];
	}
	return [flatList copy];
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSCopying

- (id)copyWithZone:(NSZone *)zone {
    PSPDFOutlineElement *outlineElement = [[[self class] alloc] initWithTitle:_title page:self.page relativePath:_relativePath children:_children level:_level];
    return outlineElement;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSCoding

- (id)initWithCoder:(NSCoder *)coder {
    NSString *title = [coder decodeObjectForKey:NSStringFromSelector(@selector(title))];
    NSUInteger page = [coder decodeIntegerForKey:NSStringFromSelector(@selector(page))];
    NSString *relativePath = [coder decodeObjectForKey:NSStringFromSelector(@selector(relativePath))];
    NSUInteger level = [coder decodeIntegerForKey:NSStringFromSelector(@selector(level))];
    NSArray *children = [coder decodeObjectForKey:NSStringFromSelector(@selector(children))];
    self = [self initWithTitle:title page:page relativePath:relativePath children:children level:level];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)coder {
    [super encodeWithCoder:coder];
    [coder encodeObject:_title forKey:NSStringFromSelector(@selector(title))];
    [coder encodeObject:_relativePath forKey:NSStringFromSelector(@selector(relativePath))];
    [coder encodeInteger:_level forKey:NSStringFromSelector(@selector(level))];
    [coder encodeObject:_children forKey:NSStringFromSelector(@selector(children))];
}

@end
