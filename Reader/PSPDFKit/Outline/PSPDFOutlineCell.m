//
//  PSPDFOutlineCell.m
//  PSPDFKit
//
//  Copyright (c) 2012 Peter Steinberger. All rights reserved.
//

#import "PSPDFOutlineCell.h"
#import "PSPDFOutlineElement.h"
#import "PSPDFIconGenerator.h"

@interface PSPDFOutlineCell()
@property (nonatomic, strong) UIButton *disclosureButton;
@property (nonatomic, assign) CGFloat outlineIntentLeftOffset;
@property (nonatomic, assign) CGFloat outlineIndentMultiplicator;
@end

@implementation PSPDFOutlineCell

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
	if ((self = [super initWithStyle:style reuseIdentifier:reuseIdentifier])) {

        _outlineIntentLeftOffset = 32.f;
        _outlineIndentMultiplicator = 15.f;

        // create expandable button
		_disclosureButton = [UIButton buttonWithType:UIButtonTypeCustom];
		[_disclosureButton setImage:[[PSPDFIconGenerator sharedGenerator] iconForType:PSPDFIconTypeForwardArrow] forState:UIControlStateNormal];
        _disclosureButton.contentMode = UIViewContentModeCenter;
		[_disclosureButton addTarget:self action:@selector(expandOrCollapse) forControlEvents:UIControlEventTouchUpInside];
		[self.contentView addSubview:_disclosureButton];
	}
	return self;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public

- (void)setOutlineElement:(PSPDFOutlineElement *)outlineElement {
    if (outlineElement != _outlineElement) {
        _outlineElement = outlineElement;
        
        CGFloat outlineIndent = self.outlineIndentMultiplicator * outlineElement.level;
        self.indentationWidth = self.outlineIntentLeftOffset + outlineIndent;
        self.indentationLevel = 1;
        self.textLabel.font = [[self class] fontForOutlineElement:outlineElement];
        self.textLabel.text = outlineElement.title;
        
        self.disclosureButton.frame = CGRectMake(outlineIndent, 0.f, 44.f, self.bounds.size.height);
        self.disclosureButton.autoresizingMask = UIViewAutoresizingFlexibleHeight;

        /*
        // for debugging the real margins of the textLabel
        dispatch_async(dispatch_get_main_queue(), ^{
            CGFloat assumedWidth = self.bounds.size.width - (kPSPDFOutlineIntentLeftOffset + kPSPDFOutlineIndentMultiplicator * outlineElement.level + 20);
            if (self.textLabel.frame.size.width != assumedWidth) {
                NSLog(@"assumed: %f, real: %f", assumedWidth, self.textLabel.frame.size.width);
            }
        });
         */
    }
    
    // always update button state (this might change after a reloadData)
    [self updateOutlineButton];
}

+ (CGFloat)heightForCellWithOutlineElement:(PSPDFOutlineElement *)outlineElement constrainedToSize:(CGSize)constraintSize outlineIntentLeftOffset:(CGFloat)leftOffset outlineIntentMultiplicator:(CGFloat)multiplicator {
    constraintSize.width -= leftOffset + multiplicator * outlineElement.level + 20; // 20 based on testing; additional margin from UIKit.
    UIFont *font = [self fontForOutlineElement:outlineElement];
    CGSize labelSize = [outlineElement.title sizeWithFont:font constrainedToSize:constraintSize lineBreakMode:UILineBreakModeTailTruncation];

    return fmaxf(44, labelSize.height + 26.f); // default margin?
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Private

+ (UIFont *)fontForOutlineElement:(PSPDFOutlineElement *)outlineElement {
    return (outlineElement.level == 0) ? [UIFont boldSystemFontOfSize:17.0f] : [UIFont boldSystemFontOfSize:15.0f];
}

// Set transform according to expansion state.
- (void)updateOutlineButton {
    if (_outlineElement.isExpanded) {
        self.disclosureButton.transform = CGAffineTransformMakeRotation(M_PI / 2);
    } else {
        self.disclosureButton.transform = CGAffineTransformIdentity;
    }

    // hide button if no child elements
    self.disclosureButton.hidden = _outlineElement.children.count == 0;
}

- (void)expandOrCollapse {
    // call delegate (who might destroy the cell in reloadData)
    [self.delegate outlineCellDidTapDisclosureButton:self];
}

@end
