//
//  PSPDFDocumentProvider.m
//  PSPDFKit
//
//  Copyright (c) 2011-2012 Peter Steinberger. All rights reserved.
//

#import "PSPDFDocumentProvider.h"
#import "PSPDFKitGlobal.h"
#import "PSPDFKitGlobal+Internal.h"
#import "PSPDFViewController+Internal.h"
#import "PSPDFDocumentParser.h"
#import "PSPDFOutlineParser.h"
#import "PSPDFTextParser.h"
#import "PSPDFLabelParser.h"
#import "PSPDFAnnotationParser.h"
#import "PSPDFDocument.h"
#import "PSPDFConverter.h"
#import "PSPDFPageInfo.h"
#import "UIColor+PSPDFKitAdditions.h"
#import <libkern/OSAtomic.h>
#import <pthread.h>

@interface PSPDFDocument (PSPDFDocumentDataWriteSupport)
@property (nonatomic, copy) NSArray *dataArray;
@end

@interface PSPDFDocumentParser (PSPDFDocumentProviderInternal)
@property (nonatomic, weak) PSPDFDocumentProvider *documentProvider;
@end

@interface PSPDFOutlineParser (PSPDFDocumentProviderInternal)
@property (nonatomic, weak) PSPDFDocumentProvider *documentProvider;
@end

@interface PSPDFAnnotationParser (PSPDFDocumentProviderInternal)
@property (nonatomic, weak) PSPDFDocumentProvider *documentProvider;
@end

@interface PSPDFLabelParser (PSPDFDocumentProviderInternal)
@property (nonatomic, weak) PSPDFDocumentProvider *documentProvider;
@end

// Tuple to save the reference count of the pageRefs.
@interface PSPDFPageTracker : NSObject
@property (nonatomic, strong) id pageRef; // CGPDFPageRef
@property (nonatomic, assign) NSUInteger page;
@property (nonatomic, assign) NSUInteger refCount;
@end

@interface PSPDFDocumentProvider() {
    BOOL _needsFlushing:1; // low memory, recreate cache!
    BOOL _parsed:1;
    BOOL _documentCreationFailed:1;
    BOOL _skipEncryptionFilterSearch:1;
    BOOL _documentRefIsUnlocked:1;
    NSMutableDictionary *_openPages;
    NSMutableDictionary *_textParsers;
    NSMutableDictionary *_fontCache;
    NSMutableDictionary *_pageInfoCache;
    NSDictionary *_metadata;
    NSMutableArray *_ownerArray;
#ifdef DEBUG
    NSMutableArray *_ownerStackTraceArray;
#endif
    dispatch_queue_t _documentQueue;
    dispatch_queue_t _pageInfoQueue;
    dispatch_queue_t _textParserCacheQueue;
    dispatch_queue_t _textParserParseQueue;
    pthread_mutex_t _propertyInitLock;
    pthread_mutex_t _documentParsingLock;
}
@property (nonatomic, assign) CGPDFDocumentRef documentRef;
@property (nonatomic, assign) NSUInteger documentRefRetainCount;
@end

@implementation PSPDFDocumentProvider

NSString *PSPDFKCloseCachedDocumentRefNotification = @"PSPDFKCloseCachedDocumentRefNotification";
static volatile int32_t _openDocumentRefCount = 0;

@synthesize fileURL = _fileURL;
@synthesize pageCount = _pageCount;
@synthesize allowsPrinting = _allowsPrinting, isEncrypted = _isEncrypted;
@synthesize isLocked = _isLocked, allowsCopying = _allowsCopying;
@synthesize documentParser = _documentParser;
@synthesize outlineParser = _outlineParser;
@synthesize annotationParser = _annotationParser;
@synthesize labelParser = _labelParser;
@synthesize title = _title;

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (id)initWithDocument:(PSPDFDocument *)document {
    if ((self = [super init])) {
        _canEmbedAnnotations = YES; // be optimistic by default :)
        _ownerArray = CFBridgingRelease(CFArrayCreateMutable(NULL, 0, NULL));
#ifdef DEBUG
        _ownerStackTraceArray = [NSMutableArray new];
#endif
        _document = document;
        _openPages = [NSMutableDictionary new];
        _textParsers = CFBridgingRelease(CFDictionaryCreateMutable(NULL, 0, NULL, &kCFTypeDictionaryValueCallBacks));
        _fontCache = [NSMutableDictionary new];
        _documentQueue = pspdf_dispatch_queue_create("com.PSPDFKit.documentQueue", 0);
        _pageInfoQueue = pspdf_dispatch_queue_create("com.PSPDFKit.pageInfoQueue", DISPATCH_QUEUE_CONCURRENT);
        _textParserCacheQueue = pspdf_dispatch_queue_create("com.PSPDFKit.textParserCacheQueue", DISPATCH_QUEUE_CONCURRENT);
        _textParserParseQueue = pspdf_dispatch_queue_create("com.PSPDFKit.textParserParseQueue", 0);
        pthread_mutex_init(&_documentParsingLock, NULL);
        pthread_mutex_init(&_propertyInitLock, NULL);

        // pageInfoCache uses a special dict that doesn't need boxing
        _pageInfoCache = CFBridgingRelease(CFDictionaryCreateMutable(NULL, 0, NULL, &kCFTypeDictionaryValueCallBacks));

        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didReceiveMemoryWarning) name:UIApplicationDidReceiveMemoryWarningNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(closeCachedDocumentRefNotification:) name:PSPDFKCloseCachedDocumentRefNotification object:nil];
    }
    return self;
}

- (id)initWithFileURL:(NSURL *)fileURL document:(PSPDFDocument *)document {
    if ((self = [self initWithDocument:document])) {
        _fileURL = fileURL;
    }
    return self;
}

- (id)initWithData:(NSData *)data document:(PSPDFDocument *)document {
    if ((self = [self initWithDocument:document])) {
        _data = data;
        _dataProvider = CGDataProviderCreateWithCFData((__bridge CFDataRef)data);
    }
    return self;
}

- (id)initWithDataProvider:(CGDataProviderRef)dataProvider document:(PSPDFDocument *)document {
    if ((self = [self initWithDocument:document])) {
        _dataProvider = CGDataProviderRetain(dataProvider);
    }
    return self;
}

- (void)dealloc {
    if ([_openPages count]) PSPDFLogWarning(@"There are open pages. You forgot calling releasePageRef: %@", self);
    if (self.documentRefRetainCount > 0) {
        PSPDFLogWarning(@"The documentRef is still referenced somewhere (%d - %@)", self.documentRefRetainCount, _ownerArray);
#ifdef DEBUG
        PSPDFLogWarning(@"Stack Traces: %@", _ownerStackTraceArray);
#endif
    }

    NSAssert([_openPages count] == 0, @"There are open pages. You forgot calling releasePageRef: %@", self);
    NSAssert(self.documentRefRetainCount == 0, @"The documentRef is still referenced somewhere!");

    [[NSNotificationCenter defaultCenter] removeObserver:self];

    CGDataProviderRelease(_dataProvider);

    // nil out any document provider classes
    _outlineParser.documentProvider = nil;
    _documentParser.documentProvider = nil;
    _annotationParser.documentProvider = nil;
    _labelParser.documentProvider = nil;

    // *every* access to _documentRef needs to be locked.
    dispatch_barrier_sync(_documentQueue, ^{
        // deallocating the document can be slow; make sure we use the background thread for it.
        // But keep main deallocation for older devices that have more memory pressure.
        if (self.documentRef) {
            CGPDFDocumentRef documentRef = _documentRef;
            BOOL deallocAsync = [NSThread mainThread] && !PSPDFIsCrappyDevice();
            pspdf_dispatch_async_if(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), deallocAsync, ^{
                // don't ever refernce self or ivars here, else we do resurrection -> crash.
                CGPDFDocumentRelease(documentRef);
            });
        }
    });
    PSPDFDispatchRelease(_documentQueue);
    PSPDFDispatchRelease(_textParserCacheQueue);
    PSPDFDispatchRelease(_textParserParseQueue);
    PSPDFDispatchRelease(_pageInfoQueue);
    pthread_mutex_destroy(&_documentParsingLock);
    pthread_mutex_destroy(&_propertyInitLock);
}

- (NSString *)description {
    NSMutableString *source = [NSMutableString string];
    if (_fileURL) {
        [source appendFormat:@"fileURL:%@ ", [_fileURL path]];
    }
    if (_data) {
        [source appendFormat:@"data length: %d ", [_data length]];
    }
    // data creates a dataProvider...
    if (_dataProvider && !_data) {
        [source appendFormat:@"dataProvider: %@ ", _dataProvider];
    }
    NSString *openDocumentRef = self.documentRef ? [NSString stringWithFormat:@"Open documentRef with %d references, ", self.documentRefRetainCount] : @"";
    NSString *description = [NSString stringWithFormat:@"<%@ %p: %@%@pageCount:%d passwordSet:%@>", NSStringFromClass([self class]), self, openDocumentRef, source, self.pageCount, [self.password length] ? @"YES" : @"NO"];

    return description;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Document Reference Code

// ensure with the atomic check that we don't open threads like crazy for cache clearing.
static volatile int32_t _closeCachedDocumentNotificationActive = 0;
- (CGPDFDocumentRef)requestDocumentRefWithOwner:(id)owner {
    // request that other document providers clean up their cache.
    // call async so that we don't deadlock.
    // TODO: still messy.
    if (_closeCachedDocumentNotificationActive == 0 && (_openDocumentRefCount >= (kPSPDFLowMemoryMode ? 1 : kPSPDFMaximumNumberOfOpenDocumentRefs))) {
        OSAtomicIncrement32Barrier(&_closeCachedDocumentNotificationActive);
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
            [[NSNotificationCenter defaultCenter] postNotificationName:PSPDFKCloseCachedDocumentRefNotification object:self];
            OSAtomicDecrement32Barrier(&_closeCachedDocumentNotificationActive);
        });
    }

    pspdf_dispatch_sync_reentrant(_documentQueue, ^{
        self.documentRefRetainCount += 1;

        if (owner) {
            [_ownerArray addObject:owner];
#ifdef DEBUG
            [_ownerStackTraceArray addObject:[NSThread callStackSymbols]];
#endif
        }

        // don't re-open documentRef if we are already open.
        if (!self.documentRef) {
            if (_documentCreationFailed) {
                // Parsing already failed, don't try again because it may take a long time to fail.
                PSPDFLogWarning(@"Skipping document creation because it failed eariler.");
                return;
            }
            
            if (self.fileURL) {
                self.documentRef = CGPDFDocumentCreateWithURL((__bridge CFURLRef)self.fileURL);
            }else {
                // NSData opens a dataProvider on init.
                self.documentRef = CGPDFDocumentCreateWithProvider(self.dataProvider);
            }

            if (!self.documentRef) {
                _documentCreationFailed = YES;
                // if we print self here, that would be recursive.
                PSPDFLogError(@"Failed to create documentRef in %@ (file: %@)", NSStringFromClass([self class]), [self.fileURL lastPathComponent]);
                if ([self.fileURL isFileURL] && ![[NSFileManager defaultManager] fileExistsAtPath:[self.fileURL path]]) {
                    PSPDFLogError(@"File at %@ does not exist.", [self.fileURL path]);
                }
            }else {
                // increase global counter if documentRef has been created
                OSAtomicIncrement32(&_openDocumentRefCount);

                // try to unlock with a password
                if (_isLocked && self.password) {
                    const char *convertedPassword = [self.password cStringUsingEncoding:NSASCIIStringEncoding];
                    if (convertedPassword) {
                        _documentRefIsUnlocked = CGPDFDocumentUnlockWithPassword(_documentRef, convertedPassword);
                        if (!_documentRefIsUnlocked) {
                            PSPDFLogError(@"Warning: failed to unlock %@.", self);
                        }
                    }else {
                        PSPDFLogWarning(@"Password couldn't be converted to ASCII: %@", self.password);
                    }
                }
            }
        }
    });
    return self.documentRef;
}

- (void)releaseDocumentRef:(CGPDFDocumentRef)documentRef withOwner:(id)owner {
    pspdf_dispatch_sync_reentrant(_documentQueue, ^{
        NSAssert(self.documentRefRetainCount > 0, @"Document retain count is empty?");
        NSAssert(documentRef == self.documentRef, @"DocumentRef must be the same that's saved internally!");
        self.documentRefRetainCount -= 1;

        if (owner) {
            NSUInteger index = [_ownerArray indexOfObjectIdenticalTo:owner];
            if (index != NSNotFound) {
                [_ownerArray removeObjectAtIndex:index];
#ifdef DEBUG
                //            NSLog(@"%@", _ownerStackTraceArray);
                [_ownerStackTraceArray removeObjectAtIndex:index];
#endif
            }
        }

        if (self.documentRefRetainCount == 0) {
            // only dealloc if too many document ref's are open.
            if (_openDocumentRefCount > (kPSPDFLowMemoryMode ? 1 : kPSPDFMaximumNumberOfOpenDocumentRefs) || _needsFlushing) {
                [self flushDocumentReference];
            }
        }
    });
}

- (void)flushDocumentReference {
    // every access to documentRef has to be synced with _documentQueue
    pspdf_dispatch_sync_reentrant(_documentQueue, ^{

        // can only be checked here, when locked in _documentQueue
        if (self.documentRefRetainCount == 0 && _documentRef) {

            // CGPDFDocumentRelease can be a slow dog.
            PSPDFLogVerbose(@"FLUSHING CGDocumentRef...............");
            CGPDFDocumentRelease(_documentRef); // Equivalent to `CFRelease(document)', except it doesn't crash

            self.documentRef = nil;
            _documentCreationFailed = NO;
            _documentRefIsUnlocked = NO;
            OSAtomicDecrement32(&_openDocumentRefCount);
            _needsFlushing = NO;
        }else {
            _needsFlushing = YES;
        }
    });
}

- (void)performBlock:(void(^)(PSPDFDocumentProvider *docProvider, CGPDFDocumentRef documentRef))documentRefBlock {
    if (documentRefBlock) {
        CGPDFDocumentRef documentRef = [self requestDocumentRefWithOwner:self];
        documentRefBlock(self, documentRef);
        [self releaseDocumentRef:documentRef withOwner:self];
    }
}

- (void)iterateOverPageRef:(void(^)(PSPDFDocumentProvider *provider, CGPDFDocumentRef documentRef, CGPDFPageRef pageRef, NSUInteger page))pageRefBlock {
    if (pageRefBlock) {
        CGPDFDocumentRef documentRef = [self requestDocumentRefWithOwner:self];
        for (NSUInteger pageNumber = 1; pageNumber <= self.pageCount; pageNumber++) {
            CGPDFPageRef pageRef = CGPDFDocumentGetPage(documentRef, pageNumber);
            pageRefBlock(self, documentRef, pageRef, pageNumber);
        }
        [self releaseDocumentRef:documentRef withOwner:self];
    }
}

- (CGPDFPageRef)requestPageRefForPageNumber:(NSUInteger)page {
    return [self requestPageRefForPageNumber:page error:NULL];
}

/// Requests a page for the current loaded document. Needs to be returned in releasePageRef.
- (CGPDFPageRef)requestPageRefForPageNumber:(NSUInteger)page error:(NSError **)error {
    __block CGPDFPageRef pageRef = nil;

    pspdf_dispatch_sync_reentrant(_documentQueue, ^{
        // for the first page ref, request to open the document. We close it if the last page is freed.
        BOOL justOpenedRef = NO;
        if ([_openPages count] == 0) {
            justOpenedRef = YES;
            [self requestDocumentRefWithOwner:nil];
        }

        if (!_documentRef) {
            NSString *errorString = [NSString stringWithFormat:@"documentRef is nil; cannot get pageRef for page %d.", page];
            PSPDFLogWarning(@"%@", errorString);
            PSPDFError(PSPDFErrorCodeUnableToGetPageReference, errorString, error);
        }else {
#ifndef __clang_analyzer__
            PSPDFPageTracker *pageTracker = _openPages[@(page)];
            pageRef = (__bridge CGPDFPageRef)(pageTracker.pageRef);
#endif
            if (pageRef) {
                pageTracker.refCount++;
            }
            if (!pageRef) {
                pageRef = CGPDFDocumentGetPage(_documentRef, page);
                if (!pageRef) {
                    PSPDFLogWarning(@"Error while getting pageRef for page %d. (documentRef: %@, pageCount:%d (justOpenedRef:%d) %@)", page, _documentRef, self.pageCount, justOpenedRef, self);
                    PSPDFError(PSPDFErrorCodeUnableToGetPageReference, [NSString stringWithFormat:@"CGPDFDocumentGetPage failed for page %d", page], error);
                }else {
                    pageTracker = [PSPDFPageTracker new];
                    pageTracker.pageRef = (__bridge id)(pageRef);
                    pageTracker.refCount = 1;
                    pageTracker.page = page;
                    _openPages[@(page)] = pageTracker;
                }
            }
        }
        // if pageRef is nil, close immediately
        if (!pageRef && justOpenedRef) {
            [self releaseDocumentRef:_documentRef withOwner:nil];
        }
    });
    return pageRef;
}

/// Releases a page reference.
- (void)releasePageRef:(CGPDFPageRef)pageRef {
    if (!pageRef) {
        PSPDFLogWarning(@"Tried to clear nil pageRef reference.");
        return;
    }

    NSNumber *pageNumber = @((NSUInteger)CGPDFPageGetPageNumber(pageRef));
    pspdf_dispatch_sync_reentrant(_documentQueue, ^{
        NSAssert(_openPages[pageNumber] != nil, @"Page Number not tracked?");
        PSPDFPageTracker *pageTracker = _openPages[pageNumber];
        if (0 == --pageTracker.refCount) {
            [_openPages removeObjectForKey:pageNumber];
        }

        if ([_openPages count] == 0) {
            [self releaseDocumentRef:_documentRef withOwner:nil];
        }
    });
}

- (BOOL)hasOpenDocumentRef {
    return self.documentRef != nil;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - PageInfo

- (void)fillPageInfoCache {
    // only needed if aspect ratio is *not* equal.
    if (!self.document.aspectRatioEqual) {
        // ensure we're not called more than once at the same time
        __block NSUInteger pageInfoCount;
        dispatch_sync(_pageInfoQueue, ^{
            pageInfoCount = [_pageInfoCache count];
        });

        // only start caching if not everything's cached yet.
        if (pageInfoCount != [self pageCount]) {
            [self iterateOverPageRef:^(PSPDFDocumentProvider *docProvider, CGPDFDocumentRef documentRef, CGPDFPageRef pageRef, NSUInteger pageNumber) {
                NSUInteger zeroBasedPage = pageNumber-1;
                [self pageInfoForPage:zeroBasedPage pageRef:pageRef];
            }];
        }
    }
}
- (PSPDFPageInfo *)pageInfoForPage:(NSUInteger)page {
    return [self pageInfoForPage:page pageRef:nil];
}

- (PSPDFPageInfo *)pageInfoForPage:(NSUInteger)page pageRef:(CGPDFPageRef)pageRef {
    PSPDFDocument *document = self.document;

    if (page >= [self pageCount]) {
        if ([document isValid]) PSPDFLogWarning(@"Invalid page %d, returning nil.", page); // warn if doc is valid
        return nil;
    }

    // if aspect ratio is the same on every page, only parse page once.
    if (document.isAspectRatioEqual) page = 0;

    PSPDFPageInfo *pageInfo = [self pageInfoForPageNoFetching:page];
    if (!pageInfo) {
        // this may block our UI thread - so we really should pre-calculate this.
        @autoreleasepool {
            PSPDFDocumentProvider *documentProvider = nil;
            if (!pageRef) {
                documentProvider = self;
                pageRef = [self requestPageRefForPageNumber:page+1];
            }

            if (!pageRef) {
                PSPDFLog(@"Warning: Failed opening pdf page: %@", pageRef);
            }else {
                // fetch page properties for caching
                int pageRotation = CGPDFPageGetRotationAngle(pageRef);
                CGRect pageRect = CGPDFPageGetBoxRect(pageRef, document.PDFBox);

                // cache rect and rotation for faster lookup
                pageInfo = [[[document classForClass:[PSPDFPageInfo class]] alloc] initWithPage:page rect:pageRect rotation:pageRotation documentProvider:self];
                dispatch_barrier_sync(_pageInfoQueue, ^{
                    CFDictionarySetValue((__bridge CFMutableDictionaryRef)_pageInfoCache, (void *)page, (__bridge const void *)pageInfo);
                });
                [documentProvider releasePageRef:pageRef];
            }
        }
    }
    return pageInfo;
}

- (PSPDFPageInfo *)pageInfoForPageNoFetching:(NSUInteger)page {
    __block PSPDFPageInfo *pageInfo = nil;
    dispatch_sync(_pageInfoQueue, ^{ // abused to lock _pageInfoCache
        pageInfo = (__bridge PSPDFPageInfo *)CFDictionaryGetValue((__bridge CFDictionaryRef)_pageInfoCache, (void *)page);
    });
    return pageInfo;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public

- (void)setDocumentRefRetainCount:(NSUInteger)documentRefRetainCount {
    if (documentRefRetainCount != _documentRefRetainCount) {

        // as the documentProvider tracks the CGDocumentRef
        // it can't be deallocated until all references are nilled out.
        if (_documentRefRetainCount == 0 && documentRefRetainCount == 1) {
            //            PSPDFLogVerbose(@"Locking DocumentProvider: %@", self);
            CFRetain((__bridge CFTypeRef)(self));
        }else if (documentRefRetainCount == 0) {
            //            PSPDFLogVerbose(@"Releasing DocumentProvider: %@", self);
            CFRelease((__bridge CFTypeRef)(self));
        }
        _documentRefRetainCount = documentRefRetainCount;
    }
}

- (void)setData:(NSData *)data {
    if (data != _data) {

        // set in parent document if this is a replace
        if (data && _data) {
            NSMutableArray *newDataArray = [self.document.dataArray mutableCopy];
            NSUInteger position = [newDataArray indexOfObjectIdenticalTo:_data];
            if (position != NSNotFound) {
                [newDataArray removeObjectAtIndex:position];
                [newDataArray insertObject:data atIndex:position];
                self.document.dataArray = newDataArray;
                PSPDFLog(@"Replaced data array. Old length: %d, new length: %d.", [_data length], [data length]);
            }else {
                PSPDFLogWarning(@"Failed to update document's dataArray, data not found.");
            }
        }

        _data = data;

        // flush any open documentRef (since the source has changed)
        [self flushDocumentReference];

        // as data is changed, we need to re-open the data provider
        CGDataProviderRelease(_dataProvider);
        _dataProvider = CGDataProviderCreateWithCFData((__bridge CFDataRef)data);
    }
}

- (NSData *)dataRepresentationWithError:(NSError **)error {
    NSData *data = self.data;
    if (self.dataProvider && !self.data) {
        data = CFBridgingRelease(CGDataProviderCopyData(self.dataProvider));
        if (!data) {
            PSPDFLogError(@"Failed to convert CGDataProvider into NSData - too big?");
            PSPDFError(PSPDFErrorCodeUnableToConvertToDataRepresentation, @"Failed to convert CGDataProvider into NSData - too big?", error);
        }
    }
    if (self.fileURL) {
        NSError *fileError;
        data = [NSData dataWithContentsOfURL:self.fileURL options:NSDataReadingMappedIfSafe error:&fileError];
        if (!data) {
            NSString *errorStr = [NSString stringWithFormat:@"Failed to load file %@: %@", [self.fileURL path], [fileError localizedDescription]];
            PSPDFLogWarning(@"%@", errorStr);
            PSPDFErrorWithUnderlyingError(PSPDFErrorCodeUnableToConvertToDataRepresentation, errorStr, fileError, error);
        }
    }
    return data;
}

// returns converted pdf metadata.
// see http://pdf.editme.com/pdfua-docinfodictionary for a list of available properties.
- (NSDictionary *)metadata {
    if (self.isLocked) return nil;

    if (!_metadata) {
        pspdf_dispatch_sync_reentrant(_documentQueue, ^{ // use for locking
            [self performBlock:^(PSPDFDocumentProvider *docProvider, CGPDFDocumentRef documentRef) {
                _metadata = PSPDFConvertPDFDictionary(CGPDFDocumentGetInfo(documentRef));
            }];
        });
    }
    return _metadata;
}

- (BOOL)isMetadataLoaded {
    return _metadata != nil;
}

- (NSString *)title {
    __block NSString *title = _title;
    // if title is not set, try to load it from the pdf
    if (!title) {
        pspdf_dispatch_sync_reentrant(_documentQueue, ^{ // lock and make a threadsafe query of the title
            @autoreleasepool {
                if (!self.isLocked) {
                    NSDictionary *metadata = [self metadata];

                    if (metadata) {
                        PSPDFLogVerbose(@"Title for %@ is not set. Looking in the pdf metadata...", self);
                        // check for "untitled" and use it as if title isn't set.
                        NSString *metadataTitle = metadata[@"Title"];
                        if (![[metadataTitle lowercaseString] hasPrefix:@"untitled"]) {
                            title = metadataTitle;
                        }
                    }
                }

                // fallback to filename if title was not found
                title = [title length] ? title : [_fileURL lastPathComponent];

                // ensure there's a title, even with data PDF's.
                if ([title length] == 0 && _data) {
                    title = PSPDFLocalize(@"Unnamed PDF");
                }

                // remove ".pdf" in the title.
                title = PSPDFStripPDFFileType(title);

                if (!self.isLocked) {
                    _title = title;
                }else {
                    // return early with title, but that one is not final
                    title = title;
                }
            }
        });
    }
    return title;
}

- (NSString *)encryptionFilter {
    __block NSString *encryptionFilter = _documentParser.encryptionFilter;

    // TODO: add some property for this? Searching for the encryptionFilter is slow.
    /*
    // don't unneccessarily start parsing
    if (!_documentParser && !_skipEncryptionFilterSearch) {
        [self performBlock:^(PSPDFDocumentProvider *docProvider, CGPDFDocumentRef documentRef) {
            size_t numberOfPages = CGPDFDocumentGetNumberOfPages(documentRef);
            if (numberOfPages > 0) {
                CGPDFDictionaryRef infoDictRef = CGPDFDocumentGetInfo(documentRef);
                if (!infoDictRef) {
                    // something fishy is going on. Adobe LifeCycle DRM?
                    encryptionFilter = self.documentParser.encryptionFilter;
                }else {
                    _skipEncryptionFilterSearch = YES;
                }
            }
        }];
    }*/
    
    return encryptionFilter;
}

- (BOOL)hasLoadedTextParserForPage:(NSUInteger)page {
    __block PSPDFTextParser *textParser = nil;
    dispatch_sync(_textParserCacheQueue, ^{ // concurrent queue, fast
        textParser = (__bridge PSPDFTextParser *)CFDictionaryGetValue((__bridge CFDictionaryRef)_textParsers, (void *)page);
    });
    return textParser != nil;
}

- (PSPDFTextParser *)textParserForPage:(NSUInteger)page {
    __block PSPDFTextParser *textParser = nil;
    dispatch_sync(_textParserCacheQueue, ^{ // concurrent queue, fast path
        textParser = (__bridge PSPDFTextParser *)CFDictionaryGetValue((__bridge CFDictionaryRef)_textParsers, (void *)page);
    });

    // if parser isn't there, start parsing.
    if (!textParser) {
        PSPDFDocument *document = self.document; // we need a document to parse.
        if (!document) return nil;

        PSPDFLogVerbose(@"Creating text parser for page %d.", page);
        dispatch_sync(_textParserParseQueue, ^{ // concurrent queue, fast.

            // check again to be sure we're not parsing multiple times
            dispatch_sync(_textParserCacheQueue, ^{ // concurrent queue, fast
                textParser = (__bridge PSPDFTextParser *)CFDictionaryGetValue((__bridge CFDictionaryRef)_textParsers, (void *)page);
            });

            if (!textParser) {
                CGPDFPageRef pageRef = [self requestPageRefForPageNumber:page+1];
                if (pageRef) {
                    textParser = [[[document classForClass:[PSPDFTextParser class]] alloc] initWithPDFPage:pageRef page:page document:document fontCache:_fontCache hideGlyphsOutsidePageRect:document.textParserHideGlyphsOutsidePageRect PDFBox:document.PDFBox];
                    [self releasePageRef:pageRef];
                }else {
                    PSPDFLogWarning(@"pageRef nil; cannot parse page %d.", page);
                }
            }
        });

        // save if parser is available
        if (textParser) {
            dispatch_barrier_async(_textParserCacheQueue, ^{
                CFDictionarySetValue((__bridge CFMutableDictionaryRef)_textParsers, (void *)page, (__bridge const void *)textParser);
            });
        }
    }
    return textParser;
}

- (PSPDFOutlineParser *)outlineParser {
    pthread_mutex_lock(&_propertyInitLock);
    if (!_outlineParser) {
        _outlineParser = [[[self.document classForClass:[PSPDFOutlineParser class]] alloc] initWithDocumentProvider:self];
    }
    pthread_mutex_unlock(&_propertyInitLock);
    return _outlineParser;
}

- (PSPDFAnnotationParser *)annotationParser {
    pthread_mutex_lock(&_propertyInitLock);
    if (!_annotationParser) {
        _annotationParser = [[[self.document classForClass:[PSPDFAnnotationParser class]] alloc] initWithDocumentProvider:self];
    }
    pthread_mutex_unlock(&_propertyInitLock);
    return _annotationParser;
}

- (PSPDFLabelParser *)labelParser {
    pthread_mutex_lock(&_propertyInitLock);
    if (!_labelParser) {
        _labelParser = [[[self.document classForClass:[PSPDFLabelParser class]] alloc] initWithDocumentProvider:self];
    }
    pthread_mutex_unlock(&_propertyInitLock);
    return _labelParser;
}

- (BOOL)isDocumentParserLoaded {
    return _documentParser != nil;
}

- (PSPDFDocumentParser *)documentParser {
    pthread_mutex_lock(&_documentParsingLock);
    if (!_documentParser) {
        // TODO: change API to allow data PDF's.
        PSPDFDocumentParser *documentParser = [[[self.document classForClass:[PSPDFDocumentParser class]] alloc] initWithDocumentProvider:self];
        NSError *error;
        BOOL parsingSuccess = [documentParser parseDocumentWithError:&error];
        if (!parsingSuccess || _pageCount != [documentParser.pageObjectNumbers count]) {
            // parsing obviously will fail if this feature is disabled, but we don't need to warn the user about that.
#ifndef PSPDFKIT_DISABLE_TEXT_SELECTION_FEATURES
            if (parsingSuccess) {
                if (_pageCount > 0 && (!self.data && !self.dataProvider)) {
                    PSPDFLogWarning(@"Discprepancy between our parser and Quartz found: parsed number of pages: %i / actual: %i. Embedding annotations will be disabled.", [documentParser.pageObjectNumbers count], _pageCount);
                }
            }else {
                PSPDFLogWarning(@"Embedding annotations will be disabled. Parser returned error: %@", [error localizedDescription]);
            }
#endif
            // Our parser reports a different number of pages than Quartz,
            // better don't save anything than potentially corrupt the file...
            _canEmbedAnnotations = NO;
        }
        _documentParser = documentParser;
    }
    pthread_mutex_unlock(&_documentParsingLock);
    return _documentParser;
}

- (void)setPassword:(NSString *)password {
    _password = password;
    [self unlockWithPassword:password];
}

- (BOOL)unlockWithPassword:(NSString *)password {
    PSPDFLogVerbose(@"unlock with password: %@ (isEncrypted:%d)", password, self.isEncrypted);
    __block BOOL result = YES;

    // don't try to unlock within isLocked-check here or we have a loop.
    if ([password length] > 0 && self.isEncrypted && [self isLockedAndTryToUnlock:NO]) {
        [self performBlock:^(PSPDFDocumentProvider *docProvider, CGPDFDocumentRef documentRef) {
            // PDF stuff works with c-strings. Convert before sending to CGPDF layer.
            const char *convertedPassword = [password cStringUsingEncoding:NSASCIIStringEncoding];
            if (convertedPassword) {
                result = CGPDFDocumentUnlockWithPassword(documentRef, convertedPassword);
                PSPDFLogVerbose(@"called CGPDFDocumentUnlockWithPassword with result %d", result);
            }else {
                PSPDFLogWarning(@"Password couldn't be converted to ASCII: %@", password);
                result = NO;
            }
        }];

        // if succesful, password is saved.
        pspdf_dispatch_sync_reentrant(_documentQueue, ^{
            // ensure we only set _documentRefIsUnlocked if _documentRef is open
            _documentRefIsUnlocked = _documentRef != nil && result;
            if (result) _password = password;
        });
    }
    return result;
}

- (BOOL)isLocked {
    return [self isLockedAndTryToUnlock:YES];
}

- (BOOL)isLockedAndTryToUnlock:(BOOL)tryUnlock {
    [self ensureDocumentIsParsed];
    PSPDFLogVerbose(@"_isLocked:%d _password:%@ _documentRef:%@ _documentRefIsUnlocked:%d tryUnlock:%d", _isLocked, _password, _documentRef, _documentRefIsUnlocked, tryUnlock);

    // if there's an encryption filter
    if (!_isLocked && [[self encryptionFilter] length]) return YES;

    __block BOOL isLocked = _isLocked;
    pspdf_dispatch_sync_reentrant(_documentQueue, ^{
        // if document is already unlocked, ignore the password
        if (!_password || !_isLocked) {
            if ([_password length]) {
                PSPDFLogWarning(@"Password for %@ will be ingored. Document is not locked.", self);
            }
            isLocked = _isLocked;
        }else if (_documentRef) { // TODO: lock documentRef?
            isLocked = !_documentRefIsUnlocked;
        }else if (tryUnlock) {
            // test the password!
            isLocked = ![self unlockWithPassword:_password];
        }
    });
    PSPDFLogVerbose(@"returning:%d", isLocked);
    return isLocked;
}

- (BOOL)isEncrypted {
    [self ensureDocumentIsParsed];
    return _isEncrypted || [[self encryptionFilter] length];
}

- (NSUInteger)pageCount {
    [self ensureDocumentIsParsed];
    return [[self encryptionFilter] length] ? 0 : _pageCount;
}

- (BOOL)canEmbedAnnotations {
    [self documentParser]; // ensure document is parsed to evaluate

    BOOL isWriteableFile = [self.fileURL isFileURL] && [[NSFileManager new] isWritableFileAtPath:[self.fileURL path]];

    // files in the app bundle are writable in the simulator, but won't be on the device (stuff is signed on the device)
    // so manually check for the bundle and disable writing if in there.
#if TARGET_IPHONE_SIMULATOR
    if ([[self.fileURL path] rangeOfString:[[NSBundle mainBundle] bundlePath]].length > 0) {
        PSPDFLogWarning(@"You're writing annotations in the bundle directory. This does work on the Simulator, but is readonly on the device. Copy your PDF file into the Documents directory to make it writeable.");
    }
#endif

    // should we make data writeable?
    BOOL isWriteableData = self.data != nil;

    BOOL canEmbedAnnotations = (isWriteableFile || isWriteableData) && _canEmbedAnnotations && ![self isEncrypted];
    return canEmbedAnnotations;
}

- (BOOL)saveChangedAnnotationsWithError:(NSError **)error {
    BOOL allowExternalSaving = self.document.annotationSaveMode == PSPDFAnnotationSaveModeExternalFile || self.document.annotationSaveMode == PSPDFAnnotationSaveModeEmbeddedWithExternalFileAsFallback;

    if (!self.canEmbedAnnotations && !allowExternalSaving) return NO;

    return [self.annotationParser saveAnnotationsWithError:error];
}

- (BOOL)allowsCopying {
    [self ensureDocumentIsParsed];
    return _allowsCopying;
}

- (void)setAllowsCopying:(BOOL)allowsCopying {
    [self ensureDocumentIsParsed];
    _allowsCopying = allowsCopying;
}

- (BOOL)allowsPrinting {
    [self ensureDocumentIsParsed];
    return _allowsPrinting;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Private

- (void)closeCachedDocumentRefNotification:(NSNotification *)notification {
    if (notification.object == self) return;

    // don't log if this is from an internal notification
    [self flushDocumentReference];
}

- (void)didReceiveMemoryWarning {
    if (_documentRef) {
        PSPDFLog(@"%@ has an open documentRef (%d references). Requesting release.", self, self.documentRefRetainCount);
    }
    [self flushDocumentReference];

    // don't clear all cache, just the textParser (can be a lot)
    dispatch_barrier_async(_textParserCacheQueue, ^{
        [_textParsers removeAllObjects];
    });
}


- (void)ensureDocumentIsParsed {
    pthread_mutex_lock(&_documentParsingLock);
    if (!_parsed) {
        [self parseDocumentProperties];
        _parsed = YES;
    }
    pthread_mutex_unlock(&_documentParsingLock);
}

- (void)parseDocumentProperties {
    [self performBlock:^(PSPDFDocumentProvider *docProvider, CGPDFDocumentRef documentRef) {
        // TODO: does this return something useful if document is locked?
        _pageCount = CGPDFDocumentGetNumberOfPages(documentRef);
        _allowsPrinting = CGPDFDocumentAllowsPrinting(documentRef);
        _allowsCopying = CGPDFDocumentAllowsCopying(documentRef);
        _isEncrypted = CGPDFDocumentIsEncrypted(documentRef);
        _isLocked = !CGPDFDocumentIsUnlocked(documentRef);
    }];
}

@end

@implementation PSPDFPageTracker
- (NSString *)description {
    return [NSString stringWithFormat:@"<%@: %p pageRef:%@ page:%d>", NSStringFromClass([self class]), self, self.pageRef, self.page];
}
@end
