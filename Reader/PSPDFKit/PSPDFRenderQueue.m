//
//  PSPDFRenderQueue.m
//  PSPDFKit
//
//  Copyright (c) 2012 Peter Steinberger. All rights reserved.
//

#import "PSPDFRenderQueue.h"
#import "PSPDFDocument.h"
#import "PSPDFAnnotationParser.h"
#import "PSPDFGlobalLock.h"

@interface PSPDFRenderJob ()
- (id)initWithDocument:(PSPDFDocument *)document forPage:(NSUInteger)page withSize:(CGSize)size clippedToRect:(CGRect)clipRect withAnnotations:(NSArray *)annotations options:(NSDictionary *)options delegate:(id<PSPDFRenderDelegate>)delegate;
@end

@interface PSPDFRenderQueue ()
@property (atomic, strong) PSPDFRenderJob *currentRenderJob;
@end

NSString *kPSPDFAnnotationAutoFetchTypes = @"kPSPDFAnnotationAutoFetchTypes";

@implementation PSPDFRenderQueue {
    dispatch_queue_t _renderQueue;
    dispatch_queue_t _renderQueueSerializer;
    BOOL _isRendering;
    NSMutableArray *_renderQueueJobs;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Singleton

+ (PSPDFRenderQueue *)sharedRenderQueue {
    __strong static PSPDFRenderQueue *_sharedRenderQueue = nil;
    static dispatch_once_t pred;
    dispatch_once(&pred, ^{ _sharedRenderQueue = [[self class] new]; });
    return _sharedRenderQueue;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (id)init {
    if ((self = [super init])) {
        _renderQueue = pspdf_dispatch_queue_create("com.PSPDFKit.renderQueue", 0);
        dispatch_set_target_queue(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), _renderQueue);
        _renderQueueSerializer = pspdf_dispatch_queue_create("com.PSPDFKit.renderQueueSerializer", 0);
        _renderQueueJobs = [NSMutableArray new];
    }
    return self;
}

- (void)dealloc {
    PSPDFDispatchRelease(_renderQueueSerializer);
    [self stopRendering];
}

- (NSString *)description {
    __block NSString *description;
    pspdf_dispatch_sync_reentrant(_renderQueueSerializer, ^{
        description = [NSString stringWithFormat:@"<%@ jobs:%@>", NSStringFromClass([self class]), _renderQueueJobs];
    });
    return description;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public

- (PSPDFRenderJob *)requestRenderedImageForDocument:(PSPDFDocument *)document forPage:(NSUInteger)page withSize:(CGSize)size clippedToRect:(CGRect)clipRect withAnnotations:(NSArray *)annotations options:(NSDictionary *)options delegate:(id<PSPDFRenderDelegate>)delegate {

    PSPDFRenderJob *renderJob = [[PSPDFRenderJob alloc] initWithDocument:document forPage:page withSize:size clippedToRect:clipRect withAnnotations:annotations options:options delegate:delegate];

    // test against crazy sizes.
    if (CGRectIsEmpty(clipRect)) clipRect = CGRectMake(0, 0, size.width, size.height);
    if (CGRectGetWidth(clipRect) > 3000 || CGRectGetHeight(clipRect) > 3000) {
        PSPDFLogWarning(@"clipRect %@ seems larger than what the system can handle. Crash might follow.", NSStringFromCGRect(clipRect));
    }

    dispatch_barrier_async(_renderQueueSerializer, ^{
        if ([_renderQueueJobs count] == 0)  {
            [[PSPDFCache sharedCache] pauseCachingForService:self];
        }

        [_renderQueueJobs addObject:renderJob];
        [self dequeueJob];
    });
    
    return renderJob;
}

- (void)cancelRenderingForDelegate:(id<PSPDFRenderDelegate>)delegate async:(BOOL)async {
    NSMutableIndexSet *indexesToRemove = [NSMutableIndexSet indexSet];
    dispatch_block_t actionBlock = ^{
        NSUInteger i = 0;
        for (PSPDFRenderJob *renderJob in _renderQueueJobs) {
            if (renderJob.delegate == delegate) {
                renderJob.delegate = nil; // remove delegate
                [indexesToRemove addIndex:i];
            }
            i++;
        }
        [_renderQueueJobs removeObjectsAtIndexes:indexesToRemove];

        // resume service?
        [self dequeueJob];
    };

    if (async) {
        dispatch_barrier_async(_renderQueueSerializer, actionBlock);
    }else {
        pspdf_dispatch_sync_reentrant(_renderQueueSerializer, actionBlock);
    }
}

- (void)stopRendering {
    pspdf_dispatch_sync_reentrant(_renderQueueSerializer, ^{
        for (PSPDFRenderJob *renderJob in _renderQueueJobs) {
            renderJob.delegate = nil;
        }
        [_renderQueueJobs removeAllObjects];
        [[PSPDFCache sharedCache] resumeCachingForService:self];
    });
}

- (BOOL)hasRenderJobsForDelegate:(id<PSPDFRenderDelegate>)delegate {
    __block BOOL hasRenderJobs = NO;
    if (self.currentRenderJob.delegate == delegate) {
        hasRenderJobs = YES;
    }
    if (!hasRenderJobs) {
        pspdf_dispatch_sync_reentrant(_renderQueueSerializer, ^{
            for (PSPDFRenderJob *renderJob in _renderQueueJobs) {
                if (renderJob.delegate == delegate) {
                    hasRenderJobs = YES;
                    break;
                }
            }
        });
    }
    return hasRenderJobs;
}

- (NSUInteger)numberOfQueuedJobs {
    __block NSUInteger numberOfQueuedJobs;
    pspdf_dispatch_sync_reentrant(_renderQueueSerializer, ^{
        numberOfQueuedJobs = [_renderQueueJobs count];
    });
    return numberOfQueuedJobs;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Private

- (void)dequeueJob {
    dispatch_barrier_async(_renderQueueSerializer, ^{
        if (_isRendering) return; // stop if we're rendering currently

        // stop and resume caching if there are no more jobs
        if ([_renderQueueJobs count] == 0) { [[PSPDFCache sharedCache] resumeCachingForService:self]; return; }

        // start new rendering job
        PSPDFRenderJob *nextJob = _renderQueueJobs[0];
        _isRendering = YES;
        dispatch_async(_renderQueue, ^{
            // perform if delegate hasn't been deallocated and nilled out yet.
            self.currentRenderJob = nextJob;
            [self performRenderJob:nextJob];
        });
        [_renderQueueJobs removeObjectAtIndex:0];
    });
}

- (void)performRenderJob:(PSPDFRenderJob *)job {
    // enable annotation background-fetching if option is set.
    if (job.delegate) {
        NSArray *annotations = job.annotations;
        NSUInteger annotationTypes = [job.options[kPSPDFAnnotationAutoFetchTypes] unsignedIntegerValue];
        if (annotationTypes) {
            annotations = [job.document annotationsForPage:job.page type:annotationTypes];
        }

        // check again, since getting annotations might take a while.
        if (job.delegate) {
            BOOL lowMemoryMode = kPSPDFLowMemoryMode;
            if (PSPDFIsCrappyDevice() || lowMemoryMode) {
                [[PSPDFGlobalLock sharedGlobalLock] lockGlobal];
            }
            job.renderedImage = [job.document renderImageForPage:job.page withSize:job.fullSize clippedToRect:job.clipRect withAnnotations:annotations options:job.options];

            if (PSPDFIsCrappyDevice() || lowMemoryMode) {
                [[PSPDFGlobalLock sharedGlobalLock] unlockGlobal];
            }
        }
    }

    self.currentRenderJob = nil;
    [self finishRenderJob:job];
}

- (void)finishRenderJob:(PSPDFRenderJob *)job {
    PSPDFLogVerbose(@"job finished: %@", job);
    // perform in sync Don older devices to not render while the image is not yet set.
    pspdf_dispatch_async_if(dispatch_get_main_queue(), !PSPDFIsCrappyDevice() && !kPSPDFLowMemoryMode, ^{
        id<PSPDFRenderDelegate> renderDelegate = job.delegate; // stronly reference weak delegate
        if (renderDelegate) {
            if (renderDelegate && [renderDelegate respondsToSelector:@selector(renderQueue:jobDidFinish:)]) {
                [renderDelegate renderQueue:self jobDidFinish:job];
            }
        }
        job.options = nil; // clear the pdfController set inside.
        job.renderedImage = nil;
    });
	_isRendering = NO;
	[self dequeueJob];
}

@end

@implementation PSPDFRenderJob

- (id)initWithDocument:(PSPDFDocument *)document forPage:(NSUInteger)page withSize:(CGSize)size clippedToRect:(CGRect)clipRect withAnnotations:(NSArray *)annotations options:(NSDictionary *)options delegate:(id)delegate {
    if ((self = [super init])) {
        NSParameterAssert(document);
        NSParameterAssert(delegate);
        _document = document;
        _page = page;
        _fullSize = size;
        _clipRect = clipRect;
        _annotations = annotations;
        _options = options;
        _delegate = delegate;
    }
	return self;
}

- (NSString *)description {
    NSString *optionStr = [[_options description] stringByReplacingOccurrencesOfString:@"\n" withString:@" "];
    return [NSString stringWithFormat:@"<%@ %@ page:%d fullSize:%@ clipRect:%@ options:%@>", NSStringFromClass([self class]), _document.title, _page, NSStringFromCGSize(_fullSize), NSStringFromCGRect(_clipRect), optionStr];
}

@end
