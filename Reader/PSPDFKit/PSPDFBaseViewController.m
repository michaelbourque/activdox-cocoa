//
//  PSPDFBaseViewController.m
//  PSPDFKit
//
//  Copyright (c) 2012 Peter Steinberger. All rights reserved.
//

#import "PSPDFBaseViewController.h"

@implementation PSPDFBaseViewController

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UIViewController

// Deprecated in iOS6, but new default is the same as this.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation {
    return PSIsIpad() ? YES : toInterfaceOrientation != UIInterfaceOrientationPortraitUpsideDown;
}

@end
