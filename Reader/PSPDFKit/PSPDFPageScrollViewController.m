//
//  PSPDFPageScrollViewController.m
//  PSPDFKit
//
//  Copyright (c) 2012 Peter Steinberger. All rights reserved.
//

#import "PSPDFPageScrollViewController.h"
#import "PSPDFViewController.h"
#import "PSPDFViewController+Internal.h"
#import "PSPDFKitGlobal+Internal.h"
#import "PSPDFDocument.h"
#import "PSPDFScrollView.h"
#import "PSPDFPageView.h"
#import "PSPDFPageInfo.h"
#import "PSPDFConverter.h"

@interface PSPDFPagingScrollView : UIScrollView @end
@implementation PSPDFPagingScrollView @end

@interface PSPDFPageScrollViewController () {
    CGRect _oldRect;
    BOOL _isTilingActive:1;
}
@property (nonatomic, assign, getter=isRotationActive) BOOL rotationActive;
@property (nonatomic, assign, getter=isRotationAnimationActive) BOOL rotationAnimationActive;
@property (nonatomic, assign, getter=isFrameChangeActive) BOOL frameChangeActive;
@property (nonatomic, assign) NSUInteger targetPageAfterRotation;
@property (nonatomic, strong) PSPDFPagingScrollView *pagingScrollView;
@property (nonatomic, strong) NSMutableSet *recycledPages;
@property (nonatomic, strong) NSMutableSet *visiblePages;
@end

@implementation PSPDFPageScrollViewController

@synthesize scrollView = _scrollView; // unused; for protocol

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (id)initWithPDFController:(PSPDFViewController *)pdfController {
    if ((self = [super initWithNibName:nil bundle:nil])) {
        self.pdfController = pdfController;
        _pagePadding = pdfController.pagePadding;
        //_pagePaddingForMatchingPagesInSingePageMode = pdfController.pagePaddingForMatchingPagesInSingePageMode;

        _recycledPages = [NSMutableSet new];
        _visiblePages  = [NSMutableSet new];
    }
    return self;
}

- (void)dealloc {
    self.visiblePages = nil; // make sure pages are nilled out.
    self.pdfController = nil;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UIViewController

// UIView is the UIScrollView!
- (void)loadView {
    PSPDFPagingScrollView *pagingScrollView = [[[self.pdfController classForClass:[PSPDFPagingScrollView class]] alloc] initWithFrame:CGRectZero];

    // changing properties
    pagingScrollView.scrollEnabled = self.pdfController.isScrollingEnabled;
    pagingScrollView.pagingEnabled = YES;

    // static stuff
    pagingScrollView.backgroundColor = [UIColor clearColor];
    pagingScrollView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    pagingScrollView.showsVerticalScrollIndicator = NO;
    pagingScrollView.showsHorizontalScrollIndicator = NO;
    pagingScrollView.delegate = self;

    if (kPSPDFDebugScrollViews) {
        pagingScrollView.backgroundColor = [UIColor colorWithRed:0.5f green:0.2f blue:0.f alpha:0.5f];
    }

    self.pagingScrollView = pagingScrollView;
    self.view = pagingScrollView;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];

    // remove all recycled pages
    [self.recycledPages removeAllObjects];
}

// Don't do stuff in willAppear/didAppear; this class needs to be compatible with iOS4.

// track rotation events.
- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    [super willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
    self.rotationActive = YES;
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    [super willAnimateRotationToInterfaceOrientation:toInterfaceOrientation duration:duration];

    // rotation is handled implicit via the setFrame-notification
    if ([self isViewLoaded] && self.view.window) {
        [self updatePagingContentSize];
        [self setPage:self.targetPageAfterRotation animated:NO];
        self.rotationAnimationActive = YES; // important to only enable the flag here (or rotate animation and delegates freak out)
        [self tilePagesForced:YES];
    }
    self.rotationAnimationActive = NO;
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
    [super didRotateFromInterfaceOrientation:fromInterfaceOrientation];
    self.rotationActive = NO;
}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];

    if (!CGRectEqualToRect(_oldRect, self.view.frame)) {
        _oldRect = self.view.frame;
        if (!self.isRotationActive) {
            self.frameChangeActive = YES;
            NSUInteger lastPage = self.pdfController.page;
            [self updatePagingContentSize];

            // if frame changes, ensure that we are not suddenly between two pages.
            [self setPage:lastPage animated:NO];
            [self tilePagesForced:YES];
            self.frameChangeActive = NO;
        }
    }
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Properties

- (BOOL)isHorizontal {
    return self.pdfController.scrollDirection == PSPDFScrollDirectionHorizontal;
}

// relay property to pages for special care during animation.
- (void)setRotationActive:(BOOL)rotationActive {
    _rotationActive = rotationActive;
    for (PSPDFScrollView *page in self.visiblePages) {
        page.rotationActive = rotationActive;
    }
}

// scroll the pagingScrollView to certain page.
- (void)setPage:(NSUInteger)page animated:(BOOL)animated {
    PSPDFLogVerbose(@"Setting page: %d animated:%d", page, animated);

    // need paging content size update?
    if (CGSizeEqualToSize(self.pagingScrollView.contentSize, CGSizeZero)) {
        [self updatePagingContentSize];
    }

    // dual page mode converts e.g. page 50 to index 25 then.
    NSUInteger actualPage = [self.pdfController actualPage:page];
    CGRect pageFrame = [self frameForViewAtIndex:actualPage];
    // compensate padding
    if ([self isHorizontal]) {
        pageFrame.origin.x -= self.pagePadding;
    }else {
        pageFrame.origin.y -= self.pagePadding;
    }

    PSPDFLogVerbose(@"Scrolling to offset: %@", NSStringFromCGRect(pageFrame));
    [self.pagingScrollView setContentOffset:pageFrame.origin animated:animated];

    // if not animated, we have to manually tile pages.
    // also don't manually call when we're in the middle of rotation
    if (!animated && !(self.isRotationActive && !self.isRotationAnimationActive)) {
        [self tilePagesForced:NO];
    }
}

// Set pdfController and relay changes to visible pages.
- (void)setPdfController:(PSPDFViewController *)pdfController {
    if (pdfController != _pdfController) {
        _pdfController = pdfController;
        [self.visiblePages makeObjectsPerformSelector:@selector(setPdfController:) withObject:pdfController];
    }
}

- (void)reloadData {
    // recycle all pages
    for (PSPDFScrollView *page in self.visiblePages) {
        [self recyclePage:page];
        [page removeFromSuperview];
    }
    [self.visiblePages removeAllObjects];

    [self updatePagingContentSize]; // ensure paging size is updated correctly.
    [self tilePagesForced:YES];
}

// coordinates for the global scrollview
- (CGRect)boundsForPagingScrollView {
    CGRect bounds = self.view.bounds;

    // extend the bounds if we don't use pageCurl
    if ([self isHorizontal]) {
        bounds.origin.x -= self.pagePadding;
        bounds.size.width += 2 * self.pagePadding;
    }else {
        bounds.origin.y -= self.pagePadding;
        bounds.size.height += 2 * self.pagePadding;
    }
    return bounds;
}

// UIScrollView likes to scroll a few pixel "too far" - letting us create pages that we instantly destroy
// after ScrollerHeartBeat corrects the problem and finishes the scrolling. Compensate.
#define kScrollAnimationCompensator 3

- (void)tilePagesForced:(BOOL)forceUpdate {
    _isTilingActive = YES;

    PSPDFViewController *pdfController = self.pdfController;
    PSPDFDocument *document = pdfController.document;
    CGRect visibleBounds = self.pagingScrollView.bounds;
    NSUInteger maxPageIndex = [pdfController isDoublePageMode] ? floorf([document pageCount]/2) : [document pageCount] - 1;

    // Calculate which pages are visible
    int firstNeededPageIndex, lastNeededPageIndex, primaryPageIndex;

    // if pagePadding is zero, we can't compensate scrollview movements
    CGFloat scrollAnimationCompensator = fminf(kScrollAnimationCompensator, _pagePadding);
    if ([self isHorizontal]) {
        firstNeededPageIndex = floorf((CGRectGetMinX(visibleBounds)+scrollAnimationCompensator) / CGRectGetWidth(visibleBounds));
        lastNeededPageIndex  = floorf((CGRectGetMaxX(visibleBounds)-scrollAnimationCompensator) / CGRectGetWidth(visibleBounds));
        primaryPageIndex = MAX(roundf(CGRectGetMinX(visibleBounds) / CGRectGetWidth(visibleBounds)), 0);
    }else {
        firstNeededPageIndex = floorf((CGRectGetMinY(visibleBounds)+scrollAnimationCompensator) / CGRectGetHeight(visibleBounds));
        lastNeededPageIndex  = floorf((CGRectGetMaxY(visibleBounds)-scrollAnimationCompensator) / CGRectGetHeight(visibleBounds));
        primaryPageIndex = MAX(roundf(CGRectGetMinY(visibleBounds) / CGRectGetHeight(visibleBounds)), 0);
    }

    // make sure indexes are within range.
    firstNeededPageIndex = MAX(firstNeededPageIndex, 0);
    lastNeededPageIndex  = MIN(lastNeededPageIndex, maxPageIndex);

    // set to zero if document isn't valid or if page size is zero.
    if (![document isValid] || CGRectIsEmpty(visibleBounds)) {
        firstNeededPageIndex = 0;
        lastNeededPageIndex = 0;
        primaryPageIndex = 0;
    }

    // remember vertical position?
    CGPoint offsetPoint = [self pageViewForPage:pdfController.page].scrollView.contentOffset;

    // Recycle no-longer-visible pages (or convert for re-use while rotation)
    NSMutableSet *removedPages = [NSMutableSet set];
    for (PSPDFScrollView *page in self.visiblePages) {
        [self convertPageOnDualModeChange:page currentPage:[pdfController landscapePage:primaryPageIndex]]; // used so we can re-use page on a double page mode change (rotate, usually)
        if (page.page < firstNeededPageIndex || page.page > lastNeededPageIndex) {
            [self recyclePage:page];
            [removedPages addObject:page];
        }
    }
    [self.visiblePages minusSet:self.recycledPages];

    // add missing pages
    NSMutableSet *updatedPages = [NSMutableSet set];
    for (int pageIndex = firstNeededPageIndex; pageIndex <= lastNeededPageIndex; pageIndex++) {
        if (![self isDisplayingPageForIndex:pageIndex]) {
            PSPDFScrollView *page = [self dequeueRecycledPage];
            if ([removedPages containsObject:page]) [removedPages removeObject:page];
            if (!page) page = [[pdfController classForClass:[PSPDFScrollView class]] new];

            // add view
            [self.pagingScrollView addSubview:page];
            [self.visiblePages addObject:page];

            // configure it (also sends delegate events)
            [self configurePage:page forIndex:pageIndex];

            // ensure content is scrolled to top. (fitToWidthEnabled + PSPDFScrollDirectionVertical is not yet supported)
            if (pdfController.fitToWidthEnabled && [self isHorizontal]) {
                if (pdfController.fixedVerticalPositionForFitToWidthEnabledMode) {
                    page.contentOffset = CGPointMake(fminf(offsetPoint.x, page.contentSize.width-self.view.bounds.size.width), fminf(offsetPoint.y, page.contentSize.height - self.view.bounds.size.height));
                }else {
                    page.contentOffset = CGPointMake(0, 0);
                }
            }
            [updatedPages addObject:page];
        }
    }

    // may invokes scrollViewDidScroll
    [removedPages makeObjectsPerformSelector:@selector(removeFromSuperview)];

    // if forced, configure all pages (used for rotation events)
    if (forceUpdate) {
        for (PSPDFScrollView *page in self.visiblePages) {
            if (![updatedPages containsObject:page]) [self configurePage:page forIndex:page.page];
        }
    }

    // finally, set new page
    NSUInteger page = [self.pdfController landscapePage:primaryPageIndex];
    if (pdfController.page != page) [pdfController setPageInternal:page];

    _isTilingActive = NO;
}

- (void)recyclePage:(PSPDFScrollView *)page {
    [page releaseDocument]; // remove set PDF, release memory (also, calls delegate!)
    [self.recycledPages addObject:page];
}

- (PSPDFScrollView *)dequeueRecycledPage {
    PSPDFScrollView *page = [self.recycledPages anyObject];
    if (page) [self.recycledPages removeObject:page];
    return page;
}

- (NSArray *)visiblePageViews {
    NSMutableArray *visiblePages = [NSMutableArray arrayWithCapacity:[self.visiblePages count] + 1];
    for (PSPDFScrollView *scrollView in self.visiblePages) {
        if (scrollView.leftPage.document) {
            [visiblePages addObject:scrollView.leftPage];
        }
        if (scrollView.rightPage.document) {
            [visiblePages addObject:scrollView.rightPage];
        }
    }
    return visiblePages;
}

- (NSArray *)visiblePageNumbers {
    NSMutableArray *visiblePageNumbers = [NSMutableArray arrayWithCapacity:[self.visiblePages count] + 1];
    for (PSPDFScrollView *scrollView in self.visiblePages) {
        if (scrollView.leftPage.document) {
            [visiblePageNumbers addObject:@(scrollView.leftPage.page)];
        }
        if (scrollView.rightPage.document) {
            [visiblePageNumbers addObject:@(scrollView.rightPage.page)];
        }
    }
    return visiblePageNumbers;
}

// search for the page within PSPDFScollView
- (PSPDFPageView *)pageViewForPage:(NSUInteger)page {
    PSPDFPageView *pageView = nil;
    for (PSPDFScrollView *scrollView in self.visiblePages) {
        if (scrollView.leftPage.page == page) {
            pageView = scrollView.leftPage;
            break;
        }else if (scrollView.rightPage.document && scrollView.rightPage.page == page) {
            pageView = scrollView.rightPage;
            break;
        }
    }
    return pageView;
}

- (void)updatePagingContentSize {
    PSPDFDocument *document = self.pdfController.document;
    CGRect pagingScrollViewFrame = [self frameForPageInScrollView];
    NSUInteger pageCount = [self.pdfController actualPage:[document pageCount]];
    if ([self.pdfController isDoublePageMode] && (([document pageCount]%2==1 && self.pdfController.doublePageModeOnFirstPage) || ([document pageCount]%2==0 && !self.pdfController.doublePageModeOnFirstPage))) {
        pageCount++; // first page...
    }
    PSPDFLogVerbose(@"pageCount:%d, used page Count:%d", [document pageCount], pageCount);

    CGSize contentSize;
    if ([self isHorizontal]) {
        contentSize = CGSizeMake(pagingScrollViewFrame.size.width * pageCount, pagingScrollViewFrame.size.height);
    }else {
        contentSize = CGSizeMake(pagingScrollViewFrame.size.width, pagingScrollViewFrame.size.height * pageCount);
    }

    PSPDFLogVerbose(@"old contentSize: %@, new: %@", NSStringFromCGSize(self.pagingScrollView.contentSize), NSStringFromCGSize(contentSize));
    self.pagingScrollView.contentSize = contentSize;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Frame calculations

- (CGRect)frameForPageInScrollView {
    CGRect bounds = UIEdgeInsetsInsetRect(self.view.superview.bounds, self.pdfController.margin);

    if ([self isHorizontal]) {
        bounds.origin.x -= self.pagePadding;
        bounds.size.width += 2 * self.pagePadding;
    }else {
        bounds.origin.y -= self.pagePadding;
        bounds.size.height += 2 * self.pagePadding;
    }
    return bounds;
}

// view might be one page or a combination of two pages
- (CGRect)frameForViewAtIndex:(NSUInteger)pageIndex {
    CGRect pageFrame;

    CGRect pagingScrollViewFrame = [self frameForPageInScrollView];
    pageFrame = pagingScrollViewFrame;
    if ([self isHorizontal]) {
        pageFrame.size.width -= 2 * self.pagePadding;
        pageFrame.origin.x = roundf(pagingScrollViewFrame.size.width * pageIndex) + self.pagePadding;
    }else {
        pageFrame.size.height -= 2 * self.pagePadding;
        pageFrame.origin.y = roundf(pagingScrollViewFrame.size.height * pageIndex) + self.pagePadding;
    }

    PSPDFLogVerbose(@"frameForPage: %@", NSStringFromCGRect(pageFrame));
    return pageFrame;
}

// if there is a discrepancy between double page mode, convert the pages first
- (void)convertPageOnDualModeChange:(PSPDFScrollView *)page currentPage:(NSUInteger)currentPage {
    if (page.doublePageMode != [self.pdfController isDoublePageMode]) {
        // we were double paged, now converting back to single-pages (or vice versa)
        page.page = page.doublePageMode ? [self.pdfController landscapePage:page.page convert:YES] : [self.pdfController actualPage:page.page convert:YES];

        // make rotation awesome. (if the *right* page of a two-page set is visible, switch internal pages so that right page is not reused)
        if (page.doublePageMode && page.page == currentPage-1) {
            [page switchPages];
            page.page = currentPage;
        }

        page.doublePageMode = [self.pdfController isDoublePageMode];
    }
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Private

// set properties within scrollview, update view
- (void)configurePage:(PSPDFScrollView *)page forIndex:(NSUInteger)pageIndex {
    page.doublePageMode = [self.pdfController isDoublePageMode];
    page.doublePageModeOnFirstPage = self.pdfController.doublePageModeOnFirstPage;
    page.frame = [self frameForViewAtIndex:pageIndex];
    page.zoomingSmallDocumentsEnabled = self.pdfController.zoomingSmallDocumentsEnabled;
    page.shadowEnabled = self.pdfController.shadowEnabled;
    page.scrollOnTapPageEndEnabled = self.pdfController.scrollOnTapPageEndEnabled;
    page.fitToWidthEnabled = [self isHorizontal] && self.pdfController.isFitToWidthEnabled; // KNOWN LIMITATION
    page.pdfController = self.pdfController;
    page.hidden = !self.pdfController.document; // hide view if no document is set
    [page displayDocument:self.pdfController.document withPage:pageIndex];
}

// Be sure to destroy pages before d'allocating
- (void)setVisiblePages:(NSMutableSet *)visiblePages {
    if (visiblePages != _visiblePages) {
        [self.visiblePages makeObjectsPerformSelector:@selector(releaseDocument)];
        _visiblePages = visiblePages;
    }
}

// Checks if a page is already displayed in the scrollview
- (BOOL)isDisplayingPageForIndex:(NSUInteger)pageIndex {
    BOOL foundPage = NO;
    for (PSPDFScrollView *page in self.visiblePages) {
        if (page.page == pageIndex) {
            foundPage = YES;
            break;
        }
    }
    return foundPage;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    // Before willAnimateRotate* is invoked, the system adapts the frame of the scrollView and thus maybe contentOffset is adapted.
    // This is happening at the very last page of a document on rotate, so ignore the event here.
    if (self.isFrameChangeActive || (self.rotationActive && !self.rotationAnimationActive)) return;

    // Tiling might invoke didScroll, don't recursive call.
    if (!_isTilingActive) [self tilePagesForced:NO];

    // Ensure menu is hidden when we start scrolling
    [[UIMenuController sharedMenuController] setMenuVisible:NO animated:YES];
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    if (self.pdfController.viewMode == PSPDFViewModeDocument) {
        [self.pdfController hideControls];
    }

    // invalidate target page (used to get correct page after rotation)
    self.targetPageAfterRotation = 1;
}

// called on finger up if user dragged. decelerate is true if it will continue moving afterwards
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    [self.pdfController hideControlsIfPageMode];

    if (!decelerate) {
        if ([self.pdfController shouldShowControls]) {
            [self.pdfController showControls];
        }
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    [self.pdfController hideControlsIfPageMode];
    if ([self.pdfController shouldShowControls]) {
        [self.pdfController showControls];
    }
}

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView {
    [self.pdfController delegateDidEndPageScrollingAnimation:scrollView];
}

- (void)scrollViewDidEndZooming:(UIScrollView *)scrollView withView:(UIView *)view atScale:(float)scale {
    [self.pdfController delegateDidEndZooming:scrollView withView:view atScale:scale];
}

@end
