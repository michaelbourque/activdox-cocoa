//
//  PSPDFPageInfo.m
//  PSPDFKit
//
//  Copyright 2011-2012 Peter Steinberger. All rights reserved.
//

#import "PSPDFPageInfo.h"
#import "PSPDFKitGlobal.h"
#import "PSPDFConverter.h"

@implementation PSPDFPageInfo

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

- (id)initWithPage:(NSUInteger)page rect:(CGRect)pageRect rotation:(NSInteger)rotation documentProvider:(PSPDFDocumentProvider *)documentProvider {
    if ((self = [super init])) {
        _pageRotation = PSPDFNormalizeRotation(rotation); // normalize (yes there really are documents with a 360 degree rotation level...)
        _pageRect = pageRect;
        _page = page;
        _documentProvider = documentProvider;
        [self updatePageTransform];
    }
    return self;
}

- (NSString *)description {
    return [NSString stringWithFormat:@"<%@ rect:%@ rotation:%d rotatedPageRect:%@>", NSStringFromClass([self class]), NSStringFromCGRect(self.pageRect), self.pageRotation, NSStringFromCGRect(self.rotatedPageRect)];
}

- (NSUInteger)hash {
    return ((int)_pageRect.origin.x ^ (int)_pageRect.origin.y ^ (int)_pageRect.size.width ^ (int)_pageRect.size.height) + self.page;
}

- (BOOL)isEqual:(id)other {
    if ([other isKindOfClass:[self class]]) {
        return [self isEqualToPageInfo:(PSPDFPageInfo *)other];
    }
    return NO;
}

- (BOOL)isEqualToPageInfo:(PSPDFPageInfo *)otherPageInfo {
    if (CGRectEqualToRect(self.pageRect, otherPageInfo.pageRect) && self.pageRotation == otherPageInfo.pageRotation && self.page == otherPageInfo.page) {
        return YES;
    }
    return NO;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public

- (void)setPageRotation:(NSUInteger)pageRotation {
    _pageRotation = pageRotation;
    [self updatePageTransform];
}

- (CGRect)rotatedPageRect {
    return PSPDFApplyRotationToRect(_pageRect, _pageRotation);
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Private

- (void)updatePageTransform {
    CGRect pageRect = PSPDFApplyRotationToRect(_pageRect, self.pageRotation);
    _pageRotationTransform = PSPDFGetTransformFromPageRectAndRotation(pageRect, self.pageRotation);
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSCopying

- (id)copyWithZone:(NSZone *)zone {
    PSPDFPageInfo *pageInfo = [[[self class] alloc] initWithPage:self.page rect:self.pageRect rotation:self.pageRotation documentProvider:self.documentProvider];
    return pageInfo;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSCoding

// password is not serialized!
- (id)initWithCoder:(NSCoder *)coder {
    NSUInteger page = [coder decodeIntegerForKey:NSStringFromSelector(@selector(page))];
    CGRect pageRect = [coder decodeCGRectForKey:NSStringFromSelector(@selector(pageRect))];
    NSInteger rotation = [coder decodeIntegerForKey:NSStringFromSelector(@selector(rotation))];
    self = [self initWithPage:page rect:pageRect rotation:rotation documentProvider:nil];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)coder {
    [coder encodeInteger:self.page forKey:NSStringFromSelector(@selector(page))];
    [coder encodeCGRect:self.pageRect forKey:NSStringFromSelector(@selector(pageRect))];
    [coder encodeInteger:self.pageRotation forKey:NSStringFromSelector(@selector(rotation))];
}

@end
