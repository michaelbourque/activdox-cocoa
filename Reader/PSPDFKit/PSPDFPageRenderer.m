//
//  PSPDFPageRenderer.m
//  PSPDFKit
//
//  Copyright 2011-2012 Peter Steinberger. All rights reserved.
//
//  Based on code by Sorin Nistor. Many, Many thanks! (Code has been altered since)
//  Copyright (c) 2011-2012 Sorin Nistor. All rights reserved. This software is provided 'as-is', without any express or implied warranty.
//  In no event will the authors be held liable for any damages arising from the use of this software.
//  Permission is granted to anyone to use this software for any purpose, including commercial applications,
//  and to alter it and redistribute it freely, subject to the following restrictions:
//  1. The origin of this software must not be misrepresented; you must not claim that you wrote the original software.
//     If you use this software in a product, an acknowledgment in the product documentation would be appreciated but is not required.
//  2. Altered source versions must be plainly marked as such, and must not be misrepresented as being the original software.
//  3. This notice may not be removed or altered from any source distribution.
//

#import "PSPDFPageRenderer.h"
#import "PSPDFPageInfo.h"
#import "PSPDFAnnotation.h"
#import "PSPDFKitGlobal+Internal.h"
#import "PSPDFConverter.h"

NSString *const kPSPDFIgnoreDisplaySettings = @"kPSPDFIgnoreDisplaySettings";
NSString *const kPSPDFPageColor = @"kPSPDFPageColor";
NSString *const kPSPDFContentOpacity = @"kPSPDFContentOpacity";
NSString *const kPSPDFInvertRendering = @"kPSPDFInvertRendering";
NSString *const kPSPDFInterpolationQuality = @"kPSPDFInterpolationQuality";
NSString *const kPSPDFDisablePageRendering = @"kPSPDFDisablePageRendering";
NSString *const kPSPDFRenderOverlayAnnotations = @"kPSPDFRenderOverlayAnnotations";
NSString *const kPSPDFIgnorePageClip = @"kPSPDFIgnorePageClip";
NSString *const kPSPDFAllowAntiAliasing = @"kPSPDFAllowAntiAliasing";
NSString *const kPSPDFBackgroundFillColor = @"kPSPDFBackgroundFillColor";
NSString *const kPSPDFPDFBox = @"kPSPDFPDFBox";

@implementation PSPDFPageRenderer

// Calculate rendering rectangle and scale from cropbox/rotation
static CGRect PSPDFRenderingRectangle(CGRect displayRectangle, CGRect cropBox, int pageRotation, float *scale) {
    CGRect renderingRectangle = CGRectMake(0, 0, 0, 0);
    if (PSPDFSizeIsEmpty(displayRectangle.size)) {
        return renderingRectangle;
    }

	CGSize pageVisibleSize = CGSizeMake(cropBox.size.width, cropBox.size.height);
	if ((pageRotation == 90) || (pageRotation == 270) ||(pageRotation == -90)) {
		pageVisibleSize = CGSizeMake(cropBox.size.height, cropBox.size.width);
	}

    float scaleX = displayRectangle.size.width / pageVisibleSize.width;
    float scaleY = displayRectangle.size.height / pageVisibleSize.height;
    *scale = scaleX < scaleY ? scaleX : scaleY;

    // Offset relative to top left corner of rectangle where the page will be displayed.
    float offsetX = 0;
    float offsetY = 0;

    float rectangleAspectRatio = displayRectangle.size.width / displayRectangle.size.height;
    float pageAspectRatio = pageVisibleSize.width / pageVisibleSize.height;

    if (pageAspectRatio < rectangleAspectRatio) {
        // The page is narrower than the rectangle, we place it at center on the horizontal.
        offsetX = (displayRectangle.size.width - pageVisibleSize.width * *scale) / 2;
    }
    else {
        // The page is wider than the rectangle, we place it at center on the vertical.
        offsetY = (displayRectangle.size.height - pageVisibleSize.height * *scale) / 2;
    }

    renderingRectangle.origin = CGPointMake(roundf(displayRectangle.origin.x + offsetX), roundf(displayRectangle.origin.y + offsetY));
    
    return renderingRectangle;
}

+ (void)setupGraphicsContext:(CGContextRef)context inRectangle:(CGRect)displayRectangle pageInfo:(PSPDFPageInfo *)pageInfo {

    CGFloat scale = 1;
    CGRect renderingRectangle = PSPDFRenderingRectangle(displayRectangle, pageInfo.pageRect, pageInfo.pageRotation, &scale);
    
    [self setupGraphicsContext:context atPoint:renderingRectangle.origin withZoom:scale*100 cropBox:pageInfo.pageRect clipCropBox:YES pageRotation:pageInfo.pageRotation];
}

+ (CGSize)setupGraphicsContext:(CGContextRef)context atPoint:(CGPoint)point withZoom:(float)zoom cropBox:(CGRect)cropBox clipCropBox:(BOOL)clipCropBox pageRotation:(int)pageRotation {
    CGSize renderingSize = CGSizeMake(0, 0);

    // Setup the coordinate system.
	// Top left corner of the displayed page must be located at the point specified by the 'point' parameter.
	CGContextTranslateCTM(context, point.x, point.y);

	// Scale the page to desired zoom level.
	CGContextScaleCTM(context, zoom / 100, zoom / 100);

	// The coordinate system must be set to match the PDF coordinate system.
	switch (pageRotation) {
		case 0:
            renderingSize.width = roundf(cropBox.size.width * zoom / 100);
            renderingSize.height = roundf(cropBox.size.height * zoom / 100);
			CGContextTranslateCTM(context, 0, cropBox.size.height);
			CGContextScaleCTM(context, 1, -1);
			break;
		case 90:
            renderingSize.width = roundf(cropBox.size.height * zoom / 100);
            renderingSize.height = roundf(cropBox.size.width * zoom / 100);
			CGContextScaleCTM(context, 1, -1);
			CGContextRotateCTM(context, -M_PI / 2.f);
			break;
		case 180:
		case -180:
            renderingSize.width = roundf(cropBox.size.width * zoom / 100);
            renderingSize.height = roundf(cropBox.size.height * zoom / 100);
			CGContextScaleCTM(context, 1, -1);
			CGContextTranslateCTM(context, cropBox.size.width, 0);
			CGContextRotateCTM(context, M_PI * 1.f);
			break;
		case 270:
		case -90:
            renderingSize.width = roundf(cropBox.size.height * zoom / 100);
            renderingSize.height = roundf(cropBox.size.width * zoom / 100);
			CGContextTranslateCTM(context, cropBox.size.height, cropBox.size.width);
			CGContextRotateCTM(context, M_PI / 2.f);
			CGContextScaleCTM(context, -1, 1);
			break;
        default:
            PSPDFLogWarning(@"Weird page rotation: %d - can't handle.", pageRotation);
	}

	// The CropBox defines the page visible area, clip everything outside it.
    if (clipCropBox) {
        CGRect clipRect = CGRectMake(0, 0, cropBox.size.width, cropBox.size.height);
        CGContextAddRect(context, clipRect);
        CGContextClip(context);
    }

    return renderingSize;
}

+ (CGRect)renderPageRef:(CGPDFPageRef)page inContext:(CGContextRef)context inRectangle:(CGRect)displayRectangle pageInfo:(PSPDFPageInfo *)pageInfo withAnnotations:(NSArray *)annotations options:(NSDictionary *)options {

    // use data from pageInfo, if available (may be different than actual pdf!)
    CGPDFBox PDFBox = options[kPSPDFPDFBox] ? [options[kPSPDFPDFBox] unsignedIntegerValue] : kCGPDFCropBox;
    CGRect cropBox = pageInfo ? pageInfo.pageRect : CGPDFPageGetBoxRect(page, PDFBox);
    int pageRotation = pageInfo ? pageInfo.pageRotation : PSPDFNormalizeRotation(CGPDFPageGetRotationAngle(page));

    CGFloat scale = 1;
    CGRect renderingRectangle = PSPDFRenderingRectangle(displayRectangle, cropBox, pageRotation, &scale);

    renderingRectangle.size = [PSPDFPageRenderer renderPage:page
                                                  inContext:context
                                                    atPoint:renderingRectangle.origin
                                                   withZoom:scale * 100
                                                   pageInfo:pageInfo
                                            withAnnotations:annotations
                                                    options:options];

    return renderingRectangle;
}


+ (CGSize)renderPage:(CGPDFPageRef)page inContext:(CGContextRef)context atPoint:(CGPoint)point withZoom:(float)zoom pageInfo:(PSPDFPageInfo *)pageInfo withAnnotations:(NSArray *)annotations options:(NSDictionary *)options {        
    // use data from pageInfo, if available (may be different than actual pdf!)
    CGPDFBox PDFBox = options[kPSPDFPDFBox] ? [options[kPSPDFPDFBox] unsignedIntegerValue] : kCGPDFCropBox;
    CGRect cropBox = pageInfo ? pageInfo.pageRect : CGPDFPageGetBoxRect(page, PDFBox);
    int pageRotation = pageInfo ? pageInfo.pageRotation : PSPDFNormalizeRotation(CGPDFPageGetRotationAngle(page));
	BOOL allowAntialising = options[kPSPDFAllowAntiAliasing] ? [options[kPSPDFAllowAntiAliasing] boolValue] : YES;
	CGContextSaveGState(context);

    CGInterpolationQuality interpolationQuality = kCGInterpolationHigh;
    NSNumber *interpolationQualityNumber = options[kPSPDFInterpolationQuality];
    if (interpolationQualityNumber) {
        interpolationQuality = [interpolationQualityNumber unsignedIntegerValue];
        if (interpolationQuality < kCGInterpolationHigh) {
            interpolationQuality = kCGInterpolationHigh; // check and limit parameters.
        }
    }

	// reduce memory prior pdf drawing
	CGContextSetInterpolationQuality(context, interpolationQuality);
	CGContextSetRenderingIntent(context, kCGRenderingIntentDefault);
    CGContextSetAllowsAntialiasing(context, allowAntialising);

    // setup the graphics context
    BOOL ignoreClip = [options[kPSPDFIgnorePageClip] boolValue];
    CGSize renderingSize = [self setupGraphicsContext:context atPoint:point withZoom:zoom cropBox:cropBox clipCropBox:!ignoreClip pageRotation:pageRotation];
    CGRect clipRect = CGRectMake(0, 0, cropBox.size.width, cropBox.size.height);

    // fill everything with white first (or a custom color)
    UIColor *backgroundColor = options[kPSPDFBackgroundFillColor] ?: [UIColor whiteColor];
    if (![backgroundColor isEqual:[UIColor clearColor]]) {
        CGContextSetFillColor(context, CGColorGetComponents(backgroundColor.CGColor));
        CGContextFillRect(context, clipRect);
    }

	CGContextTranslateCTM(context, roundf(-cropBox.origin.x), roundf(-cropBox.origin.y));

    // set contrast level
	float contentOpacity = [options[kPSPDFContentOpacity] floatValue];
    if (contentOpacity == 0.f) contentOpacity = 1.f; // don't allow zero opacity!
    CGContextSetAlpha(context, contentOpacity);

    // this might throw __cxa_throw errors because of some internal drawing functions. They usually are handled internally,
    // you still might be thrown in here if you have a breakpoint set on all exceptions. Just ignore.
    if (![options[kPSPDFDisablePageRendering] boolValue]) {
        @autoreleasepool {
            CGContextDrawPDFPage(context, page);
        }
    }

    // draw annotations
	CGContextSetAlpha(context, 1.0);

    if (annotations) {
        BOOL ignoreOverlayState = [options[kPSPDFRenderOverlayAnnotations] boolValue];
        CGContextSaveGState(context);

        // antialising is slow
        CGContextSetInterpolationQuality(context, interpolationQuality);
        CGContextSetAllowsAntialiasing(context, allowAntialising);
        CGContextSetShouldAntialias(context, YES);
        for (PSPDFAnnotation *annotation in annotations) {
            if (!annotation.isDeleted && (!annotation.isOverlay || ignoreOverlayState)) {
                [annotation drawInContext:context];
            }
        }
        CGContextRestoreGState(context);
    }

    if (renderingSize.width > 300) {
        CGContextSaveGState(context);

        if (pageInfo.pageRotation == 0) {
            CGContextTranslateCTM(context, pageInfo.pageRect.origin.x, CGRectGetMaxY(pageInfo.pageRect));
            CGContextScaleCTM(context, 1, -1);
        }else if(pageInfo.pageRotation == 90 || pageInfo.pageRotation == 270) {
            // 270 is not perfect yet
            CGContextRotateCTM(context, PSPDFDegreesToRadians(pageInfo.pageRotation));
            CGContextScaleCTM(context, 1, -1);
        }

        PSPDFDrawHelper(context, pageInfo.rotatedPageRect.size.width/600);
        CGContextRestoreGState(context);
    }

    clipRect = CGContextGetClipBoundingBox(context);

    // apply a paper color?
	UIColor *paperColor = options[kPSPDFPageColor];
	if (paperColor) {
		CGContextSetBlendMode(context, kCGBlendModeMultiply);
		CGContextSetFillColorWithColor(context, [paperColor CGColor]);
		CGContextSetAlpha(context, contentOpacity);
		CGContextFillRect(context, clipRect);
		CGContextSetAlpha(context, 1.0);
	}

    // invert all drawing?
	BOOL inverted = [options[kPSPDFInvertRendering] boolValue];
	if (inverted) {
		CGContextSetBlendMode(context, kCGBlendModeDifference);
		CGContextSetRGBFillColor(context, 1.0, 1.0, 1.0, 1.0);
		CGContextFillRect(context, clipRect);
	}

	CGContextRestoreGState(context);

    return renderingSize;
}

@end
