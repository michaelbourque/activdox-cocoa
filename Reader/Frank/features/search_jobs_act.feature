Feature: 
  Select the search toolbar and search for some text.
  
Scenario: 
  Assumes the app is viewing the JOBS act.
  Selects the search toolbar, and then searches for the word "growth" and checks the count.

Given I should see "JOBS Act - Version 4"
Then I touch "053-Search.png"
Then I wait to see a "UISearchBar"
Then I touch a "UISearchBar"

Then I clear the text from a "UISearchBar"

Then I enter the text "Growth"
Then I wait to see "76 found"

Then I wait for 1 second
Then I touch "Toggle Thumbnails"
