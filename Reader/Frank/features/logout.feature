Feature: 
  Logout
  
Scenario: 
  Logging out.  Assumes the app is at the Collections view.

Given I should see a "Sign Out" button
When I touch the button marked "Sign Out"
Then I wait to see "Login"
Then I should see a "Login" button

