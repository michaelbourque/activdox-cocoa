def app_path
  ENV['APP_BUNDLE_PATH'] || (defined?(APP_BUNDLE_PATH) && APP_BUNDLE_PATH)
end

puts "Adding ActivDox custom steps"

When /^I touch a "([^\"]*)"$/ do |viewclass|
  selector = "view:'#{viewclass}'"
  if element_exists(selector)
     touch( selector )
  else
     raise "Could not touch [#{viewclass}], it does not exist."  
  end
  sleep 1
end

Then /^I wait to see a "([^\"]*)"$/ do |expected_mark|
    quote = get_selector_quote(expected_mark)
    selector = "view:'#{expected_mark}'"
    wait_until(:message => "waited to see view marked #{quote}#{expected_mark}#{quote}"){
        element_exists( selector )
    }
end

def send_command ( command )
    %x{osascript<<APPLESCRIPT
        tell application "System Events"
        tell application "iPhone Simulator" to activate
        keystroke "#{command}"
        delay 1
        key code 36
        end tell
    APPLESCRIPT}
end

When /^I enter the text "([^\"]*)"$/ do |cmd|
    send_command(cmd)
end

When /^I clear the text from a "([^\"]*)"$/ do |field_name|
    quote = get_selector_quote(field_name)
    text_fields_modified = frankly_map( "view:'#{field_name}'", "setText:", '' )
    raise "could not find text fields with placeholder #{quote}#{field_name}#{quote}" if text_fields_modified.empty?
    #TODO raise warning if text_fields_modified.count > 1
end

When /^I touch the document at "([^\"]*),([^\"]*)"$/ do |x_coord, y_coord|
    views_touched = frankly_map( "view:'ADXPDFPageView' first", "touchx:y:", x_coord, y_coord  )
end

