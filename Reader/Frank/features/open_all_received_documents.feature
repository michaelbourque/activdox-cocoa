Feature: 
  Open All Received Documents
  
Scenario: 
  The user is logged in, and at the Collections view.  Tap the All Received Documents collection.

Then I wait for 2 seconds

Then I wait to see "All Received Documents"
Then I touch "All Received Documents"

Then I wait to see "JOBS Act"
