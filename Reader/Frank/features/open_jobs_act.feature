Feature: 
  Open JOBS Act
  
Scenario: 
  Opens the JOBS act.
  Assumes the app is viewing the All Received Documents collection.

Given I should see "All Received Documents"
Then I should see "JOBS Act"
Then I touch "JOBS Act"

Then I wait to see "JOBS Act - Version 4"
