Feature: 
  Login
  
Scenario: 
  Logging in as a valid user

# Fill in the login form  
When I type "steve.tibbett" into the "User Name" text field
When I type "pwd" into the "Password" text field
When I type "fresh.activdox.com" into the "Server" text field

# Tap the login button
When I touch the button marked "Login"

# Get past the APNS alert
Then I wait to see "Warning"
Then I should see an alert view titled "Warning"
Then I wait for 0.5 seconds
Then I touch "OK"

# Wait for the collections view
Then I wait to see "Collections"
Then I should see a navigation bar titled "Collections"

