Feature: 
  Select the search toolbar and search for some text.
  
Scenario: 
  Assumes the app is viewing the JOBS act.
  Selects the search toolbar, and then searches for the word "growth" and checks the count.

Given I should see "JOBS Act - Version 4"
Then I touch "166-Drawing.png"
Then I wait to see "038-Callout"

# Add First Note

Then I touch "038-Callout"

Then I wait for 0.5 seconds

Then I touch the document at "200,200"

Then I wait to see "Note"

Then I enter the text "This is my note text."

Then I touch the document at "100,100"

Then I wait to not see "Note"

# Add Second Note

Then I touch "038-Callout"

Then I wait for 0.5 seconds

Then I touch the document at "250,200"

Then I wait to see "Note"

Then I enter the text "This is my note text."

Then I touch the document at "100,100"

Then I wait to not see "Note"

# Add Second Note

Then I touch "038-Callout"

Then I wait for 0.5 seconds

Then I touch the document at "300,200"

Then I wait to see "Note"

Then I enter the text "This is my note text."

Then I touch the document at "100,100"

Then I wait to not see "Note"
