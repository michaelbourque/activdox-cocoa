//
//  ADXAppDelegate+Session.h
//  Reader
//
//  Created by Gavin McKenzie on 2013-04-22.
//  Copyright (c) 2013 ActivDox Inc. All rights reserved.
//

#import "ADXAppDelegate.h"

@interface ADXAppDelegate (Session)
- (void)performLogoutTasks;
- (void)performHouseKeepingTasks:(ADXCompletionBlock)completionBlock;
- (void)presentNeedToBeOnlineAlert;
@end
