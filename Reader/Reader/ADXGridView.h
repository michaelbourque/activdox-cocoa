//
//  ADXGridView.h
//  Reader
//
//  Created by Gavin McKenzie on 2012-09-11.
//  Copyright (c) 2012 ActivDox Inc. All rights reserved.
//

#import "GMGridView.h"

@interface ADXGridView : GMGridView
@end

@protocol ADXGridViewActionDelegate <NSObject>
@required
- (void)ADXGridView:(ADXGridView *)gridView didLongPressOnItemAtIndex:(NSInteger)position;
@end