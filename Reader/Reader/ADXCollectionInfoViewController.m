//
//  ADXCollectionInfoViewController.m
//  Reader
//
//  Created by Gavin McKenzie on 2013-04-08.
//  Copyright (c) 2013 ActivDox Inc. All rights reserved.
//

#import "ADXCollectionInfoViewController.h"
#import "ADXCollectionInfoTitleCell.h"
#import "ADXCollectionInfoMenuCell.h"
#import "ADXAppDelegate.h"
#import "ADXPopoverBackgroundView.h"
#import "ADXBarButtonItem.h"

static NSInteger const kADXCollectionInfoSectionTitle = 0;
static NSInteger const kADXCollectionInfoSectionDocuments = 1;
static NSInteger const kADXCollectionInfoItemSelectDocuments = 0;

static NSString * const kADXCollectionInfoTitleCellIdentifier = @"ADXCollectionInfoTitleCell";
static NSString * const kADXCollectionInfoMenuCellIdentifier = @"ADXCollectionInfoMenuCell";

@interface ADXCollectionInfoViewController () <ADXCollectionInfoTitleCellActionDelegate>
@end

@implementation ADXCollectionInfoViewController

#pragma mark - Class Methods

+ (UIPopoverController *)popoverControllerWithCollection:(ADXCollection *)collection initialMode:(ADXCollectionInfoMode)initialMode title:(NSString *)title actionDelegate:(id)delegate
{
    UIPopoverController *popover = nil;
    ADXCollectionInfoViewController *viewController = [[ADXCollectionInfoViewController alloc] initWithNibName:nil bundle:nil];
    viewController.collection = collection;
    viewController.initialMode = initialMode;
    viewController.actionDelegate = delegate;
    viewController.navigationItem.title = title;
    
    if (viewController) {
        popover = [[UIPopoverController alloc] initWithContentViewController:[[UINavigationController alloc] initWithRootViewController:viewController]];
        popover.popoverBackgroundViewClass = [ADXBluePopoverBackgroundView class];
    }
    
    viewController.parentPopoverController = popover;

    return popover;
}

#pragma mark - View Management

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"popover_bg"]];
    
    UIBarButtonItem *deleteItem = [ADXBarButtonItem itemWithImageNamed:@"trash" target:self selector:@selector(didTapDeleteButton:)];
    self.navigationItem.leftBarButtonItems = @[deleteItem];

    UINib *titleCellNib = [UINib nibWithNibName:kADXCollectionInfoTitleCellIdentifier bundle:nil];
    [self.tableView registerNib:titleCellNib forCellReuseIdentifier:kADXCollectionInfoTitleCellIdentifier];

    UINib *menuCellNib = [UINib nibWithNibName:kADXCollectionInfoMenuCellIdentifier bundle:nil];
    [self.tableView registerNib:menuCellNib forCellReuseIdentifier:kADXCollectionInfoMenuCellIdentifier];
    
    if (self.initialMode == ADXCollectionInfoModeDisplayDocuments) {
        ADXCollectionDocumentsViewController *listViewController = [self newDocumentListView];
        [self.navigationController pushViewController:listViewController animated:YES];
        [self.parentPopoverController setPopoverContentSize:listViewController.contentSizeForViewInPopover animated:YES];
    } else {
        CGSize size = self.contentSizeForViewInPopover;
        size.height += 44.0f;
        [self.parentPopoverController setPopoverContentSize:size animated:YES];
    }
}

- (CGSize)contentSizeForViewInPopover
{
    return CGSizeMake(320.0f, 160.0f);
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (ADXCollectionDocumentsViewController *)newDocumentListView
{
    ADXCollectionDocumentsViewController *listViewController = [[ADXCollectionDocumentsViewController alloc] initWithNibName:nil bundle:nil];
    listViewController.collection = self.collection;
    listViewController.parentPopoverController = self.parentPopoverController;
    listViewController.allowEditing = YES;
    listViewController.actionDelegate = self;

    if (!self.collection.numberOfDocuments) {
        listViewController.startInEditMode = YES;
    }
    return listViewController;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger result = 0;
    switch (section) {
        case kADXCollectionInfoSectionTitle:
            result = 1;
            break;

        case kADXCollectionInfoSectionDocuments:
            result = 1;
            break;

        default:
            break;
    }
    return result;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = nil;
    if (indexPath.section == kADXCollectionInfoSectionTitle) {
        cell = [tableView dequeueReusableCellWithIdentifier:kADXCollectionInfoTitleCellIdentifier];
        if (!cell) {
            cell = (ADXCollectionInfoTitleCell *)[[ADXCollectionInfoTitleCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kADXCollectionInfoTitleCellIdentifier];
        }
        [(ADXCollectionInfoTitleCell *)cell setActionDelegate:self];
    } else if (indexPath.section == kADXCollectionInfoSectionDocuments) {
        cell = [tableView dequeueReusableCellWithIdentifier:kADXCollectionInfoMenuCellIdentifier];
        if (!cell) {
            cell = (ADXCollectionInfoMenuCell *)[[ADXCollectionInfoTitleCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kADXCollectionInfoMenuCellIdentifier];
        }
    }
    
    if (indexPath.section == kADXCollectionInfoSectionTitle) {
        [(ADXCollectionInfoTitleCell *)cell titleField].placeholder = kADXCollectionUntitled;
        if (![self.collection.title isEqualToString:kADXCollectionUntitled]) {
            [(ADXCollectionInfoTitleCell *)cell titleField].text = self.collection.title;
        }
    } else if (indexPath.section == kADXCollectionInfoSectionDocuments) {
        NSString *message = @"Select documents";
        NSUInteger numberOfDocuments = self.collection.numberOfDocuments;
        if (numberOfDocuments > 1) {
            message = [NSString stringWithFormat:@"%d documents", numberOfDocuments];
        } else if (numberOfDocuments == 1) {
            message = [NSString stringWithFormat:@"%d document", numberOfDocuments];
        }
        [(ADXCollectionInfoMenuCell *)cell messageLabel].text = message;
    }
    
    return cell;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    NSString *result = nil;
    if (section == 1) {
        result = @"Documents";
    }
    return result;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 40)];
    tableView.sectionHeaderHeight = headerView.frame.size.height;
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(12, 0, self.view.bounds.size.width, 40)];
    [label setBackgroundColor:[UIColor clearColor]];
    [label setFont:[UIFont boldSystemFontOfSize:17.0]];
    [label setShadowColor:[UIColor whiteColor]];
    [label setShadowOffset:CGSizeMake(0, 1)];
    [label setText:[self tableView:tableView titleForHeaderInSection:section]];
    [label setTextColor:[UIColor adxPopoverTextColor]];
    [headerView addSubview:label];
    
    return headerView;
}

- (void)refreshDocumentCount
{
    NSIndexPath *documentCountIndexPath = [NSIndexPath indexPathForRow:kADXCollectionInfoItemSelectDocuments inSection:kADXCollectionInfoSectionDocuments];
    if (documentCountIndexPath) {
        [self.tableView reloadRowsAtIndexPaths:@[documentCountIndexPath] withRowAnimation:UITableViewRowAnimationNone];
    }
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == kADXCollectionInfoSectionDocuments && indexPath.row == kADXCollectionInfoItemSelectDocuments) {
        [tableView endEditing:YES];
        ADXCollectionDocumentsViewController *listViewController = [self newDocumentListView];
        [self.navigationController pushViewController:listViewController animated:YES];
    }
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - ADXCollectionInfoTitleCellActionDelegate

- (void)didUpdateTitleField:(NSString *)text
{
    self.collection.title = text;
}

- (void)didEndEditingTextField:(NSString *)text
{
    self.collection.title = text;
    [self.collection saveWithOptionalSync:YES completion:nil];
}

#pragma mark - ADXCollectionDocumentsActionsDelegate

- (void)didSelectDocument:(ADXDocument *)document
{
    if (self.actionDelegate) {
        [self.actionDelegate didSelectDocument:document];
    }
}

- (void)didRemoveDocument:(ADXDocument *)document fromCollection:(ADXCollection *)collection
{
    if (self.actionDelegate) {
        [self.actionDelegate didRemoveDocument:document fromCollection:collection];
    }
}

- (void)didAddDocument:(ADXDocument *)document toCollection:(ADXCollection *)collection
{
    if (self.actionDelegate) {
        [self.actionDelegate didAddDocument:document toCollection:collection];
    }
}

#pragma mark - Actions

- (void)didTapDeleteButton:(id)sender
{
    if (self.actionDelegate) {
        [self.actionDelegate didTapDeleteCollection:self.collection];
    }
}

@end
