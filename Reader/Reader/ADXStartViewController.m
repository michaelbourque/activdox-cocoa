//
//  ADXStartViewController.m
//  Reader
//
//  Created by Gavin McKenzie on 12-06-08.
//  Copyright (c) 2012 ActivDox Inc. All rights reserved.
//

#import "ADXStartViewController.h"
#import "ActivDoxCore.h"
#import "ADXAppDelegate.h"
#import "ADXCollectionsViewController.h"
#import "PSPDFKit.h"

typedef enum {
    ADXStartViewStateInitial = 0,
    ADXStartViewStateLoading,
    ADXStartViewStatePerformHousekeepingTasks,
    ADXStartViewStateAutoLogin,
    ADXStartViewStateHaveOrNeedAccount,
    ADXStartViewStateCreateAccount,
    ADXStartViewStateCreateAccountInProgress,
    ADXStartViewStateCreateAccountFailed,
    ADXStartViewStateLoginAccount,
    ADXStartViewStateLoginInProgress,
    ADXStartViewStateLoginFailed,
    ADXStartViewStateResetPassword,
    ADXStartViewStateResetPasswordInProgress,
    ADXStartViewStateResetPasswordFailed,
} ADXStartViewState;

static NSString *kADXOfflineNoConnectivity = @"No network connection. Check your Wi-Fi or broadband settings.";
static NSString *kADXOfflineNoServer = @"Could not connect to server.";

@interface ADXStartViewController ()
@property (weak, nonatomic, readonly) NSManagedObjectContext *managedObjectContext;
@property (strong, nonatomic) ADXAccount *currentAccount;
@property (assign, nonatomic) ADXStartViewState state;
@property (assign, nonatomic) ADXStartViewState previousState;
@property (strong, nonatomic) NSString *currentLoginName;
@property (strong, nonatomic) NSString *currentPassword;
@property (strong, nonatomic) NSString *currentAccessToken;
@property (strong, nonatomic) NSString *currentServerURL;
@property (assign, nonatomic) BOOL currentRememberPassword;
@property (assign, nonatomic) BOOL recentRegistrationFailed;
@property (weak, nonatomic) UIView *currentlyDisplayedPanelView;
@property (strong, nonatomic) NSString *createOrLoginPanelBannerMessage;
@property (strong, nonatomic) NSString *loginOrRegistrationFailureMessage;
@end

@implementation ADXStartViewController

#pragma mark - Initialization

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        //
    }
    return self;
}

#pragma mark - Accessors

- (NSManagedObjectContext *)managedObjectContext
{
    ADXAppDelegate *appDelegate = (ADXAppDelegate *)[[UIApplication sharedApplication] delegate];
    return [appDelegate managedObjectContext];
}

#pragma mark - Helpers

- (NSString *)trimmedServerURL:(NSString *)serverUrl
{
    NSString *result = nil;
    if ([serverUrl hasPrefix:@"https://"]) {
        result = [serverUrl substringFromIndex:8];
    } else {
        result = serverUrl;
    }
    return result;
}

#pragma mark - State Management

- (UIView *)transitionToHaveOrNeedAccountState
{
    UINib *nib = [UINib nibWithNibName:@"ADXLoginOrRegisterView" bundle:nil];
    [nib instantiateWithOwner:self options:nil];
    
    if (!IsEmpty(self.createOrLoginPanelBannerMessage)) {
        self.createOrLoginPanelBannerTextView.text = self.createOrLoginPanelBannerMessage;
        self.createOrLoginPanelBannerMessage = nil;
    } else {
        self.createOrLoginPanelBannerTextView.text = nil;
    }
    
    return self.createOrLoginPanel;
}

- (UIView *)transitionToPerformHousekeepingTasksState
{
    UINib *nib = [UINib nibWithNibName:@"ADXLoginProgressView" bundle:nil];
    [nib instantiateWithOwner:self options:nil];
    
    self.activityMessage.text = @"Cleaning document cache...";
    [self.activityIndicator startAnimating];
    
    __weak ADXStartViewController *weakSelf = self;
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        [[ADXAppDelegate sharedInstance] performHouseKeepingTasks:^(BOOL success, NSError *error) {
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                [weakSelf transitionToAppropriateStartingState];
            }];
        }];
    }];
    
    return self.loginProgressPanel;
}

- (UIView *)transitionToCreateAccountState
{
    UINib *nib = [UINib nibWithNibName:@"ADXLoginRegisterView" bundle:nil];
    [nib instantiateWithOwner:self options:nil];
    
    if (self.currentAccount && !self.recentRegistrationFailed) {
        self.registerFullNameField.text = nil;
        self.registerEmailField.text = nil;
        self.registerPasswordField.text = nil;
        self.registerPasswordConfirmField.text = nil;
        self.registerServerField.text = nil;
    }
    
    return self.loginRegisterPanel;
}

- (UIView *)transitionToCreateAccountInProgressState
{
    UINib *nib = [UINib nibWithNibName:@"ADXLoginProgressView" bundle:nil];
    [nib instantiateWithOwner:self options:nil];
    
    self.activityMessage.text = @"Sending registration request...";
    [self.activityIndicator startAnimating];
    
    return self.loginProgressPanel;
}

- (UIView *)transitionToRegistrationFailedState
{
    UINib *nib = [UINib nibWithNibName:@"ADXLoginFailedView" bundle:nil];
    [nib instantiateWithOwner:self options:nil];
    
    self.loginFailedMessage.text = self.loginOrRegistrationFailureMessage;
    self.loginOrRegistrationFailureMessage = nil;
    self.recentRegistrationFailed = YES;
    
    return self.loginFailedPanel;
}

- (UIView *)transitionToLoadingState
{
    UINib *nib = [UINib nibWithNibName:@"ADXLoginProgressView" bundle:nil];
    [nib instantiateWithOwner:self options:nil];
    
    self.activityMessage.text = nil;
    [self.activityIndicator startAnimating];
    
    return self.loginProgressPanel;
}

- (UIView *)transitionToAutoLoginState
{
    UINib *nib = [UINib nibWithNibName:@"ADXLoginProgressView" bundle:nil];
    [nib instantiateWithOwner:self options:nil];
    
    self.activityMessage.text = nil;
    [self.activityIndicator startAnimating];
    
    __weak ADXStartViewController *weakSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        [weakSelf processAutoLoginRequest];
    });
    
    return self.loginProgressPanel;
}

- (UIView *)transitionToLoginAccountState
{
    UINib *nib = [UINib nibWithNibName:@"ADXLoginView" bundle:nil];
    [nib instantiateWithOwner:self options:nil];

    ADXAppDelegate *appDelegate = [ADXAppDelegate sharedInstance];
    NSString *serverForURLRequest = nil;
    
    if (appDelegate.pendingURLSchemeRequest && appDelegate.pendingURLSchemeRequest.requiredServer) {
        // TODO: constructing an http:// URL from the server address (which is just servername:port) doesn't take https into account.
        serverForURLRequest = [NSString stringWithFormat:@"https://%@/", appDelegate.pendingURLSchemeRequest.requiredServer];
    }

    if (self.currentAccount && self.state != ADXStartViewStateLoginFailed) {
        self.loginNameField.text = self.currentAccount.loginName;
        self.serverField.text = [self trimmedServerURL:self.currentAccount.serverURL];
        self.passwordField.text = self.currentAccount.password;
        self.rememberPasswordSwitch.on = self.currentAccount.rememberPasswordValue ? YES : NO;
        
        if (appDelegate.pendingURLSchemeRequest && appDelegate.pendingURLSchemeRequest.requiredServer) {
            NSString *serverField = [NSString stringWithFormat:@"https://%@", appDelegate.pendingURLSchemeRequest.requiredServer];
            if ([self.serverField.text rangeOfString:serverField].location != 0) {
                // The URL request is for a different server.  Populate the login box with that server, and
                // clear out the login and password.
                // TODO: Check the database for a saved login/password combination for this server.
                self.loginNameField.text = nil;
                self.passwordField.text = nil;
                self.rememberPasswordSwitch.on = YES;
                self.serverField.text = serverForURLRequest;
            }
        }
        
    } else {
        self.loginNameField.text = self.currentLoginName;
        self.serverField.text = [self trimmedServerURL:self.currentServerURL];
        self.passwordField.text = self.currentPassword;
        self.rememberPasswordSwitch.on = self.currentRememberPassword;
    }
    
    [self validateLoginFields];
    
    return self.loginPanel;
}

- (UIView *)transitionToLoginInProgressState
{
    UINib *nib = [UINib nibWithNibName:@"ADXLoginProgressView" bundle:nil];
    [nib instantiateWithOwner:self options:nil];
    
    self.activityMessage.text = @"Processing login...";
    [self.activityIndicator startAnimating];
    
    return self.loginProgressPanel;
}

- (UIView *)transitionToLoginFailedState
{
    UINib *nib = [UINib nibWithNibName:@"ADXLoginFailedView" bundle:nil];
    [nib instantiateWithOwner:self options:nil];
    
    self.loginFailedMessage.text = self.loginOrRegistrationFailureMessage;
    self.loginOrRegistrationFailureMessage = nil;
    self.loginFailedCancelButton.hidden = YES;
    
    return self.loginFailedPanel;
}

- (UIView *)transitionToResetPasswordState
{
    UINib *nib = [UINib nibWithNibName:@"ADXLoginResetPasswordView" bundle:nil];
    [nib instantiateWithOwner:self options:nil];

    self.loginResetPasswordServerField.text = self.currentAccount.serverURL;
    self.loginResetPasswordEmailField.text = self.currentAccount.user.primaryEmail;
    
    return self.loginResetPanel;
}

- (UIView *)transitionToResetPasswordFailedState
{
    UINib *nib = [UINib nibWithNibName:@"ADXLoginFailedView" bundle:nil];
    [nib instantiateWithOwner:self options:nil];
    
    self.loginFailedMessage.text = self.loginOrRegistrationFailureMessage;
    self.loginOrRegistrationFailureMessage = nil;
    self.loginFailedCancelButton.hidden = NO;
    
    return self.loginFailedPanel;
}

- (UIView *)transitionToResetPasswordInProgressState
{
    UINib *nib = [UINib nibWithNibName:@"ADXLoginProgressView" bundle:nil];
    [nib instantiateWithOwner:self options:nil];
    
    self.activityMessage.text = @"Sending password reset request...";
    [self.activityIndicator startAnimating];
    
    return self.loginProgressPanel;
}

- (void)transitionToState:(ADXStartViewState)newState
{
    UIView *targetStateView = nil;
    
    switch (self.state) {
        case ADXStartViewStateLoginInProgress:
            self.activityMessage.text = nil;
            [self.activityIndicator stopAnimating];
            break;
            
        default:
            break;
    }
    
    switch (newState) {
        case ADXStartViewStateLoading:
            targetStateView = [self transitionToLoadingState];
            break;
            
        case ADXStartViewStatePerformHousekeepingTasks:
            targetStateView = [self transitionToPerformHousekeepingTasksState];
            break;
            
        case ADXStartViewStateAutoLogin:
            targetStateView = [self transitionToAutoLoginState];
            break;

        case ADXStartViewStateHaveOrNeedAccount:
            targetStateView = [self transitionToHaveOrNeedAccountState];
            break;
            
        case ADXStartViewStateCreateAccount:
            targetStateView = [self transitionToCreateAccountState];
            break;
            
        case ADXStartViewStateCreateAccountInProgress:
            targetStateView = [self transitionToCreateAccountInProgressState];
            break;
            
        case ADXStartViewStateCreateAccountFailed:
            targetStateView = [self transitionToRegistrationFailedState];
            break;

        case ADXStartViewStateLoginAccount:
            targetStateView = [self transitionToLoginAccountState];
            break;
            
        case ADXStartViewStateLoginInProgress:
            targetStateView = [self transitionToLoginInProgressState];
            break;
            
        case ADXStartViewStateLoginFailed:
            targetStateView = [self transitionToLoginFailedState];
            break;
            
        case ADXStartViewStateResetPassword:
            targetStateView = [self transitionToResetPasswordState];
            break;
            
        case ADXStartViewStateResetPasswordInProgress:
            targetStateView = [self transitionToResetPasswordInProgressState];
            break;

        case ADXStartViewStateResetPasswordFailed:
            targetStateView = [self transitionToResetPasswordFailedState];
            break;

        default:
            break;
    }

    if (self.currentlyDisplayedPanelView) {
        [UIView transitionFromView:self.currentlyDisplayedPanelView
                            toView:targetStateView
                          duration:0.4f
                           options:UIViewAnimationOptionTransitionCrossDissolve
                        completion:^(BOOL finished) {

                        }];
    } else {
        [self.panelContainer addSubview:targetStateView];
    }
    self.currentlyDisplayedPanelView = targetStateView;

    self.previousState = self.state;
    self.state = newState;
}

#pragma mark - Login

- (void)handleLoginSuccess:(ADXAccount *)account
{
    __weak ADXStartViewController *weakSelf = self;

    ADXV2Service *service = [ADXV2Service serviceForAccountID:account.id];
    if (service.networkReachable) {
        [self checkAPNSModeWithAccount:account completion:^(BOOL success, NSError *error) {
            [weakSelf finishLoginWithAccount:account];
        }];
    } else {
        [weakSelf finishLoginWithAccount:account];
    }
}

- (void)checkAPNSModeWithAccount:(ADXAccount *)account completion:(ADXCompletionBlock)completionBlock
{
    ADXV2Service *service = [ADXV2Service serviceForAccountID:account.id];
    if (!service) {
        if (completionBlock) {
            completionBlock(NO, nil);
        }
        return;
    }
    
    [[ADXAppDelegate sharedInstance] appAPNSModeIsCompatibleWithService:service completion:^(BOOL success, NSError *error) {
        if (success) {
            if (completionBlock) {
                completionBlock(YES, nil);
            }
        } else {
            PSPDFAlertView *alertView = [[PSPDFAlertView alloc] initWithTitle:@"Warning" message:@"This app is not compatible with the current server push notification configuration. Push notifications will not be received."];
            [alertView setCancelButtonWithTitle:@"OK" block:^{
                if (completionBlock) {
                    completionBlock(NO, nil);
                }
            }];
            [alertView show];
        }
    }];
}

- (void)showStartingInOfflineAlert:(NSString *)message
{
    NSString *finalMessage = [NSString stringWithFormat:@"%@\nStarting in offline mode.", message];
    PSPDFAlertView *alertView = [[PSPDFAlertView alloc] initWithTitle:@"Warning" message:finalMessage];
    [alertView setCancelButtonWithTitle:@"OK" block:^{
        //
    }];
    [alertView show];
}

- (void)finishLoginWithAccount:(ADXAccount *)account
{
    [[ADXUserDefaults sharedDefaults] setUserLoggedOut:NO];
    
    if ([ADXV2Service serviceForAccountID:account.id]) {
        LogInfo(@"Service is registered for account %@", account.id);
    } else {
        LogError(@"Service is not registered for account %@", account.id);
    }
    
    ADXAppDelegate *appDelegate = (ADXAppDelegate *)[[UIApplication sharedApplication] delegate];
    [appDelegate setReaderAppState:ADXAppState.startupEnded];
    [appDelegate setCurrentAccountByID:account.id];
    
    if (appDelegate.pendingURLSchemeRequest) {
        [appDelegate.pendingURLSchemeRequest applicationDidLoginWithAccount:account];
    }
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    UIViewController *initialViewController = [mainStoryboard instantiateInitialViewController];
    initialViewController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    
    __weak ADXStartViewController *weakSelf = self;
    
    double delayInSeconds = 1.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        dispatch_async(dispatch_get_main_queue(), ^{
            [[ADXAppDelegate sharedInstance] passCheckpoint:@"Completed login"];
            [weakSelf presentViewController:initialViewController animated:YES completion:nil];
            [weakSelf transitionToState:ADXStartViewStateLoading];
        });
    });
}

- (void)handleLoginFailure:(ADXAccount *)account message:(NSString *)message
{
    __weak ADXStartViewController *weakSelf = self;
    
    if (message) {
        weakSelf.loginOrRegistrationFailureMessage = message;
    } else {
        weakSelf.loginOrRegistrationFailureMessage = @"Login failed.";
    }
    
    [[ADXUserDefaults sharedDefaults] setUserLoggedOut:YES];

    double delayInSeconds = 1.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        dispatch_async(dispatch_get_main_queue(), ^{
            [weakSelf transitionToState:ADXStartViewStateLoginFailed];
        });
    });
}

- (void)processLogin
{
    __weak ADXStartViewController *weakSelf = self;
    
    [self.loginProcessor performLoginWithName:self.currentLoginName
                                     password:self.currentPassword
                                       server:self.currentServerURL
                             rememberPassword:self.currentRememberPassword
                                   completion:^(BOOL success, NSUInteger statusCode, BOOL online, ADXAccount *account, ADXV2Service *service, NSError *error) {
                                       dispatch_async(dispatch_get_main_queue(), ^{
                                           if (online) {
                                               if (success) {
                                                   [weakSelf handleLoginSuccess:account];
                                               } else {
                                                   [weakSelf handleLoginFailure:account message:@"Invalid login name or password."];
                                               }
                                           } else {
                                               NSString *message = nil;
                                               
                                               if (!service.networkReachable) {
                                                   // Try to contact a well-known URL before concluding lack of network reachability.
                                                   // We need to do this because if the user specified a very badly malformed URL, then
                                                   // we need to check first with a proper URL to determine network reachability.
                                                   NSError *testError = nil;
                                                   NSData *tempData = [NSData dataWithContentsOfURL:[NSURL URLWithString:@"http://www.savvydox.com/"] options:NSDataReadingUncached error:&testError];

                                                   if (!tempData) {
                                                       message = kADXOfflineNoConnectivity;
                                                   } else {
                                                       message = kADXOfflineNoServer;
                                                   }
                                               } else if (!service.serverReachable) {
                                                   message = kADXOfflineNoServer;
                                               }
                                               
                                               if (success) {
                                                   if (message) {
                                                       [weakSelf showStartingInOfflineAlert:message];
                                                   }
                                                   [weakSelf handleLoginSuccess:account];
                                               } else {
                                                   [weakSelf handleLoginFailure:account message:message];
                                               }
                                           }
                                       });
                                   }];
}

- (void)processAutoLoginRequest
{
    __weak ADXStartViewController *weakSelf = self;
    
    if (!self.currentAccount || IsEmpty(self.currentAccount.loginName) || IsEmpty(self.currentAccount.password) || IsEmpty(self.currentAccount.serverURL)) {
        [[ADXUserDefaults sharedDefaults] setUserLoggedOut:YES];
        [self transitionToState:ADXStartViewStateLoginAccount];
    } else {
        [self.loginProcessor performLoginWithName:self.currentAccount.loginName
                                         password:self.currentAccount.password
                                           server:self.currentAccount.serverURL
                                 rememberPassword:YES
                                       completion:^(BOOL success, NSUInteger statusCode, BOOL online, ADXAccount *account, ADXV2Service *service, NSError *error) {
                                           dispatch_async(dispatch_get_main_queue(), ^{
                                               if (success) {
                                                   NSString *message = nil;
                                                   if (!service.networkReachable) {
                                                       message = kADXOfflineNoConnectivity;
                                                   } else if (!service.serverReachable) {
                                                       message = kADXOfflineNoServer;
                                                   }
                                                   if (!online) {
                                                       [weakSelf showStartingInOfflineAlert:message];
                                                   }
                                                   [weakSelf handleLoginSuccess:account];
                                               } else {
                                                   // We think the server is reachable but the login was rejected, so make the user login.
                                                   dispatch_async(dispatch_get_main_queue(), ^{
                                                       [[ADXUserDefaults sharedDefaults] setUserLoggedOut:YES];
                                                       [weakSelf transitionToState:ADXStartViewStateLoginAccount];
                                                   });
                                               }
                                           });
                                       }];
    }
}

#pragma mark - Password Reset Processing

- (void)processResetPasswordRequest
{
    __weak ADXStartViewController *weakSelf = self;

    if (self.loginResetPasswordEmailField.text.length == 0 ||
        self.loginResetPasswordServerField.text.length == 0) {
        // Quick fail for no parameters, pending real input validation
        self.loginOrRegistrationFailureMessage = @"Password reset request failed.";
        [self transitionToState:ADXStartViewStateResetPasswordFailed];
        return;
    }
    
    [self.loginProcessor performPasswordResetWithServer:self.loginResetPasswordServerField.text
                                                  email:self.loginResetPasswordEmailField.text
                                             completion:^(BOOL success, NSError *error) {
                                                 
                                                 dispatch_async(dispatch_get_main_queue(), ^{
                                                     if (success) {
                                                         weakSelf.createOrLoginPanelBannerMessage = @"An email has been sent with information on how to complete your password reset.";
                                                         [weakSelf transitionToState:ADXStartViewStateLoginAccount];
                                                     } else {
                                                         self.loginOrRegistrationFailureMessage = @"Password reset request failed.";
                                                         [self transitionToState:ADXStartViewStateResetPasswordFailed];
                                                         
                                                     }
                                                 });
                                             }];
}

#pragma mark - Create Account Processing

- (void)handleCreateAccountSuccess:(ADXLoginProcessorRegistrationResponse)status
{
    __weak ADXStartViewController *weakSelf = self;
    
    if (status == ADXLoginProcessorRegistrationAcceptedForConfirmation) {
        double delayInSeconds = 1.0;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            dispatch_async(dispatch_get_main_queue(), ^{
                PSPDFAlertView *alertView = [[PSPDFAlertView alloc] initWithTitle:@"Registration"
                                                                          message:@"A response to your registration request will be sent to your email address."
                                                                         delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alertView show];
//                weakSelf.createOrLoginPanelBannerMessage = @"A response to your registration request will be sent to your email address.";
                [weakSelf transitionToState:ADXStartViewStateLoginAccount];
            });
        });
    } else {
        self.currentRememberPassword = NO;
        dispatch_async(dispatch_get_main_queue(), ^{
            [weakSelf processLogin];
        });
    }
}

- (void)handleCreateAccountFailure
{
    self.loginOrRegistrationFailureMessage = @"Registration request failed.";
    [self transitionToState:ADXStartViewStateCreateAccountFailed];
}

- (void)processCreateAccountRequest
{
    __weak ADXStartViewController *weakSelf = self;
    
    [self.loginProcessor registerWithName:self.currentLoginName
                                    email:self.currentLoginName
                                 password:self.currentPassword
                                   server:self.currentServerURL
                               completion:^(ADXLoginProcessorRegistrationResponse status, NSError *error) {
                                   if (status == ADXLoginProcessorRegistrationFailed) {
                                       dispatch_async(dispatch_get_main_queue(), ^{
                                           [weakSelf handleCreateAccountFailure];
                                       });
                                   } else {
                                       dispatch_async(dispatch_get_main_queue(), ^{
                                           [weakSelf handleCreateAccountSuccess:status];
                                       });
                                   }
                               }];
}

#pragma mark - View Management

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.buildInfoLabel.text = [[ADXAppDelegate sharedInstance] buildDescription];

    self.currentAccount = [self.loginProcessor defaultAccount];
    
    [self transitionToState:ADXStartViewStateLoading];
    [self transitionToPerformHousekeepingTasksState];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(processURLSchemeNotification:) name:kADXURLSchemeRequestNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleLogoutNotification:) name:kADXLogoutNotification object:nil];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)transitionToAppropriateStartingState
{
    // Used only to get a screenshot-ready empty view of the app for use as the default.png file.
    if ([self displayBlankStartupScreen]) {
        [self transitionToState:ADXStartViewStateLoading];
        self.activityMessage.text = nil;
        self.buildInfoLabel.text = nil;
        [self.activityIndicator stopAnimating];
        return;
    }
    
    [self setupLoginProcessor];
    
    ADXAccount *latestAccount = [self.loginProcessor defaultAccount];
    if (self.currentAccount != latestAccount) {
        self.currentAccount = latestAccount;
    }
    
    if (!self.currentAccount) {
//        [self transitionToState:ADXStartViewStateHaveOrNeedAccount];
        [self transitionToState:ADXStartViewStateLoginAccount];
    } else {
        BOOL userLoggedOut = [[ADXUserDefaults sharedDefaults] userLoggedOut];
        BOOL requireLogin = [[ADXUserDefaults sharedDefaults] requireLoginOnNextLaunch];
        BOOL didShutdownDirty = [[ADXUserDefaults sharedDefaults] didShutdownDirty];
        if (userLoggedOut) {
            LogInfo(@"Login screen will be displayed because user logged out.");
        }
        if (requireLogin) {
            LogInfo(@"Login screen will be displayed because 'require login' preference switch was set.");
        }
        if (!userLoggedOut && !requireLogin && didShutdownDirty) {
            LogInfo(@"Login screen will be displayed because previous run didn't shutdown cleanly.");
        }
        
        BOOL needToRequestLogin = userLoggedOut || requireLogin || didShutdownDirty;
        
        [[ADXUserDefaults sharedDefaults] setRequireLoginOnNextLaunch:NO];
        [[ADXUserDefaults sharedDefaults] setDidShutdownDirty:YES];
        
        if (needToRequestLogin) {
            [self forgetLastSessionState];
            [self transitionToState:ADXStartViewStateLoginAccount];
        } else {
            [self transitionToState:ADXStartViewStateAutoLogin];
        }
    }
}

- (void)forgetLastSessionState
{
    [ADXUserDefaults sharedDefaults].currentlyOpenDocumentID = nil;
    [ADXUserDefaults sharedDefaults].currentlyOpenCollectionID = nil;
}

#pragma mark - Actions

- (IBAction)didTapLoginButton:(id)sender
{
    [self transitionToState:ADXStartViewStateLoginInProgress];

    self.currentAccount = nil;
    
    self.currentLoginName = self.loginNameField.text;
    self.currentPassword = self.passwordField.text;
    self.currentRememberPassword = self.rememberPasswordSwitch.on;
    
    NSRange range = [self.serverField.text rangeOfString:@"://"];
    if (range.location == NSNotFound) {
        self.serverField.text = [NSString stringWithFormat:@"https://%@", self.serverField.text];
    }
    
    self.currentServerURL = self.serverField.text;
    [self.view endEditing:YES];
    [self processLogin];
}

- (IBAction)didTapHaveAnAccountButton:(id)sender
{
    [self transitionToState:ADXStartViewStateLoginAccount];
}

- (IBAction)didTapCreateAnAccountButton:(id)sender
{
    [self transitionToState:ADXStartViewStateCreateAccount];
}

- (IBAction)didTapLoginTryAgainButton:(id)sender
{
    if (self.state == ADXStartViewStateResetPasswordFailed) {
        [self transitionToState:ADXStartViewStateResetPassword];
    } else
    if (self.state == ADXStartViewStateCreateAccountFailed) {
        [self transitionToState:ADXStartViewStateCreateAccount];
    } else {
        [self transitionToState:ADXStartViewStateLoginAccount];
    }
}

- (IBAction)didTapRegisterCancelButton:(id)sender
{
    [self transitionToState:ADXStartViewStateLoginAccount];
}

- (IBAction)didTapLoginCancelButton:(id)sender
{
    [self transitionToState:ADXStartViewStateLoginAccount];
}

- (IBAction)didTapForgotPasswordButton:(id)sender
{
    [self transitionToState:ADXStartViewStateResetPassword];
}

- (IBAction)didTapResetPasswordButton:(id)sender
{
    [self transitionToState:ADXStartViewStateResetPasswordInProgress];
    [self processResetPasswordRequest];
}

- (IBAction)didTapCancelResetPasswordButton:(id)sender
{
    [self transitionToState:ADXStartViewStateLoginAccount];
}

- (IBAction)didTapFinishRegistrationButton:(id)sender
{
    [self transitionToState:ADXStartViewStateCreateAccountInProgress];
    
    self.currentLoginName = self.registerEmailField.text;
    self.currentPassword = self.registerPasswordField.text;
    self.currentServerURL = self.registerServerField.text;
    
    if (![self.registerPasswordField.text isEqual:self.registerPasswordConfirmField.text]) {
        self.loginOrRegistrationFailureMessage = @"Passwords do not match.";
        [self transitionToState:ADXStartViewStateCreateAccountFailed];
    } else {        
        [self processCreateAccountRequest];
    }
}

- (void)validateLoginFields
{
    BOOL enable = YES;
    
    if (self.loginNameField.text.length == 0) {
        enable = NO;
    }
    
    if (self.serverField.text.length == 0) {
        enable = NO;
    }
    
    NSURL *candidateURL = [NSURL URLWithString:self.serverField.text];
    if (!candidateURL) {
        enable = NO;
    }
    
    if ([self.serverField.text isEqualToString:@"http://"]) {
        enable = NO;
    }

    if ([self.serverField.text isEqualToString:@"https://"]) {
        enable = NO;
    }

    self.loginButton.enabled = enable;
}

- (IBAction)loginNameFieldChanged:(id)sender
{
    [self validateLoginFields];
}

- (IBAction)passwordFieldChanged:(id)sender
{
    [self validateLoginFields];
}

- (IBAction)serverFieldChanged:(id)sender
{
    [self validateLoginFields];
}

#pragma mark - URL scheme launch

- (void)urlHandlerOpenDocumentWithID:(NSString *)documentID
{
    NSAssert(false, @"urlHandlerOpenDocumentWithID before login (should not happen)");
}

-(void)urlHandlerShowLoginForUserID:(NSString *)userID
{
    [self transitionToState:ADXStartViewStateLoginAccount];
    if (![userID isEqualToString:self.loginNameField.text]) {
        self.loginNameField.text = userID;
        self.passwordField.text = @"";
    }
}

- (void)processPendingURLRequest
{
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        /// Use the URL from the launch request, if we have one
        ADXURLSchemeRequest *pendingRequest = [ADXAppDelegate sharedInstance].pendingURLSchemeRequest;
        if (pendingRequest != nil) {
            if (pendingRequest.requireThatAppHasNotLoggedIn) {
                // We're ready
                pendingRequest.delegate = self;
                [pendingRequest dispatch];
                [ADXAppDelegate sharedInstance].pendingURLSchemeRequest = nil;
            }
        }
    }];
}

- (void)processURLSchemeNotification:(NSNotification *)notification
{
    [self processPendingURLRequest];
}

- (void)handleLogoutNotification:(NSNotification *)notification
{
    [self transitionToPerformHousekeepingTasksState];
}

- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown;
}

@end
