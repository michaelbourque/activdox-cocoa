//
//  ADXBaseDocListViewController.m
//  Reader
//
//  Created by Gavin McKenzie on 2013-04-13.
//  Copyright (c) 2013 ActivDox Inc. All rights reserved.
//

#import "ADXBaseDocListViewController.h"
#import "ADXBaseDocListCell.h"
#import "ADXAppDelegate.h"
#import "ADXPopoverBackgroundView.h"
#import "ADXBarButtonItem.h"
#import "ADXPopoverBarButtonItem.h"

NSString * const kADXBaseDocListCellIdentifier = @"ADXBaseDocListCell";
NSString * const kADXBaseDocListAddRowCellIdentifier = @"ADXBaseDocListAddRowCell";

@interface ADXBaseDocListViewController () <NSFetchedResultsControllerDelegate>
@property (strong, nonatomic) UIView *searchBarDarkOverlay;
@property (assign, nonatomic) BOOL initialViewDidAppear;
@end

@implementation ADXBaseDocListViewController

#pragma mark - Class Methods

+ (UIPopoverController *)popoverControllerWithTitle:(NSString *)title allowEditing:(BOOL)allowEditing startInEditMode:(BOOL)startInEditMode allowMultiSelect:(BOOL)allowMultiSelect allowEditMultiSelect:(BOOL)allowEditMultiSelect actionDelegate:(id)actionDelegate
{
    UIPopoverController *popover = nil;
    ADXBaseDocListViewController *viewController = [[self alloc] initWithNibName:nil bundle:nil];
    viewController.requestedTitle = title;
    viewController.allowEditing = allowEditing;
    viewController.startInEditMode = startInEditMode;
    viewController.allowMultiSelect = allowMultiSelect;
    viewController.allowEditMultiSelect = allowEditMultiSelect;
    viewController.actionDelegate = actionDelegate;

    if (viewController) {
        popover = [[UIPopoverController alloc] initWithContentViewController:[[UINavigationController alloc] initWithRootViewController:viewController]];
        popover.popoverBackgroundViewClass = [ADXBluePopoverBackgroundView class];
    }
    viewController.parentPopoverController = popover;
    
    return popover;
}

#pragma mark - Initialization

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

#pragma mark - Accessors

- (void)setEditing:(BOOL)editing animated:(BOOL)animated
{
    [super setEditing:editing animated:animated];
    [self.tableView setEditing:editing animated:animated];
    [self editingStateDidChange:editing];
}

#pragma mark - View Management

- (NSString *)titleForCurrentViewMode
{
    NSString *result = @"Documents";
    if (self.requestedTitle) {
        result = self.requestedTitle;
    }
    return result;
}

- (void)disableSearchBar
{
    self.searchDisplayController.searchBar.userInteractionEnabled = NO;

    UIView *darkOverlay = [[UIView alloc] initWithFrame:self.searchDisplayController.searchBar.bounds];
    darkOverlay.backgroundColor = [UIColor blackColor];
    darkOverlay.alpha = 0.5f;
    [self.searchDisplayController.searchBar addSubview:darkOverlay];
    [self.searchDisplayController.searchBar bringSubviewToFront:darkOverlay];
    self.searchBarDarkOverlay = darkOverlay;
}

- (void)restoreSearchBar
{
    [self.searchBarDarkOverlay removeFromSuperview];
    self.searchDisplayController.searchBar.userInteractionEnabled = YES;
}

- (void)editingStateDidChange:(BOOL)editing
{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:([self numberOfItemsInSection:0 tableView:self.tableView]) inSection:0];

    [self updateNavBar];
    if (editing) {
        [self.tableView beginUpdates];
        [self.tableView insertRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
        [self.tableView endUpdates];
        
        [self disableSearchBar];
    } else {
        [self restoreSearchBar];
        
        [self.tableView beginUpdates];
        [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
        [self.tableView endUpdates];
        
        [self saveChanges];
    }
}

- (void)registerCellNibs
{
    UINib *nib = [UINib nibWithNibName:kADXBaseDocListCellIdentifier bundle:nil];
    [self.tableView registerNib:nib forCellReuseIdentifier:kADXBaseDocListCellIdentifier];
}

- (void)configureTableView
{
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"popover_bg"]];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.allowsSelectionDuringEditing = YES;

    [self registerCellNibs];
}

- (void)configureNavBar
{
    if (self.allowEditing) {
        UIBarButtonItem *separator = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
        separator.width = 8;
        self.navigationItem.rightBarButtonItems = @[ separator, self.editButtonItem ];
    }
    [self updateNavBar];
}

- (void)updateNavBar
{
    self.navigationItem.title = [self titleForCurrentViewMode];
}

- (CGSize)contentSizeForViewInPopover
{
    return CGSizeMake(320.0f, 544.0f);
}

- (void)refreshForTableView:(UITableView *)tableView
{
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        NSFetchedResultsController *frc = [self fetchedResultsControllerForTableView:tableView];
        if (frc == self.listFetchedResultsController) {
            [self prepareListFetchedResultsController];
        } else if (frc == self.searchFetchedResultsController) {
            [self prepareSearchFetchedResultsController];
        }
        [tableView reloadData];
    }];
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    if (!self.managedObjectContext) {
        ADXAppDelegate *appDelegate = (ADXAppDelegate *)[[UIApplication sharedApplication] delegate];
        self.managedObjectContext = appDelegate.managedObjectContext;
    }
    
    [self configureNavBar];
    [self configureTableView];
    
    [self prepareListFetchedResultsController];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    if (!self.initialViewDidAppear) {
        self.initialViewDidAppear = YES;
        if (self.allowEditing && self.startInEditMode && !self.editing) {
            self.editing = YES;
        }
    }
}

#pragma mark - Table view 

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    NSFetchedResultsController *frc = [self fetchedResultsControllerForTableView:tableView];
    NSInteger result = [[frc sections] count];
    return result;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger result = [self numberOfItemsInSection:section tableView:tableView];
    if (self.tableView.editing) {
        result++;
    }
    return result;
}

- (void)configureCell:(UITableViewCell *)cell indexPath:(NSIndexPath *)indexPath tableView:(UITableView *)tableView
{
    ADXBaseDocListCell *listCell = (ADXBaseDocListCell *)cell;
    ADXDocument *document = [self objectAtIndexPath:indexPath tableView:tableView];
    
    listCell.primaryLabel.text = document.title;
    listCell.secondaryLabel.text = document.author.displayNameOrLoginName;
    listCell.thumbnailImageView.image = [UIImage imageWithData:document.thumbnail.data];
}

- (UITableViewCell *)cellForAddRow
{
    UITableViewCell *addRowCell = [self.tableView dequeueReusableCellWithIdentifier:kADXBaseDocListAddRowCellIdentifier];
    if (!addRowCell) {
        addRowCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kADXBaseDocListAddRowCellIdentifier];
        addRowCell.textLabel.textColor = [UIColor adxPopoverTextColor];
        addRowCell.textLabel.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:15.0f];
        addRowCell.accessoryType = UITableViewCellAccessoryNone;
    }
    addRowCell.textLabel.text = [self messageForAddRowCell];

    return addRowCell;
}

- (NSString *)messageForAddRowCell
{
    return @"Add Document";
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 100.0f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = nil;
    
    if (self.editing && indexPath.row >= [self numberOfItemsInSection:indexPath.section tableView:tableView]) {
        cell = [self cellForAddRow];
    } else {
        // Important to deque from our main tableView, regardless of whether the search-mode tableView is displayed.
        ADXBaseDocListCell *docListCell = (ADXBaseDocListCell *)[self.tableView dequeueReusableCellWithIdentifier:kADXBaseDocListCellIdentifier];
        if (docListCell == nil) {
            docListCell = [[ADXBaseDocListCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kADXBaseDocListCellIdentifier];
        }
        [self configureCell:docListCell indexPath:indexPath tableView:tableView];
        cell = docListCell;
    }
    
    return cell;
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (!self.allowEditing) {
        return UITableViewCellEditingStyleNone;
    }
    
    UITableViewCellEditingStyle result = UITableViewCellEditingStyleNone;
    NSInteger numberOfRows = [self numberOfItemsInSection:indexPath.section tableView:tableView];
    
    if (indexPath.row >= numberOfRows) {
        result = UITableViewCellEditingStyleInsert;
    } else {
        result = UITableViewCellEditingStyleDelete;
    }
    return result;
}

- (BOOL)tableView:(UITableView *)tableView shouldHighlightRowAtIndexPath:(NSIndexPath *)indexPath
{
    BOOL result = NO;
    if (self.editing && indexPath.row == ([self.tableView numberOfRowsInSection:indexPath.section] - 1)) {
        result = YES;
    } else if (!self.editing) {
        result = YES;
    }
    return result;
}

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSIndexPath *result = indexPath;
    if (self.editing && indexPath.row != ([self.tableView numberOfRowsInSection:indexPath.section] - 1)) {
        result = nil;
    }
    return result;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - Data Management

- (void)saveChanges
{
    ADXAppDelegate *appDelegate = (ADXAppDelegate *)[[UIApplication sharedApplication] delegate];
    [appDelegate saveMainContext:nil];
}

- (NSInteger)numberOfItemsInSection:(NSInteger)section tableView:(UITableView *)tableView
{
    NSFetchedResultsController *frc = [self fetchedResultsControllerForTableView:tableView];
    id <NSFetchedResultsSectionInfo> sectionInfo = [[frc sections] objectAtIndex:section];
    NSInteger result = [sectionInfo numberOfObjects];
    return result;
}

- (id)objectAtIndexPath:indexPath tableView:(UITableView *)tableView
{
    NSFetchedResultsController *frc = [self fetchedResultsControllerForTableView:tableView];
    id object = [frc objectAtIndexPath:indexPath];
    return object;
}

- (NSPredicate *)predicateForListFetchedResultsController
{
    NSPredicate *predicate = nil;
    predicate = [NSPredicate predicateWithFormat:@"(%K == %@) AND (%K == %@)", ADXDocumentAttributes.accountID, self.accountID,  ADXDocumentAttributes.versionIsLatest, [NSNumber numberWithBool:YES]];
    return predicate;
}

- (NSSortDescriptor *)sortDescriptorForListFetchedResultsController
{
    NSSortDescriptor *sortDescriptor = nil;
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:ADXDocumentAttributes.title ascending:YES selector:@selector(localizedCaseInsensitiveCompare:)];
    return sortDescriptor;
}

- (NSPredicate *)predicateForSearchFetchedResultsController
{
    NSPredicate *predicate = nil;
    NSString *searchText = self.searchDisplayController.searchBar.text;
    
    predicate = [NSPredicate predicateWithFormat:@"(%K == %@) AND (%K == %@) AND (%K CONTAINS[cd] %@)",
                 ADXDocumentAttributes.accountID, self.accountID,
                 ADXDocumentAttributes.versionIsLatest, [NSNumber numberWithBool:YES],
                 ADXDocumentAttributes.title, searchText];
    return predicate;
}

- (NSSortDescriptor *)sortDescriptorForSearchFetchedResultsController
{
    return [self sortDescriptorForListFetchedResultsController];
}

- (NSFetchedResultsController *)fetchedResultsControllerForTableView:(UITableView *)tableView
{
    NSFetchedResultsController *result = nil;
    if (tableView == self.tableView) {
        result = self.listFetchedResultsController;
    } else {
        result = self.searchFetchedResultsController;
    }
    return result;
}

- (void)refetchFetchedResultsControllerForTableView:(UITableView *)tableView
{
    __weak NSFetchedResultsController *frc = [self fetchedResultsControllerForTableView:tableView];
    
    [self.managedObjectContext performBlockAndWait:^{
        NSError *error = nil;
        BOOL success = [frc performFetch:&error];
        if (!success) {
            LogNSError(@"Couldn't initialize NSFetchedResultsController", error);
        }
    }];
}

- (void)prepareListFetchedResultsController
{
    __weak ADXBaseDocListViewController *weakSelf = self;
    __block NSFetchedResultsController *frc = nil;
    
    if (self.listFetchedResultsController) {
        self.listFetchedResultsController.delegate = nil;
        self.listFetchedResultsController = nil;
    }
    
    NSPredicate *predicate = [self predicateForListFetchedResultsController];
    NSSortDescriptor *sortDescriptor = [self sortDescriptorForListFetchedResultsController];
    
    [self.managedObjectContext performBlockAndWait:^{
        frc = [weakSelf.managedObjectContext aps_fetchedResultsControllerForEntityName:[ADXDocument entityName]
                                                                             predicate:predicate
                                                                       sortDescriptors:[NSArray arrayWithObject:sortDescriptor]
                                                                    sectionNameKeyPath:nil
                                                                             cacheName:nil
                                                                              delegate:weakSelf];
        NSError *error = nil;
        BOOL success = [frc performFetch:&error];
        if (!success) {
            LogNSError(@"Couldn't initialize NSFetchedResultsController", error);
        }
    }];
    
    self.listFetchedResultsController = frc;
    self.listFetchedResultsController.delegate = self;
}

- (void)prepareSearchFetchedResultsController
{
    __weak ADXBaseDocListViewController *weakSelf = self;
    __block NSFetchedResultsController *frc = nil;
    
    NSPredicate *predicate = [self predicateForSearchFetchedResultsController];
    NSSortDescriptor *sortDescriptor = [self sortDescriptorForSearchFetchedResultsController];
    
    [self.managedObjectContext performBlockAndWait:^{
        frc = [weakSelf.managedObjectContext aps_fetchedResultsControllerForEntityName:[ADXDocument entityName]
                                                                             predicate:predicate
                                                                       sortDescriptors:[NSArray arrayWithObject:sortDescriptor]
                                                                    sectionNameKeyPath:nil
                                                                             cacheName:nil
                                                                              delegate:weakSelf];
        NSError *error = nil;
        BOOL success = [frc performFetch:&error];
        if (!success) {
            LogNSError(@"Couldn't initialize NSFetchedResultsController", error);
        }
    }];
    
    self.searchFetchedResultsController = frc;
}

#pragma mark - UISearchDisplayDelegate

- (void)searchDisplayControllerWillBeginSearch:(UISearchDisplayController *)controller
{
    // Do nothing
}

- (void)searchDisplayControllerDidBeginSearch:(UISearchDisplayController *)controller
{
    controller.searchResultsTableView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"popover_bg"]];
    controller.searchResultsTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    controller.searchResultsTableView.rowHeight = 100;
    controller.searchResultsTableView.allowsMultipleSelection = YES;
    controller.searchResultsTableView.allowsMultipleSelectionDuringEditing = YES;
}

- (void)searchDisplayController:(UISearchDisplayController *)controller willShowSearchResultsTableView:(UITableView *)tableView
{
    // Do nothing
}

- (BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString
{
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        [self refreshForTableView:controller.searchResultsTableView];
    }];
    return NO;
}

- (BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchScope:(NSInteger)searchOption
{
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        [self refreshForTableView:controller.searchResultsTableView];
    }];
    return NO;
}

- (void)searchDisplayControllerDidEndSearch:(UISearchDisplayController *)controller
{
    if (self.editing) {
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            [self refreshForTableView:controller.searchResultsTableView];
        }];
    }
}

- (void)searchDisplayController:(UISearchDisplayController *)controller willUnloadSearchResultsTableView:(UITableView *)tableView
{
    self.searchFetchedResultsController = nil;
}

#pragma mark - NSFetchedResultsControllerDelegate

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
    [self.tableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller
  didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex
     forChangeType:(NSFetchedResultsChangeType)type
{
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex]
                          withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex]
                          withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controller:(NSFetchedResultsController *)controller
   didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath
     forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath
{
    UITableView *tableView = nil;
    if (controller == self.listFetchedResultsController) {
        tableView = self.tableView;
    } else
    if (controller == self.searchFetchedResultsController) {
        tableView = self.searchDisplayController.searchResultsTableView;
    }
    NSAssert(tableView != nil, @"Unrecognized controller");
    
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [self configureCell:[tableView cellForRowAtIndexPath:indexPath] indexPath:indexPath tableView:tableView];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    [self.tableView endUpdates];
}


@end
