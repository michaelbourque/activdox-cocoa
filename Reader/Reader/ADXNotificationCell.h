//
//  ADXNotificationCell.h
//  Reader
//
//  Created by Gavin McKenzie on 2013-02-14.
//  Copyright (c) 2013 ActivDox Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

extern CGFloat const kADXNotificationPopoverSimpleCellHeight;
extern CGFloat const kADXNotificationPopoverCellHeight;
extern CGFloat const kADXNotificationPopoverCellWidth;

@interface ADXNotificationCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *thumbnailImageView;
@property (weak, nonatomic) IBOutlet UILabel *title;
@property (weak, nonatomic) IBOutlet UILabel *message;
@property (weak, nonatomic) IBOutlet UILabel *timeStamp;
@property (weak, nonatomic) IBOutlet UILabel *notAvailableMessage;
@end
