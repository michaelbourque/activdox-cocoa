//
//  ADXNotificationsSimpleCell.h
//  Reader
//
//  Created by Gavin McKenzie on 2013-05-08.
//  Copyright (c) 2013 ActivDox Inc. All rights reserved.
//

#import "ADXNotificationCell.h"

@interface ADXNotificationSimpleCell : ADXNotificationCell

@end
