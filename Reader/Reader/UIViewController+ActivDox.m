//
//  UIViewController+ActivDox.m
//  Reader
//
//  Created by Gavin McKenzie on 2012-09-17.
//  Copyright (c) 2012 ActivDox Inc. All rights reserved.
//

#import "UIViewController+ActivDox.h"

@implementation UIViewController (ActivDox)

- (UIBarButtonItem *)adx_barButtonItemWithCustomView:(UIView *)view
{
    __block UIBarButtonItem *barButtonItem = nil;
    
    NSMutableArray *buttonItems = [[NSMutableArray alloc] init];
    [buttonItems addObjectsFromArray:self.navigationItem.leftBarButtonItems];
    [buttonItems addObjectsFromArray:self.navigationItem.rightBarButtonItems];
    
    [buttonItems enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        if ([(UIBarButtonItem *)obj customView] == view) {
            barButtonItem = obj;
            *stop = YES;
        }
    }];
    
    return barButtonItem;
}

@end
