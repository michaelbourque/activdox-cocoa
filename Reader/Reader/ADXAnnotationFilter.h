//
//  ADXAnnotationFilter.h
//  Reader
//
//  Created by Steve Tibbett on 2013-04-16.
//  Copyright (c) 2013 ActivDox Inc. All rights reserved.
//

#import "ActivDoxCore.h"

@interface ADXAnnotationFilter : NSObject

- (BOOL)shouldFilterAnnotation:(ADXDocumentNote *)note onDocument:(ADXDocument *)adxDocument;

@end
