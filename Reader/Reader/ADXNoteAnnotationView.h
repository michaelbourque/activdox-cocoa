//
//  ADXNoteAnnotationView.h
//  Reader
//
//  Created by Steve Tibbett on 2013-04-12.
//  Copyright (c) 2013 ActivDox Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "ActivDoxCore.h"
#import "PSPDFNoteAnnotationView.h"

@interface ADXNoteAnnotationView : PSPDFNoteAnnotationView

@end
