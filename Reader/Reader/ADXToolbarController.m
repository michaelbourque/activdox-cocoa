//
//  ADXToolbarController.m
//  bottomBar
//
//  Created by matteo ugolini on 2012-10-13.
//  Copyright (c) 2012 matteo ugolini. All rights reserved.
//

#import "ADXToolbarController.h"
#import "ADXAnnotationViewController.h"
#import "ADXRevisionsViewController.h"
#import "ADXSearchViewController.h"
#import "ADXAppDelegate.h"

@interface ADXToolbarController () <ADXToolbarDelegate>
@property (nonatomic) BOOL delegateShouldSelectController;
@property (nonatomic) BOOL delegateDidSelectController;
@property (nonatomic) BOOL delegateDidDeselectController;
@property (nonatomic, strong) ADXToolbar *toolBar;
@end

@implementation ADXToolbarController

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.toolBar = [[ADXToolbar alloc] initWithFrame:CGRectMake(0, 0, 768, 44)];
    self.toolBar.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    self.toolBar.toolbarDelegate = self;
    [self.view addSubview:self.toolBar];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    CGRect r = self.view.frame;
    [self.toolBar initToolbarForViewInFrame:r];
    [self refreshToolbarElementsAnimated:NO];
}

#pragma mark - private

- (void) refreshToolbarElementsAnimated:(BOOL)animated
{
    [self.toolBar removeAllStatuses];
    
    int index = 0;
    for (UIViewController *c in self.viewControllers)
    {
        if ( c.tabBarItem.image ) {
            [self.toolBar insertStatusWithImage:c.tabBarItem.image atIndex:index animated:animated];
        } else if (c.tabBarItem.title) {
            //FIXME: no more disponible, maibe put him back ?
            //[self.toolBar insertStatusWithTitle:c.tabBarItem.title atIndex:index animated:NO];
        }
        
        if ( c.toolbarItems ){
            [self.toolBar setItems:c.toolbarItems forStatus:index animated:animated];
        }
        
        index++;
    }
}

- (void)setSelectedViewController:(UIViewController *)selectedViewController atIndex:(NSUInteger)index
{
    if( !self.delegateShouldSelectController || [self.delegate toolBarController:self shouldSelectViewController:selectedViewController] )
    {
        _selectedViewController = selectedViewController;
        if( self.delegateDidSelectController ) {
            [self.delegate toolBarController:self didSelectViewController:selectedViewController];
        }

        self.toolBar.currentStatus = index;
    }
}

#pragma mark - accessor

- (void)setSelectedViewController:(UIViewController *)selectedViewController
{
    NSUInteger controllerIndex = [self.viewControllers indexOfObject:selectedViewController];
    if (controllerIndex != NSNotFound ){
        [self setSelectedViewController:selectedViewController atIndex:controllerIndex];
    }
}

- (void)setSelectedIndex:(NSUInteger)selectedIndex
{
    if ( selectedIndex < self.viewControllers.count ) {
        [self setSelectedViewController:[self.viewControllers objectAtIndex:selectedIndex] atIndex:selectedIndex];
    }
}

- (NSUInteger)selectedIndex
{
    return self.toolBar.currentStatus;
}

- (void)setViewControllers:(NSArray *)viewControllers
{
    [self setViewControllers:viewControllers animated:YES];
}

- (void)setViewControllers:(NSArray *)viewControllers animated:(BOOL)animated
{
    _viewControllers = viewControllers;
    [self refreshToolbarElementsAnimated:animated];
}

- (void)setDelegate:(id<ADXToolbarControllerDelegate>)delegate
{
    _delegate = delegate;
    
    if ( !delegate ) {
        _delegateShouldSelectController = _delegateDidDeselectController = _delegateDidSelectController = NO;
    } else {
        _delegateShouldSelectController = [delegate respondsToSelector:@selector(toolBarController:shouldSelectViewController:)];
        _delegateDidSelectController =  [delegate respondsToSelector:@selector(toolBarController:didSelectViewController:)];
        _delegateDidDeselectController =  [delegate respondsToSelector:@selector(toolBarController:didDeselectViewController:)];
    }
}

#pragma mark - ADXToolbarDelegate

- (BOOL)toolbarControl:(ADXToolbar*)tabBarControl shouldChangeStatus:(NSUInteger)newStatus
{
    return !self.delegateShouldSelectController || [self.delegate toolBarController:self shouldSelectViewController:[self.viewControllers objectAtIndex:newStatus]];
}

- (void)toolbarControl:(ADXToolbar*)tabBarControl didSelectedStatus:(NSUInteger)status
{
    //fixme: what if are equals
    if( self.delegateDidDeselectController ) {
        [self.delegate toolBarController:self didDeselectViewController:self.selectedViewController];
    }
    self.selectedIndex = status;
    self.toolBar.currentStatus = status;

    if ([self.selectedViewController isKindOfClass:[ADXAnnotationViewController class]]) {
        [[ADXAppDelegate sharedInstance] passCheckpoint:@"Switched to the annotation toolbar"];
    }
    
    if ([self.selectedViewController isKindOfClass:[ADXRevisionsViewController class]]) {
        [[ADXAppDelegate sharedInstance] passCheckpoint:@"Switched to the revisions toolbar"];
    }
    
    if ([self.selectedViewController isKindOfClass:[ADXSearchViewController class]]) {
        [[ADXAppDelegate sharedInstance] passCheckpoint:@"Switched to the search toolbar"];
    }

}

@end


