//
//  ADXNotificationCell.m
//  Reader
//
//  Created by Gavin McKenzie on 2013-02-14.
//  Copyright (c) 2013 ActivDox Inc. All rights reserved.
//

#import "ADXNotificationCell.h"

#import <QuartzCore/QuartzCore.h>

CGFloat const kADXNotificationPopoverSimpleCellHeight = 66.0f;
CGFloat const kADXNotificationPopoverCellHeight = 120.0f;
CGFloat const kADXNotificationPopoverCellWidth = 320.0f;

@implementation ADXNotificationCell

- (void)awakeFromNib
{
    self.thumbnailImageView.layer.shadowColor = [UIColor blackColor].CGColor;
    self.thumbnailImageView.layer.shadowOffset = CGSizeMake(0, 1.0);
    self.thumbnailImageView.layer.shadowOpacity = 0.5;
    self.thumbnailImageView.layer.shadowRadius = 1.0;
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
