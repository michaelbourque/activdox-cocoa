//
//  ADXCollectionHeaderView.h
//  Reader
//
//  Created by Gavin McKenzie on 2013-04-30.
//  Copyright (c) 2013 ActivDox Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ADXCollectionController.h"

@class ADXCollectionHeaderView;

@protocol ADXCollectionHeaderViewDelegate <NSObject>
- (void)collectionHeaderView:(ADXCollectionHeaderView *)view didSelectSortOrder:(ADXCollectionControllerSort)order;
@end

@interface ADXCollectionHeaderView : UICollectionReusableView
@property (weak, nonatomic) IBOutlet UISegmentedControl *sortSegmentedButtons;
@property (weak, nonatomic) id<ADXCollectionHeaderViewDelegate> actionDelegate;
- (IBAction)didSelectSortOrder:(id)sender;
@end
