//
//  ADXPSPDFNoteAnnotationController.m
//  Reader
//
//  Created by Steve Tibbett on 2012-11-21.
//  Copyright (c) 2012 ActivDox Inc. All rights reserved.
//

#import "ADXPSPDFNoteAnnotationController.h"
#import "ADXAppDelegate.h"
#import "ADXNoteAnnotation.h"

#import "PSPDFPopoverController.h"
#import "PSPDFNoteAnnotationController.h"
#import "PSPDFActionSheet.h"
#import "PSPDFAnnotationParser.h"

#import "MBProgressHUD.h"

#define SEGMENT_PERSONAL 0
#define SEGMENT_AUTHOR 1
#define SEGMENT_EVERYONE 2

@interface ADXPSPDFNoteAnnotationController()
@property (strong, nonatomic) UISegmentedControl *noteVisibilitySegmentedControl;
@property (strong, nonatomic) MBProgressHUD *progressHUD;
@end


@implementation ADXPSPDFNoteAnnotationController

#pragma mark - Accessors

- (void)setAnnotation:(PSPDFAnnotation *)annotation
{
    if ([annotation isMemberOfClass:[ADXNoteAnnotation class]]) {
        [super setAnnotation:annotation];
        [self setupNoteReadWrite];
        [self refresh];
    } else {
        LogError(@"Received an annotation that isn't an ADXNoteAnnotation: %@", annotation);
        NSAssert(NO, @"Received an annotation that isn't an ADXNoteAnnotation.");
    }
}

- (PSPDFAnnotation *)annotation
{
    PSPDFAnnotation *theAnnotation = [super annotation];
    
    if (![theAnnotation isMemberOfClass:[ADXNoteAnnotation class]]) {
        LogError(@"Cannot handle an annotation that isn't an ADXNoteAnnotation: %@", self.annotation);
        return nil;
    }
    return theAnnotation;
}

#pragma mark - View Setup

- (void)viewDidLoad
{
    [super viewDidLoad];

    [self setupNavigationBarItems];
    [self setupToolbar];
    [self setupNoteReadWrite];
    
    self.textView.font = [UIFont fontWithName:@"HelveticaNeue" size:15.0f];
    
    [self refresh];
}

- (void)setupNoteReadWrite
{
    if (![ADXAnnotationUtil currentUserIsAuthorOfAnnotation:(id<ADXAnnotationProtocol>)self.annotation]) {
        self.textView.editable = NO;
    } else {
        self.textView.editable = YES;
    }
}

- (void)setupNavigationBarItems
{
    // Find and remove the Edit button that the original class added
    NSMutableArray *array = [NSMutableArray array];
    for (UIBarButtonItem *item in self.navigationItem.leftBarButtonItems) {
        if (item.action != @selector(toggleOptions:)) {
            [array addObject:item];
        }
    }
    
    self.navigationItem.leftBarButtonItems = array;
    self.navigationItem.rightBarButtonItem = nil;
    
    BOOL userIsAuthorOfNote = [ADXAnnotationUtil currentUserIsAuthorOfAnnotation:(id<ADXAnnotationProtocol>)self.annotation];
    BOOL userIsAuthorOfDocument = [ADXAnnotationUtil currentUserIsAuthorOfDocumentForAnnotation:(id<ADXAnnotationProtocol>)self.annotation];

    if (userIsAuthorOfNote) {
        UIBarButtonItem *deleteItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemTrash target:self action:@selector(deleteAnnotation:)];
        self.navigationItem.leftBarButtonItems = @[deleteItem];
    }
    if (userIsAuthorOfNote || userIsAuthorOfDocument) {
        ADXDocumentNote *note = [ADXAnnotationUtil noteFromAnnotation:(id<ADXAnnotationProtocol>)self.annotation];
        NSString *buttonLabel = nil;
        if (note.isClosed) {
            buttonLabel = NSLocalizedString(@"Reopen", nil);
        } else {
            buttonLabel = NSLocalizedString(@"Resolve", nil);
        }

        UIBarButtonItem *resolveItem = [[UIBarButtonItem alloc] initWithTitle:buttonLabel style:UIBarButtonItemStyleBordered target:self action:@selector(toggleAnnotationResolved:)];
        self.navigationItem.rightBarButtonItem = resolveItem;
    }
}

- (void)setupToolbar
{
    [self.navigationController setToolbarHidden:NO animated:NO];
    
    if ([ADXAnnotationUtil currentUserIsAuthorOfAnnotation:(id<ADXAnnotationProtocol>)self.annotation]) {
        [self setupToolbarForVisibilityToggle];
    } else {
        [self setupToolbarForShowingAuthor];
    }
}

- (void)setupToolbarForVisibilityToggle
{
    // Add the public/private segmented control
    NSArray *items = @[
                       NSLocalizedString(@"Personal", nil),
                       NSLocalizedString(@"Author", nil),
                       NSLocalizedString(@"Everyone", nil)
                       ];
    
    self.noteVisibilitySegmentedControl = [[UISegmentedControl alloc] initWithItems:items];
    self.noteVisibilitySegmentedControl.segmentedControlStyle = UISegmentedControlStyleBar;
    
    [self.noteVisibilitySegmentedControl addTarget:self
                                            action:@selector(visibilityValueChanged:)
                                  forControlEvents:UIControlEventValueChanged];
    
    UIBarButtonItem *toggleItem = [[UIBarButtonItem alloc] initWithCustomView:self.noteVisibilitySegmentedControl];
    
    UIBarButtonItem *spacer1 = [[UIBarButtonItem alloc]
                                initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                target:nil
                                action:nil];
    
    UIBarButtonItem *spacer2 = [[UIBarButtonItem alloc]
                                initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                target:nil
                                action:nil];

    self.toolbarItems = @[spacer1, toggleItem, spacer2];
}

- (void)setupToolbarForShowingAuthor
{
    ADXDocumentNote *note = [ADXAnnotationUtil noteFromAnnotation:(id<ADXAnnotationProtocol>)self.annotation];

    UILabel *authorLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 250.0f, 22.0f)];
    authorLabel.textColor = [UIColor whiteColor];
    authorLabel.backgroundColor = [UIColor clearColor];
    authorLabel.text = note.author.displayNameOrLoginName;
    authorLabel.textAlignment = NSTextAlignmentCenter;
    UIBarButtonItem *authorItem = [[UIBarButtonItem alloc] initWithCustomView:authorLabel];
    
    UIBarButtonItem *spacer1 = [[UIBarButtonItem alloc]
                                initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                target:nil
                                action:nil];
    
    UIBarButtonItem *spacer2 = [[UIBarButtonItem alloc]
                                initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                target:nil
                                action:nil];

    self.toolbarItems = @[spacer1, authorItem, spacer2];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];

    // Undo segmented control appearance proxy
    [self.noteVisibilitySegmentedControl setTitleTextAttributes:nil forState:UIControlStateNormal];
    [self.noteVisibilitySegmentedControl setTitleTextAttributes:nil forState:UIControlStateSelected];
    [self.noteVisibilitySegmentedControl setBackgroundImage:nil forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    [self.noteVisibilitySegmentedControl setBackgroundImage:nil forState:UIControlStateSelected barMetrics:UIBarMetricsDefault];
    [self.noteVisibilitySegmentedControl setDividerImage:nil
                                     forLeftSegmentState:UIControlStateNormal
                                       rightSegmentState:UIControlStateNormal
                                              barMetrics:UIBarMetricsDefault];
    [self.noteVisibilitySegmentedControl setDividerImage:nil
                                     forLeftSegmentState:UIControlStateSelected
                                       rightSegmentState:UIControlStateSelected
                                              barMetrics:UIBarMetricsDefault];
    [self.noteVisibilitySegmentedControl setDividerImage:nil
                                     forLeftSegmentState:UIControlStateNormal
                                       rightSegmentState:UIControlStateSelected
                                              barMetrics:UIBarMetricsDefault];
    [self.noteVisibilitySegmentedControl setDividerImage:nil
                                     forLeftSegmentState:UIControlStateSelected
                                       rightSegmentState:UIControlStateNormal
                                              barMetrics:UIBarMetricsDefault];
}

#pragma mark - View Management

- (void)refresh
{
    self.noteVisibilitySegmentedControl.selectedSegmentIndex = SEGMENT_PERSONAL;
    
    ADXNoteAnnotation *noteAnnotation = (ADXNoteAnnotation *)self.annotation;
    if ([noteAnnotation.noteVisibility isEqualToString:ADXDocumentNoteVisibility.author]) {
        self.noteVisibilitySegmentedControl.selectedSegmentIndex = SEGMENT_AUTHOR;
    } else if ([noteAnnotation.noteVisibility isEqualToString:ADXDocumentNoteVisibility.everyone]) {
        self.noteVisibilitySegmentedControl.selectedSegmentIndex = SEGMENT_EVERYONE;
    }
}

- (void)visibilityValueChanged:(id)sender
{
    NSString *newValue = nil;

    switch (self.noteVisibilitySegmentedControl.selectedSegmentIndex) {
        case SEGMENT_AUTHOR:
            newValue = ADXDocumentNoteVisibility.author;
            break;
        case SEGMENT_EVERYONE:
            newValue = ADXDocumentNoteVisibility.everyone;
            break;
        default:
            NSAssert(nil, @"Unexpected segment selected");
        case SEGMENT_PERSONAL:
            newValue = ADXDocumentNoteVisibility.personal;
            break;
    }

    [ADXUserDefaults sharedDefaults].defaultNoteVisibility = newValue;

    PSPDFAnnotation *originalAnnotation = self.annotation;
    PSPDFAnnotation *newAnnotation = [self.annotation copyAndDeleteOriginalIfNeeded];
    if (originalAnnotation == newAnnotation) {
        originalAnnotation = [self.annotation copy]; // ensure original annotation doesn't change
    }
    ADXNoteAnnotation *noteAnnotation = (ADXNoteAnnotation *)newAnnotation;
    noteAnnotation.noteVisibility = newValue;
    
    self.annotation = newAnnotation;
    
    if ([self.delegate respondsToSelector:@selector(noteAnnotationController:didChangeAnnotation:originalAnnotation:)]) {
        [self.delegate noteAnnotationController:self didChangeAnnotation:self.annotation originalAnnotation:originalAnnotation];
    }
    
}

- (void)deleteAnnotation:(UIBarButtonItem *)barButtonItem
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Confirmation", nil)
                                                    message:NSLocalizedString(@"Deleting this note will remove it from all versions of this document.  Are you sure?", nil)
                                                   delegate:self
                                          cancelButtonTitle:NSLocalizedString(@"Cancel", nil)
                                          otherButtonTitles:NSLocalizedString(@"Delete", nil), nil];
    [alert show];
}

- (void)toggleAnnotationResolved:(PSPDFAnnotation *)annotation
{
    if ([self.delegate respondsToSelector:@selector(noteAnnotationController:didToggleAnnotationResolved:)]) {
        ADXDocumentNote *note = [ADXAnnotationUtil noteFromAnnotation:(id<ADXAnnotationProtocol>)self.annotation];
        
        if (self.progressHUD) {
            [self.progressHUD hide:NO];
            self.progressHUD = nil;
        }
        
        self.progressHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        self.progressHUD.mode = MBProgressHUDModeText;
        self.progressHUD.removeFromSuperViewOnHide = YES;
        self.progressHUD.labelText = note.isClosed ? NSLocalizedString(@"Marked as not resolved", nil) : NSLocalizedString(@"Marked note as resolved", nil);
        [self.progressHUD show:YES];
        
        [(id<ADXPSPDFNoteAnnotationControllerDelegate>)self.delegate noteAnnotationController:self didToggleAnnotationResolved:self.annotation];
        [self setupNavigationBarItems];
        
        [self.progressHUD hide:YES afterDelay:0.75];
    }
}

#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (buttonIndex != alertView.cancelButtonIndex) {
        // Delete confirmation - delete the note
        if ([self.delegate respondsToSelector:@selector(noteAnnotationController:didDeleteAnnotation:)]) {
            [self.delegate noteAnnotationController:self didDeleteAnnotation:self.annotation];
        }
    }
}

@end
