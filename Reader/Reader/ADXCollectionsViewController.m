//
//  ADXCollectionsViewController.m
//  Reader
//
//  Created by Gavin McKenzie on 12-06-20.
//  Copyright (c) 2012 ActivDox Inc. All rights reserved.
//

#import "ADXCollectionsViewController.h"
#import "ADXCollectionViewController.h"
#import "ActivDoxCore.h"
#import "ADXAppDelegate.h"
#import "ADXCollectionsGridCell.h"
#import "ADXNotificationsViewController.h"
#import "ADXNotificationBubbleManager.h"
#import "ADXCollectionViewHorizontalLayout.h"
#import "ADXCollectionsController.h"
#import "ADXSettingsTableViewController.h"
#import "ADXPDFViewController.h"
#import "ADXPopoverBackgroundView.h"

#import "GMGridViewLayoutStrategies.h"
#import "PSPDFActionSheet.h"
#import "MBProgressHUD.h"


static NSUInteger const kADXGridCellWidth_iPad = 240;
static NSUInteger const kADXGridCellHeight_iPad = 270;
static NSUInteger const kADXGridCellWidth_iPhone = 160;
static NSUInteger const kADXGridCellHeight_iPhone = 220;

static NSString *kADXCollectionCellReuseIdentifier = @"ADXCollectionsGridCell";

@interface ADXCollectionsViewController () <ADXNotificationsActionsDelegate, UIPopoverControllerDelegate, UICollectionViewDataSource, UICollectionViewDelegate>
@property (strong, nonatomic) ADXCollectionsController *collectionsController;
@property (strong, nonatomic) UICollectionView *collectionView;
@property (strong, nonatomic) ADXAccount *currentAccount;
@property (strong, nonatomic) NSMutableArray *collectionSectionChanges;
@property (strong, nonatomic) NSMutableArray *collectionItemChanges;
@property (weak, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (strong, nonatomic) ADXCollection *selectedCollection;
@property (assign, nonatomic) BOOL isNotFirstPresentation;
@property (strong, nonatomic) UIPopoverController *currentPopoverController;
@property (assign, nonatomic) CGRect popoverPresentFromRect;
@property (strong, nonatomic) ADXCollection *inspectedCollection;
@property (weak, nonatomic) UIButton *notificationsButton;
@property (weak, nonatomic) UIBarButtonItem *addCollectionButtonItem;
@property (strong, nonatomic) NSString *previousCollectionIDToOpen;
@property (strong, nonatomic) NSString *previousCollectionLocalIDToOpen;
- (void)finishInitialization;
@end

@implementation ADXCollectionsViewController

#pragma mark - Initialization

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [self finishInitialization];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self finishInitialization];
    }
    return self;
}

- (void)finishInitialization
{
    ADXAppDelegate *appDelegate = (ADXAppDelegate *)[[UIApplication sharedApplication] delegate];
    
    _managedObjectContext = [appDelegate managedObjectContext];
    _currentAccount = [appDelegate currentAccount];
    _collectionsController = [[ADXCollectionsController alloc] initWithManagedObjectContext:_managedObjectContext];
    _collectionsController.delegate = self;
    _collectionsController.dataSource = self;
    _collectionsController.sortFilter = ADXCollectionsControllerSortTypeCreated;
}

#pragma mark - Notifications

- (void)watchNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleServiceUnauthorizedNotification:)
                                                 name:kADXServiceURLRequestUnauthorizedNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleServiceAllCollectionsRefreshCompletedNotification:)
                                                 name:kADXServiceAllCollectionsRefreshCompletedNotification
                                               object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleServiceAllDocumentsRefreshCompletedNotification:)
                                                 name:kADXServiceAllDocumentsRefreshCompletedNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleServiceDocumentDownloadedNotification:)
                                                 name:kADXServiceDocumentDownloadedNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleURLSchemeRequestNotification:)
                                                 name:kADXURLSchemeRequestNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleConflictingNoteChangesDiscardedNotification:)
                                                 name:kADXServiceConflictingNoteChangesDiscardedNotification object:nil];
}

- (void)unwatchNotifications
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kADXServiceURLRequestUnauthorizedNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kADXServiceAllCollectionsRefreshCompletedNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kADXServiceAllDocumentsRefreshCompletedNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kADXServiceDocumentDownloadedNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kADXURLSchemeRequestNotification object:nil];
}

- (void)handleServiceUnauthorizedNotification:(NSNotification *)notification
{
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        [[ADXAppDelegate sharedInstance] presentUserReauthenticationPopupIfNecessary];
    }];
}

- (void)handleServiceAllCollectionsRefreshCompletedNotification:(NSNotification *)notification
{
}

- (void)handleServiceAllDocumentsRefreshCompletedNotification:(NSNotification *)notification
{
}

- (void)handleServiceDocumentDownloadedNotification:(NSNotification *)notification
{
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        [self processPendingURLRequest];
    }];
}

- (void)handleURLSchemeRequestNotification:(NSNotification *)notification
{
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        [self processPendingURLRequest];
    }];
}

- (void)handleConflictingNoteChangesDiscardedNotification:(NSNotification *)notification
{
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        NSString *message = NSLocalizedString(@"Your changes to a note conflicted with earlier changes made by another user.  Your changes have been reverted.", nil);

        // TODO: This message isn't actually all that helpful.  We don't have the old text for the note, and in fact the note may be deleted, so
        // we don't have much information to provide.
        
        UIAlertView *conflictAlert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Conflict", nil)
                                                                message:message
                                                               delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                                      otherButtonTitles:nil, nil];
        [conflictAlert show];
    }];
}

#pragma mark - Helpers

- (NSIndexPath *)indexPathForCollection:(ADXCollection *)collection
{
    NSIndexPath *result = nil;
    NSInteger numberOfSections = self.collectionView.numberOfSections;
    
    for (NSInteger section = 0; section < numberOfSections; section++) {
        NSInteger numberOfItems = [self.collectionView numberOfItemsInSection:section];
        
        for (NSInteger item = 0; item < numberOfItems; item++) {
            NSIndexPath *indexPath = [NSIndexPath indexPathForItem:item inSection:section];
            ADXCollectionsGridCell *cell = (ADXCollectionsGridCell *)[self.collectionView cellForItemAtIndexPath:indexPath];

            if (cell.collectionID) {
                if ([cell.collectionID isEqualToString:collection.id]) {
                    result = indexPath;
                    break;
                }
            } else if ([cell.collectionLocalID isEqualToString:collection.localID]) {
                result = indexPath;
                break;
            }
        }
    }
    return result;
}

- (void)insertUserCollection
{
    __weak ADXCollectionsViewController* weakSelf = self;

    ADXAppDelegate *appDelegate = (ADXAppDelegate *)[[UIApplication sharedApplication] delegate];
    [appDelegate insertUserCollection:^(id resource, BOOL success, NSError *error) {
        if (success) {
            ADXCollection *collection = resource;
            [weakSelf presentPopoverForAddCollection:collection];
        } else {
            LogNSError(@"Couldn't create collection", error);
        }
    }];
}

#pragma mark - Accessors

- (ADXV2Service *)service
{
    ADXV2Service *result = [ADXV2Service serviceForAccount:self.currentAccount];
    return result;
}

#pragma mark - View Management

- (void)viewDidLoad
{
    [super viewDidLoad];
        
    [self setupNavbar];
    [self setupToolbar];

    self.collectionSectionChanges = nil;
    self.collectionItemChanges = nil;
    
    [self setupUICollectionView];
    
    // Remember the previous collection ID to open, and then reset it.
    self.previousCollectionIDToOpen = [ADXUserDefaults sharedDefaults].currentlyOpenCollectionID;
    self.previousCollectionLocalIDToOpen = [ADXUserDefaults sharedDefaults].currentlyOpenCollectionLocalID;
    
    [[ADXUserDefaults sharedDefaults] setCurrentlyOpenCollectionID:nil localID:nil];
}

- (void)viewDidUnload
{
    [self setNotificationsBarButton:nil];
    [self setProfileButtonItem:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [ADXAppDelegate sharedInstance].readerAppState = ADXAppState.viewingCollections;
    
    self.notificationsBarButton.enabled = YES;
    
    [self watchNotifications];

    // In case orientation changed while a subsequent view controller was active
    [self.collectionView.collectionViewLayout invalidateLayout];
    [self scrollToPageBoundary];

    [self presentAnyPreviouslyOpenCollection];
}

- (ADXCollection *)previouslyOpenCollection
{
    ADXCollection *result = [ADXCollection collectionWithID:self.previousCollectionIDToOpen orLocalID:self.previousCollectionLocalIDToOpen accountID:self.currentAccount.id context:self.managedObjectContext];
    return  result;
}

- (void)presentAnyPreviouslyOpenCollection
{
    if (self.previousCollectionIDToOpen || self.previousCollectionLocalIDToOpen) {
        ADXCollection *collectionToOpen = [self previouslyOpenCollection];
        self.previousCollectionIDToOpen = nil;
        self.previousCollectionLocalIDToOpen = nil;
        
        if (collectionToOpen != nil) {
            self.selectedCollection = collectionToOpen;
            [self performSegueWithIdentifier:@"kADXShowDocBrowser" sender:self];
        }
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self updatePageControl];

    [self processPendingURLRequest];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self dismissPopovers];
    [self unwatchNotifications];

    [ADXAppDelegate sharedInstance].readerAppState = ADXAppState.viewingCollectionsEnded;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return YES;
}

- (void)setupNavbar
{
    [self.navigationItem setTitle:@"Collections"];

    UIButton *addButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [addButton setShowsTouchWhenHighlighted:YES];
    [addButton setImage:[UIImage imageNamed:@"navBarAddButton"] forState:UIControlStateNormal];
    [addButton addTarget:self action:@selector(didTapAddCollectionButton:) forControlEvents:UIControlEventTouchUpInside];
    [addButton setFrame:CGRectMake(0.0f, 0.0f, 45.0f, 25.0f)];
    UIBarButtonItem *addButtonItem = [[UIBarButtonItem alloc] initWithCustomView:addButton];

    self.navigationItem.rightBarButtonItems = @[self.notificationsBarButton, addButtonItem];
    self.addCollectionButtonItem = addButtonItem;
}

- (void)setupToolbar
{
    [self.navigationController setToolbarHidden:YES];
}

- (void)refresh:(BOOL)forceLoadAll
{
    [self.collectionsController refresh:forceLoadAll];
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    [self scrollToPageBoundary];
}

#pragma mark - Popovers

- (IBAction)didTapProfileButton:(id)sender
{
    if (self.currentPopoverController != nil) {
        [self.currentPopoverController dismissPopoverAnimated:NO];
    }

    ADXSettingsTableViewController *settingsView = [self.storyboard instantiateViewControllerWithIdentifier:@"settings"];
    settingsView.signoutHandler = ^{
        [self.currentPopoverController dismissPopoverAnimated:NO];
        [[ADXAppDelegate sharedInstance] passCheckpoint:@"Tapped the Sign Out button"];
        [[ADXAppDelegate sharedInstance] performLogoutTasks];
        [self.parentViewController dismissViewControllerAnimated:YES completion:nil];
    };
    
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:settingsView];
    navigationController.title = NSLocalizedString(@"Settings", nil);
    
    UIPopoverController *popover = [[UIPopoverController alloc] initWithContentViewController:navigationController];
    popover.popoverBackgroundViewClass = [ADXBluePopoverBackgroundView class];
    [popover presentPopoverFromRect:[sender bounds] inView:sender permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    self.currentPopoverController = popover;
}

- (void)presentPopoverForAddCollection:(ADXCollection *)collection
{
    [self dismissPopovers];

    UIPopoverController *collectionInfoPopoverController = [self newPopoverForUserCollection:collection title:@"New Collection"];
    collectionInfoPopoverController.delegate = self;
    self.currentPopoverController = collectionInfoPopoverController;
    [collectionInfoPopoverController presentPopoverFromBarButtonItem:self.addCollectionButtonItem permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
}

- (void)presentPopoverForCollection:(ADXCollection *)collection atRect:(CGRect)rect title:(NSString *)title
{
    [self dismissPopovers];
    
    if ([collection.type isEqualToString:ADXCollectionType.user]) {
        UIPopoverController *collectionInfoPopoverController = [self newPopoverForUserCollection:collection title:title];
        collectionInfoPopoverController.delegate = self;
        self.currentPopoverController = collectionInfoPopoverController;
        self.popoverPresentFromRect = rect;
        [collectionInfoPopoverController presentPopoverFromRect:rect inView:self.collectionView permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    } else if ([collection.type isEqualToString:ADXCollectionType.system]) {
        UIPopoverController *documentListPopoverController = [self newPopoverForSystemCollection:collection title:title];
        documentListPopoverController.delegate = self;
        self.currentPopoverController = documentListPopoverController;
        self.popoverPresentFromRect = rect;
        [documentListPopoverController presentPopoverFromRect:rect inView:self.collectionView permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    }
}

- (UIPopoverController *)newPopoverForUserCollection:(ADXCollection *)collection title:(NSString *)title
{
    ADXCollectionInfoMode initialMode = ADXCollectionInfoModeDisplayMetadata;
    UIPopoverController *popover = [ADXCollectionInfoViewController popoverControllerWithCollection:collection initialMode:initialMode title:title actionDelegate:self];
    return popover;
}

- (UIPopoverController *)newPopoverForSystemCollection:(ADXCollection *)collection title:(NSString *)title
{
    UIPopoverController *popover = [ADXCollectionDocumentsViewController popoverControllerWithTitle:title collection:collection allowEditing:NO startInEditMode:NO allowMultiSelect:NO allowEditMultiSelect:NO showOnlyMissingDocuments:NO actionDelegate:self];
    return popover;
}

- (BOOL)popoverControllerShouldDismissPopover:(UIPopoverController *)popoverController
{
    return YES;
}

- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController
{
    self.inspectedCollection = nil;
    self.currentPopoverController = nil;
}

- (void)dismissPopovers
{
    if ([self.currentPopoverController isPopoverVisible]) {
        [self.currentPopoverController dismissPopoverAnimated:YES];
        self.popoverPresentFromRect = CGRectZero;
    }
    self.currentPopoverController = nil;
}

#pragma mark - URL scheme request handling

- (void)processPendingURLRequest
{
    ADXAppDelegate *appDelegate = [ADXAppDelegate sharedInstance];
    if (appDelegate.readerAppState != ADXAppState.viewingCollections) {
        return;
    }
    
    // Process any pending URL scheme requests
    NSManagedObjectContext *moc = [ADXAppDelegate sharedInstance].managedObjectContext;
    ADXURLSchemeRequest *pendingRequest = [ADXAppDelegate sharedInstance].pendingURLSchemeRequest;
    
    if (pendingRequest != nil) {
        // See if we have the required document
        if (pendingRequest.requiredDocumentID) {
            NSString *accountID = [ADXAppDelegate sharedInstance].currentAccount.id;
            ADXDocument *document = [ADXDocument latestDocumentWithID:pendingRequest.requiredDocumentID accountID:accountID context:moc];
            if (document == nil || !document.downloadedValue) {
                // Need to wait for the document to be downloaded
                return;
            }
            
            // Transition to a collection that contains the document
            if (document.collections.count > 0) {
                ADXCollection *collection = [document.collections anyObject];
                self.selectedCollection = collection;
                [self performSegueWithIdentifier:@"kADXShowDocBrowser" sender:self];
            } else {
                LogError(@"Document %@ is not in any collections.");
                return;
            }
        };
    }
}

#pragma mark - ADXCollectionsControllerDataSource

- (NSString *)accountIDForCollectionsController:(ADXCollectionsController *)controller
{
    ADXAppDelegate *appDelegate = [ADXAppDelegate sharedInstance];
    NSString *accountID = appDelegate.currentAccount.id;
    return accountID;
}

#pragma mark - UICollection View

- (UINib *)nibForCell
{
    UINib *result = nil;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        result = [UINib nibWithNibName:[NSString stringWithFormat:@"%@-iPad", kADXCollectionCellReuseIdentifier] bundle:nil];
    } else {
        result = [UINib nibWithNibName:[NSString stringWithFormat:@"%@-iPhone", kADXCollectionCellReuseIdentifier] bundle:nil];
    }
    return result;
}

- (void)setupUICollectionView
{
    ADXCollectionViewHorizontalLayout *layout = [[ADXCollectionViewHorizontalLayout alloc] init];
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        CGSize iPhoneItemSize = CGSizeMake(kADXGridCellWidth_iPhone, kADXGridCellHeight_iPhone);
        layout.itemSize = iPhoneItemSize;
    } else {
        CGSize iPadItemSize = CGSizeMake(kADXGridCellWidth_iPad, kADXGridCellHeight_iPad);
        layout.itemSize = iPadItemSize;
    }
    
    CGRect outerViewBounds = self.view.bounds;
    outerViewBounds.size.height -= 50.0f;
    self.collectionView = [[UICollectionView alloc] initWithFrame:outerViewBounds collectionViewLayout:layout];
    self.collectionView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    self.collectionView.pagingEnabled = YES;
    self.collectionView.backgroundColor = [UIColor clearColor];
    self.collectionView.alwaysBounceHorizontal = YES;
    self.collectionView.showsHorizontalScrollIndicator = NO;
    
    [self.collectionView registerNib:[self nibForCell] forCellWithReuseIdentifier:kADXCollectionCellReuseIdentifier];
    
    [self.view addSubview:self.collectionView];
    [self.view bringSubviewToFront:self.refreshButton];
    [self.view bringSubviewToFront:self.pageControl];

    self.collectionView.dataSource = self;
    self.collectionView.delegate = self;
    
    [self setupLongPressGesture];
}

- (void)setupLongPressGesture
{
    UILongPressGestureRecognizer* longPressGesture = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongPressGesture:)];
    NSArray* recognizers = [self.collectionView gestureRecognizers];
    
    // Make the default gesture recognizer wait until the custom one fails.
    for (UIGestureRecognizer* aRecognizer in recognizers) {
        if ([aRecognizer isKindOfClass:[UILongPressGestureRecognizer class]])
            [aRecognizer requireGestureRecognizerToFail:longPressGesture];
    }
    
    // Now add the gesture recognizer to the collection view.
    [self.collectionView addGestureRecognizer:longPressGesture];
}

- (void)scrollToPageBoundary
{
    int visiblePage = (self.collectionView.contentOffset.x + (self.collectionView.bounds.size.width/2)) / self.collectionView.bounds.size.width;
    CGPoint offset = self.collectionView.contentOffset;
    offset.x = visiblePage * self.collectionView.bounds.size.width;
    self.collectionView.contentOffset = offset;
    [self updatePageControl];
}

- (void)updatePageControl
{
    // Ask the layout how many pages are displayed
    ADXCollectionViewHorizontalLayout *layout = (ADXCollectionViewHorizontalLayout *)self.collectionView.collectionViewLayout;
    self.pageControl.numberOfPages = layout.numberOfPages;
    
    // Calculate the displayed page number
    int pageNum = (self.collectionView.contentOffset.x + (self.collectionView.bounds.size.width/2)) / self.collectionView.bounds.size.width;
    self.pageControl.currentPage = pageNum;
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    NSInteger result = [self.collectionsController numberOfItemsInSection:section];
    return result;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    NSInteger result = [self.collectionsController numberOfSections];
    return result;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    ADXCollectionsGridCell *cell = (ADXCollectionsGridCell *)[self.collectionView dequeueReusableCellWithReuseIdentifier:kADXCollectionCellReuseIdentifier forIndexPath:indexPath];

    ADXCollection *collection = [self.collectionsController objectAtIndexPath:indexPath];
    NSUInteger numberOfDocuments = collection.numberOfDocuments;
 
    cell.collectionID = collection.id;
    cell.collectionLocalID = collection.localID;
    cell.titleLabel.text = [self titleForCollection:collection];
    cell.imageView.accessibilityLabel = cell.titleLabel.text;
    
    if ([collection.type isEqualToString:ADXCollectionType.system]) {
        cell.coverBandImage.image = [UIImage imageNamed:@"notebook_dark_blue"];
    } else {
        cell.coverBandImage.image = [UIImage imageNamed:@"notebook_cardboard"];
    }
    
    if (numberOfDocuments == 0) {
        cell.firstMetaLabel.text = @"No documents";
    } else if (numberOfDocuments == 1) {
        cell.firstMetaLabel.text = [NSString stringWithFormat:@"%d Document", numberOfDocuments];
    } else {
        cell.firstMetaLabel.text = [NSString stringWithFormat:@"%d Documents", numberOfDocuments];
    }

    return cell;
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    [[ADXAppDelegate sharedInstance] passCheckpoint:@"Tapped on a document collection"];

    ADXCollection *collection = [self.collectionsController objectAtIndexPath:indexPath];
    self.selectedCollection = collection;
    [self performSegueWithIdentifier:@"kADXShowDocBrowser" sender:self];
}

- (BOOL)collectionView:(UICollectionView *)collectionView shouldShowMenuForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return NO;
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    [self updatePageControl];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [self updatePageControl];
}

#pragma mark - Helpers

- (NSString *)titleForCollection:(ADXCollection *)collection
{
    NSString *result = nil;
    
    if ([collection.type isEqualToString:ADXCollectionType.system]) {
        result = [[ADXAppDelegate sharedInstance] localizedStringForKey:collection.title];
    } else {
        result = collection.title;
    }
    
    return result;
}

#pragma mark - ADXCollectionControllerDelegate

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    self.collectionSectionChanges = [NSMutableArray new];
    self.collectionItemChanges = [NSMutableArray new];
}

- (void)controller:(NSFetchedResultsController *)controller
  didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex
     forChangeType:(NSFetchedResultsChangeType)type
{
    NSMutableDictionary *sectionChange = [NSMutableDictionary new];
    
    switch(type) {
        case NSFetchedResultsChangeInsert:
            sectionChange[@(type)] = @[@(sectionIndex)];
            break;
        case NSFetchedResultsChangeDelete:
            sectionChange[@(type)] = @[@(sectionIndex)];
            break;
    }
    
    [self.collectionSectionChanges addObject:sectionChange];
}

- (void)controller:(NSFetchedResultsController *)controller
   didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath
     forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath
{
    NSMutableDictionary *itemChange = [NSMutableDictionary new];
    
    switch(type) {
        case NSFetchedResultsChangeInsert:
            itemChange[@(type)] = newIndexPath;
            break;
        case NSFetchedResultsChangeDelete:
            itemChange[@(type)] = indexPath;
            break;
        case NSFetchedResultsChangeUpdate:
            itemChange[@(type)] = indexPath;
            break;
        case NSFetchedResultsChangeMove:
            itemChange[@(type)] = @[indexPath, newIndexPath];
            break;
    }
    [self.collectionItemChanges addObject:itemChange];
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    __weak ADXCollectionsViewController* weakSelf = self;
    if ([self.collectionSectionChanges count]) {
        [self.collectionView performBatchUpdates:^{
            for (NSDictionary *change in weakSelf.collectionSectionChanges) {
                [change enumerateKeysAndObjectsUsingBlock:^(NSNumber *key, id obj, BOOL *stop) {
                    NSFetchedResultsChangeType type = [key unsignedIntegerValue];
                    
                    switch (type) {
                        case NSFetchedResultsChangeInsert:
                            [weakSelf.collectionView insertSections:[NSIndexSet indexSetWithIndex:[obj unsignedIntegerValue]]];
                            break;
                        case NSFetchedResultsChangeDelete:
                            [weakSelf.collectionView deleteSections:[NSIndexSet indexSetWithIndex:[obj unsignedIntegerValue]]];
                            break;
                        case NSFetchedResultsChangeUpdate:
                            [weakSelf.collectionView reloadSections:[NSIndexSet indexSetWithIndex:[obj unsignedIntegerValue]]];
                            break;
                    }
                }];
            }
        } completion:nil];
    }
    
    if ([self.collectionItemChanges count] > 0 && [self.collectionSectionChanges count] == 0) {
        [self.collectionView performBatchUpdates:^{
            for (NSDictionary *change in weakSelf.collectionItemChanges) {
                [change enumerateKeysAndObjectsUsingBlock:^(NSNumber *key, id obj, BOOL *stop) {
                    NSFetchedResultsChangeType type = [key unsignedIntegerValue];
                    
                    switch (type) {
                        case NSFetchedResultsChangeInsert:
                            [weakSelf.collectionView insertItemsAtIndexPaths:@[obj]];
                            break;
                        case NSFetchedResultsChangeDelete:
                            [weakSelf.collectionView deleteItemsAtIndexPaths:@[obj]];
                            break;
                        case NSFetchedResultsChangeUpdate:
                            [weakSelf.collectionView reloadItemsAtIndexPaths:@[obj]];
                            break;
                        case NSFetchedResultsChangeMove:
                            [weakSelf.collectionView moveItemAtIndexPath:obj[0] toIndexPath:obj[1]];
                            break;
                    }
                }];
            }
        } completion:nil];
    }
    
    [self.collectionSectionChanges removeAllObjects], self.collectionSectionChanges = nil;
    [self.collectionItemChanges removeAllObjects], self.collectionItemChanges = nil;
    
    [self updatePageControl];
}

#pragma mark - Storyboard

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender
{
    if ([identifier isEqualToString:@"kADXShowSettingsPopover"]) {
        if (self.currentPopoverController != nil) {
            [self.currentPopoverController dismissPopoverAnimated:YES];
            self.currentPopoverController = nil;
            return NO;
        }
    }
    
    return YES;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"kADXShowDocBrowser"]) {
        ADXCollectionViewController *vc = (ADXCollectionViewController *)[segue destinationViewController];
        vc.collection = self.selectedCollection;
    }
}

#pragma mark - ADXCollectionsGridCellDelegate

- (void)didTapCoverButton:(ADXCollectionsGridCell *)cell
{
    [self didRequestContextPopoverOnCell:cell atIndexPath:[self.collectionView indexPathForCell:cell]];
}

#pragma mark - Actions

- (void)didRequestContextPopoverOnCell:(ADXCollectionsGridCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    CGRect cellFrame = cell.frame;
    ADXCollection *collection = [ADXCollection collectionWithID:cell.collectionID orLocalID:cell.collectionLocalID accountID:self.currentAccount.id context:self.managedObjectContext];
    if (collection) {
        self.inspectedCollection = collection;
        [self presentPopoverForCollection:collection atRect:cellFrame title:@"Collection"];
    }
}

- (void)handleLongPressGesture:(UILongPressGestureRecognizer *)longPressGesture
{
    if (longPressGesture.state != UIGestureRecognizerStateBegan)
        return;
    
    CGPoint location = [longPressGesture locationInView:self.collectionView];
    NSIndexPath *indexPath = [self.collectionView indexPathForItemAtPoint:location];
    ADXCollectionsGridCell *cell = (ADXCollectionsGridCell *)[self.collectionView cellForItemAtIndexPath:indexPath];
    
    if (cell) {
        [self didRequestContextPopoverOnCell:cell atIndexPath:indexPath];
    }
}

- (IBAction)didTapAddCollectionButton:(id)sender
{
    if (self.currentPopoverController) {
        [self dismissPopovers];
    } else {
        [self insertUserCollection];
    }
}

- (IBAction)didLongPressRefreshButton:(id)sender
{
    [[ADXAppDelegate sharedInstance] passCheckpoint:@"Long-pressed the refresh button"];

    [self refresh:YES];
}

- (IBAction)didTapNotificationsButton:(id)sender
{
    [[ADXAppDelegate sharedInstance] passCheckpoint:@"Tapped the notifications button"];

    if ([self.currentPopoverController isPopoverVisible]) {
        [self dismissPopovers];
    } else {
        self.currentPopoverController = [ADXNotificationsViewController popoverControllerWithAccount:self.currentAccount.id actionDelegate:self options:nil managedObjectContext:self.managedObjectContext];
        [self.currentPopoverController presentPopoverFromBarButtonItem:self.notificationsBarButton permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
    }
}

- (IBAction)didLongPressProfileButton:(id)sender
{
    [[ADXAppDelegate sharedInstance] presentEmailDatabaseComposer:self];
//    PSPDFActionSheet *actionSheet = [[PSPDFActionSheet alloc] initWithTitle:@"Debug"];
//    [actionSheet addButtonWithTitle:@"Email Database" block:^{
//    }];
//    [actionSheet showFromBarButtonItem:self.profileButtonItem animated:YES];
}

- (IBAction)didTapRefreshButton:(id)sender
{
    [[ADXAppDelegate sharedInstance] passCheckpoint:@"Tapped the refresh button"];

    [self dismissPopovers];
    [self refresh:NO];
}

#pragma mark - ADXNotificationsActionsDelegate

- (void)didSelectDocument:(ADXDocument *)document fromEvent:(ADXEvent *)event
{
    [self dismissPopovers];
    [self openActionForDocument:document onPage:0 fromRectOfView:nil inView:nil animated:YES];
}

#pragma mark - ADXCollectionDocumentsViewController & ADXCollectionActionsDelegate

- (void)didSelectDocument:(ADXDocument *)document
{
    [self dismissPopovers];
    [self openActionForDocument:document onPage:0 fromRectOfView:nil inView:nil animated:YES];
}

- (void)didTapDeleteCollection:(ADXCollection *)collection
{
    CGRect rectOfThumbnail = self.popoverPresentFromRect;
    [self dismissPopovers];
    
    PSPDFActionSheet *actionSheet = [[PSPDFActionSheet alloc] initWithTitle:nil];
    [actionSheet setDestructiveButtonWithTitle:@"Remove Collection" block:^{
        [self dismissPopovers];
        [self.collectionsController deleteCollection:collection.id localID:collection.localID];
    }];
    [actionSheet showFromRect:rectOfThumbnail inView:self.collectionView animated:YES];
}

- (void)didRemoveDocument:(ADXDocument *)document fromCollection:(ADXCollection *)collection
{
    [self.collectionsController removeDocument:document.id fromCollection:collection.id localID:collection.localID];
}

- (void)didAddDocument:(ADXDocument *)document toCollection:(ADXCollection *)collection
{
    [self.collectionsController addDocument:document.id toCollection:collection.id localID:collection.localID];
}

#pragma mark - Actions

- (void)openActionForDocument:(ADXDocument *)document onPage:(NSInteger)page fromRectOfView:(UIView *)rectOfView inView:(UIView *)view animated:(BOOL)animated
{
    __weak ADXCollectionsViewController *weakSelf = self;
    if (document.accessible) {
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [self.collectionsController.service refreshNotesForDocument:document.id completion:^(BOOL success, NSError *error) {
            if (!success) {
                LogNSError(@"refreshNotesForDocument failed", error);
            }
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                [[ADXAppDelegate sharedInstance] pdfResourceForDocument:document
                                                        downloadContent:YES
                                                                service:weakSelf.collectionsController.service
                                                      downloadWillStart:^{
                                                          // Do nothing
                                                      } completion:^(id resource, BOOL success, NSError *error) {
                                                          dispatch_async(dispatch_get_main_queue(), ^{
                                                              [MBProgressHUD hideHUDForView:weakSelf.view animated:YES];
                                                              if (success && resource) {
                                                                  [weakSelf openPDFResource:resource onPage:page fromDocument:document animated:animated];
                                                              }
                                                          });
                                                      }];
            }];
        }];
    } else {
        // FIXME
    }
}

- (void)openPDFResource:(PSPDFDocument *)pdfResource onPage:(NSInteger)page fromDocument:(ADXDocument *)document animated:(BOOL)animated
{
    // Update the document open date
    [[ADXAppDelegate sharedInstance] saveMainContext:nil];
    
    [self.collectionsController.service emitEventDidOpenDocument:document.id versionID:document.version completion:nil];
    
    [[ADXAppDelegate sharedInstance] passCheckpoint:@"Opened a document from the collection view"];
    
    // Setup the view controller
    ADXPDFViewController *pdfController = [[ADXPDFViewController alloc] initWithDocument:document pdfDocument:pdfResource];
    pdfController.hidesBottomBarWhenPushed = YES;
    
    // FIXME -- When the user exits from reading a document, and returns to the received documents view, we should reset the currently open document ID to nil
    
    // Update the application state
    ADXAppDelegate *appDelegate = (ADXAppDelegate *)[[UIApplication sharedApplication] delegate];
    [appDelegate setReaderAppState:ADXAppState.viewingDocBrowserEnded];
    
    // If the user tapped on a document while we're waiting for the document requested through a URL scheme to
    // download, that cancels the URL scheme launch.
    appDelegate.pendingURLSchemeRequest = nil;
    
    [self.navigationController pushViewController:pdfController animated:animated];
    
    page = page ?: 0;
    [pdfController setPage:page animated:NO];
}

@end
