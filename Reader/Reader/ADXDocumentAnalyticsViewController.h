//
//  ADXDocumentAnalyticsViewController.h
//  Reader
//
//  Created by Steve Tibbett on 2012-11-27.
//  Copyright (c) 2012 ActivDox Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ActivDoxCore.h"

@interface ADXDocumentAnalyticsViewController : UIViewController <UIWebViewDelegate>

+ (void)presentAnalyticsForDocument:(ADXDocument *)document parentViewController:(UIViewController *)parentViewController;

@end
