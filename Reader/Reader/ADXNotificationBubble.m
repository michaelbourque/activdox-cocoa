//
//  ADXNotificationBubble.m
//  Reader
//
//  Created by Gavin McKenzie on 2013-03-25.
//  Copyright (c) 2013 ActivDox Inc. All rights reserved.
//

#import "ADXNotificationBubble.h"

#import <QuartzCore/QuartzCore.h>

@interface ADXNotificationBubble ()
@property (weak, nonatomic) UILabel *messageLabel;
@property (strong, nonatomic) NSTimer *displayTimer;
@property (weak, nonatomic) UIButton *closeButton;
@property (weak, nonatomic) UIButton *stickyActionButton;
@end

@implementation ADXNotificationBubble

+ (ADXNotificationBubble *)newBubble
{
    ADXNotificationBubble *result = [[ADXNotificationBubble alloc] initWithFrame:CGRectMake(0, 0, 325, 96)];
    return result;
}

#pragma mark - Initialization

- (id)init
{
    self = [super init];
    if (self) {
//        [self watchNotifications];
        [self setupViews];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
//        [self watchNotifications];
        [self setupViews];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
//        [self watchNotifications];
        [self setupViews];
    }
    return self;
}

//- (void)dealloc
//{
//    [self unwatchNotifications];
//}
//
//#pragma mark - Notifications
//
//- (void)watchNotifications
//{
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(statusBarFrameOrOrientationChanged:) name:UIApplicationDidChangeStatusBarOrientationNotification object:nil];
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(statusBarFrameOrOrientationChanged:) name:UIApplicationDidChangeStatusBarFrameNotification object:nil];
//}
//
//- (void)unwatchNotifications
//{
//    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidChangeStatusBarOrientationNotification object:nil];
//    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidChangeStatusBarFrameNotification object:nil];
//}
//
//#pragma mark - Rotation
//
//- (void)statusBarFrameOrOrientationChanged:(NSNotification *)notification
//{
//    /*
//     This notification is most likely triggered inside an animation block,
//     therefore no animation is needed to perform this nice transition.
//     */
//    [self rotateAccordingToStatusBarOrientationAndSupportedOrientations];
//}
//
//- (void)rotateAccordingToStatusBarOrientationAndSupportedOrientations
//{
//    UIInterfaceOrientation statusBarOrientation = [UIApplication sharedApplication].statusBarOrientation;
//    CGFloat angle = UIInterfaceOrientationAngleOfOrientation(statusBarOrientation);
//    CGFloat statusBarHeight = [[self class] getStatusBarHeight];
//    
//    CGAffineTransform transform = CGAffineTransformMakeRotation(angle);
//    CGRect frame = [[self class] rectInWindowBounds:self.window.bounds statusBarOrientation:statusBarOrientation statusBarHeight:statusBarHeight];
//    
//    [self setIfNotEqualTransform:transform frame:frame];
//}
//
//- (void)setIfNotEqualTransform:(CGAffineTransform)transform frame:(CGRect)frame
//{
//    if (!CGAffineTransformEqualToTransform(self.transform, transform)) {
//        self.transform = transform;
//    }
//    if (!CGRectEqualToRect(self.frame, frame)) {
//        self.frame = frame;
//    }
//}
//
//+ (CGFloat)getStatusBarHeight
//{
//    UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
//    if (UIInterfaceOrientationIsLandscape(orientation))
//    {
//        return [UIApplication sharedApplication].statusBarFrame.size.width;
//    }
//    else
//    {
//        return [UIApplication sharedApplication].statusBarFrame.size.height;
//    }
//}
//
//+ (CGRect)rectInWindowBounds:(CGRect)windowBounds statusBarOrientation:(UIInterfaceOrientation)statusBarOrientation statusBarHeight:(CGFloat)statusBarHeight
//{
//    CGRect frame = windowBounds;
//    frame.origin.x += statusBarOrientation == UIInterfaceOrientationLandscapeLeft ? statusBarHeight : 0;
//    frame.origin.y += statusBarOrientation == UIInterfaceOrientationPortrait ? statusBarHeight : 0;
//    frame.size.width -= UIInterfaceOrientationIsLandscape(statusBarOrientation) ? statusBarHeight : 0;
//    frame.size.height -= UIInterfaceOrientationIsPortrait(statusBarOrientation) ? statusBarHeight : 0;
//    return frame;
//}
//
//CGFloat UIInterfaceOrientationAngleOfOrientation(UIInterfaceOrientation orientation)
//{
//    CGFloat angle;
//    
//    switch (orientation) {
//        case UIInterfaceOrientationPortraitUpsideDown:
//            angle = M_PI;
//            break;
//        case UIInterfaceOrientationLandscapeLeft:
//            angle = -M_PI_2;
//            break;
//        case UIInterfaceOrientationLandscapeRight:
//            angle = M_PI_2;
//            break;
//        default:
//            angle = 0.0;
//            break;
//    }
//    
//    return angle;
//}
//
//UIInterfaceOrientationMask UIInterfaceOrientationMaskFromOrientation(UIInterfaceOrientation orientation)
//{
//    return 1 << orientation;
//}
//
//- (void)didMoveToWindow
//{
//    [super didMoveToWindow];
//    if (self.window) {
//        [self rotateAccordingToStatusBarOrientationAndSupportedOrientations];
//    }
//}

#pragma mark - Accessors

- (void)setMessage:(NSString *)message
{
    _message = message;
    self.messageLabel.text = message;
}

- (void)setSticky:(BOOL)sticky
{
    _sticky = sticky;
    if (sticky && !self.closeButton) {
        [self setupStickyButtons];
    } else if (!sticky && self.closeButton) {
        [self removeStickyButtons];
    }
}

#pragma mark - View Management

- (void)setupViews
{
    self.backgroundColor = [UIColor clearColor];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(30.0, 10.0, self.frame.size.width-60.0, self.frame.size.height-22.0)];
    label.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    label.backgroundColor = [UIColor clearColor];
    label.textColor = [UIColor whiteColor];
    label.shadowColor = [UIColor blackColor];
    label.shadowOffset = CGSizeMake(0.0, 1.0);
    label.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:13.0f];
    label.lineBreakMode = NSLineBreakByWordWrapping;
    label.numberOfLines = 3;
    
    [self addSubview:label];
    _messageLabel = label;
    
    if (self.sticky) {
        [self setupStickyButtons];
    }
}

- (void)setupStickyButtons
{
    UIImage *upImage = [[UIImage imageNamed:@"button"] resizableImageWithCapInsets:UIEdgeInsetsMake(10.0f, 10.0f, 10.0f, 10.0f)];
    UIImage *downImage = [[UIImage imageNamed:@"button_pushed@2x"] resizableImageWithCapInsets:UIEdgeInsetsMake(10.0f, 10.0f, 10.0f, 10.0f)];

    UIButton *closeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    UIButton *actionButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    closeButton.frame = CGRectMake(self.frame.size.width - 84.0, 13.0f, 66.0f, 30.0f);
    [closeButton setBackgroundImage:upImage forState:UIControlStateNormal];
    [closeButton setBackgroundImage:downImage forState:UIControlStateHighlighted];
    [closeButton setTintColor:[UIColor whiteColor]];
    [closeButton.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Medium" size:13.0f]];
    [closeButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [closeButton setTitle:@"Cancel" forState:UIControlStateNormal];
    [closeButton addTarget:self action:@selector(didTapCloseButton:) forControlEvents:UIControlEventTouchUpInside];
    [closeButton addTarget:self action:@selector(didDepressButton:) forControlEvents:UIControlEventTouchDown];
    [closeButton addTarget:self action:@selector(didFailButtonTap:) forControlEvents:UIControlEventTouchUpOutside];
    
    actionButton.frame = CGRectMake(self.frame.size.width - 84.0, 50.0f, 66.0f, 30.0f);
    [actionButton setBackgroundImage:upImage forState:UIControlStateNormal];
    [actionButton setBackgroundImage:downImage forState:UIControlStateHighlighted];
    [actionButton setTintColor:[UIColor whiteColor]];
    [actionButton.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Medium" size:13.0f]];
    [actionButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [actionButton setTitle:self.stickyActionLabel forState:UIControlStateNormal];
    [actionButton addTarget:self action:@selector(didTapActionButton:) forControlEvents:UIControlEventTouchUpInside];
    [actionButton addTarget:self action:@selector(didDepressButton:) forControlEvents:UIControlEventTouchDown];
    [actionButton addTarget:self action:@selector(didFailButtonTap:) forControlEvents:UIControlEventTouchUpOutside];

    [self addSubview:closeButton];
    [self addSubview:actionButton];
    
    self.closeButton = closeButton;
    self.stickyActionButton = actionButton;
    
    CGRect messageFrame = self.messageLabel.frame;
    messageFrame.size.width = 173.0f;
    self.messageLabel.frame = messageFrame;
}

- (void)removeStickyButtons
{
    [self.closeButton removeFromSuperview];
    [self.stickyActionButton removeFromSuperview];
    
    self.closeButton = nil;
    self.stickyActionButton = nil;

    CGRect messageFrame = CGRectMake(30.0, 10.0, self.frame.size.width-60.0, self.frame.size.height-22.0);
    self.messageLabel.frame = messageFrame;
}

- (void)drawRect:(CGRect)rect
{
    //// General Declarations
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    //// Color Declarations
    UIColor* fillColor = [UIColor colorWithRed: 0 green: 0 blue: 0 alpha: 0.6];
    UIColor* strokeColor = [UIColor colorWithRed: 1 green: 1 blue: 1 alpha: 1];
    
    //// Shadow Declarations
    UIColor* shadow = fillColor;
    CGSize shadowOffset = CGSizeMake(3.1, 3.1);
    CGFloat shadowBlurRadius = 5;
    
    //// Frames
    CGRect frame = rect;
    
    //// Rounded Rectangle Drawing
    UIBezierPath* roundedRectanglePath = [UIBezierPath bezierPath];
    [roundedRectanglePath moveToPoint: CGPointMake(CGRectGetMinX(frame) + 6.5, CGRectGetMaxY(frame) - 17.5)];
    [roundedRectanglePath addCurveToPoint: CGPointMake(CGRectGetMinX(frame) + 16.5, CGRectGetMaxY(frame) - 7.5) controlPoint1: CGPointMake(CGRectGetMinX(frame) + 6.5, CGRectGetMaxY(frame) - 11.98) controlPoint2: CGPointMake(CGRectGetMinX(frame) + 10.98, CGRectGetMaxY(frame) - 7.5)];
    [roundedRectanglePath addLineToPoint: CGPointMake(CGRectGetMaxX(frame) - 17.5, CGRectGetMaxY(frame) - 7.5)];
    [roundedRectanglePath addCurveToPoint: CGPointMake(CGRectGetMaxX(frame) - 7.5, CGRectGetMaxY(frame) - 17.5) controlPoint1: CGPointMake(CGRectGetMaxX(frame) - 11.98, CGRectGetMaxY(frame) - 7.5) controlPoint2: CGPointMake(CGRectGetMaxX(frame) - 7.5, CGRectGetMaxY(frame) - 11.98)];
    [roundedRectanglePath addLineToPoint: CGPointMake(CGRectGetMaxX(frame) - 7.5, CGRectGetMinY(frame) + 14.5)];
    [roundedRectanglePath addCurveToPoint: CGPointMake(CGRectGetMaxX(frame) - 17.5, CGRectGetMinY(frame) + 4.5) controlPoint1: CGPointMake(CGRectGetMaxX(frame) - 7.5, CGRectGetMinY(frame) + 8.98) controlPoint2: CGPointMake(CGRectGetMaxX(frame) - 11.98, CGRectGetMinY(frame) + 4.5)];
    [roundedRectanglePath addLineToPoint: CGPointMake(CGRectGetMinX(frame) + 16.5, CGRectGetMinY(frame) + 4.5)];
    [roundedRectanglePath addCurveToPoint: CGPointMake(CGRectGetMinX(frame) + 6.5, CGRectGetMinY(frame) + 14.5) controlPoint1: CGPointMake(CGRectGetMinX(frame) + 10.98, CGRectGetMinY(frame) + 4.5) controlPoint2: CGPointMake(CGRectGetMinX(frame) + 6.5, CGRectGetMinY(frame) + 8.98)];
    [roundedRectanglePath addLineToPoint: CGPointMake(CGRectGetMinX(frame) + 6.5, CGRectGetMaxY(frame) - 17.5)];
    [roundedRectanglePath closePath];
    CGContextSaveGState(context);
    CGContextSetShadowWithColor(context, shadowOffset, shadowBlurRadius, shadow.CGColor);
    [fillColor setFill];
    [roundedRectanglePath fill];
    CGContextRestoreGState(context);
    
    [strokeColor setStroke];
    roundedRectanglePath.lineWidth = 1.5;
    [roundedRectanglePath stroke];
}

#pragma mark - Actions

- (void)didDepressButton:(id)sender
{
    [self stopTimer];
}

- (void)didFailButtonTap:(id)sender
{
    [self startTimer];
}

- (void)didTapCloseButton:(id)sender
{
    [self stopTimer];
    if ([self.delegate conformsToProtocol:@protocol(ADXNotificationBubbleDelegate)]) {
        [self.delegate stickyBubbleDidEndWithCancel:self];
    }
}

- (void)didTapActionButton:(id)sender
{
    [self stopTimer];
    if ([self.delegate conformsToProtocol:@protocol(ADXNotificationBubbleDelegate)]) {
        [self.delegate stickyBubbleDidEndWithAction:self];
    }
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self stopTimer];
    if ([self.delegate conformsToProtocol:@protocol(ADXNotificationBubbleDelegate)]) {
        if (self.sticky) {
            if ([self.delegate conformsToProtocol:@protocol(ADXNotificationBubbleDelegate)]) {
                [self.delegate stickyBubbleDidEndWithAction:self];
            }
        } else {
            [self.delegate bubbleDidEndWithTap:self];
        }
    }
}

#pragma mark - Display Timer

- (void)startTimer
{
    if ([self.displayTimer isValid]) {
        [self.displayTimer invalidate];
    }
    
    self.displayTimer = [NSTimer timerWithTimeInterval:6 target:self selector:@selector(timerFired:) userInfo:nil repeats:NO];
    [[NSRunLoop mainRunLoop] addTimer:self.displayTimer forMode:NSDefaultRunLoopMode];
}

- (void)stopTimer
{
    if ([self.displayTimer isValid]) {
        [self.displayTimer invalidate];
    }
    self.displayTimer = nil;
}

- (void)timerFired:(NSTimer*)theTimer
{
    [self stopTimer];
    if ([self.delegate conformsToProtocol:@protocol(ADXNotificationBubbleDelegate)]) {
        [self.delegate bubbleDidEndWithTimeout:self];
    }
}

@end
