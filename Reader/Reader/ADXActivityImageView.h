//
//  ADXActivityImageView.h
//  Reader
//
//  Created by Gavin McKenzie on 2012-07-19.
//  Copyright (c) 2012 ActivDox Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ADXAppCommonMacros.h"

@interface ADXActivityImageView : UIView
- (void)startAnimating;
- (void)stopAnimating;
- (BOOL)isAnimating;
@end
