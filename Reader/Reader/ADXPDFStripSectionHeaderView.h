//
//  ADXPDFStripSectionHeaderView.h
//  Reader
//
//  Created by Steve Tibbett on 2012-11-30.
//  Copyright (c) 2012 ActivDox Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ADXPDFStripSectionHeaderView : UICollectionReusableView

@property (weak, nonatomic) IBOutlet UILabel *sectionTitleLabel;

@end
