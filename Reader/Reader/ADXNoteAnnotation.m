//
//  ADXNoteAnnotation.m
//  Reader
//
//  Created by Matteo Ugolini on 2012-09-25.
//  Copyright (c) 2012 ActivDox Inc. All rights reserved.
//

#import "ADXNoteAnnotation.h"
#import "ADXHighlightAnnotation.h"

@interface ADXNoteAnnotation ()
@end

@implementation ADXNoteAnnotation

#pragma mark - Class Methods

+ (ADXNoteAnnotation *)noteAnnotation:(CGRect)boundingBox page:(NSUInteger)page note:(ADXDocumentNote *)note document:(ADXDocument *)document pdf:(PSPDFDocument *)pdf
{
    ADXNoteAnnotation *noteAnnotation = [[ADXNoteAnnotation alloc] init];

    // Override default colour from yellow
    noteAnnotation.color = ADXNoteAnnotationOpenColor;

    [ADXNoteAnnotation updateNoteAnnotation:noteAnnotation
                           withDocumentNote:note
                                boundingBox:boundingBox
                                   document:document
                                        pdf:pdf
                                       page:page];
    
    return noteAnnotation;
}

+ (void)updateNoteAnnotation:(ADXNoteAnnotation *)noteAnnotation
            withDocumentNote:(ADXDocumentNote *)documentNote
                 boundingBox:(CGRect)boundingBox
                    document:(ADXDocument *)document
                         pdf:(PSPDFDocument *)pdf
                        page:(NSUInteger)page
{
    noteAnnotation.document = pdf;
    noteAnnotation.boundingBox = CGRectMake(boundingBox.origin.x + (boundingBox.size.width / 2) - 16,
                                            boundingBox.origin.y + boundingBox.size.height - 8,
                                            32, 32);
    noteAnnotation.page = page;
    
    noteAnnotation.documentID = document.id;
    noteAnnotation.versionID = document.version;
    noteAnnotation.noteLocalID = documentNote.localID;
    noteAnnotation.noteID = documentNote.id;
    noteAnnotation.accountID = documentNote.accountID;
    noteAnnotation.noteVisibility = documentNote.visibility;
    noteAnnotation.contents = documentNote.text;
}

+ (ADXNoteAnnotation *)noteAnnotationOverHighlight:(ADXHighlightAnnotation *)highlight note:(ADXDocumentNote *)note document:(ADXDocument *)document pdf:(PSPDFDocument *)pdf
{
    ADXNoteAnnotation *noteAnnotation = [ADXNoteAnnotation noteAnnotation:highlight.boundingBox page:highlight.page note:note document:document pdf:pdf];
    noteAnnotation.highlightAnnotation = highlight;
    
    return noteAnnotation;
}

#pragma mark - NSCopying

- (id)copyWithZone:(NSZone *)zone
{
    ADXNoteAnnotation *annotation = (ADXNoteAnnotation *)[super copyWithZone:zone];
    
    annotation.highlightAnnotation = self.highlightAnnotation;
    annotation.noteLocalID = self.noteLocalID;
    annotation.noteID = self.noteID;
    annotation.documentID = self.documentID;
    annotation.versionID = self.versionID;
    annotation.accountID = self.accountID;
    annotation.noteVisibility = self.noteVisibility;
    
    return annotation;
}

#pragma mark - Accessors

- (void)setDeleted:(BOOL)deleted
{
    [super setDeleted:deleted];
}

@end
