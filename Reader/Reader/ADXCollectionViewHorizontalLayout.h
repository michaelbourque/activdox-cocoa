//
//  ADXCollectionViewHorizontalLayout.h
//  Reader
//
//  Created by Steve Tibbett on 2013-01-09.
//  Copyright (c) 2013 ActivDox Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

// UICollectionViewLayout that implements our horizontal paging with vertical flow per page.

@interface ADXCollectionViewHorizontalLayout : UICollectionViewLayout

@property (nonatomic) CGSize itemSize;
@property (nonatomic) float minimumOuterMargin;
@property (nonatomic) float minimumItemSpacing;
@property (readonly) int numberOfPages;
@property (readonly) int numberOfItemsPerPage;
@property (nonatomic) BOOL columnSort;

@end
