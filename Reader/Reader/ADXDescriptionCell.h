//
//  ADXDescriptionCell.h
//  Reader
//
//  Created by Matteo Ugolini on 12-08-29.
//  Copyright (c) 2012 ActivDox Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ADXAppCommonMacros.h"
#import "ADXAttributedLabel.h"

@interface ADXDescriptionCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UIView *background;
@property (nonatomic, weak) IBOutlet UILabel *textSampleLabel;
@property (nonatomic, weak) IBOutlet UILabel *pageInfoLabel;
@property (nonatomic, weak) IBOutlet UIImageView *shadow;

@end
