//
//  ADXRevisionsViewController.m
//  Reader
//
//  Created by Matteo Ugolini on 12-08-03.
//  Copyright (c) 2012 ActivDox Inc. All rights reserved.
//

#import "ADXRevisionsViewController.h"
#import "PSPDFViewController.h"
#import "ActivDoxCore.h"
#import "ADXPageThumbnailView.h"
#import "ADXPDFChangeAnnotationView.h"
#import "MBProgressHUD.h"
#import "ADXDescriptionCell.h"
#import "ADXChangeDetailViewController.h"
#import "DDPopoverBackgroundView.h"
#import "ADXDocVersionComparisonViewController.h"
#import "ADXAppDelegate.h"
#import "ADXPDFViewMenuPopoverController.h"
#import "ADXCollectionViewHorizontalLayout.h"
#import "ADXAnnotationUtil.h"

#define KThumbnailForPageLanscape 7
#define KThumbnailForPagePortrait 5
#define kThumbnailSizeWidth 135
#define kThumbnailSizeHeight 165

#define KComparisonButtonLabel @"Δ %d" // @"∂%d"

@interface Change : NSObject

@property (nonatomic, strong) NSString      *description;
@property (nonatomic, strong) NSString      *pageInfo;
@property (nonatomic, assign) NSUInteger    pageNumber;

@end

@interface ADXRevisionsViewController () <ADXDocVersionComparisonViewControllerDelegate, ADXPDFViewMenuPopoverDelegate>
@property (nonatomic, strong) PSPDFDocument *document;
@property (nonatomic, weak) ADXPDFViewController    *pdfController;
@property (nonatomic, assign) BOOL showChangeHighlighting;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *toggleThumbnailsButton;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *toggleChangeStripsButton;

@property (nonatomic, weak) IBOutlet UILabel *changesCountLabel;
@property (nonatomic, weak) IBOutlet UIPageControl *thumbPageControl;
@property (nonatomic, weak) IBOutlet UILabel *noResultFound;
@property (weak, nonatomic) IBOutlet UICollectionView *thumbnailCollectionView;

@property (weak, nonatomic) IBOutlet UIToolbar *temporaryToolBar;

@property (nonatomic, strong) NSMutableSet *changedPages;
@property (nonatomic, strong) NSMutableArray *changesCache;
@property (nonatomic, strong) UIPopoverController *detailPopover;
@property (nonatomic, strong) UIPopoverController *currentPopoverController;

@property (nonatomic, assign) CGRect selectedChangeRect;
@property (atomic, strong) ADXHighlightedContentHelper *currentHighlightedContent;
@property (nonatomic) int currentHighlightedContentVersion;
@property (nonatomic, strong) IBOutlet UIBarButtonItem *revisionVersionSelector;
 
@property (nonatomic, retain) IBOutletCollection(UIBarButtonItem) NSArray *buttonItems;

@property (weak, nonatomic) IBOutlet UIView *thumbnailView;
@property (strong, nonatomic) NSMutableArray *changeStrips;
@end

@implementation ADXRevisionsViewController
    
- (id)initWithDocument:(PSPDFDocument *)document pdfController:(ADXPDFViewController *)pdfController 
{
    NSString *ii = UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ? @"Phone" : @"Pad";
    
    self = [super initWithNibName:[NSString stringWithFormat:@"ADXDocumentRevisionPane-%@", ii] bundle:nil];
    if ( self )
    {
        _pdfController = pdfController;
        _document = document;
        [_pdfController addToolsDelegate:self];
        
        UINib *nib = [UINib nibWithNibName:@"ADXRevisionToolsBar-Pad" bundle:nil];
        [nib instantiateWithOwner:self options:nil];
        
        // Remove the toolbar items from the nib we just loaded and add them to the shared UIToolbar
        NSArray *items = [self.buttonItems copy];
        [self.temporaryToolBar setItems:nil];
        
        [super setToolbarItems:items];
        
        UITabBarItem *tabBarItem = [[UITabBarItem alloc] initWithTitle:@"revisions" image:[UIImage imageNamed:@"changes_button_icon.png"] tag:1];
        tabBarItem.accessibilityLabel = NSLocalizedString(@"Revisions", nil);
        [super setTabBarItem:tabBarItem];
        
        _changesCache = [NSMutableArray array];
        _changedPages = [NSMutableSet set];
        
        [self resetCurrentDocumentChangeHighlight];
        [self refresh];


        self.view.backgroundColor = [UIColor clearColor];
        
        self.showChangeHighlighting = [[ADXUserDefaults sharedDefaults] annotationFilterShowChanges];
        if (self.showChangeHighlighting) {
            [self addChangeAnnotationsAnimated:YES];
        }
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self configureThumbnailCollectionView];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleAnnotationFilterChangeNotification:)
                                                 name:kADXAnnotationFilterChangedNotification
                                               object:nil];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];

    [self refresh];

    [self updatePageControl];
}

- (void)handleAnnotationFilterChangeNotification:(NSNotification *)notification
{
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        BOOL showChanges = [[ADXUserDefaults sharedDefaults] annotationFilterShowChanges];
        if (showChanges != self.showChangeHighlighting) {
            self.showChangeHighlighting = showChanges;
            if (self.showChangeHighlighting) {
                [self addChangeAnnotationsAnimated:YES];
            } else {
                [self removeChangeAnnotationsAnimated:YES completion:NULL];
            }
        }
    }];
}

- (void)updatePageControl
{
    // Ask the layout how many pages are displayed
    ADXCollectionViewHorizontalLayout *layout = (ADXCollectionViewHorizontalLayout *)self.thumbnailCollectionView.collectionViewLayout;
    self.thumbPageControl.numberOfPages = layout.numberOfPages;
    
    // Calculate the displayed page number
    int pageNum = (self.thumbnailCollectionView.contentOffset.x + (self.thumbnailCollectionView.bounds.size.width/2)) / self.thumbnailCollectionView.bounds.size.width;
    self.thumbPageControl.currentPage = pageNum;
}

- (void)setDocument:(PSPDFDocument *)document
{
    _document = document;

    // If the document we just switched to doesn't have changes downloaded, download them.
    if (!self.pdfController.adxDocument.changeContentDownloaded.boolValue && self.pdfController.adxDocument.changeCountValue) {
        __weak ADXRevisionsViewController *weakSelf = self;
        __weak ADXPDFViewController *weakPdfController = self.pdfController;
        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            [weakPdfController.service downloadChangesForDocument:weakPdfController.adxDocument.id versionID:weakPdfController.adxDocument.version completion:^(BOOL success, NSError *error) {
                if (success) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [weakSelf resetCurrentDocumentChangeHighlight];
                        [weakSelf refresh];
                    });
                }
            }];
        });
    }
    
    // Refresh now anyway even if we're waiting on an async download, so the display isn't stale in the meantime
    [self resetCurrentDocumentChangeHighlight];
    [self refresh];
}

- (void) resetCurrentDocumentChangeHighlight
{
    ADXDocument *adxDocument = self.pdfController.adxDocument;
    
    self.currentHighlightedContent = [ADXHighlightedContentHelper highlightedContentWithData:adxDocument.highlightedContent.data];
    NSInteger comparedVersion = adxDocument.version.intValue-1;

    self.currentHighlightedContentVersion = comparedVersion;
    
    if(comparedVersion) {
        self.revisionVersionSelector.title = [NSString stringWithFormat:KComparisonButtonLabel,comparedVersion];
        [self.revisionVersionSelector setEnabled:YES];
    } else {
        self.revisionVersionSelector.title = @"Δ";
        [self.revisionVersionSelector setEnabled:NO];
    }
    
    [self refreshChanges];
}

- (NSArray*) refreshPageIndex
{
    [self.changesCache removeAllObjects];
    [self.changedPages removeAllObjects];
    
    __block id oldChange = nil;
    ADXHighlightedContentHelper *hc = self.currentHighlightedContent;
    [hc enumerateChangesWithBlock:^(NSUInteger pageIndex, NSArray *pageRects, NSString *type, id change) {
    
        NSArray *revisedPages = [hc revisedPagesForChange:change];
        
        if( revisedPages )
        {
            for (NSNumber *n in revisedPages)
            {
                [self.changedPages addObject:n];
            }
        }
        
        if( change != oldChange )
        {
            Change *c = [Change new];
            
            NSString *changeType = [hc changeTypeDescriptionForChange:change];
            NSString *changedText = [hc changedTextForChange:change];
            if (changedText) {
                c.description = [NSString stringWithFormat:@"%@:\n%@", changeType, changedText];
            } else {
                c.description = changeType;
            }
            
            if( revisedPages && revisedPages.count)
            {
                c.pageNumber = [revisedPages[0] integerValue] - 1;
                if( revisedPages.count == 1 )
                {
                    c.pageInfo = [NSString stringWithFormat:NSLocalizedString(@"Change on page %@",nil), revisedPages[0]];
                }
                else
                {
                    //FIXME maibe not right check it.
                    c.pageInfo = [NSString stringWithFormat:NSLocalizedString(@"Change between pages %@-%@",nil),
                                  revisedPages[0], revisedPages[revisedPages.count-1]];
                }
            }
            
            [self.changesCache addObject:c];
            oldChange = change;
        }
    }];
    
    return self.changesCache;
}

- (BOOL)documentHasChanges
{
    NSUInteger foundChanges = self.changesCache.count;
    BOOL result = (foundChanges > 0) ? YES : NO;
    return result;
}

- (void)refresh
{
    [self refreshPageIndex];
    
    NSUInteger foundChanges = self.changesCache.count;
    
    [self.toggleThumbnailsButton setEnabled:YES];

    if ([self documentHasChanges]) {
        self.noResultFound.hidden = YES;
        NSString* format = NSLocalizedString( foundChanges > 1 ? @"%d changes" : @"1 change" , nil);
        self.changesCountLabel.text = [NSString stringWithFormat:format, foundChanges];
        [self.toggleChangeStripsButton setEnabled:YES];
        [self.toggleThumbnailsButton setEnabled:YES];
    } else {
        self.noResultFound.hidden = NO;
        self.changesCountLabel.text = NSLocalizedString( @"No changes found", nil);
        [self.toggleChangeStripsButton setEnabled:NO];
        [self.toggleThumbnailsButton setEnabled:NO];
    }

    [self.thumbnailCollectionView reloadData];
    BOOL hasPreviousVersions = self.pdfController.adxDocument.version > 0;
    [self.revisionVersionSelector setEnabled:hasPreviousVersions];
}

#pragma mark - Collection View

- (void)configureThumbnailCollectionView
{
    ADXCollectionViewHorizontalLayout *layout = (ADXCollectionViewHorizontalLayout *)self.thumbnailCollectionView.collectionViewLayout;
    layout.itemSize = CGSizeMake(135, 166);

    self.thumbnailCollectionView.opaque = NO;
    self.thumbnailCollectionView.backgroundColor = [UIColor clearColor];
    
    UINib *nib = [UINib nibWithNibName:@"ADXPageThumbnailView" bundle:nil];
    [self.thumbnailCollectionView registerNib:nib
               forCellWithReuseIdentifier:@"thumbnail"];
}

#pragma mark - UICollectionViewDelegate

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    [self updatePageControl];
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSArray *sortedKeys = [self.changedPages.allObjects sortedArrayUsingSelector:@selector(compare:)];
    NSNumber *pageNumber = [sortedKeys objectAtIndex:indexPath.item];
    [self.pdfController setPage:pageNumber.intValue - 1 animated:YES];
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.changedPages.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    ADXPageThumbnailView *cell = [self.thumbnailCollectionView dequeueReusableCellWithReuseIdentifier:@"thumbnail" forIndexPath:indexPath];
    NSArray *sortedKeys = [self.changedPages.allObjects sortedArrayUsingSelector:@selector(compare:)];
    NSNumber *pageNumber = [sortedKeys objectAtIndex:indexPath.item];
    cell.pageIndex = pageNumber.intValue - 1;
    cell.pdfDocument = self.pdfController.document;
    
    NSUInteger foundChanges = [self.currentHighlightedContent numberOfChangesOnPage:pageNumber.intValue - 1];
    NSString *manyChangesFormat = NSLocalizedString(@"%d changes", nil);
    NSString *oneChangeFormat = NSLocalizedString(@"1 change", nil);
    cell.metaInfo = [NSString stringWithFormat:foundChanges == 1 ? oneChangeFormat : manyChangesFormat, foundChanges];
    return cell;
}

#pragma mark - UITableView datasource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.changesCache.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellID = @"PSPDFDescriptionTableViewCell";
    ADXDescriptionCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    
	if (cell == nil) {
        UINib *nib = [UINib nibWithNibName:@"ADXDescriptionCell" bundle:[NSBundle mainBundle]];
        cell = [[nib instantiateWithOwner:self options:nil] objectAtIndex:0];
    }
    
    Change *c = [self.changesCache objectAtIndex:indexPath.row];
    cell.textSampleLabel.text = c.description;
    cell.pageInfoLabel.text = c.pageInfo;
   
    return cell;
}

- (IBAction)didTapShowChangeStripsButton:(id)sender
{
    if (!self.pdfController.isPDFStripsVisible) {
        [[ADXAppDelegate sharedInstance] passCheckpoint:@"Showed changes as PDF strips"];

        // About to reveal the change strips.  Prepare them.
        [self refreshChangeStrips];
    }
    
    if ([self changeThumbnailsVisible]) {
        [self.pdfController dismissViewControllerAnimated:YES completion:^{
            [self.pdfController togglePDFStripsWithDataSource:self];
        }];
    } else {
        [self.pdfController togglePDFStripsWithDataSource:self];
    }
}

- (void)refreshChangeStrips
{
    __block NSMutableArray *mutableChanges = [NSMutableArray array];
    __block NSUInteger lastPageIndex = -1;
    __block NSMutableDictionary *currentPageInfo = nil;
    
    [self.currentHighlightedContent enumerateChangesWithBlock:^(NSUInteger pageIndex, NSArray *rectsOnPage, NSString *type, id change) {
        if (lastPageIndex != pageIndex) {
            currentPageInfo = [NSMutableDictionary dictionary];
            currentPageInfo[@"pageIndex"] = @(pageIndex);
            currentPageInfo[@"strips"] = [NSMutableArray array];
            [mutableChanges addObject:currentPageInfo];
            lastPageIndex = pageIndex;
        }
        
        ADXPDFStripInfo *changeInfo = [[ADXPDFStripInfo alloc] init];
        changeInfo.change = change;
        changeInfo.pageIndex = pageIndex;
        
        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle defaultParagraphStyle] mutableCopy];
        paragraphStyle.alignment = NSTextAlignmentRight;
        
        NSDictionary *attributes = @{ NSFontAttributeName: [UIFont systemFontOfSize:13],
                                      NSForegroundColorAttributeName: [UIColor grayColor],
                                      };
        
        if ([type isEqualToString:kADXChangeTypeContentUpdated]) {
            NSString *originalText = [self.currentHighlightedContent descriptionForChange:change];
            NSString *summaryText = [NSString stringWithFormat:@"Original text: %@", originalText];

            NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:summaryText attributes:attributes];
            changeInfo.summary = string;
        }

        if ([type isEqualToString:kADXChangeTypeContentDeleted]) {
            NSString *originalText = [self.currentHighlightedContent descriptionForChange:change];
            NSString *summaryText = [NSString stringWithFormat:@"Deleted: %@", originalText];
            
            NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:summaryText attributes:attributes];
            changeInfo.summary = string;
        }

        CGRect extent = CGRectNull;
        BOOL extentSet = NO;
        
        for (NSValue *rectValue in rectsOnPage) {
            CGRect rect = [rectValue CGRectValue];
            if (!extentSet) {
                extentSet = YES;
                extent = rect;
            } else {
                extent = CGRectUnion(extent, rect);
            }
        }
        
        changeInfo.stripExtent = extent;
        [currentPageInfo[@"strips"] addObject:changeInfo];
    }];
    
    self.changeStrips = mutableChanges;
    [self.pdfController refreshPDFStrips];
}

- (IBAction)didTapShowChangeThumbnailsButton:(id)sender
{
    if (self.pdfController.viewMode == PSPDFViewModeThumbnails) {
        [self.pdfController hideThumbnails:nil];
    }
    
    if ([self changeThumbnailsVisible]) {
        [[ADXAppDelegate sharedInstance] passCheckpoint:@"Toggled the revisions thumbnails drawer closed"];
    } else {
        [[ADXAppDelegate sharedInstance] passCheckpoint:@"Toggled the revisions thumbnails drawer open"];
    }
    
    [self openClose:nil];
}

- (IBAction)didTapToolbarMenu:(id)sender
{
    if ([self.currentPopoverController isPopoverVisible]) {
        [self dismissPopovers];
    } else {
        NSMutableArray *options = [[NSMutableArray alloc] init];
        NSString *pageOrThumbnailsOptionLabel = (self.pdfController.viewMode == PSPDFViewModeThumbnails && ![self.pdfController isPDFStripsVisible]) ? @"Hide thumbnails" : @"Show thumbnails";
//        [options addObject:@{kADXPDFViewMenuPopoverOptionLabel : pageOrThumbnailsOptionLabel, kADXPDFViewMenuPopoverOptionIcon : @"269-NineColumn.png", kADXPDFViewMenuPopoverOptionID : @"thumbnails"}];
        [options addObject:@{kADXPDFViewMenuPopoverOptionLabel : pageOrThumbnailsOptionLabel, kADXPDFViewMenuPopoverOptionID : @"thumbnails"}];
        
        if ([self documentHasChanges]) {
            NSString *changeThumbnailsOptionLabel = [self changeThumbnailsVisible] ? @"Hide changed pages" : @"Show changed pages";
            NSString *changeStripsOptionLabel = [self.pdfController isPDFStripsVisible] ? @"Hide change summaries" : @"Show change summaries";
            [options addObjectsFromArray:@[
             @{kADXPDFViewMenuPopoverOptionLabel : changeThumbnailsOptionLabel, kADXPDFViewMenuPopoverOptionID : @"changeThumbnails"},
             @{kADXPDFViewMenuPopoverOptionLabel : changeStripsOptionLabel, kADXPDFViewMenuPopoverOptionID : @"changeSummaries"}]];
        }
        UIPopoverController *popover = [ADXPDFViewMenuPopoverController popoverControllerWithOptions:options actionDelegate:self];
        self.currentPopoverController = popover;
        [self.currentPopoverController presentPopoverFromBarButtonItem:sender permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    }    
}

- (void)didSelectPDFViewMenuOption:(NSDictionary *)option
{
    __weak ADXRevisionsViewController *weakSelf = self;
    
    if ([(NSString *)option[kADXPDFViewMenuPopoverOptionID] isEqualToString:@"thumbnails"]) {
        if ([self changeThumbnailsVisible]) {
            [self.pdfController dismissViewControllerAnimated:YES completion:^{
                if (weakSelf.pdfController.viewMode == PSPDFViewModeThumbnails && ![weakSelf.pdfController isPDFStripsVisible]) {
                    [weakSelf.pdfController hideThumbnails:nil];
                } else {
                    [weakSelf.pdfController showThumbnails:nil];
                }
            }];
        } else {
            if (self.pdfController.viewMode == PSPDFViewModeThumbnails && ![self.pdfController isPDFStripsVisible]) {
                [self.pdfController hideThumbnails:nil];
            } else {
                [self.pdfController showThumbnails:nil];
            }
        }
    } else if ([(NSString *)option[kADXPDFViewMenuPopoverOptionID] isEqualToString:@"changeThumbnails"]) {
    } else if ([(NSString *)option[kADXPDFViewMenuPopoverOptionID] isEqualToString:@"changeSummaries"]) {
    }
}

- (BOOL)changeThumbnailsVisible
{
    BOOL result = self.pdfController.presentedViewController == self;
    return result;
}

- (IBAction)openClose:(id)sender
{
    __weak ADXRevisionsViewController *weakSelf = self;
    if ([self changeThumbnailsVisible]){
        [self.pdfController dismissViewControllerAnimated:YES completion:^{
            [weakSelf refresh];
        }];
    } else {
        [self.pdfController presentViewController:self animated:YES completion:NULL];
    }
}

#pragma mark - Change Highlighting

- (void) refreshChanges
{
    __weak ADXRevisionsViewController *weakSelf = self;
    [self removeChangeAnnotationsAnimated:NO
                               completion:^{
                                   if ([[ADXUserDefaults sharedDefaults] annotationFilterShowChanges]) {
                                       [weakSelf addChangeAnnotationsAnimated:NO];
                                   }
                               }];
}


- (void)removeChangeAnnotationsAnimated:(BOOL)animated completion:(void (^)(void))completion
{
    // Walk the visible pages
    NSMutableArray *toRemove = [NSMutableArray array];
    NSArray *visiblePageNumbers = [self.pdfController visiblePageNumbers];
    for (NSNumber *pageNumber in visiblePageNumbers) {
        // Look for the change view and remove it
        PSPDFPageView *pageView = [self.pdfController pageViewForPage:[pageNumber integerValue]];
        for (UIView *subview in pageView.subviews) {
            if ([subview isKindOfClass:[ADXPDFChangeAnnotationView class]]) {
                [toRemove addObject:subview];
                break;
            }
        }
    }
    
    [UIView animateWithDuration:animated ? 0.25 : 0 animations:^{
        for (UIView* subview in toRemove) {
            subview.alpha = 0.0;
        }
    } completion:^(BOOL finished) {
        for (UIView* subview in toRemove) {
            [subview removeFromSuperview];
        }
        if(completion){
            completion();
        }
    }];
    
    self.showChangeHighlighting = NO;
}

- (void)setShowChangeHighlighting:(BOOL)showChangeHighlighting
{
    _showChangeHighlighting = showChangeHighlighting;
}

- (void)showHighlightedDocumentWithMessages:(BOOL)showMessages
{
    if (!self.pdfController.service)
        return;
    
    if (self.pdfController.adxDocument.changeContentDownloaded) {
        [self addChangeAnnotationsAnimated:YES];
    } else if (self.pdfController.adxDocument.changeCountValue) {
        __weak ADXPDFViewController *weakPdfController = self.pdfController;
        __weak MBProgressHUD *HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        __weak ADXRevisionsViewController *weakSelf = self;
        
        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            [weakPdfController.service downloadChangesForDocument:weakPdfController.adxDocument.id versionID:weakPdfController.adxDocument.version completion:^(BOOL success, NSError *error) {
                [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                    HUD.mode = MBProgressHUDModeText;
                    NSString *message;
                    if (success) {
                        message = @"Showing changes";
                    } else {
                        LogNSError(@"couldn't load changes for document: ", error);
                        message = @"No changes";
                    }
                    
                    if (showMessages) {
                        // Show the message briefly
                        HUD.labelText = message;
                        [HUD hide:YES afterDelay:0.5];
                    } else {
                        // Just hide the HUD
                        [HUD hide:YES];
                    }
                    [weakSelf addChangeAnnotationsAnimated:YES];
                }];
            }];
        });
    }
}

- (void)showNonHighlightedDocument
{
    [self removeChangeAnnotationsAnimated:YES completion:NULL];
}

- (void)addChangeAnnotationsToPageView:(PSPDFPageView *)pageView animated:(BOOL)animated
{
    // Make sure it's not already there
    for (UIView *subview in pageView.subviews) {
        if ([subview isKindOfClass:[ADXPDFChangeAnnotationView class]]) {
            return;
        }
    }

    ADXPDFChangeAnnotationView *changesView = [[ADXPDFChangeAnnotationView alloc] initWithChanges:self.currentHighlightedContent
                                                                                        pageIndex:pageView.page
                                                                                       pageExtent:pageView.pageInfo.rotatedPageRect];
    if( animated ){
        changesView.alpha = 0.0;
        [pageView insertSubview:changesView aboveSubview:pageView.contentView];
        
        [UIView animateWithDuration:0.25 animations:^{
            changesView.alpha = 1.0;
        }];
    }
    else {
        [pageView insertSubview:changesView aboveSubview:pageView.contentView];
    }
    
    self.showChangeHighlighting = YES;
}

- (void)addChangeAnnotationsAnimated:(BOOL)animated
{
    self.showChangeHighlighting = YES;
        
    NSArray *visiblePageNumbers = [self.pdfController visiblePageNumbers];
    for (NSNumber *pageNumber in visiblePageNumbers) {
        // Look for the change view and remove it
        PSPDFPageView *pageView = [self.pdfController pageViewForPage:[pageNumber integerValue]];
        [self addChangeAnnotationsToPageView:pageView animated:animated];
    }
}

#pragma mark - PSPDFViewControllerDelegate

- (void)pdfController:(ADXPDFViewController *)controller selectedToolDidChanged:(UIViewController*)tool
{
    if (tool != self)
    {
        if( self.pdfController.presentedViewController == self )
        {
            [self.pdfController dismissViewControllerAnimated:YES completion:NULL];
        }
    }
    else
    {
        [self refresh];
    }
}

- (void) documentDidChanged:(PSPDFDocument *)document
{
    self.document = document;
    if (self.showChangeHighlighting) {
        [self showHighlightedDocumentWithMessages:NO];
    }
    
    [self refresh];
}

- (void)pdfController:(ADXPDFViewController *)pdfController didLoadPageView:(PSPDFPageView *)pageView
{
    if (self.showChangeHighlighting) {
        [self addChangeAnnotationsAnimated:YES];
    }
    
    if( self.detailPopover && self.detailPopover.isPopoverVisible){
        [self.detailPopover dismissPopoverAnimated:YES];
    }
}

- (BOOL)pdfController:(ADXPDFViewController *)pdfController didTapOnPageView:(PSPDFPageView *)pageView atPoint:(CGPoint)viewPoint
{
    NSArray *visiblePageNumbers = [self.pdfController visiblePageNumbers];
    for (NSNumber *pageNumber in visiblePageNumbers)
    {
        PSPDFPageView *pageView = [self.pdfController pageViewForPage:[pageNumber integerValue]];
        for (UIView *changeView in pageView.subviews)
        {
            if ([changeView isKindOfClass:[ADXPDFChangeAnnotationView class]])
            {
                ADXPDFFoundChange foundChange = [((ADXPDFChangeAnnotationView*)changeView) hitTestForChangeAtPoint:viewPoint inPage:pageView.page];
                if ( foundChange.change )
                {
                    NSString *description = nil;
                    ADXHighlightedContentHelper *highlightedContent = self.currentHighlightedContent;
                    NSString *imagePath = [highlightedContent originalImagePathForChange:foundChange.change];
                    NSString *changeType = [highlightedContent changeTypeDescriptionForChange:foundChange.change];
                    
                    if ([[ADXHighlightedContentHelper changeTypeForChange:foundChange.change] isEqualToString: @"content.inserted"]) {
                        // Inserted content
                        description = NSLocalizedString(@"Inserted content", nil);
                    } else {
                        description = [highlightedContent changedTextForChange:foundChange.change];
                    }
                    
                    ADXChangeDetailViewController* ctrl;
                   
                    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
                    {
                        ctrl = [ADXChangeDetailViewController new];
                        self.pdfController.modalPresentationStyle = UIModalPresentationFormSheet;
                        [self.pdfController presentViewController:ctrl animated:YES completion:NULL];
                    }
                    else
                    {
                        if( !self.detailPopover )
                        {
                            ctrl = [ADXChangeDetailViewController new];
                            self.detailPopover = [[UIPopoverController alloc] initWithContentViewController:ctrl];
                            [DDPopoverBackgroundView setTintColor:[UIColor darkGrayColor]];
                            self.detailPopover.popoverBackgroundViewClass = [DDPopoverBackgroundView class];
                        }
                        else
                        {
                            ctrl = (ADXChangeDetailViewController*)self.detailPopover.contentViewController;
                        }
                        
                        NSString* longer = description.length > changeType.length ? description : changeType;
                        CGSize size = [longer sizeWithFont:[UIFont systemFontOfSize:[UIFont systemFontSize]]
                                         constrainedToSize:CGSizeMake(600, 200)
                                             lineBreakMode:NSLineBreakByWordWrapping];
                        
                        self.selectedChangeRect = foundChange.rect;
                        size = CGSizeMake(MAX(size.width, 300), size.height+50);
                        [self presentPopoverWithSize:size inRect:foundChange.rect inPageView:pageView];
                    }

                    ctrl.titleView.text = changeType;

                    if (imagePath.length > 0) {
                        [ctrl showChangeImage:nil];
                        
                        ADXV2Service *service = [ADXV2Service serviceForAccount:[ADXAppDelegate sharedInstance].currentAccount];
                        [service downloadImageAtPath:imagePath completion:^(id response, BOOL success, NSError *error) {
                            UIImage *image = [UIImage imageWithData:response];
                            [ctrl showChangeImage:image];
                            
                            float newHeight = image.size.height / image.size.width * 300.0;
                            
                            self.detailPopover.popoverContentSize = CGSizeMake(300, newHeight);
                        }];
                    } else {
                        [ctrl showDescriptionText:description];
                    }
                    return YES;
                }
            }
        }
    }

    if( self.detailPopover && self.detailPopover.isPopoverVisible )
    {
        [self.detailPopover dismissPopoverAnimated:YES];
        return YES;
    }
    
    return NO;
}

- (void)pdfController:(ADXPDFViewController *)pdfController didEndPageZooming:(UIScrollView *)scrollView atScale:(CGFloat)scale;
{
    [self refreshPopover];
}

- (void)pdfController:(ADXPDFViewController *)pdfController didEndPageDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset
{
    [self refreshPopover];
}

- (void) presentPopoverWithSize:(CGSize)size inRect:(CGRect)rect inPageView:(PSPDFPageView*)pageView
{
    UIScrollView *scrollView = pageView.scrollView;
    float zoom = scrollView.zoomScale;
    
    CGRect pageViewPort = scrollView.bounds;
    pageViewPort.origin = CGPointZero;
    
    CGRect zommedChangeRect = CGRectMake((rect.origin.x * zoom) - scrollView.contentOffset.x,
                                         (rect.origin.y * zoom) - scrollView.contentOffset.y,
                                         rect.size.width * zoom, rect.size.height * zoom);
    
    CGRect visibleRect = CGRectIntersection(pageViewPort, zommedChangeRect);
    
    if( !CGRectIsNull(visibleRect))
    {
        self.detailPopover.passthroughViews = @[pageView];
        self.detailPopover.popoverContentSize = size;
        [self.detailPopover presentPopoverFromRect:visibleRect inView:self.pdfController.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    }
    else if (self.detailPopover && self.detailPopover.isPopoverVisible)
    {
        [self.detailPopover dismissPopoverAnimated:YES];
    }
}

- (void) refreshPopover
{
    if( self.detailPopover && self.detailPopover.isPopoverVisible)
    {
        PSPDFPageView* pageView = [self.pdfController pageViewForPage:self.pdfController.page];
        CGSize size = self.detailPopover.popoverContentSize;
        [self presentPopoverWithSize:size inRect:self.selectedChangeRect inPageView:pageView];
    }
}

- (BOOL)pdfController:(ADXPDFViewController *)pdfController shouldSelectAnnotation:(PSPDFAnnotation *)annotation onPageView:(PSPDFPageView *)pageView
{
    if( self.detailPopover ){
        [self.detailPopover dismissPopoverAnimated:YES];
    }
    return YES;
}

- (void)pdfController:(ADXPDFViewController *)pdfController willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    __weak ADXRevisionsViewController *weakSelf = self;
    [self removeChangeAnnotationsAnimated:YES completion:^{
        UIInterfaceOrientation interfaceOrientation = [[UIApplication sharedApplication] statusBarOrientation];
        if ( interfaceOrientation == toInterfaceOrientation ){
            [weakSelf addChangeAnnotationsAnimated:YES];
        }
    }];
}

- (void)pdfController:(ADXPDFViewController *)pdfController didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
     [self addChangeAnnotationsAnimated:YES];
}

- (IBAction)toggleVersionsSelector:(id)sender
{
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        [self toggleVersionsSelectorForiPhone:sender];
    } else {
        [self toggleVersionsSelectorForiPad:sender];
    }
}

- (void)internalToggleVersionsSelectorForiPhone:(id)sender
{
    __weak ADXRevisionsViewController *weakSelf = self;
    ADXAppDelegate *appDelegate = (ADXAppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *moc = [appDelegate managedObjectContext];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        UIViewController *vc = [ADXDocVersionComparisonViewController modalControllerWithDocument:weakSelf.pdfController.adxDocument
                                                                   managedObjectContext:moc
                                                                         actionDelegate:weakSelf];
        [weakSelf.pdfController presentModalViewController:vc embeddedInNavigationController:NO withCloseButton:YES animated:YES];
    });
}

- (void)toggleVersionsSelectorForiPhone:(id)sender
{
    __weak ADXRevisionsViewController *weakSelf = self;
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [weakSelf.pdfController.service refreshDocumentVersions:weakSelf.pdfController.adxDocument.id completion:^(BOOL success, NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [MBProgressHUD hideHUDForView:weakSelf.view animated:YES];
                [weakSelf internalToggleVersionsSelectorForiPhone:sender];
            });
        }];
    });
}

- (void)toggleVersionsSelectorForiPad:(id)sender
{
    if ([self.currentPopoverController isPopoverVisible]) {
        [self dismissPopovers];
    } else {
        __weak ADXRevisionsViewController *weakSelf = self;
        
        [MBProgressHUD showHUDAddedTo:self.pdfController.view animated:YES];
        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            [weakSelf.pdfController.service refreshDocumentVersions:weakSelf.pdfController.adxDocument.id completion:^(BOOL success, NSError *error) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [MBProgressHUD hideHUDForView:weakSelf.pdfController.view animated:YES];
                    [weakSelf internalToggleVersionsSelectorForiPad:sender];
                });
            }];
        });
    }
}

- (void)internalToggleVersionsSelectorForiPad:(id)sender
{
    __weak ADXRevisionsViewController *weakSelf = self;
    ADXAppDelegate *appDelegate = (ADXAppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *moc = [appDelegate managedObjectContext];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        weakSelf.currentPopoverController = [ADXDocVersionComparisonViewController popoverControllerWithDocument:weakSelf.pdfController.adxDocument
                                                                                            managedObjectContext:moc
                                                                                                  actionDelegate:weakSelf];
        [weakSelf.currentPopoverController presentPopoverFromBarButtonItem:sender
                                                  permittedArrowDirections:UIPopoverArrowDirectionUp
                                                                  animated:YES];
    });
}

- (void)dismissPopovers
{
    if ([self.currentPopoverController isPopoverVisible]) {
        [self.currentPopoverController dismissPopoverAnimated:YES];
        self.currentPopoverController = nil;
    }
    if ([self.detailPopover isPopoverVisible]) {
        [self.detailPopover dismissPopoverAnimated:YES];
        self.detailPopover = nil;
    }
}

- (void)didSelectDoumentForComparison:(ADXDocument*)document
{
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        [self.pdfController dismissViewControllerAnimated:YES completion:NULL];
    } else {
        [self dismissPopovers];
    }

    ADXDocument *adxDocument =  self.pdfController.adxDocument;
    
    __weak ADXRevisionsViewController *weakSelf = self;
    [MBProgressHUD showHUDAddedTo:self.pdfController.view animated:YES];
    [self.pdfController.service changesForDocument:adxDocument.id versionID:adxDocument.version compare:document.version completion:^(id response, BOOL success, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [MBProgressHUD hideAllHUDsForView:weakSelf.pdfController.view animated:YES];
            if (success) {
                weakSelf.revisionVersionSelector.title = [NSString stringWithFormat:KComparisonButtonLabel,(int)document.version.intValue];
                weakSelf.currentHighlightedContent = response;
                weakSelf.currentHighlightedContentVersion = document.version.intValue;
                
                [weakSelf refreshChanges];
                [weakSelf refresh];
                
                [weakSelf refreshChangeStrips];
                [self.pdfController refreshPDFStrips];
            } else {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[ADXAppDelegate sharedInstance] presentNeedToBeOnlineAlert];
                });
            }
        });
    }];
}

#pragma mark - ADXPDFStripsDataSource

- (int)numberOfSectionsForPDFStrips
{
    return self.changeStrips.count;
}

- (int)numberOfPDFStripsInSection:(int)section
{
    NSMutableDictionary *dict = self.changeStrips[section];
    NSArray *changes = dict[@"strips"];
    return changes.count;
}

- (ADXPDFStripInfo *)infoForPDFStripAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *pageChanges = [self.changeStrips objectAtIndex:indexPath.section];
    return [pageChanges[@"strips"] objectAtIndex:indexPath.item];
}

- (NSString *)titleForPDFStripSection:(int)section
{
    NSMutableDictionary *sectionDict = [self.changeStrips objectAtIndex:section];
    NSArray *changes = sectionDict[@"strips"];
    ADXPDFStripInfo *stripInfo = [changes objectAtIndex:0];
    
    NSString *format = NSLocalizedString(@"Changes on page %d", nil);
    NSString *title = [NSString stringWithFormat:format, stripInfo.pageIndex+1];
    return title;
}

- (ADXHighlightedContentHelper *)highlightedContentForPDFStrips
{
    return self.currentHighlightedContent;
}

@end

@implementation Change

- (id)init
{
    self = [super init];
    if (self) {
        _description = @"";
        _pageInfo = @"";
        _pageNumber = NSNotFound;
    }
    return self;}


@end
