//
//  ADXDocActionsDelegate.h
//  Reader
//
//  Created by Gavin McKenzie on 2012-09-14.
//  Copyright (c) 2012 ActivDox Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ActivDoxCore.h"

@protocol ADXDocActionsDelegate <NSObject>
@optional
- (void)didSelectDocShareAction:(id)sender document:(ADXDocument *)document;
- (void)didSelectDocOpenAction:(id)sender document:(ADXDocument *)document;
- (void)didSelectDocAnalyticsAction:(id)sender document:(ADXDocument *)document;
- (void)didSelectDocRemoveAction:(id)sender document:(ADXDocument *)document;
- (void)didSelectDocPermissionsAction:(id)sender document:(ADXDocument *)document;
@end
