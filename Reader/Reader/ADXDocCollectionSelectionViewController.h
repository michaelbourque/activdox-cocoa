//
//  ADXDocCollectionSelectionViewController.h
//  Reader
//
//  Created by Gavin McKenzie on 2013-04-23.
//  Copyright (c) 2013 ActivDox Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ActivDoxCore.h"

@class ADXDocCollectionSelectionViewController;

@protocol ADXDocCollectionSelectionDelegate <NSObject>
- (void)didSelectCollection:(ADXCollection *)collection;
@end

@interface ADXDocCollectionSelectionViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) UIPopoverController *parentPopoverController;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (strong, nonatomic) NSFetchedResultsController *listFetchedResultsController;
@property (strong, nonatomic) NSFetchedResultsController *searchFetchedResultsController;
@property (strong, nonatomic) ADXDocument *document;
@property (weak, nonatomic) id<ADXDocCollectionSelectionDelegate> actionDelegate;
@end
