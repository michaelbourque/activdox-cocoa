//
//  ADXPageThumbnailView.m
//  Reader
//
//  Created by Matteo Ugolini on 12-08-15.
//  Copyright (c) 2012 ActivDox Inc. All rights reserved.
//

#import "ADXPageThumbnailView.h"

@interface ADXPageThumbnailView () <UIScrollViewDelegate>

@property (strong, nonatomic) IBOutlet UIImageView *documentBackgroundView;
@property (nonatomic, weak) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *metaLabel;
@end

@implementation ADXPageThumbnailView

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    NSString *pageIndex = [NSString stringWithFormat:@"%d", self.pageIndex + 1];
    CGSize s = [pageIndex sizeWithFont:self.titleLabel.font];
    
    CGRect frame = self.titleLabel.frame;
    frame.size.width = MIN(self.frame.size.width - 10, MAX(30, s.width + frame.size.height));
    frame.origin.x = (self.frame.size.width - frame.size.width) / 2;
    
    self.titleLabel.frame = frame;
    self.titleLabel.text = pageIndex;
    self.titleLabel.layer.cornerRadius = frame.size.height / 2;;
    
    self.metaLabel.text = self.metaInfo;
    
    [self buildThumbnail];
}

- (void)dealloc
{
    [[PSPDFCache sharedCache] removeDelegate:self];
}

- (void)willMoveToSuperview:(UIView *)newSuperview
{
    NSAssert(self.pdfDocument != nil, @"Expected document to be set");
    
    if (newSuperview == nil) {
        [[PSPDFCache sharedCache] removeDelegate:self];
    }
}

- (void)prepareForReuse
{
    self.pdfDocument = nil;
    self.titleLabel.text = nil;
    self.imageView.image = nil;
    self.metaLabel.text = nil;
}

- (void)setPdfDocument:(PSPDFDocument *)pdfDocument
{
    _pdfDocument = pdfDocument;

    [self setNeedsLayout];
}


- (void)setMetaInfo:(NSString *)metaInfo
{
    _metaInfo = metaInfo;
    
    [self setNeedsLayout];
}

- (void)setPageIndex:(NSUInteger)pageIndex
{
    _pageIndex = pageIndex;
    
    [self setNeedsLayout];
}

#pragma mark - Private Methods

- (void)presentThumbnailImage:(UIImage *)thumbnail
{
    self.imageView.contentMode = UIViewContentModeScaleAspectFit;
    self.imageView.image = thumbnail;
    self.documentBackgroundView.hidden = YES;
}

- (void)presentDefaultImage
{
    self.imageView.contentMode = UIViewContentModeScaleAspectFit;
    self.imageView.image = [UIImage imageNamed:@"57-download.png"];
    self.documentBackgroundView.hidden = YES;
}

- (void) buildThumbnail
{
    [[PSPDFCache sharedCache] addDelegate:self];
    UIImage *cachedImage = [[PSPDFCache sharedCache] cachedImageForDocument:self.pdfDocument
                                                                            page:self.pageIndex
                                                                            size:PSPDFSizeThumbnail];
    if (cachedImage == nil) {
        [self presentDefaultImage];
    } else {
        [[PSPDFCache sharedCache] removeDelegate:self];
        [self presentThumbnailImage:cachedImage];
    }
}

#pragma mark - PSPDFCacheDelegate

- (void)didCachePageForDocument:(PSPDFDocument *)document page:(NSUInteger)page image:(UIImage *)cachedImage size:(PSPDFSize)size
{
    if (document == self.pdfDocument && page == self.pageIndex && size == PSPDFSizeThumbnail) {
        [self presentThumbnailImage:cachedImage];
    }
}

@end
