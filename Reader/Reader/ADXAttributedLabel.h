//
//  OHAttributedLabel+ADXAttributedLabelHighlight.h
//  Reader
//
//  Created by Matteo Ugolini on 12-09-14.
//  Copyright (c) 2012 ActivDox Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PSPDFAttributedLabel.h"

@interface ADXAttributedLabel : PSPDFAttributedLabel

@property (nonatomic) UIColor* highlightedLinkColor;
@property (nonatomic, readonly) NSRange highlightedTextRage;

-(void)highlightTextInRange:(NSRange)textRange;

@end
