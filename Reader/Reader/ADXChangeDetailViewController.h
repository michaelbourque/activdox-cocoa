//
//  ADXChangeDetailViewController.h
//  Reader
//
//  Created by Matteo Ugolini on 2012-11-06.
//  Copyright (c) 2012 ActivDox Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ADXChangeDetailViewController : UIViewController

@property (nonatomic, weak, readwrite) IBOutlet UILabel* titleView;

- (void)showDescriptionText:(NSString *)text;
- (void)showChangeImage:(UIImage *)image;

@end
