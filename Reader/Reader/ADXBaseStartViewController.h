//
//  ADXBaseStartViewController.h
//  
//  Created by Gavin McKenzie on 2012-08-28.
//
//

#import <UIKit/UIKit.h>
#import "ActivDoxCore.h"
#import "ADXAppCommonMacros.h"

extern NSString * const kADXStartViewControllerAlreadyLaunched;

@interface ADXBaseStartViewController : UIViewController
@property (strong, nonatomic) ADXLoginProcessor *loginProcessor;
- (void)setupLoginProcessor;
- (BOOL)firstLaunch;
- (BOOL)displayBlankStartupScreen;
@end
