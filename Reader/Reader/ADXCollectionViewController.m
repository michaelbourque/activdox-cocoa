//
//  ADXCollectionViewController.m
//  Reader
//
//  Created by Gavin McKenzie on 2012-10-15.
//  Copyright (c) 2012 ActivDox Inc. All rights reserved.
//

#import "ADXCollectionViewController.h"
#import "ADXAppDelegate.h"

#import "ADXCollectionController.h"
#import "ADXDocVersionViewController.h"
#import "ADXDocSharingController.h"
#import "ADXDocumentAnalyticsViewController.h"
#import "ADXNotificationsViewController.h"
#import "ADXCollectionDocumentsViewController.h"

#import "ADXCollectionViewCell.h"
#import "ADXCollectionHeaderView.h"
#import "ADXCollectionViewFlowLayout.h"

#import "ADXBarButtonItem.h"
#import "ADXImageCache.h"
#import "ADXPDFViewController.h"
#import "ADXPopoverBackgroundView.h"

#import "MBProgressHUD.h"
#import "UIAlertView+Blocks.h"

#import <QuartzCore/QuartzCore.h>
#import <MessageUI/MessageUI.h>



static NSUInteger const kADXCollectionThumbnailCellPageTag = 100;

typedef enum {
    ADXCollectionViewNavbarShowTitleState = 0,
    ADXCollectionViewNavbarShowCountState,
    ADXCollectionViewNavbarShowMessageState,
} ADXCollectionViewNavbarTitleState;

static NSString *kADXCollectionThumbnailCellMediumReuseIdentifier = @"ADXCollectionThumbnailCellMedium";
static NSString *kADXCollectionThumbnailCellSmallReuseIdentifier = @"ADXCollectionThumbnailCellSmall";
static NSString *kADXCollectionListCellSmallReuseIdentifier = @"ADXCollectionListCellSmall";
static NSString *kADXCollectionHeaderViewIdentifier = @"ADXCollectionHeaderView";

static NSUInteger const kADXCollectionThumbnailMediumImageWidth_iPad = 129;
static NSUInteger const kADXCollectionThumbnailMediumImageHeight_iPad = 170;

@interface ADXCollectionViewController () <UIPopoverControllerDelegate, UIActionSheetDelegate, UICollectionViewDataSource, UICollectionViewDelegate, ADXDocActionsDelegate, ADXNotificationsActionsDelegate, ADXCollectionControllerDelegate, ADXURLSchemeRequestDelegate, ADXCollectionDocumentsActionsDelegate, ADXCollectionHeaderViewDelegate, MFMailComposeViewControllerDelegate>
@property (strong, nonatomic) ADXCollectionController *collectionController;
@property (strong, nonatomic) ADXDocSharingController *documentSharingController;
@property (weak, nonatomic) UIViewController *selectedViewController;
@property (weak, nonatomic) ADXDocument *longPressedDocument;
@property (strong, nonatomic) UIPopoverController *currentPopoverController;
@property (weak, nonatomic) UIPopoverController *addDocumentsPopoverController;
@property (weak, nonatomic) UIView *popoverPresentFromRectOfView;
@property (weak, nonatomic) UIView *popoverPresentInView;
@property (weak, nonatomic) UIBarButtonItem *popoverPresentFromBarButtonItem;
@property (nonatomic) BOOL alreadyDidAutoOpenDocumentCheck;
@property (assign, nonatomic) ADXCollectionViewNavbarTitleState titleState;
@property (assign, nonatomic) BOOL stopTitleRotation;
@property (assign, nonatomic) BOOL titleRotationInProgress;
@property (strong, nonatomic) UIButton *displayModeToggleControl;
@property (weak, nonatomic) UIBarButtonItem *addButtonItem;
@property (strong, nonatomic) UICollectionView *collectionView;
@property (strong, nonatomic) NSMutableArray *collectionSectionChanges;
@property (strong, nonatomic) NSMutableArray *collectionItemChanges;
@property (strong, nonatomic) ADXImageCache *imageCache;
@property (strong, nonatomic) MBProgressHUD *activityHUD;
@end

@implementation ADXCollectionViewController

#pragma mark - Initialization and Destruction

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [self finishInitialization];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self finishInitialization];
    }
    return self;
}

- (void)finishInitialization
{
    NSManagedObjectContext *moc = [ADXAppDelegate sharedInstance].managedObjectContext;
    NSUInteger sortOrder = [[ADXUserDefaults sharedDefaults] documentSortOrder];
    _collectionController = [[ADXCollectionController alloc] initWithManagedObjectContext:moc sortFilter:sortOrder];
}

- (void)cleanup
{
    self.collectionController.delegate = nil;
    self.collectionController = nil;
}

- (void)dealloc
{
    _collectionController.delegate = nil;
}

#pragma mark - Memory Management

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - Notifications

- (void)watchNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleServiceUnauthorizedNotification:)
                                                 name:kADXServiceURLRequestUnauthorizedNotification
                                               object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleServiceCollectionRefreshCompletedNotification:)
                                                 name:kADXServiceCollectionRefreshCompletedNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleServiceDocumentDownloadedNotification:)
                                                 name:kADXServiceDocumentDownloadedNotification
                                               object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleURLSchemeRequestNotification:)
                                                 name:kADXURLSchemeRequestNotification
                                               object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleReachabilityChanged:)
                                                 name:kADXServiceNotificationReachabilityChanged
                                               object:nil];
}

- (void)unwatchNotifications
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kADXServiceURLRequestUnauthorizedNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kADXServiceCollectionRefreshCompletedNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kADXServiceDocumentDownloadedNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kADXURLSchemeRequestNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kADXServiceNotificationReachabilityChanged object:nil];
}

- (void)handleServiceUnauthorizedNotification:(NSNotification *)notification
{
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        [[ADXAppDelegate sharedInstance] presentUserReauthenticationPopupIfNecessary];
    }];
}

- (void)handleServiceCollectionRefreshCompletedNotification:(NSNotification *)notification
{
    NSString *collectionID = [notification.userInfo valueForKey:kADXServiceNotificationPayload];
    if (![collectionID isEqualToString:self.collection.id]) {
        return;
    }
    
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        [self startRotatingTitleMessage];
    }];
}

- (void)handleServiceDocumentDownloadedNotification:(NSNotification *)notification
{
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        if (self.isViewLoaded && self.view.window && [ADXAppDelegate sharedInstance].pendingURLSchemeRequest) {
            [self processPendingURLRequest];
        }
    }];
}

- (void)handleURLSchemeRequestNotification:(NSNotification *)notification
{
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        if (self.isViewLoaded && self.view.window && [ADXAppDelegate sharedInstance].pendingURLSchemeRequest) {
            [self processPendingURLRequest];
        }
    }];
}

#pragma mark - View Lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.collectionController.collection = self.collection;
    self.collectionSectionChanges = nil;
    self.collectionItemChanges = nil;
    
    [self setupBackgroundView];
    [self setupNavbar];
    [self setupToolbar];
    [self setupTitleMessage];
    [self stopRotatingTitleMessage];
    [self setupCollectionView];
    
    [self setupLocationServices];
    
    self.titleMessage.layer.shadowOpacity = 1.0;
    self.titleMessage.layer.shadowRadius = 0.0;
    self.titleMessage.layer.shadowColor = [UIColor colorWithWhite:0.5 alpha:1.0].CGColor;
    self.titleMessage.layer.shadowOffset = CGSizeMake(0.0f, -1.0f);    

    NSAssert(self.collection != nil, @"Expected collection to be populated");
    [[ADXUserDefaults sharedDefaults] setCurrentlyOpenCollectionID:self.collection.id localID:self.collection.localID];
}

- (void)viewWillAppear:(BOOL)animated
{
    // Start watching for changes
    self.collectionController.delegate = self;
    [self refresh:YES];

    if (self.isMovingToParentViewController) {
        [self watchNotifications];
    }

    NSUInteger currentDisplayMode = [ADXUserDefaults sharedDefaults].currentDocumentBrowserDisplayMode;
    [self setDisplayMode:currentDisplayMode];

    ADXAppDelegate *appDelegate = (ADXAppDelegate *)[[UIApplication sharedApplication] delegate];
    [appDelegate setReaderAppState:ADXAppState.viewingDocBrowser];

    [self.selectedViewController beginAppearanceTransition:YES animated:animated];

    [self.navigationController setToolbarHidden:YES];
    [self restoreStatusBar:animated];
    [self repositionNavigationBar];

    // Check to see if we've been asked to open a certain document
    [self processPendingURLRequest];
    
    // If we're still here...
    if ([ADXAppDelegate sharedInstance].readerAppState == ADXAppState.viewingDocBrowser) {
        [self presentAnyPreviouslyOpenedDocument];
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self.selectedViewController endAppearanceTransition];

    self.titleState = ADXCollectionViewNavbarShowTitleState;
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    // Stop watching for changes
    self.collectionController.delegate = nil;
    
    [self dismissPopovers];
    if (self.isMovingFromParentViewController) {
        [self unwatchNotifications];
    }
}

- (void)didMoveToParentViewController:(UIViewController *)parent
{
    if (parent == nil) {
        // View popped from the navigation stack
        [[ADXUserDefaults sharedDefaults] setCurrentlyOpenCollectionID:nil localID:nil];
    }
}

#pragma mark - View Management

- (UINib *)nibForCellThumbnailMedium
{
    UINib *result = nil;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        result = [UINib nibWithNibName:@"ADXCollectionThumbnailCellMedium" bundle:nil];
    } else {
        result = [UINib nibWithNibName:[NSString stringWithFormat:@"%@-iPhone", @"ADXCollectionThumbnailCellMedium"] bundle:nil];
    }
    return result;
}

- (UINib *)nibForCellThumbnailSmall
{
    UINib *result = nil;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        result = [UINib nibWithNibName:@"ADXCollectionThumbnailCellSmall" bundle:nil];
    } else {
        result = [UINib nibWithNibName:[NSString stringWithFormat:@"%@-iPhone", @"ADXCollectionThumbnailCellSmall"] bundle:nil];
    }
    return result;
}

- (UINib *)nibForCellListSmall
{
    UINib *result = nil;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        result = [UINib nibWithNibName:@"ADXCollectionListCellSmall" bundle:nil];
    } else {
        result = [UINib nibWithNibName:[NSString stringWithFormat:@"%@-iPhone", @"ADXCollectionListCellSmall"] bundle:nil];
    }
    return result;
}

- (UINib *)nibForHeader
{
    UINib *result = nil;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        result = [UINib nibWithNibName:@"ADXCollectionHeaderView" bundle:nil];
    } else {
        result = [UINib nibWithNibName:[NSString stringWithFormat:@"%@-iPhone", @"ADXCollectionHeaderView"] bundle:nil];
    }
    return result;
}

- (void)setupNavbar
{
    if ([self.collection.type isEqualToString:ADXCollectionType.user]) {
        UIBarButtonItem *addButtonItem = [ADXBarButtonItem itemWithImageNamed:@"navBarAddButton" target:self selector:@selector(didTapAddDocumentButton:)];
        self.navigationItem.rightBarButtonItems = @[self.notificationsBarButton, addButtonItem];
        self.addButtonItem = addButtonItem;
    } else {
        self.navigationItem.rightBarButtonItems = @[self.notificationsBarButton];
    }

    UIButton *displayModeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [displayModeButton setShowsTouchWhenHighlighted:YES];
    [displayModeButton setImage:[UIImage imageNamed:@"269-TwoColumn"] forState:UIControlStateNormal];
    [displayModeButton addTarget:self action:@selector(didTapDisplayModeToggleButton:) forControlEvents:UIControlEventTouchUpInside];
    [displayModeButton setFrame:CGRectMake(0, 0, 45.0f, 25.0f)];
    [displayModeButton setContentEdgeInsets:UIEdgeInsetsMake(0.0f, 10.0f, 0.0f, 10.0f)];
    [displayModeButton setContentMode:UIViewContentModeScaleAspectFit];
    self.displayModeToggleControl = displayModeButton;
    UIBarButtonItem *displayModeButtonItem = [[UIBarButtonItem alloc] initWithCustomView:self.displayModeToggleControl];

    self.navigationItem.leftItemsSupplementBackButton = YES;
    self.navigationItem.leftBarButtonItems = @[displayModeButtonItem];
}

- (void)setupToolbar
{
    [self.navigationController setToolbarHidden:YES];
}

- (void)setupTitleMessage
{
    self.titleMessage.enabled = YES;
    self.titleMessage.userInteractionEnabled = YES;
}

- (void)setupBackgroundView
{
    CGFloat toolbarHeight = self.navigationController.navigationBar.frame.size.height;
    UIView *backgroundTextureView = [[UIView alloc] initWithFrame:CGRectMake(0, -toolbarHeight, self.view.bounds.size.width, self.view.bounds.size.height + toolbarHeight)];
    backgroundTextureView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    backgroundTextureView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"light-bg"]];
    [self.view addSubview:backgroundTextureView];
}

- (void)setupLocationServices{
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters;
    [locationManager startUpdatingLocation];
    
    [self checkForLocationSensitiveDocuments];
    //hack because the document is updated and restored via the server
    [self performSelector:@selector(checkForLocationSensitiveDocuments) withObject:self afterDelay:1.0];
    [self performSelector:@selector(checkForLocationSensitiveDocuments) withObject:self afterDelay:2.0];
}

-(void)checkForLocationSensitiveDocuments{
    
    NSString *accountID = [ADXAppDelegate sharedInstance].currentAccount.id;
    NSManagedObjectContext *moc = [ADXAppDelegate sharedInstance].managedObjectContext;
    NSString *path = [[NSBundle mainBundle] pathForResource:@"LocationsDemo" ofType:@"plist"];
    NSString *docID;
    
    static bool deviceWithinRange = false;
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    if ([fileManager fileExistsAtPath:path]) {
        
        NSMutableDictionary *plistDict = [[NSMutableDictionary alloc] initWithContentsOfFile:path];
        docID = [plistDict objectForKey:@"documentID"]; //test that it exists
        
        [ADXDocument makeDocumentInaccessible:docID accountID:accountID status:ADXDocumentStatus.inaccessible context:moc];     //make the document inaccessible before we begin our checks
        
        // should also check if document exists
        if (docID) {
            if (location) {
                NSArray * locations= [plistDict objectForKey:@"locations"];
                for (NSDictionary *thelocation in locations){
                    NSNumber* latitude = (NSNumber*) [thelocation objectForKey:@"latitude"];
                    NSNumber* longitude= (NSNumber*)[thelocation objectForKey:@"longitude"];
                    if (longitude && latitude) {
                        CLLocation * loc = [[CLLocation alloc]initWithLatitude:latitude.floatValue longitude:longitude.floatValue];
                        if ([location distanceFromLocation:loc] < 30) {
                            deviceWithinRange = true;
                            break;
                        }else if ([location distanceFromLocation:loc] > 30 && !deviceWithinRange){
                            [ADXDocument makeDocumentInaccessible:docID accountID:accountID status:ADXDocumentStatus.inaccessible context:moc];
                        }
                    }
                }
            }
            else{
               [ADXDocument makeDocumentInaccessible:docID accountID:accountID status:ADXDocumentStatus.inaccessible context:moc];
            }
        }
    }
    
    if(deviceWithinRange) {
        [ADXDocument restoreInaccessibleVersions:docID accountID:accountID context:moc];
        return;
    }else{
        [ADXDocument makeDocumentInaccessible:docID accountID:accountID status:ADXDocumentStatus.inaccessible context:moc];
    }
    
    deviceWithinRange = false;

}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    if ([self.currentPopoverController isPopoverVisible]) {
        if (self.popoverPresentFromBarButtonItem) {
            [self.currentPopoverController presentPopoverFromBarButtonItem:self.popoverPresentFromBarButtonItem permittedArrowDirections:UIPopoverArrowDirectionAny animated:NO];
        } else {
            [self.currentPopoverController presentPopoverFromRect:self.popoverPresentFromRectOfView.frame
                                                           inView:self.popoverPresentInView
                                         permittedArrowDirections:(UIPopoverArrowDirectionLeft | UIPopoverArrowDirectionRight)
                                                         animated:NO];
        }
    }
}

- (void)restoreStatusBar:(BOOL)animated
{
    __weak ADXCollectionViewController *weakSelf = self;
    // ensure our navigation bar is visible. PSPDFKit restores the properties,
    // but since we're doing a custom fade-out on the navigationBar alpha,
    // we also have to restore this properly.
    [self.navigationController setNavigationBarHidden:NO animated:animated];
    [UIView animateWithDuration:0.25f animations:^{
        weakSelf.navigationController.navigationBar.alpha = 1.f;
    }];
    
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationFade];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault animated:YES];
}

- (void)repositionNavigationBar
{
    // if navigationBar is offset, we're fixing that.
    if (self.navigationController.navigationBar) {
        CGRect navigationBarFrame = self.navigationController.navigationBar.frame;
        CGRect statusBarFrame = [UIApplication sharedApplication].statusBarFrame;
        if (navigationBarFrame.origin.y <= statusBarFrame.origin.y) {
            // compensate rotation
            navigationBarFrame.origin.y = fminf(statusBarFrame.size.height, statusBarFrame.size.width);
            self.navigationController.navigationBar.frame = navigationBarFrame;
        }
    }
}

- (void)refresh:(BOOL)forceLoadAll
{
    [self.collectionController refresh:forceLoadAll];
}

#pragma mark - UICollection View Setup

- (void)setupCollectionView
{
    ADXCollectionViewFlowLayout *layout = [[ADXCollectionViewFlowLayout alloc] init];
    layout.scrollDirection = UICollectionViewScrollDirectionVertical;
    layout.headerReferenceSize = CGSizeMake(0.0f, 50.0f);
        
    [self setupImageCache];
    
    CGRect outerViewBounds = self.view.bounds;
    outerViewBounds.size.height -= 0.0f;
    self.collectionView = [[UICollectionView alloc] initWithFrame:outerViewBounds collectionViewLayout:layout];
    self.collectionView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    self.collectionView.backgroundColor = [UIColor clearColor];
    self.collectionView.alwaysBounceVertical = YES;
    
    [self.collectionView registerNib:[self nibForCellThumbnailMedium] forCellWithReuseIdentifier:kADXCollectionThumbnailCellMediumReuseIdentifier];
    [self.collectionView registerNib:[self nibForCellThumbnailSmall] forCellWithReuseIdentifier:kADXCollectionThumbnailCellSmallReuseIdentifier];
    [self.collectionView registerNib:[self nibForCellListSmall] forCellWithReuseIdentifier:kADXCollectionListCellSmallReuseIdentifier];
    [self.collectionView registerNib:[self nibForHeader] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:kADXCollectionHeaderViewIdentifier];

    [self.view addSubview:self.collectionView];
    
    self.collectionView.dataSource = self;
    self.collectionView.delegate = self;
    
    [self setupLongPressGesture];
    
}

- (void)setupImageCache
{
    self.imageCache = [[ADXImageCache alloc] initWithImageSize:[self currentThumbnailSize]];
}

- (void)resetImageCache
{
    [self.imageCache resetCacheWithImageSize:[self currentThumbnailSize]];
}

- (CGSize)currentThumbnailSize
{
    CGSize result = CGSizeZero;

    // TODO: implement for iPhone
    if (self.displayMode == ADXCollectionViewDisplayModeThumbnail) {
        result = CGSizeMake(129.0f, 170.0f);
    } else if (self.displayMode == ADXCollectionViewDisplayModeThumbnailSmall) {
        result = CGSizeMake(99.0f, 132.0f);
    } else {
        result = CGSizeMake(79.0f, 105.0f);
    }
    
    return result;
}

- (void)setupLongPressGesture
{
    UILongPressGestureRecognizer* longPressGesture = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongPressGesture:)];
    NSArray* recognizers = [self.collectionView gestureRecognizers];
    
    // Make the default gesture recognizer wait until the custom one fails.
    for (UIGestureRecognizer* aRecognizer in recognizers) {
        if ([aRecognizer isKindOfClass:[UILongPressGestureRecognizer class]])
            [aRecognizer requireGestureRecognizerToFail:longPressGesture];
    }
    
    // Now add the gesture recognizer to the collection view.
    [self.collectionView addGestureRecognizer:longPressGesture];
}

#pragma mark - UICollectionViewDelegateFlowLayout

-(void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [self.collectionView reloadData];
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    UIEdgeInsets result = UIEdgeInsetsZero;
    
    if (UIDeviceOrientationIsLandscape([[UIApplication sharedApplication] statusBarOrientation])) {
        if (self.displayMode == ADXCollectionViewDisplayModeThumbnail) {
            result = UIEdgeInsetsMake(8.0f, 4.0f, 0.0f, 4.0f);
        } else if (self.displayMode == ADXCollectionViewDisplayModeThumbnailSmall) {
            result = UIEdgeInsetsMake(8.0f, 0.0f, 0.0f, 0.0f);
        } else {
            result = UIEdgeInsetsMake(8.0f, 96.0f, 0.0f, 96.0f);
        }
    } else {
        if (self.displayMode == ADXCollectionViewDisplayModeThumbnail) {
            result = UIEdgeInsetsMake(8.0f, 36.0f, 0.0f, 36.0f);
        } else if (self.displayMode == ADXCollectionViewDisplayModeThumbnailSmall) {
            result = UIEdgeInsetsMake(8.0f, 0.0f, 0.0f, 0.0f);
        } else {
            result = UIEdgeInsetsMake(8.0f, 36.0f, 0.0f, 36.0f);
        }
    }
    return result;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    CGFloat result = 0.0f;
    
    if (UIDeviceOrientationIsLandscape([[UIApplication sharedApplication] statusBarOrientation])) {
        if (self.displayMode == ADXCollectionViewDisplayModeThumbnail) {
            result = 6.0f;
        } else if (self.displayMode == ADXCollectionViewDisplayModeThumbnailSmall) {
            result = 32.0f;
        } else {
            result = 0.0f;
        }
    } else {
        if (self.displayMode == ADXCollectionViewDisplayModeThumbnail) {
            result = 8.0f;
        } else if (self.displayMode == ADXCollectionViewDisplayModeThumbnailSmall) {
            result = 38.0f;
        } else {
            result = 0.0f;
        }
    }
    return result;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGSize result = CGSizeZero;
    
    if (UIDeviceOrientationIsLandscape([[UIApplication sharedApplication] statusBarOrientation])) {
        if (self.displayMode == ADXCollectionViewDisplayModeThumbnail) {
            result = CGSizeMake(330.0f, 200.0f);
        } else if (self.displayMode == ADXCollectionViewDisplayModeThumbnailSmall) {
            result = CGSizeMake(256.0f, 160.0f);
        } else {
            result = CGSizeMake(696.0f, 120.0f);
        }
    } else {
        if (self.displayMode == ADXCollectionViewDisplayModeThumbnail) {
            result = CGSizeMake(330.0f, 200.0f);
        } else if (self.displayMode == ADXCollectionViewDisplayModeThumbnailSmall) {
            result = CGSizeMake(256.0f, 160.0f);
        } else {
            result = CGSizeMake(696.0f, 120.0f);
        }
    }
    return result;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    if (self.displayMode == ADXCollectionViewDisplayModeThumbnailSmall) {
        return 0.0f;
    } else {
        return 4.0f;
    }
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    NSInteger result = [self.collectionController numberOfItemsInSection:section];
    return result;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    NSInteger result = [self.collectionController numberOfSections];
    return result;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    ADXCollectionViewCell *cell = nil;
    
    if (self.displayMode == ADXCollectionViewDisplayModeThumbnail) {
        cell = [self.collectionView dequeueReusableCellWithReuseIdentifier:kADXCollectionThumbnailCellMediumReuseIdentifier forIndexPath:indexPath];
    } else if (self.displayMode == ADXCollectionViewDisplayModeThumbnailSmall) {
        cell = [self.collectionView dequeueReusableCellWithReuseIdentifier:kADXCollectionThumbnailCellSmallReuseIdentifier forIndexPath:indexPath];
    } else {
        cell = [self.collectionView dequeueReusableCellWithReuseIdentifier:kADXCollectionListCellSmallReuseIdentifier forIndexPath:indexPath];
    }
    
    cell.trackReachabilityChanges = YES;
    cell.documentEntity = [self.collectionController objectAtIndexPath:indexPath];
    cell.imageCache = self.imageCache;
    
    return cell;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    UICollectionReusableView *reusableview = nil;
    
    if (kind == UICollectionElementKindSectionHeader) {
        ADXCollectionHeaderView *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader
                                                                                 withReuseIdentifier:kADXCollectionHeaderViewIdentifier
                                                                                        forIndexPath:indexPath];
        headerView.actionDelegate = self;
        headerView.sortSegmentedButtons.selectedSegmentIndex = self.collectionController.sortFilter;
        reusableview = headerView;
    }
    
    return reusableview;
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    ADXDocument *document = [self.collectionController objectAtIndexPath:indexPath];
    if ([ADXUserDefaults sharedDefaults].shouldOpenLastReadVersion) {
        // See if there's a last read version we should be opening instead of the latest version
        ADXDocument *lastDocument = [ADXDocument mostRecentlyOpenedDocumentWithID:document.id
                                                                        accountID:[ADXAppDelegate sharedInstance].currentAccount.id
                                                                          context:document.managedObjectContext];
        if (lastDocument != nil) {
            document = lastDocument;
        }
    }
    
    [self openActionForDocument:document onPage:0 fromRectOfView:[collectionView cellForItemAtIndexPath:indexPath] inView:collectionView animated:YES];
}

- (BOOL)collectionView:(UICollectionView *)collectionView shouldShowMenuForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return NO;
}

#pragma mark - Accessors

- (void)setDisplayMode:(ADXCollectionViewDisplayMode)displayMode
{
    _displayMode = displayMode;
    
    [ADXUserDefaults sharedDefaults].currentDocumentBrowserDisplayMode = displayMode;
        
    if (displayMode == ADXCollectionViewDisplayModeThumbnail) {
        [self.displayModeToggleControl setImage:[UIImage imageNamed:@"grid_block_9"] forState:UIControlStateNormal];
    } else if (displayMode == ADXCollectionViewDisplayModeThumbnailSmall) {
        [self.displayModeToggleControl setImage:[UIImage imageNamed:@"grid_block_list"] forState:UIControlStateNormal];
    } else {
        [self.displayModeToggleControl setImage:[UIImage imageNamed:@"grid_block_4"] forState:UIControlStateNormal];
    }
}

#pragma mark - Helpers

- (NSString *)collectionTitle
{
    NSString *result = nil;
    
    if ([self.collection.type isEqualToString:ADXCollectionType.system]) {
        result = [[ADXAppDelegate sharedInstance] localizedStringForKey:self.collection.title];
    } else {
        result = self.collection.title;
    }
    
    return result;
}

- (void)showActivityHUD
{
    if (self.activityHUD) {
        [self hideActivityHUD];
    }
    
    self.activityHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
}

- (void)hideActivityHUD
{
    [self.activityHUD hide:YES];
}

#pragma mark - URL Scheme Handling

- (void)processPendingURLRequest
{
    ADXAppDelegate *appDelegate = [ADXAppDelegate sharedInstance];
    if (appDelegate.readerAppState != ADXAppState.viewingDocBrowser) {
        return;
    }
    
    // Process any pending URL scheme requests
    NSManagedObjectContext *moc = [ADXAppDelegate sharedInstance].managedObjectContext;
    ADXURLSchemeRequest *pendingRequest = [ADXAppDelegate sharedInstance].pendingURLSchemeRequest;
    
    if (pendingRequest != nil) {
        // See if we have the required document
        if (pendingRequest.requiredDocumentID) {
            NSString *accountID = [ADXAppDelegate sharedInstance].currentAccount.id;
            
            // We may not have the required version of the document, but see if we have any version of the document
            ADXDocument *document;
            
            if (pendingRequest.requiredDocumentVersion != nil) {
                // Fetch the requested version
                document = [ADXDocument documentWithID:pendingRequest.requiredDocumentID
                                               version:pendingRequest.requiredDocumentVersion
                                             accountID:accountID
                                               context:moc];
            } else {
                document = [ADXDocument latestDocumentWithID:pendingRequest.requiredDocumentID
                                                   accountID:accountID
                                                     context:moc];
            }
            
            if (document == nil || !document.downloadedValue) {
                // The document isn't ready yet
                if (!pendingRequest.updateAttempted) {
                    ADXV2Service *service = [ADXV2Service serviceForManagedObject:document];
                    [service refreshDocument:document.id
                                   versionID:pendingRequest.requiredDocumentVersion
                             downloadContent:YES
                               trackProgress:NO
                                  completion:nil];
                    pendingRequest.updateAttempted = YES;
                }
            
                return;
            }
            
            if (document != nil) {
                pendingRequest.delegate = self;
                [pendingRequest dispatch];
            }
        };
    }
}

- (void)urlHandlerOpenDocumentWithID:(NSString *)documentID
{
    NSString *accountID = [ADXAppDelegate sharedInstance].currentAccount.id;
    ADXDocument *document = [ADXDocument latestDocumentWithID:documentID accountID:accountID context:[ADXAppDelegate sharedInstance].managedObjectContext];
    if (document != nil) {
        [self openActionForDocument:document animated:NO];
    }
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    BOOL result = YES;
    if (textField == self.titleMessage) {
        result = NO;
    }
    return result;
}

#pragma mark - Title Area

- (void)toggleBottomStatusMessage
{    
    if (self.titleState == ADXCollectionViewNavbarShowTitleState) {
        [self startRotatingTitleMessage];
    } else {
        [self stopRotatingTitleMessage];
    }
}

- (void)stopRotatingTitleMessage
{
    self.stopTitleRotation = YES;
    self.titleRotationInProgress = NO;
    
    self.titleState = ADXCollectionViewNavbarShowTitleState;
    [self updateTitle];
}

- (void)startRotatingTitleMessage
{
    @synchronized(self) {
        if (self.titleRotationInProgress) {
            return;
        } else {
            self.titleRotationInProgress = YES;
        }
    }
    
    self.stopTitleRotation = NO;
    self.titleState = ADXCollectionViewNavbarShowTitleState;
    [self updateTitle];    
    [self rotateTitleMessage];
}

- (void)rotateTitleMessage
{
    if (self.stopTitleRotation) {
        return;
    }
    
    BOOL continueRotation = NO;
    
    switch (self.titleState) {
        case ADXCollectionViewNavbarShowTitleState:
            self.titleState = ADXCollectionViewNavbarShowCountState;
            [self updateTitle];
            continueRotation = YES;
            break;

        case ADXCollectionViewNavbarShowCountState:
            self.titleState = ADXCollectionViewNavbarShowMessageState;
            [self updateTitle];
            continueRotation = YES;
            break;

        case ADXCollectionViewNavbarShowMessageState:
            self.titleState = ADXCollectionViewNavbarShowTitleState;
            [self updateTitle];
            continueRotation = NO;
            break;

        default:
            break;
    }
    
    if (continueRotation) {
        __weak ADXCollectionViewController *weakSelf = self;
        int64_t delayInSeconds = 2.0;
        
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            [weakSelf rotateTitleMessage];
        });

    } else {
        self.titleRotationInProgress = NO;
    }
}

- (void)updateTitle
{
    NSString *message = nil;
    NSInteger numberOfDocuments = self.collectionController.numberOfItems;
    
    switch (self.titleState) {
        case ADXCollectionViewNavbarShowTitleState:
        {
            message = [self collectionTitle];
            break;
        }
            
        case ADXCollectionViewNavbarShowCountState:
        {
            if (numberOfDocuments == 0) {
                message = NSLocalizedString(@"Collection document count 0", nil);
            } else if (numberOfDocuments == 1) {
                message = [NSString stringWithFormat:NSLocalizedString(@"Collection document count 1", nil), numberOfDocuments];
            } else if (numberOfDocuments > 1) {
                message = [NSString stringWithFormat:NSLocalizedString(@"Collection document count many", nil), numberOfDocuments];
            }
            break;
        }
            
        case ADXCollectionViewNavbarShowMessageState:
        {
            ADXV2Service *service = self.collectionController.service;
            NSString *mostRecentUpdateMessage = [self.collectionController.collection.mostRecentRefresh stringFromDateCapitalized:NO];
            if (IsEmpty(mostRecentUpdateMessage)) {
                message = @"Most recent update information unavailable.";
            } else {
                if (service.networkReachable && service.serverReachable) {
                    message = [NSString stringWithFormat:@"Updated %@", mostRecentUpdateMessage];
                } else {
                    NSString *sinceMessage = @"";
                    if (!IsEmpty(mostRecentUpdateMessage)) {
                        sinceMessage = [NSString stringWithFormat:@"since %@", mostRecentUpdateMessage];
                    }
                    if (!service.networkReachable) {
                        message = [NSString stringWithFormat:@"Offline %@", sinceMessage];
                    } else {
                        message = [NSString stringWithFormat:@"Server unavailable %@", sinceMessage];
                    }
                }
            }
            break;
        }
            
        default:
            break;
    }
    
    self.titleMessage.text = message;
}

#pragma mark - Reachability

- (void)handleReachabilityChanged:(NSNotification *)notification
{
    LogInfo(@"Reachability changed -- network reachability: %@", (self.collectionController.service.networkReachable ? @"YES" : @"NO"));
    LogInfo(@"Reachability changed -- server reachability: %@", (self.collectionController.service.serverReachable ? @"YES" : @"NO"));
    
    __weak ADXCollectionViewController *weakSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        [weakSelf startRotatingTitleMessage];
    });
}

#pragma mark - UIPopover

- (BOOL)popoverControllerShouldDismissPopover:(UIPopoverController *)popoverController
{
    return YES;
}

- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController
{
    self.currentPopoverController = nil;
}

- (void)dismissPopovers
{
    if ([self.currentPopoverController isPopoverVisible]) {
        [self.currentPopoverController dismissPopoverAnimated:YES];
    }
    self.currentPopoverController = nil;
}

#pragma mark - Document Presentation

- (void)presentAnyPreviouslyOpenedDocument
{
    if (self.alreadyDidAutoOpenDocumentCheck)
        return;
    
    self.alreadyDidAutoOpenDocumentCheck = YES;

    NSManagedObjectContext *moc = [ADXAppDelegate sharedInstance].managedObjectContext;
    
    NSString *recentDocumentID = [[ADXUserDefaults sharedDefaults] currentlyOpenDocumentID];
    NSInteger recentDocumentVersion = [[ADXUserDefaults sharedDefaults] currentlyOpenDocumentVersion];
    NSInteger recentDocumentPage = [[ADXUserDefaults sharedDefaults] currentlyOpenDocumentPage] - 1;
    NSString *accountID = [ADXAppDelegate sharedInstance].currentAccount.id;
    
    if (!IsEmpty(recentDocumentID)) {
        // In the interest of keeping this as a synchronous operation, we'll skip the check for new notes
        // and open the document as it was when the user was last reading it.
        [moc performBlock:^{
            ADXDocument *documentToOpen = nil;
            documentToOpen = [ADXDocument documentWithID:recentDocumentID version:[NSNumber numberWithInteger:recentDocumentVersion] accountID:accountID context:moc];
            
            if (!documentToOpen) {
                [[ADXUserDefaults sharedDefaults] clearCurrentlyOpenDocument];
            } else if (documentToOpen.accessible) {
                ADXDocumentContent *content = documentToOpen.content;
                PSPDFDocument *pdfDocument = [PSPDFDocument PDFDocumentWithData:content.data];
                [self openPDFResource:pdfDocument onPage:recentDocumentPage fromDocument:documentToOpen animated:NO];
            }
        }];
    }
}

#pragma mark - Revoked/inacessible Documents
- (void)presentInaccessibleDocumentActionSheetForDocument:(ADXDocument *)document fromRectOfView:(UIView *)rectOfView inView:(UIView *)view{
    
    PSPDFActionSheet *actionSheet = [[PSPDFActionSheet alloc] initWithTitle:@"This document cannot be opened from your current location."];
    [actionSheet addButtonWithTitle:@"OK" block:^{
        //[weakSelf removeRevokedDocument:document];
    }];
    [actionSheet setCancelButtonWithTitle:@"Cancel" block:nil];
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        UIView *pageView = [rectOfView viewWithTag:kADXCollectionThumbnailCellPageTag];
        if (pageView != nil) {
            rectOfView = pageView;
        }
        
        [actionSheet showFromRect:rectOfView.bounds inView:rectOfView animated:YES];
    } else {
        [actionSheet showInView:view.window];
    }
}

- (void)presentRemoveRevokedDocumentActionSheetForDocument:(ADXDocument *)document fromRectOfView:(UIView *)rectOfView inView:(UIView *)view
{
    __weak ADXCollectionViewController *weakSelf = self;
    PSPDFActionSheet *actionSheet = [[PSPDFActionSheet alloc] initWithTitle:@"This document was revoked by the author, and cannot be opened."];
    [actionSheet setDestructiveButtonWithTitle:@"Remove" block:^{
        [weakSelf removeRevokedDocument:document];
    }];
    [actionSheet setCancelButtonWithTitle:@"Cancel" block:nil];
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        UIView *pageView = [rectOfView viewWithTag:kADXCollectionThumbnailCellPageTag];
        if (pageView != nil) {
            rectOfView = pageView;
        }
        
        [actionSheet showFromRect:rectOfView.bounds inView:rectOfView animated:YES];
    } else {
        [actionSheet showInView:view.window];
    }
}

- (void)removeRevokedDocument:(ADXDocument *)document
{
    NSManagedObjectContext *moc = [ADXAppDelegate sharedInstance].managedObjectContext;
    [moc performBlock:^{
        NSError *saveError = nil;

        [moc deleteObject:document];
        BOOL saveSuccess = [moc save:&saveError];
        if (!saveSuccess) {
            LogNSError(@"removeRevokedDocument unable to save context after delete", saveError);
        }
    }];
}

#pragma mark - ADXCollectionsViewController

- (void)openActionForDocument:(ADXDocument *)document animated:(BOOL)animated
{
    [self openActionForDocument:document onPage:0 animated:animated];
}

- (void)openActionForDocument:(ADXDocument *)document onPage:(NSInteger)page animated:(BOOL)animated
{
    [self openActionForDocument:document onPage:page fromRectOfView:nil inView:nil animated:animated];
}

- (void)openActionForDocument:(ADXDocument *)document onPage:(NSInteger)page fromRectOfView:(UIView *)rectOfView inView:(UIView *)view animated:(BOOL)animated
{
    __weak ADXCollectionViewController *weakSelf = self;
    if (document.accessible)
    {
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [self.collectionController.service refreshNotesForDocument:document.id completion:^(BOOL success, NSError *error) {
            if (!success) {
                LogNSError(@"refreshNotesForDocument failed", error);
            }
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                [[ADXAppDelegate sharedInstance] pdfResourceForDocument:document
                                                        downloadContent:YES
                                                                service:weakSelf.collectionController.service
                                                      downloadWillStart:^{
                                                          // Do nothing
                                                      } completion:^(id resource, BOOL success, NSError *error) {
                                                          dispatch_async(dispatch_get_main_queue(), ^{
                                                              [MBProgressHUD hideHUDForView:weakSelf.view animated:YES];
                                                              if (success && resource) {
                                                                  [weakSelf openPDFResource:resource onPage:page fromDocument:document animated:animated];
                                                              }
                                                          });
                                                      }];
            }];
        }];
    } else {
        if ([document.status isEqualToString:ADXDocumentStatus.inaccessible]) {
            [self presentInaccessibleDocumentActionSheetForDocument:document fromRectOfView:rectOfView inView:view];
        }
        else
            [self presentRemoveRevokedDocumentActionSheetForDocument:document fromRectOfView:rectOfView inView:view];
    }
}

- (void)openPDFResource:(PSPDFDocument *)pdfResource onPage:(NSInteger)page fromDocument:(ADXDocument *)document animated:(BOOL)animated
{
    // Update the document open date
    [[ADXAppDelegate sharedInstance] saveMainContext:nil];
    
    [self.collectionController.service emitEventDidOpenDocument:document.id versionID:document.version completion:nil];
    
    [[ADXAppDelegate sharedInstance] passCheckpoint:@"Opened a document from the collection view"];

    // Setup the view controller
    ADXPDFViewController *pdfController = [[ADXPDFViewController alloc] initWithDocument:document pdfDocument:pdfResource];
    pdfController.hidesBottomBarWhenPushed = YES;
    
    // FIXME -- When the user exits from reading a document, and returns to the received documents view, we should reset the currently open document ID to nil
    
    // Update the application state
    ADXAppDelegate *appDelegate = (ADXAppDelegate *)[[UIApplication sharedApplication] delegate];
    [appDelegate setReaderAppState:ADXAppState.viewingDocBrowserEnded];
    
    // If the user tapped on a document while we're waiting for the document requested through a URL scheme to
    // download, that cancels the URL scheme launch.
    appDelegate.pendingURLSchemeRequest = nil;
    
    [self.navigationController pushViewController:pdfController animated:animated];
    
    page = page ?: 0;
    [pdfController setPage:page animated:NO];
}

- (void)popoverActionForDocument:(ADXDocument *)document thumbnail:(UIImage *)thumbnail fromRectOfView:(UIView *)rectOfView inView:(UIView *)view onRefreshRequired:(ADXCollectionViewOnRefreshRequiredBlock)refreshBlock
{
    [self dismissPopovers];
    
    NSManagedObjectContext *moc = [ADXAppDelegate sharedInstance].managedObjectContext;
    __weak ADXCollectionViewController *weakSelf = self;
    
    [[ADXAppDelegate sharedInstance] passCheckpoint:@"Opened a document popover"];

    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        dispatch_async(dispatch_get_main_queue(), ^{
            weakSelf.currentPopoverController = [ADXDocVersionViewController popoverControllerWithDocument:document
                                                                                allowNavigatingCollections:YES
                                                                                          hideDeleteButton:NO
                                                                                      managedObjectContext:moc
                                                                                            actionDelegate:self];
            weakSelf.currentPopoverController.popoverBackgroundViewClass = [ADXBluePopoverBackgroundView class];
            weakSelf.currentPopoverController.delegate = self;
            weakSelf.longPressedDocument = document;
            weakSelf.popoverPresentFromRectOfView = rectOfView;
            weakSelf.popoverPresentInView = view;
            weakSelf.onRefreshRequiredBlock = refreshBlock;
            [weakSelf.currentPopoverController presentPopoverFromRect:weakSelf.popoverPresentFromRectOfView.frame
                                                               inView:weakSelf.popoverPresentInView
                                             permittedArrowDirections:(UIPopoverArrowDirectionLeft | UIPopoverArrowDirectionRight)
                                                             animated:YES];
        });
    } else {
        dispatch_async(dispatch_get_main_queue(), ^{
            weakSelf.currentPopoverController.delegate = nil;
            weakSelf.currentPopoverController = nil;
            weakSelf.popoverPresentFromRectOfView = nil;
            weakSelf.popoverPresentInView = nil;
            weakSelf.longPressedDocument = document;
            weakSelf.onRefreshRequiredBlock = refreshBlock;

            UIViewController *vc = [ADXDocVersionViewController modalControllerWithDocument:document
                                                                 allowNavigatingCollections:YES
                                                                           hideDeleteButton:NO
                                                                       managedObjectContext:moc
                                                                             actionDelegate:self];
            [weakSelf presentViewController:vc animated:YES completion:nil];
        });
    }
}

#pragma mark - ADXDocActionsDelegate

- (void)didSelectDocShareAction:(id)sender document:(ADXDocument *)document
{
    [self dismissPopovers];
    
    self.documentSharingController = [ADXDocSharingController controllerWithDocument:document mailDelegate:self];
    [self.documentSharingController presentShareDocumentViewFromViewController:self];
}

- (void)didSelectDocOpenAction:(id)sender document:(ADXDocument *)document
{
    [self dismissPopovers];

    __weak ADXCollectionViewController *weakSelf = self;
    ADXV2Service *service = self.collectionController.service;
    if (!service) {
        LogError(@"didSelectDocOpenAction could not retrieve handle to service");
        return;
    }

    if (document.downloaded) {
        [self openActionForDocument:document animated:YES];
    } else {
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];

        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
            [service downloadContentForDocument:document.id versionID:document.version trackProgress:NO completion:^(BOOL success, NSError *error) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [MBProgressHUD hideHUDForView:weakSelf.view animated:YES];
                    if (error) {
                        LogNSError(@"couldn't load highlighted document content", error);
                    } else {
                        [weakSelf openActionForDocument:document animated:YES];
                    }
                });
            }];
        });
    }
}

- (void)didSelectDocAnalyticsAction:(id)sender document:(ADXDocument *)document
{
    [[ADXAppDelegate sharedInstance] passCheckpoint:@"Tapped the Analytics button"];

    [self dismissPopovers];
    
    [ADXDocumentAnalyticsViewController presentAnalyticsForDocument:document parentViewController:self];
}

- (void)didSelectDocRemoveAction:(id)sender document:(ADXDocument *)document
{
    __weak ADXCollectionViewController *weakSelf = self;
    
    [[ADXAppDelegate sharedInstance] passCheckpoint:@"Opened document remove popover"];
    
    [self dismissPopovers];

    ADXV2Service *service = self.collectionController.service;
    if (!service) {
        LogError(@"didSelectDocOpenAction could not retrieve handle to service");
        return;
    }
    
    PSPDFActionSheet *actionSheet = [[PSPDFActionSheet alloc] initWithTitle:@"Remove Document"];
    if (!document.metaDocument.doNotAutomaticallyDownloadContentValue) {
        [actionSheet addButtonWithTitle:@"From Local Storage" block:^{
            [service excludeDocumentFromAutomaticContentDownload:document.id removeContentNow:YES completion:^(BOOL success, NSError *error) {
                [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                    if (weakSelf.onRefreshRequiredBlock) {
                        weakSelf.onRefreshRequiredBlock();
                    }
                }];
            }];
        }];
    }
    if ([self.collection.type isEqualToString:ADXCollectionType.user]) {
        [actionSheet addButtonWithTitle:@"From Collection" block:^{
            [[ADXAppDelegate sharedInstance] removeDocument:document.id fromCollection:weakSelf.collection.id localID:weakSelf.collection.localID completion:^(BOOL success, NSError *error) {
                // No need to call the refresh block
            }];
        }];
    }
    
    NSString *documentAuthorID = document.author.id;
    NSString *accountID = [ADXAppDelegate sharedInstance].currentAccount.id;
    if ([documentAuthorID isEqualToString:accountID]) {
        [actionSheet setDestructiveButtonWithTitle:@"From SavvyDox System" block:^{
            [weakSelf confirmDocumentDeletion:document];
        }];
    }
    
    [actionSheet setCancelButtonWithTitle:@"Cancel" block:nil];
    [actionSheet showFromRect:self.popoverPresentFromRectOfView.frame inView:self.popoverPresentInView animated:YES];
}

#pragma mark - Document Deletion

- (void)confirmDocumentDeletion:(ADXDocument *)document
{
    __weak ADXCollectionViewController *weakSelf = self;
    ADXV2Service *service = self.collectionController.service;

    if (service.networkReachable && service.serverReachable) {
        [self showActivityHUD];
        [UIAlertView displayAlertWithTitle:@"Remove Document from SavvyDox System"
                                   message:@"This will revoke the document from all recipients and completely remove the document from the system.\nThis action cannot be undone."
                           leftButtonTitle:@"Cancel"
                          leftButtonAction:^{
                              [weakSelf hideActivityHUD];
                          }
                          rightButtonTitle:@"Remove"
                         rightButtonAction:^{
                             ADXV2Service *service = weakSelf.collectionController.service;
                             [service deleteDocument:document.id completion:^(BOOL success, NSError *error) {
                                 [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                                     [weakSelf hideActivityHUD];
                                     if (success) {
                                         [weakSelf documentDeletionCompletedWithSuccess:document];
                                     } else {
                                         [weakSelf documentDeletionCompletedWithFailure:document];
                                     }
                                 }];
                             }];
                         }];
    } else {
        [UIAlertView displayAlertWithTitle:@"Cannot Remove Document"
                                   message:@"Document removal requires an online connection with the server.\nPlease connect to the server and retry."
                           leftButtonTitle:@"OK"
                          leftButtonAction:nil
                          rightButtonTitle:nil
                         rightButtonAction:nil];
    }
}

- (void)documentDeletionCompletedWithSuccess:(ADXDocument *)document
{
    [self.collectionController purgeRemovedDocument:document.id];
    [self refresh:NO];
}

- (void)documentDeletionCompletedWithFailure:(ADXDocument *)document
{
    [UIAlertView displayAlertWithTitle:@"Document Not Removed"
                               message:@"An error occurred when attempting to remove the document."
                       leftButtonTitle:@"OK"
                      leftButtonAction:nil
                      rightButtonTitle:nil
                     rightButtonAction:nil];
}

#pragma mark - ADXCollectionControllerDelegate

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    self.collectionSectionChanges = [NSMutableArray new];
    self.collectionItemChanges = [NSMutableArray new];
}

- (void)controller:(NSFetchedResultsController *)controller
  didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex
     forChangeType:(NSFetchedResultsChangeType)type
{
    NSMutableDictionary *sectionChange = [NSMutableDictionary new];
    
    switch(type) {
        case NSFetchedResultsChangeInsert:
            sectionChange[@(type)] = @[@(sectionIndex)];
            break;
        case NSFetchedResultsChangeDelete:
            sectionChange[@(type)] = @[@(sectionIndex)];
            break;
    }
    
    [self.collectionSectionChanges addObject:sectionChange];
}

- (void)controller:(NSFetchedResultsController *)controller
   didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath
     forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath
{
    NSMutableDictionary *itemChange = [NSMutableDictionary new];
    
    switch(type) {
        case NSFetchedResultsChangeInsert:
            itemChange[@(type)] = newIndexPath;
            break;
        case NSFetchedResultsChangeDelete:
            itemChange[@(type)] = indexPath;
            break;
        case NSFetchedResultsChangeUpdate:
            itemChange[@(type)] = indexPath;
            break;
        case NSFetchedResultsChangeMove:
            itemChange[@(type)] = @[indexPath, newIndexPath];
            break;
    }
    [self.collectionItemChanges addObject:itemChange];
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    __weak ADXCollectionViewController *weakSelf = self;
    
    NSAssert([[NSThread currentThread] isMainThread], @"Should be on main thread");
    
    if ([self.collectionSectionChanges count]) {
        [self.collectionView performBatchUpdates:^{
            for (NSDictionary *change in weakSelf.collectionSectionChanges) {
                [change enumerateKeysAndObjectsUsingBlock:^(NSNumber *key, id obj, BOOL *stop) {
                    NSFetchedResultsChangeType type = [key unsignedIntegerValue];
                    
                    switch (type) {
                        case NSFetchedResultsChangeInsert:
                            [weakSelf.collectionView insertSections:[NSIndexSet indexSetWithIndex:[obj unsignedIntegerValue]]];
                            break;
                        case NSFetchedResultsChangeDelete:
                            [weakSelf.collectionView deleteSections:[NSIndexSet indexSetWithIndex:[obj unsignedIntegerValue]]];
                            break;
                        case NSFetchedResultsChangeUpdate:
                            [weakSelf.collectionView reloadSections:[NSIndexSet indexSetWithIndex:[obj unsignedIntegerValue]]];
                            break;
                    }
                }];
            }
        } completion:^(BOOL finished){
            [self checkForLocationSensitiveDocuments];
        }];
    }
    
    if ([self.collectionItemChanges count] > 0 && [self.collectionSectionChanges count] == 0) {
        [self.collectionView performBatchUpdates:^{
            for (NSDictionary *change in weakSelf.collectionItemChanges) {
                [change enumerateKeysAndObjectsUsingBlock:^(NSNumber *key, id obj, BOOL *stop) {
                    NSFetchedResultsChangeType type = [key unsignedIntegerValue];
                    
                    switch (type) {
                        case NSFetchedResultsChangeInsert:
                            [weakSelf.collectionView insertItemsAtIndexPaths:@[obj]];
                            break;
                        case NSFetchedResultsChangeDelete:
                            [weakSelf.collectionView deleteItemsAtIndexPaths:@[obj]];
                            break;
                        case NSFetchedResultsChangeUpdate:
                            [weakSelf.collectionView reloadItemsAtIndexPaths:@[obj]];
                            break;
                        case NSFetchedResultsChangeMove:
                            [weakSelf.collectionView moveItemAtIndexPath:obj[0] toIndexPath:obj[1]];
                            break;
                    }
                }];
            }
        } completion:nil];
    }
    
    [self.collectionSectionChanges removeAllObjects], self.collectionSectionChanges = nil;
    [self.collectionItemChanges removeAllObjects], self.collectionItemChanges = nil;
}

#pragma mark - ADXCollectionHeaderViewDelegate

- (void)collectionHeaderView:(ADXCollectionHeaderView *)view didSelectSortOrder:(ADXCollectionControllerSort)order
{
    self.collectionController.sortFilter = order;
    [[ADXUserDefaults sharedDefaults] setDocumentSortOrder:order];
    [self.collectionView reloadData];
}

#pragma mark - Document Popover

- (void)presentPopoverForCellAtIndexPath:(NSIndexPath *)indexPath
{
    __weak ADXCollectionViewController *weakSelf = self;
    ADXDocument *document = [self.collectionController objectAtIndexPath:indexPath];
    
    if (document.accessible) {
        ADXCollectionViewCell *cell = (ADXCollectionViewCell *)[self.collectionView cellForItemAtIndexPath:indexPath];
        [self popoverActionForDocument:cell.documentEntity thumbnail:cell.thumbnailImage fromRectOfView:cell inView:self.collectionView onRefreshRequired:^{
            [weakSelf.collectionView reloadData];
        }];
    }
}

#pragma mark - ADXCollectionDocumentsActionsDelegate

- (void)didRemoveDocument:(ADXDocument *)document fromCollection:(ADXCollection *)collection
{
    [[ADXAppDelegate sharedInstance] removeDocument:document.id fromCollection:collection.id localID:collection.localID completion:nil];
    if (self.collection == collection) {
        [self dismissPopovers];
    }
}

- (void)didAddDocument:(ADXDocument *)document toCollection:(ADXCollection *)collection
{
    [[ADXAppDelegate sharedInstance] addDocument:document.id toCollection:collection.id localID:collection.localID completion:nil];
}

#pragma mark - MFMailComposeViewControllerDelegate

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    [controller dismissViewControllerAnimated:YES completion:nil];
    self.documentSharingController = nil;
}

#pragma mark - ADXNotificationsActionsDelegate

- (void)didSelectDocument:(ADXDocument *)document fromEvent:(ADXEvent *)event
{
    [self openActionForDocument:document onPage:0 fromRectOfView:nil inView:nil animated:YES];
}

#pragma mark - ADXCollectionDocumentsActionsDelegate

- (void)didSelectDocument:(ADXDocument *)document
{
    [[ADXAppDelegate sharedInstance] addDocument:document.id toCollection:self.collection.id localID:self.collection.localID completion:nil];
}

#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"Location Manager didFailWithError: %@", error);
    UIAlertView *errorAlert = [[UIAlertView alloc]
                               initWithTitle:@"Error" message:@"Failed to Get Your Location" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [errorAlert show];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
   if ([locations count]>0) {
        location = [locations lastObject];
    }
    
    [self checkForLocationSensitiveDocuments];
}

#pragma mark - Actions

- (void)handleLongPressGesture:(UILongPressGestureRecognizer *)longPressGesture
{
    if (longPressGesture.state != UIGestureRecognizerStateBegan)
        return;
    
    CGPoint location = [longPressGesture locationInView:self.collectionView];
    NSIndexPath *indexPath = [self.collectionView indexPathForItemAtPoint:location];
    [self presentPopoverForCellAtIndexPath:indexPath];
}

- (void)didTapAddDocumentButton:(id)sender
{
    if (self.currentPopoverController && self.currentPopoverController == self.addDocumentsPopoverController) {
        [self dismissPopovers];
    } else {
        [self dismissPopovers];
        UIPopoverController *popover = [ADXCollectionDocumentsViewController popoverControllerWithTitle:@"Add Documents"
                                                                                             collection:self.collection
                                                                                           allowEditing:NO
                                                                                        startInEditMode:NO
                                                                                       allowMultiSelect:NO
                                                                                   allowEditMultiSelect:NO
                                                                               showOnlyMissingDocuments:YES
                                                                                         actionDelegate:self];
        self.currentPopoverController = popover;
        self.currentPopoverController.delegate = self;
        self.addDocumentsPopoverController = popover;
        self.popoverPresentFromBarButtonItem = self.addButtonItem;
        [popover presentPopoverFromBarButtonItem:self.addButtonItem permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
    }
}

- (IBAction)didTapRefreshButton:(id)sender
{
    [[ADXAppDelegate sharedInstance] passCheckpoint:@"Pressed the Refresh button (from a collection view)"];

    [self dismissPopovers];
    [self refresh:NO];
}

- (IBAction)didTapBottomStatusMessage:(id)sender
{
    [[ADXAppDelegate sharedInstance] passCheckpoint:@"Tapped the status message"];

    [self dismissPopovers];
    [self toggleBottomStatusMessage];
}

- (IBAction)didTapNotificationsButton:(id)sender
{
    [[ADXAppDelegate sharedInstance] passCheckpoint:@"Tapped the notifications button"];

    if ([self.currentPopoverController isPopoverVisible]) {
        [self dismissPopovers];
    } else {
        NSManagedObjectContext *moc = [ADXAppDelegate sharedInstance].managedObjectContext;
        NSString *accountID = [ADXAppDelegate sharedInstance].currentAccount.id;
        
        self.currentPopoverController = [ADXNotificationsViewController popoverControllerWithAccount:accountID actionDelegate:self options:nil managedObjectContext:moc];
        self.currentPopoverController.delegate = self;
        [self.currentPopoverController presentPopoverFromBarButtonItem:self.notificationsBarButton permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
    }
}

- (IBAction)didTapDisplayModeToggleButton:(id)sender
{
    [self dismissPopovers];

    if (self.displayMode == ADXCollectionViewDisplayModeThumbnail) {
        [[ADXAppDelegate sharedInstance] passCheckpoint:@"Switched a collection view to small thumbnail mode"];
        self.displayMode = ADXCollectionViewDisplayModeThumbnailSmall;
    } else if (self.displayMode == ADXCollectionViewDisplayModeThumbnailSmall) {
        [[ADXAppDelegate sharedInstance] passCheckpoint:@"Switched a collection view to list mode"];
        self.displayMode = ADXCollectionViewDisplayModeList;
    } else {
        [[ADXAppDelegate sharedInstance] passCheckpoint:@"Switched a collection view to standard thumbnail mode"];
        self.displayMode = ADXCollectionViewDisplayModeThumbnail;
    }
    
    [self resetImageCache];
    [self.collectionView reloadData];
}

- (IBAction)didLongPressRefreshButton:(id)sender
{
    [[ADXAppDelegate sharedInstance] passCheckpoint:@"Long-pressed the Refresh button (from a collection view)"];

    [self dismissPopovers];
    [self refresh:YES];
}

@end
