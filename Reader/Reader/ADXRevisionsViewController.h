//
//  ADXRevisionsViewController.h
//  Reader
//
//  Created by Matteo Ugolini on 12-08-03.
//  Copyright (c) 2012 ActivDox Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ADXAppCommonMacros.h"
#import "ADXPDFViewController.h"


@interface ADXRevisionsViewController : UIViewController <UICollectionViewDataSource, UICollectionViewDelegate, ADXPDFToolsDelegate, ADXPDFStripsViewControllerDataSource>

- (id)initWithDocument:(PSPDFDocument *)document pdfController:(ADXPDFViewController *)pdfController;

@end
