//
//  ADXNoteAnnotation.h
//  Reader
//
//  Created by Matteo Ugolini on 2012-09-25.
//  Copyright (c) 2012 ActivDox Inc. All rights reserved.
//

#import "PSPDFNoteAnnotation.h"
#import "ADXAnnotationUtil.h"
#import "ActivDoxCore.h"

@class ADXHighlightAnnotation;

@interface ADXNoteAnnotation : PSPDFNoteAnnotation <ADXAnnotationProtocol>
@property (strong, nonatomic) ADXHighlightAnnotation *highlightAnnotation;
@property (strong, nonatomic) NSString *noteLocalID;
@property (strong, nonatomic) NSString *noteID;
@property (strong, nonatomic) NSString *documentID;
@property (strong, nonatomic) NSNumber *versionID;
@property (strong, nonatomic) NSString *accountID;
@property (strong, nonatomic) NSString *noteVisibility;
@property (strong, nonatomic) UIColor *avatarColor;
+ (ADXNoteAnnotation *)noteAnnotation:(CGRect)boundingBox page:(NSUInteger)page note:(ADXDocumentNote *)note document:(ADXDocument *)document pdf:(PSPDFDocument *)pdf;
+ (ADXNoteAnnotation *)noteAnnotationOverHighlight:(ADXHighlightAnnotation *)highlight note:(ADXDocumentNote *)note document:(ADXDocument *)document pdf:(PSPDFDocument *)pdf;

+ (void)updateNoteAnnotation:(ADXNoteAnnotation *)noteAnnotation
            withDocumentNote:(ADXDocumentNote *)documentNote
                 boundingBox:(CGRect)boundingBox
                    document:(ADXDocument *)document
                         pdf:(PSPDFDocument *)pdf
                        page:(NSUInteger)page;

@end
