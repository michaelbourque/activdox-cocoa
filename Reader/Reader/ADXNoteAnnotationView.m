//
//  ADXNoteAnnotationView.m
//  Reader
//
//  Created by Steve Tibbett on 2013-04-12.
//  Copyright (c) 2013 ActivDox Inc. All rights reserved.
//

#import "ADXNoteAnnotationView.h"
#import "PSPDFAnnotation.h"
#import "PSPDFNoteAnnotation.h"
#import <QuartzCore/QuartzCore.h>
#import "ADXNoteAnnotation.h"

@implementation ADXNoteAnnotationView

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UIView

- (void)drawRect:(CGRect)rect {
    
	if ([self.annotation isKindOfClass:[ADXNoteAnnotation class]]) {
        ADXNoteAnnotation *adxAnnotation = (ADXNoteAnnotation *)self.annotation;

        self.clipsToBounds = NO;
        self.layer.masksToBounds = NO;
        self.layer.shadowOpacity = 0.7;
        self.layer.shadowRadius = 2.0;
        self.layer.shadowColor = [UIColor blackColor].CGColor;
        self.layer.shadowOffset = CGSizeMake(0.0, 1.0);
        
		CGContextRef c = UIGraphicsGetCurrentContext();
		CGContextConcatCTM(c, CGAffineTransformMakeScale(1, -1));
		CGContextTranslateCTM(c, 0, -self.bounds.size.height);
		
		UIImage *iconImage = [UIImage imageNamed:[NSString stringWithFormat:@"note"]];
		CGContextSaveGState(c);
		
		CGContextDrawImage(c, self.bounds, [iconImage CGImage]);
        [self.annotation.color set];
		
		CGRect maskRect = CGRectMake(0, 0, self.bounds.size.width, self.bounds.size.height);
		CGContextClipToMask(c, maskRect, [iconImage CGImage]);
		
		CGContextSetBlendMode(c, kCGBlendModeSourceAtop);
		CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
		size_t num_locations = 2;
		CGFloat locations[2] = { 0.0, 1.0 };
        
        const float *color = CGColorGetComponents(self.annotation.color.CGColor);
        CGFloat components[8] = {
            color[0], color[1], color[2], 1.0,
            color[0] + 0.2, color[1] + 0.2, color[2] + 0.2, 1.0};
        
		CGGradientRef gradient = CGGradientCreateWithColorComponents(colorSpace, components, locations, num_locations);
		CGContextDrawLinearGradient(c, gradient, CGPointMake(0, 0), CGPointMake(0, CGRectGetMaxY(self.bounds)), 0);
        CGGradientRelease(gradient);
		
        CGColorSpaceRelease(colorSpace);
		CGContextRestoreGState(c);

        // Now overlay the user image, with a colour
        UIColor *avatarColor = adxAnnotation.avatarColor;
        UIImage *avatar = [UIImage imageNamed:@"user"];
		CGContextSetBlendMode(c, kCGBlendModeSourceAtop);

        UIGraphicsBeginImageContextWithOptions(self.bounds.size, NO, avatar.scale);
        [avatar drawAtPoint:CGPointMake(0, 0)];
        CGContextRef avatarContext = UIGraphicsGetCurrentContext();
        CGContextSetFillColorWithColor(avatarContext, avatarColor.CGColor);
        CGContextSetBlendMode(avatarContext, kCGBlendModeSourceAtop);
        CGContextFillRect(avatarContext, self.bounds);
        UIImage *avatarColored = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
		CGContextDrawImage(c, CGRectMake(0, 0, avatarColored.size.width, avatarColored.size.height), avatarColored.CGImage);
	}
}

@end
