//
//  ADXPDFViewController.h
//  Reader
//
//  Created by matteo ugolini on 2012-07-31.
//  Copyright (c) 2012 ActivDox Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ADXAppCommonMacros.h"

#import "PSPDFKit.h"
#import "ADXDocActionsDelegate.h"
#import "ADXToolbarController.h"
#import "ActivDoxCore.h"
#import "ADXPDFStripsViewController.h"
#import "ADXCollectionDocumentsViewController.h"

extern NSString * const kADXPDFViewControllerDidShowPage;
extern NSString * const kADXPDFViewControllerDidShowPageUserInfoPageKey;

@class ADXDocument;
@class ADXPDFViewController;

@protocol ADXPDFViewControllerActionDelegate <NSObject>
- (void)pdfViewController:(ADXPDFViewController *)controller willFinishWithDocument:(ADXDocument *)document;
@end

@protocol ADXPDFToolsDelegate;

@interface ADXPDFViewController : PSPDFViewController <PSPDFViewControllerDelegate,
                                                            ADXDocActionsDelegate,
                                                            ADXCollectionDocumentsActionsDelegate,
                                                            ADXToolbarControllerDelegate,
                                                            UIPopoverControllerDelegate,
                                                            ADXPDFStripsViewControllerDelegate>

@property (assign, nonatomic, getter=areAnnotationsVisible) BOOL annotationsVisible;
@property (strong, nonatomic, readonly) ADXDocument *adxDocument;
@property (strong, nonatomic, readonly) ADXV2Service *service;
@property (strong, nonatomic) ADXToolbarController *toolbarController;
@property (weak, nonatomic) id<ADXPDFViewControllerActionDelegate>actionDelegate;

- (id) initWithDocument:(ADXDocument *)adxDocument pdfDocument:(PSPDFDocument *)pdfDocument;
- (void)addToolsDelegate:(id<ADXPDFToolsDelegate>)delegate;
- (void)removeToolsDelegate:(id<ADXPDFToolsDelegate>)delegate;
- (void)dismissPopovers;

- (void)showThumbnails:(id)sender;
- (void)hideThumbnails:(id)sender;

- (void)togglePDFStripsWithDataSource:(id<ADXPDFStripsViewControllerDataSource>)dataSource;
- (void)refreshPDFStrips;
- (void)hidePDFStrips;
- (BOOL)isPDFStripsVisible;

- (void)refreshAnnotations;
@end

@protocol ADXPDFToolsDelegate <NSObject>
@required
- (void) documentDidChanged:(PSPDFDocument*)document;

@optional
- (NSArray*)menuItemsForSelectedText:(NSString *)selectedText atSuggestedTargetRect:(CGRect)rect
                              inRect:(CGRect)textRect onPageView:(PSPDFPageView *)pageView;
- (void)pdfController:(ADXPDFViewController *)controller selectedToolDidChanged:(UIViewController*)tool;
- (void)pdfController:(ADXPDFViewController *)pdfController didShowPageView:(PSPDFPageView *)pageView;
- (void)pdfController:(ADXPDFViewController *)pdfController didLoadPageView:(PSPDFPageView *)pageView;
- (BOOL)pdfController:(ADXPDFViewController *)pdfController didLongPressOnPageView:(PSPDFPageView *)pageView atPoint:(CGPoint)viewPoint gestureRecognizer:(UILongPressGestureRecognizer *)gestureRecognizer;
- (BOOL)pdfController:(ADXPDFViewController *)pdfController didTapOnPageView:(PSPDFPageView *)pageView atPoint:(CGPoint)viewPoint;
- (BOOL)pdfController:(ADXPDFViewController *)pdfController shouldScrollToPage:(NSUInteger)page;
- (void)pdfController:(ADXPDFViewController *)pdfController didRenderPageView:(PSPDFPageView *)pageView;
- (void)pdfController:(ADXPDFViewController *)pdfController didEndPageZooming:(UIScrollView *)scrollView atScale:(CGFloat)scale;
- (void)pdfController:(ADXPDFViewController *)pdfController didEndPageDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset;
- (BOOL)pdfController:(ADXPDFViewController *)pdfController shouldSelectAnnotation:(PSPDFAnnotation *)annotation onPageView:(PSPDFPageView *)pageView;
- (void)pdfController:(ADXPDFViewController *)pdfController didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation;
- (void)pdfController:(ADXPDFViewController *)pdfController willRotateToInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation;
- (void)pdfController:(ADXPDFViewController *)pdfController didChangeViewMode:(PSPDFViewMode)viewMode;
@end


