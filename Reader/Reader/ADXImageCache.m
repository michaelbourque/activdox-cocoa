//
//  ADXImageCache.m
//  Reader
//
//  Created by Steve Tibbett on 2013-02-07.
//  Copyright (c) 2013 ActivDox Inc. All rights reserved.
//

#import "ADXImageCache.h"

// Number of images to cache (arbitrary)
#define ADXImageCacheCountLimit 100

@interface ADXImageCache()
@property (strong, nonatomic) NSCache *cache;
@property (strong, nonatomic) NSMutableSet *currentlyResizing;
@end

@implementation ADXImageCache

- (id)initWithImageSize:(CGSize)imageSize
{
    self = [super init];
    if (self) {
        [self resetCacheWithImageSize:imageSize];
    }
    return self;
}

- (void)resetCacheWithImageSize:(CGSize)imageSize
{
    self.cache = [[NSCache alloc] init];
    self.cache.countLimit = ADXImageCacheCountLimit;
    
    float scale = [[UIScreen mainScreen] scale];
    self.imageSize = CGSizeMake(imageSize.width * scale, imageSize.height * scale);
    
    self.currentlyResizing = [NSMutableSet set];
}

- (void)setImageSize:(CGSize)imageSize
{
    _imageSize = imageSize;
    [self.cache removeAllObjects];
}

- (UIImage *)imageForKey:(NSString *)key
{
    return (UIImage *)[self.cache objectForKey:key];
}

// Resizing code from http://stackoverflow.com/questions/1282830/uiimagepickercontroller-uiimage-memory-and-more
- (UIImage *)imageWithImage:(UIImage *)sourceImage scaledToSizeWithSameAspectRatio:(CGSize)targetSize;
{
    CGSize imageSize = sourceImage.size;
    CGFloat width = imageSize.width;
    CGFloat height = imageSize.height;
    CGFloat targetWidth = targetSize.width;
    CGFloat targetHeight = targetSize.height;
    CGFloat scaleFactor = 0.0;
    CGFloat scaledWidth = targetWidth;
    CGFloat scaledHeight = targetHeight;
    CGPoint thumbnailPoint = CGPointMake(0.0, 0.0);
    
    if (CGSizeEqualToSize(imageSize, targetSize) == NO) {
        CGFloat widthFactor = targetWidth / width;
        CGFloat heightFactor = targetHeight / height;

        if (heightFactor * width > targetWidth) {
            scaleFactor = widthFactor; // scale to fit height
        }
        else {
            scaleFactor = heightFactor; // scale to fit width
        }
        
        scaledWidth  = floor(width * scaleFactor);
        scaledHeight = floor(height * scaleFactor);
    }
    
    CGImageRef imageRef = [sourceImage CGImage];
    CGBitmapInfo bitmapInfo = CGImageGetBitmapInfo(imageRef);
    CGColorSpaceRef colorSpaceInfo = CGImageGetColorSpace(imageRef);
    
    if (bitmapInfo == kCGImageAlphaNone) {
        bitmapInfo = kCGImageAlphaNoneSkipLast;
    }
    
    CGContextRef bitmap;
    
    if (sourceImage.imageOrientation == UIImageOrientationUp || sourceImage.imageOrientation == UIImageOrientationDown) {
        bitmap = CGBitmapContextCreate(NULL, scaledWidth, scaledHeight, CGImageGetBitsPerComponent(imageRef), CGImageGetBytesPerRow(imageRef), colorSpaceInfo, bitmapInfo);
        
    } else {
        bitmap = CGBitmapContextCreate(NULL, scaledHeight, scaledWidth, CGImageGetBitsPerComponent(imageRef), CGImageGetBytesPerRow(imageRef), colorSpaceInfo, bitmapInfo);
        
    }
    
    // In the right or left cases, we need to switch scaledWidth and scaledHeight,
    // and also the thumbnail point
    if (sourceImage.imageOrientation == UIImageOrientationLeft) {
        thumbnailPoint = CGPointMake(thumbnailPoint.y, thumbnailPoint.x);
        CGFloat oldScaledWidth = scaledWidth;
        scaledWidth = scaledHeight;
        scaledHeight = oldScaledWidth;
        
        CGContextRotateCTM (bitmap, M_PI_2); // + 90 degrees
        CGContextTranslateCTM (bitmap, 0, -targetHeight);
        
    } else if (sourceImage.imageOrientation == UIImageOrientationRight) {
        thumbnailPoint = CGPointMake(thumbnailPoint.y, thumbnailPoint.x);
        CGFloat oldScaledWidth = scaledWidth;
        scaledWidth = scaledHeight;
        scaledHeight = oldScaledWidth;
        
        CGContextRotateCTM (bitmap, -M_PI_2); // - 90 degrees
        CGContextTranslateCTM (bitmap, -targetWidth, 0);
        
    } else if (sourceImage.imageOrientation == UIImageOrientationUp) {
        // NOTHING
    } else if (sourceImage.imageOrientation == UIImageOrientationDown) {
        CGContextTranslateCTM (bitmap, targetWidth, targetHeight);
        CGContextRotateCTM (bitmap, -M_PI); // - 180 degrees
    }
    
    CGContextSetInterpolationQuality(bitmap, kCGInterpolationMedium);
    CGContextDrawImage(bitmap, CGRectMake(thumbnailPoint.x, thumbnailPoint.y, scaledWidth, scaledHeight), imageRef);
    CGImageRef ref = CGBitmapContextCreateImage(bitmap);
    UIImage* newImage = [UIImage imageWithCGImage:ref];
    
    CGContextRelease(bitmap);
    CGImageRelease(ref);
    
    return newImage; 
}

- (void)addImage:(UIImage *)image forKey:(NSString *)key
{
    if (image == nil) {
        [self.cache removeObjectForKey:key];
    } else {
        // Should we resize the image?
        if (image.size.width > self.imageSize.width || image.size.height > self.imageSize.height) {
            // Yes .. see if another thread is already resizing this image; if so, skip it
            BOOL alreadyResizing = NO;
            @synchronized(self) {
                if (![self.currentlyResizing containsObject:key]) {
                    [self.currentlyResizing addObject:key];
                } else {
                    alreadyResizing = YES;
                }
            }
            if (!alreadyResizing) {
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                    
                    UIImage *resizedImage = [self imageWithImage:image scaledToSizeWithSameAspectRatio:self.imageSize];
                    [self.cache setObject:resizedImage forKey:key];
                    
                    @synchronized(self) {
                        [self.currentlyResizing removeObject:key];
                    }
                });
            }
        } else {
            // Just store it
            [self.cache setObject:image forKey:key];
        }
    }
}


@end
