//
//  ADXToolbar.m
//  bottomBar
//
//  Created by matteo ugolini on 2012-10-13.
//  Copyright (c) 2012 matteo ugolini. All rights reserved.
//

#import "ADXToolbar.h"

@protocol ADXToolbarDelegate;

@interface ADXToolbar ()

@property (nonatomic) BOOL delegateShouldChangeStatus;
@property (nonatomic) BOOL delegateDidSelectedStatus;
@property (nonatomic) BOOL delegateDidGoToStatus;

@property (nonatomic) NSMutableDictionary *itemsForStatus;

@property (nonatomic) UISegmentedControl  *statusesControl;
@property (nonatomic) UIBarButtonItem     *statusesControlBarButton;
@property (nonatomic) UIBarButtonItem     *iphoneButtonSpace;

@property (nonatomic) UIButton      *slideBarOpener;
@property (nonatomic) UIToolbar     *slideBar;
@property (nonatomic) UIToolbar     *toolbar;

@property (nonatomic) BOOL          slideBarVisible;

@end

@implementation ADXToolbar

#pragma mark -
#pragma mark private

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        _itemsForStatus = [NSMutableDictionary dictionary];
        _statusesControl = [[UISegmentedControl alloc] initWithItems:@[]];
        _statusesControl.segmentedControlStyle = UISegmentedControlStyleBar;
        _currentStatus = NSNotFound;
    }
    return self;
}

- (void)awakeFromNib {
    [self initToolbarForViewInFrame:CGRectMake(0, 0, self.superview.frame.size.width, 44)];
}

- (void)initToolbarForViewInFrame:(CGRect)frame
{
    self.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    
    _toolbar = [[UIToolbar alloc] initWithFrame:frame];
    _toolbar.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    [self addSubview:_toolbar];
    
    [_statusesControl addTarget:self action:@selector(segmentedControlValueChanged:) forControlEvents:UIControlEventValueChanged];
    
    _statusesControlBarButton = [[UIBarButtonItem alloc] initWithCustomView:self.statusesControl];
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        _iphoneButtonSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:NULL];
        _iphoneButtonSpace.width = 32;
        
        _slideBarOpener = [UIButton buttonWithType:UIButtonTypeCustom];
        _slideBarOpener.frame = CGRectMake(0, 1, 32, 43);
        [_slideBarOpener addTarget:self action:@selector(toggleToolbar) forControlEvents:UIControlEventTouchUpInside];
        
        _slideBar = [[UIToolbar alloc] initWithFrame:CGRectMake(-frame.size.width, 0, frame.size.width, 44)];
        _slideBar.tintColor = [UIColor lightGrayColor];
        _slideBar.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        [_slideBar setBackgroundImage:nil forToolbarPosition:UIToolbarPositionAny barMetrics:UIBarMetricsDefault];
        
        [self addSubview:_slideBar];
        [self addSubview:_slideBarOpener];
        
        [self setSlideBarVisible:NO animated:YES];
    
        [_slideBar setItems:@[_iphoneButtonSpace,_statusesControlBarButton] animated:NO];
    }
 
    [self refreshView];
}

- (void) refreshView
{
    [self setItems:[self.itemsForStatus objectForKey:@(self.currentStatus)]];
    
    // Set the selected index in the segmented control asynchronously, because, for some reason,
    // just setting it here doesn't work.
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        self.statusesControl.selectedSegmentIndex = self.currentStatus;
    }];
}

#pragma mark -
#pragma mark accessor

- (NSUInteger)statusCount
{
    return self.statusesControl.numberOfSegments;
}

- (void)setToolbarDelegate:(id<ADXToolbarDelegate>)delegate
{
    _toolbarDelegate = delegate;
    
    if (!delegate) {
        _delegateShouldChangeStatus = _delegateDidSelectedStatus = _delegateDidGoToStatus = NO;
    } else {
        _delegateShouldChangeStatus = [delegate respondsToSelector:@selector(toolbarControl:shouldChangeStatus:)];
        _delegateDidSelectedStatus =  [delegate respondsToSelector:@selector(toolbarControl:didSelectedStatus:)];
        _delegateDidGoToStatus =  [delegate respondsToSelector:@selector(toolbarControl:didGoToStatus:fromStatus:)];
    }
}

- (void)updateSegmentedControl
{
    if (_currentStatus == NSNotFound && self.statusCount > 0) {
        self.currentStatus = 0;
    }
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        [self bringSubviewToFront:self.slideBarOpener];
    }
   
    NSUInteger width = 0;
    NSString *title;
    UIImage *image;
    
    for (int index=0; index<self.statusCount; index++)
    {
        title = [self.statusesControl titleForSegmentAtIndex:index];
        image = [self.statusesControl imageForSegmentAtIndex:index];
        
        if (title) {
            width += [title sizeWithFont:[UIFont systemFontOfSize:[UIFont labelFontSize]]].width;
        } else if (image) {
            width += image.size.width + 20;
        }
        
        width += 10;
    }
    
    self.statusesControlBarButton.width = width;
}

- (void)setCurrentStatus:(NSUInteger)newStatus
{
    NSUInteger oldStatus = _currentStatus;
    if ( newStatus < self.statusCount && self.delegateShouldChangeStatus && [self.toolbarDelegate toolbarControl:self shouldChangeStatus:newStatus])
    {
        _currentStatus = newStatus;
        self.statusesControl.selectedSegmentIndex = newStatus;
        
        NSArray *items = [self.toolbar.items copy];
        
        if(self.delegateDidGoToStatus)
        {
            [self.toolbarDelegate toolbarControl:self didGoToStatus:_currentStatus fromStatus:oldStatus];
        }
        
        //if the items remains equals meens
        if ([items isEqualToArray:self.toolbar.items]) {
            [self refreshView];
        }
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
        {
            [self.slideBarOpener setImage:[self.statusesControl imageForSegmentAtIndex:_currentStatus] forState:UIControlStateNormal];
        }
    }
}

- (void)setItems:(NSArray*)items
{
    [self setItems:items animated:NO];
}

- (void)setItems:(NSArray*)items animated:(BOOL)animated
{
    NSMutableArray *newItems = [NSMutableArray array];
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        [newItems addObject:self.iphoneButtonSpace];
    }
    else
    {
        if (self.statusesControlBarButton) {
            [newItems addObject:self.statusesControlBarButton];
        }
    }

    if (items) {
        [newItems addObjectsFromArray:items];
        [self.itemsForStatus setObject:items forKey:@(self.currentStatus)];
    } else {
        [self.itemsForStatus removeObjectForKey:@(self.currentStatus)];
    }
    
    [self.toolbar setItems:newItems animated:animated];
}

- (void)setItems:(NSArray*)items forStatus:(NSUInteger)status animated:(BOOL)animated
{
    if (status == self.currentStatus) {
        [self setItems:items animated:animated];
    } else if (items) {
        [self.itemsForStatus setObject:items forKey:@(status)];
    }
}

#pragma mark -
#pragma mark UISegmentedControl decorator.

- (void)setImage:(UIImage*)image forStatus:(NSUInteger)status
{
    [self.statusesControl setImage:image forSegmentAtIndex:status];
    [self updateSegmentedControl];
}

- (void)setTitle:(NSString*)title forStatus:(NSUInteger)status
{
    [self.statusesControl setTitle:title forSegmentAtIndex:status];
    [self updateSegmentedControl];
}

- (void)insertStatusWithImage:(UIImage*)image atIndex:(NSUInteger)index animated:(BOOL)animated
{
    [self.statusesControl insertSegmentWithImage:image atIndex:index animated:animated];
    [self updateSegmentedControl];
}

- (void)insertStatusWithTitle:(NSString*)title atIndex:(NSUInteger)index animated:(BOOL)animated
{
    [self.statusesControl insertSegmentWithTitle:title atIndex:index animated:animated];
    [self updateSegmentedControl];
}

- (void)removeAllStatuses
{
    [self.statusesControl removeAllSegments];
    [self.itemsForStatus removeAllObjects];
    [self updateSegmentedControl];
}

- (void)removeStatusAtIndex:(NSUInteger)index animated:(BOOL)animated
{
    [self.statusesControl removeSegmentAtIndex:index animated:animated];
    [self.itemsForStatus removeObjectForKey:@(index)];
    [self updateSegmentedControl];
    
    //FIXME: ADD CODE IN CASE THE IS THE SELECTED ELEMENT TO BE DELETED...
}

#pragma mark -
#pragma mark UIToolbar decorator

//FIMXE: add some way to specify the toolbar in case of iphone.
- (void)setBackgroundImage:(UIImage *)backgroundImage forToolbarPosition:(UIToolbarPosition)topOrBottom barMetrics:(UIBarMetrics)barMetrics
{
    [self.toolbar setBackgroundImage:backgroundImage forToolbarPosition:topOrBottom barMetrics:barMetrics];
}

- (UIImage *)backgroundImageForToolbarPosition:(UIToolbarPosition)topOrBottom barMetrics:(UIBarMetrics)barMetrics
{
    return [self.toolbar backgroundImageForToolbarPosition:topOrBottom barMetrics:barMetrics];
}

- (void)setShadowImage:(UIImage *)shadowImage forToolbarPosition:(UIToolbarPosition)topOrBottom
{
    [self.toolbar setShadowImage:shadowImage forToolbarPosition:topOrBottom];
}

- (UIImage *)shadowImageForToolbarPosition:(UIToolbarPosition)topOrBottom
{
    return [self.toolbar shadowImageForToolbarPosition:topOrBottom];
}

- (void) setTintColor:(UIColor*)color
{
    [self.toolbar setTintColor:color];
}

#pragma mark -
#pragma mark slideBarOpener

- (void)toggleToolbar
{
    self.slideBarVisible = !self.slideBarVisible;
}

- (void)setSlideBarVisible:(BOOL)visible
{
    [self setSlideBarVisible:visible animated:YES];
}

- (void)setSlideBarVisible:(BOOL)visible animated:(BOOL)animated
{
    __weak ADXToolbar *weakSelf = self;
    void(^slide)() = ^{
        CGRect f = weakSelf.slideBar.frame;
        f.origin.x = visible ? 0 : - MAX(f.size.width, f.size.height);
        weakSelf.slideBar.frame = f;
    };
    
    UIImage *icon = nil;
    if ( visible )
    {
        icon = [UIImage imageNamed:@"277-MultiplyCircle.png"];
    }
    else if ( self.statusesControl.numberOfSegments > 0 )
    {
        icon = [self.statusesControl imageForSegmentAtIndex:_currentStatus];
    }
    
    [self.slideBarOpener setImage:icon forState:UIControlStateNormal];
    
    if ( animated ) {
        weakSelf.slideBar.alpha = 1;
        [UIView animateWithDuration:0.25 delay:0 options:(UIViewAnimationOptionCurveLinear) animations:slide completion: ^(BOOL finished) {
            _slideBarVisible = visible;
            if (!visible){
                weakSelf.slideBar.alpha = 0;
            }
        }];
    } else {
        //slide();
         weakSelf.slideBar.alpha = _slideBarVisible ? 1 :  0;
        _slideBarVisible = visible;
    }
}

#pragma mark -
#pragma mark UISegmentedControl listener

- (void)segmentedControlValueChanged:(UISegmentedControl*) segmentedControl
{
    self.currentStatus = segmentedControl.selectedSegmentIndex;
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        [self setSlideBarVisible:NO];
    }
    
    if (self.delegateDidSelectedStatus) {
        [self.toolbarDelegate toolbarControl:self didSelectedStatus:self.currentStatus];
    }
}

@end





