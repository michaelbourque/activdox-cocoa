//
//  ADXSettingsTableViewController.m
//  Reader
//
//  Created by Steve Tibbett on 2013-04-11.
//  Copyright (c) 2013 ActivDox Inc. All rights reserved.
//

#import "ADXSettingsTableViewController.h"
#import "ADXAppDelegate.h"
#import "MBProgressHUD.h"

@interface ADXSettingsTableViewController () <UIAlertViewDelegate>
@property (weak, nonatomic) IBOutlet UISwitch *openLatestSwitch;
- (IBAction)openLatestSwitchChanged:(id)sender;
@property (weak, nonatomic) IBOutlet UISwitch *alternativeColorsSwitch;
- (IBAction)alternativeColorsSwitchChanged:(id)sender;
@end

@implementation ADXSettingsTableViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.tableView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"popover_bg"]];
    self.tableView.backgroundView = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [self refresh];
}

- (void)refresh
{
    self.openLatestSwitch.on = ![ADXUserDefaults sharedDefaults].shouldOpenLastReadVersion;
    self.alternativeColorsSwitch.on = [ADXUserDefaults sharedDefaults].useAlternativeColors;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
    if ([cell.reuseIdentifier isEqualToString:@"logout"]) {
        self.signoutHandler();
    }
    
    if ([cell.reuseIdentifier isEqualToString:@"removeOldDocuments"]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Remove Older Document Versions", nil)
                                   message:NSLocalizedString(@"This will remove all but the newest version of all the documents on your device. Opening an older version will automatically download it again.", nil)
                                  delegate:self
                         cancelButtonTitle:NSLocalizedString(@"Cancel", nil)
                         otherButtonTitles:NSLocalizedString(@"Remove", nil), nil];
        [alert show];
        
        [self.tableView deselectRowAtIndexPath:self.tableView.indexPathForSelectedRow animated:YES];
    }

    if ([cell.reuseIdentifier isEqualToString:@"support"]) {
        cell.selected = NO;
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:NSLocalizedString(@"support.url", nil)]];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    NSString *title = [self.tableView.dataSource tableView:tableView titleForFooterInSection:section];
    
    float height = 0;
    if (title.length > 0) {
        height = [title sizeWithFont:[UIFont systemFontOfSize:15.0] constrainedToSize:CGSizeMake(tableView.bounds.size.width-20, self.tableView.bounds.size.height)].height;

        // Padding
        height += 15;
    }

    return height;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    int height = [self tableView:tableView heightForFooterInSection:section];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 5, tableView.bounds.size.width - 20, height+5)];
    label.text = [self tableView:tableView titleForFooterInSection:section];
    label.textColor = [UIColor adxPopoverTextColor];
    label.font = [UIFont systemFontOfSize:15.0];
    label.backgroundColor = [UIColor clearColor];
    label.shadowColor = [UIColor whiteColor];
    label.shadowOffset = CGSizeMake(0, 1);
    label.textAlignment = NSTextAlignmentCenter;
    label.numberOfLines = 2;
    return label;
}

- (IBAction)alternativeColorsSwitchChanged:(id)sender
{
    [ADXUserDefaults sharedDefaults].useAlternativeColors = self.alternativeColorsSwitch.on;
    [self refresh];
}

- (IBAction)openLatestSwitchChanged:(id)sender
{
    [ADXUserDefaults sharedDefaults].shouldOpenLastReadVersion = !self.openLatestSwitch.on;
    [self refresh];
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (alertView.cancelButtonIndex != buttonIndex) {
        [self deleteOldVersions];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [super tableView:tableView cellForRowAtIndexPath:indexPath];

    cell.textLabel.textColor = [UIColor adxPopoverTextColor];

    if ([cell.reuseIdentifier isEqualToString:@"removeOldDocuments"]) {
        if ([self documentsWithOldContent].count == 0) {
            cell.textLabel.textColor = [UIColor lightGrayColor];
            cell.userInteractionEnabled = NO;
        } else {
            cell.userInteractionEnabled = YES;
        }
    }
    
    return cell;
}

- (NSArray *)documentsWithOldContent
{
    ADXAccount *currentAccount = [ADXAppDelegate sharedInstance].currentAccount;
    NSManagedObjectContext *moc = [ADXAppDelegate sharedInstance].managedObjectContext;
    NSArray *allDocuments = [ADXDocument allDocumentsInContext:moc];
    NSMutableSet *processedDocumentIDs = [NSMutableSet set];
    NSMutableArray *documentsWithOldContent = [NSMutableArray array];
    
    for (ADXDocument *document in allDocuments) {
        if ([processedDocumentIDs containsObject:document.id]) {
            continue;
        }
        [processedDocumentIDs addObject:document.id];
        
        // Find all versions of this document
        NSArray *documents = [ADXDocument allVersionsOfDocument:document.id accountID:currentAccount.id context:moc];
        ADXDocument *latestVersion = [ADXDocument latestDocumentWithID:document.id accountID:currentAccount.id context:moc];
        
        for (ADXDocument *docVersion in documents) {
            if (docVersion != latestVersion && docVersion.content.data != nil) {
                [documentsWithOldContent addObject:docVersion];
            }
        }
    }

    return documentsWithOldContent;
}

- (void)deleteOldVersions
{
    MBProgressHUD *HUD = [MBProgressHUD showHUDAddedTo:self.tableView animated:YES];
    HUD.labelText = NSLocalizedString(@"Removing Documents", nil);

    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{

        NSManagedObjectContext *moc = [ADXAppDelegate sharedInstance].managedObjectContext;

        NSArray *documentsWithOldContent = [self documentsWithOldContent];
        for (ADXDocument *document in documentsWithOldContent) {
            document.content.data = nil;
        }

        NSError *error = nil;
        [moc save:&error];
        if (error != nil) {
            LogError(@"Unexpected error deleting old document versions: %@", error);
        }
        
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            [HUD hide:YES];
            [self.tableView reloadData];
        }];
    });
}

@end
