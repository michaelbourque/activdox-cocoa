//
//  ADXCollectionViewCell.h
//  Reader
//
//  Created by Gavin McKenzie on 2013-04-28.
//  Copyright (c) 2013 ActivDox Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ActivDoxCore.h"

#import "ADXAppCommonMacros.h"
#import "ADXActivityImageView.h"
#import "ADXImageCache.h"

@interface ADXCollectionViewCell : UICollectionViewCell
@property (strong, nonatomic) ADXDocument *documentEntity;
@property (strong, nonatomic) UIImageView *imageView;
@property (strong, nonatomic) UIImageView *ribbonView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *firstMetaLabel;
@property (weak, nonatomic) IBOutlet UILabel *secondMetaLabel;
@property (weak, nonatomic) IBOutlet UILabel *thirdMetaLabel;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (strong, nonatomic) UIProgressView *progressBar;
@property (weak, nonatomic) IBOutlet ADXActivityImageView *spinner;
@property (weak, nonatomic) IBOutlet UIImageView *documentNewBadge;
@property (weak, nonatomic) IBOutlet UIButton *changeBadge;
@property (assign, nonatomic) BOOL trackReachabilityChanges;
@property (strong, nonatomic) ADXImageCache *imageCache;
- (UIImage *)thumbnailImage;
- (void)updateBadge;
@end
