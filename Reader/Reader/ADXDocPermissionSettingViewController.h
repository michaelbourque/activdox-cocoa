//
//  ADXPermissionSettingViewController.h
//  Reader
//
//  Created by Kei Turner on 2013-09-15.
//  Copyright (c) 2013 ActivDox Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ADXDocPermissionSettingViewController : UITableViewController{
    NSMutableArray* includedLocations;
}

@property (strong, nonatomic) IBOutlet UIView *locationsHeaderView;

-(id)initWithStyle: (UITableViewStyle)style andIncludedLocations:(NSArray*) locations;
@end
