//
//  ADXToolbar.h
//  bottomBar
//
//  Created by matteo ugolini on 2012-10-13.
//  Copyright (c) 2012 matteo ugolini. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ADXToolbarDelegate;

@interface ADXToolbar : UIView
@property (nonatomic)           NSUInteger currentStatus;
@property (nonatomic, readonly) NSUInteger statusCount;
@property (nonatomic, retain)    UIColor     *tintColor;
@property (nonatomic) id<ADXToolbarDelegate> toolbarDelegate;

- (void)setImage:(UIImage*)image forStatus:(NSUInteger)status;
- (void)setTitle:(NSString*)title forStatus:(NSUInteger)status;

- (void)insertStatusWithImage:(UIImage*)image atIndex:(NSUInteger)index animated:(BOOL)animated;

- (void)removeAllStatuses;
- (void)removeStatusAtIndex:(NSUInteger)index animated:(BOOL)animated;

- (void)setItems:(NSArray*)items;
- (void)setItems:(NSArray*)items animated:(BOOL)animated;
- (void)setItems:(NSArray*)items forStatus:(NSUInteger)status animated:(BOOL)animated;

- (void)setBackgroundImage:(UIImage *)backgroundImage forToolbarPosition:(UIToolbarPosition)topOrBottom barMetrics:(UIBarMetrics)barMetrics;
- (UIImage *)backgroundImageForToolbarPosition:(UIToolbarPosition)topOrBottom barMetrics:(UIBarMetrics)barMetrics;

- (void)setShadowImage:(UIImage *)shadowImage forToolbarPosition:(UIToolbarPosition)topOrBottom;
- (UIImage *)shadowImageForToolbarPosition:(UIToolbarPosition)topOrBottom;

- (void)initToolbarForViewInFrame:(CGRect)frame;

@end

@protocol ADXToolbarDelegate <NSObject>
@optional
- (BOOL)toolbarControl:(ADXToolbar*)tabBarControl shouldChangeStatus:(NSUInteger)newStatus;
- (void)toolbarControl:(ADXToolbar*)tabBarControl didSelectedStatus:(NSUInteger)status;
- (void)toolbarControl:(ADXToolbar*)tabBarControl didGoToStatus:(NSUInteger)status fromStatus:(NSUInteger)oldStatus;
@end
