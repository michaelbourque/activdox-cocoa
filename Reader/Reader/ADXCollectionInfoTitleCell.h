//
//  ADXCollectionInfoTitleCell.h
//  Reader
//
//  Created by Gavin McKenzie on 2013-04-09.
//  Copyright (c) 2013 ActivDox Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ADXCollectionInfoTitleCellActionDelegate <NSObject>
- (void)didUpdateTitleField:(NSString *)text;
- (void)didEndEditingTextField:(NSString *)text;
@end

@interface ADXCollectionInfoTitleCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UITextField *titleField;
@property (weak, nonatomic) id<ADXCollectionInfoTitleCellActionDelegate> actionDelegate;
@end
