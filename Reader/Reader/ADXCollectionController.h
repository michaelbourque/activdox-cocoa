//
//  ADXCollectionController.h
//  Reader
//
//  Created by Gavin McKenzie on 2012-10-15.
//  Copyright (c) 2012 ActivDox Inc. All rights reserved.
//

#import <CoreData/CoreData.h>
#import "ActivDoxCore.h"

@class ADXCollectionController;

@protocol ADXCollectionControllerDelegate <NSFetchedResultsControllerDelegate>
@end

typedef enum {
    ADXCollectionControllerSortTitle = 0,
    ADXCollectionControllerSortDate,
    ADXCollectionControllerSortChanges
} ADXCollectionControllerSort;

@interface ADXCollectionController : NSObject
@property (strong, nonatomic) ADXCollection *collection;
@property (weak, nonatomic) id<ADXCollectionControllerDelegate> delegate;
@property (assign, nonatomic) ADXCollectionControllerSort sortFilter;
- (id)initWithManagedObjectContext:(NSManagedObjectContext *)moc sortFilter:(ADXCollectionControllerSort)sortFilter;
- (ADXCollection *)collection;
- (ADXV2Service *)service;
- (NSInteger)numberOfItems;
- (NSInteger)numberOfItemsInSection:(NSInteger)section;
- (NSInteger)numberOfSections;
- (id)objectAtIndexPath:indexPath;
- (void)refresh:(BOOL)forceLoadAll;
- (void)purgeRemovedDocument:(NSString *)documentID;
@end
