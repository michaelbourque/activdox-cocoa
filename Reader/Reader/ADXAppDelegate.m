//
//  ADXAppDelegate.m
//  Reader
//
//  Created by Gavin McKenzie on 12-04-30.
//  Copyright (c) 2012 ActivDox Inc. All rights reserved.
//

#import "ADXAppDelegate.h"
#import "ActivDoxCore.h"
#import "ADXToolbar.h"
#import "ADXNotificationBubbleManager.h"
#import "ADXPopoverBarButtonItem.h"
#import "ADXPopoverController.h"

#import "DSFingerTipWindow.h"
#import "PSPDFKit.h"

const struct ADXAppState ADXAppState = {
    .startup = @"startup",
    .startupEnded = @"startupEnded",
    .viewingDocBrowser = @"viewingDocBrowser",
    .viewingDocBrowserEnded = @"viewingDocBrowserEnded",
    .viewingDocument = @"viewingDocument",
    .viewingDocumentEnded = @"viewingDocumentEnded",
};

NSString * const kADXLogoutNotification = @"kADXLogoutNotification";

@interface ADXAppDelegate () <MFMailComposeViewControllerDelegate, UIAlertViewDelegate>
@property (strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@property (strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (strong, nonatomic) UIWindow *customWindow;
@property (readwrite, strong, nonatomic) ADXAccount *currentAccount;
@property (strong, nonatomic) UIViewController *emailDatabaseParentViewController;
@property (strong, nonatomic) UIAlertView *reauthAlertView;
@property (assign, nonatomic) BOOL reauthInProgress;
@end

@implementation ADXAppDelegate
@synthesize window = _window;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;
@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize readerAppState = _appState;
@synthesize currentAccount = _currentAccount;

#pragma mark - Class Methods

+ (ADXAppDelegate *)sharedInstance
{
    return (ADXAppDelegate *)[[UIApplication sharedApplication] delegate];
}

#pragma mark - Application LifeCycle

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [self setupTestFlightApp];
    LogInfo(@"Started launch of app version %@", [self buildDescription]);
    
    [self updateSettingsVersionReadout];
    [self resetLocalStorageIfNecessary];
    [[AFNetworkActivityIndicatorManager sharedManager] setEnabled:YES];
    [self setupRemoteNotifications];
    
    [self setupUIAppearance];
    
    [self setReaderAppState:ADXAppState.startup];
    [self setStashedState:[[NSMutableDictionary alloc] init]];

    // Check for push notification received while the app was not running (scenario here is that the user clicked on a notification to lauch the app.  See didReceiveRemoteNotification below for case where the app is in foreground or suspended
    if (launchOptions != nil)
	{
		NSDictionary* userInfo = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
        if (userInfo != nil) {
            LogInfo(@"Did receive notification: %@", userInfo);
            [self processNotification:userInfo userActivated:YES];
        }
	}
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    [[ADXAppDelegate sharedInstance] passCheckpoint:@"Application entered background"];

    LogTrace(@"Begun entering background...");
    [ADXV2Service saveContextsForAllRegisteredServices];
    [[ADXUserDefaults sharedDefaults] setDidShutdownDirty:NO];
    LogTrace(@"Finished entering background...");
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    [[ADXAppDelegate sharedInstance] passCheckpoint:@"Application became active"];

    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    LogInfo(@"Received URL request'%@' from application '%@'", url.absoluteString, sourceApplication);
    self.pendingURLSchemeRequest = [[ADXURLSchemeRequest alloc] initWithURL:url.absoluteString];
    [[NSNotificationCenter defaultCenter] postNotificationName:kADXURLSchemeRequestNotification object:nil];
    return YES;
}

#pragma mark - UI Customization

- (UIWindow *)window
{
    if (!_window) {
        _window = [[DSFingerTipWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    }
    return _window;
}

- (void)setupDefaultNavbarAppearance
{
    UIImage *navBarBg = [[UIImage imageNamed:@"navbar.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(15.0f, 15.0f, 0.0f, 15.0f)];
    
    [[UINavigationBar appearance] setBackgroundImage:navBarBg forBarMetrics:UIBarMetricsDefault];
    [[UINavigationBar appearance] setTitleTextAttributes:
     [NSDictionary dictionaryWithObjectsAndKeys:
      [UIColor colorWithRed:255.0/255.0 green:255.0/255.0 blue:255.0/255.0 alpha:1.0],
      UITextAttributeTextColor,
      [UIColor colorWithWhite:0.5 alpha:1.0],
      UITextAttributeTextShadowColor,
      [NSValue valueWithUIOffset:UIOffsetMake(0, -1)],
      UITextAttributeTextShadowOffset,
      [UIFont fontWithName:@"HelveticaNeue-Medium" size:0.0],
      UITextAttributeFont,
      nil]];
    [[UINavigationBar appearanceWhenContainedIn:[UIPopoverController class], nil] setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];
    
    // Regular navbar buttons
    [[UIBarButtonItem appearance] setBackgroundImage:[UIImage imageNamed:@"button.png"] forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    [[UIBarButtonItem appearance] setBackgroundImage:[UIImage imageNamed:@"button_pushed.png"] forState:UIControlStateHighlighted barMetrics:UIBarMetricsDefault];

    // Back button
    UIImage *buttonBack = [[UIImage imageNamed:@"button_back.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(0.0f, 30.0f, 0.0f, 20.0f)];
    UIImage *buttonBackPushed = [[UIImage imageNamed:@"button_back_pushed.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(0.0f, 30.0f, 0.0f, 20.0f)];
    [[UIBarButtonItem appearance] setBackButtonBackgroundImage:buttonBack forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    [[UIBarButtonItem appearance] setBackButtonBackgroundImage:buttonBackPushed forState:UIControlStateHighlighted barMetrics:UIBarMetricsDefault];

    // Popover navbar buttons
    [[UIBarButtonItem appearanceWhenContainedIn:[UIPopoverController class], nil] setBackgroundImage:[UIImage imageNamed:@"popover_btn_sq"] forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    [[UIBarButtonItem appearanceWhenContainedIn:[UIPopoverController class], nil] setBackgroundImage:[UIImage imageNamed:@"popover_btn_sq_pushed"] forState:UIControlStateHighlighted barMetrics:UIBarMetricsDefault];
    
    [[UIBarButtonItem appearanceWhenContainedIn:[PSPDFPopoverController class], nil] setBackgroundImage:nil forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    [[UIBarButtonItem appearanceWhenContainedIn:[PSPDFPopoverController class], nil] setBackgroundImage:nil forState:UIControlStateHighlighted barMetrics:UIBarMetricsDefault];
    
    UIImage *image = [UIImage imageNamed:@"button.png"];
    [[ADXPopoverBarButtonItem appearanceWhenContainedIn:[ADXPopoverController class], nil] setBackgroundImage:image forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    image = [UIImage imageNamed:@"button_pushed.png"];
    [[ADXPopoverBarButtonItem appearanceWhenContainedIn:[ADXPopoverController class], nil] setBackgroundImage:image forState:UIControlStateHighlighted barMetrics:UIBarMetricsDefault];

    
    // Popover Back Button
    UIImage *popoverButtonBack = [[UIImage imageNamed:@"popover_btn"] resizableImageWithCapInsets:UIEdgeInsetsMake(0.0f, 25.0f, 0.0f, 5.0f)];
    UIImage *popoverButtonBackPushed = [[UIImage imageNamed:@"popover_btn_pushed"] resizableImageWithCapInsets:UIEdgeInsetsMake(0.0f, 25.0f, 0.0f, 5.0f)];

    [[UIBarButtonItem appearanceWhenContainedIn:[UIPopoverController class], nil] setBackButtonBackgroundImage:popoverButtonBack forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    [[UIBarButtonItem appearanceWhenContainedIn:[UIPopoverController class], nil] setBackButtonBackgroundImage:popoverButtonBackPushed forState:UIControlStateHighlighted barMetrics:UIBarMetricsDefault];

    [[UIBarButtonItem appearanceWhenContainedIn:[UIPopoverController class], nil] setBackButtonTitlePositionAdjustment:UIOffsetMake(-2.0, 0.0) forBarMetrics:UIBarMetricsDefault];

    //
    // Segmented buttons
    //
    
    
    [[UISegmentedControl appearance] setTitleTextAttributes:@{
                                  UITextAttributeTextColor : [UIColor colorWithRed:0.310 green:0.329 blue:0.349 alpha:1.000],
                            UITextAttributeTextShadowColor : [UIColor colorWithRed:0.851 green:0.863 blue:0.875 alpha:1.000]} forState:UIControlStateSelected];
    
    [[UISegmentedControl appearance] setTitleTextAttributes:@{
                                  UITextAttributeTextColor : [UIColor colorWithRed:0.310 green:0.329 blue:0.349 alpha:1.000],
                            UITextAttributeTextShadowColor : [UIColor colorWithRed:0.851 green:0.863 blue:0.875 alpha:1.000]} forState:UIControlStateNormal];

    
    UIImage *unselectedBackgroundImage = [[UIImage imageNamed:@"segment_caps_unselected"] resizableImageWithCapInsets:UIEdgeInsetsMake(10, 9, 10, 9)];
    [[UISegmentedControl appearance] setBackgroundImage:unselectedBackgroundImage
                                               forState:UIControlStateNormal
                                             barMetrics:UIBarMetricsDefault];
    
    /* Selected background */
    UIImage *selectedBackgroundImage = [[UIImage imageNamed:@"segment_caps_selected"] resizableImageWithCapInsets:UIEdgeInsetsMake(10, 9, 10, 9)];
    [[UISegmentedControl appearance] setBackgroundImage:selectedBackgroundImage
                                               forState:UIControlStateSelected
                                             barMetrics:UIBarMetricsDefault];
    
    /* Image between two unselected segments */
    UIImage *bothUnselectedImage = [[UIImage imageNamed:@"segment_middle_both_unselected"] resizableImageWithCapInsets:UIEdgeInsetsMake(10, 6, 10, 6)];
    [[UISegmentedControl appearance] setDividerImage:bothUnselectedImage
                                 forLeftSegmentState:UIControlStateNormal
                                   rightSegmentState:UIControlStateNormal
                                          barMetrics:UIBarMetricsDefault];
    
    /* Image between two selected segments */
    UIImage *bothSelectedImage = [[UIImage imageNamed:@"segment_middle_both_selected"] resizableImageWithCapInsets:UIEdgeInsetsMake(10, 6, 10, 6)];
    [[UISegmentedControl appearance] setDividerImage:bothSelectedImage
                                 forLeftSegmentState:UIControlStateSelected
                                   rightSegmentState:UIControlStateSelected
                                          barMetrics:UIBarMetricsDefault];

    /* Image between segment selected on the left and unselected on the right */
    UIImage *leftSelectedImage = [[UIImage imageNamed:@"segment_middle_left_selected"] resizableImageWithCapInsets:UIEdgeInsetsMake(10, 6, 10, 6)];
    [[UISegmentedControl appearance] setDividerImage:leftSelectedImage
                                 forLeftSegmentState:UIControlStateSelected
                                   rightSegmentState:UIControlStateNormal
                                          barMetrics:UIBarMetricsDefault];
    
    /* Image between segment selected on the right and unselected on the right */
    UIImage *rightSelectedImage = [[UIImage imageNamed:@"segment_middle_right_selected"] resizableImageWithCapInsets:UIEdgeInsetsMake(10, 6, 10, 6)];
    [[UISegmentedControl appearance] setDividerImage:rightSelectedImage
                                 forLeftSegmentState:UIControlStateNormal
                                   rightSegmentState:UIControlStateSelected
                                          barMetrics:UIBarMetricsDefault];

    // Segmented button in ADXToolbar
    unselectedBackgroundImage = [[UIImage imageNamed:@"end_segment_reader"] resizableImageWithCapInsets:UIEdgeInsetsMake(5, 5, 5, 5)];
    [[UISegmentedControl appearanceWhenContainedIn:[ADXToolbar class], nil] setBackgroundImage:unselectedBackgroundImage
                                               forState:UIControlStateNormal
                                             barMetrics:UIBarMetricsDefault];

    selectedBackgroundImage = [[UIImage imageNamed:@"end_segment_pushed_reader"] resizableImageWithCapInsets:UIEdgeInsetsMake(5, 5, 5, 5)];
    [[UISegmentedControl appearanceWhenContainedIn:[ADXToolbar class], nil] setBackgroundImage:selectedBackgroundImage
                                               forState:UIControlStateSelected
                                             barMetrics:UIBarMetricsDefault];
    
    /* Image between two unselected segments */
    bothUnselectedImage = [[UIImage imageNamed:@"middle_segment_unpushed_wht"] resizableImageWithCapInsets:UIEdgeInsetsMake(10, 6, 10, 6)];
    [[UISegmentedControl appearanceWhenContainedIn:[ADXToolbar class], nil] setDividerImage:bothUnselectedImage
                                 forLeftSegmentState:UIControlStateNormal
                                   rightSegmentState:UIControlStateNormal
                                          barMetrics:UIBarMetricsDefault];
    
    /* Image between two selected segments */
    bothSelectedImage = [[UIImage imageNamed:@"middle_segment_pushed_reader"] resizableImageWithCapInsets:UIEdgeInsetsMake(10, 6, 10, 6)];
    [[UISegmentedControl appearanceWhenContainedIn:[ADXToolbar class], nil] setDividerImage:bothSelectedImage
                                 forLeftSegmentState:UIControlStateSelected
                                   rightSegmentState:UIControlStateSelected
                                          barMetrics:UIBarMetricsDefault];
    
    /* Image between segment selected on the left and unselected on the right */
    leftSelectedImage = [[UIImage imageNamed:@"middle_segment_left_pushed_reader"] resizableImageWithCapInsets:UIEdgeInsetsMake(10, 6, 10, 6)];
    [[UISegmentedControl appearanceWhenContainedIn:[ADXToolbar class], nil] setDividerImage:leftSelectedImage
                                 forLeftSegmentState:UIControlStateSelected
                                   rightSegmentState:UIControlStateNormal
                                          barMetrics:UIBarMetricsDefault];
    
    /* Image between segment selected on the right and unselected on the right */
    rightSelectedImage = [[UIImage imageNamed:@"middle_segment_right_pushed_reader"] resizableImageWithCapInsets:UIEdgeInsetsMake(10, 6, 10, 6)];
    [[UISegmentedControl appearanceWhenContainedIn:[ADXToolbar class], nil] setDividerImage:rightSelectedImage
                                 forLeftSegmentState:UIControlStateNormal
                                   rightSegmentState:UIControlStateSelected
                                          barMetrics:UIBarMetricsDefault];
}

- (void)setupUIAppearance
{
    [self setupDefaultNavbarAppearance];
    
    // custom pdf controller toolbar
    [[UISegmentedControl appearanceWhenContainedIn:[ADXToolbar class], nil] setTintColor:[UIColor darkGrayColor]];
    [[UIBarButtonItem appearanceWhenContainedIn:[ADXToolbar class], nil] setTintColor:[UIColor blackColor]];
    
    [[UILabel appearanceWhenContainedIn:[UITableViewHeaderFooterView class], nil] setTextColor:[UIColor whiteColor]];
    [[UILabel appearanceWhenContainedIn:[UITableViewHeaderFooterView class], nil] setShadowColor:[UIColor clearColor]];

    [[UIBarButtonItem appearanceWhenContainedIn:[UISearchBar class], nil] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                                                                                  [UIColor darkGrayColor],
                                                                                                  UITextAttributeTextColor,
                                                                                                  [UIColor clearColor],
                                                                                                  UITextAttributeTextShadowColor,
                                                                                                  [NSValue valueWithUIOffset:UIOffsetMake(0, -1)],
                                                                                                  UITextAttributeTextShadowOffset,
                                                                                                  nil]
                                                                                        forState:UIControlStateNormal];

    NSMutableDictionary *attributes = [NSMutableDictionary dictionary];
    [attributes setValue:[UIColor colorWithWhite:0.9 alpha:1.0] forKey:UITextAttributeTextColor];
    [attributes setValue:[UIColor colorWithWhite:0.3 alpha:1.0] forKey:UITextAttributeTextShadowColor];
    [[UIBarButtonItem appearanceWhenContainedIn:[ADXToolbar class], nil] setTitleTextAttributes:attributes forState:UIControlStateNormal];
    
    [[UIToolbar appearanceWhenContainedIn:[ADXToolbar class], nil] setBackgroundImage:[UIImage imageNamed:@"toolbar_reader_view_44.png"] forToolbarPosition:UIToolbarPositionAny barMetrics:UIBarMetricsDefault];
    
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        [[UISearchBar appearance] setBackgroundImage:[UIImage imageNamed:@"navbar.png"]];
    }
    
    
    
    
    //    [[UIToolbar appearance] setTitleTextAttributes:
//     [NSDictionary dictionaryWithObjectsAndKeys:
//      [UIColor colorWithRed:255.0/255.0 green:255.0/255.0 blue:255.0/255.0 alpha:1.0],
//      UITextAttributeTextColor,
//      [UIColor colorWithWhite:0.5 alpha:1.0],
//      UITextAttributeTextShadowColor,
//      [NSValue valueWithUIOffset:UIOffsetMake(0, -1)],
//      UITextAttributeTextShadowOffset,
//      [UIFont fontWithName:@"HelveticaNeue-Medium" size:0.0],
//      UITextAttributeFont,
//      nil]];
    //    [[UIToolbar appearance] setBackgroundImage:[UIImage imageNamed:@"navbar.png"] forToolbarPosition:UIToolbarPositionAny barMetrics:UIBarMetricsDefault];
}

#pragma mark - Accessors

- (void)setAppState:(NSString *)appState
{
    @synchronized(self) {
        _appState = appState;
    }
    [[NSNotificationCenter defaultCenter] postNotificationName:appState object:self];
}

- (void)setCurrentAccountByID:(NSString *)accountID
{
    [self clearCurrentAccount];
    
    ADXAccount *accountInMainManagedObjectContext = [ADXAccount entityWithID:accountID context:self.managedObjectContext];
    NSAssert(accountInMainManagedObjectContext, @"setCurrentAccountByID could not retrieve account object");
    
    [self setCurrentAccount:accountInMainManagedObjectContext];
}

- (void)clearCurrentAccount
{
    [self setCurrentAccount:nil];
}

#pragma mark - Helpers

- (void)setupRemoteNotifications
{
    // Register app to receive remote (push) notifications
    [[UIApplication sharedApplication] registerForRemoteNotificationTypes:(UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound |UIRemoteNotificationTypeAlert)];
}

- (void)updateSettingsVersionReadout
{
    NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"];
    [[NSUserDefaults standardUserDefaults] setValue:version forKey:@"version_display"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

#pragma mark - TestFlight

- (void)setupTestFlightApp
{
#if SEND_UDID_TO_TESTFLIGHT
    // Enabling this will cause the device's UDID to be set to TestFlight, so crashes and logs
    // can be associated with a specific user and device.  This cannot be enabled in an App Store build.
    [TestFlight setDeviceIdentifier:[[UIDevice currentDevice] uniqueIdentifier]];
#endif
    
    // If the bundle identifier ends with .Integration then use the TestFlight integration app token
    NSString *bundleIdentifier = [[NSBundle mainBundle] bundleIdentifier];
    if ([bundleIdentifier.pathExtension isEqualToString:@"Integration"]) {
        // Use the integration token
        [TestFlight takeOff:@"dcf48706-788b-40f2-9545-4747a66afc80"];
    } else {
        // Use the standard token
        [TestFlight takeOff:@"509e7210-0368-494d-a7f2-cc1da5004690"];
    }
}

- (void)passCheckpoint:(NSString *)checkpointName
{
#ifdef TESTING
    [TestFlight passCheckpoint:checkpointName];
#endif
}

#pragma mark - Storage Setup

- (NSString *)applicationDocumentsDirectory 
{	
    BOOL isDir = NO;
    NSError *error = nil;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
    NSString *documentsPath = [paths objectAtIndex:0];
    documentsPath = [documentsPath stringByAppendingPathComponent:@"Private Documents"];
    
    NSDictionary *fileAttributes = @{NSFileProtectionKey:NSFileProtectionComplete};
    if (![[NSFileManager defaultManager] fileExistsAtPath:documentsPath isDirectory:&isDir] && isDir == NO) {
        [[NSFileManager defaultManager] createDirectoryAtPath:documentsPath withIntermediateDirectories:NO attributes:fileAttributes error:&error];
    }
    return documentsPath;
}

- (NSString *)applicationCachesDirectory 
{
    BOOL isDir = NO;
    NSError *error = nil;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *cachePath = [paths objectAtIndex:0];
    NSDictionary *fileAttributes = @{NSFileProtectionKey:NSFileProtectionComplete};
    if (![[NSFileManager defaultManager] fileExistsAtPath:cachePath isDirectory:&isDir] && isDir == NO) {
        [[NSFileManager defaultManager] createDirectoryAtPath:cachePath withIntermediateDirectories:NO attributes:fileAttributes error:&error];
    }
    
    return cachePath;
}

- (NSManagedObjectContext *)managedObjectContext 
{	
    if (!_managedObjectContext) {
        NSPersistentStoreCoordinator *coordinator = self.persistentStoreCoordinator;
        if (coordinator) {
            _managedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
            [_managedObjectContext performBlockAndWait:^{
                [_managedObjectContext setPersistentStoreCoordinator:coordinator];
            }];
        }
    }
    return _managedObjectContext;
}

- (NSManagedObjectModel *)managedObjectModel 
{	
    if (!_managedObjectModel) {
        NSURL *activDoxCoreModelBundleURL = [[NSBundle mainBundle] URLForResource:@"ActivDoxCoreModel" withExtension:@"bundle"];
        NSBundle *activDoxCoreModelBundle = [NSBundle bundleWithURL:activDoxCoreModelBundleURL];
        _managedObjectModel = [NSManagedObjectModel mergedModelFromBundles:[NSArray arrayWithObject:activDoxCoreModelBundle]];
    }
    return _managedObjectModel;
}

- (NSURL *)storeURL
{
    NSURL *storeUrl = [NSURL fileURLWithPath: [[self applicationDocumentsDirectory] stringByAppendingPathComponent: @"ActivDox.sqlite"]];
    return storeUrl;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{	
    if (!_persistentStoreCoordinator) {
        NSURL *storeUrl = [self storeURL];
        
        NSError *error = nil;
        NSMutableDictionary *options = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                        [NSNumber numberWithBool:YES], NSMigratePersistentStoresAutomaticallyOption,
                                        [NSNumber numberWithBool:YES], NSInferMappingModelAutomaticallyOption,
                                        NSFileProtectionComplete, NSPersistentStoreFileProtectionKey,
                                        nil]; 
        NSManagedObjectModel *model = self.managedObjectModel;
        _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] 
                                       initWithManagedObjectModel:model];
        if (![_persistentStoreCoordinator 
              addPersistentStoreWithType:NSSQLiteStoreType
              configuration:nil
              URL:storeUrl
              options:options
              error:&error]) {
            LogError(@"Unresolved error %@, %@", error, [error userInfo]);
            
            // Can't open the existing database; let's wipe it and try again
            [self resetLocalStorage];

            if (![_persistentStoreCoordinator
                  addPersistentStoreWithType:NSSQLiteStoreType
                  configuration:nil
                  URL:storeUrl
                  options:options
                  error:&error]) {
                // That didn't work either; there's nothing more we can do
                exit(-1);  // Fail
            }
        }

        NSURL *baseURL = [storeUrl URLByDeletingLastPathComponent];
        NSArray *files = [[NSFileManager defaultManager] contentsOfDirectoryAtURL:baseURL
                                      includingPropertiesForKeys:nil
                                                         options:0
                                                           error:nil];
        for (NSURL *fileURL in files) {
            BOOL succ = [fileURL setResourceValue:[NSNumber numberWithBool:YES] forKey:NSURLIsExcludedFromBackupKey error:&error];
            if (!succ) {
                LogError(@"Error setting %@ to be excluded from iCloud backups: %@", fileURL, error);
            }
        }
    }
    
    return _persistentStoreCoordinator;
}

- (void)resetLocalStorage
{
    [[ADXUserDefaults sharedDefaults] resetAll];
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"reset_storage"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    NSURL *storeUrl = [NSURL fileURLWithPath: [[self applicationDocumentsDirectory] stringByAppendingPathComponent: @"ActivDox.sqlite"]];
    NSError *error = nil;
    BOOL success = [[NSFileManager defaultManager] removeItemAtURL:storeUrl error:&error];
    if (!success) {
        LogNSError(@"Couldn't remove persistent store.", error);
    }
}

- (void)resetLocalStorageIfNecessary
{
    BOOL resetStorage = [[NSUserDefaults standardUserDefaults] boolForKey:@"reset_storage"];
    if (resetStorage) {
        [self resetLocalStorage];
    }
}

#pragma mark - Notifications

// Handle successful registration for remote (push) notifications - device token received
- (void)application:(UIApplication *)app didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    if (![deviceToken length]) {
        LogError(@"didRegisterForRemoteNotificationsWithDeviceToken received an empty device token");
        return;
    }
    
    // Get current token from UserDefaults and check whether it's changed.  If not, it's a NO-OP.  However, if it has changed, we need to update UserDefaults and send the updated token to the ActivDox server (our provider)    
    NSData *currentToken=[[ADXUserDefaults sharedDefaults] currentDeviceToken];
    
    BOOL notify = NO;
    if (currentToken == nil)
    {
        // Token has never been set - save new token as the current token in user defaults
        [[ADXUserDefaults sharedDefaults] setCurrentDeviceToken:deviceToken];
        notify = YES;
    }
    else {
        if ([deviceToken isEqualToData:currentToken] == NO)
        {
            // There is an existing token and it's changed.  Set last token in user defaults to the current token and set the current token to the new one.
            [[ADXUserDefaults sharedDefaults] setPreviousDeviceToken:currentToken];
            [[ADXUserDefaults sharedDefaults] setCurrentDeviceToken:deviceToken];
            notify = YES;
        }
    }
    
    if (notify) {
        [[NSNotificationCenter defaultCenter] postNotificationName:kADXServiceNotificationAPNSTokenReceived object:deviceToken];
    }
}

// Handle failure to register for remote (push) notifications
- (void)application:(UIApplication *)app didFailToRegisterForRemoteNotificationsWithError:(NSError *)err {
    LogNSError(@"failed to register device token", err);
}

// Handle push notification when app is active.  On iOS 4.0 and later, also called if user selects notification when app suspended, causing it to wake up.
- (void)application:(UIApplication*)application didReceiveRemoteNotification:(NSDictionary*)userInfo
{
    LogInfo(@"Did receive notification: %@", userInfo);
    BOOL userActivated = !(application.applicationState == UIApplicationStateActive);
    [self processNotification:userInfo userActivated:userActivated];
}

// Promote the user acting on a notification to the equivalent of a URL scheme launch
- (void)processNotificationRequestWithEventID:(NSString *)eventID
{
    LogInfo(@"Requesting associated document ID for event ID %@", eventID);
 
    ADXV2Service *service = [ADXV2Service serviceForAccount:self.currentAccount];
    [service requestDocumentIDForEventWithID:eventID
                                  completion:^(id response, BOOL success, NSError *error) {
                                      if (success) {
                                          NSDictionary *responseDict = (NSDictionary *)response;
                                          NSString *documentID = responseDict[ADXDocumentAttributes.id];
                                          NSString *versionID = responseDict[ADXDocumentAttributes.version];
                                          
                                          NSURL *url = [NSURL URLWithString:service.account.serverURL];
                                          NSString *server = url.host;
                                          if (url.port.intValue != 80) {
                                              server = [NSString stringWithFormat:@"%@:%d", server, url.port.intValue];
                                          }
                                          
                                          NSString *launchURLString = nil;

                                          if ([versionID respondsToSelector:@selector(intValue)]) {
                                              launchURLString = [NSString stringWithFormat:@"adox://open/%@/%@/%d", server, documentID, [versionID intValue]];
                                          } else {
                                              launchURLString = [NSString stringWithFormat:@"adox://open/%@/%@", server, documentID];
                                          }
                                          
                                          LogInfo(@"Processing as %@", launchURLString);
                                          
                                          self.pendingURLSchemeRequest = [[ADXURLSchemeRequest alloc] initWithURL:launchURLString];
                                          
                                          [[NSNotificationCenter defaultCenter] postNotificationName:kADXURLSchemeRequestNotification object:nil];
                                      }
                                  }];
}

- (void)processNotification:(NSDictionary *)userInfo userActivated:(BOOL)userActivated
{
    if ([ADXUserDefaults sharedDefaults].displayRemoteNotifications && !userActivated) {
        dispatch_async(dispatch_get_main_queue(), ^{
            NSString *message = [userInfo valueForKeyPath:kADXNotificationPayloadAPSAlertMessageKeyPath];
            if (IsEmpty(message)) {
                message = @"(no message)";
            }
            
            [[ADXNotificationBubbleManager sharedInstance] displayBubbleWithMessage:message userInfo:nil];
        });
    }
    
    NSString *url = [userInfo valueForKey:kADXNotificationPayloadURLKey];
    if (url != nil) {
        // An URL was supplied in the notification; use that
        self.pendingURLSchemeRequest = [[ADXURLSchemeRequest alloc] initWithURL:url];
        [[NSNotificationCenter defaultCenter] postNotificationName:kADXURLSchemeRequestNotification object:nil];
        return;
    }
    
    NSString *eventID = [userInfo valueForKey:kADXNotificationPayloadEventIDKey];
    NSString *accountID = [userInfo valueForKey:kADXNotificationPayloadAccountIDKey];
    LogInfo(@"Requesting associated document ID for event ID %@, account %@", eventID, accountID);
    
    if (userActivated) {
        ADXV2Service *service = [ADXV2Service serviceForAccountID:accountID];
        if (service != nil && ![service.account.id isEqualToString:self.currentAccount.id]) {
            LogError(@"Unexpected: User activated a notification for a different user.  Logging in this user should have invalidated the other user's push token.");
        } else {
            if (service == nil) {
                // We can't translate the event ID into an action right now
                NSString *launchURLString = [NSString stringWithFormat:@"adox://event/%@/%@", accountID, eventID];
                LogInfo(@"Interim URL until post-login: %@", launchURLString);
                self.pendingURLSchemeRequest = [[ADXURLSchemeRequest alloc] initWithURL:launchURLString];
            } else {
                [self processNotificationRequestWithEventID:eventID];
            }
        }
    }

    [[NSNotificationCenter defaultCenter] postNotificationName:kADXRemoteNotificationEvent object:nil];
}

- (void)processUserActivatedNotification:(NSNotification *)notification
{
    BOOL userActivated = ([UIApplication sharedApplication].applicationState == UIApplicationStateActive);
    [self processNotification:notification.userInfo userActivated:userActivated];
}

- (BOOL)isPushEnabled
{
    UIApplication *application = [UIApplication sharedApplication];
    return application.enabledRemoteNotificationTypes != UIRemoteNotificationTypeNone;
}

#pragma mark - APNS

- (void)appAPNSModeIsCompatibleWithService:(ADXV2Service *)service completion:(ADXCompletionBlock)completionBlock;
{
    if (!service) {
        if (completionBlock) {
            dispatch_async(dispatch_get_main_queue(), ^{
                completionBlock(NO, nil);
            });
        }
    }
    
    [service getServerInfoAPNSMode:^(id response, BOOL success, NSError *error) {
        BOOL compatible = NO;
        NSString *apnsMode = (NSString *)response;
        NSString *compatibleAPNSMode = nil;

#if SAVVYDOX_BUILD_ADHOC
        compatibleAPNSMode = kADXServiceServerInfoAPNSSandbox;
#else
        compatibleAPNSMode = kADXServiceServerInfoAPNSProduction;
#endif
        
        if (!response || !success) {
            compatible = NO;
        } else {
            if ([apnsMode isEqualToString:compatibleAPNSMode]) {
                compatible = YES;
            } else {
                compatible = NO;
            }
        }
        
        if (completionBlock) {
            dispatch_async(dispatch_get_main_queue(), ^{
                completionBlock(compatible, nil);
            });
        }
    }];
}

#pragma mark - App Info

- (NSString *)buildDescription
{
    NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"];
    NSString *shortVersion = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    NSString *buildType = nil;
    
#if TARGET_IPHONE_SIMULATOR
    buildType = @"(Simulator Build)";
#else
#if DEBUG
    buildType = @"(Development Build)";
#else
    buildType = @"(Distribution Build)";
#endif
#endif
    
    return [NSString stringWithFormat:@"%@ %@ %@", shortVersion, version, buildType];
}

#pragma mark - Localization

- (NSString *)localizedStringForKey:(NSString *)key
{
    NSString *result = nil;
    result = NSLocalizedString(key, nil);
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        NSString *iPhoneKey = [NSString stringWithFormat:@"%@_iPhone", key];
        NSString *iPhoneResult = NSLocalizedString(iPhoneKey, nil);
        if (![iPhoneResult isEqualToString:iPhoneKey]) {
            result = iPhoneResult;
        }
    }
    return result;
}

#pragma mark - Debugging

- (void)presentEmailDatabaseComposer:(UIViewController *)viewController
{
    if (self.emailDatabaseParentViewController) {
        return;
    }
    
    self.emailDatabaseParentViewController = viewController;
    
    MFMailComposeViewController *mailComposer = [[MFMailComposeViewController alloc] init];
    mailComposer.mailComposeDelegate = self;
    
    [mailComposer setToRecipients:[NSArray arrayWithObject:@"dan@activdox.com"]];
    [mailComposer setSubject:@"Local database copy for debugging purposes"];
    [mailComposer setMessageBody:@"See the attached database file." isHTML:NO];
    [mailComposer setModalPresentationStyle:UIModalPresentationFormSheet];
    
    NSData *sqliteFile = [NSData dataWithContentsOfURL:[self storeURL]];
    [mailComposer addAttachmentData:sqliteFile mimeType:@"application/x-savvydox.backup.sqlite" fileName:@"ActivDox.sqlite"];
    
    [self.emailDatabaseParentViewController presentViewController:mailComposer animated:YES completion:nil];
}

- (void)dumpAllDocuments
{
    __weak ADXAppDelegate *weakSelf = self;
    [self.managedObjectContext performBlockAndWait:^{
        [ADXDocument dumpInContext:weakSelf.managedObjectContext];
    }];
}

- (void)dumpAllFolders
{
    [self.managedObjectContext performBlockAndWait:^{
        [ADXCollection dumpInContext:self.managedObjectContext];
    }];
}

#pragma mark - Mail Delegate

- (void)mailComposeController:(MFMailComposeViewController*)controller
          didFinishWithResult:(MFMailComposeResult)result
                        error:(NSError*)error {
    [self.emailDatabaseParentViewController dismissViewControllerAnimated:YES completion:nil];
    self.emailDatabaseParentViewController = nil;
}

#pragma mark - Authentication Popup

- (void)presentUserReauthenticationPopupIfNecessary
{
    __weak ADXAppDelegate *weakSelf = self;
    
    @synchronized(self) {
        if (self.reauthInProgress) {
            return;
        } else {
            self.reauthInProgress = YES;
        }
    }

    // Try to authenticate with current credentials before displaying the popup
    [self reauthenticateCurrentAccountWithPassword:nil completion:^(BOOL success, NSError *error) {
        if (success) {
            self.reauthInProgress = NO;
        } else {
            [weakSelf presentUserReauthenticationPopup];
        }
    }];
}

- (void)presentUserReauthenticationPopup
{
    NSString *message = [NSString stringWithFormat:@"Enter password for %@", self.currentAccount.loginName];

    self.reauthAlertView = [[UIAlertView alloc]
                            initWithTitle:@"Re-authentication Required"
                            message:message
                            delegate:self
                            cancelButtonTitle:@"Cancel"
                            otherButtonTitles:@"OK", nil];
    self.reauthAlertView.alertViewStyle = UIAlertViewStyleSecureTextInput;

    [self.reauthAlertView show];
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    __weak ADXAppDelegate *weakSelf = self;

    self.reauthAlertView = nil;

    if (buttonIndex == 0) {
        self.reauthInProgress = NO;
        return;
    }
    
    NSString *password = [[alertView textFieldAtIndex:0] text];
    [self reauthenticateCurrentAccountWithPassword:password completion:^(BOOL success, NSError *error) {
        if (success) {
            self.reauthInProgress = NO;
        } else {
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                [weakSelf presentUserReauthenticationPopup];
            }];
        }
    }];
}

- (void)reauthenticateCurrentAccountWithPassword:(NSString *)password completion:(ADXCompletionBlock)completionBlock
{
    __weak ADXAppDelegate *weakSelf = self;
    ADXV2Service *service = [ADXV2Service serviceForAccount:self.currentAccount];

    NSString *loginName = self.currentAccount.loginName;

    if (IsEmpty(password)) {
        password = self.currentAccount.password;
    }
    
    [service reauthenticateWithLoginName:loginName password:password completion:^(NSUInteger statusCode, BOOL success, NSError *error) {
        if (success) {
            LogTrace(@"Reauthentication succeeeded");
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                BOOL reauthSuccess = success;
                NSError *reauthError = error;
                [weakSelf updateAccountPasswordAfterReauthentication:password completion:^(BOOL success, NSError *error) {
                    if (completionBlock) {
                        completionBlock(reauthSuccess, reauthError);
                    }
                }];
            }];
        } else {
            LogNSError(@"Reauthentication failed:", error);
            if (completionBlock) {
                [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                    completionBlock(success, error);
                }];
            }
        }
    }];
}

- (void)updateAccountPasswordAfterReauthentication:(NSString *)password completion:(ADXCompletionBlock)completionBlock
{
    __weak ADXAppDelegate *weakSelf = self;
    NSManagedObjectContext *moc = self.managedObjectContext;
    
    [moc performBlock:^{
        weakSelf.currentAccount.password = password;
        [weakSelf saveMainContext:^(BOOL success, NSError *error) {
            if (!success) {
                LogNSError(@"Couldn't save updated password in main context after reauthentication.", error);
            }
            if (completionBlock) {
                [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                    completionBlock(success, error);
                }];
            }
        }];
    }];
}

@end
