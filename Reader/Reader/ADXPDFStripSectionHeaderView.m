//
//  ADXPDFStripSectionHeaderView.m
//  Reader
//
//  Created by Steve Tibbett on 2012-11-30.
//  Copyright (c) 2012 ActivDox Inc. All rights reserved.
//

#import "ADXPDFStripSectionHeaderView.h"

@implementation ADXPDFStripSectionHeaderView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
