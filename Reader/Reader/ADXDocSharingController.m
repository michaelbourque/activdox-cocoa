//
//  ADXDocSharingController.m
//  Reader
//
//  Created by Gavin McKenzie on 2012-09-14.
//  Copyright (c) 2012 ActivDox Inc. All rights reserved.
//

#import "ADXDocSharingController.h"
#import "ActivDoxCore.h"

#import "PSPDFKit.h"

@interface ADXDocSharingController () <MFMailComposeViewControllerDelegate>
@property (weak, nonatomic) id<MFMailComposeViewControllerDelegate> delegate;
@property (weak, nonatomic) ADXDocument *document;
@property (strong, nonatomic) PSPDFDocument *pdfDocument;
@property (strong, nonatomic) UIImage *thumbnail;
@end

@implementation ADXDocSharingController

#pragma mark - Class Methods

+ (ADXDocSharingController *)controllerWithDocument:(ADXDocument *)document mailDelegate:(id<MFMailComposeViewControllerDelegate>)delegate
{
    ADXDocSharingController *controller = [[ADXDocSharingController alloc] init];
    controller.delegate = delegate;
    controller.document = document;
    [controller updateThumbnail];
    
    return controller;
}

#pragma mark - Accessors

- (void)setDocument:(ADXDocument *)document
{
    _document = document;    
}

- (PSPDFDocument *)pdfDocument
{
    if (!_pdfDocument && self.document.downloaded && self.document.content.data) {
        _pdfDocument = [[PSPDFDocument alloc] initWithData:self.document.content.data];
    }
    return _pdfDocument;
}

- (void)updateThumbnail
{
    CGPDFPageRef pdfPage = [[PSPDFGlobalLock sharedGlobalLock] lockWithDocument:self.pdfDocument page:0 error:nil];
    if (pdfPage) {
        self.thumbnail = [[PSPDFCache sharedCache] renderImageForDocument:self.pdfDocument page:0 size:PSPDFSizeThumbnail PDFPage:pdfPage];
        [[PSPDFGlobalLock sharedGlobalLock] freeWithPDFPageRef:pdfPage];
    }
}

#pragma mark - Message Population

- (NSString *)populatedMessageBody
{
    NSString *mailTemplateContent = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"mail_template" ofType:@"html"]
                                                              encoding:NSUTF8StringEncoding
                                                                 error:NULL];
    NSError *error = nil;
    GRMustacheTemplate *mailTemplate = [GRMustacheTemplate templateFromString:mailTemplateContent error:&error];
    if (!mailTemplate) {
        LogNSError(@"populatedMessageBody could not create mail body template.", error);
        return nil;
    }

    NSAssert(mailTemplate, @"populatedMessageBody couldn't find mail template");

    NSString *documentDescription = IsEmpty(self.document.documentDescription) ? @"n/a" : self.document.documentDescription;
    NSString *documentTitle = IsEmpty(self.document.title) ? @"Untitled" : self.document.title;
    NSString *documentVersion = [self.document versionDisplayLabel];
    
    NSData *thumbnailData = UIImagePNGRepresentation(self.thumbnail);
    NSString *thumbnailString = [thumbnailData LR_base64EncodedString];
    thumbnailString = [thumbnailString stringByReplacingOccurrencesOfString:@"\r" withString:@""];
    thumbnailString = [thumbnailString stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    
    NSDictionary *dictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                documentTitle, @"document_title",
                                documentVersion, @"document_version",
                                documentDescription, @"document_description",
                                thumbnailString, @"document_thumbnail",
                                @"#", @"document_link",
                                nil];
    NSString *populatedBody = [mailTemplate renderObject:dictionary error:&error];
    return populatedBody;
}

- (BOOL)presentShareDocumentViewFromViewController:(UIViewController *)viewController
{
    BOOL success = YES;
    
    if (![MFMailComposeViewController canSendMail])
        return NO;
    
    NSString *messageBody = [self populatedMessageBody];
    if (IsEmpty(messageBody)) {
        LogError(@"presentShareDocumentViewFromViewController received nil message body.");
        return NO;
    }
    
    MFMailComposeViewController *mailViewController = [[MFMailComposeViewController alloc] init];
    [mailViewController setMessageBody:messageBody isHTML:YES];
    [mailViewController setToRecipients:nil];
    [mailViewController setSubject:[NSString stringWithFormat:@"Shared ActivDox document \"%@\"", self.document.title]];
    [mailViewController setMailComposeDelegate:self];
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        mailViewController.modalPresentationStyle = UIModalPresentationFormSheet;
    } else {
        mailViewController.modalPresentationStyle = UIModalPresentationFullScreen;
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [viewController presentViewController:mailViewController animated:YES completion:nil];
    });
    
    return success;
}

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    __weak ADXDocSharingController *weakSelf = self;
    
    if (self.delegate) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [weakSelf.delegate mailComposeController:controller didFinishWithResult:result error:error];
        });
    }
}

@end
