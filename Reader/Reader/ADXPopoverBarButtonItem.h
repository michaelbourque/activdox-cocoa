//
//  ADXPopoverBarButtonItem.h
//  Reader
//
//  Created by Steve Tibbett on 2013-05-14.
//  Copyright (c) 2013 ActivDox Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

// Do-nothing class that exists just for UIAppearance targeting
@interface ADXPopoverBarButtonItem : UIBarButtonItem

@end
