//
//  ADXNotificationsViewController.h
//  Reader
//
//  Created by Gavin McKenzie on 2013-02-14.
//  Copyright (c) 2013 ActivDox Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ActivDoxCore.h"
#import "ADXActivityImageView.h"

extern NSString * const kADXNotificationsOptionKeyFilterType;
extern NSString * const kADXNotificationsOptionValueFilterTypeDocument;
extern NSString * const kADXNotificationsOptionValueFilterTypeCollection;
extern NSString * const kADXNotificationsOptionKeyFilterObjectID;

@protocol ADXNotificationsActionsDelegate <NSObject>
- (void)didSelectDocument:(ADXDocument *)document fromEvent:(ADXEvent *)event;
@end

@interface ADXNotificationsViewController : UITableViewController
@property (weak, nonatomic) UIPopoverController *parentPopoverController;
@property (strong, nonatomic) NSString *accountID;
+ (UIPopoverController *)popoverControllerWithAccount:(NSString *)accountID actionDelegate:(id)delegate options:(NSDictionary *)options managedObjectContext:(NSManagedObjectContext *)moc;
+ (UIViewController *)modalControllerWithAccount:(NSString *)accountID actionDelegate:(id)delegate options:(NSDictionary *)options managedObjectContext:(NSManagedObjectContext *)moc;
@property (weak, nonatomic) id<ADXNotificationsActionsDelegate> actionDelegate;
@end
