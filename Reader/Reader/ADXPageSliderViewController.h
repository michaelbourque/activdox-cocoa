//
//  ADXPageSliderViewController.h
//  Reader
//
//  Created by Steve Tibbett on 2013-04-03.
//  Copyright (c) 2013 ActivDox Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^ADXPageSliderNavigateBlock)(int pageNumber);

@interface ADXPageSliderViewController : UIViewController
@property (assign, nonatomic) int currentPage;
- (id)initWithNumPages:(int)numPages navigateBlock:(ADXPageSliderNavigateBlock)navigateBlock;
@end
