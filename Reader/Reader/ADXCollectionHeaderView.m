//
//  ADXCollectionHeaderView.m
//  Reader
//
//  Created by Gavin McKenzie on 2013-04-30.
//  Copyright (c) 2013 ActivDox Inc. All rights reserved.
//

#import "ADXCollectionHeaderView.h"

@implementation ADXCollectionHeaderView

- (IBAction)didSelectSortOrder:(id)sender
{
    if (self.actionDelegate) {
        [self.actionDelegate collectionHeaderView:self didSelectSortOrder:self.sortSegmentedButtons.selectedSegmentIndex];
    }
}

@end
