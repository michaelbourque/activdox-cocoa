//
//  ADXSettingsTableViewController.h
//  Reader
//
//  Created by Steve Tibbett on 2013-04-11.
//  Copyright (c) 2013 ActivDox Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ADXSettingsTableViewController : UITableViewController
@property (strong, nonatomic) void (^signoutHandler)();
@end
