//
//  ADXPSPDFNoteAnnotationController.h
//  Reader
//
//  Created by Steve Tibbett on 2012-11-21.
//  Copyright (c) 2012 ActivDox Inc. All rights reserved.
//

#import "PSPDFNoteAnnotationController.h"

@protocol ADXPSPDFNoteAnnotationControllerDelegate <PSPDFNoteAnnotationControllerDelegate>
- (void)noteAnnotationController:(PSPDFNoteAnnotationController *)noteAnnotationController didToggleAnnotationResolved:(PSPDFAnnotation *)annotation;
@end

@interface ADXPSPDFNoteAnnotationController : PSPDFNoteAnnotationController <UIAlertViewDelegate>
@end
