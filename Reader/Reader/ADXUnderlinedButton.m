//
//  ADXUnderlinedButton.m
//  Reader
//
//  Created by Gavin McKenzie on 2012-07-19.
//  Copyright (c) 2012 ActivDox Inc. All rights reserved.
//

#import "ADXUnderlinedButton.h"

@implementation ADXUnderlinedButton

+ (ADXUnderlinedButton*)underlinedButton
{
    ADXUnderlinedButton* button = [[ADXUnderlinedButton alloc] init];
    return button;
}

- (void)drawRect:(CGRect)rect
{
    CGRect textRect = self.titleLabel.frame;
    
    // need to put the line at top of descenders (negative value)
    CGFloat descender = self.titleLabel.font.descender;

    CGContextRef contextRef = UIGraphicsGetCurrentContext();
    CGFloat shadowHeight = self.titleLabel.shadowOffset.height;
    descender += shadowHeight + 1.0f;
        
    // set to same colour as text
    CGContextSetStrokeColorWithColor(contextRef, self.titleLabel.textColor.CGColor);
    
    CGContextMoveToPoint(contextRef, textRect.origin.x, textRect.origin.y + textRect.size.height + descender);
    
    CGContextAddLineToPoint(contextRef, textRect.origin.x + textRect.size.width, textRect.origin.y + textRect.size.height + descender);
    
    CGContextClosePath(contextRef);
    
    CGContextDrawPath(contextRef, kCGPathStroke);
}

- (void)setHighlighted:(BOOL)highlighted
{
    [super setHighlighted:highlighted];
    [self setNeedsDisplay];
}

@end
