//
//  ADXLinkedAnnotation.h
//  Reader
//
//  Created by Matteo Ugolini on 2012-09-25.
//  Copyright (c) 2012 ActivDox Inc. All rights reserved.
//

#import "PSPDFHighlightAnnotation.h"
#import "ADXAnnotationUtil.h"
#import "ActivDoxCore.h"

@class ADXNoteAnnotation;

@interface ADXHighlightAnnotation : PSPDFHighlightAnnotation <ADXAnnotationProtocol>
@property (strong, nonatomic) ADXNoteAnnotation *noteAnnotation;
@property (strong, nonatomic) NSString *noteLocalID;
@property (strong, nonatomic) NSString *noteID;
@property (strong, nonatomic) NSString *documentID;
@property (strong, nonatomic) NSNumber *versionID;
@property (strong, nonatomic) NSString *accountID;
+ (ADXHighlightAnnotation *)highlightAnnotation:(CGRect)boundingBox page:(NSUInteger)page note:(ADXDocumentNote *)note document:(ADXDocument *)document pdf:(PSPDFDocument *)pdf;
- (ADXNoteAnnotation *)addEmptyNoteAnnotation;

+ (void)updateHighlightAnnotation:(ADXHighlightAnnotation *)highlightAnnotation
                         withPage:(NSUInteger)page
                      boundingBox:(CGRect)boundingBox
                             note:(ADXDocumentNote *)note
                         document:(ADXDocument *)adxDocument
                              pdf:(PSPDFDocument *)pdf;

@end
