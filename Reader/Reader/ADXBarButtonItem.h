//
//  ADXBarButtonItem.h
//  Reader
//
//  Created by Steve Tibbett on 2013-04-22.
//  Copyright (c) 2013 ActivDox Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ADXBarButtonItem : UIBarButtonItem

+ (ADXBarButtonItem *)itemWithImageNamed:(NSString *)imageName target:(id)target selector:(SEL)selector;
+ (ADXBarButtonItem *)itemWithLabelText:(NSString *)text target:(id)target selector:(SEL)selector;

@end
