//
//  ADXCollectionListCellSmall.m
//  Reader
//
//  Created by Gavin McKenzie on 2013-04-28.
//  Copyright (c) 2013 ActivDox Inc. All rights reserved.
//

#import "ADXCollectionListCellSmall.h"

@implementation ADXCollectionListCellSmall

- (void)updateBadge
{
    [super updateBadge];

    if (self.documentNewBadge.hidden && self.changeBadge.hidden) {
        self.titleLeftConstraint.constant = 120.0f;
    } else {
        if (self.changeBadge.hidden) {
            self.titleLeftConstraint.constant = 147;
        } else {
            self.titleLeftConstraint.constant = 144 + [self.changeBadge.titleLabel.text sizeWithFont:self.changeBadge.titleLabel.font].width;
        }
    }
}

@end
