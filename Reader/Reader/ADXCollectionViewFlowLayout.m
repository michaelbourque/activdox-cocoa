//
//  ADXCollectionViewFlowLayout.m
//  Reader
//
//  Created by Gavin McKenzie on 2013-04-28.
//  Copyright (c) 2013 ActivDox Inc. All rights reserved.
//

#import "ADXCollectionViewFlowLayout.h"

@implementation ADXCollectionViewFlowLayout

//- (CGPoint)targetContentOffsetForProposedContentOffset:(CGPoint)proposedContentOffset withScrollingVelocity:(CGPoint)velocity
//{
//    CGFloat offsetAdjustment = MAXFLOAT;
//    CGFloat targetY = proposedContentOffset.y + self.minimumLineSpacing + self.sectionInset.top;
////    CGFloat verticalCenter = proposedContentOffset.y + (CGRectGetHeight(self.collectionView.bounds) / 2.0);
//    
//    CGRect targetRect = CGRectMake(0.0, proposedContentOffset.y, self.collectionView.bounds.size.width, self.collectionView.bounds.size.height);
//    NSArray *array = [super layoutAttributesForElementsInRect:targetRect];
//    
//    for (UICollectionViewLayoutAttributes* layoutAttributes in array) {
//        CGFloat itemY = layoutAttributes.frame.origin.y;
////        CGFloat itemVerticalCenter = layoutAttributes.center.y;
////        
////        if (ABS(itemVerticalCenter - verticalCenter) < ABS(offsetAdjustment)) {
////            offsetAdjustment = itemVerticalCenter - verticalCenter;
////        }
//        if (ABS(itemY - targetY) < ABS(offsetAdjustment)) {
//            offsetAdjustment = itemY - targetY;
//        }
//    }
//    return CGPointMake(proposedContentOffset.x, proposedContentOffset.y + offsetAdjustment);
//}

@end
