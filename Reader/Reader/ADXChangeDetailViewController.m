//
//  ADXChangeDetailViewController.m
//  Reader
//
//  Created by Matteo Ugolini on 2012-11-06.
//  Copyright (c) 2012 ActivDox Inc. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>

#import "ADXChangeDetailViewController.h"
#import "ADXAppDelegate.h"

@interface ADXChangeDetailViewController ()

@property (nonatomic, weak) IBOutlet UIButton* closeButton;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (nonatomic, weak) IBOutlet UITextView *descriptionView;

@end

@implementation ADXChangeDetailViewController

- (id)init
{
    NSString *ii = UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ? @"Phone" : @"Pad";
    self = [super initWithNibName:[NSString stringWithFormat:@"ADXChangeDetailViewController-%@", ii] bundle:nil];
    if (self) {
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if (self.closeButton){
        self.closeButton.layer.cornerRadius = self.closeButton.frame.size.width / 2;
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self.descriptionView flashScrollIndicators];
}

-(IBAction)done:(id)sender
{
    [self.parentViewController dismissViewControllerAnimated:YES completion:NULL];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)showDescriptionText:(NSString *)text
{
    self.imageView.hidden = YES;
    self.descriptionView.hidden = NO;
    self.descriptionView.text = text;
}

- (void)showChangeImage:(UIImage *)image
{
    self.imageView.hidden = NO;
    self.descriptionView.hidden = YES;
    self.imageView.contentMode = UIViewContentModeScaleAspectFit;
    self.imageView.image = image;
}

@end
