//
//  ADXLinkedAnnotation.m
//  Reader
//
//  Created by Matteo Ugolini on 2012-09-25.
//  Copyright (c) 2012 ActivDox Inc. All rights reserved.
//

#import "ADXHighlightAnnotation.h"
#import "ADXNoteAnnotation.h"

@interface ADXHighlightAnnotation ()
@end

@implementation ADXHighlightAnnotation

#pragma mark - Class Methods

+ (ADXHighlightAnnotation *)highlightAnnotation:(CGRect)boundingBox page:(NSUInteger)page note:(ADXDocumentNote *)note document:(ADXDocument *)document pdf:(PSPDFDocument *)pdf
{
    ADXHighlightAnnotation *highlightAnnotation = [[ADXHighlightAnnotation alloc] init];
    
    [ADXHighlightAnnotation updateHighlightAnnotation:highlightAnnotation
                                             withPage:page
                                          boundingBox:boundingBox
                                                 note:note
                                             document:document
                                                  pdf:pdf];
    
    if (note.text) { // We don't care if the text is a zero-length string.
        [highlightAnnotation addEmptyNoteAnnotation];
    }
    
    return highlightAnnotation;
}

+ (void)updateHighlightAnnotation:(ADXHighlightAnnotation *)highlightAnnotation
                         withPage:(NSUInteger)page
                      boundingBox:(CGRect)boundingBox
                             note:(ADXDocumentNote *)note
                         document:(ADXDocument *)adxDocument
                              pdf:(PSPDFDocument *)pdf
{
    highlightAnnotation.document = pdf;
    highlightAnnotation.boundingBox = boundingBox;
    highlightAnnotation.page = page;
    highlightAnnotation.documentID = adxDocument.id;
    highlightAnnotation.versionID = adxDocument.version;
    highlightAnnotation.noteLocalID = note.localID;
    highlightAnnotation.noteID = note.id;
    highlightAnnotation.accountID = note.accountID;
}

#pragma mark - Helpers

- (ADXNoteAnnotation *)addEmptyNoteAnnotation
{
    if (self.noteAnnotation) {
        LogError(@"cannot addEmptyNoteAnnotation for %@; it already exists", self.noteLocalID);
        return nil;
    }
    
    ADXDocumentNote *note = [ADXAnnotationUtil noteFromAnnotation:self];
    ADXDocument *document = [ADXAnnotationUtil documentFromAnnotation:self];
    ADXNoteAnnotation *noteAnnotation = [ADXNoteAnnotation noteAnnotationOverHighlight:self note:note document:document pdf:self.document];
    self.noteAnnotation = noteAnnotation;

    if (!note.text) {
        note.text = @"";
    }
    
    return self.noteAnnotation;
}

#pragma mark - NSCopying

- (id)copyWithZone:(NSZone *)zone {
    ADXHighlightAnnotation *annotation = (ADXHighlightAnnotation *)[super copyWithZone:zone];

    annotation.noteAnnotation = self.noteAnnotation;
    annotation.noteLocalID = self.noteLocalID;
    annotation.noteID = self.noteID;
    annotation.documentID = self.documentID;
    annotation.versionID = self.versionID;
    annotation.accountID = self.accountID;
    
    return annotation;
}

#pragma mark - Accessors

- (void)setDeleted:(BOOL)deleted
{
    [super setDeleted:deleted];
}

- (void)setColor:(UIColor *)color
{
    [super setColor:color];
    if (self.noteAnnotation) {
        [self.noteAnnotation setColor:color];
    }
}

@end
