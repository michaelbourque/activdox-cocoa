//
//  ADXNotificationBubbleContainerView.h
//  Reader
//
//  Created by Gavin McKenzie on 2013-03-27.
//  Copyright (c) 2013 ActivDox Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ADXNotificationBubbleContainerView : UIView

@end
