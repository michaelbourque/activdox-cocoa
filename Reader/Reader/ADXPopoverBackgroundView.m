//
//  ADXPopoverBackgroundView.m
//  Reader
//
//  Created by Steve Tibbett on 2013-04-17.
//  Copyright (c) 2013 ActivDox Inc. All rights reserved.
//

#import "ADXPopoverBackgroundView.h"
#import <QuartzCore/QuartzCore.h>


@interface ADXPopoverBackgroundView()
@property (strong, nonatomic) UIImageView *titlebarView;
@property (strong, nonatomic) UIImageView *arrowView;
@property (strong, nonatomic) UIImageView *bottomView;
@end


@implementation ADXBluePopoverBackgroundView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.titlebarView.image = [[UIImage imageNamed:@"blue_popover_titlebar"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 16, 0, 16)];
        self.arrowView.image = [UIImage imageNamed:@"blue_popover_arrow"];
        self.bottomView.image = [[UIImage imageNamed:@"blue_popover_bottom"] resizableImageWithCapInsets:UIEdgeInsetsMake(1, 16, 10, 16)];
    }
    return self;
}

@end


#define POPOVER_TITLE_HEIGHT 46

@implementation ADXPopoverBackgroundView
@synthesize arrowDirection = _arrowDirection;
@synthesize arrowOffset = _arrowOffset;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        self.opaque = NO;
        self.layer.needsDisplayOnBoundsChange = YES;
        
        UIImage *titlebar = [[UIImage imageNamed:@"popover_titlebar"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 16, 0, 16)];
        self.titlebarView = [[UIImageView alloc] initWithImage:titlebar];
        [self addSubview:self.titlebarView];
        
        UIImage *bottom = [[UIImage imageNamed:@"popover_bottom"] resizableImageWithCapInsets:UIEdgeInsetsMake(1, 16, 10, 16)];
        self.bottomView = [[UIImageView alloc] initWithImage:bottom];
        [self addSubview:self.bottomView];
        self.bottomView.autoresizingMask = UIViewAutoresizingFlexibleHeight;

        UIImage *arrow = [UIImage imageNamed:@"popover_arrow"];
        self.arrowView = [[UIImageView alloc] initWithImage:arrow];
        [self addSubview:self.arrowView];

        self.opaque = YES;
        
    }
    return self;
}

+ (UIEdgeInsets)contentViewInsets
{
    return UIEdgeInsetsMake(9, 1, 9, 1);
}

- (void)setArrowDirection:(UIPopoverArrowDirection)arrowDirection
{
    _arrowDirection = arrowDirection;
    [self setNeedsLayout];
}

- (CGFloat)arrowOffset
{
    return _arrowOffset;
}

- (void)setArrowOffset:(CGFloat)arrowOffset
{
    _arrowOffset = arrowOffset;
    
    CGPoint center = self.arrowView.center;
    center.x = self.bounds.size.width / 2 + arrowOffset;
    self.arrowView.center = center;
    [self setNeedsLayout];
}

+ (CGFloat)arrowBase
{
    return 36;
}

+ (CGFloat)arrowHeight
{
    return 18;
}

+ (BOOL)wantsDefaultContentAppearance
{
    return NO;
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    
    CGFloat height = self.frame.size.height;
    CGFloat width = self.frame.size.width;
    CGFloat left = 0.0;
    CGFloat top = 0.0;
    CGFloat coordinate = 0.0;
    CGAffineTransform rotation = CGAffineTransformIdentity;

    float arrowBase = [ADXPopoverBackgroundView arrowBase];
    float arrowHeight = [ADXPopoverBackgroundView arrowHeight];
    
    switch (self.arrowDirection) {
        case UIPopoverArrowDirectionAny:
        case UIPopoverArrowDirectionUnknown:
            NSAssert(NO, @"Any should have resolved to an actual direction by now");
            break;
            
        case UIPopoverArrowDirectionUp:
            top += (arrowHeight-1);
            height -= arrowHeight;
            coordinate = ((self.frame.size.width / 2) + self.arrowOffset) - (arrowBase/2);
            coordinate = MAX(5, coordinate);
            coordinate = MIN(self.frame.size.width - (arrowBase + 4), coordinate);
            self.arrowView.frame = CGRectMake(coordinate, 1, arrowBase, arrowHeight);
            break;
            
        case UIPopoverArrowDirectionDown:
            height -= arrowHeight;
            coordinate = ((self.frame.size.width / 2) + self.arrowOffset) - (arrowBase/2);
            self.arrowView.frame = CGRectMake(coordinate, height-1, arrowBase, arrowHeight);
            rotation = CGAffineTransformMakeRotation( M_PI );
            break;
            
        case UIPopoverArrowDirectionLeft:
            width -= arrowHeight;
            left += 18;
            coordinate = ((self.frame.size.height / 2) + self.arrowOffset) - (arrowHeight/2);
            self.arrowView.frame = CGRectMake(-8, coordinate, arrowBase, arrowHeight);
            rotation = CGAffineTransformMakeRotation( -M_PI_2 );
            break;
            
        case UIPopoverArrowDirectionRight:
            width -= arrowHeight;
            coordinate = ((self.frame.size.height / 2) + self.arrowOffset)- (arrowHeight/2);
            self.arrowView.frame = CGRectMake(width-10, coordinate, arrowBase, arrowHeight);
            rotation = CGAffineTransformMakeRotation( M_PI_2 );
            break;
    }
    

    self.titlebarView.frame = CGRectMake(left, top, width, POPOVER_TITLE_HEIGHT);
    float bottomTop = self.titlebarView.frame.origin.y + self.titlebarView.frame.size.height;
    self.bottomView.frame = CGRectMake(left, bottomTop, width, height+top-bottomTop);

    [self.arrowView setTransform:rotation];
    [self bringSubviewToFront:self.arrowView];
}

@end
