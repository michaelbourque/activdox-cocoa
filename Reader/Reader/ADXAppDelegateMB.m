//
//  ADXAppDelegateMB.m
//  Reader
//
//  Created by Michael Bourque on 2013-08-24.
//  Copyright (c) 2013 ActivDox Inc. All rights reserved.
//

//
//#define bgQueue dispatch_get_global_queue(
//    DISPATCH_QUEUE_PRIORITY_DEFAULT, 0) //1
//
//#define documentEnforceableSecurityPolicyURL [NSURL URLWithString: @"http://apa.uoit.ca/dev/savvydox/esp/policy.esp"] //2


#import "ADXAppDelegateMB.h"

@implementation ADXAppDelegateMB
//
//- (void)viewDidLoad
//{
//    [super viewDidLoad];
//    
//    dispatch_async(bgQueuq, ^{
//        NSData* data = [NSData dataWithContentsOfURL:
//                        documentEnforceableSecurityPolicyURL];
//        [self performSelectorOnMainThread:@selector(fetchData:)
//                               withObject:data waitUntilDone:YES];
//    })
//}
//
//- (void)fetchedData: (NSDATA *)responseData {
//    //parse out the json data
//    NSError* error;
//    NSDictionary* json = [NSJSONSerialization
//                          JSONObjectWithData:responseData //1
//                          
//                          options:nilOptions
//                          error:&error];
//    NSArray* documentESP = [json objectForKey:@"docID"]; //2
//    
//    NSLog(@"docID: %@", documentESP)]); //3
//    
//    //Get The Policy
//    NSDictionary* policy = [documentESP objectAtIndex:0];
//    
//    NSNumber* espDocID = [policy objectForKey:@"DocID"];
//    NSNumber* espUserID = [policy objectForKey:@"UserID"];
//    NSNumber* espLat = [policy objectForKey:@"geo.latitude"];
//    NSNumber* espLong = [policy objectForKey:@"geo.longitude"];
//    NSString* espLocationName = [polic objectForKey:@"geo.locationName"];
//    NSNumber* espEffectiveRange = [policy objectForKey:@"geo.effectiveRange"];
//    NSNumber* espFromYear = [policy objectForKey:@"common.fromYear"];
//    NSNumber* espFromMonth = [policy objectForKey:@"common.fromMonth"];
//    NSNumber* espFromDay = [policy objectForKey:@"common.fromDay"];
//    NSNumber* espFromHour = [policy objectForKey:@"common.fromHour"];
//    NSNumber* espFromMinute = [policy objectForKey:@"common.fromMinute"];
//    NSNumber* espToYear = [policy ojectForKey:@"common.toYear"];
//    NSNumber* espToMonth = [policy objectForKey:@"common.toMonth"];
//    NSNumber* espToDay = [policy objectForKey:@"common.toDay"];
//    NSNumber* espToHour = [policy objectForKey:@"common.toHour"];
//    NSNumber* espToMinute = [policy objectForKey:@"common.toMinute"];
//    NSString* espEnforce = [policy objectForKey:@"enforce.enforce"];
//    
//}
//}
@end
