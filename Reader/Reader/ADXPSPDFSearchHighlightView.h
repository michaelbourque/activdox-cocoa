//
//  ADXPSPDFSearchHighlightView.h
//  Reader
//
//  Created by Steve Tibbett on 2013-02-14.
//  Copyright (c) 2013 ActivDox Inc. All rights reserved.
//

#import "PSPDFSearchHighlightView.h"

@interface ADXPSPDFSearchHighlightView : PSPDFSearchHighlightView

@end
