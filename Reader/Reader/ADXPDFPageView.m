//
//  ADXPDFPageView.m
//  Reader
//
//  Created by Matteo Ugolini on 2012-10-01.
//  Copyright (c) 2012 ActivDox Inc. All rights reserved.
//

#import "ADXPDFPageView.h"
#import "ADXAppDelegate.h"
#import "ADXNoteAnnotation.h"
#import "ADXHighlightAnnotation.h"

#import "PSPDFDocument.h"
#import "PSPDFMenuItem.h"
#import "PSPDFNoteAnnotationController.h"
#import "ADXPDFViewController.h"

@interface PSPDFPageView () <PSPDFNoteAnnotationControllerDelegate>
@end

@implementation ADXPDFPageView

#pragma mark - Menus

- (NSArray *)menuItemsForAnnotation:(PSPDFAnnotation *)annotation
{
    NSMutableArray *newMenuItems = [NSMutableArray array];
    NSArray *menuItems = [super menuItemsForAnnotation:annotation];
    for (PSPDFMenuItem *menuItem in menuItems)
    {
        if ([menuItem respondsToSelector:@selector(identifier)] && [PSPDFLocalize(@"Remove") isEqualToString: menuItem.identifier])
        {
            if ([annotation conformsToProtocol:@protocol(ADXAnnotationProtocol)] &&
                [ADXAnnotationUtil currentUserIsAuthorOfAnnotation:(id<ADXAnnotationProtocol>)annotation]) {

                __weak ADXPDFPageView *weakSelf = self;
                menuItem.block = ^{
                    [weakSelf deleteAnnotation:annotation];
                    [weakSelf saveNoteForAnnotation:annotation withOptionalSync:YES];
                    [weakSelf updateView];
                };
                
                menuItem.title = NSLocalizedString(@"Delete", nil);
                [newMenuItems addObject:menuItem];
                break;
            }
        }
    }
    
    if (!annotation.isDeleted && [self.document.editableAnnotationTypes containsObject:annotation.typeString]) {
        if (annotation.type == PSPDFAnnotationTypeHighlight && [annotation isMemberOfClass:[ADXHighlightAnnotation class]]) {
            if (annotation.isEditable) {
                ADXHighlightAnnotation *highlightAnnotation = (ADXHighlightAnnotation *)annotation;
                ADXDocumentNote *note = [ADXAnnotationUtil noteFromAnnotation:highlightAnnotation];
                if (!note) {
                    LogError(@"Couldn't retrieve note associated with annotation %@", annotation);
                } else {
                    //
                    // Mark Resolved / Mark Unresolved
                    
                    if (highlightAnnotation.noteAnnotation) {
                        BOOL userIsAuthorOfNote = [ADXAnnotationUtil currentUserIsAuthorOfAnnotation:(id<ADXAnnotationProtocol>)annotation];
                        BOOL userIsAuthorOfDocument = [ADXAnnotationUtil currentUserIsAuthorOfDocumentForAnnotation:(id<ADXAnnotationProtocol>)annotation];
                        
                        if (userIsAuthorOfNote || userIsAuthorOfDocument) {
                            NSString *buttonLabel = nil;
                            if (note.isClosed) {
                                buttonLabel = @"Mark Unresolved";
                            } else {
                                buttonLabel = @"Mark Resolved";
                            }
                            
                            __weak ADXPDFPageView *weakSelf = self;
                            PSPDFMenuItem *resolveItem = [[PSPDFMenuItem alloc] initWithTitle:buttonLabel block:^{
                                [weakSelf toggleAnnotationResolved:(id<ADXAnnotationProtocol>)annotation];
                            } identifier:@"Toggle Resolved"];
                            
                            [newMenuItems addObject:resolveItem];
                        }
                    }
                    
                    //
                    // Attach Note to highlight
                    
                    if (!highlightAnnotation.noteAnnotation) {
                        PSPDFMenuItem *resolveItem = [[PSPDFMenuItem alloc] initWithTitle:@"Attach Note" block:^{
                            
                            if (![ADXUserDefaults sharedDefaults].annotationFilterShowNotes ||
                                ![ADXUserDefaults sharedDefaults].annotationFilterIncludeMyNotes) {
                                // The note the user is about to add would not be visible.  Prompt whether to turn notes on or cancel.
                                PSPDFAlertView *alertView = [[PSPDFAlertView alloc] initWithTitle:NSLocalizedString(@"Notes Filtered", nil)
                                                                                          message:NSLocalizedString(@"Notes you create are currently hidden.  You must show notes before you can turn this highlight into one.", nil)];
                                [alertView addButtonWithTitle:NSLocalizedString(@"Show Notes", nil)
                                                        block:^{
                                                            [ADXUserDefaults sharedDefaults].annotationFilterShowNotes = YES;
                                                            [ADXUserDefaults sharedDefaults].annotationFilterIncludeMyNotes = YES;
                                                            
                                                            ADXPDFViewController *pdfController = (ADXPDFViewController *)self.pdfController;
                                                            [pdfController refreshAnnotations];
                                                            
                                                            ADXNoteAnnotation *noteAnnotation = [highlightAnnotation addEmptyNoteAnnotation];
                                                            [self.document addAnnotations:@[noteAnnotation] forPage:self.page];
                                                            [self addAnnotation:noteAnnotation animated:NO];
                                                            [self showNoteControllerForAnnotation:noteAnnotation showKeyboard:YES animated:YES];
                                                            [self saveNoteForAnnotation:noteAnnotation withOptionalSync:NO];
                                                        }];
                                [alertView addButtonWithTitle:@"Cancel"];
                                [alertView show];
                                
                            } else {
                                ADXNoteAnnotation *noteAnnotation = [highlightAnnotation addEmptyNoteAnnotation];
                                [self.document addAnnotations:@[noteAnnotation] forPage:self.page];
                                [self addAnnotation:noteAnnotation animated:NO];
                                [self showNoteControllerForAnnotation:noteAnnotation showKeyboard:YES animated:YES];
                                [self saveNoteForAnnotation:noteAnnotation withOptionalSync:NO];
                            }
                        } identifier:@"Attach Note"];
                        
                        [newMenuItems addObject:resolveItem];
                    }
                }
            }
        }
    }

    return newMenuItems;
}

#pragma mark - Annotation Operations

- (void)saveNoteForAnnotation:(PSPDFAnnotation*)annotation withOptionalSync:(BOOL)sync
{
    ADXDocumentNote *note = [ADXAnnotationUtil noteFromAnnotation:(id<ADXAnnotationProtocol>)annotation];
    [note saveWithOptionalSync:sync completion:^(BOOL success, NSError *error) {
        if (!success) {
            LogNSError(@"error saving annotation", error);
        }
    }];
}

- (void)deleteAnnotation:(PSPDFAnnotation*)annotation
{
    if (![annotation conformsToProtocol:@protocol(ADXAnnotationProtocol)]) {
        LogError(@"received annotation of incorrect class %@", annotation);
        return;
    }
    
    ADXDocumentNote *note = [ADXAnnotationUtil noteFromAnnotation:(id<ADXAnnotationProtocol>)annotation];
    if (!note) {
        LogError(@"Couldn't retrieve note associated with annotation %@", annotation);
        return;
    }
    
    ADXNoteAnnotation *noteAnnotation = nil;
    ADXHighlightAnnotation *highlightAnnotation = nil;
    if ([annotation isKindOfClass:[ADXHighlightAnnotation class]]) {
        highlightAnnotation = (ADXHighlightAnnotation *)annotation;
        noteAnnotation = highlightAnnotation.noteAnnotation;
    } else {
        noteAnnotation = (ADXNoteAnnotation *)annotation;
        highlightAnnotation = noteAnnotation.highlightAnnotation;
    }

    [highlightAnnotation setDeleted:YES];
    [noteAnnotation setDeleted:YES];

    [[NSNotificationCenter defaultCenter] postNotificationName:PSPDFAnnotationChangedNotification
                                                        object:annotation
                                                      userInfo:@{PSPDFAnnotationChangedNotificationKeyPathKey : @[NSStringFromSelector(@selector(isDeleted))], PSPDFAnnotationChangedNotificationOriginalAnnotationKey : annotation}];

    [note markDeleted];
    
    [note saveWithOptionalSync:YES completion:^(BOOL success, NSError *error) {
        if (!success) {
            LogNSError(@"Failed to save note after attempting to delete.", error);
        } else {
            LogTrace(@"Deleted note %@", note);
        }
    }];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kADXDocumentNoteDidChangeNotification object:annotation];
}

- (void)toggleAnnotationResolved:(id<ADXAnnotationProtocol>)annotation
{
    if (![annotation conformsToProtocol:@protocol(ADXAnnotationProtocol)]) {
        LogError(@"received annotation of incorrect class %@", annotation);
        return;
    }

    ADXDocumentNote *note = [ADXAnnotationUtil noteFromAnnotation:annotation];
    
    if (note.isClosed) {
        [note unsetClosed];
    } else {
        note.closeVersion = annotation.versionID;
    }

    PSPDFAnnotation *newAnnotation = [(PSPDFAnnotation *)annotation copyAndDeleteOriginalIfNeeded];
    newAnnotation.color = note.isClosed ? ADXNoteAnnotationClosedColor : ADXNoteAnnotationOpenColor;
    [[NSNotificationCenter defaultCenter] postNotificationName:PSPDFAnnotationChangedNotification object:newAnnotation userInfo:@{PSPDFAnnotationChangedNotificationKeyPathKey : @[NSStringFromSelector(@selector(color))], PSPDFAnnotationChangedNotificationOriginalAnnotationKey : annotation}];
    
    if ([annotation isKindOfClass:[ADXNoteAnnotation class]] && ((ADXNoteAnnotation *)annotation).highlightAnnotation) {
        ADXHighlightAnnotation *highlightAnnotation = ((ADXNoteAnnotation *)annotation).highlightAnnotation;
        highlightAnnotation.color = [ADXAnnotationUtil colorForAnnotation:annotation note:note];
        [[NSNotificationCenter defaultCenter] postNotificationName:PSPDFAnnotationChangedNotification object:newAnnotation userInfo:@{PSPDFAnnotationChangedNotificationKeyPathKey : @[NSStringFromSelector(@selector(color))], PSPDFAnnotationChangedNotificationOriginalAnnotationKey : annotation}];
    }

    [self updateView];
    
    [note saveWithOptionalSync:YES completion:^(BOOL success, NSError *error) {
        if (!success) {
            LogNSError(@"Failed to save note after attempting to mark resolved.", error);
        } else {
            LogTrace(@"Resolved note with closeVersion of %@", note.closeVersion);
        }
    }];

    [[NSNotificationCenter defaultCenter] postNotificationName:kADXDocumentNoteDidChangeNotification object:annotation];
}

- (void)noteAnnotationController:(PSPDFNoteAnnotationController *)noteAnnotationController didDeleteAnnotation:(PSPDFNoteAnnotation *)annotation
{
    if (![annotation conformsToProtocol:@protocol(ADXAnnotationProtocol)]) {
        LogError(@"received annotation of incorrect class %@", annotation);
        return;
    }

    [super noteAnnotationController:noteAnnotationController didDeleteAnnotation:annotation];
    [self deleteAnnotation:annotation];
    [self updateView];
}

- (void)noteAnnotationController:(PSPDFNoteAnnotationController *)noteAnnotationController didChangeAnnotation:(PSPDFNoteAnnotation *)annotation originalAnnotation:(PSPDFNoteAnnotation *)originalAnnotation
{
    if (![annotation conformsToProtocol:@protocol(ADXAnnotationProtocol)] || ![originalAnnotation conformsToProtocol:@protocol(ADXAnnotationProtocol)]) {
        LogError(@"received annotation of incorrect class %@", annotation);
        return;
    }
}

- (void)noteAnnotationController:(PSPDFNoteAnnotationController *)noteAnnotationController willDismissWithAnnotation:(PSPDFAnnotation *)annotation
{
    if (![annotation conformsToProtocol:@protocol(ADXAnnotationProtocol)]) {
        LogError(@"received annotation of incorrect class %@", annotation);
        return;
    }

    if ([ADXAnnotationUtil currentUserIsAuthorOfAnnotation:(id<ADXAnnotationProtocol>)annotation]) {
        ADXDocumentNote *note = [ADXAnnotationUtil noteFromAnnotation:(id<ADXAnnotationProtocol>)annotation];
        ADXNoteAnnotation *noteAnnotation = (ADXNoteAnnotation *)annotation;
        
        NSString *originalText = note.text;
        NSString *originalVisibility = note.visibility;
        NSString *newText = annotation.contents;
        NSString *newVisibility = noteAnnotation.noteVisibility;
        
        if (![originalText isEqualToString:newText] || ![originalVisibility isEqualToString:newVisibility]) {
            note.visibility = newVisibility;
            note.text = newText;
            
            [note saveWithOptionalSync:YES completion:^(BOOL success, NSError *error) {
                if (!success) {
                    LogError(@"error saving the annotation");
                }
            }];
        }
    }
}

- (void)noteAnnotationController:(PSPDFNoteAnnotationController *)noteAnnotationController didToggleAnnotationResolved:(PSPDFAnnotation *)annotation
{
    if (![annotation conformsToProtocol:@protocol(ADXAnnotationProtocol)]) {
        LogError(@"received annotation of incorrect class %@", annotation);
        return;
    }

    [self toggleAnnotationResolved:(id<ADXAnnotationProtocol>)annotation];
}

- (void)updatePageAnnotationView:(UIView<PSPDFAnnotationView> *)annotationView usingBlock:(void (^)(PSPDFAnnotation *annotation))block
{
    [super updatePageAnnotationView:annotationView usingBlock:block];
    
    if ([annotationView.annotation.class isSubclassOfClass:[ADXNoteAnnotation class]]) {
        // The user moved a note.  Update the associated entity.
        ADXNoteAnnotation *noteAnnotation = (ADXNoteAnnotation *)annotationView.annotation;
        ADXDocumentNote *note = [ADXAnnotationUtil noteFromAnnotation:(id<ADXAnnotationProtocol>)noteAnnotation];
        
        CGPoint point = [self convertViewPointToPDFPoint:CGPointMake(annotationView.center.x, annotationView.frame.origin.y + annotationView.frame.size.height)];
        [note moveNoteToPoint:point inVersion:noteAnnotation.versionID];
        [note saveWithOptionalSync:YES completion:^(BOOL success, NSError *error) {
            if (!success) {
                LogError(@"error saving the note annotation %@");
            }
        }];
    }
}

@end
