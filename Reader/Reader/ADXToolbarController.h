//
//  ADXToolbarController.h
//  bottomBar
//
//  Created by matteo ugolini on 2012-10-13.
//  Copyright (c) 2012 matteo ugolini. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ADXToolbar.h"

@protocol ADXToolbarControllerDelegate;

@interface ADXToolbarController : UIViewController
@property (nonatomic, copy)     NSArray             *viewControllers;
@property (nonatomic, strong)   UIViewController    *selectedViewController;
@property (nonatomic, assign)   NSUInteger          selectedIndex;
@property (nonatomic, strong, readonly) ADXToolbar          *toolBar;
@property (nonatomic, weak)     id<ADXToolbarControllerDelegate> delegate;

- (void)setViewControllers:(NSArray *)viewControllers animated:(BOOL)animated;
- (void)setSelectedViewController:(UIViewController *)selectedViewController;
- (void)setSelectedIndex:(NSUInteger)selectedIndex;

@end

@protocol ADXToolbarControllerDelegate <NSObject>
@optional
- (BOOL)toolBarController:(ADXToolbarController *)toolBarController shouldSelectViewController:(UIViewController *)viewController;
- (void)toolBarController:(ADXToolbarController *)toolBarController didSelectViewController:(UIViewController *)viewController;
- (void)toolBarController:(ADXToolbarController *)toolBarController didDeselectViewController:(UIViewController *)viewController;
@end
