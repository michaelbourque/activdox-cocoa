//
//  ADXAppDelegate+LocalStore.m
//  Reader
//
//  Created by Gavin McKenzie on 2013-04-22.
//  Copyright (c) 2013 ActivDox Inc. All rights reserved.
//

#import "ADXAppDelegate+LocalStore.h"
#import "PSPDFKit.h"

NSString * const kADXCollectionUntitled = @"Untitled";

@implementation ADXAppDelegate (LocalStore)

- (void)saveMainContext:(ADXCompletionBlock)completionBlock
{
    __weak NSManagedObjectContext *moc = self.managedObjectContext;
    
    [moc performBlock:^{
        NSError *saveError = nil;
        BOOL saveSuccess = [moc save:&saveError];
        if (!saveSuccess) {
            LogNSError(@"couldn't save main service context", saveError);
        }
        if (completionBlock) {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                completionBlock(saveSuccess, saveError);
            });
        }
    }];
}

#pragma mark - PDF Resource

- (void)pdfResourceForDocument:(NSString *)documentID versionID:(NSNumber *)versionID downloadContent:(BOOL)downloadContent service:(ADXV2Service *)service downloadWillStart:(ADXPDFResourceDownloadingBlock)startBlock completion:(ADXResourceCompletionBlock)completionBlock
{
    __weak ADXAppDelegate *weakSelf = self;
    __weak NSManagedObjectContext *moc = self.managedObjectContext;
    
    [moc performBlock:^{
        ADXDocument *document = [ADXDocument documentWithID:documentID version:versionID accountID:service.account.id context:moc];
        if (!document) {
            if (completionBlock) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    completionBlock(nil, NO, nil);
                });
                return;
            }
        }
        
        [weakSelf pdfResourceForDocument:document downloadContent:downloadContent service:service downloadWillStart:startBlock completion:completionBlock];
    }];
}

- (void)pdfResourceForDocument:(ADXDocument *)document downloadContent:(BOOL)downloadContent service:(ADXV2Service *)service downloadWillStart:(ADXPDFResourceDownloadingBlock)startBlock completion:(ADXResourceCompletionBlock)completionBlock
{
    __weak ADXAppDelegate *weakSelf = self;
    __weak NSManagedObjectContext *moc = self.managedObjectContext;
    
    [moc performBlock:^{
        ADXDocumentContent *content = document.content;
        PSPDFDocument *pdfDocument = nil;
        if (content.data && [content.data length]) {
            pdfDocument = [PSPDFDocument PDFDocumentWithData:content.data];
        }
        
        // Either we didn't have the document content downloaded or the attempt to instantiate a PSPDFDocument failed.
        if (!pdfDocument) {
            // If we were not asked to download content, just let the caller know that the resource isn't available.
            if (!downloadContent) {
                if (completionBlock) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        completionBlock(nil, NO, nil);
                    });
                }
                return;
            }
            
            // We were asked to download the content, so let the caller know we are about to download content
            if (startBlock) {
                startBlock();
            }
            
            // Check to see if the document was excluded from automatic download.
            // If it was, then flag it for automatic download from here on.
            ADXMetaDocument *metaDocument = document.metaDocument;
            if (metaDocument.doNotAutomaticallyDownloadContentValue) {
                metaDocument.doNotAutomaticallyDownloadContent = @NO;
            }
            
            // Do the download
            [service forceDownloadContentForDocument:document.id versionID:document.version trackProgress:YES completion:^(BOOL success, NSError *error) {
                if (success) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [weakSelf pdfResourceForDocument:document downloadContent:NO service:service downloadWillStart:nil completion:completionBlock];
                    });
                } else {
                    if (completionBlock) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            completionBlock(nil, NO, error);
                        });
                    }
                }
            }];
            return;
        } else {
            if (completionBlock) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    completionBlock(pdfDocument, YES, nil);
                });
            }
            return;
        }
    }];
}

#pragma mark - Collections

- (void)insertUserCollection:(ADXResourceCompletionBlock)completionBlock
{
    ADXCollection *collection = [ADXCollection insertCollectionForAccountID:self.currentAccount.id type:ADXCollectionType.user context:self.managedObjectContext];
    if (collection) {
        collection.title = kADXCollectionUntitled;
        
        [collection saveWithOptionalSync:YES completion:^(BOOL success, NSError *error) {
            if (completionBlock) {
                [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                    completionBlock(collection, success, error);
                }];
            }
        }];
    } else {
        if (completionBlock) {
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                completionBlock(nil, NO, nil);
            }];
        }
    }
}

- (void)deleteCollection:(NSString *)collectionID localID:(NSString *)localID completion:(ADXCompletionBlock)completionBlock
{
    ADXCollection *collection = [ADXCollection collectionWithID:collectionID orLocalID:localID accountID:self.currentAccount.id context:self.managedObjectContext];
    if (collection) {
        [collection markDeleted];
        [collection saveWithOptionalSync:YES completion:^(BOOL success, NSError *error) {
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                completionBlock(success, error);
            }];
        }];
    } else {
        if (completionBlock) {
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                completionBlock(NO, nil);
            }];
        }
    }
}

- (void)addDocument:(NSString *)documentID toCollection:(NSString *)collectionID localID:(NSString *)localID completion:(ADXServiceCompletionBlock)completionBlock
{
    __weak NSManagedObjectContext *moc = self.managedObjectContext;

    NSString *accountID = self.currentAccount.id;
    ADXCollection *collection = [ADXCollection collectionWithID:collectionID orLocalID:localID accountID:accountID context:self.managedObjectContext];
    ADXDocument *document = [ADXDocument latestDocumentWithID:documentID accountID:accountID context:self.managedObjectContext];
    
    if (collection && document) {
        [document.collectionsSet addObject:collection];
        [self saveMainContext:^(BOOL success, NSError *error) {
            ADXSyncQueueEntry *syncEntry = nil;
            NSDictionary *userInfo = @{ADXCollectionAttributes.id: (collectionID ? collectionID : [NSNull null]), ADXCollectionAttributes.localID: (localID ? localID : [NSNull null])};
            syncEntry = [ADXSyncQueueEntry insertEntryForEntity:document id:documentID accountID:accountID operation:ADXSyncQueueEntryOperation.link payload:nil userInfo:userInfo context:moc];
            
            [syncEntry save:^(BOOL success, NSError *error) {
                if (!success) {
                    LogNSError(@"Unable to enqueue a synchronization request.", error);
                }
                if (completionBlock) {
                    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                        completionBlock(success, error);
                    }];
                }
            }];
        }];
    } else {
        if (completionBlock) {
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                completionBlock(NO, nil);
            }];
        }
    }
}

- (void)removeDocument:(NSString *)documentID fromCollection:(NSString *)collectionID localID:(NSString *)localID completion:(ADXServiceCompletionBlock)completionBlock
{
    __weak NSManagedObjectContext *moc = self.managedObjectContext;
    
    NSString *accountID = self.currentAccount.id;
    ADXCollection *collection = [ADXCollection collectionWithID:collectionID orLocalID:localID accountID:accountID context:moc];
    ADXDocument *document = [ADXDocument latestDocumentWithID:documentID accountID:accountID context:moc];
    [document.collectionsSet removeObject:collection];
    
    [self saveMainContext:^(BOOL success, NSError *error) {
        ADXSyncQueueEntry *syncEntry = nil;
        NSDictionary *userInfo = @{ADXCollectionAttributes.id: (collectionID ? collectionID : [NSNull null]), ADXCollectionAttributes.localID: (localID ? localID : [NSNull null])};
        syncEntry = [ADXSyncQueueEntry insertEntryForEntity:document id:documentID accountID:accountID operation:ADXSyncQueueEntryOperation.unlink payload:nil userInfo:userInfo context:moc];
        
        [syncEntry save:^(BOOL success, NSError *error) {
            if (!success) {
                LogNSError(@"Unable to enqueue a synchronization request.", error);
            }
            if (completionBlock) {
                [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                    completionBlock(success, error);
                }];
            }
        }];
    }];
}

@end
