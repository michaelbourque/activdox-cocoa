//
//  ADXBaseViewController.m
//  Reader
//
//  Created by Gavin McKenzie on 12-06-20.
//  Copyright (c) 2012 ActivDox Inc. All rights reserved.
//

#import "ADXBaseViewController.h"

@interface ADXBaseViewController ()
- (void)setupBackgroundTextureView;
@end

@implementation ADXBaseViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setupBackgroundTextureView];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return YES;
}

- (void)setupBackgroundTextureView
{
    CGFloat toolbarHeight = self.navigationController.navigationBar.frame.size.height;
    UIView *backgroundTextureView = [[UIView alloc] initWithFrame:CGRectMake(0, -toolbarHeight, self.view.bounds.size.width, self.view.bounds.size.height + toolbarHeight)];
    backgroundTextureView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;    
    backgroundTextureView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"light-bg"]];
    [self.view addSubview:backgroundTextureView];
}

@end
