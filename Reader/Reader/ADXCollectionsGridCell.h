//
//  ADXCollectionsGridCell.h
//  Reader
//
//  Created by Gavin McKenzie on 12-06-20.
//  Copyright (c) 2012 ActivDox Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ADXAppCommonMacros.h"

@class ADXCollectionsGridCell;

@interface ADXCollectionsGridCell : UICollectionViewCell
@property (strong, nonatomic) NSString *collectionID;
@property (strong, nonatomic) NSString *collectionLocalID;
@property (weak, nonatomic) IBOutlet UIImageView *coverBandImage;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *firstMetaLabel;
@end
