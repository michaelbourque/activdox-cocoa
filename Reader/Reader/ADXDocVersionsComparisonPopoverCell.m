//
//  ADXDocVersionsComparisonPopoverCell.m
//  Reader
//
//  Created by Gavin McKenzie on 2012-09-13.
//  Copyright (c) 2012 ActivDox Inc. All rights reserved.
//

#import "ADXDocVersionsComparisonPopoverCell.h"
#import "ADXAppDelegate.h"
#import "PSPDFKit.h"

CGFloat const kADXDocVersionsComparisonPopoverCellHeight = 58.0f;
CGFloat const kADXDocVersionsComparisonPopoverCellWidth = 250.0f;

@interface ADXDocVersionsComparisonPopoverCell ()
@property (strong, nonatomic) PSPDFDocument *pdfDocument;
@property (nonatomic) BOOL currentlyReading;
@end

@implementation ADXDocVersionsComparisonPopoverCell

#pragma mark - Destruction

- (void)dealloc
{
    [self removeDocumentObservers];
}

#pragma mark - Notifications

- (void)addDocumentObservers
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleThumbnailDownloadedNotification:) name:kADXServiceDocumentThumbnailDownloadedNotification object:nil];
}

- (void)removeDocumentObservers
{
    @try {
        [[NSNotificationCenter defaultCenter] removeObserver:self name:kADXServiceDocumentThumbnailDownloadedNotification object:nil];
    }
    @catch (NSException *exception) {
        // Do nothing.
    }
}

#pragma mark - Accessors

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

- (void)setDocument:(ADXDocument *)document
{
    [self removeDocumentObservers];
    _document = document;

    ADXAccount *account = [ADXAppDelegate sharedInstance].currentAccount;
    ADXDocument *mostRecent = [ADXDocument mostRecentlyOpenedDocumentWithID:document.id accountID:account.id context:document.managedObjectContext];
    self.currentlyReading = (document == mostRecent);
    
    if (document) {
        [self addDocumentObservers];
        [self refresh];
    } else {
        self.versionLabel.text = nil;
        self.versionLabel.enabled = self.enabled;
        self.dateLabel.text = nil;
        self.dateLabel.enabled = self.enabled;
    }

    self.contentView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"popover_bg"]];
}

- (PSPDFDocument *)pdfDocument
{
    if (!_pdfDocument && self.document.downloaded && self.document.content.data) {
        _pdfDocument = [[PSPDFDocument alloc] initWithData:self.document.content.data];
    }
    return _pdfDocument;
}

- (void)refresh
{
    if (self.currentlyReading) {
        self.versionLabel.text = [NSString stringWithFormat:@"%@  ☕", self.document.versionDisplayLabel];
    } else {
        self.versionLabel.text = [self.document versionDisplayLabel];
    }
    
    self.dateLabel.text = [self.document publishedDateDisplayLabel:YES];
    self.versionLabel.enabled = self.enabled;
    self.dateLabel.enabled = self.enabled;
    
    __weak ADXDocVersionsComparisonPopoverCell *weakSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        [weakSelf setNeedsLayout];
    });    
}

- (void) setEnabled:(BOOL)enabled
{
    _enabled = enabled;
    
    self.versionLabel.enabled = enabled;
    self.dateLabel.enabled = enabled;
}

#pragma mark - Notification Handling

- (void)handleThumbnailDownloadedNotification:(NSNotification *)notification
{
    NSString *documentID = [notification.userInfo valueForKey:kADXServiceNotificationPayload];
    if (![documentID isEqualToString:self.document.id]) {
        return;
    }
    
    __weak ADXDocVersionsComparisonPopoverCell *weakSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        [weakSelf refresh];
    });
}

@end
