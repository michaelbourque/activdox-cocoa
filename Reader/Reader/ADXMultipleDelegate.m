//
//  ADXMultipleDelegate.m
//  Reader
//
//  Created by Matteo Ugolini on 2012-10-29.
//  Copyright (c) 2012 ActivDox Inc. All rights reserved.
//

#import "ADXMultipleDelegate.h"

@interface ADXMultipleDelegate ()

@property(nonatomic, strong) NSMutableArray *delegates;

@end

@implementation ADXMultipleDelegate

- (id)init
{
    self = [super init];
    if (self) {
        _delegates = [NSMutableArray array];
    }
    return self;
}

- (void)addDelegate:(id)delegate
{
    [self.delegates addObject:[NSValue valueWithNonretainedObject:delegate]];
}

- (void)removeDelegate:(id)delegate
{
    int index = 0;
    BOOL found = NO;
    NSValue *toRemove = nil;
    while (index < self.delegates.count && !found)
    {
        toRemove = [self.delegates objectAtIndex:index++];
        found = [toRemove.nonretainedObjectValue isEqual:delegate];
    }
    
    if( found ) {
        [self.delegates removeObject:[NSValue valueWithNonretainedObject:toRemove]];
    }
}

- (void)enumerateDelegateUsingBlock:(void (^)(id d, BOOL *s))block
{
    id delegate;
    if( block && self.delegates && self.delegates.count )
    {
        BOOL stop = NO;
        for ( NSValue *value in self.delegates )
        {
            delegate = value.nonretainedObjectValue;
            block (delegate, &stop);
            if(stop){
                break;
            }
        }
    }
}

- (void)enumerateDelegateWhoRespondsToSelector:(SEL)selector usingBlock:(void (^)(id d, BOOL *s))block
{
    id delegate;
    if( block && selector && self.delegates && self.delegates.count )
    {
        BOOL stop = NO;
        for ( NSValue *value in self.delegates )
        {
            delegate = value.nonretainedObjectValue;
            if ( [delegate respondsToSelector:selector] )
            {
                block (delegate, &stop);
                if(stop){
                    break;
                }
            }
        }
    }
}

@end
