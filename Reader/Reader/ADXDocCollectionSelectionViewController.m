//
//  ADXDocCollectionSelectionViewController.m
//  Reader
//
//  Created by Gavin McKenzie on 2013-04-23.
//  Copyright (c) 2013 ActivDox Inc. All rights reserved.
//

#import "ADXDocCollectionSelectionViewController.h"
#import "ADXAppDelegate.h"

@interface ADXDocCollectionSelectionViewController () <NSFetchedResultsControllerDelegate>
@end

@implementation ADXDocCollectionSelectionViewController

#pragma mark - View Management

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if (!self.managedObjectContext) {
        ADXAppDelegate *appDelegate = (ADXAppDelegate *)[[UIApplication sharedApplication] delegate];
        self.managedObjectContext = appDelegate.managedObjectContext;
    }
    
    self.navigationItem.title = @"Select Collections";
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"popover_bg"]];

    [self prepareListFetchedResultsController];
}

- (CGSize)contentSizeForViewInPopover
{
    return CGSizeMake(320.0f, 544.0f);
}

- (void)refreshForTableView:(UITableView *)tableView
{
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        NSFetchedResultsController *frc = [self fetchedResultsControllerForTableView:tableView];
        if (frc == self.listFetchedResultsController) {
            [self prepareListFetchedResultsController];
        } else if (frc == self.searchFetchedResultsController) {
            [self prepareSearchFetchedResultsController];
        }
        [tableView reloadData];
    }];
}

#pragma mark - Table view

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    NSFetchedResultsController *frc = [self fetchedResultsControllerForTableView:tableView];
    NSInteger result = [[frc sections] count];
    return result;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger result = [self numberOfItemsInSection:section tableView:tableView];
    return result;
}

- (void)configureCell:(UITableViewCell *)cell indexPath:(NSIndexPath *)indexPath tableView:(UITableView *)tableView
{
    ADXCollection *collection = [self.listFetchedResultsController objectAtIndexPath:indexPath];
    cell.textLabel.text = collection.title;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Important to deque from our main tableView, regardless of whether the search-mode tableView is displayed.
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"kADXDocCollectionSelectionCell"];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"kADXDocCollectionSelectionCell"];
        cell.textLabel.textColor = [UIColor colorWithWhite:80/255.0 alpha:1.0];
        cell.textLabel.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:15.0f];
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    [self configureCell:cell indexPath:indexPath tableView:tableView];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

    ADXCollection *collection = [self.listFetchedResultsController objectAtIndexPath:indexPath];
    if (self.actionDelegate) {
        [self.actionDelegate didSelectCollection:collection];
    }
}

#pragma mark - Data Management

- (void)saveChanges
{
    ADXAppDelegate *appDelegate = (ADXAppDelegate *)[[UIApplication sharedApplication] delegate];
    [appDelegate saveMainContext:nil];
}

- (NSInteger)numberOfItemsInSection:(NSInteger)section tableView:(UITableView *)tableView
{
    NSFetchedResultsController *frc = [self fetchedResultsControllerForTableView:tableView];
    id <NSFetchedResultsSectionInfo> sectionInfo = [[frc sections] objectAtIndex:section];
    NSInteger result = [sectionInfo numberOfObjects];
    return result;
}

- (id)objectAtIndexPath:indexPath tableView:(UITableView *)tableView
{
    NSFetchedResultsController *frc = [self fetchedResultsControllerForTableView:tableView];
    id object = [frc objectAtIndexPath:indexPath];
    return object;
}

- (NSPredicate *)predicateForListFetchedResultsController
{
    NSPredicate *predicate = nil;
    predicate = [NSPredicate predicateWithFormat:@"(%K == %@) AND (%K != %@) AND SUBQUERY(documents, $x, $x == %@).@count == 0",
                 ADXCollectionAttributes.accountID, self.document.accountID,
                 ADXCollectionAttributes.type, ADXCollectionType.system,
                 self.document];
    return predicate;
}

- (NSSortDescriptor *)sortDescriptorForListFetchedResultsController
{
    NSSortDescriptor *sortDescriptor = nil;
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:ADXCollectionAttributes.title ascending:YES selector:@selector(localizedCaseInsensitiveCompare:)];
    return sortDescriptor;
}

- (NSPredicate *)predicateForSearchFetchedResultsController
{
    NSPredicate *predicate = nil;
    NSString *searchText = self.searchDisplayController.searchBar.text;
    
    predicate = [NSPredicate predicateWithFormat:@"(%K == %@) AND (%K != %@) AND (%K CONTAINS[cd] %@) AND SUBQUERY(documents, $x, $x == %@).@count == 0",
                 ADXCollectionAttributes.accountID, self.document.accountID,
                 ADXCollectionAttributes.type, ADXCollectionType.system,
                 ADXCollectionAttributes.title, searchText,
                 self.document];
    return predicate;
}

- (NSSortDescriptor *)sortDescriptorForSearchFetchedResultsController
{
    return [self sortDescriptorForListFetchedResultsController];
}

- (NSFetchedResultsController *)fetchedResultsControllerForTableView:(UITableView *)tableView
{
    NSFetchedResultsController *result = nil;
    if (tableView == self.tableView) {
        result = self.listFetchedResultsController;
    } else {
        result = self.searchFetchedResultsController;
    }
    return result;
}

- (void)refetchFetchedResultsControllerForTableView:(UITableView *)tableView
{
    __weak NSFetchedResultsController *frc = [self fetchedResultsControllerForTableView:tableView];
    
    [self.managedObjectContext performBlockAndWait:^{
        NSError *error = nil;
        BOOL success = [frc performFetch:&error];
        if (!success) {
            LogNSError(@"Couldn't initialize NSFetchedResultsController", error);
        }
    }];
}

- (void)prepareListFetchedResultsController
{
    __weak ADXDocCollectionSelectionViewController *weakSelf = self;
    __block NSFetchedResultsController *frc = nil;
    
    if (self.listFetchedResultsController) {
        self.listFetchedResultsController.delegate = nil;
        self.listFetchedResultsController = nil;
    }
    
    NSPredicate *predicate = [self predicateForListFetchedResultsController];
    NSSortDescriptor *sortDescriptor = [self sortDescriptorForListFetchedResultsController];
    
    [self.managedObjectContext performBlockAndWait:^{
        frc = [weakSelf.managedObjectContext aps_fetchedResultsControllerForEntityName:[ADXCollection entityName]
                                                                             predicate:predicate
                                                                       sortDescriptors:[NSArray arrayWithObject:sortDescriptor]
                                                                    sectionNameKeyPath:nil
                                                                             cacheName:nil
                                                                              delegate:weakSelf];
        NSError *error = nil;
        BOOL success = [frc performFetch:&error];
        if (!success) {
            LogNSError(@"Couldn't initialize NSFetchedResultsController", error);
        }
    }];
    
    self.listFetchedResultsController = frc;
    self.listFetchedResultsController.delegate = self;
}

- (void)prepareSearchFetchedResultsController
{
    __weak ADXDocCollectionSelectionViewController *weakSelf = self;
    __block NSFetchedResultsController *frc = nil;
    
    NSPredicate *predicate = [self predicateForSearchFetchedResultsController];
    NSSortDescriptor *sortDescriptor = [self sortDescriptorForSearchFetchedResultsController];
    
    [self.managedObjectContext performBlockAndWait:^{
        frc = [weakSelf.managedObjectContext aps_fetchedResultsControllerForEntityName:[ADXCollection entityName]
                                                                             predicate:predicate
                                                                       sortDescriptors:[NSArray arrayWithObject:sortDescriptor]
                                                                    sectionNameKeyPath:nil
                                                                             cacheName:nil
                                                                              delegate:weakSelf];
        NSError *error = nil;
        BOOL success = [frc performFetch:&error];
        if (!success) {
            LogNSError(@"Couldn't initialize NSFetchedResultsController", error);
        }
    }];
    
    self.searchFetchedResultsController = frc;
}

#pragma mark - UISearchDisplayDelegate

- (void)searchDisplayControllerWillBeginSearch:(UISearchDisplayController *)controller
{
    // Do nothing
}

- (void)searchDisplayControllerDidBeginSearch:(UISearchDisplayController *)controller
{
    controller.searchResultsTableView.backgroundColor = [UIColor colorWithWhite:0.196 alpha:1.000];
    controller.searchResultsTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    controller.searchResultsTableView.rowHeight = 44;
}

- (void)searchDisplayController:(UISearchDisplayController *)controller willShowSearchResultsTableView:(UITableView *)tableView
{
    // Do nothing
}

- (BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString
{
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        [self refreshForTableView:controller.searchResultsTableView];
    }];
    return NO;
}

- (BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchScope:(NSInteger)searchOption
{
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        [self refreshForTableView:controller.searchResultsTableView];
    }];
    return NO;
}

- (void)searchDisplayControllerDidEndSearch:(UISearchDisplayController *)controller
{
    if (self.editing) {
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            [self refreshForTableView:controller.searchResultsTableView];
        }];
    }
}

- (void)searchDisplayController:(UISearchDisplayController *)controller willUnloadSearchResultsTableView:(UITableView *)tableView
{
    self.searchFetchedResultsController = nil;
}

#pragma mark - NSFetchedResultsControllerDelegate

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
    [self.tableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller
  didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex
     forChangeType:(NSFetchedResultsChangeType)type
{
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex]
                          withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex]
                          withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controller:(NSFetchedResultsController *)controller
   didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath
     forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath
{
    UITableView *tableView = self.tableView;
    
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [self configureCell:[tableView cellForRowAtIndexPath:indexPath] indexPath:indexPath tableView:tableView];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    [self.tableView endUpdates];
}

@end
