//
//  ADXAnnotationFilterTableViewController.m
//  Reader
//
//  Created by Steve Tibbett on 2013-03-25.
//  Copyright (c) 2013 ActivDox Inc. All rights reserved.
//

#import "ADXAnnotationFilterTableViewController.h"
#import "ADXAnnotationUtil.h"
#import "ADXAppDelegate.h"

@interface ADXAnnotationFilterTableViewController ()

@property (weak, nonatomic) IBOutlet UISwitch *showNotesSwitch;
- (IBAction)showNotesSwitchChanged:(id)sender;

@property (weak, nonatomic) IBOutlet UISwitch *showHighlightsSwitch;
- (IBAction)showHighlightsSwitchChanged:(id)sender;

@property (weak, nonatomic) IBOutlet UISwitch *showChangesSwitch;
- (IBAction)showChangesSwitchChanged:(id)sender;

@property (weak, nonatomic) IBOutlet UISwitch *showMyNotesSwitch;
- (IBAction)showMyNotesSwitchChanged:(id)sender;

@property (weak, nonatomic) IBOutlet UISwitch *showAuthorNotesSwitch;
- (IBAction)showAuthorNotesSwitchChanged:(id)sender;

@property (weak, nonatomic) IBOutlet UISwitch *showOtherNotesSwitch;
- (IBAction)showOtherNotesSwitchChanged:(id)sender;

@property (weak, nonatomic) IBOutlet UISwitch *showResolvedNotesSwitch;
- (IBAction)showResolvedNotesSwitchChanged:(id)sender;

@end

@implementation ADXAnnotationFilterTableViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIImage *show = [UIImage imageNamed:@"show"];
    UIImage *hide = [UIImage imageNamed:@"hide"];
    
    self.showNotesSwitch.onImage = show;
    self.showNotesSwitch.offImage = hide;

    self.showChangesSwitch.onImage = show;
    self.showChangesSwitch.offImage = hide;
    
    self.showHighlightsSwitch.onImage = show;
    self.showHighlightsSwitch.offImage = hide;
    
    self.showMyNotesSwitch.onImage = show;
    self.showMyNotesSwitch.offImage = hide;
    
    self.showOtherNotesSwitch.onImage = show;
    self.showOtherNotesSwitch.offImage = hide;
    
    self.showResolvedNotesSwitch.onImage = show;
    self.showResolvedNotesSwitch.offImage = hide;
    
    self.showAuthorNotesSwitch.onImage = show;
    self.showAuthorNotesSwitch.offImage = hide;
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"popover_bg"]];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self refresh];
    
    NSAssert(self.tableView.delegate == self, @"Table delegate not set");
}

#pragma mark - Table view data source

#pragma mark - Table view delegate

- (void)refresh
{
    ADXUserDefaults *defaults = [ADXUserDefaults sharedDefaults];
    BOOL showNotes = defaults.annotationFilterShowNotes;
    
    self.showMyNotesSwitch.enabled = showNotes;
    self.showAuthorNotesSwitch.enabled = showNotes;
    self.showOtherNotesSwitch.enabled = showNotes;

    self.showNotesSwitch.on = defaults.annotationFilterShowNotes;
    self.showHighlightsSwitch.on = defaults.annotationFilterShowHighlights;
    self.showChangesSwitch.on = defaults.annotationFilterShowChanges;
    self.showMyNotesSwitch.on = defaults.annotationFilterIncludeMyNotes;
    self.showAuthorNotesSwitch.on = defaults.annotationFilterIncludeAuthorNotes;
    self.showOtherNotesSwitch.on = defaults.annotationFilterIncludeOtherNotes;
    self.showResolvedNotesSwitch.on = defaults.annotationFilterIncludeResolved;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10, 8, tableView.bounds.size.width-30, 18)];
    label.text = [self tableView:tableView titleForHeaderInSection:section];
    label.textColor = [UIColor colorWithRed:80/255.0 green:80/255.0 blue:80/255.0 alpha:1.0];
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont boldSystemFontOfSize:18];
    
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, 30)];
    [headerView addSubview:label];
    
    return headerView;
}

- (IBAction)showNotesSwitchChanged:(id)sender
{
    BOOL showNotes = self.showNotesSwitch.on;
    [[ADXUserDefaults sharedDefaults] setAnnotationFilterShowNotes:showNotes];
    [self refresh];

    if (showNotes) {
        [[ADXAppDelegate sharedInstance] passCheckpoint:@"Changed annotation visibility: Show notes ON"];
    } else {
        [[ADXAppDelegate sharedInstance] passCheckpoint:@"Changed annotation visibility: Show notes OFF"];
    }

    [[NSNotificationCenter defaultCenter] postNotificationName:kADXAnnotationFilterChangedNotification object:nil];
}

- (IBAction)showHighlightsSwitchChanged:(id)sender
{
    BOOL newValue = self.showHighlightsSwitch.on;
    [[ADXUserDefaults sharedDefaults] setAnnotationFilterShowHighlights:newValue];
    [self refresh];

    if (newValue) {
        [[ADXAppDelegate sharedInstance] passCheckpoint:@"Changed annotation visibility: Show highlights ON"];
    } else {
        [[ADXAppDelegate sharedInstance] passCheckpoint:@"Changed annotation visibility: Show highlights OFF"];
    }

    [[NSNotificationCenter defaultCenter] postNotificationName:kADXAnnotationFilterChangedNotification object:nil];
}

- (IBAction)showChangesSwitchChanged:(id)sender
{
    BOOL showChanges = self.showChangesSwitch.on;
    [[ADXUserDefaults sharedDefaults] setAnnotationFilterShowChanges:showChanges];
    [self refresh];
    
    if (showChanges) {
        [[ADXAppDelegate sharedInstance] passCheckpoint:@"Show changes set to ON"];
    } else {
        [[ADXAppDelegate sharedInstance] passCheckpoint:@"Show changes set to OFF"];
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kADXAnnotationFilterChangedNotification object:nil];
}

- (IBAction)showMyNotesSwitchChanged:(id)sender
{
    BOOL newValue = self.showMyNotesSwitch.on;
    [[ADXUserDefaults sharedDefaults] setAnnotationFilterIncludeMyNotes:newValue];
    [self refresh];

    if (newValue) {
        [[ADXAppDelegate sharedInstance] passCheckpoint:@"Changed annotation visibility: Show my notes ON"];
    } else {
        [[ADXAppDelegate sharedInstance] passCheckpoint:@"Changed annotation visibility: Show my notes OFF"];
    }

    [[NSNotificationCenter defaultCenter] postNotificationName:kADXAnnotationFilterChangedNotification object:nil];
}

- (IBAction)showAuthorNotesSwitchChanged:(id)sender
{
    BOOL newValue = self.showAuthorNotesSwitch.on;
    [[ADXUserDefaults sharedDefaults] setAnnotationFilterIncludeAuthorNotes:newValue];
    [self refresh];

    if (newValue) {
        [[ADXAppDelegate sharedInstance] passCheckpoint:@"Changed annotation visibility: Show author notes ON"];
    } else {
        [[ADXAppDelegate sharedInstance] passCheckpoint:@"Changed annotation visibility: Show author notes OFF"];
    }

    [[NSNotificationCenter defaultCenter] postNotificationName:kADXAnnotationFilterChangedNotification object:nil];
}

- (IBAction)showOtherNotesSwitchChanged:(id)sender
{
    BOOL newValue = self.showOtherNotesSwitch.on;
    [[ADXUserDefaults sharedDefaults] setAnnotationFilterIncludeOtherNotes:newValue];
    [self refresh];

    if (newValue) {
        [[ADXAppDelegate sharedInstance] passCheckpoint:@"Changed annotation visibility: Show other notes ON"];
    } else {
        [[ADXAppDelegate sharedInstance] passCheckpoint:@"Changed annotation visibility: Show other notes OFF"];
    }

    [[NSNotificationCenter defaultCenter] postNotificationName:kADXAnnotationFilterChangedNotification object:nil];
}

- (IBAction)showResolvedNotesSwitchChanged:(id)sender
{
    BOOL newValue = self.showResolvedNotesSwitch.on;
    [[ADXUserDefaults sharedDefaults] setAnnotationFilterIncludeResolved:newValue];
    [self refresh];
    
    if (newValue) {
        [[ADXAppDelegate sharedInstance] passCheckpoint:@"Changed annotation visibility: Show resolved notes ON"];
    } else {
        [[ADXAppDelegate sharedInstance] passCheckpoint:@"Changed annotation visibility: Show resolved notes OFF"];
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kADXAnnotationFilterChangedNotification object:nil];
}
@end
