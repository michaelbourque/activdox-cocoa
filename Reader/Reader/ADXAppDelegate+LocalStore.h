//
//  ADXAppDelegate+LocalStore.h
//  Reader
//
//  Created by Gavin McKenzie on 2013-04-22.
//  Copyright (c) 2013 ActivDox Inc. All rights reserved.
//

#import "ADXAppDelegate.h"

extern NSString * const kADXCollectionUntitled;

@interface ADXAppDelegate (LocalStore)
- (void)saveMainContext:(ADXCompletionBlock)completionBlock;

//
// Documents
//
- (void)pdfResourceForDocument:(NSString *)documentID
                     versionID:(NSNumber *)versionID
               downloadContent:(BOOL)downloadContent
                       service:(ADXV2Service *)service
             downloadWillStart:(ADXPDFResourceDownloadingBlock)startBlock
                    completion:(ADXResourceCompletionBlock)completionBlock;
- (void)pdfResourceForDocument:(ADXDocument *)document
               downloadContent:(BOOL)downloadContent
                       service:(ADXV2Service *)service
             downloadWillStart:(ADXPDFResourceDownloadingBlock)startBlock
                    completion:(ADXResourceCompletionBlock)completionBlock;

//
// Collections
//

- (void)insertUserCollection:(ADXResourceCompletionBlock)completionBlock;
- (void)deleteCollection:(NSString *)collectionID localID:(NSString *)localID completion:(ADXCompletionBlock)completionBlock;
- (void)addDocument:(NSString *)documentID toCollection:(NSString *)collectionID localID:(NSString *)localID completion:(ADXServiceCompletionBlock)completionBlock;
- (void)removeDocument:(NSString *)documentID fromCollection:(NSString *)collectionID localID:(NSString *)localID completion:(ADXServiceCompletionBlock)completionBlock;
@end
