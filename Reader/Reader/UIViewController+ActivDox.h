//
//  UIViewController+ActivDox.h
//  Reader
//
//  Created by Gavin McKenzie on 2012-09-17.
//  Copyright (c) 2012 ActivDox Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (ActivDox)

- (UIBarButtonItem *)adx_barButtonItemWithCustomView:(UIView *)view;

@end
