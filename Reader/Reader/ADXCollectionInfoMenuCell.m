//
//  ADXCollectionInfoMenuCell.m
//  Reader
//
//  Created by Gavin McKenzie on 2013-04-09.
//  Copyright (c) 2013 ActivDox Inc. All rights reserved.
//

#import "ADXCollectionInfoMenuCell.h"

@implementation ADXCollectionInfoMenuCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

@end
