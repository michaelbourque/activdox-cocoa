//
//  ADXBaseDocListCell.m
//  Reader
//
//  Created by Gavin McKenzie on 2013-04-13.
//  Copyright (c) 2013 ActivDox Inc. All rights reserved.
//

#import "ADXBaseDocListCell.h"
#import <QuartzCore/QuartzCore.h>

@implementation ADXBaseDocListCell

- (void)awakeFromNib
{
    // Due to a flaw in AutoLayout use in UITableViewCell, if we don't do the following the cell won't adjust to edit mode properly.
    // See: http://stackoverflow.com/questions/12600214/contentview-not-indenting-in-ios-6-uitableviewcell-prototype-cell
    
    [self removeConstraint:self.leadingHConstraint];
    [self removeConstraint:self.trailingHConstraint];
    NSArray *constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"|-0-[_containerView]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(_containerView)];
    [self.contentView addConstraints:constraints];
    
    self.thumbnailImageView.layer.shadowColor = [UIColor blackColor].CGColor;
    self.thumbnailImageView.layer.shadowOffset = CGSizeMake(0, 1.0);
    self.thumbnailImageView.layer.shadowOpacity = 0.5;
    self.thumbnailImageView.layer.shadowRadius = 1.0;
}

@end
