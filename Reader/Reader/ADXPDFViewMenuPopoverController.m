//
//  ADXPDFViewMenuPopoverController.m
//  Reader
//
//  Created by Gavin McKenzie on 2013-01-18.
//  Copyright (c) 2013 ActivDox Inc. All rights reserved.
//

#import "ADXPDFViewMenuPopoverController.h"

NSString * const kADXPDFViewMenuPopoverOptionID = @"id";
NSString * const kADXPDFViewMenuPopoverOptionLabel = @"label";
NSString * const kADXPDFViewMenuPopoverOptionIcon = @"icon";

CGFloat const kADXPDFViewMenuPopoverCellHeight = 44.0f;
CGFloat const kADXPDFViewMenuPopoverCellWidth = 320.0f;

@interface ADXPDFViewMenuPopoverController ()
@property (strong, nonatomic) NSArray *options;
@end

@implementation ADXPDFViewMenuPopoverController

#pragma mark - Class Methods

+ (ADXPDFViewMenuPopoverController *)viewControllerWithOptions:(NSArray *)options
{
    ADXPDFViewMenuPopoverController *viewController = [[ADXPDFViewMenuPopoverController alloc] initWithNibName:nil bundle:nil];
    viewController.options = options;
    return viewController;
}

+ (UIPopoverController *)popoverControllerWithOptions:(NSArray *)options actionDelegate:(id)delegate
{
    UIPopoverController *popover = nil;
    ADXPDFViewMenuPopoverController *viewController = [ADXPDFViewMenuPopoverController viewControllerWithOptions:options];
    viewController.actionDelegate = delegate;

    popover = [[UIPopoverController alloc] initWithContentViewController:viewController];
    [popover setPopoverContentSize:CGSizeMake(kADXPDFViewMenuPopoverCellWidth, kADXPDFViewMenuPopoverCellHeight * options.count) animated:NO];
    viewController.parentPopoverController = popover;

    return popover;
}

#pragma mark - Initialization

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor colorWithWhite:0.196 alpha:1.000];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Options

- (NSDictionary *)optionAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *option = self.options[indexPath.row];
    return option;
}

- (NSString *)labelForOptionAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *option = [self optionAtIndexPath:indexPath];
    NSString *result = option[kADXPDFViewMenuPopoverOptionLabel];
    return result;
}

- (UIImage *)iconForOptionAtIndexPath:(NSIndexPath *)indexPath
{
    UIImage *result = nil;
    NSDictionary *option = [self optionAtIndexPath:indexPath];
    NSString *imageFilename = option[kADXPDFViewMenuPopoverOptionIcon];
    if (imageFilename) {
        result = [UIImage imageNamed:imageFilename];
    }
    return result;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.options.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"ADXPDFViewMenuPopoverController";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    cell.textLabel.text = [self labelForOptionAtIndexPath:indexPath];
    cell.textLabel.textColor = [UIColor lightTextColor];
    cell.imageView.image = [self iconForOptionAtIndexPath:indexPath];
    
    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.actionDelegate) {
        [self.actionDelegate didSelectPDFViewMenuOption:[self optionAtIndexPath:indexPath]];
    }
    [self.parentPopoverController dismissPopoverAnimated:YES];
}

@end
