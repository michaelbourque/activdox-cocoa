//
//  ADXStartViewController_iPhone.h
//  Reader
//
//  Created by Gavin McKenzie on 2012-08-03.
//  Copyright (c) 2012 ActivDox Inc. All rights reserved.
//

#import "ADXBaseStartViewController.h"
#import "ADXAppCommonMacros.h"
#import "ADXActivityImageView.h"

@interface ADXStartViewController_iPhone : ADXBaseStartViewController
@property (weak, nonatomic) IBOutlet UIView *menuContainerView;
@property (weak, nonatomic) IBOutlet ADXActivityImageView *activityIndicator;
@property (weak, nonatomic) IBOutlet UILabel *buildInfoLabel;
@end
