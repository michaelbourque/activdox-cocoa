//
//  ADXMultipleDelegate.h
//  Reader
//
//  Created by Matteo Ugolini on 2012-10-29.
//  Copyright (c) 2012 ActivDox Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ADXMultipleDelegate : NSObject

- (void)addDelegate:(id)delegate;
- (void)removeDelegate:(id)delegate;
- (void)enumerateDelegateUsingBlock:(void (^)(id delegate, BOOL *stop))block;
- (void)enumerateDelegateWhoRespondsToSelector:(SEL)selector usingBlock:(void (^)(id delegate, BOOL *stop))block;

@end
