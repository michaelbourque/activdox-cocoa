//
//  ADXDocPermissionsCell.h
//  Reader
//
//  Created by Kei Turner on 2013-09-09.
//  Copyright (c) 2013 ActivDox Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ADXDocPermissionsCell : UITableViewCell{
    
}
@property (weak, nonatomic) IBOutlet UILabel *usernameLabel;
@property (weak, nonatomic) IBOutlet UILabel *permissionTypeLabel;

@end
