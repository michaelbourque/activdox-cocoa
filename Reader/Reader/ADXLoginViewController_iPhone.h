//
//  ADXLoginViewController_iPhone.h
//  Reader
//
//  Created by Gavin McKenzie on 2012-08-03.
//  Copyright (c) 2012 ActivDox Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ActivDoxCore.h"
#import "ADXAppCommonMacros.h"
#import "ADXUnderlinedButton.h"

@interface ADXLoginViewController_iPhone : UITableViewController
@property (strong, nonatomic) ADXLoginProcessor *loginProcessor;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *doneButton;
@property (weak, nonatomic) IBOutlet UITextField *loginNameField;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *registrationDoneButton;
@property (weak, nonatomic) IBOutlet UITextField *passwordField;
@property (weak, nonatomic) IBOutlet UITextField *serverField;
@property (weak, nonatomic) IBOutlet UISwitch *rememberPasswordSwitch;
@property (weak, nonatomic) IBOutlet ADXUnderlinedButton *forgotPasswordLink;
@property (weak, nonatomic) IBOutlet UITextField *registerEmailField;
@property (weak, nonatomic) IBOutlet UITextField *registerPasswordField;
@property (weak, nonatomic) IBOutlet UITextField *registerPasswordConfirmField;
@property (weak, nonatomic) IBOutlet UITextField *registerServerField;
- (IBAction)didTapLoginDoneButton:(id)sender;
- (IBAction)didTapRegistrationDoneButton:(id)sender;
@end
