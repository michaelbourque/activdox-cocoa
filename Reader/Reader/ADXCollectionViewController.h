//
//  ADXCollectionViewController.h
//  Reader
//
//  Created by Gavin McKenzie on 2012-10-15.
//  Copyright (c) 2012 ActivDox Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ADXCollectionController.h"

#import <CoreLocation/CoreLocation.h>

@class ADXCollectionsViewController;

typedef enum {
    ADXCollectionViewDisplayModeThumbnail = 0,
    ADXCollectionViewDisplayModeThumbnailSmall,
    ADXCollectionViewDisplayModeList
} ADXCollectionViewDisplayMode;

typedef void(^ADXCollectionViewOnRefreshRequiredBlock)();

@protocol ADXCollectionViewActionsProtocol <NSObject>
@optional
- (void)openActionForDocument:(ADXDocument *)document animated:(BOOL)animated;
- (void)openActionForDocument:(ADXDocument *)document onPage:(NSInteger)page animated:(BOOL)animated;
- (void)openActionForDocument:(ADXDocument *)document onPage:(NSInteger)page fromRectOfView:(UIView *)rectOfView inView:(UIView *)view animated:(BOOL)animated;
- (void)popoverActionForDocument:(ADXDocument *)document thumbnail:(UIImage *)thumbnail fromRectOfView:(UIView *)rectOfView inView:(UIView *)view onRefreshRequired:(ADXCollectionViewOnRefreshRequiredBlock)refreshBlock;
@end

@interface ADXCollectionViewController : UIViewController <ADXCollectionControllerDelegate, ADXCollectionViewActionsProtocol, UIScrollViewDelegate, CLLocationManagerDelegate>{
    CLLocationManager*locationManager;
    CLLocation*location;
}
@property (strong, nonatomic) ADXCollection *collection;
@property (strong, nonatomic) ADXCollectionViewOnRefreshRequiredBlock onRefreshRequiredBlock;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *notificationsBarButton;
@property (weak, nonatomic) IBOutlet UITextField *titleMessage;
@property (assign, nonatomic) ADXCollectionViewDisplayMode displayMode;
@property (weak, nonatomic) id<ADXCollectionViewActionsProtocol> actionsDelegate;

- (IBAction)didTapRefreshButton:(id)sender;
- (IBAction)didTapBottomStatusMessage:(id)sender;
- (IBAction)didLongPressRefreshButton:(id)sender;
- (IBAction)didTapNotificationsButton:(id)sender;



@end
