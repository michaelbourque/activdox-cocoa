//
//  ADXAppDelegate.h
//  Reader
//
//  Created by Gavin McKenzie on 12-04-30.
//  Copyright (c) 2012 ActivDox Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ADXAppCommonMacros.h"
#import "ActivDoxCore.h"

extern const struct ADXAppState {
	__unsafe_unretained NSString *startup;
    __unsafe_unretained NSString *startupEnded;
    __unsafe_unretained NSString *viewingCollections;
    __unsafe_unretained NSString *viewingCollectionsEnded;
    __unsafe_unretained NSString *viewingDocBrowser;
    __unsafe_unretained NSString *viewingDocBrowserEnded;
    __unsafe_unretained NSString *viewingDocument;
    __unsafe_unretained NSString *viewingDocumentEnded;
} ADXAppState;

extern NSString * const kADXLogoutNotification;

typedef void(^ADXPDFResourceDownloadingBlock)(void);
typedef void(^ADXResourceCompletionBlock)(id resource, BOOL success, NSError *error);
typedef void(^ADXCompletionBlock)(BOOL success, NSError *error);

@class ADXAccount;

@interface ADXAppDelegate : UIResponder <UIApplicationDelegate>
@property (strong, nonatomic) UIWindow *window;
@property (readonly, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (assign, nonatomic) NSString *readerAppState;
@property (strong, nonatomic) NSMutableDictionary *stashedState;
@property (readonly, strong, nonatomic) ADXAccount *currentAccount;
@property (strong, nonatomic) ADXURLSchemeRequest *pendingURLSchemeRequest;
+ (ADXAppDelegate *)sharedInstance;

- (NSString *)applicationDocumentsDirectory;
- (NSString *)applicationCachesDirectory;

- (void)setCurrentAccountByID:(NSString *)accountID;
- (void)clearCurrentAccount;
- (void)presentUserReauthenticationPopupIfNecessary;

- (NSString *)localizedStringForKey:(NSString *)key;
- (void)appAPNSModeIsCompatibleWithService:(ADXV2Service *)service completion:(ADXCompletionBlock)completionBlock;
- (void)processNotificationRequestWithEventID:(NSString *)eventID;

- (NSString *)buildDescription;
- (void)setupDefaultNavbarAppearance;

- (void)presentEmailDatabaseComposer:(UIViewController *)viewController;

- (void)passCheckpoint:(NSString *)checkpointName;
@end


#import "ADXAppDelegate+Session.h"
#import "ADXAppDelegate+LocalStore.h"
