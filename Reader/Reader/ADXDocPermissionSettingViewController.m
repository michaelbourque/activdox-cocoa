//
//  ADXPermissionSettingViewController.m
//  Reader
//
//  Created by Kei Turner on 2013-09-15.
//  Copyright (c) 2013 ActivDox Inc. All rights reserved.
//

#import "ADXDocPermissionSettingViewController.h"

#import "ADXAppDelegate.h"
#import <CoreLocation/CoreLocation.h>
#import <AddressBook/AddressBook.h>

@interface ADXDocPermissionSettingViewController ()

@end

@implementation ADXDocPermissionSettingViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (id)initWithStyle:(UITableViewStyle)style andIncludedLocations:(NSArray *)locations{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
        includedLocations = [[NSMutableArray alloc]initWithArray:locations];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"popover_bg"]];
    
    NSString *path = [[NSBundle mainBundle] pathForResource:@"LocationsDemo" ofType:@"plist"];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if ([fileManager fileExistsAtPath:path]) {
        
        NSMutableDictionary *plistDict = [[NSMutableDictionary alloc] initWithContentsOfFile:path];
        NSString *docID = [plistDict objectForKey:@"documentID"]; //test the user exist
        
        if (docID) {
            NSArray * locations= [plistDict objectForKey:@"locations"];
            includedLocations = [locations mutableCopy];
        }
    }

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (CGSize)contentSizeForViewInPopover
{
    CGSize result = CGSizeMake(320.0f, 436.0f);
    return result;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source
-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    return self.locationsHeaderView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return self.locationsHeaderView.frame.size.height;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return includedLocations.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    if (cell.textLabel.text.length == 0) {
        [cell.textLabel setText:@""];
        [cell.textLabel setTextColor:[UIColor colorWithWhite:0.3 alpha:1.0]];
        NSDictionary*thelocation= [includedLocations objectAtIndex:indexPath.row];
        CLGeocoder * geoCoder = [[CLGeocoder alloc] init];
        NSNumber* latitude = (NSNumber*) [thelocation objectForKey:@"latitude"];
        NSNumber* longitude= (NSNumber*)[thelocation objectForKey:@"longitude"];
        [geoCoder reverseGeocodeLocation:[[CLLocation alloc]initWithLatitude:latitude.doubleValue longitude:longitude.doubleValue] completionHandler:^(NSArray *placemarks, NSError *error) {
            for (CLPlacemark * placemark in placemarks) {
                NSDictionary *addressDictionary =
                placemark.addressDictionary;
                
                NSLog(@"%@ ", addressDictionary);
                NSString *address = [addressDictionary
                                     objectForKey:(NSString *)kABPersonAddressStreetKey];
                [cell.textLabel setText:address];
                [cell.detailTextLabel setText:[addressDictionary objectForKey:@"Name"]];
            }
        }];
    }
    
    //cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    // Configure the cell...
    
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}


@end
