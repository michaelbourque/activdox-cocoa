//
//  ADXCollectionDocumentsViewController.m
//  Reader
//
//  Created by Gavin McKenzie on 2013-04-13.
//  Copyright (c) 2013 ActivDox Inc. All rights reserved.
//

#import "ADXCollectionDocumentsViewController.h"
#import "ADXPopoverBackgroundView.h"

@interface ADXCollectionDocumentsViewController () <ADXCollectionDocumentsActionsDelegate>
@end

@implementation ADXCollectionDocumentsViewController

#pragma mark - Class Methods

+ (UIPopoverController *)popoverControllerWithTitle:(NSString *)title collection:(ADXCollection *)collection allowEditing:(BOOL)allowEditing startInEditMode:(BOOL)startInEditMode allowMultiSelect:(BOOL)allowMultiSelect allowEditMultiSelect:(BOOL)allowEditMultiSelect showOnlyMissingDocuments:(BOOL)showOnlyMissingDocuments actionDelegate:(id)actionDelegate
{
    UIPopoverController *popover = nil;
    ADXCollectionDocumentsViewController *viewController = [[self alloc] initWithNibName:nil bundle:nil];
    viewController.requestedTitle = title;
    viewController.allowEditing = allowEditing;
    viewController.startInEditMode = startInEditMode;
    viewController.allowMultiSelect = allowMultiSelect;
    viewController.allowEditMultiSelect = allowEditMultiSelect;
    viewController.actionDelegate = actionDelegate;
    viewController.collection = collection;
    viewController.showOnlyMissingDocuments = showOnlyMissingDocuments;
    
    if (viewController) {
        popover = [[UIPopoverController alloc] initWithContentViewController:[[UINavigationController alloc] initWithRootViewController:viewController]];
        popover.popoverBackgroundViewClass = [ADXBluePopoverBackgroundView class];
    }
    viewController.parentPopoverController = popover;
    
    return popover;
}

#pragma mark - Accessors

- (void)setCollection:(ADXCollection *)collection
{
    _collection = collection;
    self.accountID = collection.accountID;
}

#pragma mark - Table View

- (NSString *)messageForAddRowCell
{
    return @" Add Documents";
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (!self.editing) {
        if ([self.actionDelegate respondsToSelector:@selector(didSelectDocument:)]) {
            ADXDocument *document = [self objectAtIndexPath:indexPath tableView:tableView];
            [(id<ADXCollectionDocumentsActionsDelegate>)self.actionDelegate didSelectDocument:document];
        }
    } else {
        ADXCollectionDocumentsViewController *listViewController = [self newDocumentListView];
        [self.navigationController pushViewController:listViewController animated:YES];
    }
    [super tableView:tableView didSelectRowAtIndexPath:indexPath];
}

- (NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return @"Remove";
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        ADXDocument *document = [self objectAtIndexPath:indexPath tableView:tableView];
        if ([self.actionDelegate respondsToSelector:@selector(didRemoveDocument:fromCollection:)]) {
            [(id<ADXCollectionDocumentsActionsDelegate>)self.actionDelegate didRemoveDocument:document fromCollection:self.collection];
        }
    } else {
        ADXCollectionDocumentsViewController *listViewController = [self newDocumentListView];
        [self.navigationController pushViewController:listViewController animated:YES];
    }
}

- (ADXCollectionDocumentsViewController *)newDocumentListView
{
    ADXCollectionDocumentsViewController *listViewController = [[ADXCollectionDocumentsViewController alloc] initWithNibName:nil bundle:nil];
    listViewController.collection = self.collection;
    listViewController.parentPopoverController = self.parentPopoverController;
    listViewController.allowEditing = NO;
    listViewController.actionDelegate = self;
    listViewController.showOnlyMissingDocuments = YES;
    listViewController.requestedTitle = @"Add Documents";

    if (!self.collection.numberOfDocuments) {
        listViewController.startInEditMode = YES;
    }
    return listViewController;
}

#pragma mark - Data Management

- (NSPredicate *)predicateForListFetchedResultsController
{
    NSPredicate *predicate = nil;
    ADXCollection *collection = [ADXCollection collectionWithID:self.collection.id orLocalID:self.collection.localID accountID:self.collection.accountID context:self.managedObjectContext];

    if (self.showOnlyMissingDocuments) {
        predicate = [NSPredicate predicateWithFormat:@"(%K == %@) AND (%K == %@) AND SUBQUERY(collections, $x, $x == %@).@count == 0",
                     ADXDocumentAttributes.accountID, self.collection.accountID,
                     ADXDocumentAttributes.versionIsLatest, [NSNumber numberWithBool:YES],
                     collection];
    } else {
        // No need to include account id in predicate, as collection ids are guaranteed to be unique across accounts.
        predicate = [NSPredicate predicateWithFormat:@"(%K == %@) AND SUBQUERY(collections, $x, $x == %@).@count > 0", ADXDocumentAttributes.versionIsLatest, [NSNumber numberWithBool:YES], self.collection];
    }
    return predicate;
}

- (NSPredicate *)predicateForSearchFetchedResultsController
{
    NSPredicate *predicate = nil;
    NSString *searchText = self.searchDisplayController.searchBar.text;
    ADXCollection *collection = [ADXCollection collectionWithID:self.collection.id orLocalID:self.collection.localID accountID:self.collection.accountID context:self.managedObjectContext];

    if (self.showOnlyMissingDocuments) {
        predicate = [NSPredicate predicateWithFormat:@"(%K == %@) AND (%K == %@) AND (%K CONTAINS[cd] %@) AND SUBQUERY(collections, $x, $x == %@).@count == 0",
                     ADXDocumentAttributes.accountID, self.collection.accountID,
                     ADXDocumentAttributes.versionIsLatest, [NSNumber numberWithBool:YES],
                     ADXDocumentAttributes.title, searchText,
                     collection];
    } else {
        // No need to include account id in predicate, as collection ids are guaranteed to be unique across accounts.
        predicate = [NSPredicate predicateWithFormat:@"(%K == %@) AND SUBQUERY(collections, $x, $x == %@).@count > 0 AND (%K CONTAINS[cd] %@)",
                     ADXDocumentAttributes.versionIsLatest, [NSNumber numberWithBool:YES],
                     self.collection,
                     ADXDocumentAttributes.title, searchText];
    }
    return predicate;
}

#pragma mark - ADXCollectionDocumentsActionsDelegate

- (void)didSelectDocument:(ADXDocument *)document
{
    if ([self.actionDelegate respondsToSelector:@selector(didAddDocument:toCollection:)]) {
        [(id<ADXCollectionDocumentsActionsDelegate>)self.actionDelegate didAddDocument:document toCollection:self.collection];
    }
}

@end
