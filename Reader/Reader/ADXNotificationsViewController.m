//
//  ADXNotificationsViewController.m
//  Reader
//
//  Created by Gavin McKenzie on 2013-02-14.
//  Copyright (c) 2013 ActivDox Inc. All rights reserved.
//

#import "ADXNotificationsViewController.h"
#import "ADXNotificationCell.h"
#import "ADXNotificationSimpleCell.h"
#import "ADXPopoverBackgroundView.h"

#import "ActivDoxCore.h"

NSString * const kADXNotificationsOptionKeyFilterType = @"filterType";
NSString * const kADXNotificationsOptionValueFilterTypeDocument = @"document";
NSString * const kADXNotificationsOptionValueFilterTypeCollection = @"collection";
NSString * const kADXNotificationsOptionKeyFilterObjectID = @"filterObjectID";

@interface ADXNotificationsViewController () <NSFetchedResultsControllerDelegate>
@property (strong, nonatomic) ADXV2Service *service;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) NSDictionary *options;
@property (assign, nonatomic) BOOL applyFilter;
@end

@implementation ADXNotificationsViewController

#pragma mark - Class Methods

+ (ADXNotificationsViewController *)viewControllerWithAccount:(NSString *)accountID actionDelegate:(id)delegate options:(NSDictionary *)options managedObjectContext:(NSManagedObjectContext *)moc
{
    ADXNotificationsViewController *viewController = [[ADXNotificationsViewController alloc] initWithNibName:nil bundle:nil];
    
    viewController.accountID = accountID;
    viewController.managedObjectContext = moc;
//    viewController.contentSizeForViewInPopover = [viewController.view sizeThatFits:CGSizeZero];
    viewController.service = [ADXV2Service serviceForAccountID:accountID];
    viewController.actionDelegate = delegate;
    viewController.options = options;
    
    return viewController;
}

+ (UIPopoverController *)popoverControllerWithAccount:(NSString *)accountID actionDelegate:(id)delegate options:(NSDictionary *)options managedObjectContext:(NSManagedObjectContext *)moc
{
    UIPopoverController *popover = nil;
    ADXNotificationsViewController *viewController = [ADXNotificationsViewController viewControllerWithAccount:accountID actionDelegate:delegate options:options managedObjectContext:moc];
    
    if (viewController) {
        popover = [[UIPopoverController alloc] initWithContentViewController:[[UINavigationController alloc] initWithRootViewController:viewController]];
        [popover setPopoverContentSize:CGSizeMake(kADXNotificationPopoverCellWidth, 480.0f) animated:NO];
        viewController.parentPopoverController = popover;
    }

    if (viewController) {
        popover = [[UIPopoverController alloc] initWithContentViewController:[[UINavigationController alloc] initWithRootViewController:viewController]];
        popover.popoverBackgroundViewClass = [ADXBluePopoverBackgroundView class];
    }

    return popover;
}

+ (UIViewController *)modalControllerWithAccount:(NSString *)accountID actionDelegate:(id)delegate options:(NSDictionary *)options managedObjectContext:(NSManagedObjectContext *)moc
{
    ADXNotificationsViewController *viewController = [ADXNotificationsViewController viewControllerWithAccount:accountID actionDelegate:delegate options:options managedObjectContext:moc];
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:viewController];
    viewController.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:viewController action:@selector(didTapDoneButton:)];
    viewController.parentPopoverController = nil;
    
    return navigationController;
}

#pragma mark - Initialization

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

#pragma mark - View Management

- (void)viewDidLoad
{
    [super viewDidLoad];

    if (self.options) {
        NSString *filterType = self.options[kADXNotificationsOptionKeyFilterType];
        if ([filterType isEqualToString:kADXNotificationsOptionValueFilterTypeDocument]) {
            self.applyFilter = [ADXUserDefaults sharedDefaults].activityViewFilterThisDocument;
        } else if ([filterType isEqualToString:kADXNotificationsOptionValueFilterTypeCollection]) {
            self.applyFilter = [ADXUserDefaults sharedDefaults].activityViewFilterThisCollection;
        }
    }
    
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([ADXNotificationCell class]) bundle:[NSBundle mainBundle]] forCellReuseIdentifier:NSStringFromClass([ADXNotificationCell class])];
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([ADXNotificationSimpleCell class]) bundle:[NSBundle mainBundle]] forCellReuseIdentifier:NSStringFromClass([ADXNotificationSimpleCell class])];
    
    self.tableView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"popover_bg"]];
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
}

- (void)setupToolbar
{
    if (!self.options || !self.options[kADXNotificationsOptionKeyFilterType]) {
        return;
    }
    
    NSString *filterType = self.options[kADXNotificationsOptionKeyFilterType];
    NSString *filterLabel = nil;

    BOOL shouldFilter = NO;
    if ([filterType isEqualToString:kADXNotificationsOptionValueFilterTypeDocument]) {
        shouldFilter = [ADXUserDefaults sharedDefaults].activityViewFilterThisDocument;
        filterLabel = @"This Document";
    } else if ([filterType isEqualToString:kADXNotificationsOptionValueFilterTypeCollection]) {
        shouldFilter = [ADXUserDefaults sharedDefaults].activityViewFilterThisCollection;
        filterLabel = @"This Collection";
    } else {
        return;
    }
    
    NSArray *segmentedItems = [NSArray arrayWithObjects:@"All Activity", filterLabel, nil];
    UISegmentedControl *segmentedButton = [[UISegmentedControl alloc] initWithItems:segmentedItems];
    segmentedButton.segmentedControlStyle = UISegmentedControlStyleBar;
    segmentedButton.selectedSegmentIndex = shouldFilter ? 1 : 0;
    [segmentedButton addTarget:self action:@selector(didChangeFilterSelection:) forControlEvents:UIControlEventValueChanged];
    
    UIBarButtonItem *flexibleSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    UIBarButtonItem *buttonItem = [[UIBarButtonItem alloc] initWithCustomView:segmentedButton];
    segmentedButton.frame = CGRectMake(0.0f, 0.0f, self.view.frame.size.width - 20.0f, 30.0f);
    [self setToolbarItems:@[flexibleSpace, buttonItem, flexibleSpace]];
    
    [self.navigationController setToolbarHidden:NO animated:YES];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.title = @"Activity";
    [self setupToolbar];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    __weak ADXNotificationsViewController *weakSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        [weakSelf refresh];
    });
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    if (self.isMovingFromParentViewController) {
        self.fetchedResultsController.delegate = nil;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setTableView:nil];
    [super viewDidUnload];
}

- (void)refresh
{
    __weak ADXNotificationsViewController *weakSelf = self;
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        // Hide the table view while we reconfigure the popover
//        [self.tableView setHidden:YES];
    }
    [self.service refreshEvents:^(BOOL success, NSError *error) {
        if (!success) {
            LogError(@"Error refreshing events");
        }
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            [weakSelf.tableView reloadData];
        }];
    }];
}

#pragma mark - Table View Datasource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:0];
    NSInteger result = [sectionInfo numberOfObjects];
    return result;
}

- (void)configureCell:(ADXNotificationCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    ADXEvent *event = [self.fetchedResultsController objectAtIndexPath:indexPath];
    NSString *documentID = event.resource[ADXJSONDocumentAttributes.id];
    ADXDocument *document = [ADXDocument latestDocumentWithID:documentID accountID:self.accountID context:self.managedObjectContext];

    if (document && document.thumbnail.data) {
        cell.thumbnailImageView.image = [UIImage imageWithData:document.thumbnail.data];
        cell.thumbnailImageView.backgroundColor = [UIColor clearColor];
        cell.notAvailableMessage.hidden = YES;
    } else {
        cell.thumbnailImageView.image = nil;
        cell.thumbnailImageView.backgroundColor = [UIColor whiteColor];
        cell.notAvailableMessage.hidden = NO;
    }
    
    cell.title.text = event.resource[ADXJSONDocumentAttributes.title];
    cell.timeStamp.text = [event.timeStamp stringFromDateCapitalized:YES];

    if (self.applyFilter) {
        cell.message.text = [event fullDescription];
    } else {
        cell.message.text = [event shortDescription];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ADXNotificationCell *cell = nil;
    NSString *cellIdentifier = nil;
    
    if (self.applyFilter) {
        cellIdentifier = NSStringFromClass([ADXNotificationSimpleCell class]);
    } else {
        cellIdentifier = NSStringFromClass([ADXNotificationCell class]);
    }
    cell = (ADXNotificationCell *)[self.tableView dequeueReusableCellWithIdentifier:cellIdentifier];    
    
    [self configureCell:cell forRowAtIndexPath:indexPath];
    return cell;
}

#pragma mark - Table View Delegate


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat result = 0.0f;
    
    if (self.applyFilter) {
        result = kADXNotificationPopoverSimpleCellHeight;
    } else {
        result = kADXNotificationPopoverCellHeight;
    }
    return result;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([self.actionDelegate respondsToSelector:@selector(didSelectDocument:fromEvent:)]) {
        ADXEvent *event = [self.fetchedResultsController objectAtIndexPath:indexPath];
        NSString *documentID = event.payload[ADXJSONEventAttributes.payload.document];
        NSNumber *versionID = event.payload[ADXJSONEventAttributes.payload.version];
        if (!IsEmpty(documentID)) {
            ADXDocument *document = nil;
            if (!versionID) {
                document = [ADXDocument latestDocumentWithID:documentID accountID:self.accountID context:self.managedObjectContext];
            } else {
                document = [ADXDocument documentWithID:documentID version:versionID accountID:self.accountID context:self.managedObjectContext];
            }
            if (document && document.accessible) {
                [(id<ADXNotificationsActionsDelegate>)self.actionDelegate didSelectDocument:document fromEvent:event];
            }
        }
    }

    [self.tableView deselectRowAtIndexPath:indexPath animated:NO];
}

#pragma mark - Data Management

- (void)updateFetchedResultsControllerPredicate
{
    if (!self.managedObjectContext)
        return;
    
    NSPredicate *predicate = nil;
    if (self.applyFilter) {
        NSString *filterObjectID = self.options[kADXNotificationsOptionKeyFilterObjectID];
        predicate = [NSPredicate predicateWithFormat:@"(%K == %@) AND (%K != %@) AND (%K == %@)", ADXEventAttributes.accountID, self.accountID, ADXEventAttributes.originatorID, self.accountID, ADXEventAttributes.referencedDocumentID, filterObjectID];
    } else {
        predicate = [NSPredicate predicateWithFormat:@"(%K == %@) AND (%K != %@)", ADXEventAttributes.accountID, self.accountID, ADXEventAttributes.originatorID, self.accountID];
    }
    [_fetchedResultsController.fetchRequest setPredicate:predicate];
}

- (void)refetchFetchedResultsController
{
    if (!self.managedObjectContext)
        return;
    
    __weak NSFetchedResultsController *frc = _fetchedResultsController;
    
    [self.managedObjectContext performBlockAndWait:^{
        NSError *error = nil;
        BOOL success = [frc performFetch:&error];
        if (!success) {
            LogNSError(@"Couldn't initialize NSFetchedResultsController", error);
        }
    }];
}

- (NSFetchedResultsController *)prepareFetchedResultsController
{
    __block NSFetchedResultsController *frc = nil;
    __weak ADXNotificationsViewController *weakSelf = self;
    
    if (!self.managedObjectContext)
        return nil;
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:ADXEventAttributes.timeStamp ascending:NO];
    NSManagedObjectContext *moc = self.managedObjectContext;
    
    [moc performBlockAndWait:^{
        frc = [moc aps_fetchedResultsControllerForEntityName:[ADXEvent entityName]
                                                   predicate:nil
                                             sortDescriptors:[NSArray arrayWithObject:sortDescriptor]
                                          sectionNameKeyPath:nil
                                                   cacheName:nil
                                                    delegate:weakSelf];
    }];
    return frc;
}

- (NSFetchedResultsController *)fetchedResultsController
{
    if (!_fetchedResultsController) {
        _fetchedResultsController = [self prepareFetchedResultsController];
        _fetchedResultsController.delegate = self;
        [self updateFetchedResultsControllerPredicate];
        [self refetchFetchedResultsController];
    }
    return _fetchedResultsController;
}

#pragma mark - NSFetchedResultsControllerDelegate

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView reloadData];
}

#pragma mark - Actions

- (void)didChangeFilterSelection:(id)sender
{
    UISegmentedControl *control = sender;
    if (control.selectedSegmentIndex == 0) {
        self.applyFilter = NO;
    } else {
        self.applyFilter = YES;
    }
    
    
    NSString *filterType = self.options[kADXNotificationsOptionKeyFilterType];
    if ([filterType isEqualToString:kADXNotificationsOptionValueFilterTypeDocument]) {
        [ADXUserDefaults sharedDefaults].activityViewFilterThisDocument = self.applyFilter;
    }
    if ([filterType isEqualToString:kADXNotificationsOptionValueFilterTypeCollection]) {
        [ADXUserDefaults sharedDefaults].activityViewFilterThisCollection = self.applyFilter;
    }
    
    [self updateFetchedResultsControllerPredicate];
    [self refetchFetchedResultsController];
    
    [self.tableView reloadData];
}

@end
