//
//  ADXPSPDFSearchHighlightView.m
//  Reader
//
//  Created by Steve Tibbett on 2013-02-14.
//  Copyright (c) 2013 ActivDox Inc. All rights reserved.
//

#import "ADXPSPDFSearchHighlightView.h"

@implementation ADXPSPDFSearchHighlightView

- (id)initWithSearchResult:(PSPDFSearchResult *)searchResult {
    if ((self = [super initWithSearchResult:nil])) {
        self.selectionBackgroundColor = [UIColor colorWithRed:0.0 green:1.0 blue:1.0 alpha:0.5];
        self.searchResult = searchResult;
        [self setNeedsLayout];
    }
    return self;
}

@end
