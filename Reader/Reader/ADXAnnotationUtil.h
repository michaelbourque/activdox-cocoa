//
//  ADXAnnotationUtil.h
//  Reader
//
//  Created by matteo ugolini on 2012-10-03.
//  Copyright (c) 2012 ActivDox Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "ActivDoxCore.h"
#import "PSPDFDocument.h"

extern NSString *kADXAnnotationFilterChangedNotification;


#define ADXHighlightAnnotationColor [UIColor colorWithRed:238/255.0 green:238/255.0 blue:0/255.0 alpha:0.5]
#define ADXNoteAnnotationOpenColor [UIColor whiteColor]
#define ADXNoteAnnotationClosedColor [UIColor colorWithWhite:0.75f alpha:1.0f]

@protocol ADXAnnotationProtocol <NSObject>
@property (strong, nonatomic) NSString *noteLocalID;
@property (strong, nonatomic) NSString *noteID;
@property (strong, nonatomic) NSString *documentID;
@property (strong, nonatomic) NSNumber *versionID;
@property (strong, nonatomic) NSString *accountID;
@property (strong, nonatomic) UIColor *color;
@end

@interface ADXAnnotationUtil : NSObject
+ (void)addAnnotationsToPDF:(PSPDFDocument*)pdfDocument fromDocument:(ADXDocument*)adxDocument;
+ (ADXDocumentNote *)noteFromAnnotation:(id<ADXAnnotationProtocol>)annotation;
+ (ADXDocument *)documentFromAnnotation:(id<ADXAnnotationProtocol>)annotation;
+ (BOOL)currentUserIsAuthorOfAnnotation:(id<ADXAnnotationProtocol>)annotation;
+ (BOOL)currentUserIsAuthorOfDocumentForAnnotation:(id<ADXAnnotationProtocol>)annotation;
+ (UIColor *)colorForAnnotation:(id<ADXAnnotationProtocol>)annotation note:(ADXDocumentNote *)note;
@end
