//
//  ADXDocInfoViewController.m
//  Reader
//
//  Created by Gavin McKenzie on 2012-09-13.
//  Copyright (c) 2012 ActivDox Inc. All rights reserved.
//

#import "ADXDocInfoViewController.h"
#import "ADXBarButtonItem.h"
#import "ADXPopoverController.h"
#import "PSPDFKit.h"


@interface ADXDocInfoViewController () <PSPDFCacheDelegate>
@property (strong, nonatomic) PSPDFDocument *pdfDocument;
@property (strong, nonatomic) UIBarButtonItem *shareButton;
@end

@implementation ADXDocInfoViewController

#pragma mark - Class Methods

+ (UIPopoverController *)popoverControllerWithDocument:(ADXDocument *)document thumbnail:(UIImage *)thumbnail actionDelegate:(id)delegate
{
    UIPopoverController *popover = nil;
    ADXDocInfoViewController *viewController = [[ADXDocInfoViewController alloc] initWithNibName:nil bundle:nil];
    viewController.document = document;
    viewController.thumbnail = thumbnail;
//    viewController.contentSizeForViewInPopover = [viewController.view sizeThatFits:CGSizeZero];
    viewController.actionDelegate = delegate;
    
    if (viewController) {
        popover = [[ADXPopoverController alloc] initWithContentViewController:[[UINavigationController alloc] initWithRootViewController:viewController]];
    }
    return popover;
}

#pragma mark - Initialization

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [self finishInitialization];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self finishInitialization];
    }
    return self;
}

- (void)finishInitialization
{
    [[PSPDFCache sharedCache] addDelegate:self];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateRightBarButtons) name:kADXServiceNotificationReachabilityChanged object:nil];
}

#pragma mark - Destruction

- (void)dealloc
{
    [self removeDocumentObservers];
    [[PSPDFCache sharedCache] removeDelegate:self];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kADXServiceNotificationReachabilityChanged object:nil];
}

#pragma mark - Notifications

- (void)addDocumentObservers
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleThumbnailDownloadedNotification:) name:kADXServiceDocumentThumbnailDownloadedNotification object:nil];
}

- (void)removeDocumentObservers
{
    @try {
        [[NSNotificationCenter defaultCenter] removeObserver:self name:kADXServiceDocumentThumbnailDownloadedNotification object:nil];
    }
    @catch (NSException *exception) {
        // Do nothing.
    }
}

#pragma mark - Helpers

- (NSString *)publicLabelText
{
    if (self.document.isPublicValue) {
        return @"Public document";
    } else {
        return @"Private document";
    }
}

- (NSString *)documentDescriptionText
{
    if (!IsEmpty(self.document.documentDescription)) {
        return self.document.documentDescription;
    } else {
        return @"None";
    }
}

- (NSString *)authorMessageText
{
    if (!IsEmpty(self.document.message)) {
        return self.document.message;
    } else {
        return @"None";
    }
}

- (PSPDFDocument *)pdfDocument
{
    if (!_pdfDocument && self.document.downloaded && self.document.content.data) {
        _pdfDocument = [[PSPDFDocument alloc] initWithData:self.document.content.data];
    }
    return _pdfDocument;
}

#pragma mark - Accessors

- (UIBarButtonItem *)shareButton
{
    if (!_shareButton) {
        _shareButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAction target:self action:@selector(didTapShareButton:)];
    }
    return _shareButton;
}

- (void)setHideShareButton:(BOOL)hideShareButton
{
    _hideShareButton = hideShareButton;
    [self updateRightBarButtons];
}

#pragma mark - View Management

- (CGSize)contentSizeForViewInPopover
{
    CGSize result = CGSizeMake(320.0f, 436.0f);
    return result;
}

- (void)updateRightBarButtons
{
    ADXV2Service *service = [ADXV2Service serviceForManagedObject:self.document];
    NSMutableArray *rightBarButtons = [[NSMutableArray alloc] initWithCapacity:2];

    if (self.document.downloaded || service.networkReachable ) {
        UIBarButtonItem *openButton = [[UIBarButtonItem alloc] initWithTitle:@"Open" style:UIBarButtonItemStyleBordered target:self action:@selector(didTapOpenButton:)];
        
        [rightBarButtons addObject:openButton];
    } else {
        UIBarButtonItem *openButton = [[UIBarButtonItem alloc] initWithCustomView:self.unavailableLabel];
        openButton.enabled = NO;
        [rightBarButtons addObject:openButton];
    }
    
    if (!self.hideShareButton) {
        if (self.document.isPublicValue) {
            [rightBarButtons addObject:self.shareButton];
        }
    }

    UIBarButtonItem *separator = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    separator.width = 8;

    [rightBarButtons insertObject:separator atIndex:0];
    
    self.navigationItem.rightBarButtonItems = rightBarButtons;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self updateRightBarButtons];
    self.view.backgroundColor = [UIColor colorWithWhite:0.196 alpha:1.000];
    
    if (!self.thumbnail) {
        ADXV2Service *service = [ADXV2Service serviceForManagedObject:self.document];
        ADXDocumentThumbnail *documentThumbnail = self.document.thumbnail;
        
        if (documentThumbnail && !IsEmpty(documentThumbnail.data)) {
            self.thumbnail = [UIImage imageWithData:documentThumbnail.data];
        } else {
            [self removeDocumentObservers];
            [self addDocumentObservers];
            [service downloadThumbnailForDocument:self.document.id versionID:self.document.version completion:nil];
        }
    }
    [self updateThumbnail];

    self.titleLabel.text = self.document.title;
    self.authorLabel.text = self.document.author.displayNameOrLoginName;
    self.versionLabel.text = [self.document versionDisplayLabel];
    self.dateLabel.text = [self.document publishedDateDisplayLabel:YES];
    self.publicLabel.text = [self publicLabelText];
    self.descriptionLabel.text = [self documentDescriptionText];
    [self.descriptionLabel sizeToFit];
    self.authorMessageLabel.text = [self authorMessageText];
    [self.authorMessageLabel sizeToFit];
    
    if (self.document.errorMessage.length > 0) {
        self.errorMessageLabel.hidden = NO;
        self.errorMessageLabel.text = [NSString stringWithFormat:@"\u26A0 %@", self.document.errorMessage];
    } else {
        [self.errorMessageLabel removeFromSuperview];
//        self.errorMessageLabel.hidden = YES;
//        CGRect errorMessageFrame = self.errorMessageLabel.frame;
//        errorMessageFrame.size.height = 0;
//        [self.errorMessageLabel setFrame:errorMessageFrame];
    }
    
    self.imageView.layer.shadowColor = [UIColor blackColor].CGColor;
    self.imageView.layer.shadowOffset = CGSizeMake(0, 1.0);
    self.imageView.layer.shadowOpacity = 0.5;
    self.imageView.layer.shadowRadius = 2.0;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"popover_bg"]];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self.scrollView flashScrollIndicators];
}

- (void)updateThumbnail
{
    if (self.thumbnail) {
        [self presentThumbnailImage:self.thumbnail];
    } else {
        [self presentEmptyThumbnailImage];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setImageView:nil];
    [self setTitleLabel:nil];
    [self setAuthorLabel:nil];
    [self setVersionLabel:nil];
    [self setDateLabel:nil];
    [self setDescriptionLabel:nil];
    [self setAuthorMessageLabel:nil];
    [self setPublicLabel:nil];
    [self setUnavailableLabel:nil];
    [self setErrorMessageLabel:nil];
    [self setScrollView:nil];
    [super viewDidUnload];
}

#pragma mark - Thumbnail

- (void)presentThumbnailImage:(UIImage *)thumbnail
{
    self.imageView.image = thumbnail;
}

- (void)presentEmptyThumbnailImage
{
    self.imageView.image = nil;
}

#pragma mark - Notification Handling

- (void)handleThumbnailDownloadedNotification:(NSNotification *)notification
{
    NSString *documentID = [notification.userInfo valueForKey:kADXServiceNotificationPayload];
    if (![documentID isEqualToString:self.document.id]) {
        return;
    }
    
    __weak ADXDocInfoViewController *weakSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        [weakSelf updateThumbnail];
    });
}

#pragma mark - Actions

- (void)didTapOpenButton:(id)sender
{
    if ([self.actionDelegate respondsToSelector:@selector(didSelectDocOpenAction:document:)]) {
        [self.actionDelegate didSelectDocOpenAction:self document:self.document];
    }
}

- (void)didTapShareButton:(id)sender
{
    if ([self.actionDelegate respondsToSelector:@selector(didSelectDocShareAction:document:)]) {
        [self.actionDelegate didSelectDocShareAction:self document:self.document];
    }
}

#pragma mark - PSPDFCacheDelegate

- (void)didCachePageForDocument:(PSPDFDocument *)document page:(NSUInteger)page image:(UIImage *)cachedImage size:(PSPDFSize)size
{
    if (document == self.pdfDocument && page == 0 && size == PSPDFSizeThumbnail) {
        self.thumbnail = cachedImage;
        [self updateThumbnail];
    }
}

@end
