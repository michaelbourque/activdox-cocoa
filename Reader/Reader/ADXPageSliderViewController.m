//
//  ADXPageSliderViewController.m
//  Reader
//
//  Created by Steve Tibbett on 2013-04-03.
//  Copyright (c) 2013 ActivDox Inc. All rights reserved.
//

#import "ADXPageSliderViewController.h"

@interface ADXPageSliderViewController ()
@property (weak, nonatomic) IBOutlet UILabel *pageNumberLabel;
@property (strong, nonatomic) ADXPageSliderNavigateBlock navigateBlock;
@property (weak, nonatomic) IBOutlet UISlider *pageSlider;
@property (assign, nonatomic) int numPages;
@end

@implementation ADXPageSliderViewController

- (id)initWithNumPages:(int)numPages navigateBlock:(ADXPageSliderNavigateBlock)navigateBlock
{
    self = [super initWithNibName:@"ADXPageSliderViewController" bundle:nil];
    if (self) {
        self.navigateBlock = navigateBlock;
        self.numPages = numPages;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if (self.numPages == 1) {
        self.pageSlider.hidden = YES;
    } else {
        self.pageSlider.minimumValue = 0;
        self.pageSlider.maximumValue = self.numPages - 0.01;
    }
    
    [self refresh];
}

- (void)setCurrentPage:(int)currentPage
{
    self.pageSlider.value = currentPage;
    [self refresh];
}

- (void)refresh
{
    self.pageNumberLabel.text = [NSString stringWithFormat:NSLocalizedString(@"Page %d of %d", nil), (int)floor(self.pageSlider.value) + 1, self.numPages];
}

- (IBAction)pageSliderChanged:(id)sender
{
    self.navigateBlock(floor(self.pageSlider.value));
    [self refresh];
}

@end
