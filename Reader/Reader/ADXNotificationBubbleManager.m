//
//  ADXNotificationBubbleManager.m
//  Reader
//
//  Created by Gavin McKenzie on 2013-03-26.
//  Copyright (c) 2013 ActivDox Inc. All rights reserved.
//

#import "ADXNotificationBubbleManager.h"
#import "ADXNotificationBubbleContainerView.h"

static CGFloat kADXBubbleOffset = 57;

@interface ADXNotificationBubbleManager ()
@property (strong, nonatomic) NSMutableArray *bubbles;
@property (weak, nonatomic) ADXNotificationBubbleContainerView *bubbleContainerView;
@end

@implementation ADXNotificationBubbleManager 

#pragma mark - Class Methods

+ (ADXNotificationBubbleManager *)sharedInstance
{
    static ADXNotificationBubbleManager *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[ADXNotificationBubbleManager alloc] init];
    });
    return sharedInstance;
}

#pragma mark - Initialization

- (id)init
{
    self = [super init];
    if (self) {
        self.bubbles = [[NSMutableArray alloc] init];
    }
    return self;
}

#pragma mark - Helpers

- (UIWindow *)keyWindow
{
    return [[UIApplication sharedApplication] keyWindow];
}

- (void)validateContainerView
{
    if (!self.bubbleContainerView) {
        CGRect viewFrame = self.keyWindow.screen.applicationFrame;
        ADXNotificationBubbleContainerView *containerView = [[ADXNotificationBubbleContainerView alloc] initWithFrame:viewFrame];
        containerView.backgroundColor = [UIColor clearColor];
        containerView.autoresizesSubviews = YES;
        
        self.bubbleContainerView = containerView;
        [self.keyWindow addSubview:containerView];
    }
}

#pragma mark - Bubble Display

- (ADXNotificationBubble *)displayBubbleWithMessage:(NSString *)message userInfo:(NSDictionary *)userInfo
{
    ADXNotificationBubble *bubble = [self prepareBubbleWithMessage:message sticky:NO actionLabel:nil];
    bubble.userInfo = userInfo;

    [self animateBubbleIn:bubble];
    return bubble;
}

- (ADXNotificationBubble *)displayStickyBubbleWithMessage:(NSString *)message actionLabel:(NSString *)actionLabel userInfo:(NSDictionary *)userInfo delegate:(id<ADXNotificationBubbleManagerDelegate>)delegate
{
    ADXNotificationBubble *bubble = [self prepareBubbleWithMessage:message sticky:YES actionLabel:actionLabel];
    bubble.userInfo = userInfo;
    bubble.privateStickyDelegate = delegate;

    [self animateBubbleIn:bubble];
    return bubble;
}

- (void)removeBubble:(ADXNotificationBubble *)bubble
{
    if (!bubble) {
        return;
    }
    
    [self animateBubbleOut:bubble];
}

- (ADXNotificationBubble *)prepareBubbleWithMessage:(NSString *)message sticky:(BOOL)sticky actionLabel:(NSString *)actionLabel
{
    ADXNotificationBubble *bubble = [ADXNotificationBubble newBubble];
    bubble.message = message;
    bubble.delegate = self;

    if (actionLabel) {
        bubble.stickyActionLabel = actionLabel;
    }

    bubble.sticky = sticky;
    
    return bubble;
}

- (void)animateBubbleIn:(ADXNotificationBubble *)bubble
{
    [self validateContainerView];

    CGFloat offset = kADXBubbleOffset;
    CGRect bubbleFrame = bubble.frame;
    bubbleFrame.origin.y = -bubbleFrame.size.height;
    bubbleFrame.origin.x = self.bubbleContainerView.bounds.size.width - bubbleFrame.size.width - 11;
    bubble.frame = bubbleFrame;
    bubble.alpha = 0.0f;
    bubble.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
    
    [self.bubbleContainerView addSubview:bubble];
    [self.bubbleContainerView bringSubviewToFront:bubble];
    
    [UIView animateWithDuration:0.4 delay:0 options:UIViewAnimationOptionCurveLinear animations:^{
        if ([self.bubbles count]) {
            [self shiftBubblesDown];
        }
        [self.bubbles insertObject:bubble atIndex:0];
        
        bubble.frame = CGRectOffset(bubble.frame, 0.0, bubbleFrame.size.height + offset);
        bubble.alpha = 1.0f;
    } completion:^(BOOL finished) {
        if (!bubble.sticky) {
            [bubble startTimer];
        }
    }];
}

- (void)animateBubbleOut:(ADXNotificationBubble *)bubble
{
    CGFloat offset = kADXBubbleOffset;
    
    [UIView animateWithDuration:0.4 delay:0 options:UIViewAnimationOptionCurveLinear animations:^{
        bubble.frame = CGRectOffset(bubble.frame, bubble.frame.size.width + offset, 0.0);
        bubble.alpha = 0.0f;
        NSInteger bubbleIndex = [self.bubbles indexOfObjectIdenticalTo:bubble];
        if (bubbleIndex < (self.bubbles.count - 1)) {
            [self shiftBubblesUpStartingAtIndex:bubbleIndex + 1];
        }
    } completion:^(BOOL finished) {
        [bubble removeFromSuperview];
        [self.bubbles removeObjectIdenticalTo:bubble];
    }];
}

- (void)shiftBubblesDown
{
    for (ADXNotificationBubble *bubble in self.bubbles) {
        CGRect bubbleFrame = bubble.frame;
        bubble.frame = CGRectOffset(bubble.frame, 0.0, bubbleFrame.size.height);
    }
}

- (void)shiftBubblesUpStartingAtIndex:(NSInteger)index
{
    for (NSInteger i = index; i < self.bubbles.count; i++) {
        ADXNotificationBubble *bubble = [self.bubbles objectAtIndex:i];
        CGRect bubbleFrame = bubble.frame;
        bubble.frame = CGRectOffset(bubble.frame, 0.0, -bubbleFrame.size.height);
    }
}

#pragma mark - ADXNotificationBubbleDelegate

- (void)bubbleDidEndWithTimeout:(ADXNotificationBubble *)bubble
{
    [self animateBubbleOut:bubble];
}

- (void)bubbleDidEndWithTap:(ADXNotificationBubble *)bubble
{
    [self animateBubbleOut:bubble];
}

- (void)stickyBubbleDidEndWithAction:(ADXNotificationBubble *)bubble
{
    [self animateBubbleOut:bubble];
    if ([bubble.privateStickyDelegate conformsToProtocol:@protocol(ADXNotificationBubbleManagerDelegate)]) {
        [bubble.privateStickyDelegate stickyBubbleWasActioned:bubble];
    }
}

- (void)stickyBubbleDidEndWithCancel:(ADXNotificationBubble *)bubble
{
    [self animateBubbleOut:bubble];
    if ([bubble.privateStickyDelegate conformsToProtocol:@protocol(ADXNotificationBubbleManagerDelegate)]) {
        [bubble.privateStickyDelegate stickyBubbleWasCancelled:bubble];
    }
}

@end
