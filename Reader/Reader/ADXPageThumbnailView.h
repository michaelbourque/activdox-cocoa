//
//  ADXPageThumbnailView.h
//  Reader
//
//  Created by Matteo Ugolini on 12-08-15.
//  Copyright (c) 2012 ActivDox Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PSPDFKit.h"
#import "ADXAppCommonMacros.h"

@interface ADXPageThumbnailView : UICollectionViewCell <PSPDFCacheDelegate>

@property (strong, nonatomic) PSPDFDocument *pdfDocument;
@property (strong, nonatomic) NSString *metaInfo;
@property (nonatomic) NSUInteger pageIndex;

@end
