//
//  ADXBaseViewController.h
//  Reader
//
//  Created by Gavin McKenzie on 12-06-20.
//  Copyright (c) 2012 ActivDox Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ADXAppCommonMacros.h"

@interface ADXBaseViewController : UIViewController
@end
