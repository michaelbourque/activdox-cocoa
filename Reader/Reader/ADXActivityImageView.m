//
//  ADXActivityImageView.m
//  Reader
//
//  Created by Gavin McKenzie on 2012-07-19.
//  Copyright (c) 2012 ActivDox Inc. All rights reserved.
//

#import "ADXActivityImageView.h"

@interface ADXActivityImageView ()
@property (strong, nonatomic) UIImageView *imageView;
@end

@implementation ADXActivityImageView
@synthesize imageView = _imageView;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        //
    }
    return self;
}

- (void)willMoveToSuperview:(UIView *)newSuperview
{
    if (newSuperview && !self.imageView) {
        self.imageView = [[UIImageView alloc] initWithFrame:self.bounds];
        self.imageView.animationImages = [NSArray arrayWithObjects:
                                          [UIImage imageNamed:@"loading_1"],
                                          [UIImage imageNamed:@"loading_2"],
                                          [UIImage imageNamed:@"loading_3"],
                                          [UIImage imageNamed:@"loading_4"],
                                          [UIImage imageNamed:@"loading_5"],
                                          [UIImage imageNamed:@"loading_6"],
                                          [UIImage imageNamed:@"loading_7"],
                                          [UIImage imageNamed:@"loading_8"],
                                          [UIImage imageNamed:@"loading_9"],
                                          [UIImage imageNamed:@"loading_10"],
                                          [UIImage imageNamed:@"loading_11"],
                                          [UIImage imageNamed:@"loading_12"],nil];
        self.imageView.animationDuration = 0.6f;
        [self addSubview:self.imageView];
    }
}

- (void)startAnimating
{
    [self.imageView startAnimating];
}

- (void)stopAnimating
{
    [self.imageView stopAnimating];
}

- (BOOL)isAnimating
{
    return [self.imageView isAnimating];
}

@end
