//
//  ADXDocVersionComparisonViewController.m
//  Reader
//
//  Created by Gavin McKenzie on 2012-09-13.
//  Copyright (c) 2012 ActivDox Inc. All rights reserved.
//

#import "ADXDocVersionComparisonViewController.h"
#import "ADXDocVersionsComparisonPopoverCell.h"
#import "ADXPopoverBackgroundView.h"
#import "ADXDocInfoViewController.h"

#import "ActivDoxCore.h"

static CGFloat kADXDocVersionPopoverHeightAdjustment = 37.0f;

@interface ADXDocVersionComparisonViewController () <UITableViewDataSource, UITableViewDelegate, NSFetchedResultsControllerDelegate>
@property (strong, nonatomic) ADXV2Service *service;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (assign, nonatomic) BOOL alreadyRefreshed;
@property (assign, nonatomic) BOOL refreshVersionsSuccess;
@end

@implementation ADXDocVersionComparisonViewController

#pragma mark - Class Methods

+ (ADXDocVersionComparisonViewController *)viewControllerWithDocument:(ADXDocument *)document managedObjectContext:(NSManagedObjectContext *)moc actionDelegate:(id)delegate
{
    ADXDocVersionComparisonViewController *viewController = [[ADXDocVersionComparisonViewController alloc] initWithNibName:nil bundle:nil];

    viewController.document = document;
    viewController.managedObjectContext = moc;
    viewController.contentSizeForViewInPopover = [viewController.view sizeThatFits:CGSizeZero];
    viewController.service = [ADXV2Service serviceForManagedObject:document];
    viewController.actionDelegate = delegate;
    
    return viewController;
}

+ (UIPopoverController *)popoverControllerWithDocument:(ADXDocument *)document managedObjectContext:(NSManagedObjectContext *)moc actionDelegate:(id)delegate
{
    UIPopoverController *popover = nil;
    ADXDocVersionComparisonViewController *viewController = [ADXDocVersionComparisonViewController viewControllerWithDocument:document managedObjectContext:moc actionDelegate:delegate];
    
    if (viewController) {
        popover = [[UIPopoverController alloc] initWithContentViewController:[[UINavigationController alloc] initWithRootViewController:viewController]];
        popover.popoverBackgroundViewClass = [ADXPopoverBackgroundView class];
        [popover setPopoverContentSize:CGSizeMake(kADXDocVersionsComparisonPopoverCellWidth, kADXDocVersionsComparisonPopoverCellHeight + kADXDocVersionPopoverHeightAdjustment) animated:NO];
        viewController.parentPopoverController = popover;
    }

    return popover;
}

+ (UIViewController *)modalControllerWithDocument:(ADXDocument *)document managedObjectContext:(NSManagedObjectContext *)moc actionDelegate:(id)delegate
{
    ADXDocVersionComparisonViewController *viewController = [ADXDocVersionComparisonViewController viewControllerWithDocument:document managedObjectContext:moc actionDelegate:delegate];
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:viewController];
    viewController.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:viewController action:@selector(didTapDoneButton:)];
    viewController.parentPopoverController = nil;
    
    return navigationController;
}

#pragma mark - Initialization

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

#pragma mark - View Management

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"popover_bg"]];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.title = NSLocalizedString(@"Compare", @"");
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];

    __weak ADXDocVersionComparisonViewController *weakSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        [weakSelf refresh];
    });
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    if (self.isMovingFromParentViewController) {
        self.fetchedResultsController.delegate = nil;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setTableView:nil];
    [self setSpinner:nil];
    [super viewDidUnload];
}

- (void)updateAfterRefresh:(BOOL)refreshSuccess error:(NSError *)refreshError
{
    __weak ADXDocVersionComparisonViewController *weakSelf = self;
    
    if (self.refreshVersionsSuccess) {
        self.tableView.tableHeaderView = nil;
    } else {
        self.tableView.tableHeaderView = self.tableHeaderWarningView;
    }
    int64_t delayInSeconds = 0.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [weakSelf.spinner stopAnimating];
        weakSelf.alreadyRefreshed = YES;
        dispatch_async(dispatch_get_main_queue(), ^{
            [weakSelf.tableView setHidden:NO];
            [weakSelf.tableView reloadData];
            CGSize size = CGSizeMake(weakSelf.tableView.contentSize.width, weakSelf.tableView.contentSize.height + kADXDocVersionPopoverHeightAdjustment);
            [weakSelf.parentPopoverController setPopoverContentSize:size animated:YES];
        });
    });
}

- (void)refresh
{
    __weak ADXDocVersionComparisonViewController *weakSelf = self;

    if (self.alreadyRefreshed) {
        CGSize size = CGSizeMake(weakSelf.tableView.contentSize.width, weakSelf.tableView.contentSize.height + kADXDocVersionPopoverHeightAdjustment);
        [weakSelf.parentPopoverController setPopoverContentSize:size animated:YES];
    } else {
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            // Hide the table view while we reconfigure the popover
            [self.tableView setHidden:YES];
        }
        [self.spinner startAnimating];
        [self.service refreshDocumentVersions:self.document.id completion:^(BOOL success, NSError *error) {
            weakSelf.refreshVersionsSuccess = success;
            dispatch_async(dispatch_get_main_queue(), ^{
                [weakSelf updateAfterRefresh:success error:error];
            });
        }];
    }    
}

#pragma mark - Table View Datasource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:0];
    NSInteger result = [sectionInfo numberOfObjects];
    return result;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ADXDocVersionsComparisonPopoverCell *cell = (ADXDocVersionsComparisonPopoverCell *)[self.tableView dequeueReusableCellWithIdentifier:NSStringFromClass([ADXDocVersionsComparisonPopoverCell class])];
    
	if (!cell) {
        UINib *nib = [UINib nibWithNibName:NSStringFromClass([ADXDocVersionsComparisonPopoverCell class]) bundle:[NSBundle mainBundle]];
        cell = [[nib instantiateWithOwner:self options:nil] objectAtIndex:0];
    }
    
    ADXDocument *document = [self.fetchedResultsController objectAtIndexPath:indexPath];
    cell.document = document;
    cell.imageView.image = nil;

    if ([document.version compare:self.document.version] == NSOrderedSame) {
        // Same document version
        cell.versionLabel.text = [cell.versionLabel.text stringByAppendingString:@" (Currently open)"];
    } else if ([document.version compare:self.document.version] == NSOrderedDescending) {
        // We don't support comparison with newer versions
    } else {
        // It's an older version, but we need to check if the change content is available
        if (![self changeContentAvailableToCompareWithDocumentVersionAtIndexPath:indexPath]) {
            cell.versionLabel.text = [cell.versionLabel.text stringByAppendingString:@" (Unavailable)"];
        }
    }
    cell.dateLabel.text = [document publishedDateDisplayLabel:YES];
    cell.enabled = document.version && [document.version compare:self.document.version] == NSOrderedAscending && [self changeContentAvailableToCompareWithDocumentVersionAtIndexPath:indexPath];
    
    return cell;
}

- (BOOL)changeContentAvailableToCompareWithDocumentVersionAtIndexPath:(NSIndexPath *)indexPath
{
    BOOL result = NO;
    
    // TODO: In the future, extend this method to check for offline cached change content between any two document versions.
    
    // If we think we're online, then return YES because we the content is retrievable from the server
    if (self.refreshVersionsSuccess) {
        result = YES;
    } else {
        // Otherwise, if we're offline, we can only compare against the previous version of the current document
        ADXDocument *document = [self.fetchedResultsController objectAtIndexPath:indexPath];
        result = (document.versionValue + 1) == self.document.versionValue;
    }
    
    return result;
}

#pragma mark - Table View Delegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return kADXDocVersionsComparisonPopoverCellHeight;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    ADXDocument *document = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    if( self.actionDelegate )
        [self.actionDelegate didSelectDoumentForComparison:document];
    
    [self.tableView deselectRowAtIndexPath:indexPath animated:NO];
}

- (BOOL)tableView:(UITableView *)tableView shouldHighlightRowAtIndexPath:(NSIndexPath *)indexPath
{
    ADXDocument *document = [self.fetchedResultsController objectAtIndexPath:indexPath];
    return document.version &&  [document.version compare:self.document.version] == NSOrderedAscending;
}

#pragma mark - Data Management

- (void)updateFetchedResultsControllerPredicate
{
    if (!self.document || !self.managedObjectContext)
        return;
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(%K == %@) AND (%K == %@)", ADXDocumentAttributes.id, self.document.id, ADXDocumentAttributes.accountID, self.document.accountID];
    [_fetchedResultsController.fetchRequest setPredicate:predicate];
}

- (void)refetchFetchedResultsController
{
    if (!self.document || !self.managedObjectContext)
        return;
    
    __weak NSFetchedResultsController *frc = _fetchedResultsController;
    
    [self.managedObjectContext performBlockAndWait:^{
        NSError *error = nil;
        BOOL success = [frc performFetch:&error];
        if (!success) {
            LogNSError(@"Couldn't initialize NSFetchedResultsController", error);
        }
    }];
}

- (NSFetchedResultsController *)prepareFetchedResultsController
{
    __block NSFetchedResultsController *frc = nil;
    __weak ADXDocVersionComparisonViewController *weakSelf = self;
    
    if (!self.document || !self.managedObjectContext)
        return nil;
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:ADXDocumentAttributes.version ascending:NO];
    NSManagedObjectContext *moc = self.managedObjectContext;
    
    [moc performBlockAndWait:^{
        frc = [moc aps_fetchedResultsControllerForEntityName:[ADXDocument entityName]
                                                   predicate:nil
                                             sortDescriptors:[NSArray arrayWithObject:sortDescriptor]
                                          sectionNameKeyPath:nil
                                                   cacheName:nil
                                                    delegate:weakSelf];
    }];
    return frc;
}

- (NSFetchedResultsController *)fetchedResultsController
{
    if (!_fetchedResultsController) {
        _fetchedResultsController = [self prepareFetchedResultsController];
        _fetchedResultsController.delegate = self;
        [self updateFetchedResultsControllerPredicate];
        [self refetchFetchedResultsController];
    }
    return _fetchedResultsController;
}

#pragma mark - NSFetchedResultsControllerDelegate

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView reloadData];
}

@end