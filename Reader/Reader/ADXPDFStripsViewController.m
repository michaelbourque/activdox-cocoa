//
//  ADXPDFStripsViewController.m
//  Reader
//
//  Created by Steve Tibbett on 2012-11-29.
//  Copyright (c) 2012 ActivDox Inc. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>

#import "ActivDoxCore.h"
#import "ADXPDFStripsViewController.h"
#import "ADXPDFStripSectionHeaderView.h"
#import "ADXPDFStripDescriptionCell.h"
#import "ADXAppDelegate.h"
#import "MBProgressHUD.h"
#import "PSPDFKit.h"


@interface ADXPDFStripsViewController ()
@property (weak, nonatomic) IBOutlet UIToolbar *toolbar;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (strong, nonatomic) ADXPDFStripCell *zoomCell;
@property (strong, nonatomic) NSMutableDictionary *cellIndex;
@end

#define MAX_DOCUMENT_POSITIONS_TO_REMEMBER 100

@implementation ADXPDFStripsViewController
@dynamic contentInset;

+ (NSMutableArray *)recentDocumentPositions
{
    static NSMutableArray *recentDocumentPositions;
    if (!recentDocumentPositions) {
        recentDocumentPositions = [NSMutableArray array];
    }
    return recentDocumentPositions;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.collectionView.collectionViewLayout = [[UICollectionViewFlowLayout alloc] init];

    [self.collectionView registerNib:[UINib nibWithNibName:@"ADXPDFStripSectionHeaderView" bundle:nil]
          forSupplementaryViewOfKind:UICollectionElementKindSectionHeader
                 withReuseIdentifier:@"ADXPDFStripSectionHeaderView"];

    [self.collectionView registerClass:[ADXPDFStripCell class] forCellWithReuseIdentifier:@"ADXPDFStripCell"];
    
    [self.collectionView registerNib:[UINib nibWithNibName:@"ADXPDFStripDescriptionCell" bundle:nil] forCellWithReuseIdentifier:@"ADXPDFStripDescriptionCell"];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self refresh];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];

    [self restoreScrollPosition];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];

    [self saveScrollPosition];
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    [super didRotateFromInterfaceOrientation:fromInterfaceOrientation];
    [self.collectionView reloadData];
}

- (UIEdgeInsets)contentInset
{
    return self.collectionView.contentInset;
}

- (void)setContentInset:(UIEdgeInsets)contentInset
{
    self.collectionView.contentInset = contentInset;
}

- (void)refresh
{
    [self buildCellIndex];
    [self.collectionView reloadData];
}


- (void)buildCellIndex
{
    self.cellIndex = [NSMutableDictionary dictionary];
    
    if (self.pspdfDocument == nil) {
        // No cells until the document is ready
        return;
    }
    
    int numSections = [self.PDFStripsDataSource numberOfSectionsForPDFStrips];
    for (int section = 0; section < numSections; section++) {
        int numItems = [self.PDFStripsDataSource numberOfPDFStripsInSection:section];
        int originalIndex = 0;
        int newItem = 0;
        
        for (int item = 0; item < numItems; item++) {
            // Add the NSIndexPath of the original item
            NSIndexPath *indexPath = [NSIndexPath indexPathForItem:newItem++ inSection:section];
            NSIndexPath *originalIndexPath = [NSIndexPath indexPathForItem:originalIndex++ inSection:section];
            [self.cellIndex setObject:originalIndexPath forKey:indexPath];

            // Add the description cell if required
            ADXPDFStripInfo *stripInfo = [self.PDFStripsDataSource infoForPDFStripAtIndexPath:originalIndexPath];
            if (stripInfo.summary.length > 0) {
                // Add the strip info to indicate a description cell
                indexPath = [NSIndexPath indexPathForItem:newItem++ inSection:section];
                [self.cellIndex setObject:stripInfo forKey:indexPath];
            }
        }
    }
}

#pragma mark - UICollectionViewDelegate

- (void)startTransition:(id)context
{
    ADXPDFStripInfo *stripInfo = [context objectForKey:@"stripInfo"];
    ADXPDFStripCell *cell = [context objectForKey:@"cell"];
    
    PSPDFPageInfo *pageInfo = [self.pspdfDocument pageInfoForPage:stripInfo.pageIndex];
    CGRect extentOfPageInCell = [ADXPDFStripCell pageRectForStripExtent:stripInfo.stripExtent pageExtent:pageInfo.pageRect];
    
    // Synchronously cache the page we're about to transition to
    [self.PDFStripsDelegate prepareForPDFStripTransitionToPage:stripInfo.pageIndex];
    
    CATransition* transition = [CATransition animation];
    transition.duration = 0.75;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionFade;
    [self.navigationController.view.layer addAnimation:transition forKey:nil];
    
    [self.PDFStripsDelegate positionPDFStripView:cell
                                          atRect:extentOfPageInCell
                                          onPage:stripInfo.pageIndex
                                   withFocusArea:stripInfo.stripExtent];
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.zoomCell) {
        return;
    }

    id obj = [self.cellIndex objectForKey:indexPath];
    if ([obj isKindOfClass:[ADXPDFStripInfo class]]) {
        // It's a description cell
        return;
    }

    NSAssert([obj isKindOfClass:[NSIndexPath class]], @"Expected either ADXPDFStripInfo or NSIndexPath in cellIndex");
    
    // Map from our index (which includes the description items) to the client's index (which doesn't)
    int mappedItem = 0;
    for (int item = 0; item < indexPath.item; item++) {
        id obj = [self.cellIndex objectForKey:[NSIndexPath indexPathForItem:item inSection:indexPath.section]];
        if (![obj isKindOfClass:[ADXPDFStripInfo class]]) {
            mappedItem++;
        }
    }

    NSIndexPath *mappedIndexPath = [NSIndexPath indexPathForItem:mappedItem inSection:indexPath.section];
 
    ADXPDFStripInfo *stripInfo = [self.PDFStripsDataSource infoForPDFStripAtIndexPath:mappedIndexPath];
    if (self.PDFStripsDelegate == nil ||
        UIInterfaceOrientationIsLandscape([[UIApplication sharedApplication] statusBarOrientation])) {
        // Default transition, if no delegate set, or in landscape mode
        [self.PDFStripsDelegate hidePDFStripsAndShowPage:stripInfo.pageIndex];
        return;
    }
    
    // Create a new cell for the selection that will persist during the animation
    UICollectionViewCell *cell = [collectionView cellForItemAtIndexPath:indexPath];
    CGRect frameInWindowCoords = [cell convertRect:cell.bounds toView:nil];
    [cell removeFromSuperview];
    [self.view.window addSubview:cell];
    cell.frame = frameInWindowCoords;

    // Give it an extra large shadow as feedback that it's been tapped
    cell.layer.shadowOpacity = 0.4;
    cell.layer.shadowRadius = 4.0;
    
    NSDictionary *context = @{
        @"stripInfo": stripInfo,
        @"cell": cell
    };
    
    [self performSelectorOnMainThread:@selector(startTransition:) withObject:context waitUntilDone:NO];
}

#pragma mark - UICollectionViewDataSource

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    if ([kind isEqualToString:UICollectionElementKindSectionHeader]) {
        ADXPDFStripSectionHeaderView *headerView = [self.collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader
                                                                                             withReuseIdentifier:@"ADXPDFStripSectionHeaderView"
                                                                                                    forIndexPath:indexPath];
        
        headerView.sectionTitleLabel.text = [self.PDFStripsDataSource titleForPDFStripSection:indexPath.section];
        
        return headerView;
    }
    
    return nil;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    int count = 0;
    for (NSIndexPath *indexPath in self.cellIndex.allKeys) {
        if (indexPath.section == section) {
            count++;
        }
    }
    
    return count;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return [self.PDFStripsDataSource numberOfSectionsForPDFStrips];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    id obj = [self.cellIndex objectForKey:indexPath];
    if ([obj isKindOfClass:[ADXPDFStripInfo class]]) {
        // It's a description cell
        ADXPDFStripDescriptionCell *cell = [self.collectionView dequeueReusableCellWithReuseIdentifier:@"ADXPDFStripDescriptionCell" forIndexPath:indexPath];
        ADXPDFStripInfo *stripInfo = (ADXPDFStripInfo *)obj;
        
        NSMutableAttributedString *mutableString = [cell.labelView.attributedText mutableCopy];
        [mutableString replaceCharactersInRange:NSMakeRange(0, mutableString.length) withString:stripInfo.summary.string];
        cell.labelView.attributedText = mutableString;
        
        return cell;
    } else {
        NSAssert([obj isKindOfClass:[NSIndexPath class]], @"Expected either ADXPDFStripInfo or NSIndexPath in cellIndex");
        indexPath = obj;
    }

    ADXPDFStripInfo *stripInfo = [self.PDFStripsDataSource infoForPDFStripAtIndexPath:indexPath];
    ADXPDFStripCell *stripCell = [self.collectionView dequeueReusableCellWithReuseIdentifier:@"ADXPDFStripCell" forIndexPath:indexPath];
    stripCell.stripInfo = stripInfo;
    stripCell.highlightedContent = [self.PDFStripsDataSource highlightedContentForPDFStrips];
    stripCell.pspdfDocument = self.pspdfDocument;
    stripCell.adxDocument = self.document;

    if (stripCell.longPressRecognizer == nil) {
        stripCell.longPressRecognizer = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(actionCellLongPress:)];
        [stripCell addGestureRecognizer:stripCell.longPressRecognizer];
    }
    
    [stripCell setNeedsDisplay];
    return stripCell;
}

#pragma mark - UICollectionViewFlowLayoutDelegate

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section
{
    const float sectionHeaderHeight = 48;
    return CGSizeMake(0, sectionHeaderHeight);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    const float sectionSpacing = 24;
    return sectionSpacing;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    // Width is fixed.  Height is based on the scaled height of the strip plus some padding.
    float padding = 32;
    float verticalPadding = padding;
    float horizontalPadding = padding;
    float cellWidth = self.collectionView.bounds.size.width - (horizontalPadding*2);

    id obj = [self.cellIndex objectForKey:indexPath];
    if ([obj isKindOfClass:[ADXPDFStripInfo class]]) {
        // It's a description cell
        return CGSizeMake(cellWidth, 0);
    } else {
        NSAssert([obj isKindOfClass:[NSIndexPath class]], @"Expected either ADXPDFStripInfo or NSIndexPath in cellIndex");
        indexPath = obj;
    }
    
    ADXPDFStripInfo *stripInfo = [self.PDFStripsDataSource infoForPDFStripAtIndexPath:indexPath];
    PSPDFPageInfo *pageInfo = [self.pspdfDocument pageInfoForPage:stripInfo.pageIndex];
    float scale = cellWidth / pageInfo.pageRect.size.width;
    
    CGSize size = CGSizeMake(cellWidth, (stripInfo.stripExtent.size.height + verticalPadding*2)*scale);

    return size;
}

#pragma mark - Saved scroll positions

- (void)saveScrollPosition
{
    NSMutableArray *recents = [ADXPDFStripsViewController recentDocumentPositions];
    
    // Remember the scroll position for next time
    BOOL found = NO;
    for (NSMutableDictionary *item in recents) {
        if ([item[@"id"] isEqual:self.document.id]) {
            found = YES;
            [item setObject:@(self.collectionView.bounds.origin.y) forKey:@"position"];
            
            // Move item to the end of the list (and keep a reference to it while we do so so it
            // doesn't wind up deallocated)
            NSMutableDictionary *tempItem = item;
            [recents removeObject:tempItem];
            [recents addObject:tempItem];
            break;
        }
    }
    
    if (!found) {
        [recents addObject:[@{ @"position": @(self.collectionView.bounds.origin.y),
                                            @"id": self.document.id } mutableCopy]];
        
        // Keep the list from growing unbounded
        if (recents.count > MAX_DOCUMENT_POSITIONS_TO_REMEMBER) {
            [recents removeObjectAtIndex:0];
        }
    }
}

- (void)restoreScrollPosition
{
    NSMutableArray *recents = [ADXPDFStripsViewController recentDocumentPositions];

    // Check for a remembered scroll position
    for (NSMutableDictionary *item in recents) {
        if ([item[@"id"] isEqual:self.document.id]) {
            // Found it
            float position = [item[@"position"] floatValue];
            CGRect bounds = self.collectionView.bounds;
            bounds.origin.y = position;
            self.collectionView.bounds = bounds;
        }
    }
}

#pragma mark - Cell Zooming

- (void)animationTimer:(NSTimer *)timer
{
    NSMutableDictionary *userInfo = timer.userInfo;
    NSDate *startDate = [userInfo objectForKey:@"startDate"];
    CGRect startRect = [[userInfo objectForKey:@"startRect"] CGRectValue];
    CGRect endRect = [[userInfo objectForKey:@"endRect"] CGRectValue];

    double interval = -[startDate timeIntervalSinceNow];
    if (interval > .2) {
        // Done
        self.zoomCell.frame = endRect;
        [timer invalidate];
    } else {
        double delta = interval * 5;
    
        CGRect rect = startRect;
        rect.origin.x = startRect.origin.x + (endRect.origin.x - startRect.origin.x) * delta;
        rect.origin.y = startRect.origin.y + (endRect.origin.y - startRect.origin.y) * delta;
        rect.size.width = startRect.size.width + (endRect.size.width - startRect.size.width) * delta;
        rect.size.height = startRect.size.height + (endRect.size.height - startRect.size.height) * delta;
    
        self.zoomCell.frame = rect;
    }
}

- (void)actionCellLongPress:(UILongPressGestureRecognizer *)sender
{
    if (self.zoomCell != nil) {
        // Watch for the end of the long press
        if (sender.state == UIGestureRecognizerStateEnded) {
            [self.zoomCell removeFromSuperview];
            self.zoomCell = nil;
        }
        return;
    }

    ADXPDFStripCell *cell = (ADXPDFStripCell *)[sender view];
    if (cell != nil) {
        // Find the index path of the selected cell
        NSIndexPath *indexPath = [self.collectionView indexPathForCell:cell];
        if (indexPath != nil) {
            // Create a new cell whose frame we can manipulate
            ADXPDFStripCell *currentCell = (ADXPDFStripCell *)[self.collectionView cellForItemAtIndexPath:indexPath];
            self.zoomCell = [[ADXPDFStripCell alloc] initWithFrame:currentCell.frame];
            
            self.zoomCell.stripInfo = currentCell.stripInfo;
            self.zoomCell.pspdfDocument = currentCell.pspdfDocument;
            self.zoomCell.adxDocument = currentCell.adxDocument;
            
            __block CGRect frameInWindowCoords = [cell convertRect:cell.bounds toView:nil];
            
            [self.zoomCell removeFromSuperview];
            [self.view.window addSubview:self.zoomCell];
            self.zoomCell.frame = frameInWindowCoords;
            self.zoomCell.layer.shouldRasterize = NO;

            CGRect endRect = frameInWindowCoords;

            float contextAmount = 80;

            if (UIInterfaceOrientationIsLandscape([UIApplication sharedApplication].statusBarOrientation)) {
                endRect.origin.x -= contextAmount;
                endRect.size.width += (contextAmount*2);
                endRect.origin.y -= 8;
                endRect.size.height += 16;
            } else {
                endRect.origin.y -= contextAmount;
                endRect.size.height += (contextAmount*2);
                endRect.origin.x -= 8;
                endRect.size.width += 16;
            }

//            NSLog(@"fromRect: %@", NSStringFromCGRect(frameInWindowCoords));
//            NSLog(@"  toRect: %@", NSStringFromCGRect(endRect));
            
            NSMutableDictionary *animationContext = [NSMutableDictionary dictionary];
            [animationContext setObject:[NSDate date] forKey:@"startDate"];
            [animationContext setObject:[NSValue valueWithCGRect:frameInWindowCoords] forKey:@"startRect"];
            [animationContext setObject:[NSValue valueWithCGRect:endRect] forKey:@"endRect"];
            [NSTimer scheduledTimerWithTimeInterval:1/60 target:self selector:@selector(animationTimer:) userInfo:animationContext repeats:YES];

            // Give it an extra large shadow as feedback that it's been tapped
            self.zoomCell.layer.shadowOpacity = 0.4;
            self.zoomCell.layer.shadowRadius = 4.0;

            [self.zoomCell becomeFirstResponder];
            [self.zoomCell setNeedsDisplay];
        }
    }
}

@end
