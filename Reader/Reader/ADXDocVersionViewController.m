//
//  ADXDocVersionViewController.m
//  Reader
//
//  Created by Gavin McKenzie on 2012-09-13.
//  Copyright (c) 2012 ActivDox Inc. All rights reserved.
//

#import "ADXDocVersionViewController.h"
#import "ADXDocVersionsPopoverCell.h"
#import "ADXDocInfoViewController.h"
#import "ADXPopoverBackgroundView.h"
#import "ADXDocPermissionsViewController.h"
#import "ADXDocPermissionSettingViewController.h"
#import "ADXAppDelegate.h"
#import "ADXBarButtonItem.h"
#import "MBProgressHUD.h"

#import "ActivDoxCore.h"

static CGFloat kADXDocVersionPopoverHeightAdjustment = 37.0f;

static NSInteger kADXDocVersionPopoverSectionCollections = 0;
static NSInteger kADXDocVersionPopoverSectionVersions = 1;

@interface ADXDocVersionViewController () <UITableViewDataSource, UITableViewDelegate, NSFetchedResultsControllerDelegate>
@property (strong, nonatomic) ADXV2Service *service;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (assign, nonatomic) BOOL alreadyRefreshed;
@property (strong, nonatomic) UIBarButtonItem *shareButton;
@property (assign, nonatomic) BOOL currentUserIsDocumentAuthor;
@property (assign, nonatomic) BOOL refreshVersionsSuccess;
@property (strong, nonatomic) NSArray *collections;

@property (strong, nonatomic) UIBarButtonItem *permissionsButton;
@end

@implementation ADXDocVersionViewController

#pragma mark - Class Methods

+ (ADXDocVersionViewController *)viewControllerWithDocument:(ADXDocument *)document
                                 allowNavigatingCollections:(BOOL)allowNavigatingCollections
                                           hideDeleteButton:(BOOL)hideDeleteButton
                                       managedObjectContext:(NSManagedObjectContext *)moc
                                             actionDelegate:(id)delegate
{
    ADXDocVersionViewController *viewController = [[ADXDocVersionViewController alloc] initWithNibName:nil bundle:nil];

    viewController.hideDeleteButton = hideDeleteButton;
    viewController.allowNavigatingCollections = allowNavigatingCollections;
    viewController.document = document;
    viewController.managedObjectContext = moc;
    viewController.contentSizeForViewInPopover = [viewController.view sizeThatFits:CGSizeZero];
    viewController.service = [ADXV2Service serviceForManagedObject:document];
    viewController.actionDelegate = delegate;
    
    return viewController;
}

+ (UIPopoverController *)popoverControllerWithDocument:(ADXDocument *)document
                            allowNavigatingCollections:(BOOL)allowNavigatingCollections
                                      hideDeleteButton:(BOOL)hideDeleteButton
                                  managedObjectContext:(NSManagedObjectContext *)moc
                                        actionDelegate:(id)delegate
{
    ADXDocVersionViewController *viewController = [ADXDocVersionViewController viewControllerWithDocument:document allowNavigatingCollections:allowNavigatingCollections hideDeleteButton:hideDeleteButton managedObjectContext:moc actionDelegate:delegate];
    viewController.navigationItem.title = @"Document";
    
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:viewController];
    UIPopoverController *popover = [[UIPopoverController alloc] initWithContentViewController:navigationController];
    popover.popoverBackgroundViewClass = [ADXPopoverBackgroundView class];
    [popover setPopoverContentSize:CGSizeMake(kADXDocVersionsPopoverCellWidth, kADXDocVersionsPopoverCellHeight + kADXDocVersionPopoverHeightAdjustment) animated:NO];
    viewController.parentPopoverController = popover;
    
    return popover;
}

+ (UIViewController *)modalControllerWithDocument:(ADXDocument *)document allowNavigatingCollections:(BOOL)allowNavigatingCollections hideDeleteButton:(BOOL)hideDeleteButton managedObjectContext:(NSManagedObjectContext *)moc actionDelegate:(id)delegate
{
    ADXDocVersionViewController *viewController = [ADXDocVersionViewController viewControllerWithDocument:document
                                                                             allowNavigatingCollections:allowNavigatingCollections
                                                                                         hideDeleteButton:hideDeleteButton
                                                                                     managedObjectContext:moc
                                                                                           actionDelegate:delegate];
    
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:viewController];
    viewController.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:viewController action:@selector(didTapDoneButton:)];
    viewController.parentPopoverController = nil;
    
    return navigationController;
}

#pragma mark - Initialization

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

#pragma mark - View Management

- (void)viewDidLoad
{
    [super viewDidLoad];

    [self loadCollections];
    
    [self updateBarButtons];
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"popover_bg"]];
    
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.allowsSelectionDuringEditing = YES;
    
    self.titleLabel.text = self.document.title;
    self.authorLabel.text = self.document.author.displayNameOrLoginName;
    
    // Update the label height given the new text, and adjust the header view's height.
    // Autolayout would be a better solution but doesn't seem to work (for me anyway) for
    // the header view of a table.  Constraints that should have affected the view's height
    // weren't moving the rest of the table down.
    CGSize labelSize = self.titleLabel.frame.size;
    float oldHeight = labelSize.height;
    labelSize.height = labelSize.height * 4; // Let it grow to be up to 4x its current height
    CGRect newFrame = self.titleLabel.frame;
    newFrame.size.height = [self.titleLabel.text sizeWithFont:self.titleLabel.font constrainedToSize:labelSize lineBreakMode:self.titleLabel.lineBreakMode].height;
    self.titleLabel.frame = newFrame;
    
    // Add the change in the label's height to the height of the header
    CGRect titleFrame = self.tableView.tableHeaderView.frame;
    titleFrame.size.height += (newFrame.size.height - oldHeight);
    self.tableView.tableHeaderView.frame = titleFrame;
    
    UIBarButtonItem*bbItem = [[UIBarButtonItem alloc]initWithTitle:@"Permissions" style:UIBarButtonItemStyleBordered target:self action:@selector(didTapPermissionsButton:)];
    self.navigationItem.rightBarButtonItems = [NSArray arrayWithObject:bbItem];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];

    __weak ADXDocVersionViewController *weakSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        [weakSelf refresh];
    });
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    if (self.isMovingFromParentViewController) {
        self.fetchedResultsController.delegate = nil;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setTableView:nil];
    [self setTitleLabel:nil];
    [self setAuthorLabel:nil];
    [self setVersionHistorySectionHeaderView:nil];
    [self setCollectionSectionHeaderView:nil];
    [self setCollectionEditButton:nil];
    [super viewDidUnload];
}

- (void)updateBarButtons
{
    NSMutableArray *rightBarButtons = [[NSMutableArray alloc] initWithCapacity:2];
    NSMutableArray *leftBarButtons = [[NSMutableArray alloc] initWithCapacity:1];

    if ([ADXUserDefaults sharedDefaults].allDocsShareableOverride) {
        [rightBarButtons addObject:self.shareButton];
    } else {
        if (!self.hideShareButton) {
            if (self.document.isPublicValue) {
                [rightBarButtons addObject:self.shareButton];
            }
        }
    }

    [rightBarButtons addObject:self.permissionsButton];
    
    
    if (!self.hideDeleteButton) {
        UIBarButtonItem *deleteItem = [ADXBarButtonItem itemWithImageNamed:@"trash" target:self selector:@selector(didTapRemoveButton:)];
        [leftBarButtons addObject:deleteItem];
    }

    // Add the Analytics button if the current user is the document author
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad && self.document) {
        ADXV2Service *service = [ADXV2Service serviceForManagedObject:self.document];
        ADXUser *loggedInUser = service.account.user;
        if (service.networkReachable && service.serverReachable && [self.document.author.id isEqual:loggedInUser.id]) {
            UIBarButtonItem *analyticsButton = [ADXBarButtonItem itemWithImageNamed:@"analytics" target:self selector:@selector(didTapAnalyticsButton:)];
            [leftBarButtons addObject:analyticsButton];
        }
    }

    self.navigationItem.leftBarButtonItems = leftBarButtons;
    self.navigationItem.rightBarButtonItems = rightBarButtons;
}

- (void)updateAfterRefresh:(BOOL)refreshSuccess error:(NSError *)refreshError
{
    __weak ADXDocVersionViewController *weakSelf = self;

    if (self.refreshVersionsSuccess) {
        self.tableView.tableHeaderView = nil;
    } else {
        self.tableView.tableHeaderView = self.tableHeaderWarningView;
    }

    int64_t delayInSeconds = 0.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        
        [MBProgressHUD hideAllHUDsForView:self.tableView animated:YES];

        weakSelf.alreadyRefreshed = YES;
        dispatch_async(dispatch_get_main_queue(), ^{
            [weakSelf.tableView setHidden:NO];
            [weakSelf.tableView reloadData];
            CGSize size = CGSizeMake(weakSelf.tableView.contentSize.width, weakSelf.tableView.contentSize.height + kADXDocVersionPopoverHeightAdjustment);
            [weakSelf.parentPopoverController setPopoverContentSize:size animated:YES];
        });
    });
}

- (void)refresh
{
    __weak ADXDocVersionViewController *weakSelf = self;

    if (self.alreadyRefreshed) {
        CGSize size = CGSizeMake(weakSelf.tableView.contentSize.width, weakSelf.tableView.contentSize.height + kADXDocVersionPopoverHeightAdjustment);
        [weakSelf.parentPopoverController setPopoverContentSize:size animated:YES];
    } else {
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            // Disable the table view while we reconfigure the popover
            self.tableView.userInteractionEnabled = NO;
        }
        
        [MBProgressHUD showHUDAddedTo:self.tableView animated:YES];

        [self.service refreshDocumentVersions:self.document.id completion:^(BOOL success, NSError *error) {
            weakSelf.refreshVersionsSuccess = success;
            dispatch_async(dispatch_get_main_queue(), ^{
                self.tableView.userInteractionEnabled = YES;
                [weakSelf updateAfterRefresh:success error:error];
            });
        }];
    }
}

#pragma mark - Accessors

- (UIBarButtonItem *)shareButton
{
    if (!_shareButton) {
        _shareButton = [[UIBarButtonItem alloc] initWithTitle:@"Share" style:UIBarButtonItemStyleBordered target:self action:@selector(didTapShareButton:)];
    }
    return _shareButton;
}

- (UIBarButtonItem *)permissionsButton
{
    if (!_permissionsButton) {
        _permissionsButton = [[UIBarButtonItem alloc] initWithTitle:@"Permissions" style:UIBarButtonItemStyleBordered target:self action:@selector(didTapPermissionsButton:)];
    }
    return _permissionsButton;
}

- (void)setHideShareButton:(BOOL)hideShareButton
{
    _hideShareButton = hideShareButton;
    [self updateBarButtons];
}

#pragma mark - Helpers

- (ADXCollectionDocumentsViewController *)newDocumentListViewForCollection:(ADXCollection *)collection;
{
    ADXCollectionDocumentsViewController *listViewController = [[ADXCollectionDocumentsViewController alloc] initWithNibName:nil bundle:nil];
    listViewController.collection = collection;
    listViewController.parentPopoverController = self.parentPopoverController;
    listViewController.allowEditing = NO;
    listViewController.actionDelegate = self;
    
    return listViewController;
}

- (void)pushCollectionSelectionViewController
{
    ADXDocCollectionSelectionViewController *viewController = [[ADXDocCollectionSelectionViewController alloc] initWithNibName:nil bundle:nil];
    viewController.document = self.document;
    viewController.actionDelegate = self;
    [self.navigationController pushViewController:viewController animated:YES];    
}

#pragma mark - Table View Datasource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger result = 0;
    
    if (section == kADXDocVersionPopoverSectionVersions) {
        id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:0];
        result = [sectionInfo numberOfObjects];
    }
    
    if (section == kADXDocVersionPopoverSectionCollections) {
        result = self.document.collections.count;
        if (self.tableView.editing) {
            result++;
        }
    }
    
    return result;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = nil;
    
    if (indexPath.section == kADXDocVersionPopoverSectionVersions) {
        ADXDocVersionsPopoverCell *versionCell = (ADXDocVersionsPopoverCell *)[self.tableView dequeueReusableCellWithIdentifier:NSStringFromClass([ADXDocVersionsPopoverCell class])];
        
        if (!versionCell) {
            UINib *nib = [UINib nibWithNibName:NSStringFromClass([ADXDocVersionsPopoverCell class]) bundle:[NSBundle mainBundle]];
            versionCell = [[nib instantiateWithOwner:self options:nil] objectAtIndex:0];
        }
        
        NSIndexPath *versionIndexPath = [NSIndexPath indexPathForRow:indexPath.row inSection:0];
        ADXDocument *document = [self.fetchedResultsController objectAtIndexPath:versionIndexPath];
        versionCell.document = document;
        versionCell.imageView.image = nil;
        versionCell.dateLabel.text = [document publishedDateDisplayLabel:YES];
        
        cell = versionCell;
    }
    
    if (indexPath.section == kADXDocVersionPopoverSectionCollections) {
        UITableViewCell *collectionCell = [self.tableView dequeueReusableCellWithIdentifier:@"kADXDocVersionPopoverCollectionCell"];
        if (!collectionCell) {
            collectionCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"kADXDocVersionPopoverCollectionCell"];
            collectionCell.textLabel.textColor = [UIColor colorWithWhite:80/255.0 alpha:1.0];
            collectionCell.textLabel.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:15.0f];
            
            if (self.allowNavigatingCollections) {
                collectionCell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                collectionCell.selectionStyle = UITableViewCellSelectionStyleBlue;
            } else {
                collectionCell.selectionStyle = UITableViewCellSelectionStyleNone;
            }
        }

        NSString *title = nil;
        if (indexPath.row >= self.collections.count) {
            title = @"Add collection";
        } else {
            ADXCollection *collection = self.collections[indexPath.row];
            if ([collection.type isEqualToString:ADXCollectionType.system]) {
                title = [[ADXAppDelegate sharedInstance] localizedStringForKey:collection.title];
            } else {
                title = collection.title;
            }
        }
        collectionCell.textLabel.text = title;
        cell = collectionCell;
    }
    
    return cell;
}

#pragma mark - Table View Delegate

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *result = nil;
    
    if (section == kADXDocVersionPopoverSectionCollections) {
        result = self.collectionSectionHeaderView;
    }
    
    if (section == kADXDocVersionPopoverSectionVersions) {
        result = self.versionHistorySectionHeaderView;
    }
    
    return result;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 44.0f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat result = 44.0f;
    if (indexPath.section == kADXDocVersionPopoverSectionVersions) {
        result = kADXDocVersionsPopoverCellHeight;
    }
    
    return result;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.tableView.editing) {
        NSIndexPath *collectionInsertionRowIndexPath = [NSIndexPath indexPathForRow:self.document.collections.count inSection:kADXDocVersionPopoverSectionCollections];
        if ([indexPath isEqual:collectionInsertionRowIndexPath]) {
            [self pushCollectionSelectionViewController];
        }
    } else {
        if (indexPath.section == kADXDocVersionPopoverSectionVersions) {
            NSIndexPath *versionIndexPath = [NSIndexPath indexPathForRow:indexPath.row inSection:0];
            ADXDocument *document = [self.fetchedResultsController objectAtIndexPath:versionIndexPath];
            
            ADXDocInfoViewController *viewController = [[ADXDocInfoViewController alloc] initWithNibName:nil bundle:nil];
            viewController.title = [NSString stringWithFormat:NSLocalizedString(@"Version %d", nil), (int)document.versionValue];
            viewController.document = document;
            viewController.thumbnail = nil;
            viewController.hideShareButton = YES;
            viewController.actionDelegate = self;
            
            [self.navigationController pushViewController:viewController animated:YES];
        }
        
        if (indexPath.section == kADXDocVersionPopoverSectionCollections) {
            if (self.allowNavigatingCollections) {
                ADXCollection *collection = self.collections[indexPath.row];
                ADXCollectionDocumentsViewController *listViewController = [self newDocumentListViewForCollection:collection];
                [self.navigationController pushViewController:listViewController animated:YES];
            }
        }
    }
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCellEditingStyle result = UITableViewCellEditingStyleNone;
    
    if (indexPath.section == kADXDocVersionPopoverSectionCollections) {
        if (indexPath.row >= self.collections.count) {
            result = UITableViewCellEditingStyleInsert;
        } else {
            ADXCollection *collection = self.collections[indexPath.row];
            if ([collection.type isEqualToString:ADXCollectionType.user]) {
                result = UITableViewCellEditingStyleDelete;
            }
        }
    }
    return result;
}

- (BOOL)tableView:(UITableView *)tableView shouldIndentWhileEditingRowAtIndexPath:(NSIndexPath *)indexPath
{
    BOOL result = NO;
    if (indexPath.section == kADXDocVersionPopoverSectionCollections) {
        result = YES;
    }
    return result;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        ADXCollection *collection = self.collections[indexPath.row];
        if ([self.actionDelegate respondsToSelector:@selector(didRemoveDocument:fromCollection:)]) {
            [(id<ADXCollectionDocumentsActionsDelegate>)self.actionDelegate didRemoveDocument:self.document fromCollection:collection];
            [self loadCollections];
            [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationAutomatic];
        }
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        [self pushCollectionSelectionViewController];
    }
}

#pragma mark - Data Management

- (void)loadCollections
{
    self.collections = [[self.document.collections allObjects] sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        NSString *firstTitle = [(ADXCollection *)obj1 title];
        NSString *secondTitle = [(ADXCollection *)obj2 title];
        return [firstTitle localizedCaseInsensitiveCompare:secondTitle];
    }];
}

- (void)updateFetchedResultsControllerPredicate
{
    if (!self.document || !self.managedObjectContext)
        return;
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(%K == %@) AND (%K == %@)", ADXDocumentAttributes.id, self.document.id, ADXDocumentAttributes.accountID, self.document.accountID];
    [_fetchedResultsController.fetchRequest setPredicate:predicate];
}

- (void)refetchFetchedResultsController
{
    if (!self.document || !self.managedObjectContext)
        return;
    
    __weak NSFetchedResultsController *frc = _fetchedResultsController;
    
    [self.managedObjectContext performBlockAndWait:^{
        NSError *error = nil;
        BOOL success = [frc performFetch:&error];
        if (!success) {
            LogNSError(@"Couldn't initialize NSFetchedResultsController", error);
        }
    }];
}

- (NSFetchedResultsController *)prepareFetchedResultsController
{
    __block NSFetchedResultsController *frc = nil;
    __weak ADXDocVersionViewController *weakSelf = self;
    
    if (!self.document || !self.managedObjectContext)
        return nil;
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:ADXDocumentAttributes.version ascending:NO];
    NSManagedObjectContext *moc = self.managedObjectContext;
    
    [moc performBlockAndWait:^{
        frc = [moc aps_fetchedResultsControllerForEntityName:[ADXDocument entityName]
                                                   predicate:nil
                                             sortDescriptors:[NSArray arrayWithObject:sortDescriptor]
                                          sectionNameKeyPath:nil
                                                   cacheName:nil
                                                    delegate:weakSelf];
    }];
    return frc;
}

- (NSFetchedResultsController *)fetchedResultsController
{
    if (!_fetchedResultsController) {
        _fetchedResultsController = [self prepareFetchedResultsController];
        _fetchedResultsController.delegate = self;
        [self updateFetchedResultsControllerPredicate];
        [self refetchFetchedResultsController];
    }
    return _fetchedResultsController;
}

#pragma mark - NSFetchedResultsControllerDelegate

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView reloadData];
}

#pragma mark - ADXCollectionDocumentsActionsDelegate

- (void)didSelectDocument:(ADXDocument *)document
{
    if ([self.actionDelegate respondsToSelector:@selector(didSelectDocOpenAction:document:)]) {
        [self.actionDelegate didSelectDocOpenAction:self document:document];
    }
}

#pragma mark - ADXDocCollectionSelectionDelegate

- (void)didSelectCollection:(ADXCollection *)collection
{
    if ([self.actionDelegate respondsToSelector:@selector(didAddDocument:toCollection:)]) {
        [(id<ADXCollectionDocumentsActionsDelegate>)self.actionDelegate didAddDocument:self.document toCollection:collection];
    }
    [self loadCollections];
    [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationAutomatic];
}

#pragma mark - Actions

- (void)didSelectDocShareAction:(id)sender document:(ADXDocument *)document
{
    __weak ADXDocVersionViewController *weakSelf = self;
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        [self dismissViewControllerAnimated:YES completion:^{
            if ([weakSelf.actionDelegate respondsToSelector:@selector(didSelectDocShareAction:document:)]) {
                [weakSelf.actionDelegate didSelectDocShareAction:weakSelf document:document];
            }
        }];
    } else {
        if ([self.actionDelegate respondsToSelector:@selector(didSelectDocShareAction:document:)]) {
            [self.actionDelegate didSelectDocShareAction:self document:document];
        }
    }
}

- (void)didSelectDocOpenAction:(id)sender document:(ADXDocument *)document
{
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    
    if ([self.actionDelegate respondsToSelector:@selector(didSelectDocOpenAction:document:)]) {
        [self.actionDelegate didSelectDocOpenAction:self document:document];
    }
}

- (void)didSelectDocRemoveAction:(id)sender document:(ADXDocument *)document
{
    if ([self.actionDelegate respondsToSelector:@selector(didSelectDocRemoveAction:document:)]) {
        [self.actionDelegate didSelectDocRemoveAction:self document:self.document];
    }
}

- (void)didTapDoneButton:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)didTapShareButton:(id)sender
{
    [self didSelectDocShareAction:sender document:self.document];
}

- (void)didTapAnalyticsButton:(id)sender
{
    if ([self.actionDelegate respondsToSelector:@selector(didSelectDocAnalyticsAction:document:)]) {
        [self.actionDelegate didSelectDocAnalyticsAction:self document:self.document];
    }
}

- (void)didTapRemoveButton:(id)sender
{
    if ([self.actionDelegate respondsToSelector:@selector(didSelectDocRemoveAction:document:)]) {
        [self.actionDelegate didSelectDocRemoveAction:sender document:self.document];
    }
}

- (void)didTapPermissionsButton:(id)sender{
        
//        NSIndexPath *versionIndexPath = [NSIndexPath indexPathForRow:indexPath.row inSection:0];
//        ADXDocument *document = [self.fetchedResultsController objectAtIndexPath:versionIndexPath];
    
        
    ADXDocPermissionSettingViewController *permissionSettingsView = [[ADXDocPermissionSettingViewController alloc]initWithStyle:UITableViewStylePlain andIncludedLocations:[NSArray arrayWithObject:@"Location 1"]];
    
    [permissionSettingsView setTitle:@"Permission Settings"];
    [self.navigationController pushViewController:permissionSettingsView animated:YES];
        
        
//        ADXDocInfoViewController *viewController = [[ADXDocInfoViewController alloc] initWithNibName:nil bundle:nil];
//        viewController.title = [NSString stringWithFormat:NSLocalizedString(@"Version %d", nil), (int)document.versionValue];
//        viewController.document = document;
//        viewController.thumbnail = nil;
//        viewController.hideShareButton = YES;
//        viewController.actionDelegate = self;
}

- (IBAction)didTapCollectionsEditButton:(id)sender
{
    NSIndexPath *collectionInsertionRowIndexPath = [NSIndexPath indexPathForRow:self.document.collections.count inSection:kADXDocVersionPopoverSectionCollections];
    
    if (self.tableView.editing) {
        [self.tableView setEditing:NO animated:YES];
        [self.collectionEditButton setTitle:@"Edit" forState:UIControlStateNormal];

        [self.tableView beginUpdates];
        [self.tableView deleteRowsAtIndexPaths:@[collectionInsertionRowIndexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
        [self.tableView endUpdates];
    } else {
        [self.tableView setEditing:YES animated:YES];
        [self.collectionEditButton setTitle:@"Done" forState:UIControlStateNormal];

        [self.tableView beginUpdates];
        [self.tableView insertRowsAtIndexPaths:@[collectionInsertionRowIndexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
        [self.tableView endUpdates];
    }
}

@end
