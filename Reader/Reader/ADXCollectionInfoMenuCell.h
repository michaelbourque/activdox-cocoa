//
//  ADXCollectionInfoMenuCell.h
//  Reader
//
//  Created by Gavin McKenzie on 2013-04-09.
//  Copyright (c) 2013 ActivDox Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ADXCollectionInfoMenuCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *messageLabel;
@end
