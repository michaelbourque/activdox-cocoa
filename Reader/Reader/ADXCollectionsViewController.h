//
//  ADXCollectionsViewController.h
//  Reader
//
//  Created by Gavin McKenzie on 12-06-20.
//  Copyright (c) 2012 ActivDox Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ADXAppCommonMacros.h"
#import "ADXBaseViewController.h"
#import "ActivDoxCore.h"

#import "ADXCollectionsController.h"
#import "ADXCollectionInfoViewController.h"
#import "ADXCollectionDocumentsViewController.h"

@interface ADXCollectionsViewController : ADXBaseViewController <ADXCollectionsControllerDataSource, ADXCollectionsControllerDelegate, ADXCollectionActionsDelegate, ADXCollectionDocumentsActionsDelegate>
@property (weak, nonatomic) IBOutlet UIBarButtonItem *profileButtonItem;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *notificationsBarButton;
@property (weak, nonatomic) IBOutlet UIButton *refreshButton;
@property (weak, nonatomic) IBOutlet UIPageControl *pageControl;
- (IBAction)didTapRefreshButton:(id)sender;
- (IBAction)didLongPressRefreshButton:(id)sender;
- (IBAction)didTapNotificationsButton:(id)sender;
@end
