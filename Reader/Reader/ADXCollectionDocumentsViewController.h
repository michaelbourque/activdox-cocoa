//
//  ADXCollectionDocumentsViewController.h
//  Reader
//
//  Created by Gavin McKenzie on 2013-04-13.
//  Copyright (c) 2013 ActivDox Inc. All rights reserved.
//

#import "ADXBaseDocListViewController.h"
#import "ActivDoxCore.h"

@protocol ADXCollectionDocumentsActionsDelegate <ADXBaseDocListActionsDelegate>
@optional
- (void)didSelectDocument:(ADXDocument *)document;
- (void)didRemoveDocument:(ADXDocument *)document fromCollection:(ADXCollection *)collection;
- (void)didAddDocument:(ADXDocument *)document toCollection:(ADXCollection *)collection;
@end

@interface ADXCollectionDocumentsViewController : ADXBaseDocListViewController
+ (UIPopoverController *)popoverControllerWithTitle:(NSString *)title
                                         collection:(ADXCollection *)collection
                                       allowEditing:(BOOL)allowEditing
                                    startInEditMode:(BOOL)startInEditMode
                                   allowMultiSelect:(BOOL)allowMultiSelect
                               allowEditMultiSelect:(BOOL)allowEditMultiSelect
                           showOnlyMissingDocuments:(BOOL)showOnlyMissingDocuments
                                     actionDelegate:(id)actionDelegate;
@property (strong, nonatomic) ADXCollection *collection;
@property (assign, nonatomic) BOOL showOnlyMissingDocuments;
@end
