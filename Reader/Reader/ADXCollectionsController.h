//
//  ADXCollectionsController.h
//  Reader
//
//  Created by Gavin McKenzie on 2012-12-06.
//  Copyright (c) 2012 ActivDox Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "ActivDoxCore.h"

@class ADXCollectionsController;

@protocol ADXCollectionsControllerDelegate <NSFetchedResultsControllerDelegate>
@end

@protocol ADXCollectionsControllerDataSource <NSObject>
- (NSString *)accountIDForCollectionsController:(ADXCollectionsController *)controller;
@end

typedef enum {
    ADXCollectionsControllerSortTypeCreated = 0,
    ADXCollectionsControllerSortTitle,
    ADXCollectionsControllerSortDate,
    ADXCollectionsControllerSortChanges,
    ADXCollectionsControllerSortType,
} ADXCollectionsControllerSort;

@interface ADXCollectionsController : NSObject
@property (weak, nonatomic) id<ADXCollectionsControllerDataSource> dataSource;
@property (weak, nonatomic) id<ADXCollectionsControllerDelegate> delegate;
@property (assign, nonatomic) ADXCollectionsControllerSort sortFilter;

- (id)initWithManagedObjectContext:(NSManagedObjectContext *)moc;

- (ADXV2Service *)service;
- (NSInteger)numberOfItems;
- (NSInteger)numberOfItemsInSection:(NSInteger)section;
- (NSInteger)numberOfSections;
- (id)objectAtIndexPath:indexPath;
- (void)refresh:(BOOL)forceLoadAll;
- (void)deleteCollection:(NSString *)collectionID localID:(NSString *)localID;
- (void)addDocument:(NSString *)documentID toCollection:(NSString *)collectionID localID:(NSString *)localID;
- (void)removeDocument:(NSString *)documentID fromCollection:(NSString *)collectionID localID:(NSString *)localID;

@end
