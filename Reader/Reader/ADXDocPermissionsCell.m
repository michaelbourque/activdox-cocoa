//
//  ADXDocPermissionsCell.m
//  Reader
//
//  Created by Kei Turner on 2013-09-09.
//  Copyright (c) 2013 ActivDox Inc. All rights reserved.
//

#import "ADXDocPermissionsCell.h"

@implementation ADXDocPermissionsCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
