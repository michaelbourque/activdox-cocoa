//
//  ADXTintedImageView.h
//  Reader
//
//  Created by Gavin McKenzie on 2013-03-28.
//  Copyright (c) 2013 ActivDox Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ADXTintedImageView : UIView
@property (strong, nonatomic) UIImage *image;
@property (strong, nonatomic) UIColor *tintColor;
- (id)initWithImage:(UIImage *)image;
@end
