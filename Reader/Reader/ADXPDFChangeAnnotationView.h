//
//  ADXPDFChangeAnnotationView.h
//  Reader
//
//  Created by Steve Tibbett on 2012-10-09.
//  Copyright (c) 2012 ActivDox Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PSPDFAnnotationView.h"
#import "ActivDoxCore.h"

typedef struct
{
    CGRect rect;
    __unsafe_unretained id change;
    
} ADXPDFFoundChange;

@interface ADXPDFChangeAnnotationView : UIView 

/// Initialize with a search result. Coordinates are recalculated automatically.
- (id)initWithChanges:(ADXHighlightedContentHelper *)changes pageIndex:(NSUInteger)pageIndex pageExtent:(CGRect)pageExtent;
- (ADXPDFFoundChange)hitTestForChangeAtPoint:(CGPoint)point inPage:(NSUInteger)pageIndex;

@end
