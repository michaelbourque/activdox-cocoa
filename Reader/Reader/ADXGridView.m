//
//  ADXGridView.m
//  Reader
//
//  Created by Gavin McKenzie on 2012-09-11.
//  Copyright (c) 2012 ActivDox Inc. All rights reserved.
//

#import "ADXGridView.h"
#import "GMGridViewLayoutStrategies.h"

@implementation ADXGridView

- (void)commonInit
{
    [super commonInit];
    [_tapGesture requireGestureRecognizerToFail:_longPressGesture];
}

- (void)longPressGestureUpdated:(UILongPressGestureRecognizer *)longPressGesture
{
    if (longPressGesture.state != UIGestureRecognizerStateBegan)
        return;
    
    CGPoint locationTouch = [longPressGesture locationInView:self];
    NSInteger position = [self.layoutStrategy itemPositionFromLocation:locationTouch];
    
    if (position != GMGV_INVALID_POSITION) {
        if ([self.actionDelegate conformsToProtocol:@protocol(ADXGridViewActionDelegate)]) {
            [(id<ADXGridViewActionDelegate>)self.actionDelegate ADXGridView:self didLongPressOnItemAtIndex:position];
        }
    }
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return YES;
}

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer
{
    BOOL valid = YES;
    BOOL isScrolling = self.isDragging || self.isDecelerating;

    if (gestureRecognizer == _longPressGesture) {
        valid = !isScrolling;
    } else {
        valid = [super gestureRecognizerShouldBegin:gestureRecognizer];
    }
    
    return valid;
}

@end
