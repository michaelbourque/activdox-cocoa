//
//  ADXAnnotationFilter.m
//  Reader
//
//  Created by Steve Tibbett on 2013-04-16.
//  Copyright (c) 2013 ActivDox Inc. All rights reserved.
//

#import "ADXAnnotationFilter.h"
#import "ActivDoxCore.h"
#import "ADXAppDelegate.h"

@interface ADXAnnotationFilter ()
@property (nonatomic) BOOL showHighlights;
@property (nonatomic) BOOL showNotes;
@property (nonatomic) BOOL includeMyNotes;
@property (nonatomic) BOOL includeAuthorNotes;
@property (nonatomic) BOOL includeOtherNotes;
@property (nonatomic) BOOL includeResolved;
@end

@implementation ADXAnnotationFilter

- (id)init
{
    self = [super init];
    if (self) {
        self.showNotes = [[ADXUserDefaults sharedDefaults] annotationFilterShowNotes];
        self.showHighlights = [[ADXUserDefaults sharedDefaults] annotationFilterShowHighlights];
        self.includeMyNotes = [[ADXUserDefaults sharedDefaults] annotationFilterIncludeMyNotes];
        self.includeAuthorNotes = [[ADXUserDefaults sharedDefaults] annotationFilterIncludeAuthorNotes];
        self.includeOtherNotes = [[ADXUserDefaults sharedDefaults] annotationFilterIncludeOtherNotes];
        self.includeResolved = [[ADXUserDefaults sharedDefaults] annotationFilterIncludeResolved];
    }
    return self;
}

- (BOOL)shouldFilterAnnotation:(ADXDocumentNote *)note onDocument:(ADXDocument *)adxDocument
{
    if (!self.showNotes && !self.showHighlights) {
        return YES;
    }

    if (note.text == nil) {
        // It's a highlight
        return !self.showHighlights;
    } else {
        // It's a note
        if (!self.showNotes) {
            return YES;
        }
    }
    
    if (!self.includeResolved) {
        if (note.closeVersion.intValue > 0 && note.closeVersion.intValue <= adxDocument.version.intValue) {
            return YES;
        }
    }
    
    BOOL isAuthorNote = [note.author isEqual:adxDocument.author];
    if (!self.includeAuthorNotes && isAuthorNote) {
        return YES;
    }
    
    BOOL isMyNote = [note.author isEqual:[ADXAppDelegate sharedInstance].currentAccount.user];
    if (!self.includeMyNotes && isMyNote) {
        return YES;
    }
    
    if (!self.includeOtherNotes) {
        if (!isMyNote && !isAuthorNote) {
            return YES;
        }
    }
    
    return NO;
}

@end

