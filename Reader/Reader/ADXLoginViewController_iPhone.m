//
//  ADXLoginViewController_iPhone.m
//  Reader
//
//  Created by Gavin McKenzie on 2012-08-03.
//  Copyright (c) 2012 ActivDox Inc. All rights reserved.
//

#import "ADXLoginViewController_iPhone.h"
#import "ADXAppDelegate.h"
#import "UIAlertView+Blocks.h"

@interface ADXLoginViewController_iPhone ()
@property (strong, nonatomic) NSString *currentLoginName;
@property (strong, nonatomic) NSString *currentPassword;
@property (strong, nonatomic) NSString *currentServerURL;
@property (assign, nonatomic) BOOL currentRememberPassword;
- (void)disableDoneButton;
- (void)updateDoneButton;
@end

@implementation ADXLoginViewController_iPhone
@synthesize doneButton;
@synthesize loginNameField;
@synthesize passwordField;
@synthesize serverField;
@synthesize rememberPasswordSwitch;
@synthesize forgotPasswordLink;

#pragma mark - View Management

- (void)viewDidUnload
{
    [self setLoginNameField:nil];
    [self setPasswordField:nil];
    [self setServerField:nil];
    [self setRememberPasswordSwitch:nil];
    [self setForgotPasswordLink:nil];
    [self setDoneButton:nil];
    [self setRegisterEmailField:nil];
    [self setRegisterPasswordField:nil];
    [self setRegisterPasswordConfirmField:nil];
    [self setRegisterServerField:nil];
    [self setRegistrationDoneButton:nil];
    [super viewDidUnload];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    ADXAccount *defaultAccount = [self.loginProcessor defaultAccount];
    if (defaultAccount) {
        self.loginNameField.text = defaultAccount.loginName;
        self.serverField.text = defaultAccount.serverURL;
        self.passwordField.text = defaultAccount.password;
        self.rememberPasswordSwitch.on = defaultAccount.rememberPasswordValue ? YES : NO;
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self updateDoneButton];
    [self updateRegistrationDoneButton];
}

- (void)disableDoneButton
{
    self.doneButton.enabled = NO;
}

- (void)updateDoneButton
{
    self.doneButton.enabled = YES;
}

- (void)disableRegistrationDoneButton
{
    self.registrationDoneButton.enabled = NO;
}

- (void)updateRegistrationDoneButton
{
    self.registrationDoneButton.enabled = YES;
}

#pragma mark - Login

- (void)handleLoginSuccess:(ADXAccount *)account
{
    ADXAppDelegate *appDelegate = (ADXAppDelegate *)[[UIApplication sharedApplication] delegate];
    [appDelegate setReaderAppState:ADXAppState.startupEnded];
    [appDelegate setCurrentAccountByID:account.id];
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MainStoryboard-iPhone" bundle:nil];
    UIViewController *initialViewController = [mainStoryboard instantiateInitialViewController];
    initialViewController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    
    __weak ADXLoginViewController_iPhone *weakSelf = self;
    
    double delayInSeconds = 1.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        dispatch_async(dispatch_get_main_queue(), ^{
            [weakSelf presentViewController:initialViewController animated:YES completion:nil];
        });
    });
}

- (void)handleLoginFailure:(ADXAccount *)account
{
    [self updateDoneButton];
}

- (void)processLogin
{
    __weak ADXLoginViewController_iPhone *weakSelf = self;
    
    [self.loginProcessor performLoginWithName:self.currentLoginName
                                     password:self.currentPassword
                                       server:self.currentServerURL
                             rememberPassword:self.currentRememberPassword
                                   completion:^(BOOL success, NSUInteger statusCode, BOOL online, ADXAccount *account, ADXV2Service *service, NSError *error) {
                                       dispatch_async(dispatch_get_main_queue(), ^{
                                           if (success) {
                                               [weakSelf handleLoginSuccess:account];
                                           } else {
                                               [weakSelf handleLoginFailure:account];
                                           }
                                       });
                                   }];
}

#pragma mark - Create Account Processing

- (void)handleCreateAccountSuccess:(ADXLoginProcessorRegistrationResponse)status
{
    __weak ADXLoginViewController_iPhone *weakSelf = self;
    
    if (status == ADXLoginProcessorRegistrationAcceptedForConfirmation) {
        [UIAlertView displayAlertWithTitle:@"Account Registration"
                                   message:@"A response to your registration request will be sent to your email address."
                           leftButtonTitle:@"OK"
                          leftButtonAction:^{
                              [weakSelf.navigationController popViewControllerAnimated:YES];
                          }
                          rightButtonTitle:nil
                         rightButtonAction:nil];
    } else {
        self.currentRememberPassword = NO;
        dispatch_async(dispatch_get_main_queue(), ^{
            [weakSelf processLogin];
        });
    }
}

- (void)handleCreateAccountFailure
{
    __weak ADXLoginViewController_iPhone *weakSelf = self;
    [UIAlertView displayAlertWithTitle:@"Account Registration"
                               message:@"Registration request failed."
                       leftButtonTitle:@"Cancel"
                      leftButtonAction:^{
                          [weakSelf.navigationController popViewControllerAnimated:YES];
                      }
                      rightButtonTitle:@"Retry"
                     rightButtonAction:^{
                         [weakSelf updateRegistrationDoneButton];
                     }];
}

- (void)processCreateAccountRequest
{
    // TODO: Implement this
    __weak ADXLoginViewController_iPhone *weakSelf = self;
    
    [self.loginProcessor registerWithName:self.currentLoginName
                                    email:self.currentLoginName
                                 password:self.currentPassword
                                   server:self.currentServerURL
                               completion:^(ADXLoginProcessorRegistrationResponse status, NSError *error) {
                                   if (status == ADXLoginProcessorRegistrationFailed) {
                                       dispatch_async(dispatch_get_main_queue(), ^{
                                           [weakSelf handleCreateAccountFailure];
                                       });
                                   } else {
                                       dispatch_async(dispatch_get_main_queue(), ^{
                                           [weakSelf handleCreateAccountSuccess:status];
                                       });
                                   }
                               }];
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == self.loginNameField) {
        [self.passwordField becomeFirstResponder];
    } else if (textField == self.passwordField) {
        [self.serverField becomeFirstResponder];
    } else if (textField == self.serverField) {
        [textField resignFirstResponder];
    }
    
    if (textField == self.registerEmailField) {
        [self.registerPasswordField becomeFirstResponder];
    } else if (textField == self.registerPasswordField) {
        [self.registerPasswordConfirmField becomeFirstResponder];
    } else if (textField == self.registerPasswordConfirmField) {
        [self.registerServerField becomeFirstResponder];
    } else if (textField == self.registerServerField){
        [textField resignFirstResponder];
    }
    
    return YES;
}

#pragma mark - Actions

- (IBAction)didTapLoginDoneButton:(id)sender
{
    [self disableDoneButton];
    
    self.currentLoginName = self.loginNameField.text;
    self.currentPassword = self.passwordField.text;
    self.currentRememberPassword = self.rememberPasswordSwitch.on;
    self.currentServerURL = self.serverField.text;

    [self.view endEditing:YES];
    [self processLogin];
}

- (IBAction)didTapRegistrationDoneButton:(id)sender
{
    __weak ADXLoginViewController_iPhone *weakSelf = self;

    [self disableRegistrationDoneButton];
    
    self.currentLoginName = self.registerEmailField.text;
    self.currentPassword = self.registerPasswordField.text;
    self.currentServerURL = self.registerServerField.text;
    
    if (![self.registerPasswordField.text isEqual:self.registerPasswordConfirmField.text]) {
        [UIAlertView displayAlertWithTitle:@"Account Registration"
                                   message:@"The passwords do not match."
                           leftButtonTitle:@"Retry"
                          leftButtonAction:^{
                              [weakSelf updateRegistrationDoneButton];
                          }
                          rightButtonTitle:nil
                         rightButtonAction:nil];
    } else {
        [self processCreateAccountRequest];
    }
}

@end
