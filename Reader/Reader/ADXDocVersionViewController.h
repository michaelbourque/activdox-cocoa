//
//  ADXDocVersionViewController.h
//  Reader
//
//  Created by Gavin McKenzie on 2012-09-13.
//  Copyright (c) 2012 ActivDox Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ActivDoxCore.h"
#import "ADXActivityImageView.h"
#import "ADXDocActionsDelegate.h"
#import "ADXCollectionDocumentsViewController.h"
#import "ADXDocCollectionSelectionViewController.h"

@interface ADXDocVersionViewController : UIViewController <ADXDocActionsDelegate, ADXDocCollectionSelectionDelegate, ADXCollectionDocumentsActionsDelegate>
@property (strong, nonatomic) ADXDocument *document;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *authorLabel;
@property (weak, nonatomic) IBOutlet ADXActivityImageView *spinner;
@property (strong, nonatomic) IBOutlet UIView *collectionSectionHeaderView;
@property (strong, nonatomic) IBOutlet UIView *versionHistorySectionHeaderView;
@property (strong, nonatomic) IBOutlet UIView *tableHeaderWarningView;
@property (weak, nonatomic) UIPopoverController *parentPopoverController;
@property (assign, nonatomic) BOOL hideShareButton;
@property (assign, nonatomic) BOOL hideDeleteButton;
@property (weak, nonatomic) IBOutlet UIButton *collectionEditButton;
@property (weak, nonatomic) id<ADXDocActionsDelegate,ADXCollectionDocumentsActionsDelegate> actionDelegate;
@property (nonatomic) BOOL allowNavigatingCollections;

+ (UIPopoverController *)popoverControllerWithDocument:(ADXDocument *)document allowNavigatingCollections:(BOOL)allowNavigatingCollections hideDeleteButton:(BOOL)hideDeleteButton managedObjectContext:(NSManagedObjectContext *)moc actionDelegate:(id)delegate;
+ (UIViewController *)modalControllerWithDocument:(ADXDocument *)document allowNavigatingCollections:(BOOL)allowNavigatingCollections hideDeleteButton:(BOOL)hideDeleteButton managedObjectContext:(NSManagedObjectContext *)moc actionDelegate:(id)delegate;
- (void)didTapRemoveButton:(id)sender;
- (IBAction)didTapCollectionsEditButton:(id)sender;
- (void)didTapPermissionsButton:(id)sender;
@end
