//
//  ADXCollectionController.m
//  Reader
//
//  Created by Gavin McKenzie on 2012-10-15.
//  Copyright (c) 2012 ActivDox Inc. All rights reserved.
//

#import "ADXCollectionController.h"
#import "ADXAppDelegate.h"

@interface ADXCollectionController () <NSFetchedResultsControllerDelegate>
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@end

@implementation ADXCollectionController

#pragma mark - Initialization

- (id)initWithManagedObjectContext:(NSManagedObjectContext *)moc sortFilter:(ADXCollectionControllerSort)sortFilter
{
    self = [super init];
    if (self) {
        _managedObjectContext = moc;
        _sortFilter = sortFilter;
    }
    return self;
}

- (ADXV2Service *)service
{
    ADXV2Service *service = [ADXV2Service serviceForAccountID:self.collection.accountID];
    return service;
}

#pragma mark - Data Management

- (void)refresh:(BOOL)forceLoadAll
{
    if (IsEmpty(self.collection.id)) {
        return;
    }
    
    if (forceLoadAll) {
        [self.service forceRefreshCollectionsAndDocuments:nil downloadContent:YES trackProgress:YES];
    } else {
        [self.service refresh];
    }
}

- (NSInteger)numberOfItems
{
    NSInteger result = 0;
    NSInteger numberOfSections = [self numberOfSections];

    for (NSInteger i = 0; i < numberOfSections; i++) {
        result += [self numberOfItemsInSection:i];
    }

    return result;
}

- (NSInteger)numberOfItemsInSection:(NSInteger)section
{
    id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
    NSInteger result = [sectionInfo numberOfObjects];
    return result;
}

- (NSInteger)numberOfSections
{
    NSInteger result = [[self.fetchedResultsController sections] count];
    return result;
}

- (id)objectAtIndexPath:indexPath
{
    id object = [self.fetchedResultsController objectAtIndexPath:indexPath];
    return object;
}

#pragma mark - Data Management

- (void)updateFetchedResultsControllerPredicate
{
    // No need to include account id in predicate, as collection ids are guaranteed to be unique across accounts.
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(ANY collections == %@) AND (%K == %@)", self.collection, ADXDocumentAttributes.versionIsLatest, [NSNumber numberWithBool:YES]];
    [_fetchedResultsController.fetchRequest setPredicate:predicate];
}

- (NSSortDescriptor *)sortDescriptor
{
    NSSortDescriptor *sortDescriptor;
    
    switch (self.sortFilter) {
        case ADXCollectionControllerSortDate:
            sortDescriptor = [[NSSortDescriptor alloc] initWithKey:ADXDocumentAttributes.uploadDate ascending:NO];
            break;
        case ADXCollectionControllerSortChanges:
            sortDescriptor = [[NSSortDescriptor alloc] initWithKey:ADXDocumentAttributes.changeCount ascending:NO];
            break;
        case ADXCollectionControllerSortTitle:
        default:
            sortDescriptor = [[NSSortDescriptor alloc] initWithKey:ADXDocumentAttributes.title ascending:YES selector:@selector(localizedCaseInsensitiveCompare:)];
            break;
    }
    
    return sortDescriptor;
}

- (void)refetchFetchedResultsController
{
    __weak NSFetchedResultsController *frc = _fetchedResultsController;
    
    [self.managedObjectContext performBlockAndWait:^{
        NSError *error = nil;
        BOOL success = [frc performFetch:&error];
        if (!success) {
            LogNSError(@"Couldn't initialize NSFetchedResultsController", error);
        }
    }];
}

- (NSFetchedResultsController *)prepareFetchedResultsController
{
    __weak ADXCollectionController *weakSelf = self;
    __block NSFetchedResultsController *frc = nil;
    
    NSSortDescriptor *sortDescriptor = [self sortDescriptor];
    
    [self.managedObjectContext performBlockAndWait:^{
        frc = [weakSelf.managedObjectContext aps_fetchedResultsControllerForEntityName:[ADXDocument entityName]
                                                                             predicate:nil
                                                                       sortDescriptors:[NSArray arrayWithObject:sortDescriptor]
                                                                    sectionNameKeyPath:nil
                                                                             cacheName:nil
                                                                              delegate:weakSelf];
    }];
    return frc;
}

- (void)prepareSearchFetchedResultsController
{
    //
}

- (NSFetchedResultsController *)fetchedResultsController
{
    if (!_fetchedResultsController) {
        _fetchedResultsController = [self prepareFetchedResultsController];
        [self updateFetchedResultsControllerPredicate];
        [self refetchFetchedResultsController];
    }
    return _fetchedResultsController;
}

- (void)setSortFilter:(ADXCollectionControllerSort)sortFilter
{    
    _sortFilter = sortFilter;
    _fetchedResultsController = nil;
}

- (void)purgeRemovedDocument:(NSString *)documentID
{
    __weak NSManagedObjectContext *moc = self.managedObjectContext;
    
    NSString *accountID = self.collection.accountID;
    
    [self.managedObjectContext performBlockAndWait:^{
        NSArray *documents = [ADXDocument allVersionsOfDocument:documentID accountID:accountID context:moc];
        ADXMetaDocument *metaDocument = [ADXMetaDocument metaDocumentWithID:documentID accountID:accountID context:moc];
        [moc deleteObject:metaDocument];
        
        for (ADXDocument *document in documents) {
            [moc deleteObject:document];
        }
        
        NSError *error = nil;
        BOOL success = [moc save:&error];
        if (!success) {
            LogNSError(@"Error saving main context when attempting to purge document", error);
        }
    }];
}

#pragma mark - NSFetchedResultsControllerDelegate

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    if (self.delegate) {
        [self.delegate controllerWillChangeContent:controller];
    }
}

- (void)controller:(NSFetchedResultsController *)controller
  didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex
     forChangeType:(NSFetchedResultsChangeType)type
{
    if (self.delegate) {
        [self.delegate controller:controller didChangeSection:sectionInfo atIndex:sectionIndex forChangeType:type];
    }
}

- (void)controller:(NSFetchedResultsController *)controller
   didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath
     forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath
{
    if (self.delegate) {
        [self.delegate controller:controller didChangeObject:anObject atIndexPath:indexPath forChangeType:type newIndexPath:newIndexPath];
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    if (self.delegate) {
        [self.delegate controllerDidChangeContent:controller];
    }
}

@end
