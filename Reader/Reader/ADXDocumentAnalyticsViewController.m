//
//  ADXDocumentAnalyticsViewController.m
//  Reader
//
//  Created by Steve Tibbett on 2012-11-27.
//  Copyright (c) 2012 ActivDox Inc. All rights reserved.
//

#import "ADXDocumentAnalyticsViewController.h"
#import "MBProgressHUD.h"

#define ANALYTICS_PATH @"savvydox/docAnalytics.html"
#define ACCESS_TOKEN_COOKIE @"adox_accessToken"
#define DOCID_COOKIE @"adox_docId"

@interface ADXDocumentAnalyticsViewController ()
@property (weak, nonatomic) IBOutlet UIWebView *webView;
- (void)loadAnalyticsForDocument:(ADXDocument *)document;
@end

@implementation ADXDocumentAnalyticsViewController

+ (void)presentAnalyticsForDocument:(ADXDocument *)document parentViewController:(UIViewController *)parentViewController
{
    ADXDocumentAnalyticsViewController *viewController = [[ADXDocumentAnalyticsViewController alloc] initWithNibName:@"ADXDocumentAnalyticsViewController" bundle:nil];
    viewController.title = NSLocalizedString(@"Document Analytics", nil);
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:viewController];
    navigationController.modalPresentationStyle = UIModalPresentationFormSheet;
    UIBarButtonItem *closeButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone
                                                                                 target:viewController
                                                                                 action:@selector(didTapDone:)];
    viewController.navigationItem.rightBarButtonItem = closeButton;
    [parentViewController presentViewController:navigationController animated:YES completion:^{
        [viewController loadAnalyticsForDocument:document];
    }];
    
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.webView.delegate = self;
}

- (void)viewDidUnload {
    [self setWebView:nil];
    [super viewDidUnload];
}

- (void)didTapDone:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)loadAnalyticsForDocument:(ADXDocument *)document
{
    ADXV2Service *service = [ADXV2Service serviceForManagedObject:document];
    ADXAccount *account = service.account;
    
    NSURL *rootURL = [NSURL URLWithString:account.serverURL];
    
    // TODO: this URL string should be configurable or come from somewhere other than being baked into the app
    NSURL *pageURL = [NSURL URLWithString:ANALYTICS_PATH relativeToURL:rootURL];
    
    NSHTTPCookieStorage *cookieJar = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    
    NSDictionary *cookieProperties = [NSMutableDictionary dictionary];
    [cookieProperties setValue:pageURL.host forKey:NSHTTPCookieDomain];
    [cookieProperties setValue:@"/" forKey:NSHTTPCookiePath];
    [cookieProperties setValue:ACCESS_TOKEN_COOKIE forKey:NSHTTPCookieName];
    [cookieProperties setValue:service.accessToken forKey:NSHTTPCookieValue];
    [cookieJar setCookie:[NSHTTPCookie cookieWithProperties:cookieProperties]];
    
    cookieProperties = [NSMutableDictionary dictionary];
    [cookieProperties setValue:pageURL.host forKey:NSHTTPCookieDomain];
    [cookieProperties setValue:@"/" forKey:NSHTTPCookiePath];
    [cookieProperties setValue:DOCID_COOKIE forKey:NSHTTPCookieName];
    [cookieProperties setValue:document.id forKey:NSHTTPCookieValue];
    [cookieJar setCookie:[NSHTTPCookie cookieWithProperties:cookieProperties]];

    NSURLRequest *request = [NSURLRequest requestWithURL:pageURL
                                        cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData
                                         timeoutInterval:30.0];
    [self.webView loadRequest:request];
}

#pragma mark - UIWebViewDelegate

- (void)webViewDidStartLoad:(UIWebView *)webView
{
    MBProgressHUD *HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    HUD.labelText = NSLocalizedString(@"Retrieving Analytics", nil);
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    
    NSString* css = @"\"@font-face { font-family: 'Helvetica';  } body { background-color: #F0F0F0; color: #272B00; font-family: Helvetica; font-size: 7pt; margin: 0px; } th { background-color: #e0e0e0; font-size: 7pt; } td { font-size: 7pt;}  \"";
    NSString* js = [NSString stringWithFormat:
                    @"var styleNode = document.createElement('style');\n"
                    "styleNode.type = \"text/css\";\n"
                    "var styleText = document.createTextNode(%@);\n"
                    "styleNode.appendChild(styleText);\n"
                    "document.getElementsByTagName('head')[0].appendChild(styleNode);\n",css];
    [self.webView stringByEvaluatingJavaScriptFromString:js];
}

@end
