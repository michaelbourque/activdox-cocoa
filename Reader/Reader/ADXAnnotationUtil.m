//
//  ADXAnnotationUtil.m
//  Reader
//
//  Created by matteo ugolini on 2012-10-03.
//  Copyright (c) 2012 ActivDox Inc. All rights reserved.
//

#import "ADXAnnotationUtil.h"
#import "ADXAppDelegate.h"
#import "ADXNoteAnnotation.h"
#import "ADXHighlightAnnotation.h"
#import "ADXAnnotationFilter.h"

#import "PSPDFPageView.h"
#import "PSPDFPageInfo.h"

NSString *kADXAnnotationFilterChangedNotification = @"kADXAnnotationFilterChangedNotification";

@implementation ADXAnnotationUtil

+ (id<ADXAnnotationProtocol>)findInArray:(NSArray *)array onPage:(NSUInteger)page withClass:(Class)cls
{
    for (id<ADXAnnotationProtocol> annotation in array) {
        if ([annotation isKindOfClass:cls]) {
            PSPDFAnnotation *psAnno = (PSPDFAnnotation *)annotation;
            if (psAnno.page == page) {
                return annotation;
            }
        }
    }
    
    return nil;
}

// This method will sync the PSPDFDocument's annotations with the ADXDocument's annotations.
// In a nutshell:
//  - Build a dictionary of note IDs to existing annotations on the PSPDFDocument
//  - Walk the ADXDocument's notes and for each one, either find & update or create a new PSPDFAnnotation
//  - Remove any remaining 

+ (void)addAnnotationsToPDF:(PSPDFDocument*)pdfDocument fromDocument:(ADXDocument*)adxDocument
{
    BOOL showNotes = [[ADXUserDefaults sharedDefaults] annotationFilterShowNotes];
    BOOL showHighlights = [[ADXUserDefaults sharedDefaults] annotationFilterShowHighlights];

    // Catalog the annotations already on the document; this dictionary maps a note's
    // localID to an array of id<ADXAnnotationProtocol> instances
    NSMutableDictionary *annotationsOnDocument = [NSMutableDictionary dictionary];
    for (int i=0; i<pdfDocument.pageCount; i++) {
        NSArray *annotationsOnPage = [pdfDocument annotationsForPage:i type:PSPDFAnnotationTypeHighlight | PSPDFAnnotationTypeNote];
        for (PSPDFAnnotation *annotation in annotationsOnPage) {
            if ([annotation conformsToProtocol:@protocol(ADXAnnotationProtocol)]) {
                id<ADXAnnotationProtocol> adxanno = (id<ADXAnnotationProtocol>)annotation;
                NSMutableArray *array = [annotationsOnDocument valueForKey:adxanno.noteLocalID];
                if (array) {
                    [array addObject:adxanno];
                } else {
                    [annotationsOnDocument setValue:[@[adxanno] mutableCopy] forKey:adxanno.noteLocalID];
                }
            }
        }
    }
    
    // Walk the ADXDocument's annotations
    ADXMetaDocument *metaDocument = adxDocument.metaDocument;
    NSSet *notes = metaDocument.notes;
    NSString *currentUserID = [ADXAppDelegate sharedInstance].currentAccount.user.id;
    ADXAnnotationFilter *annotationFilter = [[ADXAnnotationFilter alloc] init];
    
    for (ADXDocumentNote *note in notes) {
        ADXHighlightAnnotation *highlightAnnotation = nil;
        
        if (!note.isAccessible) {
            // Note is deleted or revoked
            continue;
        }
        
        if ([annotationFilter shouldFilterAnnotation:note onDocument:adxDocument]) {
            continue;
        }

        UIColor *avatarColor;
        if ([note.author.id isEqualToString:currentUserID]) {
            // The current user is always black
            avatarColor = [UIColor blackColor];
        } else {
            avatarColor = [UIColor colorWithHue:(note.author.avatarSeedValue%6)/6.0 saturation:0.5 brightness:0.8 alpha:1.0];
        }
        
        NSMutableArray *existingAnnotations = [annotationsOnDocument objectForKey:note.localID];
        
        NSSet *pages = [note pagesForVersion:adxDocument.version];
        for (NSNumber *page in pages.allObjects) {
            NSNumber *pdfPage = @([page integerValue] - 1);
            NSArray *locationsForPage = [note locationsForVersion:adxDocument.version page:page];
            
            // Calculate the bounding box
            CGRect rect;
            CGRect boundingBox = CGRectZero;
            NSMutableArray *rects = [NSMutableArray array];
            for (NSDictionary *location in locationsForPage) {
                NSArray *highlights = location[ADXJSONDocumentNoteAttributes.note.versions.location.highlightArray];
                if (IsEmpty(highlights)) {
                    LogError(@"Missing location data for note %@", note);
                    continue;
                }
                
                for (NSDictionary *rectDict in highlights) {
                    rect =  [ADXDocumentNote rectFromDictionary:rectDict];
                    [rects addObject:[NSValue valueWithCGRect:rect]];
                    
                    if (CGRectEqualToRect(boundingBox, CGRectZero)) {
                        boundingBox = rect;
                    } else {
                        boundingBox = CGRectUnion(boundingBox, rect);
                    }
                }
            }
            
            // If the bounding box is empty, then it's a floating note; otherwise
            // it's either a highlight, or a note anchored to a highlight.
            
            UIColor *noteColor = note.isClosed ? ADXNoteAnnotationClosedColor : ADXNoteAnnotationOpenColor;

            if (!CGSizeEqualToSize(boundingBox.size, CGSizeZero)) {

                // If it's a note and we're showing notes, or if it's a highlight and we're showing highlights..
                if ((note.text != nil && showNotes) || (note.text == nil && showHighlights) ) {
                    
                    if (boundingBox.size.height < 0) {
                        boundingBox.size.height *= -1;
                        boundingBox.origin.y -= boundingBox.size.height;
                    }
                    
                    ADXHighlightAnnotation *existingHighlight = (ADXHighlightAnnotation *)[ADXAnnotationUtil findInArray:existingAnnotations
                                                                                                                  onPage:pdfPage.unsignedIntegerValue
                                                                                                               withClass:[ADXHighlightAnnotation class]];
                    
                    // If the existing highlight includes a note and the new one wouldn't (or vice versa) then ignore
                    // the existing one (which will get removed later).
                    if ((existingHighlight.noteAnnotation == nil) != (note.text == nil)) {
                        existingHighlight = nil;
                    }
                    
                    if (existingHighlight != nil) {
                        highlightAnnotation = existingHighlight;
                        [ADXHighlightAnnotation updateHighlightAnnotation:highlightAnnotation
                                                                 withPage:pdfPage.unsignedIntegerValue
                                                              boundingBox:boundingBox
                                                                     note:note
                                                                 document:adxDocument
                                                                      pdf:pdfDocument];
                    } else {
                        highlightAnnotation = [ADXHighlightAnnotation highlightAnnotation:boundingBox
                                                                                     page:[pdfPage unsignedIntegerValue]
                                                                                     note:note
                                                                                 document:adxDocument
                                                                                      pdf:pdfDocument];
                        [pdfDocument addAnnotations:@[ highlightAnnotation ] forPage:pdfPage.unsignedIntegerValue];
                        
                        if (highlightAnnotation.noteAnnotation) {
                            [pdfDocument addAnnotations:@[ highlightAnnotation.noteAnnotation ] forPage:pdfPage.unsignedIntegerValue];
                        }
                    };
                    
                    // Add the highlight
                    highlightAnnotation.color = [ADXAnnotationUtil colorForAnnotation:highlightAnnotation note:note];
                    highlightAnnotation.rects = rects;
                    highlightAnnotation.deleted = NO;

                    // Add the note
                    if (highlightAnnotation.noteAnnotation) {
                        highlightAnnotation.noteAnnotation.avatarColor = avatarColor;
                        highlightAnnotation.noteAnnotation.deleted = NO;
                        highlightAnnotation.noteAnnotation.color = noteColor;
                    }
                    
                    [existingAnnotations removeObject:highlightAnnotation];
                    [existingAnnotations removeObject:highlightAnnotation.noteAnnotation];
                }
            } else {
                if (showNotes) {
                    ADXNoteAnnotation *noteAnnotation = (ADXNoteAnnotation *)[ADXAnnotationUtil findInArray:existingAnnotations onPage:pdfPage.unsignedIntegerValue withClass:[ADXNoteAnnotation class]];
                    if (noteAnnotation == nil) {
                        noteAnnotation = [ADXNoteAnnotation noteAnnotation:boundingBox
                                                                      page:[pdfPage unsignedIntegerValue]
                                                                      note:note
                                                                  document:adxDocument
                                                                       pdf:pdfDocument];
                        [pdfDocument addAnnotations:@[ noteAnnotation ] forPage:pdfPage.unsignedIntegerValue];
                    } else {
                        [ADXNoteAnnotation updateNoteAnnotation:noteAnnotation
                                               withDocumentNote:note
                                                    boundingBox:boundingBox
                                                       document:adxDocument
                                                            pdf:pdfDocument
                                                           page:pdfPage.unsignedIntegerValue];
                        
                        [existingAnnotations removeObject:noteAnnotation];
                    }

                    noteAnnotation.deleted = NO;
                    noteAnnotation.color = noteColor;
                    noteAnnotation.avatarColor = avatarColor;
                }
            }
        }
    }
    
    // Any annotations left in annotationsOnDocument are not needed. Remove 'em.
    for (NSArray *array in annotationsOnDocument.allValues) {
        for (PSPDFAnnotation *annotation in array) {
            if ([annotation isKindOfClass:[ADXNoteAnnotation class]]) {
                ADXNoteAnnotation *note = (ADXNoteAnnotation *)annotation;
                note.deleted = YES;
                if (note.highlightAnnotation) {
                    note.highlightAnnotation.deleted = YES;
                }
            } else
                if ([annotation isKindOfClass:[ADXHighlightAnnotation class]]) {
                    ADXHighlightAnnotation *highlight = (ADXHighlightAnnotation *)annotation;
                    highlight.deleted = YES;
                    if (highlight.noteAnnotation) {
                        highlight.noteAnnotation.deleted = YES;
                    }
                } else
                {
                    NSAssert(nil, @"Unrecognized annotation type found in list");
                }
        }
    }
}

+ (UIColor *)colorForAnnotation:(id<ADXAnnotationProtocol>)annotation note:(ADXDocumentNote *)note
{
    UIColor *result = ADXHighlightAnnotationColor;
    if (note.isClosed) {
        result = [UIColor colorWithWhite:0.85f alpha:1.0f];
    }
    return result;
}

+ (ADXDocumentNote *)noteFromAnnotation:(id<ADXAnnotationProtocol>)annotation
{
    if (![annotation conformsToProtocol:@protocol(ADXAnnotationProtocol)]) {
        LogError(@"received annotation of incorrect class %@", annotation);
        return nil;
    }
    
    NSManagedObjectContext *moc = [ADXAppDelegate sharedInstance].managedObjectContext;
    ADXDocumentNote *note = nil;
    
    NSString *accountID = annotation.accountID;
    NSString *noteLocalID = annotation.noteLocalID;
    NSString *noteID = annotation.noteID;
    
    if (!IsEmpty(noteID)) {
        note = [ADXDocumentNote noteWithID:noteID accountID:accountID context:moc];
    }
    if (!note) {
        note = [ADXDocumentNote noteWithLocalID:noteLocalID accountID:accountID context:moc];
    }
    
    return note;
}

+ (ADXDocument *)documentFromAnnotation:(id<ADXAnnotationProtocol>)annotation
{
    if (![annotation conformsToProtocol:@protocol(ADXAnnotationProtocol)]) {
        LogError(@"received annotation of incorrect class %@", annotation);
        return nil;
    }

    NSManagedObjectContext *moc = [ADXAppDelegate sharedInstance].managedObjectContext;
    ADXDocument *document = nil;
    
    if ([annotation isMemberOfClass:[ADXNoteAnnotation class]]) {
        ADXNoteAnnotation *noteAnnotation = (ADXNoteAnnotation *)annotation;
        document = [ADXDocument documentWithID:noteAnnotation.documentID version:noteAnnotation.versionID accountID:noteAnnotation.accountID context:moc];
    } else if ([annotation isMemberOfClass:[ADXHighlightAnnotation class]]) {
        ADXHighlightAnnotation *highlightAnnotation = (ADXHighlightAnnotation *)annotation;
        document = [ADXDocument documentWithID:highlightAnnotation.documentID version:highlightAnnotation.versionID accountID:highlightAnnotation.accountID context:moc];
    }
    
    return document;
}

+ (BOOL)currentUserIsAuthorOfAnnotation:(id<ADXAnnotationProtocol>)annotation
{
    BOOL result = NO;    
    NSString *accountID = [(id<ADXAnnotationProtocol>)annotation accountID];

    ADXDocumentNote *note = [ADXAnnotationUtil noteFromAnnotation:annotation];
    ADXUser *noteAuthor = note.author;
    NSString *noteAuthorID = noteAuthor.id;
    
    if ([noteAuthorID isEqualToString:accountID]) {
        result = YES;
    }
    return result;
}

+ (BOOL)currentUserIsAuthorOfDocumentForAnnotation:(id<ADXAnnotationProtocol>)annotation
{
    BOOL result = NO;
    NSManagedObjectContext *moc = [ADXAppDelegate sharedInstance].managedObjectContext;
    NSString *accountID = [(id<ADXAnnotationProtocol>)annotation accountID];
    
    ADXDocumentNote *note = [ADXAnnotationUtil noteFromAnnotation:annotation];
    ADXMetaDocument *metaDocument = note.metaDocument;
    ADXDocument *document = [ADXDocument latestDocumentWithID:metaDocument.documentID accountID:metaDocument.accountID context:moc];
    ADXUser *documentAuthor = document.author;
    NSString *documentAuthorID = documentAuthor.id;
    
    if ([documentAuthorID isEqualToString:accountID]) {
        result = YES;
    }
    return result;
}

+ (void)updateAnnotationsOnPDF:(PSPDFDocument *)pdfDocument fromDocument:(ADXDocument *)adxDocument
{
    
}

@end
