//
//  ADXCollectionInfoViewController.h
//  Reader
//
//  Created by Gavin McKenzie on 2013-04-08.
//  Copyright (c) 2013 ActivDox Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ActivDoxCore.h"
#import "ADXCollectionDocumentsViewController.h"

typedef enum {
    ADXCollectionInfoModeDisplayMetadata = 0,
    ADXCollectionInfoModeDisplayDocuments,
} ADXCollectionInfoMode;

@class ADXCollectionInfoViewController;

@protocol ADXCollectionActionsDelegate <NSObject>
- (void)didSelectDocument:(ADXDocument *)document;
- (void)didTapDeleteCollection:(ADXCollection *)collection;
- (void)didRemoveDocument:(ADXDocument *)document fromCollection:(ADXCollection *)collection;
- (void)didAddDocument:(ADXDocument *)document toCollection:(ADXCollection *)collection;
@end

@interface ADXCollectionInfoViewController : UIViewController <ADXCollectionDocumentsActionsDelegate>
+ (UIPopoverController *)popoverControllerWithCollection:(ADXCollection *)collection initialMode:(ADXCollectionInfoMode)initialMode title:(NSString *)title actionDelegate:(id)delegate;
@property (weak, nonatomic) UIPopoverController *parentPopoverController;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) ADXCollection *collection;
@property (assign, nonatomic) ADXCollectionInfoMode initialMode;
@property (weak, nonatomic) id<ADXCollectionActionsDelegate> actionDelegate;
@end
