//
//  ADXDescriptionCell.m
//  Reader
//
//  Created by Matteo Ugolini on 12-08-29.
//  Copyright (c) 2012 ActivDox Inc. All rights reserved.
//

#import "ADXDescriptionCell.h"
#import <QuartzCore/QuartzCore.h>

@implementation ADXDescriptionCell

@synthesize shadow = _shadow;
@synthesize textSampleLabel = _textSampleLabel;
@synthesize pageInfoLabel = _pageInfoLabel;
@synthesize background = _background;

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    __weak ADXDescriptionCell *weakSelf = self;

    [super setSelected:selected animated:animated];
    void (^selectEffects)(void) = ^ {
        weakSelf.background.backgroundColor =
            selected ? [UIColor colorWithWhite:0.6 alpha:0.9] : [UIColor colorWithWhite:1 alpha:0.9];
    };
    
    if (animated) {
        [UIView animateWithDuration:0.25 animations:selectEffects];
    } else {
        selectEffects();
    }
}

- (void) setShadow:(UIImageView *)shadow
{
    _shadow = shadow;
    _shadow.image = [[UIImage imageNamed:@"MessagePaperShadow.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(15, 300, 0, 300)];
}

- (void) setBackground:(UIView *)background{
    _background = background;
    _background.layer.cornerRadius = 3;
    _background.layer.masksToBounds = YES;
}

@end
