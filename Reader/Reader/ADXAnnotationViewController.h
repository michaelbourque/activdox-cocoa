//
//  ADXAnnotationViewController.h
//  PSPDFCatalog
//
//  Copyright (c) 2012 Peter Steinberger. All rights reserved.
//



#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#import "PSPDFAttributedLabel.h"
#import "PSPDFCache.h"
#import "PSPDFViewController.h"

#import "ADXAppCommonMacros.h"
#import "ADXPDFViewController.h"
#import "ADXPageThumbnailView.h"

typedef NS_ENUM(NSUInteger, ADXDocumentAnnotationMode) {
    ADXDocumentAnnotationModeNone,
    ADXDocumentAnnotationModeComment,
    ADXDocumentAnnotationModeHighlight,
    ADXDocumentAnnotationModeStrikeOut,
    ADXDocumentAnnotationModeUnderline
};

/// Display all annotations in the document
@interface ADXAnnotationViewController : UIViewController <PSPDFSelectionViewDelegate, ADXPDFToolsDelegate, ADXPDFStripsViewControllerDataSource, UIPopoverControllerDelegate>

@property (nonatomic, strong) IBOutlet UIBarButtonItem *annotationsCountBarButton;
@property (nonatomic, strong) IBOutlet UIBarButtonItem *showHideAnnotationsBarButton;
@property (nonatomic, strong) IBOutlet UIBarButtonItem *highlightButton;
@property (nonatomic, strong) IBOutlet UIBarButtonItem *addNoteButton;

@property (nonatomic, assign) ADXDocumentAnnotationMode   documentAnnotationMode;

- (id)initWithDocument:(PSPDFDocument *)document pdfController:(ADXPDFViewController *)pdfController;

@end
