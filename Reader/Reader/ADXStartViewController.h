//
//  ADXStartViewController.h
//  Reader
//
//  Created by Gavin McKenzie on 12-06-08.
//  Copyright (c) 2012 ActivDox Inc. All rights reserved.
//

#import "ADXBaseStartViewController.h"
#import "ADXAppCommonMacros.h"
#import "ADXBaseViewController.h"
#import "ADXActivityImageView.h"

typedef void(^ADXStartVCCompletionBlock)(NSError *error);

@interface ADXStartViewController : ADXBaseStartViewController <ADXURLSchemeRequestDelegate>
@property (weak, nonatomic) IBOutlet UIView *panelContainer;

@property (strong, nonatomic) IBOutlet UIView *createOrLoginPanel;
@property (strong, nonatomic) IBOutlet UIView *loginPanel;
@property (strong, nonatomic) IBOutlet UIView *loginProgressPanel;
@property (strong, nonatomic) IBOutlet UIView *loginFailedPanel;
@property (strong, nonatomic) IBOutlet UIView *loginResetPanel;
@property (strong, nonatomic) IBOutlet UIView *loginRegisterPanel;

@property (weak, nonatomic) IBOutlet UITextView *createOrLoginPanelBannerTextView;
@property (weak, nonatomic) IBOutlet UITextField *loginNameField;
@property (weak, nonatomic) IBOutlet UITextField *passwordField;
@property (weak, nonatomic) IBOutlet UITextField *serverField;
@property (weak, nonatomic) IBOutlet UITextView *loginFailedMessage;
@property (weak, nonatomic) IBOutlet UISwitch *rememberPasswordSwitch;
@property (weak, nonatomic) IBOutlet UITextField *activityMessage;
@property (weak, nonatomic) IBOutlet ADXActivityImageView *activityIndicator;
@property (weak, nonatomic) IBOutlet UITextField *loginResetPasswordEmailField;
@property (weak, nonatomic) IBOutlet UITextField *loginResetPasswordServerField;
@property (weak, nonatomic) IBOutlet UITextField *registerFullNameField;
@property (weak, nonatomic) IBOutlet UITextField *registerEmailField;
@property (weak, nonatomic) IBOutlet UITextField *registerPasswordField;
@property (weak, nonatomic) IBOutlet UITextField *registerPasswordConfirmField;
@property (weak, nonatomic) IBOutlet UITextField *registerServerField;
@property (weak, nonatomic) IBOutlet UILabel *buildInfoLabel;
@property (weak, nonatomic) IBOutlet UIButton *loginFailedCancelButton;
@property (weak, nonatomic) IBOutlet UIButton *loginButton;

- (IBAction)didTapLoginButton:(id)sender;
- (IBAction)didTapHaveAnAccountButton:(id)sender;
- (IBAction)didTapCreateAnAccountButton:(id)sender;
- (IBAction)didTapLoginTryAgainButton:(id)sender;
- (IBAction)didTapLoginCancelButton:(id)sender;
- (IBAction)didTapRegisterCancelButton:(id)sender;
- (IBAction)didTapForgotPasswordButton:(id)sender;
- (IBAction)didTapResetPasswordButton:(id)sender;
- (IBAction)didTapCancelResetPasswordButton:(id)sender;
- (IBAction)didTapFinishRegistrationButton:(id)sender;
@end
