//
//  ADXPDFPageView.h
//  Reader
//
//  Created by Matteo Ugolini on 2012-10-01.
//  Copyright (c) 2012 ActivDox Inc. All rights reserved.
//

#import "PSPDFPageView.h"
#import "ADXPSPDFNoteAnnotationController.h"

extern NSString *ADXPDFPageViewRenderingCompleteNotification;

@interface ADXPDFPageView : PSPDFPageView <ADXPSPDFNoteAnnotationControllerDelegate>
@property (nonatomic, assign, getter=isRendering)  BOOL rendering;
@end
