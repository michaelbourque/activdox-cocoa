//
//  ADXDocVersionsComparisonPopoverCell.h
//  Reader
//
//  Created by Gavin McKenzie on 2012-09-13.
//  Copyright (c) 2012 ActivDox Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ActivDoxCore.h"
#import "ADXActivityImageView.h"

extern CGFloat const kADXDocVersionsComparisonPopoverCellHeight;
extern CGFloat const kADXDocVersionsComparisonPopoverCellWidth;

@interface ADXDocVersionsComparisonPopoverCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *versionLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (assign, nonatomic) BOOL enabled;
@property (strong, nonatomic) ADXDocument *document;
@property (weak, nonatomic) IBOutlet UILabel *bookmarkLabel;

@end
