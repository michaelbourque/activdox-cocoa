//
//  ADXTintedImageView.m
//  Reader
//
//  Created by Gavin McKenzie on 2013-03-28.
//  Copyright (c) 2013 ActivDox Inc. All rights reserved.
//

#import "ADXTintedImageView.h"

@implementation ADXTintedImageView

- (id)initWithImage:(UIImage *)image
{
    self = [super initWithFrame:CGRectMake(0, 0, image.size.width, image.size.height)];
    
    if(self)
    {
        self.image = image;
        
        //set the view to opaque
        self.opaque = NO;
    }
    
    return self;
}

- (void)setTintColor:(UIColor *)color
{
    _tintColor = color;
    [self setNeedsDisplay];
}

- (void)drawRect:(CGRect)rect
{
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    //resolve CG/iOS coordinate mismatch
    CGContextScaleCTM(context, 1, -1);
    CGContextTranslateCTM(context, 0, -rect.size.height);
    
    //set the clipping area to the image
    CGContextClipToMask(context, rect, _image.CGImage);
    
    //set the fill color
    CGContextSetFillColor(context, CGColorGetComponents(_tintColor.CGColor));
    CGContextFillRect(context, rect);
    
    //blend mode overlay
    CGContextSetBlendMode(context, kCGBlendModeOverlay);
    
    //draw the image
    CGContextDrawImage(context, rect, _image.CGImage);
}

@end
