//
//  ADXDocVersionComparisonViewController.h
//  Reader
//
//  Created by Gavin McKenzie on 2012-09-13.
//  Copyright (c) 2012 ActivDox Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ActivDoxCore.h"
#import "ADXActivityImageView.h"

@protocol ADXDocVersionComparisonViewControllerDelegate <NSObject>
@required
- (void) didSelectDoumentForComparison:(ADXDocument*)document;
@end

@interface ADXDocVersionComparisonViewController : UIViewController
@property (strong, nonatomic) ADXDocument *document;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet ADXActivityImageView *spinner;
@property (strong, nonatomic) IBOutlet UIView *tableHeaderWarningView;
@property (weak, nonatomic) UIPopoverController *parentPopoverController;
@property (weak, nonatomic) id<ADXDocVersionComparisonViewControllerDelegate> actionDelegate;

+ (UIPopoverController *)popoverControllerWithDocument:(ADXDocument *)document managedObjectContext:(NSManagedObjectContext *)moc actionDelegate:(id)delegate;
+ (UIViewController *)modalControllerWithDocument:(ADXDocument *)document managedObjectContext:(NSManagedObjectContext *)moc actionDelegate:(id)delegate;
@end
