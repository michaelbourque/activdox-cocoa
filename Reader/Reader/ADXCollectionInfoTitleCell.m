//
//  ADXCollectionInfoTitleCell.m
//  Reader
//
//  Created by Gavin McKenzie on 2013-04-09.
//  Copyright (c) 2013 ActivDox Inc. All rights reserved.
//

#import "ADXCollectionInfoTitleCell.h"

@implementation ADXCollectionInfoTitleCell

- (void)awakeFromNib
{
    [self.titleField addTarget:self action:@selector(didUpdateTextField:) forControlEvents:UIControlEventEditingChanged];
    [self.titleField addTarget:self action:@selector(didEndEditingTextField:) forControlEvents:(UIControlEventEditingDidEndOnExit|UIControlEventEditingDidEnd)];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    if (selected) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.titleField becomeFirstResponder];
            [self setSelected:NO animated:YES];
        });
    }
}

- (void)didUpdateTextField:(id)sender
{
    if (self.actionDelegate) {
        [self.actionDelegate didUpdateTitleField:self.titleField.text];
    }
}

- (void)didEndEditingTextField:(id)sender
{
    if (self.actionDelegate) {
        [self.actionDelegate didEndEditingTextField:self.titleField.text];
    }
}

@end
