//
//  ADXDocSharingController.h
//  Reader
//
//  Created by Gavin McKenzie on 2012-09-14.
//  Copyright (c) 2012 ActivDox Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MessageUI/MessageUI.h>
#import "ActivDoxCore.h"

@interface ADXDocSharingController : NSObject 
+ (ADXDocSharingController *)controllerWithDocument:(ADXDocument *)document mailDelegate:(id<MFMailComposeViewControllerDelegate>)delegate;
- (BOOL)presentShareDocumentViewFromViewController:(UIViewController *)viewController;
@end
