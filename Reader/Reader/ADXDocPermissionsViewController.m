//
//  ADXDocPermissionsViewController.m
//  Reader
//
//  Created by Kei Turner on 2013-09-09.
//  Copyright (c) 2013 ActivDox Inc. All rights reserved.
//

#import "ADXDocPermissionsViewController.h"
#import "ADXDocPermissionsCell.h"
#import "ADXDocPermissionSettingViewController.h"

@interface ADXDocPermissionsViewController ()

@end

@implementation ADXDocPermissionsViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}



- (IBAction)addRecipientsAction:(id)sender {
    
}

-(id)initWithStyle: (UITableViewStyle)style andIncludedMembers:(NSArray*) members
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
        includedMembers = [[NSMutableArray alloc]initWithArray:members];
    }
    return self;
}

- (CGSize)contentSizeForViewInPopover
{
    CGSize result = CGSizeMake(320.0f, 436.0f);
    return result;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"popover_bg"]];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source
-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    return self.permissionsHeaderView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return self.permissionsHeaderView.frame.size.height;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return includedMembers.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = nil;
    
    if (indexPath.section == 0) {
        ADXDocPermissionsCell *permissionCell = (ADXDocPermissionsCell *)[self.tableView dequeueReusableCellWithIdentifier:NSStringFromClass([ADXDocPermissionsCell class])];
        
        if (!permissionCell) {
            UINib *nib = [UINib nibWithNibName:NSStringFromClass([ADXDocPermissionsCell class]) bundle:[NSBundle mainBundle]];
            permissionCell = [[nib instantiateWithOwner:self options:nil] objectAtIndex:0];
        }
        [permissionCell.usernameLabel setText:@"Frank Connors"];
        [permissionCell.usernameLabel setTextColor:[UIColor colorWithWhite:0.3 alpha:1.0]];
        [permissionCell.permissionTypeLabel setText:@"Limited Access"];
        [permissionCell.permissionTypeLabel setTextColor:[UIColor colorWithWhite:0.3 alpha:1.0]];
        
        
        cell = permissionCell;
    }
    
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    ADXDocPermissionSettingViewController *permissionSettingsView = [[ADXDocPermissionSettingViewController alloc]initWithStyle:UITableViewStylePlain andIncludedLocations:[NSArray arrayWithObject:@"Location 1"]];
    
    [permissionSettingsView setTitle:@"Permission Settings"];
    [self.navigationController pushViewController:permissionSettingsView animated:YES];
    
}

@end
