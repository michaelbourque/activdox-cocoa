//
//  ADXAnnotationFilterTableViewController.h
//  Reader
//
//  Created by Steve Tibbett on 2013-03-25.
//  Copyright (c) 2013 ActivDox Inc. All rights reserved.
//

#import "ActivDoxCore.h"

@interface ADXAnnotationFilterTableViewController : UITableViewController

@end
