
//
//  ADXPDFViewController.m
//  Reader
//
//  Created by matteo ugolini on 2012-07-31.
//  Copyright (c) 2012 ActivDox Inc. All rights reserved.
//

#import "ADXPDFViewController.h"
#import <MessageUI/MessageUI.h>

#import "UIViewController+MJPopupViewController.h"
#import "UIViewController+ActivDox.h"
#import "MJPopupBackgroundView.h"

#import "ActivDoxCore.h"

#import "ADXAppDelegate.h"
#import "ADXDocVersionViewController.h"
#import "ADXSearchViewController.h"
#import "ADXAnnotationViewController.h"
#import "ADXRevisionsViewController.h"
#import "ADXDocSharingController.h"
#import "ADXHighlightAnnotation.h"
#import "ADXNoteAnnotation.h"
#import "ADXNoteAnnotationView.h"
#import "ADXPDFPageView.h"
#import "ADXAnnotationUtil.h"
#import "ADXToolbarController.h"
#import "ADXMultipleDelegate.h"
#import "ADXToolbar.h"
#import "ADXDocumentAnalyticsViewController.h"
#import "ADXPDFStripsViewController.h"
#import "ADXNotificationsViewController.h"
#import "ADXNotificationBubbleManager.h"
#import "ADXAnnotationFilterTableViewController.h"
#import "ADXPageSliderViewController.h"
#import "ADXPopoverBackgroundView.h"

#import "PSPDFViewController+Delegates.h"
#import "PSPDFViewController+Internal.h"
#import "PSPDFGlyph.h"

#import "PSPDFDocument.h"
#import "PSPDFTextBlock.h"
#import "PSPDFNoteAnnotationController.h"
#import "ADXPSPDFNoteAnnotationController.h"
#import "PSPDFSearchHighlightView.h"
#import "ADXPSPDFSearchHighlightView.h"

#import "MBProgressHUD.h"

#define kBarHeight 44
#define KHighlightColor [UIColor colorWithRed:0 green:.7 blue:.36 alpha:1]
#define PSPDFLoadingViewTag 225475

NSString * const kADXPDFViewControllerDidShowPage = @"kADXPDFViewControllerDidShowPage";
NSString * const kADXPDFViewControllerDidShowPageUserInfoPageKey = @"kADXPDFViewControllerDidShowPageUserInfoPageKey";

static const NSInteger kADXPDFViewControllerDocumentVersionsButtonTag = 10;

@interface ADXPDFViewController () <ADXNotificationsActionsDelegate, MFMailComposeViewControllerDelegate, PSPDFSelectionViewDelegate, ADXNotificationBubbleManagerDelegate>
@property (strong, readwrite, nonatomic) ADXDocument *adxDocument;
@property (strong, nonatomic, readwrite) ADXV2Service *service;
@property (weak, nonatomic)   UIPanGestureRecognizer *navigationBarPanGestureRecognizer;
@property (strong, nonatomic) UIPopoverController *currentPopoverController;
@property (strong, nonatomic) NSTimer *pageIdleTimer;

@property (strong, nonatomic) ADXDocSharingController *documentSharingController;

@property (nonatomic, assign) BOOL keyboardlVisible; //fixme change that ?

@property (assign, nonatomic) BOOL originalNavBarTranslucentSetting;

@property (weak, nonatomic) UIButton *infoButton;
@property (weak, nonatomic) UIBarButtonItem *infoBarButtonItem;
@property (weak, nonatomic) UIButton *showThumbsButton;
@property (weak, nonatomic) UIButton *showFilterPopoverButton;
@property (weak, nonatomic) UIButton *notificationsButton;

@property (nonatomic, strong) IBOutlet UIView *toolsPane;
@property (nonatomic, strong) IBOutlet UIBarButtonItem *openCloseButton;
@property (assign, nonatomic) NSInteger pendingClosePageIndex;

@property (nonatomic, strong) ADXMultipleDelegate   *toolsDelegates;
@property (nonatomic, strong) UIViewController      *privatePresentedViewController;
@property (nonatomic, strong) UIView                *toolbarContainer;

@property (nonatomic, assign) UIModalPresentationStyle actualModalPresentationStyle;

@property (strong, nonatomic) ADXPDFStripsViewController *PDFStripsViewController;
@property (strong, nonatomic) UIView *stripToPosition;
@property (nonatomic) NSUInteger stripPageIndex;
@property (nonatomic) CGRect stripPageRect;
@property (nonatomic) CGRect stripFocusRect;

@property (weak, nonatomic) ADXNotificationBubble *versionAlertBubble;

@end

@implementation ADXPDFViewController

#pragma mark - NSObject

- (id)initWithDocument:(ADXDocument *)adxDocument pdfDocument:(PSPDFDocument *)pdfDocument
{
    self = [super initWithDocument:pdfDocument];
    if (self) {
        self.overrideClassNames = @{
            (id)[PSPDFPageView class] : [ADXPDFPageView class],
            (id)[PSPDFNoteAnnotationController class]: [ADXPSPDFNoteAnnotationController class],
            (id)[PSPDFSearchHighlightView class]: [ADXPSPDFSearchHighlightView class],
            (id)[PSPDFNoteAnnotationView class] : [ADXNoteAnnotationView class]
        };
        pdfDocument.annotationSaveMode = PSPDFAnnotationSaveModeDisabled;
        
        [ADXAnnotationUtil addAnnotationsToPDF:pdfDocument fromDocument:adxDocument];
        _adxDocument = adxDocument;
        _pendingClosePageIndex = -1;
        _toolsDelegates = [ADXMultipleDelegate new];
        [self updateUserDefaultsForCurrentlyOpenDocument];
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    UIImage *buttonBack = [[UIImage imageNamed:@"back_button_reader.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(0.0f, 30.0f, 0.0f, 20.0f)];
    UIImage *buttonBackPushed = [[UIImage imageNamed:@"back_button_reader_pushed.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(0.0f, 30.0f, 0.0f, 20.0f)];
    [[UIBarButtonItem appearance] setBackButtonBackgroundImage:buttonBack forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    [[UIBarButtonItem appearance] setBackButtonBackgroundImage:buttonBackPushed forState:UIControlStateHighlighted barMetrics:UIBarMetricsDefault];

    self.pageMode = PSPDFPageModeSingle;
    self.fitToWidthEnabled = YES;
    
    [self setStatusBarStyleSetting:PSPDFStatusBarInherit];
    [self setToolbarEnabled:NO];
    [self setupNavbar];
    
    ADXV2Service *service = [ADXV2Service serviceForManagedObject:self.adxDocument];
    self.service = service;
    
    [self.service refreshDocumentVersions:self.adxDocument.id completion:nil];
    
    // fixme: this doesnt need animore. Now we overrides the delegate methods directly.
    // [super setDelegate:self];
    
    self.toolbarController = [ADXToolbarController new];
    [self addChildViewController:self.toolbarController];
    [self.toolbarController didMoveToParentViewController:self];
    self.toolbarController.delegate = self;
    
    //fixme: add the controllers
    ADXAnnotationViewController *annotationController = [[ADXAnnotationViewController alloc] initWithDocument:self.document pdfController:self];
    ADXSearchViewController *searchController = [[ADXSearchViewController alloc] initWithDocument:self.document pdfController:self];
    ADXRevisionsViewController *revisionsViewController = [[ADXRevisionsViewController alloc] initWithDocument:self.document pdfController:self];
    // Force the toolbar to load
    [self.toolbarController view];
    self.toolbarController.viewControllers = @[revisionsViewController, annotationController, searchController];
    self.annotationsVisible = YES;
    self.createAnnotationMenuEnabled = NO;
    
    self.linkAction = PSPDFLinkActionOpenSafari;
}

- (void)viewWillAppear:(BOOL)animated
{
    // It's very important that we setup this observer *before* calling [super viewWillAppear] in order to catch the initial page event.
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleShowPageNotification:)
                                                 name:kADXPDFViewControllerDidShowPage
                                               object:nil];

    [super viewWillAppear:animated];

    ADXAppDelegate *appDelegate = (ADXAppDelegate *)[[UIApplication sharedApplication] delegate];
    [appDelegate setReaderAppState:ADXAppState.viewingDocument];
    [self updateUserDefaultsForCurrentlyOpenDocument];
    
    self.pagingScrollView.alwaysBounceVertical = NO;
    
    // override UIAppearance
    self.originalNavBarTranslucentSetting = self.navigationController.navigationBar.translucent;
    self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
    self.navigationController.navigationBar.translucent = YES;
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"toolbar_reader_view.png"] forBarMetrics:UIBarMetricsDefault];

    if ( !self.toolbarContainer )
    {
        self.toolbarContainer = [[UIView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height - (self.navigationBarHidden ? 0 : kBarHeight), self.view.frame.size.width, kBarHeight)];
        self.toolbarContainer.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin;
    
        ADXToolbar *toolbar = self.toolbarController.toolBar;
        toolbar.tintColor = [UIColor darkGrayColor];
        [toolbar setBackgroundImage:[UIImage imageNamed:@"toolbar_reader_view.png"] forToolbarPosition:UIToolbarPositionAny barMetrics:UIBarMetricsDefault];
    
        if(UI_USER_INTERFACE_IDIOM() != UIUserInterfaceIdiomPhone)
        {
            UIInterfaceOrientation interfaceOrientation = [[UIApplication sharedApplication] statusBarOrientation];
            float keyboardHeight = UIDeviceOrientationIsLandscape(interfaceOrientation) ? 352 : 264;
            self.toolsPane = [[UIView alloc]initWithFrame:CGRectMake(0, self.view.frame.size.height + (self.navigationBarHidden ? 0 : kBarHeight), self.view.frame.size.width, keyboardHeight)];

            self.toolsPane.backgroundColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.8];

            [self.toolsPane setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin];
            
            [self.view addSubview:self.toolsPane];
        }
        
        self.toolbarController.view.frame = self.toolbarContainer.bounds;
        [self.view addSubview:self.toolbarContainer];
        [self.toolbarContainer addSubview:self.toolbarController.view];
    }
    
    ADXSelectedDocumentTab selectedTab = [[ADXUserDefaults sharedDefaults] currentlySelectedDocumentTab];
    switch (selectedTab) {
        case ADXSelectedDocumentTabRevisions:
            [self.toolbarController setSelectedIndex:0];
            break;
            
        case ADXSelectedDocumentTabAnnotations:
            [self.toolbarController setSelectedIndex:1];
            break;

        case ADXSelectedDocumentTabSearch:
            [self.toolbarController setSelectedIndex:2];
            break;
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];

    [self watchNotifications];
    
    [self updateDocumentVersionStatusIndicator];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];

    [self unwatchNotifications];
    
    [self dismissPopovers];
    
    //backTo UIAppearance
    [[ADXAppDelegate sharedInstance] setupDefaultNavbarAppearance];
    self.navigationController.navigationBar.barStyle = [[UIToolbar appearance] barStyle];
    self.navigationController.navigationBar.translucent = self.originalNavBarTranslucentSetting;
    
    UIImage * img = [[UINavigationBar appearance] backgroundImageForBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setBackgroundImage:img
                                                  forBarMetrics:UIBarMetricsDefault];
    
    if ([self isMovingFromParentViewController]) {
        if (self.versionAlertBubble) {
            [[ADXNotificationBubbleManager sharedInstance] removeBubble:self.versionAlertBubble];
        }
        [self willCloseDocument];
    }
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    return PSIsIpad() ? YES : toInterfaceOrientation != UIInterfaceOrientationPortraitUpsideDown;
}

- (void) willRotateToInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation duration:(NSTimeInterval)duration
{
    __weak ADXPDFViewController *weakSelf = self;
    [self.toolsDelegates enumerateDelegateWhoRespondsToSelector:@selector(pdfController:willRotateToInterfaceOrientation:) usingBlock:^(id delegate, BOOL *stop) {
        [delegate pdfController:weakSelf willRotateToInterfaceOrientation:fromInterfaceOrientation];
    }];

    if (!self.HUDVisible) {
        // Temporarily show the status bar during the rotation so the navigation bar's position is calculated correctly
        [[UIApplication sharedApplication] setStatusBarHidden:NO];
    }
    
    [super willRotateToInterfaceOrientation:fromInterfaceOrientation duration:duration];
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    if (!self.HUDVisible) {
        // Hide the status bar again
        [[UIApplication sharedApplication] setStatusBarHidden:YES];
    }
    
    [super willAnimateRotationToInterfaceOrientation:toInterfaceOrientation duration:duration];
}

- (void) didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    __weak ADXPDFViewController *weakSelf = self;
    [self.toolsDelegates enumerateDelegateWhoRespondsToSelector:@selector(pdfController:didRotateFromInterfaceOrientation:)
                                                     usingBlock:^(id delegate, BOOL *stop) {
              [delegate pdfController:weakSelf didRotateFromInterfaceOrientation:fromInterfaceOrientation];
    }];
    [super didRotateFromInterfaceOrientation:fromInterfaceOrientation];
}

- (void)setupNavbar
{
    UIButton *infoButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [infoButton setShowsTouchWhenHighlighted:YES];
    [infoButton setImage:[UIImage imageNamed:@"document_old.png"] forState:UIControlStateNormal];
    [infoButton addTarget:self action:@selector(handleBarButtonPress:) forControlEvents:UIControlEventTouchUpInside];
    [infoButton setFrame:CGRectMake(0.0f, 0.0f, 45.0f, 25.0f)];
    UIBarButtonItem *infoBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:infoButton];
    infoBarButtonItem.tag = kADXPDFViewControllerDocumentVersionsButtonTag;
    self.infoButton = infoButton;
    self.infoBarButtonItem = infoBarButtonItem;
    
    UIButton *showThumbsButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [showThumbsButton setShowsTouchWhenHighlighted:YES];
    [showThumbsButton setImage:[UIImage imageNamed:@"grid_block_9"] forState:UIControlStateNormal];
    [showThumbsButton addTarget:self action:@selector(toggleThumbnails:) forControlEvents:UIControlEventTouchUpInside];
    [showThumbsButton setFrame:CGRectMake(0.0f, 0.0f, 45.0f, 25.0f)];
    [showThumbsButton setContentEdgeInsets:UIEdgeInsetsMake(0.0f, 10.0f, 0.0f, 10.0f)];
    UIBarButtonItem *showThumbs = [[UIBarButtonItem alloc] initWithCustomView:showThumbsButton];
    self.showThumbsButton = showThumbsButton;

    UILongPressGestureRecognizer *longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPressThumbsButton:)];
    [showThumbsButton addGestureRecognizer:longPress];
    
    UIButton *showFilterPopoverButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [showFilterPopoverButton setShowsTouchWhenHighlighted:YES];
    [showFilterPopoverButton setImage:[UIImage imageNamed:@"233-Files"] forState:UIControlStateNormal];
    [showFilterPopoverButton addTarget:self action:@selector(showFilterPopover:) forControlEvents:UIControlEventTouchUpInside];
    [showFilterPopoverButton setFrame:CGRectMake(0.0f, 0.0f, 45.0f, 25.0f)];
    UIBarButtonItem *showFilterPopover = [[UIBarButtonItem alloc] initWithCustomView:showFilterPopoverButton];
    self.showFilterPopoverButton = showFilterPopoverButton;
    
    UIButton *notificationsButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [notificationsButton setShowsTouchWhenHighlighted:YES];
    [notificationsButton setImage:[UIImage imageNamed:@"inbox"] forState:UIControlStateNormal];
    [notificationsButton addTarget:self action:@selector(handleBarButtonPress:) forControlEvents:UIControlEventTouchUpInside];
    [notificationsButton setFrame:CGRectMake(0.0f, 0.0f, 45.0f, 25.0f)];
    UIBarButtonItem *notificationsButtonItem = [[UIBarButtonItem alloc] initWithCustomView:notificationsButton];
    self.notificationsButton = notificationsButton;

    self.navigationItem.rightBarButtonItems = @[notificationsButtonItem, infoBarButtonItem];
    self.navigationItem.leftBarButtonItems = @[showThumbs, showFilterPopover];
    [self.navigationItem setLeftItemsSupplementBackButton:YES];
}

- (void)showFilterPopover:(id)sender
{
    [self hidePDFStrips];

    if (self.currentPopoverController) {
        [self dismissPopovers];
        return;
    }
    
    UIViewController *annotationFilter = [[UIStoryboard storyboardWithName:@"ADXAnnotationFilter" bundle:nil] instantiateInitialViewController];
    UIPopoverController *popover = [[UIPopoverController alloc] initWithContentViewController:annotationFilter];
    popover.popoverBackgroundViewClass = [ADXPopoverBackgroundView class];
    self.currentPopoverController = popover;
    popover.delegate = self;
    
    [popover presentPopoverFromRect:[sender frame] inView:[sender superview] permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
}

// A long press on the thumbnails button brings up the page slider
- (void)longPressThumbsButton:(UILongPressGestureRecognizer*)gesture
{
    if (gesture.state == UIGestureRecognizerStateBegan) {
        [self dismissPopovers];
        
        ADXPageSliderViewController *pageSlider = [[ADXPageSliderViewController alloc] initWithNumPages:self.document.pageCount navigateBlock:^(int pageIndex) {
            self.page = pageIndex;
        }];

        // Load the view now so we can set the slider position
        [pageSlider view];
        pageSlider.currentPage = self.page;

        if (self.document.pageCount == 1) {
            pageSlider.contentSizeForViewInPopover = CGSizeMake(730, 60);
        } else {
            pageSlider.contentSizeForViewInPopover = CGSizeMake(730, 100);
        }
        
        UIPopoverController *popover = [[UIPopoverController alloc] initWithContentViewController:pageSlider];
        [popover presentPopoverFromRect:self.showThumbsButton.bounds inView:self.showThumbsButton permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
        popover.delegate = self;
        self.currentPopoverController = popover;
    }
}

#pragma mark - ADXToolsDelegate

- (void)addToolsDelegate:(id<ADXPDFToolsDelegate>)delegate
{
    [self.toolsDelegates addDelegate:delegate];
}

- (void)removeToolsDelegate:(id<ADXPDFToolsDelegate>)delegate
{
    [self.toolsDelegates removeDelegate:delegate];
}

- (void)handleBarButtonPress:(id)sender
{
    UIButton *button = (UIButton *)sender;
    UIBarButtonItem *barButtonItem = [self adx_barButtonItemWithCustomView:button];
    
    __weak ADXPDFViewController *weakSelf = self;
    
    if (button == self.infoButton) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [weakSelf toggleVersionsSelector:barButtonItem];
        });
    } else if (button == self.notificationsButton) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [weakSelf didTapNotificationsButton:barButtonItem];
        });
    }
}

#pragma mark - Accessors

- (void)setAdxDocument:(ADXDocument *)adxDocument
{
    // If _adxDocument is nil, then this instance likely didn't come through the initWithDocument initializer,
    // and we should initialize the _pendingClosePageIndex value too.
    if (!_adxDocument) {
        _pendingClosePageIndex = -1;
    } else {
        [self emitAnyPendingPageCloseEvents];
        [self.service emitEventDidCloseDocument:self.adxDocument.id versionID:self.adxDocument.version completion:nil];
    }
    
    _adxDocument = adxDocument;
    
    [self.service emitEventDidOpenDocument:self.adxDocument.id versionID:self.adxDocument.version completion:nil];
    
    [self hidePDFStrips];
    
    [self updateUserDefaultsForCurrentlyOpenDocument];
}

- (void)setViewMode:(PSPDFViewMode)viewMode animated:(BOOL)animated
{
    [self hidePDFStrips];
    [[PSPDFCache sharedCache] clearCache];
    [super setViewMode:viewMode animated:animated];
}

//FIXME check all functionality
- (BOOL)setHUDVisible:(BOOL)show animated:(BOOL)animated
{
    if ( [self isControllerLocked] )
        return NO;
    
    if (![super setHUDVisible:show animated:animated])
        return NO;
    
    if (!show) {
        [self dismissPopovers];
    }
    
    __weak ADXPDFViewController *weakSelf = self;
    void (^setBarsVisible)(BOOL) = ^(BOOL show) {
        CGFloat alpha = show ? 1.0f : 0.0f;
        weakSelf.navigationController.navigationBar.alpha = alpha;
        weakSelf.toolbarContainer.alpha = alpha;
        weakSelf.toolsPane.alpha = alpha;
    };
    
    if (animated)
    {
        [UIView animateWithDuration:0.25f delay:0.f options:UIViewAnimationOptionAllowUserInteraction animations:^{
            setBarsVisible(show);
        } completion:NULL];
    }
    else
    {
        setBarsVisible(show);
    }

    return YES;
}

// PSPDFViewController override
- (BOOL)toggleControls
{
    if ([self isHUDVisible]) {
        self.HUDViewMode = PSPDFHUDViewAutomatic;
        [self hideControls];
        return NO;
    } else {
        [self showControls];
        self.HUDViewMode = PSPDFHUDViewAlways;
        return YES;
    }
}

- (void)setDocument:(PSPDFDocument *)document
{
    if ( self.viewMode == PSPDFViewModeThumbnails ) {
        [[PSPDFCache sharedCache] clearCache];
    }
    
    [super setDocument:document];
    
    [ADXAnnotationUtil addAnnotationsToPDF:document fromDocument:self.adxDocument];
    [self.toolsDelegates enumerateDelegateUsingBlock:^(id<ADXPDFToolsDelegate> delegate, BOOL *stop) {
        [delegate documentDidChanged:document];
    }];
    [self updateTitle];
}

- (void)setAnnotationsVisible:(BOOL)visible
{
    if ( _annotationsVisible != visible )
    {
        _annotationsVisible = visible;
        self.document.annotationsEnabled = visible;
        for (NSNumber *page in self.visiblePageNumbers)
        {
            PSPDFPageView* pageView = [self pageViewForPage:page.integerValue];
            [pageView updateView];
            if ( visible ){
                for( UIView* annotation in [pageView visibleAnnotationViews]){
                    annotation.alpha = 1;
                }
            } else {
                for( UIView* annotation in [pageView visibleAnnotationViews]){
                    annotation.alpha = 0;
                }
            }
        }
    }
}

- (BOOL)setPage:(NSUInteger)page animated:(BOOL)animated
{
    BOOL done = NO;
    
    if (page >= self.document.pageCount) {
        page = 0;
    }
    
    if (super.viewMode == PSPDFViewModeThumbnails) {
        done = [super setPage:page animated:NO];
        [super setViewMode:PSPDFViewModeDocument animated:animated];
    } else {
        done = [super setPage:page animated:animated];
    }
    
    return done;
}

#pragma mark - Notifications


- (void)watchNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleServiceUnauthorizedNotification:)
                                                 name:kADXServiceURLRequestUnauthorizedNotification
                                               object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleKeyboardWillShowNotification:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleKeyboardDidHideNotification:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleURLSchemeRequestNotification:)
                                                 name:kADXURLSchemeRequestNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleServiceDocumentDownloadedNotification:)
                                                 name:kADXServiceDocumentDownloadedNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleServiceNotificationReachabilityChanged:)
                                                 name:kADXServiceNotificationReachabilityChanged
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleServiceNotificationLatestDocumentVersionChanged:)
                                                 name:kADXServiceNotificationLatestDocumentVersionChanged
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleServiceNotificationDocumentInaccessible:)
                                                 name:kADXServiceNotificationDocumentInaccessible
                                               object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleAnnotationFilterChangeNotification:)
                                                 name:kADXAnnotationFilterChangedNotification
                                               object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleDocumentNotesDownloadedNotification:)
                                                 name:kADXServiceDocumentNotesDownloadedNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleConflictingNoteChangesDiscardedNotification:)
                                                 name:kADXServiceConflictingNoteChangesDiscardedNotification
                                               object:nil];
}

- (void)unwatchNotifications
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kADXServiceURLRequestUnauthorizedNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kADXURLSchemeRequestNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kADXServiceDocumentDownloadedNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kADXServiceNotificationReachabilityChanged object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kADXServiceNotificationLatestDocumentVersionChanged object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kADXServiceNotificationDocumentInaccessible object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kADXServiceConflictingNoteChangesDiscardedNotification object:nil];

    // This notification is added in viewWillAppear
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kADXPDFViewControllerDidShowPage object:nil];
}

- (void)handleServiceUnauthorizedNotification:(NSNotification *)notification
{
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        [[ADXAppDelegate sharedInstance] presentUserReauthenticationPopupIfNecessary];
    }];
}

- (void)handleKeyboardWillShowNotification:(NSNotification*)notificaion
{    
    self.keyboardlVisible = YES;
    if ( ![super isControllerLocked] ){
        [self moveUpToolbarAnimated:YES completion:NULL];
    }
}

- (void)handleKeyboardDidHideNotification:(NSNotification*)notificaion
{
    [self unlockController];
    self.keyboardlVisible = NO;
    [self moveDownToolBarAnimated:YES completion:NULL];
}

- (void)handleServiceNotificationDocumentInaccessible:(NSNotification *)notification
{
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        NSString *documentID = [notification.userInfo valueForKey:kADXServiceNotificationPayload];
        if ([documentID isEqualToString:self.adxDocument.id]) {
            [self documentVersionWasRevoked];
        }
    }];
}

- (void)handleServiceNotificationLatestDocumentVersionChanged:(NSNotification *)notification
{
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        NSDictionary *userInfo = [notification.userInfo valueForKey:kADXServiceNotificationPayload];
        NSString *documentID = userInfo[ADXDocumentAttributes.id];
        if ([documentID isEqualToString:self.adxDocument.id]) {
            [self documentVersionDidChange];
        }
    }];
}

- (void)handleServiceNotificationReachabilityChanged:(NSNotification *)notification
{
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        [self updateDocumentVersionStatusIndicator];
    }];
}

- (void)handleServiceDocumentDownloadedNotification:(NSNotification *)notification
{
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        // Check to see if this is a document we're waiting for (but only if we're currently visible).
        NSString *documentID = [notification.userInfo valueForKey:kADXServiceNotificationPayload];
        if ([documentID isEqualToString:[ADXAppDelegate sharedInstance].pendingURLSchemeRequest.requiredDocumentID]) {
            if (![documentID isEqualToString:self.adxDocument.id]) {
                [self.navigationController popViewControllerAnimated:YES];
            }
        }
    }];
}

- (void)handleURLSchemeRequestNotification:(NSNotification *)notification
{
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        // App has just received a URL launch request.  Pop to the collection view to handle it.
        [self.navigationController popViewControllerAnimated:YES];
    }];
}

- (void)handleDocumentNotesDownloadedNotification:(NSNotification *)notification
{
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        NSString *documentID = [notification.userInfo valueForKey:kADXServiceNotificationPayload];
        if (![documentID isEqualToString:self.adxDocument.id]) {
            return;
        }
        
        [self refreshAnnotations];
    }];
}

- (void)handleConflictingNoteChangesDiscardedNotification:(NSNotification *)notification
{
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        [self refreshAnnotations];
    }];
}

- (void)refreshAnnotations
{
    // Add the current filtered set of annotations
    [ADXAnnotationUtil addAnnotationsToPDF:self.document fromDocument:self.adxDocument];
    
    [self reloadData];
}

- (void)handleAnnotationFilterChangeNotification:(NSNotification *)notification
{
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{

        [self refreshAnnotations];
    }];
}

#pragma mark - Activity Stream

- (void)didTapNotificationsButton:(id)sender
{
    [[ADXAppDelegate sharedInstance] passCheckpoint:@"Tapped the notifications drawer (while viewing a document)"];
    
    if ([self.currentPopoverController isPopoverVisible]) {
        [self dismissPopovers];
    } else {
        ADXAppDelegate *appDelegate = (ADXAppDelegate *)[[UIApplication sharedApplication] delegate];
        NSManagedObjectContext *moc = [appDelegate managedObjectContext];
        NSString *accountID = [ADXAppDelegate sharedInstance].currentAccount.id;
        
        NSDictionary *popoverOptions = @{kADXNotificationsOptionKeyFilterType: kADXNotificationsOptionValueFilterTypeDocument,
                                         kADXNotificationsOptionKeyFilterObjectID: self.adxDocument.id};
        self.currentPopoverController = [ADXNotificationsViewController popoverControllerWithAccount:accountID actionDelegate:self options:popoverOptions managedObjectContext:moc];
        self.currentPopoverController.popoverBackgroundViewClass = [ADXPopoverBackgroundView class];
        self.currentPopoverController.delegate = self;
        
        [self.currentPopoverController presentPopoverFromBarButtonItem:sender permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
    }
}

#pragma mark - Versions

- (void)presentNewerVersionAlert
{
    if (self.versionAlertBubble) {
        [[ADXNotificationBubbleManager sharedInstance] removeBubble:self.versionAlertBubble];
    }
    ADXNotificationBubble *bubble = [[ADXNotificationBubbleManager sharedInstance] displayStickyBubbleWithMessage:@"A newer version of this document is available." actionLabel:@"Open" userInfo:nil delegate:self];
    self.versionAlertBubble = bubble;
}

- (void)presentRevokedDocumentAlert
{
    __weak ADXPDFViewController *weakSelf = self;
    PSPDFAlertView *alertView = [[PSPDFAlertView alloc] initWithTitle:nil message:@"Access to this document has been revoked."];
    [alertView setCancelButtonWithTitle:@"Close Document" block:^{
        [weakSelf.navigationController popViewControllerAnimated:YES];
    }];
    
    [alertView show];
}

- (void)documentVersionWasRevoked
{
    [self presentRevokedDocumentAlert];
}

- (void)documentVersionDidChange
{
    [self dismissPopovers];
    [self updateDocumentVersionStatusIndicator];

    // Suppress the new version alert when the app is in the middle of responding to a URL scheme request.
    // While the app is responding to this request, it is quite likely waiting for a new version of a document
    // to be downloaded from the server so it can be displayed; the newer version alert interferes with this.
    if (![ADXAppDelegate sharedInstance].pendingURLSchemeRequest) {
        [self presentNewerVersionAlert];
    }
}

- (void)updateDocumentVersionStatusIndicator
{
    UIBarButtonItem *indicatorButtonItem = nil;
    for (UIBarButtonItem *barButtonItem in self.navigationItem.rightBarButtonItems) {
        if (barButtonItem.tag == kADXPDFViewControllerDocumentVersionsButtonTag) {
            indicatorButtonItem = barButtonItem;
            break;
        }
    }
    
    if (!indicatorButtonItem)
        return;
    
    UIButton *button = (UIButton *)indicatorButtonItem.customView;
    BOOL online = [self.service networkReachable] && [self.service serverReachable];
    if (self.adxDocument.versionIsLatestValue) {
        if (online) {
            [button setImage:[UIImage imageNamed:@"document_latest.png"] forState:UIControlStateNormal];
        } else {
            [button setImage:[UIImage imageNamed:@"offline_document_latest.png"] forState:UIControlStateNormal];
        }
    } else {
        if (online) {
            [button setImage:[UIImage imageNamed:@"document_old.png"] forState:UIControlStateNormal];
        } else {
            [button setImage:[UIImage imageNamed:@"offline_document_old.png"] forState:UIControlStateNormal];
        }
    }
}

- (void)popoverActionForDocument:(ADXDocument *)document thumbnail:(UIImage *)thumbnail fromRectOfView:(UIView *)view
{
    
}

- (void)internalToggleVersionsSelectorForiPad:(id)sender
{
    __weak ADXPDFViewController *weakSelf = self;
    ADXAppDelegate *appDelegate = (ADXAppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *moc = [appDelegate managedObjectContext];

    dispatch_async(dispatch_get_main_queue(), ^{
        weakSelf.currentPopoverController = [ADXDocVersionViewController popoverControllerWithDocument:weakSelf.adxDocument
                                                                            allowNavigatingCollections:NO
                                                                                      hideDeleteButton:YES
                                                                                  managedObjectContext:moc
                                                                                        actionDelegate:weakSelf];
        weakSelf.currentPopoverController.delegate = self;
        [weakSelf.currentPopoverController presentPopoverFromBarButtonItem:sender
                                                  permittedArrowDirections:UIPopoverArrowDirectionUp
                                                                  animated:YES];
    });
}

- (void)toggleVersionsSelectorForiPad:(id)sender
{
    [[ADXAppDelegate sharedInstance] passCheckpoint:@"Tapped the document version selector"];

    if ([self.currentPopoverController isPopoverVisible]) {
        [self dismissPopovers];
    } else {
        __weak ADXPDFViewController *weakSelf = self;
        
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            [weakSelf.service refreshDocumentVersions:weakSelf.adxDocument.id completion:^(BOOL success, NSError *error) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [hud hide:YES];
                    [weakSelf internalToggleVersionsSelectorForiPad:sender];
                });
            }];
        });
    }
}

- (void)internalToggleVersionsSelectorForiPhone:(id)sender
{
    __weak ADXPDFViewController *weakSelf = self;
    ADXAppDelegate *appDelegate = (ADXAppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *moc = [appDelegate managedObjectContext];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        UIViewController *vc = [ADXDocVersionViewController modalControllerWithDocument:weakSelf.adxDocument
                                                             allowNavigatingCollections:NO
                                                                       hideDeleteButton:YES
                                                                   managedObjectContext:moc
                                                                         actionDelegate:weakSelf];
        [weakSelf presentModalViewController:vc embeddedInNavigationController:NO withCloseButton:YES animated:YES];
    });    
}

- (void)toggleVersionsSelectorForiPhone:(id)sender
{
    __weak ADXPDFViewController *weakSelf = self;
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [weakSelf.service refreshDocumentVersions:weakSelf.adxDocument.id completion:^(BOOL success, NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [hud hide:YES];
                [weakSelf internalToggleVersionsSelectorForiPhone:sender];
            });
        }];
    });
}

- (void)toggleVersionsSelector:(id)sender
{
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        [self toggleVersionsSelectorForiPhone:sender];
    } else {
        [self toggleVersionsSelectorForiPad:sender];
    }
}

- (BOOL)isPDFStripsVisible
{
    return (self.PDFStripsViewController ? YES : NO);
}

- (void)togglePDFStripsWithDataSource:(id<ADXPDFStripsViewControllerDataSource>)dataSource;
{
    if ([self isPDFStripsVisible]) {
        [self hidePDFStrips];
    } else {
        // Show it
        self.PDFStripsViewController = [[ADXPDFStripsViewController alloc] initWithNibName:@"ADXPDFStripsViewController" bundle:nil];
        self.PDFStripsViewController.document = self.adxDocument;
        self.PDFStripsViewController.pspdfDocument = self.document;
        self.PDFStripsViewController.PDFStripsDelegate = self;
        self.PDFStripsViewController.PDFStripsDataSource = dataSource;
        
        [self.view addSubview:self.PDFStripsViewController.view];
        self.PDFStripsViewController.view.frame = CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height);
        [self.PDFStripsViewController willMoveToParentViewController:self];
        [self addChildViewController:self.PDFStripsViewController];
        [self.PDFStripsViewController didMoveToParentViewController:self];
        
        // Offset the content from the top and bottom so it doesn't appear underneath the toolbar or navigation bars
        self.PDFStripsViewController.contentInset = UIEdgeInsetsMake(self.navigationController.navigationBar.frame.size.height * 1.5, 0, self.toolbarContainer.frame.size.height * 1.5, 0);
        
        [self.view bringSubviewToFront:self.toolbarContainer];
        
        self.PDFStripsViewController.view.alpha = 0.0;
        [UIView animateWithDuration:0.25 animations:^{
            self.PDFStripsViewController.view.alpha = 1.0;
        }];
    }
}

- (void)hidePDFStrips
{
    // Fade out
    self.PDFStripsViewController.view.userInteractionEnabled = NO;
    [UIView animateWithDuration:0.25 animations:^{
        self.PDFStripsViewController.view.alpha = 0.0;
    } completion:^(BOOL finished) {
        // If there's a change strip that needs to be positioned as a result of a transition from the changes view..
        if (self.stripToPosition != nil) {
            [self animatePDFStrip];
        }
        
        // Remove it
        [self.PDFStripsViewController willMoveToParentViewController:nil];
        [self.PDFStripsViewController.view removeFromSuperview];
        [self.PDFStripsViewController removeFromParentViewController];
        [self.PDFStripsViewController didMoveToParentViewController:nil];
        self.PDFStripsViewController = nil;
    }];
}

- (void)refreshPDFStrips
{
    [self.PDFStripsViewController refresh];
}

/*- (void)updatePDFStripsWithHighlightedContent:(ADXHighlightedContentHelper *)changes deltaVersion:(int)version
{
    if (changes.hasChanges) {
        if (self.changeStripsViewController) {
            self.changeStripsViewController.highlightedContent = changes;
            self.changeStripsViewController.highlightedContentVersion = version;
        }
    } else {
        [self hidePDFStrips];
    }
}
*/

- (void)refreshChangeStrips
{
    [self.PDFStripsViewController refresh];
}

#pragma mark - ADXCollectionDocumentsActionsDelegate

- (void)didRemoveDocument:(ADXDocument *)document fromCollection:(ADXCollection *)collection
{
    [[ADXAppDelegate sharedInstance] removeDocument:document.id fromCollection:collection.id localID:collection.localID completion:nil];
}

- (void)didAddDocument:(ADXDocument *)document toCollection:(ADXCollection *)collection
{
    [[ADXAppDelegate sharedInstance] addDocument:document.id toCollection:collection.id localID:collection.localID completion:nil];
}

#pragma mark - ADXDocActionsDelegate

- (void)didSelectDocShareAction:(id)sender document:(ADXDocument *)document
{
    [self dismissPopovers];
    self.documentSharingController = [ADXDocSharingController controllerWithDocument:document mailDelegate:self];
    [self.documentSharingController presentShareDocumentViewFromViewController:self];
}

- (void)openLatestVersion
{
    __weak ADXPDFViewController *weakSelf = self;
    ADXAppDelegate *appDelegate = (ADXAppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *moc = [appDelegate managedObjectContext];
    
    [moc performBlock:^{
        NSString *accountID = [ADXAppDelegate sharedInstance].currentAccount.id;
        ADXDocument *document = [ADXDocument latestDocumentWithID:weakSelf.adxDocument.id accountID:accountID context:moc];
        dispatch_async(dispatch_get_main_queue(), ^{
            [weakSelf openDocument:document atCurrentPage:NO];
        });
    }];
}

- (void)openDocument:(ADXDocument *)document atCurrentPage:(BOOL)atCurrentPage
{
    __weak ADXPDFViewController *weakSelf = self;
    
    NSUInteger pageIndex = 0;
    if (atCurrentPage) {
        pageIndex = self.page;
    }
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [self.service refreshNotesForDocument:document.id completion:^(BOOL success, NSError *error) {
        if (!success) {
            LogNSError(@"refreshNotesForDocument failed", error);
        }
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            [[ADXAppDelegate sharedInstance] pdfResourceForDocument:document
                                                    downloadContent:YES
                                                            service:weakSelf.service
                                                  downloadWillStart:^{
                                                  } completion:^(id resource, BOOL success, NSError *error) {
                                                      dispatch_async(dispatch_get_main_queue(), ^{
                                                          [hud hide:YES];
                                                          
                                                          if (success && resource) {
                                                              [weakSelf openPDFResource:resource onPage:pageIndex fromDocument:document];
                                                              [weakSelf updateDocumentVersionStatusIndicator];
                                                          } else {
                                                              // FIXME: Display an error
                                                          }
                                                      });
                                                  }];
        }];
    }];
}

- (void)openPDFResource:(PSPDFDocument *)pdfResource onPage:(NSInteger)page fromDocument:(ADXDocument *)document
{
    self.adxDocument = document;
    self.document = pdfResource;
    [self setPage:page animated:NO];
    [self refreshAnnotations];
}

- (void)didSelectDocOpenAction:(id)sender document:(ADXDocument *)document
{
    [[ADXAppDelegate sharedInstance] passCheckpoint:@"Opened a different version of the document"];

    [self dismissPopovers];
    
    if (!document)
        return;
    
    if (!self.service)
        return;
    
    [self openDocument:document atCurrentPage:YES];
}

- (void)didSelectDocAnalyticsAction:(id)sender document:(ADXDocument *)document
{
    [[ADXAppDelegate sharedInstance] passCheckpoint:@"Opened document analytics (while viewing a document)"];

    [self dismissPopovers];
    
    if (!document)
        return;

    [ADXDocumentAnalyticsViewController presentAnalyticsForDocument:document parentViewController:self];
}

- (void)didSelectDocRemoveAction:(id)sender document:(ADXDocument *)document
{
    __weak ADXPDFViewController *weakSelf = self;

    [[ADXAppDelegate sharedInstance] passCheckpoint:@"Opened document remove popover"];

    [self dismissPopovers];

    PSPDFActionSheet *actionSheet = [[PSPDFActionSheet alloc] initWithTitle:@"Remove Document"];
    if (!document.metaDocument.doNotAutomaticallyDownloadContentValue) {
        [actionSheet setDestructiveButtonWithTitle:@"From Local Storage" block:^{
            [weakSelf.service excludeDocumentFromAutomaticContentDownload:document.id removeContentNow:YES completion:nil];
        }];
    }
    [actionSheet setCancelButtonWithTitle:@"Cancel" block:nil];    
    [actionSheet showFromBarButtonItem:self.infoBarButtonItem animated:YES];
}

#pragma mark - Document Level Events

- (void)willCloseDocument
{
    if (self.actionDelegate) {
        [self.actionDelegate pdfViewController:self willFinishWithDocument:self.adxDocument];
    }
    self.adxDocument.mostRecentOpenedDate = [NSDate date];
    [self didCloseDocument];
}

- (void)didCloseDocument
{
    ADXAppDelegate *appDelegate = (ADXAppDelegate *)[[UIApplication sharedApplication] delegate];
    [appDelegate setReaderAppState:ADXAppState.viewingDocumentEnded];
    [[ADXUserDefaults sharedDefaults] setCurrentlyOpenDocumentID:nil];

    [self emitAnyPendingPageCloseEvents];
    [self.service emitEventDidCloseDocument:self.adxDocument.id versionID:self.adxDocument.version completion:nil];
}

#pragma mark - Page Level Events

- (void)emitAnyPendingPageCloseEvents
{
    if (self.pendingClosePageIndex != -1) {
        [self.service emitEventDidClosePageOfDocument:self.adxDocument.id versionID:self.adxDocument.version page:(self.pendingClosePageIndex + 1) completion:nil];
        self.pendingClosePageIndex = -1;
    }
}

- (void)handlePageIdleTimerFiring:(NSTimer *)sender
{
    NSUInteger pageIndex = [(NSNumber *)[sender.userInfo objectForKey:kADXPDFViewControllerDidShowPageUserInfoPageKey] unsignedIntegerValue];

    __weak ADXPDFViewController *weakSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        [weakSelf emitAnyPendingPageCloseEvents];
        weakSelf.pendingClosePageIndex = pageIndex;

        NSUInteger pageNumber = (pageIndex + 1);
        [weakSelf.service emitEventDidOpenPageOfDocument:weakSelf.adxDocument.id versionID:weakSelf.adxDocument.version page:pageNumber completion:nil];
    });
}

- (void)resetPageIdleTimerWithUserInfo:(id)userInfo
{
    if (self.pageIdleTimer) {
        [self.pageIdleTimer invalidate];
    }
    
    self.pageIdleTimer = [NSTimer scheduledTimerWithTimeInterval:3 target:self selector:@selector(handlePageIdleTimerFiring:) userInfo:userInfo repeats:NO];
}

- (void)handleShowPageNotification:(NSNotification *)notification
{
    [self resetPageIdleTimerWithUserInfo:notification.userInfo];
}

- (void)didShowPageIndex:(NSUInteger)pageIndex
{
    [self resetPageIdleTimerWithUserInfo:[NSDictionary dictionaryWithObjectsAndKeys:
                                          [NSNumber numberWithUnsignedInteger:pageIndex], kADXPDFViewControllerDidShowPageUserInfoPageKey,
                                          nil]];
}

#pragma mark - UIPopoverControllerDelegate

- (BOOL)popoverControllerShouldDismissPopover:(UIPopoverController *)popoverController
{
    return YES;
}

- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController
{
    self.currentPopoverController = nil;
}

#pragma mark - Slide-Aside

// FIXME - Remove this if we aren't doing slide-aside view
- (UIViewController *)parentSliderViewController
{
    UIViewController *result = nil;
    UIViewController *parentVC = self.navigationController.parentViewController;
    
    while (parentVC) {
        if ([parentVC respondsToSelector:@selector(revealGesture:)] && [parentVC respondsToSelector:@selector(revealToggle:)]) {
            result = parentVC;
            break;
        } else {
            parentVC = parentVC.parentViewController;
        }
    }
    
    return result;
}

// FIXME - Remove this if we aren't doing slide-aside view
- (UIPanGestureRecognizer *)setupSliderNavigationBarWithGestureRecognizer:(UIPanGestureRecognizer *)gestureRecognizer
{
    UIViewController *sliderViewController = [self parentSliderViewController];
    
	if (sliderViewController)
	{
		// Check if a UIPanGestureRecognizer already sits atop our NavigationBar.
		if (![[self.navigationController.navigationBar gestureRecognizers] containsObject:gestureRecognizer]) {
			// If not, allocate one and add it.
			UIPanGestureRecognizer *panGestureRecognizer = [[UIPanGestureRecognizer alloc]
                                                            initWithTarget:sliderViewController
                                                            action:@selector(revealGesture:)];
			[self.navigationController.navigationBar addGestureRecognizer:panGestureRecognizer];
		}
		
		// Check if we have a revealButton already.
		if (![self.navigationItem leftBarButtonItem]) {
			// If not, allocate one and add it.
            UIBarButtonItem *revealButton = [[UIBarButtonItem alloc]
                                             initWithImage:[UIImage imageNamed:@"list-slideaside.png"]
                                             style:UIBarButtonItemStylePlain
                                             target:sliderViewController
                                             action:@selector(revealToggle:)];
			self.navigationItem.leftBarButtonItem = revealButton;
		}
	}
    
    return gestureRecognizer;
}

#pragma mark - Private

- (void)dismissPopovers
{
    if ([self.currentPopoverController isPopoverVisible]) {
        [self.currentPopoverController dismissPopoverAnimated:YES];
        self.currentPopoverController = nil;
    }
}

- (void)updateTitle
{
    // Value based on the space we currently have in the navigation bar, in portrait orientation
    float maxWidth = 360.0;

    UIView *titleView = self.navigationItem.titleView;
    if (titleView == nil) {
        titleView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, maxWidth, 40)];
    }

    UILabel *titleLabel = (UILabel *)[titleView viewWithTag:1];
    if (titleLabel == nil) {
        titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, maxWidth, 17)];
        titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:17.0];
        titleLabel.backgroundColor = [UIColor clearColor];
        titleLabel.textColor = [UIColor whiteColor];
        titleLabel.layer.shadowColor = [UIColor blackColor].CGColor;
        titleLabel.layer.shadowOffset = CGSizeMake(0, 1.0);
        titleLabel.layer.shadowOpacity = 0.5;
        titleLabel.layer.shadowRadius = 1.0;
        titleLabel.lineBreakMode = NSLineBreakByTruncatingMiddle;
        titleLabel.minimumScaleFactor = 0.75;
        titleLabel.adjustsFontSizeToFitWidth = YES;
        titleLabel.adjustsLetterSpacingToFitWidth = YES;
        titleLabel.tag = 1;
        [titleView addSubview:titleLabel];
        titleLabel.textAlignment = NSTextAlignmentCenter;

        [self.navigationItem setTitleView:titleView];
    }

    [titleLabel setText:self.adxDocument.title];

    UILabel *detailLabel = (UILabel *)[titleView viewWithTag:2];
    if (detailLabel == nil) {
        detailLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, maxWidth, 30)];
        detailLabel.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:13.0];
        detailLabel.backgroundColor = [UIColor clearColor];
        detailLabel.textColor = [UIColor whiteColor];
        detailLabel.shadowColor = [UIColor colorWithWhite:0.5 alpha:1.0];
        detailLabel.shadowOffset = CGSizeMake(0, -1);
        detailLabel.lineBreakMode = NSLineBreakByTruncatingMiddle;
        detailLabel.tag = 2;
        detailLabel.textAlignment = NSTextAlignmentCenter;
        [titleView addSubview:detailLabel];
    }
    
    detailLabel.text = [NSString stringWithFormat:@"%@ - Page %d of %d", self.adxDocument.versionDisplayLabel, self.page+1, self.document.pageCount];

    titleLabel.center = CGPointMake(titleView.frame.size.width/2, 10);
    detailLabel.center = CGPointMake(titleView.frame.size.width/2, (titleLabel.frame.size.height + (detailLabel.frame.size.height/2)) - 4);
    
    titleLabel.frame = CGRectIntegral(titleLabel.frame);
    detailLabel.frame = CGRectIntegral(detailLabel.frame);
}

- (void)toggleThumbnails:(id)sender
{
    if (self.viewMode == PSPDFViewModeDocument) {
        [self showThumbnails:sender];
    } else {
        [self hideThumbnails:sender];
    }
}

- (void)showThumbnails:(id)sender
{
    [[ADXAppDelegate sharedInstance] passCheckpoint:@"Viewed document pages as thumbnails"];

    [self setViewMode:PSPDFViewModeThumbnails animated:YES];
    // hide the keyboard in case is visible.
    [self.view endEditing:TRUE];
    [self closeToolsPane];
}

- (void)hideThumbnails:(id)sender
{
    [self setViewMode:PSPDFViewModeDocument animated:YES];
    [self closeToolsPane];
}

- (void)updateUserDefaultsForCurrentlyOpenDocument
{
    [[ADXUserDefaults sharedDefaults] setCurrentlyOpenDocumentID:self.adxDocument.id version:[self.adxDocument.version integerValue]];
}

#pragma mark - ToolsPane

- (void)openToolsPaneFromKeyboard:(BOOL)fromkeyboard completion:(void (^)(void))completion
{
    [self openToolsPaneFromKeyboard:fromkeyboard animated:YES completion:completion];
}

- (void)openToolsPaneFromKeyboard:(BOOL)fromkeyboard animated:(BOOL)animated completion:(void (^)(void))completion
{
    float panelHeight = self.toolsPane.frame.size.height;
    float originY = self.view.frame.size.height - panelHeight - kBarHeight;
    
    __weak ADXPDFViewController *weakSelf = self;
    
    void(^openToolsPane)() = ^{
        CGRect f = weakSelf.toolsPane.frame;
        f.origin.y = originY;
        weakSelf.toolsPane.frame = f;
    };
    
    if ( animated )
    {
        [UIView animateWithDuration:0.25 delay:0 options:(UIViewAnimationOptionCurveLinear) animations:openToolsPane completion: ^(BOOL finished) {
           // since the panel fades on touch the scrollview isn't resized anymore.
           // [weakSelf setSrollviewShrinked:YES];
            if (completion)
                completion();
        }];
    }
    else {
        openToolsPane();
        // since the panel fades on touch the scrollview isn't resized anymore.
        // [weakSelf setSrollviewShrinked:YES];
        if (completion)
            completion();
    }
}

- (void)closeToolsPaneAnimated:(BOOL)animated completion:(void (^)(void))completion
{
    __weak ADXPDFViewController *weakSelf = self;
    void(^closeToolsPane)() = ^{
        [UIView animateWithDuration:0.25 delay:0 options:(UIViewAnimationOptionCurveLinear) animations:^{
                       
            CGRect f = weakSelf.toolsPane.frame;
            f.origin.y = weakSelf.view.frame.size.height + (weakSelf.navigationBarHidden ? 0 : kBarHeight);
            weakSelf.toolsPane.frame = f;
        } completion: ^(BOOL finished) {
        }];
    };
    
    // since the panel fades on touch the scrollview isn't resized anymore. 
    //[self setSrollviewShrinked:NO];
    if ( animated )
    {
        [UIView animateWithDuration:0.25 delay:0 options:(UIViewAnimationOptionCurveLinear) animations:closeToolsPane completion: ^(BOOL finished) {
            if (completion)
                completion();
        }];
    }
    else {
        closeToolsPane();
        if (completion)
            completion();
    }
    
}

// FIXME: since the panel fades on touch the scrollview isn't resized anymore. 
//
//- (void)setSrollviewShrinked:(BOOL)shrinked
//{
//    float panelHeigth = self.toolsPane.frame.size.height;
//    float heigth = shrinked ? self.view.frame.size.height - panelHeigth - kBarHeight : self.view.frame.size.height;
//    
//    CGRect f = self.pagingScrollView.frame;
//    f.size.height = heigth;
//    self.pagingScrollView.frame = f;
//}

- (void)closeToolsPane
{
    [self closeToolsPaneAnimated:YES completion:NULL];
}

#pragma mark - ToolBar

- (void)moveUpToolbarAnimated:(BOOL)animated completion:(void (^)(void))completion
{
    UIInterfaceOrientation interfaceOrientation = [[UIApplication sharedApplication] statusBarOrientation];
    BOOL isLandscape = UIDeviceOrientationIsLandscape(interfaceOrientation);
    
    float toolbarHeight = self.toolbarContainer.frame.size.height;
    float keyboardHeight = isLandscape ? 352 : 264;
    float originY = self.view.frame.size.height - keyboardHeight;
    
    __weak ADXPDFViewController *weakSelf = self;
    void(^moveUp)() = ^{
        CGRect f = self.toolbarContainer.frame;
        f.origin.y = originY -  toolbarHeight;
        weakSelf.toolbarContainer.frame = f;
    };
    
    if ( animated )
    {
        [UIView animateWithDuration:0.25 delay:0 options:(UIViewAnimationOptionCurveLinear) animations:moveUp completion: ^(BOOL finished) {
            if (completion)
                completion();
        }];
    }
    else {
        moveUp();
        if (completion)
            completion();
    }
}

- (void)moveDownToolBarAnimated:(BOOL)animated completion:(void (^)(void))completion
{
    __weak ADXPDFViewController *weakSelf = self;
    void(^moveDown)() = ^{
        CGRect f = weakSelf.toolbarContainer.frame;
        f.origin.y = weakSelf.view.frame.size.height - weakSelf.toolbarContainer.frame.size.height;
        weakSelf.toolbarContainer.frame = f;
    };

    if ( animated )
    {
        [UIView animateWithDuration:0.25 delay:0 options:(UIViewAnimationOptionCurveLinear) animations:moveDown completion: ^(BOOL finished) {
            if (completion)
                completion();
        }];
    }
    else {
        moveDown();
        if (completion)
            completion();
    }
}

#pragma mark - Show/Hide bars 

- (void)showHideBars:(UITapGestureRecognizer*)gr
{
    if (self.navigationController.navigationBarHidden) {
        [self.navigationController setNavigationBarHidden:NO animated:YES];
    }
}

#pragma mark - PSPDFViewControllerDelegate

- (BOOL)delegateDidLongPressOnPageView:(PSPDFPageView *)pageView atPoint:(CGPoint)viewPoint
                     gestureRecognizer:(UILongPressGestureRecognizer *)gestureRecognizer
{
    __block BOOL touchProcessed = NO;
    __weak ADXPDFViewController *weakSelf = self;
    [self.toolsDelegates enumerateDelegateWhoRespondsToSelector:@selector(pdfController:didLongPressOnPageView:atPoint:gestureRecognizer:) usingBlock:^(id delegate, BOOL *stop) {
        *stop = touchProcessed = [delegate pdfController:weakSelf
                                  didLongPressOnPageView:pageView
                                                 atPoint:viewPoint
                                       gestureRecognizer:gestureRecognizer];
    }];
    
    return touchProcessed || [super delegateDidLongPressOnPageView:pageView
                                                           atPoint:viewPoint
                                                 gestureRecognizer:gestureRecognizer];
}

- (NSArray *)delegateShouldShowMenuItems:(NSArray *)menuItems atSuggestedTargetRect:(CGRect)rect forSelectedText:(NSString *)selectedText inRect:(CGRect)textRect onPageView:(PSPDFPageView *)pageView
{
    NSMutableArray *newMenuItems = [NSMutableArray array];
    
    for (PSPDFMenuItem *menuItem in menuItems)
    {
        if ([menuItem isMemberOfClass:[UIMenuItem class]])
        {
            [newMenuItems addObject:menuItem];
        }
    }

    [self.toolsDelegates enumerateDelegateWhoRespondsToSelector:@selector(menuItemsForSelectedText:atSuggestedTargetRect:inRect:onPageView:) usingBlock:^(id delegate, BOOL *stop)
    {
        NSArray *toolItems = [delegate menuItemsForSelectedText:selectedText atSuggestedTargetRect:rect inRect:textRect onPageView:pageView];
        if ( toolItems && toolItems.count ) {
            [newMenuItems addObjectsFromArray:toolItems];
        }
    }];
    
    return [super delegateShouldShowMenuItems:newMenuItems atSuggestedTargetRect:rect forSelectedText:selectedText
                                       inRect:textRect onPageView:pageView];
}

- (void)delegateDidSelectAnnotation:(PSPDFAnnotation *)annotation onPageView:(PSPDFPageView *)pageView
{
    if ([annotation isKindOfClass:[ADXNoteAnnotation class]]) {
        if(UI_USER_INTERFACE_IDIOM() != UIUserInterfaceIdiomPhone) {
            // SPT - this lockController call is unbalanced when tapping on a
            // note, and leads to the view being locked when the note popover is
            // dismissed.  I can't find any ill consequence to not locking the
            // view controller, so this is commented out for now.
//            [self lockController];
        }
        [self closeToolsPane];
    }
    
    [super delegateDidSelectAnnotation:annotation onPageView:pageView];
}

- (BOOL)delegateDidTapOnAnnotation:(PSPDFAnnotation *)annotation annotationPoint:(CGPoint)annotationPoint annotationView:(UIView<PSPDFAnnotationView> *)annotationView pageView:(PSPDFPageView *)pageView viewPoint:(CGPoint)viewPoint
{
    BOOL handled = NO;
    
    [[ADXAppDelegate sharedInstance] passCheckpoint:@"Tapped on an annotation"];

    if ([annotation isKindOfClass:[ADXHighlightAnnotation class]]) {
        ADXHighlightAnnotation *highlightAnnotation = (ADXHighlightAnnotation *)annotation;
        ADXNoteAnnotation *noteAnnotation = highlightAnnotation.noteAnnotation;
        
        if (noteAnnotation) {
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                [self closeToolsPane];
                [pageView showNoteControllerForAnnotation:noteAnnotation showKeyboard:NO animated:YES];
            }];
            handled = YES;
        }
    }
    
    return handled || [super delegateDidTapOnAnnotation:annotation annotationPoint:annotationPoint annotationView:annotationView pageView:pageView viewPoint:viewPoint];
}

- (BOOL)delegateDidTapOnPageView:(PSPDFPageView *)pageView atPoint:(CGPoint)viewPoint
{
    __block BOOL handled = NO;
    
    __weak ADXPDFViewController *weakSelf = self;
    [self.toolsDelegates enumerateDelegateWhoRespondsToSelector:@selector(pdfController:didTapOnPageView:atPoint:) usingBlock:^(id delegate, BOOL *stop) {
           *stop = handled = [delegate pdfController:weakSelf didTapOnPageView:pageView atPoint:viewPoint];
    }];
    
    return handled || [super delegateDidTapOnPageView:pageView atPoint:viewPoint];
}

- (BOOL)delegateShouldScrollToPage:(NSUInteger)page
{
    __block BOOL should = YES;
    
    __weak ADXPDFViewController *weakSelf = self;
    [self.toolsDelegates enumerateDelegateWhoRespondsToSelector:@selector(pdfController:shouldScrollToPage:) usingBlock:^(id delegate, BOOL *stop) {
        should = [delegate pdfController:weakSelf shouldScrollToPage:page];
        *stop = !should;
    }];
    
    return should && [super delegateShouldScrollToPage:page];
}

- (void)delegateDidLoadPageView:(PSPDFPageView *)pageView
{
    UIActivityIndicatorView *indicator = (UIActivityIndicatorView *)[pageView viewWithTag:PSPDFLoadingViewTag];
    if (!indicator) {
        indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        [indicator sizeToFit];
        [indicator startAnimating];
        indicator.tag = PSPDFLoadingViewTag;
        indicator.frame = CGRectMake(floorf((pageView.frame.size.width - indicator.frame.size.width)/2), floorf((pageView.frame.size.height - indicator.frame.size.height)/2), indicator.frame.size.width, indicator.frame.size.height);
        [pageView addSubview:indicator];
    }
    
    __weak ADXPDFViewController *weakSelf = self;
    [self.toolsDelegates enumerateDelegateWhoRespondsToSelector:@selector(pdfController:didLoadPageView:) usingBlock:^(id delegate, BOOL *stop) {
        [delegate pdfController:weakSelf didLoadPageView:pageView];
    }];
    
    [super delegateDidLoadPageView:pageView];
}

- (BOOL)delegateShouldSelectAnnotation:(PSPDFAnnotation *)annotation onPageView:(PSPDFPageView *)pageView
{
    __block BOOL should = YES;
    
    __weak ADXPDFViewController *weakSelf = self;
    [self.toolsDelegates enumerateDelegateWhoRespondsToSelector:@selector(pdfController:shouldSelectAnnotation:onPageView:) usingBlock:^(id delegate, BOOL *stop) {
        should = [delegate pdfController:weakSelf shouldSelectAnnotation:annotation onPageView:pageView];
        *stop = !should;
    }];
    
    return should && [super delegateShouldSelectAnnotation:annotation onPageView:pageView];
}

- (void)delegateDidRenderPageView:(PSPDFPageView *)pageView
{
    // remove loading indicator
    UIActivityIndicatorView *indicator = (UIActivityIndicatorView *)[pageView viewWithTag:PSPDFLoadingViewTag];
    if (indicator) {
        [UIView animateWithDuration:0.25f delay:0.f options:UIViewAnimationOptionAllowUserInteraction animations:^{
            indicator.alpha = 0.f;
        } completion:^(BOOL finished) {
            [indicator removeFromSuperview];
        }];
    }
    
    [super delegateDidRenderPageView:pageView];
}

- (void)delegateDidEndZooming:(UIScrollView *)scrollView withView:(UIView *)view atScale:(CGFloat)scale
{
    __weak ADXPDFViewController *weakSelf = self;
    [self.toolsDelegates enumerateDelegateWhoRespondsToSelector:@selector(pdfController:didEndPageZooming:atScale:)
                                                     usingBlock:^(id delegate, BOOL *stop) {
        [delegate pdfController:weakSelf didEndPageZooming:scrollView atScale:scale];
    }];
    [super delegateDidEndZooming:scrollView withView:view atScale:scale];
}

- (void)delegateDidEndPageDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset
{
    __weak ADXPDFViewController *weakSelf = self;
    [self.toolsDelegates enumerateDelegateWhoRespondsToSelector:@selector(pdfController:didEndPageDragging:willDecelerate:withVelocity:targetContentOffset:) usingBlock:^(id delegate, BOOL *stop) {
        [delegate pdfController:weakSelf didEndPageDragging:scrollView willDecelerate:decelerate withVelocity:velocity targetContentOffset:targetContentOffset];
    }];
    
    [super delegateDidEndPageDragging:scrollView willDecelerate:decelerate withVelocity:velocity targetContentOffset:targetContentOffset];
}

- (void)delegateDidShowPage:(NSUInteger)realPage
{
    [[ADXUserDefaults sharedDefaults] setCurrentlyOpenDocumentPage:realPage+1];
    
    __weak ADXPDFViewController *weakSelf = self;
    [self.toolsDelegates enumerateDelegateWhoRespondsToSelector:@selector(pdfController:didShowPageView:) usingBlock:^(id delegate, BOOL *stop) {
        [delegate pdfController:weakSelf didShowPageView:[self pageViewForPage:realPage]];
    }];

    [[NSNotificationCenter defaultCenter] postNotificationName:kADXPDFViewControllerDidShowPage
                                                        object:self
                                                      userInfo:[NSDictionary dictionaryWithObjectsAndKeys:
                                                                [NSNumber numberWithUnsignedInteger:realPage], kADXPDFViewControllerDidShowPageUserInfoPageKey,
                                                                nil]];

    [super delegateDidShowPage:realPage];

    [self updateTitle];
}

#pragma mark - MFMailComposeViewControllerDelegate

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    [controller dismissViewControllerAnimated:YES completion:NULL];
    self.documentSharingController = nil;
}

#pragma mark - ToolBarControllerDelegate

- (void)toolBarController:(ADXToolbarController *)toolBarController didSelectViewController:(UIViewController *)viewController
{
    [self hidePDFStrips];
    
    if ([viewController isKindOfClass:[ADXAnnotationViewController class]]) {
        [[ADXUserDefaults sharedDefaults] setCurrentlySelectedDocumentTab:ADXSelectedDocumentTabAnnotations];
    }

    if ([viewController isKindOfClass:[ADXRevisionsViewController class]]) {
        [[ADXUserDefaults sharedDefaults] setCurrentlySelectedDocumentTab:ADXSelectedDocumentTabRevisions];
    }

    if ([viewController isKindOfClass:[ADXSearchViewController class]]) {
        [[ADXUserDefaults sharedDefaults] setCurrentlySelectedDocumentTab:ADXSelectedDocumentTabSearch];
    }

    __weak ADXPDFViewController *weakSelf = self;
    [self.toolsDelegates enumerateDelegateWhoRespondsToSelector:@selector(pdfController:selectedToolDidChanged:)
                                           usingBlock:^(id delegate, BOOL *stop) {
                                               [delegate pdfController:weakSelf selectedToolDidChanged:viewController];
                                        }];
}

- (void)delegateDidChangeViewMode:(PSPDFViewMode)viewMode
{
    __weak ADXPDFViewController *weakSelf = self;
    [self.toolsDelegates enumerateDelegateWhoRespondsToSelector:@selector(pdfController:didChangeViewMode:) usingBlock:^(id delegate, BOOL *stop) {
        [delegate pdfController:weakSelf didChangeViewMode:viewMode];
    }];
    
    [super delegateDidChangeViewMode:viewMode];
}

#pragma mark - presentViewController

- (void)presentViewController:(UIViewController *)viewControllerToPresent animated:(BOOL)flag completion:(void (^)(void))completion
{
    [self hidePDFStrips];
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        if ( self.modalPresentationStyle == UIModalPresentationFormSheet )
        {
            [self addChildViewController:viewControllerToPresent];
            [viewControllerToPresent didMoveToParentViewController:self];
             self.privatePresentedViewController = viewControllerToPresent;
            
            [self presentPopupViewController:viewControllerToPresent animationType:MJPopupViewAnimationFade];
            self.actualModalPresentationStyle = UIModalPresentationFormSheet;
        }
        else {
            self.actualModalPresentationStyle = UIModalPresentationFullScreen;
            [super presentViewController:viewControllerToPresent animated:flag completion:completion];
        }
    }
    else
    {
        if (viewControllerToPresent.modalPresentationStyle == UIModalPresentationFormSheet) {
            // Let the base class handle it
            [super presentViewController:viewControllerToPresent animated:flag completion:completion];
        } else {
            [self addChildViewController:viewControllerToPresent];
            [viewControllerToPresent didMoveToParentViewController:self];
            
            // Update the tool view's with to match the container
            CGRect frame = viewControllerToPresent.view.frame;
            frame.size.width = self.toolsPane.bounds.size.width;
            viewControllerToPresent.view.frame = frame;

            // Update the container's height to match the tool view
            frame = self.toolsPane.frame;
            frame.size.height = viewControllerToPresent.view.frame.size.height;
            self.toolsPane.frame = frame;

            [self.toolsPane addSubview:viewControllerToPresent.view];
        
            self.privatePresentedViewController = viewControllerToPresent;
            if( self.toolbarController.selectedViewController != viewControllerToPresent && [self.toolbarController.viewControllers containsObject:viewControllerToPresent])
            {
                [self.toolbarController setSelectedViewController:viewControllerToPresent];
            }
        
            [self openToolsPaneFromKeyboard:YES completion:completion];
        }
    }
}

- (void)dismissViewControllerAnimated:(BOOL)animated completion:(void (^)(void))completion
{
    [self hidePDFStrips];
    
    __weak ADXPDFViewController *weakSelf = self;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        if ( self.actualModalPresentationStyle == UIModalPresentationFormSheet )
        {
            if ( self.privatePresentedViewController )
            {
                [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade completion:^{
                    [weakSelf.privatePresentedViewController.view removeFromSuperview];
                    [weakSelf.privatePresentedViewController removeFromParentViewController];
                    weakSelf.privatePresentedViewController = nil;
                    
                    if(completion)
                        completion();
                }];
                
            }
        }
        else
        {
            [super dismissViewControllerAnimated:animated completion:completion];
        }
        
        self.modalPresentationStyle = UIModalPresentationFullScreen;
    }
    else
    {
        if ( self.privatePresentedViewController )
        {
            [self closeToolsPaneAnimated:animated completion:^{
                [weakSelf.privatePresentedViewController.view removeFromSuperview];
                [weakSelf.privatePresentedViewController removeFromParentViewController];
                weakSelf.privatePresentedViewController = nil;
                
                if(completion)
                    completion();
            }];
        }
    }
}

- (UIViewController*) presentedViewController
{
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        return [super presentedViewController];
    }
    else
    {
        return self.privatePresentedViewController;
    }
}

#pragma mark - ADXPDFChangesViewControllerDelegate

- (void)prepareForPDFStripTransitionToPage:(NSUInteger)pageIndex
{
    self.stripPageIndex = pageIndex;
    [self setPage:self.stripPageIndex animated:NO];

    PSPDFCache *cache = [PSPDFCache sharedCache];
    [cache renderAndCacheImageForDocument:self.document page:pageIndex size:PSPDFSizeNative error:nil];
}

- (void)positionPDFStripView:(UIView *)view atRect:(CGRect)pageRect onPage:(NSUInteger)pageIndex withFocusArea:(CGRect)stripFocusRect
{
    // Save these for when the view appears
    self.stripPageIndex = pageIndex;
    self.stripToPosition = view;
    self.stripPageRect = pageRect;
    self.stripFocusRect = stripFocusRect;
    
    [self hidePDFStrips];
}

- (void)animatePDFStripPartTwo
{
    PSPDFPageView *pageView = [self pageViewForPage:self.stripPageIndex];
    CGRect viewRect = [pageView convertPDFRectToViewRect:self.stripPageRect];
    CGRect windowRect = [self.view.window convertRect:viewRect fromView:pageView];
    
    // TODO: I don't know why (and that's why it's a TODO) but offsetting the origin by 4 here
    // results in a smoother "landing" for the strip (as in, it seems to land exactly over
    // the target document).  This is likely accounting for an error elsewhere.
    windowRect.origin.y -= 4;
    
    [UIView animateWithDuration:0.75
                          delay:0.25
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         self.stripToPosition.frame = windowRect;
                     } completion:^(BOOL finished) {
                         [UIView animateWithDuration:0.25 animations:^{
                             self.stripToPosition.alpha = 0.0;
                         } completion:^(BOOL finished) {
                             [self.stripToPosition removeFromSuperview];
                             self.stripToPosition = nil;
                             
                             // TODO: we should be using the change itself, and not the location of the change, to trigger the popup
                             
                             CGRect changeRectInViewCoords = [pageView convertPDFRectToViewRect:self.stripFocusRect];
                             CGPoint changeCenter = CGPointMake(changeRectInViewCoords.origin.x + changeRectInViewCoords.size.width/2,
                                                                changeRectInViewCoords.origin.y + changeRectInViewCoords.size.height/2);
                             
                             [self.toolsDelegates enumerateDelegateWhoRespondsToSelector:@selector(pdfController:didTapOnPageView:atPoint:) usingBlock:^(id delegate, BOOL *stop) {
                                 if ([delegate isKindOfClass:[ADXRevisionsViewController class]]) {
                                     [delegate pdfController:self didTapOnPageView:pageView atPoint:changeCenter];
                                     *stop = YES;
                                 }
                             }];
                         }];
                     }];
}

// When transitioning back from the changes view, transition the disembodied change strip into position over the document
- (void)animatePDFStrip
{
    PSPDFPageView *pageView = [self pageViewForPage:self.stripPageIndex];
    if (!pageView) {
        [self.stripToPosition removeFromSuperview];
        self.stripToPosition = nil;
        return;
    }
    
    // PSPDFPageView is going to return bad values for convertPDFRectToViewRect unless we let one
    // iteration of the run loop go by before asking.  So here goes.  (As suggested by @steipete)
    [self performSelector:@selector(animatePDFStripPartTwo) withObject:nil afterDelay:0];
}

- (void)hidePDFStripsAndShowPage:(NSUInteger)pageIndex
{
    [self hidePDFStrips];
    [self setPage:pageIndex animated:YES];
}

#pragma mark - ADXNotificationsActionsDelegate

- (void)didSelectDocument:(ADXDocument *)document fromEvent:(ADXEvent *)event
{
    if ([document.id isEqualToString:self.adxDocument.id] && [document.version isEqualToNumber:self.adxDocument.version]) {
        return;
    }
    [self didSelectDocOpenAction:nil document:document];
}

#pragma mark - ADXNotificationBubbleManagerDelegate

- (void)stickyBubbleWasCancelled:(ADXNotificationBubble *)bubble
{
    [[ADXAppDelegate sharedInstance] passCheckpoint:@"Tapped Cancel on the newer version alert"];
}

- (void)stickyBubbleWasActioned:(ADXNotificationBubble *)bubble
{
    [[ADXAppDelegate sharedInstance] passCheckpoint:@"Tapped Open on the newer version alert"];
    [self openLatestVersion];
}

@end
