//
//  ADXPDFChangeAnnotationView.m
//  Reader
//
//  Created by Steve Tibbett on 2012-10-09.
//  Copyright (c) 2012 ActivDox Inc. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "ADXPDFChangeAnnotationView.h"
#import "PSPDFPageInfo.h"
#import "PSPDFDocument.h"

#define KChangeRectMinimumWidth 20

@interface ADXPDFChangeAnnotationView()
@property (strong, nonatomic) ADXHighlightedContentHelper *changes;
@property (nonatomic) NSUInteger pageIndex;
@property (nonatomic) CGRect pageExtent;
@end


@implementation ADXPDFChangeAnnotationView

- (id)initWithChanges:(ADXHighlightedContentHelper *)changes pageIndex:(NSUInteger)pageIndex pageExtent:(CGRect)pageExtent
{
    if ((self = [super initWithFrame:CGRectZero])) {
        self.backgroundColor = [UIColor clearColor];
        self.changes = changes;
        self.pageIndex = pageIndex;
        self.pageExtent = pageExtent;
        [self setNeedsLayout];
    }
    return self;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UIView

- (void)layoutSubviews {
    [super layoutSubviews];

    self.frame = self.superview.frame;
}

- (void)drawRect:(CGRect)rect
{
    __weak ADXPDFChangeAnnotationView *weakSelf = self;
    CGContextRef context = UIGraphicsGetCurrentContext();
    UIGraphicsPushContext(context);
    
    [self.changes drawChangesOnPage:self.pageIndex context:context withBlock:^CGRect(CGRect pageRect) {
        // Map pageRect from document coordinates to view coordinates:
        return [weakSelf mapPageRectBlock:pageRect];
    }];
    
    UIGraphicsPopContext();
}


- (float)point:(CGPoint)point distanceFromRect:(CGRect)rect
{
    if (CGRectContainsPoint(rect, point)) {
        // Rect contains the point
        return 0.0;
    }

    float dx = 0.0;
    if (point.x < rect.origin.x) {
        dx = rect.origin.x - point.x;
    } else
    if (point.x > (rect.origin.x + rect.size.width)) {
        dx = point.x - (rect.origin.x + rect.size.width);
    }

    float dy = 0.0;
    if (point.y < rect.origin.y) {
        dy = rect.origin.y - point.y;
    } else
    if (point.y > (rect.origin.y + rect.size.height)) {
        dy = point.y - (rect.origin.y + rect.size.height);
    }

    return fmax(dx, dy);
}

- (ADXPDFFoundChange)hitTestForChangeAtPoint:(CGPoint)point inPage:(NSUInteger)pageIndex
{
    __block ADXPDFFoundChange res;
    __weak ADXPDFChangeAnnotationView *weakSelf = self;

    // Must be closer than this to be considered
    __block float changeDist = KChangeRectMinimumWidth;
    res.change = nil;
    
    [self.changes enumerateChangesOnPage:pageIndex withBlock:^(CGRect pageRect, NSString *type, id c) {

        CGRect changeRectOnPage = [weakSelf mapPageRectBlock:pageRect];
        float dist = [self point:point distanceFromRect:changeRectOnPage];
        if (dist < changeDist) {
            changeDist = dist;
            res.change = c;
            res.rect = changeRectOnPage;
        }
    }];
    
    return res;
};

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public

- (void)popupAnimation {
    __weak ADXPDFChangeAnnotationView *weakSelf = self;
    self.transform = CGAffineTransformMakeScale(2, 2);
    [UIView animateWithDuration:0.6f delay:0.f options:UIViewAnimationOptionAllowUserInteraction animations:^{
        weakSelf.transform = CGAffineTransformIdentity;
    } completion:nil];
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - PSPDFAnnotationView

- (void)didChangePageFrame:(CGRect)frame {
    [self setNeedsLayout];
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark -

- (CGRect) mapPageRectBlock:(CGRect)pageRect
{
    // Scale
    float scale = self.bounds.size.height / self.pageExtent.size.height;
    pageRect.origin.x *= scale;
    pageRect.origin.y *= scale;
    pageRect.size.width *= scale;
    pageRect.size.height *= scale;
    
    // Flip
    pageRect.origin.y = (self.bounds.size.height - pageRect.origin.y) - pageRect.size.height;
    
    return pageRect;
}

@end
