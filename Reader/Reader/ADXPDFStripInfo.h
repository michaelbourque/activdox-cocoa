//
//  ADXPDFStripInfo.h
//  Reader
//
//  Created by Steve Tibbett on 2012-11-30.
//  Copyright (c) 2012 ActivDox Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

// Used by ADXPDFStripsViewController and ADXPDFStripCell

@interface ADXPDFStripInfo : NSObject

// PSPDFAnnotations to draw on top of the strip
@property (strong, nonatomic) NSArray *annotations;

// Changes to draw on top of the strip
@property (strong, nonatomic) id change;

// Page index the strip is on
@property (nonatomic) NSUInteger pageIndex;

// Extent of the area on the page the strip is to indicate
@property (nonatomic) CGRect stripExtent;

@property (strong, nonatomic) NSAttributedString *summary;

@end
