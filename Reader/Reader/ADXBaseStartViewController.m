//
//  ADXBaseStartViewController.m
//  
//
//  Created by Gavin McKenzie on 2012-08-28.
//
//

#import "ADXBaseStartViewController.h"
#import "ADXAppDelegate.h"

NSString * const kADXStartViewControllerAlreadyLaunched = @"kADXStartViewControllerAlreadyLaunched_iPhone";

@implementation ADXBaseStartViewController

#pragma mark - Helpers

- (BOOL)firstLaunch
{
    BOOL result = YES;
    ADXAppDelegate *appDelegate = (ADXAppDelegate *)[[UIApplication sharedApplication] delegate];
    if ([appDelegate.stashedState valueForKey:kADXStartViewControllerAlreadyLaunched]) {
        result = NO;
    }
    return result;
}

- (void)setupLoginProcessor
{
    ADXAppDelegate *appDelegate = (ADXAppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = appDelegate.managedObjectContext;

    // We always want a fresh instance
    self.loginProcessor = [[ADXLoginProcessor alloc] initWithParentManagedObjectContext:context];
}

- (BOOL)displayBlankStartupScreen
{
    BOOL result = [[NSUserDefaults standardUserDefaults] boolForKey:@"display_blank_startup_screen"];
    if (result) {
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"display_blank_startup_screen"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    return result;
}

#pragma mark - View Management

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}


@end
