//
//  ADXBaseDocListCell.h
//  Reader
//
//  Created by Gavin McKenzie on 2013-04-13.
//  Copyright (c) 2013 ActivDox Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ADXBaseDocListCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UIImageView *thumbnailImageView;
@property (weak, nonatomic) IBOutlet UILabel *primaryLabel;
@property (weak, nonatomic) IBOutlet UILabel *secondaryLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *leadingHConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *trailingHConstraint;
@end
