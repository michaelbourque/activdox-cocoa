//
//  ADXDocVersionsPopoverCell.m
//  Reader
//
//  Created by Gavin McKenzie on 2012-09-13.
//  Copyright (c) 2012 ActivDox Inc. All rights reserved.
//

#import "ADXDocVersionsPopoverCell.h"
#import "PSPDFKit.h"
#import "ADXAppDelegate.h"
#import <QuartzCore/QuartzCore.h>

CGFloat const kADXDocVersionsPopoverCellHeight = 101.0f;
CGFloat const kADXDocVersionsPopoverCellWidth = 320.0f;

@interface ADXDocVersionsPopoverCell () <PSPDFCacheDelegate>
@property (strong, nonatomic) PSPDFDocument *pdfDocument;
@property (nonatomic) BOOL currentlyReading;
@end

@implementation ADXDocVersionsPopoverCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self finishInitialization];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self finishInitialization];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self finishInitialization];
    }
    return self;
}

- (void)finishInitialization
{
    [[PSPDFCache sharedCache] addDelegate:self];
}

- (void)awakeFromNib
{
    self.thumbnailImageView.layer.shadowColor = [UIColor blackColor].CGColor;
    self.thumbnailImageView.layer.shadowOffset = CGSizeMake(0, 1);
    self.thumbnailImageView.layer.shadowOpacity = 0.7;
    self.thumbnailImageView.layer.shadowRadius = 1.0;
}

#pragma mark - Destruction

- (void)dealloc
{
    [self removeDocumentObservers];
    [[PSPDFCache sharedCache] removeDelegate:self];
}

#pragma mark - Notifications

- (void)addDocumentObservers
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleThumbnailDownloadedNotification:) name:kADXServiceDocumentThumbnailDownloadedNotification object:nil];
}

- (void)removeDocumentObservers
{
    @try {
        [[NSNotificationCenter defaultCenter] removeObserver:self name:kADXServiceDocumentThumbnailDownloadedNotification object:nil];
    }
    @catch (NSException *exception) {
        // Do nothing.
    }
}

#pragma mark - Accessors

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

- (void)setDocument:(ADXDocument *)document
{
    [self removeDocumentObservers];
    _document = document;
    
    ADXAccount *account = [ADXAppDelegate sharedInstance].currentAccount;
    ADXDocument *mostRecent = [ADXDocument mostRecentlyOpenedDocumentWithID:document.id accountID:account.id context:document.managedObjectContext];
    self.currentlyReading = (document == mostRecent);

    if (document) {
        [self addDocumentObservers];
        [self refresh];
    } else {
        self.thumbnailImageView.image = nil;
        self.versionLabel.text = nil;
        self.dateLabel.text = nil;
    }
}

- (PSPDFDocument *)pdfDocument
{
    if (!_pdfDocument && self.document.downloaded && self.document.content.data) {
        _pdfDocument = [[PSPDFDocument alloc] initWithData:self.document.content.data];
    }
    return _pdfDocument;
}

#pragma mark - Thumbnail

- (void)showProgressIndicators
{
    [self.spinner startAnimating];
    [self.spinner setHidden:NO];
}

- (void)hideProgressIndicators
{
    [self.spinner stopAnimating];
    [self.spinner setHidden:YES];
}

- (UIImage *)thumbnailImage
{
    return self.thumbnailImageView.image;
}

- (void)presentThumbnailImage:(UIImage *)thumbnail
{
    [self hideProgressIndicators];
    self.thumbnailImageView.image = thumbnail;
}

- (void)presentEmptyThumbnailImage
{
    [self hideProgressIndicators];
    self.thumbnailImageView.image = [UIImage imageNamed:@"doc_background"];
}

- (void)refresh
{
    if (self.currentlyReading) {
        self.versionLabel.text = [NSString stringWithFormat:@"%@  ☕", self.document.versionDisplayLabel];
    } else {
        self.versionLabel.text = [self.document versionDisplayLabel];
    }

    self.dateLabel.text = [self.document publishedDateDisplayLabel:YES];
    
    UIImage *thumbnail = nil;
    ADXV2Service *service = [ADXV2Service serviceForManagedObject:self.document];
    ADXDocumentThumbnail *documentThumbnail = self.document.thumbnail;
    
    if (documentThumbnail && !IsEmpty(documentThumbnail.data)) {
        thumbnail = [UIImage imageWithData:documentThumbnail.data];
        [self presentThumbnailImage:thumbnail];
    } else {
        [self presentEmptyThumbnailImage];
        [service downloadThumbnailForDocument:self.document.id versionID:self.document.version completion:nil];
    }
    
    __weak ADXDocVersionsPopoverCell *weakSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        [weakSelf setNeedsLayout];
    });    
}

#pragma mark - Notification Handling

- (void)handleThumbnailDownloadedNotification:(NSNotification *)notification
{
    NSString *documentID = [notification.userInfo valueForKey:kADXServiceNotificationPayload];
    if (![documentID isEqualToString:self.document.id]) {
        return;
    }
    
    __weak ADXDocVersionsPopoverCell *weakSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        [weakSelf refresh];
    });
}

#pragma mark - PSPDFCacheDelegate

- (void)didCachePageForDocument:(PSPDFDocument *)document page:(NSUInteger)page image:(UIImage *)cachedImage size:(PSPDFSize)size
{
    if (document == self.pdfDocument && page == 0 && size == PSPDFSizeThumbnail) {
        [self presentThumbnailImage:cachedImage];
    }
}

@end
