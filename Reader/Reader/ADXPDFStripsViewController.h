//
//  ADXPDFStripsViewController.h
//  Reader
//
//  Created by Steve Tibbett on 2012-11-29.
//  Copyright (c) 2012 ActivDox Inc. All rights reserved.
//

#import "ActivDoxCore.h"
#import "ADXPDFStripCell.h"

@protocol ADXPDFStripsViewControllerDelegate <NSObject>
- (void)positionPDFStripView:(UIView *)view atRect:(CGRect)pageRect onPage:(NSUInteger)pageIndex withFocusArea:(CGRect)focusRect;
- (void)prepareForPDFStripTransitionToPage:(NSUInteger)pageIndex;
- (void)hidePDFStripsAndShowPage:(NSUInteger)pageIndex;
@end

@protocol ADXPDFStripsViewControllerDataSource <NSObject>
- (int)numberOfSectionsForPDFStrips;
- (int)numberOfPDFStripsInSection:(int)pageIndex;
- (ADXPDFStripInfo *)infoForPDFStripAtIndexPath:(NSIndexPath *)indexPath;
- (NSString *)titleForPDFStripSection:(int)section;
- (ADXHighlightedContentHelper *)highlightedContentForPDFStrips;
@end

@interface ADXPDFStripsViewController : UIViewController <UICollectionViewDelegateFlowLayout, UICollectionViewDelegate, UICollectionViewDataSource>

@property (weak, nonatomic) ADXDocument *document;
@property (weak, nonatomic) PSPDFDocument *pspdfDocument;
@property (weak, nonatomic) id<ADXPDFStripsViewControllerDelegate> PDFStripsDelegate;
@property (weak, nonatomic) id<ADXPDFStripsViewControllerDataSource> PDFStripsDataSource;
@property (nonatomic) UIEdgeInsets contentInset;

- (void)refresh;

@end
