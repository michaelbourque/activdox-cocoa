//
//  ADXAppDelegate+Session.m
//  Reader
//
//  Created by Gavin McKenzie on 2013-04-22.
//  Copyright (c) 2013 ActivDox Inc. All rights reserved.
//

#import "ADXAppDelegate+Session.h"
#import "PSPDFAlertView.h"

@implementation ADXAppDelegate (Session)

- (void)performLogoutTasks
{
    NSString *accountID = self.currentAccount.id;
    
    [ADXV2Service deregisterServiceForAccount:accountID];
    [[ADXUserDefaults sharedDefaults] setUserLoggedOut:YES];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kADXLogoutNotification object:nil];
    
    [self clearCurrentAccount];
}

- (void)presentNeedToBeOnlineAlert
{
    dispatch_async(dispatch_get_main_queue(), ^{
        PSPDFAlertView *alertView = [[PSPDFAlertView alloc] initWithTitle:@"No server connection" message:@"This feature requires a connection to the server. Please check your\nWi-Fi and cellular settings."];
        [alertView setCancelButtonWithTitle:@"OK" block:nil];
        [alertView show];
    });
}

#pragma mark - Housekeeping

- (void)performHouseKeepingTasks:(ADXCompletionBlock)completionBlock
{
    [self houseKeepingRemoveCollections:^(BOOL success, NSError *error) {
        BOOL removeCollectionsSuccess = success;
        
        [self houseKeepingRemoveDocuments:^(BOOL success, NSError *error) {
            BOOL removeDocumentsSuccess = success;
            
            [self houseKeepingRemoveNotes:^(BOOL success, NSError *error) {
                BOOL allSuccess = removeCollectionsSuccess && removeDocumentsSuccess && success;
                
                if (completionBlock) {
                    completionBlock(allSuccess, error);
                }
            }];
        }];
    }];
}

- (void)houseKeepingRemoveCollections:(ADXCompletionBlock)completionBlock
{
    __weak NSManagedObjectContext *moc = self.managedObjectContext;
    
    [self.managedObjectContext performBlock:^{
        // Collection deletion
        //
        NSArray *collectionsToDelete = [ADXCollection collectionsToBeDeletedOnStartup:moc];
        for (ADXCollection *collection in collectionsToDelete) {
            LogTrace(@"Removing collection ID %@", collection.id);
            [self.managedObjectContext deleteObject:collection];
        }
        
        if (completionBlock) {
            completionBlock(YES, nil);
        }
    }];
}

- (void)houseKeepingRemoveDocuments:(ADXCompletionBlock)completionBlock
{
    __weak NSManagedObjectContext *moc = self.managedObjectContext;
    
    [self.managedObjectContext performBlock:^{
        // Document deletion
        //
        NSArray *documentsToDelete = [ADXDocument documentsToBeDeletedOnStartup:moc];
        for (ADXDocument *document in documentsToDelete) {
            LogTrace(@"Removing document ID %@ version %@", document.id, document.version);
            [self.managedObjectContext deleteObject:document];
        }
        
        // Drop content of documents excluded from automatic download
        //
        NSArray *metaDocumentsExcluded = [ADXMetaDocument metaDocumentsExcludedFromAutomaticContentDownload:moc];
        for (ADXMetaDocument *metaDocument in metaDocumentsExcluded) {
            NSSet *documents = metaDocument.versions;
            for (ADXDocument *document in documents) {
                if (document.content.data) {
                    LogTrace(@"Removing content from document %@ version %@", document.id, document.version);
                    document.content.data = nil;
                    document.downloaded = @NO;
                }
            }
        }
        
        if (completionBlock) {
            completionBlock(YES, nil);
        }
    }];
}

- (void)houseKeepingRemoveNotes:(ADXCompletionBlock)completionBlock
{
    __weak NSManagedObjectContext *moc = self.managedObjectContext;
    
    [self.managedObjectContext performBlock:^{
        NSArray *notesToDelete = [ADXDocumentNote notesToBeDeletedOnStartup:moc];
        for (ADXDocumentNote *note in notesToDelete) {
            LogTrace(@"Removing note id %@", note.id);
            [self.managedObjectContext deleteObject:note];
        }
        
        if (completionBlock) {
            completionBlock(YES, nil);
        }
    }];
}

@end
