//
//  ADXNotificationBubble.h
//  Reader
//
//  Created by Gavin McKenzie on 2013-03-25.
//  Copyright (c) 2013 ActivDox Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ADXNotificationBubble;

@protocol ADXNotificationBubbleDelegate <NSObject>
- (void)bubbleDidEndWithTimeout:(ADXNotificationBubble *)bubble;
- (void)bubbleDidEndWithTap:(ADXNotificationBubble *)bubble;
- (void)stickyBubbleDidEndWithCancel:(ADXNotificationBubble *)bubble;
- (void)stickyBubbleDidEndWithAction:(ADXNotificationBubble *)bubble;
@end

@interface ADXNotificationBubble : UIView
@property (strong, nonatomic) NSString *message;
@property (assign, nonatomic) BOOL sticky;
@property (strong, nonatomic) NSString *stickyActionLabel;
@property (strong, nonatomic) NSDictionary *userInfo;
@property (weak, nonatomic) id<ADXNotificationBubbleDelegate> delegate;
@property (weak, nonatomic) id privateStickyDelegate;
+ (ADXNotificationBubble *)newBubble;
- (void)startTimer;
@end
