//
//  ADXSearchViewController.h
//  PSPDFKit
//
//  Created by matteo ugolini on 2012-08-01.
//  Copyright (c) 2012 ActivDox Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#import "PSPDFAttributedLabel.h"
#import "PSPDFViewController.h"
#import "PSPDFTextSearch.h"
#import "PSPDFCache.h"

#import "ADXPDFViewController.h"
#import "ADXPageThumbnailView.h"
#import "ADXAppCommonMacros.h"

typedef enum {
    ADXSearchModeNone = 0,
    ADXSearchModeCurrentPage,
    ADXSearchModeEntireDocument
} ADXSearchMode;

@class PSPDFDocument, PSPDFViewController, PSPDFSearchResult;

/// pdf search controller.
@interface ADXSearchViewController : UIViewController <UISearchDisplayDelegate, UISearchBarDelegate, PSPDFTextSearchDelegate, ADXPDFToolsDelegate>

@property(nonatomic, assign) BOOL       clearHighlightsWhenClosed;
@property(nonatomic, assign, readonly)  PSPDFSearchStatus searchStatus;
@property(nonatomic, assign, readonly)  ADXSearchMode searchMode;

- (void)searchForString:(NSString*)searchText;
- (id)initWithDocument:(PSPDFDocument*)document pdfController:(ADXPDFViewController *)pdfController;

@end
