//
//  ADXBarButtonItem.m
//  Reader
//
//  Created by Steve Tibbett on 2013-04-22.
//  Copyright (c) 2013 ActivDox Inc. All rights reserved.
//

#import "ADXBarButtonItem.h"

@implementation ADXBarButtonItem


+ (ADXBarButtonItem *)itemWithLabelText:(NSString *)text target:(id)target selector:(SEL)selector
{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.showsTouchWhenHighlighted = YES;
    [button setTitle:text forState:UIControlStateNormal];
    button.titleLabel.font = [UIFont boldSystemFontOfSize:[UIFont systemFontSize] - 1];
    [button addTarget:target action:selector forControlEvents:UIControlEventTouchUpInside];
    CGSize size = [text sizeWithFont:button.titleLabel.font];
    [button setFrame:CGRectMake(0.0f, 0.0f, size.width + 20, 25.0f)];
    ADXBarButtonItem *item = [[ADXBarButtonItem alloc] initWithCustomView:button];
    return item;
}

+ (ADXBarButtonItem *)itemWithImageNamed:(NSString *)imageName target:(id)target selector:(SEL)selector
{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.showsTouchWhenHighlighted = YES;
    [button setImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
    [button addTarget:target action:selector forControlEvents:UIControlEventTouchUpInside];
    [button setFrame:CGRectMake(0.0f, 0.0f, 45.0f, 25.0f)];
    ADXBarButtonItem *item = [[ADXBarButtonItem alloc] initWithCustomView:button];
    return item;
}

@end
