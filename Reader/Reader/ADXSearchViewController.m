//
//  ADXSearchViewController.m
//  PSPDFKit
//
//  Created by matteo ugolini on 2012-08-01.
//  Copyright (c) 2012 ActivDox Inc. All rights reserved.
//
#import <CoreText/CoreText.h>

#import "PSPDFKit.h"
#import "ADXSearchViewController.h"
#import "PSPDFSearchResult.h"
#import "PSPDFViewController.h"
#import "PSPDFHighlightAnnotationView.h"
#import "PSPDFSearchHighlightView.h"
#import "PSPDFSearchStatusCell.h"
#import "PSPDFTextSearch.h"
#import "PSPDFBarButtonItem.h"

#import "PSPDFViewController+Internal.h"
#import "PSPDFMenuItem.h"

#import "ADXPageThumbnailView.h"
#import "ADXPDFViewController.h"
#import "ADXCollectionViewHorizontalLayout.h"
#import "ADXDescriptionCell.h"

// used in UITableViewCell
#define kPSPDFSearchMinimumLength 3
#define kADX_PSPDFAttributedLabelTag 25633

typedef enum {
    DisplayModeThumbnail = 0,
    DisplayModeDetail
} DisplayMode;

@interface ADXSearchViewController ()

@property (nonatomic, assign, readwrite)    ADXSearchMode       searchMode;
@property (nonatomic, strong)               PSPDFDocument       *document;
@property (nonatomic, weak)               ADXPDFViewController *pdfController;
@property (nonatomic, assign)               PSPDFSearchStatus   searchStatus;
@property (nonatomic, strong)               NSMutableArray      *searchResults;
@property (nonatomic, strong)               NSMutableDictionary *searchResultsByPage;

@property (nonatomic, weak) IBOutlet        UIView              *thumbnailView;
@property (nonatomic, weak) IBOutlet        UITableView         *detailView;
@property (nonatomic, weak) IBOutlet        UIPageControl       *thumbPageControl;
@property (nonatomic, weak) IBOutlet        UILabel             *noResultFound;
@property (nonatomic, weak) IBOutlet        UISearchBar         *searchBar;
@property (nonatomic, weak) IBOutlet        UISearchBar         *iPhoneToolbarSearch;
@property (nonatomic, weak) IBOutlet        UILabel             *resultCountLabel;
@property (nonatomic, weak) IBOutlet        UIActivityIndicatorView *searchIndicatorView;
@property (weak, nonatomic) IBOutlet        UIBarButtonItem     *toggleThumbnailsBarButtonItem;
@property (weak, nonatomic) IBOutlet        UIBarButtonItem     *toggleDetailBarButtonItem;
@property (nonatomic, assign)               DisplayMode   displayMode;
@property (weak, nonatomic) IBOutlet UICollectionView *thumbnailCollectionView;
@property (strong, nonatomic) PSPDFTextSearch *textSearch;
@property (nonatomic, strong) IBOutletCollection(UIBarButtonItem) NSArray *buttonItems;

- (void) refreshPageResultsIndex;

@end

@implementation ADXSearchViewController

#pragma mark - NSObject

- (id)initWithDocument:(PSPDFDocument *)document pdfController:(ADXPDFViewController *)pdfController
{
    NSString *ii = UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ? @"Phone" : @"Pad";
    
    self = [super initWithNibName:[NSString stringWithFormat:@"ADXSearchPane-%@", ii] bundle:nil];
    if ( self )
    {
        _document = document;
        _pdfController = pdfController;
        
        self.textSearch = [[PSPDFTextSearch alloc] initWithDocument:document];
        self.textSearch.delegate = self;
        
        UINib *nib = [UINib nibWithNibName:[NSString stringWithFormat:@"ADXSearchToolsBar-%@", ii]
                                    bundle:nil];
        [nib instantiateWithOwner:self options:nil];
        [super setToolbarItems:[self.buttonItems copy]];
        
        UITabBarItem *tabBarItem = [[UITabBarItem alloc] initWithTitle:@"search" image:[UIImage imageNamed:@"053-Search.png"] tag:1];
        tabBarItem.accessibilityLabel = NSLocalizedString(@"Search", nil);
        [super setTabBarItem:tabBarItem];
        
        self.toggleThumbnailsBarButtonItem.accessibilityLabel = NSLocalizedString(@"Toggle Thumbnails", nil);
        self.toggleDetailBarButtonItem.accessibilityLabel = NSLocalizedString(@"Toggle Details", nil);
        
        _document.textSearch.delegate = self;
        [_pdfController addToolsDelegate:self];
        
        self.displayMode = DisplayModeThumbnail;
        
        //FIXME: WORKAROUND TO DELETE THE BACKGROUND FROM THE SEARCH BAR... we are better to design it with a normal textfild
        if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone )
        {
            UIView *background = [self.iPhoneToolbarSearch.subviews objectAtIndex:0];
            [background removeFromSuperview];
            self.displayMode = DisplayModeDetail;
        }
    }
    return self;
}

- (void) viewDidLoad
{
    [super viewDidLoad];

    [self configureThumbnailCollectionView];
}

- (void)dealloc
{
    [self.pdfController removeToolsDelegate:self];
}

#pragma mark - UIView

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.searchStatus = PSPDFSearchStatusIdle;

    [self refresh];
}


- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [self.document.textSearch cancelAllOperationsAndWait];
    [self.textSearch cancelAllOperationsAndWait];
    
    [self.searchBar resignFirstResponder];
    
    if (_clearHighlightsWhenClosed) {
        [self.pdfController clearHighlightedSearchResults];
    }
}

- (void)viewDidUnload
{
    [self setThumbnailCollectionView:nil];
    [self setToggleThumbnailsBarButtonItem:nil];
    [self setToggleDetailBarButtonItem:nil];
    [super viewDidUnload];
    self.document.textSearch.delegate = self.pdfController;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    return PSIsIpad() ? YES : toInterfaceOrientation != UIInterfaceOrientationPortraitUpsideDown;
}

#pragma mark - accessors

- (void)setDisplayMode:(DisplayMode)displayMode
{
    _displayMode = displayMode;
    
    [self refresh];
}

- (void)setDocument:(PSPDFDocument *)document
{
    [self.document.textSearch cancelAllOperationsAndWait];
    [self.textSearch cancelAllOperationsAndWait];
    
    _document = document;
    
    self.document.textSearch.delegate = self;
    self.textSearch.delegate = self;
    
    // force a new search into the new document
    [self filterContentForSearchText:self.searchBar.text scope:0];
}

#pragma mark - 

- (void) searchForString:(NSString*)searchText
{
    self.searchBar.text = searchText;
    [self filterContentForSearchText:searchText scope:0];
    self.searchMode = ADXSearchModeEntireDocument;
}

- (void) refresh
{
    [self refreshResultCount];
    [self refreshPageResultsIndex];
    
    if ([self.searchResults count] == 0) {
        self.noResultFound.hidden = NO;
    } else {
        self.noResultFound.hidden = YES;
    }

    self.thumbnailView.hidden = self.displayMode == DisplayModeDetail;
    self.detailView.hidden = self.displayMode == DisplayModeThumbnail;

    [self reloadData];
    [self updatePageControl];
}

- (void)refreshResultCount
{
    self.resultCountLabel.text = [NSString stringWithFormat:NSLocalizedString(@"%d found",nil),[self.searchResults count]];
}

- (IBAction)didTapThumbnailMenu:(id)sender
{
    BOOL shouldToggle = YES;
    
    [self.searchBar resignFirstResponder];
    
    if (self.displayMode != DisplayModeThumbnail && self.pdfController.presentedViewController == self) {
        // Switching from detail to thumbnail, don't toggle the view closed
        shouldToggle = NO;
    }

    self.displayMode = DisplayModeThumbnail;
    
    if (shouldToggle) {
        [self openClose:sender];
    }
}

- (IBAction)didTapDetailMenu:(id)sender
{
    BOOL shouldToggle = YES;

    [self.searchBar resignFirstResponder];

    if (self.displayMode != DisplayModeDetail && self.pdfController.presentedViewController == self) {
        // Switching from thumbnail to detail, don't toggle the view closed
        shouldToggle = NO;
    }
    
    self.displayMode = DisplayModeDetail;
    
    if (shouldToggle) {
        [self openClose:sender];
    }
}

- (IBAction)openClose:(id)sender
{
    __weak ADXSearchViewController *weakSelf = self;
    if(self.pdfController.presentedViewController == self){
        [self.pdfController dismissViewControllerAnimated:YES completion:^{
            [weakSelf refresh];
        }];
    } else {
        [self.pdfController presentViewController:self animated:YES completion:NULL];
    }
}

- (void)setSearchStatus:(PSPDFSearchStatus)searchStatus updateResults:(BOOL)updateResults
{
    if (searchStatus != _searchStatus)
    {
        [self willChangeValueForKey:@"searchStatus"];
        _searchStatus = searchStatus;
        [self didChangeValueForKey:@"searchStatus"];
        
        if (updateResults)
        {
            if (self.searchStatus == PSPDFSearchStatusFinished) {
                [self refresh];
            }

            if (self.searchResults.count == 0) {
                self.noResultFound.hidden = NO;
            } else {
                self.noResultFound.hidden = YES;
            }
        }
    }
}

- (void)refreshPageResultsIndex
{
    [self.searchResultsByPage removeAllObjects];
    for(PSPDFSearchResult *res in self.searchResults ) {
        [self addResultToIndex:res];
    }
}

- (void)addResultToIndex:(PSPDFSearchResult*)res
{
    NSNumber *pageIndex = [NSNumber numberWithInteger:res.pageIndex];
    NSMutableArray* pageResults = [self.searchResultsByPage objectForKey:pageIndex];
    if (pageResults == nil) {
        pageResults = [NSMutableArray array];
        [self.searchResultsByPage setObject:pageResults forKey:pageIndex];
    }
    
    [pageResults addObject:res];
}

- (void)updateIndex:(PSPDFSearchResult*) res
{
    NSNumber *pageIndex = [NSNumber numberWithInteger:res.pageIndex];
    
    NSMutableArray* pageResults = [self.searchResultsByPage objectForKey:pageIndex];
    NSUInteger objectIndex = [pageResults indexOfObject:res];

    if (pageResults != nil && objectIndex != NSNotFound) {
        [pageResults replaceObjectAtIndex:objectIndex withObject:res];
    } else {
        [self addResultToIndex:res];
    }
}

- (void)setSearchStatus:(PSPDFSearchStatus)searchStatus
{
    [self setSearchStatus:searchStatus updateResults:NO];
}

- (void)filterContentForSearchText:(NSString *)searchText scope:(NSString *)scope
{
    // clear search
    self.searchResults = nil; // First clear the filtered array.
    self.searchResultsByPage = nil;
    [self reloadData];

    // start searching the html documents
    if ([searchText length] >= kPSPDFSearchMinimumLength) {
        // FIXME: add method to controller
        [self.document.textSearch searchForString:searchText];
        [self.textSearch searchForString:searchText];
    } else {
        // Clear the result count label, since we're not actually searching
        self.resultCountLabel.text = @"";
        
        [self setSearchStatus:PSPDFSearchStatusIdle updateResults:YES];
        [self.pdfController clearHighlightedSearchResults];
    }
    
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone )
    {
        self.iPhoneToolbarSearch.text = searchText;
    }
}

- (void) reloadData
{
    if (self.displayMode == DisplayModeDetail) {
        [self.detailView reloadData];
    } else if (self.displayMode == DisplayModeThumbnail) {
        [self.thumbnailCollectionView reloadData];
    }
}

#pragma mark - PSPDFTextSearchDelegate

- (void)willStartSearch:(PSPDFTextSearch *)textSearch forTerm:(NSString *)searchTerm isFullSearch:(BOOL)isFullSearch
{
    if (textSearch != self.textSearch) {
        // Ignore the document search instance
        [self.pdfController willStartSearch:textSearch forTerm:searchTerm  isFullSearch:isFullSearch];
        return;
    }

    if (!isFullSearch) {
        return; // ignore partial search updates
    }
    
    PSPDFLog(@"Starting search for %@.", searchTerm);
    [self setSearchStatus:PSPDFSearchStatusActive updateResults:YES];
    
    self.searchResultsByPage = [NSMutableDictionary dictionaryWithCapacity:self.document.pageCount];
    self.searchResults = [NSMutableArray array];
    [self.searchIndicatorView startAnimating];
    
    [self refresh];
    
    // relay
    [self.pdfController willStartSearch:textSearch forTerm:searchTerm  isFullSearch:isFullSearch];
}

- (void)didUpdateSearch:(PSPDFTextSearch *)textSearch forTerm:(NSString *)searchTerm newSearchResults:(NSArray *)searchResults forPage:(NSUInteger)page
{
    if (textSearch != self.textSearch) {
        // Ignore the document search instance
        [self.pdfController didUpdateSearch:textSearch forTerm:searchTerm newSearchResults:searchResults forPage:page];
        return;
    }
    
    // ignore if filtered list content is not set (new search is already in progress?)
    if (!_searchResults || ![searchTerm isEqualToString:self.searchBar.text]) {
        [self.searchIndicatorView stopAnimating];
        return;
    }

    if ([searchResults count] > 0)
    {
        self.noResultFound.hidden = YES;
        NSMutableArray *indexPaths = [NSMutableArray arrayWithCapacity:[searchResults count]];
        for (PSPDFSearchResult *searchResult in searchResults) {
            // note: this may just be an *update* to include selection data.
            NSUInteger objectIndex = [_searchResults indexOfObject:searchResult];
            if (objectIndex != NSNotFound) {
                if (searchResult.selection) {  // replace only if we have selectionData
                    [_searchResults replaceObjectAtIndex:objectIndex withObject:searchResult];
                    [self updateIndex:searchResult];
                }
            } else {
                [self addResultToIndex:searchResult];
                
                NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[_searchResults count] inSection:0];
                [indexPaths addObject:indexPath];
                [_searchResults addObject:searchResult];
            }
        }

        // relay
        [self.pdfController didUpdateSearch:textSearch forTerm:searchTerm newSearchResults:searchResults forPage:page];
    }
}

- (void)didFinishSearch:(PSPDFTextSearch *)textSearch forTerm:(NSString *)searchTerm searchResults:(NSArray *)searchResults isFullSearch:(BOOL)isFullSearch {
    if (textSearch != self.textSearch) {
        // Ignore the document search instance
        if ([self.pdfController respondsToSelector:@selector(didFinishSearch:forTerm:searchResults:isFullSearch:)]) {
            [self.pdfController didFinishSearch:textSearch forTerm:searchTerm searchResults:searchResults isFullSearch:isFullSearch];
        }
        return;
    }
    
    LogTrace(@"Search for %@ finished.", searchTerm);
    LogTrace(@"%@",self.searchBar.text);
    if (!isFullSearch || ![searchTerm isEqualToString:self.searchBar.text]) {
        return; // ignore partial search updates
    }
    
    [self.searchIndicatorView stopAnimating];
    [self setSearchStatus:PSPDFSearchStatusFinished updateResults:YES];
    
    // relay
    if ([self.pdfController respondsToSelector:@selector(didFinishSearch:forTerm:searchResults:isFullSearch:)]) {
        [self.pdfController didFinishSearch:textSearch forTerm:searchTerm searchResults:searchResults isFullSearch:isFullSearch];
    }
}

- (void)didCancelSearch:(PSPDFTextSearch *)textSearch forTerm:(NSString *)searchTerm isFullSearch:(BOOL)isFullSearch
{
    if (textSearch != self.textSearch) {
        // Ignore the document search instance
        if ([self.pdfController respondsToSelector:@selector(didCancelSearch:forTerm:isFullSearch:)]) {
            [self.pdfController didCancelSearch:textSearch forTerm:searchTerm isFullSearch:isFullSearch];
        }
        return;
    }

    if (!isFullSearch || ![searchTerm isEqualToString:self.searchBar.text]) {
        return; // ignore partial search updates
    }
    
    LogTrace(@"Search for %@ cancelled.", searchTerm);
    [self setSearchStatus:PSPDFSearchStatusCancelled updateResults:YES];
    self.resultCountLabel.text = @"";
    
    // relay
    if ([self.pdfController respondsToSelector:@selector(didCancelSearch:forTerm:isFullSearch:)]) {
        [self.pdfController didCancelSearch:textSearch forTerm:searchTerm isFullSearch:isFullSearch];
    }
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row >= [self.searchResults count]) { // user pressed on search status cell
        return; 
    }
    
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone )
    {
        [self.pdfController dismissViewControllerAnimated:YES completion:NULL];
    }
    
    PSPDFSearchResult *searchResult = [self.searchResults objectAtIndex:indexPath.row];
    
    [self.pdfController setPage:searchResult.pageIndex animated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 70;
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSUInteger listCount = [self.searchResults count];    
    return listCount;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellID = @"PSPDFDescriptionTableViewCell";
    ADXDescriptionCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    
	if (cell == nil) {
        UINib *nib = [UINib nibWithNibName:@"ADXDescriptionCell" bundle:[NSBundle mainBundle]];
        cell = [[nib instantiateWithOwner:self options:nil] objectAtIndex:0];
    }
    
    PSPDFSearchResult *searchResult = [self.searchResults objectAtIndex:indexPath.row];
    [self updateResultCell:cell searchResult:searchResult];

    return cell;
}

- (void)updateResultCell:(ADXDescriptionCell *)cell searchResult:(PSPDFSearchResult *)searchResult
{
    if ([searchResult.previewText length])
    {
        NSString *searchKey = self.searchBar.text;
        NSString *previewText = searchResult.previewText;
        NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc]initWithString:previewText];
        NSRegularExpression *regEx = [NSRegularExpression regularExpressionWithPattern:searchKey
                                                                               options:NSRegularExpressionCaseInsensitive
                                                                                 error:nil];
        
        [regEx enumerateMatchesInString:previewText
                                options:0
                                  range:NSMakeRange(0, previewText.length)
                             usingBlock:^(NSTextCheckingResult *result, NSMatchingFlags flags, BOOL *stop)
        {
            if ( result && result.range.location != NSNotFound ){
                [attrString addAttribute:NSBackgroundColorAttributeName
                                   value:[UIColor yellowColor]
                                   range:result.range];
            }
        }];
        
        cell.textSampleLabel.attributedText = attrString;
    } else {
        cell.textSampleLabel.text = @"";
    }
    
    // do we have a page title here? only use outline if we have > 1 element (some documents just have page title, not what we want)
    NSString *outlineTitle = nil;
    if ([self.document.outlineParser.outline.children count] > 1) {
        outlineTitle = [self.document.outlineParser outlineElementForPage:searchResult.pageIndex exactPageOnly:NO].title;
        outlineTitle = [outlineTitle substringWithRange:NSMakeRange(0, MIN(25, [outlineTitle length]))]; // limit
    }
    
    NSString *pageTitle = [NSString stringWithFormat:PSPDFLocalize(@"Page %d"), searchResult.pageIndex+1];
    NSString *cellText = outlineTitle ? [NSString stringWithFormat:@"%@, %@", outlineTitle, pageTitle] : pageTitle;
    cell.pageInfoLabel.text = cellText;

}

#pragma mark - UISearchBarDelegate

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
     self.searchMode = ADXSearchModeCurrentPage;
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
     self.searchMode = ADXSearchModeCurrentPage;
    [self filterContentForSearchText:searchText scope:0];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *) searchBar
{
    self.searchMode = ADXSearchModeNone;
     // showsCancelButton is set for modal view
    [self.pdfController dismissViewControllerAnimated:YES completion:^{
       //FIXME: ADD SOME CODE HERE?
    }]; 
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    __weak ADXSearchViewController *weakSelf = self;

    self.searchMode = ADXSearchModeEntireDocument;
    [searchBar resignFirstResponder];
    
    [self.pdfController presentViewController:self animated:YES completion:^{
        [weakSelf refresh];
    }];
}

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar
{
    __weak ADXSearchViewController *weakSelf = self;

    if ( searchBar != self.searchBar && UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone )
    {
        [self.pdfController presentViewController:self animated:YES completion:^{
            [weakSelf.searchBar becomeFirstResponder];
        }];
        return NO;
    }
    
    return YES;
}

- (void)searchBarResultsListButtonClicked:(UISearchBar *)searchBar
{
    if ( searchBar != self.searchBar && UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone )
    {
        [self.pdfController presentViewController:self animated:YES completion:NULL];
    }
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar
{
    [self refresh];
}

#pragma mark - PSPDFViewControllerDelegate

- (void)pdfController:(ADXPDFViewController *)pdfController didShowPageView:(PSPDFPageView *)pageView
{
    if (self.searchMode != ADXSearchModeNone) {
        if ([pdfController.visiblePageNumbers count] > 0) {
            // Restart the document search, but not the local search
            [self.document.textSearch searchForString:self.searchBar.text];
        }
    }
}

- (NSArray*)menuItemsForSelectedText:(NSString *)selectedText atSuggestedTargetRect:(CGRect)rect inRect:(CGRect)textRect onPageView:(PSPDFPageView *)pageView;
{
    __weak ADXSearchViewController *weakSelf = self;
    NSArray* items = nil;
    
    if (selectedText && selectedText.length > 2 ){
        PSPDFMenuItem *searchItem = [[PSPDFMenuItem alloc] initWithTitle:NSLocalizedString(@"Search", nil) block:^{
            NSString *trimmedSearchText = PSPDFTrimString(selectedText);
            [weakSelf.pdfController presentViewController:weakSelf animated:YES completion:^{
                [weakSelf searchForString:trimmedSearchText];
                [pageView.selectionView discardSelection];
            }];
        } identifier:@"Search"];
        
        items = @[searchItem];
    }
    
    return items;
}

- (void)pdfController:(ADXPDFViewController *)controller selectedToolDidChanged:(UIViewController*)tool
{
    if (tool != self)
    {
        if( self.pdfController.presentedViewController == self )
        {
            [self.pdfController dismissViewControllerAnimated:YES completion:NULL];
        }
        [self filterContentForSearchText:nil scope:0];
        self.searchMode = ADXSearchModeNone;   
    }
    else
    {
        [self searchForString:self.searchBar.text];
    }
}

- (void) documentDidChanged:(PSPDFDocument *)document
{
    self.document = document;
}

#pragma mark - Collection View

- (void)updatePageControl
{
    // Ask the layout how many pages are displayed
    ADXCollectionViewHorizontalLayout *layout = (ADXCollectionViewHorizontalLayout *)self.thumbnailCollectionView.collectionViewLayout;
    self.thumbPageControl.numberOfPages = layout.numberOfPages;
    
    // Calculate the displayed page number
    int pageNum = (self.thumbnailCollectionView.contentOffset.x + (self.thumbnailCollectionView.bounds.size.width/2)) / self.thumbnailCollectionView.bounds.size.width;
    self.thumbPageControl.currentPage = pageNum;
}

- (void)configureThumbnailCollectionView
{
    ADXCollectionViewHorizontalLayout *layout = (ADXCollectionViewHorizontalLayout *)self.thumbnailCollectionView.collectionViewLayout;
    layout.itemSize = CGSizeMake(135, 166);
    
    self.thumbnailCollectionView.opaque = NO;
    self.thumbnailCollectionView.backgroundColor = [UIColor clearColor];
    self.thumbnailCollectionView.allowsSelection = YES;
    
    UINib *nib = [UINib nibWithNibName:@"ADXPageThumbnailView" bundle:nil];
    [self.thumbnailCollectionView registerNib:nib
                   forCellWithReuseIdentifier:@"thumbnail"];
}

#pragma mark - UICollectionViewDelegate

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    [self updatePageControl];
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSArray *sortedKeys = [[self.searchResultsByPage allKeys] sortedArrayUsingSelector:@selector(compare:)];
    NSNumber *pageNumber = [sortedKeys objectAtIndex:indexPath.item];

    [self.pdfController setPage:pageNumber.intValue animated:YES];
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.searchResultsByPage.allKeys.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSArray *sortedKeys = [[self.searchResultsByPage allKeys] sortedArrayUsingSelector:@selector(compare:)];
    NSNumber *pageNumber = [sortedKeys objectAtIndex:indexPath.item];

    ADXPageThumbnailView *cell = [self.thumbnailCollectionView dequeueReusableCellWithReuseIdentifier:@"thumbnail" forIndexPath:indexPath];
    cell.pageIndex = pageNumber.intValue;
    cell.pdfDocument = self.pdfController.document;
    
    int numOnPage = [[self.searchResultsByPage objectForKey:[sortedKeys objectAtIndex:indexPath.item]] count];
    NSString *metaInfo = nil;
    if (numOnPage == 1) {
        metaInfo = NSLocalizedString(@"1 match", nil);
    } else {
        metaInfo = [NSString stringWithFormat:NSLocalizedString(@"%d matches", nil), numOnPage];
    }
    cell.metaInfo = metaInfo;
    return cell;
}

@end
