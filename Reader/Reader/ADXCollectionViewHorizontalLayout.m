//
//  ADXCollectionViewHorizontalLayout.m
//  Reader
//
//  Created by Steve Tibbett on 2013-01-09.
//  Copyright (c) 2013 ActivDox Inc. All rights reserved.
//

#import "ADXCollectionViewHorizontalLayout.h"

@interface ADXCollectionViewHorizontalLayout()
@property (nonatomic) int itemsPerRow;
@property (nonatomic) int rowsPerPage;
@property (nonatomic) CGSize cellSize;
@end

@implementation ADXCollectionViewHorizontalLayout
@dynamic numberOfPages;

- (id)init
{
    self = [super init];
    if (self) {
        self.minimumOuterMargin = 20.0;
        self.minimumItemSpacing = 10.0;
    }
    return self;
}

- (void)prepareLayout
{
    CGSize size = self.collectionView.bounds.size;
    
    self.itemsPerRow = (size.width - (self.minimumOuterMargin*2)) / (self.itemSize.width - self.minimumItemSpacing);
    self.rowsPerPage = (size.height - (self.minimumOuterMargin*2)) / (self.itemSize.height - self.minimumItemSpacing);
    self.cellSize = CGSizeMake((self.collectionView.bounds.size.width - (self.minimumOuterMargin * 2)) / self.itemsPerRow,
                               (self.collectionView.bounds.size.height - (self.minimumOuterMargin * 2)) / self.rowsPerPage);
}

- (CGSize)collectionViewContentSize
{
    CGSize size = CGSizeMake(self.collectionView.bounds.size.width * self.numberOfPages, self.collectionView.bounds.size.height);
    return size;
}

- (NSArray *)layoutAttributesForElementsInRect:(CGRect)rect
{
    NSMutableArray *array = [NSMutableArray array];
    
    // Calculate which pages intersect the rect, and return the items on those pages
    int startPage = MAX(0, floor(rect.origin.x / self.collectionView.bounds.size.width));
    int endPage = ceil((rect.origin.x + rect.size.width-1) / self.collectionView.bounds.size.width);
    
    int itemsPerPage = self.itemsPerRow * self.rowsPerPage;
    int maxItemToAdd = ((endPage+1) * itemsPerPage)-1;
    int endItem = MIN([self.collectionView.dataSource collectionView:self.collectionView numberOfItemsInSection:0], maxItemToAdd);

    for (int i=startPage*itemsPerPage; i<endItem; i++) {
        [array addObject:[self layoutAttributesForItemAtIndexPath:[NSIndexPath indexPathForItem:i inSection:0]]];
    }
    return array;
}

- (UICollectionViewLayoutAttributes *)layoutAttributesForItemAtIndexPath:(NSIndexPath *)indexPath
{
    int itemsPerPage = self.itemsPerRow * self.rowsPerPage;
    int page = indexPath.item / itemsPerPage;
    
    UICollectionViewLayoutAttributes *attributes = [UICollectionViewLayoutAttributes layoutAttributesForCellWithIndexPath:indexPath];
    attributes.size = self.itemSize;

    int itemIndexOnPage = (indexPath.item - (itemsPerPage * page));

    int pageRow = 0;
    int pageColumn = 0;
    if (self.columnSort) {
        pageRow = itemIndexOnPage % self.rowsPerPage;
        pageColumn = itemIndexOnPage / self.rowsPerPage;
    } else {
        pageRow = itemIndexOnPage / self.itemsPerRow;
        pageColumn = itemIndexOnPage % self.itemsPerRow;
    }
    
    attributes.center = CGPointMake(roundf((self.collectionView.bounds.size.width * page) + self.minimumOuterMargin + (pageColumn * self.cellSize.width) + (self.cellSize.width/2)),
                                    roundf(self.minimumOuterMargin + (pageRow * self.cellSize.height) + (self.cellSize.height/2)));

    return attributes;
                                    
}

- (int)numberOfPages
{
    int numItems = [self.collectionView.dataSource collectionView:self.collectionView numberOfItemsInSection:0];
    int itemsPerPage = self.numberOfItemsPerPage;
    if (itemsPerPage > 0) {
        int numPages = ((numItems + (itemsPerPage - 1)) / (itemsPerPage));
        return numPages;
    } else {
        return 0;
    }
}

- (int)numberOfItemsPerPage
{
    return self.itemsPerRow * self.rowsPerPage;
}

- (BOOL)shouldInvalidateLayoutForBoundsChange:(CGRect)newBounds
{
    return YES;
}
@end
