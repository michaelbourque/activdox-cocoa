//
//  ADXCollectionListCellSmall.h
//  Reader
//
//  Created by Gavin McKenzie on 2013-04-28.
//  Copyright (c) 2013 ActivDox Inc. All rights reserved.
//

#import "ADXCollectionViewCell.h"

@interface ADXCollectionListCellSmall : ADXCollectionViewCell
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *titleLeftConstraint;
@end
