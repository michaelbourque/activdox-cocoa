//
//  ADXPDFViewMenuPopoverController.h
//  Reader
//
//  Created by Gavin McKenzie on 2013-01-18.
//  Copyright (c) 2013 ActivDox Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

extern NSString * const kADXPDFViewMenuPopoverOptionID;
extern NSString * const kADXPDFViewMenuPopoverOptionLabel;
extern NSString * const kADXPDFViewMenuPopoverOptionIcon;

@class ADXPDFViewMenuPopoverController;

@protocol ADXPDFViewMenuPopoverDelegate <NSObject>
- (void)didSelectPDFViewMenuOption:(NSDictionary *)option;
@end

@interface ADXPDFViewMenuPopoverController : UITableViewController
@property (weak, nonatomic) UIPopoverController *parentPopoverController;
@property (weak, nonatomic) id<ADXPDFViewMenuPopoverDelegate> actionDelegate;
+ (UIPopoverController *)popoverControllerWithOptions:(NSArray *)options actionDelegate:(id)delegate;
@end
