//
//  ADXCollectionsGridCell.m
//  Reader
//
//  Created by Gavin McKenzie on 12-06-20.
//  Copyright (c) 2012 ActivDox Inc. All rights reserved.
//

#import "ADXCollectionsGridCell.h"


@interface ADXCollectionsGridCell ()
@property (strong, nonatomic) UINib *nib;
@end

@implementation ADXCollectionsGridCell
@synthesize nib = _nib;
@synthesize imageView = _imageView;
@synthesize titleLabel = _titleLabel;
@synthesize firstMetaLabel = _firstMetaLabel;

- (void)setHighlighted:(BOOL)highlighted
{
    if (highlighted) {
        self.imageView.backgroundColor = [UIColor blackColor];
        self.imageView.alpha = 0.4f;
    } else {
        self.imageView.backgroundColor = [UIColor clearColor];
        self.imageView.alpha = 1.0f;
    }
}

@end
