//
//  ADXBaseDocListViewController.h
//  Reader
//
//  Created by Gavin McKenzie on 2013-04-13.
//  Copyright (c) 2013 ActivDox Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ActivDoxCore.h"

@protocol ADXBaseDocListActionsDelegate <NSObject>
@end

extern NSString * const kADXDocListCellIdentifier;
extern NSString * const kADXBaseDocListAddRowCellIdentifier;

@interface ADXBaseDocListViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>
+ (UIPopoverController *)popoverControllerWithTitle:(NSString *)title allowEditing:(BOOL)allowEditing startInEditMode:(BOOL)startInEditMode allowMultiSelect:(BOOL)allowMultiSelect allowEditMultiSelect:(BOOL)allowEditMultiSelect actionDelegate:(id)actionDelegate;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) UIPopoverController *parentPopoverController;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (strong, nonatomic) NSFetchedResultsController *searchFetchedResultsController;
@property (strong, nonatomic) NSFetchedResultsController *listFetchedResultsController;
@property (strong, nonatomic) NSString *accountID;
@property (assign, nonatomic) BOOL allowEditing;
@property (assign, nonatomic) BOOL startInEditMode;
@property (assign, nonatomic) BOOL allowMultiSelect;
@property (assign, nonatomic) BOOL allowEditMultiSelect;
@property (strong, nonatomic) NSString *requestedTitle;
@property (weak, nonatomic) id<ADXBaseDocListActionsDelegate> actionDelegate;

- (void)configureTableView;
- (void)registerCellNibs;
- (void)configureCell:(UITableViewCell *)cell indexPath:(NSIndexPath *)indexPath tableView:(UITableView *)tableView;
- (id)objectAtIndexPath:indexPath tableView:(UITableView *)tableView;

- (void)configureNavBar;
- (void)updateNavBar;
- (void)editingStateDidChange:(BOOL)editing;
- (UITableViewCell *)cellForAddRow;
- (NSString *)messageForAddRowCell;

- (NSPredicate *)predicateForListFetchedResultsController;
- (NSSortDescriptor *)sortDescriptorForListFetchedResultsController;
- (NSPredicate *)predicateForSearchFetchedResultsController;
- (NSSortDescriptor *)sortDescriptorForSearchFetchedResultsController;

- (void)saveChanges;

@end
