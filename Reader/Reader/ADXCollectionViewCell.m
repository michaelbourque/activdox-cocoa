//
//  ADXCollectionViewCell.m
//  Reader
//
//  Created by Gavin McKenzie on 2013-04-28.
//  Copyright (c) 2013 ActivDox Inc. All rights reserved.
//

#import "ADXCollectionViewCell.h"
#import "ADXAppDelegate.h"

#import <QuartzCore/QuartzCore.h>
#import <AVFoundation/AVFoundation.h>

@interface ADXCollectionViewCell ()
@property (assign, nonatomic) long long cachedTotalBytesExpected;
@property (assign, nonatomic) long long cachedTotalBytesRead;
@property (assign, nonatomic) BOOL didAddObservers;
@property (assign, nonatomic) BOOL progressFinished;
@property (assign, nonatomic) BOOL reachabilityOk;
@property (strong, nonatomic) NSManagedObjectID *documentThumbnailID;
@property (nonatomic) CGSize originalImageViewSize;
@property (strong, nonatomic) NSLayoutConstraint *imageViewWidthConstraint;
@property (strong, nonatomic) NSLayoutConstraint *imageViewHeightConstraint;

- (void)handleProgressNotificationFinished:(NSNotification *)notification;
- (void)handleProgressNotificationUpdate:(NSNotification *)notification;
@end

@implementation ADXCollectionViewCell

#pragma mark - Initialization and Destruction

- (void)dealloc
{
    [self removeDocumentObservers];
}

#pragma mark - Notification Setup

- (void)addDocumentObservers
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleProgressNotificationUpdate:) name:kADXServiceNotificationProgressUpdate object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleProgressNotificationStarted:) name:kADXServiceNotificationProgressStarted object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleProgressNotificationFinished:) name:kADXServiceNotificationProgressUpdateFinished object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleThumbnailDownloadedNotification:) name:kADXServiceDocumentThumbnailDownloadedNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleThumbnailDownloadedNotification:) name:kADXServiceDocumentThumbnailNotDownloadedNotification object:nil];
    
    self.didAddObservers = YES;
}

- (void)removeDocumentObservers
{
    if (!self.didAddObservers || !_documentEntity)
        return;
    
    @try {
        [[NSNotificationCenter defaultCenter] removeObserver:self name:kADXServiceNotificationProgressUpdate object:nil];
        [[NSNotificationCenter defaultCenter] removeObserver:self name:kADXServiceNotificationProgressStarted object:nil];
        [[NSNotificationCenter defaultCenter] removeObserver:self name:kADXServiceNotificationProgressUpdateFinished object:nil];
        [[NSNotificationCenter defaultCenter] removeObserver:self name:kADXServiceDocumentThumbnailDownloadedNotification object:nil];
        [[NSNotificationCenter defaultCenter] removeObserver:self name:kADXServiceDocumentThumbnailNotDownloadedNotification object:nil];
    }
    @catch (NSException *exception) {
        // Do nothing.
    }
}

- (void)updateReachabilityObservers
{
    @try {
        [[NSNotificationCenter defaultCenter] removeObserver:self name:kADXServiceNotificationReachabilityChanged object:nil];
    }
    @catch (NSException *exception) {
        // Do nothing.
    }
    
    if (self.trackReachabilityChanges) {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleReachabilityChanged:) name:kADXServiceNotificationReachabilityChanged object:nil];
    }
}

#pragma mark - Notification Handling

- (void)handleProgressNotificationStarted:(NSNotification *)notification
{
    NSString *documentID = [notification.userInfo objectForKey:ADXDocumentAttributes.id];
    __weak ADXCollectionViewCell *weakSelf = self;
    self.progressFinished = NO;
    
    if ([documentID isEqualToString:self.documentEntity.id]) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [weakSelf refresh];
        });
    }
}

- (void)handleProgressNotificationFinished:(NSNotification *)notification
{
    NSString *documentID = [notification.userInfo objectForKey:ADXDocumentAttributes.id];
    __weak ADXCollectionViewCell *weakSelf = self;
    self.progressFinished = YES;
    
    if ([documentID isEqualToString:self.documentEntity.id]) {
        int64_t delayInSeconds = 1.0;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            [weakSelf hideProgressIndicators];
            [weakSelf refresh];
        });
    }
}

- (void)handleProgressNotificationUpdate:(NSNotification *)notification
{
    NSString *documentID = [notification.userInfo objectForKey:ADXDocumentAttributes.id];
    if (![documentID isEqualToString:self.documentEntity.id]) {
        return;
    }
    
    __weak ADXCollectionViewCell *weakSelf = self;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        NSNumber *newReadBytes = [notification.userInfo objectForKey:kADXServiceNotificationProgressUpdateUserInfoBytesReadKey];
        weakSelf.cachedTotalBytesRead = [newReadBytes longLongValue];
        
        NSNumber *newExpectedBytes = [notification.userInfo objectForKey:kADXServiceNotificationProgressUpdateUserInfoBytesExpectedKey];
        weakSelf.cachedTotalBytesExpected = [newExpectedBytes longLongValue];
                
        [weakSelf updateActivityIndicators];
    });
}

- (void)handleReachabilityChanged:(NSNotification *)notification
{
    __weak ADXCollectionViewCell *weakSelf = self;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [weakSelf updateReachability];
        [weakSelf refresh];
    });
}

- (void)handleThumbnailDownloadedNotification:(NSNotification *)notification
{
    NSString *documentID = [notification.userInfo valueForKey:kADXServiceNotificationPayload];
    if (![documentID isEqualToString:self.documentEntity.id]) {
        return;
    }
    
    __weak ADXCollectionViewCell *weakSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        [weakSelf refresh];
    });
}

#pragma mark - Accessors

- (void)setDocumentEntity:(ADXDocument *)documentEntity
{
    //    NSLog(@"setDocumentEntity on title: %@", documentEntity.title);
    
    @synchronized(self) {
        if (documentEntity == _documentEntity) {
            return;
        }
        
        _documentEntity = nil;
        
        [self resetState];
        [self removeDocumentObservers];
        
        if (documentEntity) {
            _documentEntity = documentEntity;
            [self addDocumentObservers];
            
            __weak ADXCollectionViewCell *weakSelf = self;
            dispatch_async(dispatch_get_main_queue(), ^{
                [weakSelf updateReachability];
                [weakSelf refresh];
            });
        } else {
            [self hideProgressIndicators];
        }
        
    }
}

- (void)setTrackReachabilityChanges:(BOOL)trackReachabilityChanges
{
    _trackReachabilityChanges = trackReachabilityChanges;
    [self updateReachabilityObservers];
    [self updateReachability];
    
    __weak ADXCollectionViewCell *weakSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        [weakSelf refresh];
    });
}

#pragma mark - Helpers

- (void)prepareImageView
{
    self.imageView = [[UIImageView alloc] init];
    self.imageView.translatesAutoresizingMaskIntoConstraints = NO;
    self.imageView.contentMode = UIViewContentModeScaleAspectFit;
    self.imageView.accessibilityLabel = self.documentEntity.title;
    self.imageView.layer.borderColor = [UIColor colorWithWhite:0.7 alpha:0.5].CGColor;
    self.imageView.layer.borderWidth = 2.0;
    [self addSubview:self.imageView];
    
    self.ribbonView = [[UIImageView alloc] initWithImage:[self ribbonImage]];
    self.ribbonView.translatesAutoresizingMaskIntoConstraints = NO;
    [self addSubview:self.ribbonView];
    
    self.progressBar = [[UIProgressView alloc] initWithProgressViewStyle:UIProgressViewStyleDefault];
    self.progressBar.translatesAutoresizingMaskIntoConstraints = NO;
    self.progressBar.hidden = YES;
    [self addSubview:self.progressBar];
    
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.imageView
                                                     attribute:NSLayoutAttributeCenterX
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:self.spinner
                                                     attribute:NSLayoutAttributeCenterX
                                                    multiplier:1.0
                                                      constant:0]];
    
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.imageView
                                                     attribute:NSLayoutAttributeCenterY
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:self.spinner
                                                     attribute:NSLayoutAttributeCenterY
                                                    multiplier:1.0
                                                      constant:0]];
    
    // Ribbon
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.ribbonView
                                                     attribute:NSLayoutAttributeLeft
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:self.imageView
                                                     attribute:NSLayoutAttributeLeft
                                                    multiplier:1.0
                                                      constant:0]];
    
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.ribbonView
                                                     attribute:NSLayoutAttributeTop
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:self.imageView
                                                     attribute:NSLayoutAttributeTop
                                                    multiplier:1.0
                                                      constant:0]];
    
    // Progress bar
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.progressBar
                                                     attribute:NSLayoutAttributeLeft
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:self.imageView
                                                     attribute:NSLayoutAttributeLeft
                                                    multiplier:1.0
                                                      constant:10]];
    
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.progressBar
                                                     attribute:NSLayoutAttributeRight
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:self.imageView
                                                     attribute:NSLayoutAttributeRight
                                                    multiplier:1.0
                                                      constant:-10]];
    
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.progressBar
                                                     attribute:NSLayoutAttributeCenterY
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:self.imageView
                                                     attribute:NSLayoutAttributeBottom
                                                    multiplier:1.0
                                                      constant:-16]];
    
    self.imageViewWidthConstraint = [NSLayoutConstraint constraintWithItem:self.imageView
                                                                 attribute:NSLayoutAttributeWidth
                                                                 relatedBy:NSLayoutRelationEqual
                                                                    toItem:nil
                                                                 attribute:NSLayoutAttributeNotAnAttribute
                                                                multiplier:1.0
                                                                  constant:60];
    
    self.imageViewHeightConstraint = [NSLayoutConstraint constraintWithItem:self.imageView
                                                                  attribute:NSLayoutAttributeHeight
                                                                  relatedBy:NSLayoutRelationEqual
                                                                     toItem:nil
                                                                  attribute:NSLayoutAttributeNotAnAttribute
                                                                 multiplier:1.0
                                                                   constant:100];

    [self addConstraints:@[ self.imageViewWidthConstraint, self.imageViewHeightConstraint ]];
    
    [self.spinner.superview bringSubviewToFront:self.spinner];
    
}

- (void)updateReachability
{
    if (!self.trackReachabilityChanges) {
        self.reachabilityOk = YES;
        return;
    }
    
    ADXV2Service *service = [ADXV2Service serviceForManagedObject:self.documentEntity];
    if ([service networkReachable] && [service serverReachable]) {
        self.reachabilityOk = YES;
    } else {
        self.reachabilityOk = NO;
    }
}

- (NSString *)publishedDateAsString
{
    return [self.documentEntity publishedDateDisplayLabel:YES];
}

- (NSString *)versionString
{
    NSString *ver = [self.documentEntity versionDisplayLabel];
    
    ADXDocument *recent = [ADXDocument mostRecentlyOpenedDocumentWithID:self.documentEntity.id
                                                              accountID:[ADXAppDelegate sharedInstance].currentAccount.id
                                                                context:self.documentEntity.managedObjectContext];
    
    if (recent != nil) {
        if (recent.versionValue != self.documentEntity.versionValue) {
            ver = [ver stringByAppendingFormat:@" \u2014 ☕ %d", (int)recent.versionValue];
        }
    }
    
    return ver;
}

- (void)hideProgressIndicators
{
    [self.spinner stopAnimating];
    [self.spinner setHidden:YES];
    [self.progressBar setHidden:YES];
}

#pragma mark - View Management

- (void)removeFromSuperview
{
    @try {
        [[NSNotificationCenter defaultCenter] removeObserver:self name:kADXServiceNotificationReachabilityChanged object:nil];
    }
    @catch (NSException *exception) {
        // Do nothing.
    }
    
    [super removeFromSuperview];
}

- (void)resetState
{
    self.progressFinished = NO;
    self.reachabilityOk = YES;
    
    [self.titleLabel setText:nil];
    [self.firstMetaLabel setText:nil];
    [self.secondMetaLabel setText:nil];
    [self.thirdMetaLabel setText:nil];
    [self.descriptionLabel setText:nil];

    [self setCachedTotalBytesExpected:0];
    [self setCachedTotalBytesRead:0];
    
    [self.ribbonView setHidden:YES];
    [self.progressBar setHidden:YES];
    [self.documentNewBadge setHidden:YES];
    [self.changeBadge setHidden:YES];
    [self.spinner stopAnimating];
    [self.spinner setHidden:YES];
}

- (void)prepareForReuse
{
    [super prepareForReuse];
    
    self.imageView.image = nil;
    
    [self setDocumentEntity:nil];
}

- (void)refresh
{
    if (self.documentEntity) {
        ADXDocumentThumbnail *documentThumbnail = self.documentEntity.thumbnail;
        if (documentThumbnail && !IsEmpty(documentThumbnail.data)) {
            // Is the thumbnail different than the current thumbnail we're displaying?
            if (![documentThumbnail.objectID.URIRepresentation isEqual:self.documentThumbnailID.URIRepresentation]) {
                NSString *key = documentThumbnail.objectID.URIRepresentation.absoluteString;
                
                // Is the thumbnail image in the cache?
                UIImage *image = [self.imageCache imageForKey:key];
                if (!image) {
                    // No, load it and cache it
                    NSData *thumbnailData = documentThumbnail.data;
                    image = [UIImage imageWithData:thumbnailData];
                    [self.imageCache addImage:image forKey:key];
                }

                self.ribbonView.image = [self ribbonImage];
                self.ribbonView.hidden = NO;

                [self presentThumbnailImage:image];
            }
        } else {
            [self presentThumbnailImage:[UIImage imageNamed:@"doc_background"]];
            // Thumbnail missing -- attempt to fetch
            ADXV2Service *service = [ADXV2Service serviceForManagedObject:self.documentEntity];
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                [service downloadThumbnailForDocument:self.documentEntity.id versionID:self.documentEntity.version completion:nil];
            });
        }
        
        self.titleLabel.text = self.documentEntity.title;
        if (self.documentEntity.accessible) {
            self.firstMetaLabel.text = [self publishedDateAsString];
            self.secondMetaLabel.text = [self versionString];
            self.thirdMetaLabel.text = self.documentEntity.author.displayName;
            self.descriptionLabel.text = self.documentEntity.documentDescription;
        } else if ([self.documentEntity.status isEqualToString:ADXDocumentStatus.inaccessible]){
            self.firstMetaLabel.text = @"Document Unavailable";
            self.descriptionLabel.text = @"Document unavailable from current location";
            [self hideProgressIndicators];
        }
        
        
        else {
            self.firstMetaLabel.text = @"Document Unavailable";
            self.descriptionLabel.text = nil;
            [self hideProgressIndicators];
        }
        
        //        NSLog(@"REFRESH entity title: %@ ", self.documentEntity.title);
    } else {
        [self.titleLabel setText:nil];
        [self.firstMetaLabel setText:nil];
        [self.secondMetaLabel setText:nil];
        [self.thirdMetaLabel setText:nil];
        [self.descriptionLabel setText:nil];
        self.imageView.alpha = 0.0f;
    }
    
    [self updateBadge];
}

- (void)setHighlighted:(BOOL)highlighted
{
    if (highlighted) {
        self.imageView.backgroundColor = [UIColor blackColor];
        self.imageView.alpha = 0.4f;
    } else {
        self.imageView.backgroundColor = [UIColor clearColor];
        self.imageView.alpha = 1.0f;
    }
}

#pragma mark - Thumbnail

- (UIImage *)thumbnailImage
{
    return self.imageView.image;
}

- (void)presentThumbnailImage:(UIImage *)thumbnail
{
    if (thumbnail == self.imageView.image) {
        return;
    }
    
    if (thumbnail != self.imageView.image) {
        if (self.imageView == nil) {
            [self prepareImageView];
        }
        
        self.imageView.image = thumbnail;
        self.imageView.accessibilityLabel = self.documentEntity.title;

        CGSize thumbnailSize = thumbnail.size;
        CGRect actualThumbnailFrame = AVMakeRectWithAspectRatioInsideRect(thumbnailSize, CGRectMake(0, 0, self.bounds.size.height * 0.6, self.bounds.size.height * 0.8));

        self.imageViewWidthConstraint.constant = actualThumbnailFrame.size.width;
        self.imageViewHeightConstraint.constant = actualThumbnailFrame.size.height;
    }
}

#pragma mark - Ribbon

- (void)updateActivityIndicators
{
    if (!self.documentEntity.accessible) {
        [self hideProgressIndicators];
        return;
    }
    
    if (self.cachedTotalBytesExpected > 0 && self.cachedTotalBytesRead > 0) {
        // Show progress with the bar
        if (!self.spinner.hidden) {
            [self.spinner setHidden:YES];
        }
        if (self.progressBar.hidden) {
            [self.progressBar setHidden:NO];
        }
        float percentDone = ((float)self.cachedTotalBytesRead) / ((float)self.cachedTotalBytesExpected);
        self.progressBar.progress = percentDone;
    } else {
        // Show progress with the spinner
        if (!self.progressBar.hidden) {
            [self.progressBar setHidden:YES];
        }
        if (self.spinner.hidden) {
            [self.spinner setHidden:NO];
        }
        if (![self.spinner isAnimating]) {
            [self.spinner startAnimating];
        }
    }
}

- (BOOL)shouldShowRibbon
{
    BOOL result = NO;
    
    if (!self.documentEntity.accessible) {
        // If the document is inaccessible, we always want to display a thumbnail, event if we can't load the thumbnail image
        result = YES;
    } else if (self.documentEntity.versionIsLatestValue) {
        result = YES;
    }
    return result;
}

- (UIImage *)ribbonImage
{
    UIImage *result = nil;
    NSString *ribbonColour = nil;
    NSString *ribbonImageName = nil;
    
    if (self.documentEntity.metaDocument.doNotAutomaticallyDownloadContentValue || self.documentEntity.downloadedValue == NO || IsEmpty(self.documentEntity.content.data)) {
        ribbonColour = @"yellow";
    } else if ([self.documentEntity.status isEqualToString:ADXDocumentStatus.accessible]) {
        ribbonColour = @"green";
    } else if ([self.documentEntity.status isEqualToString:ADXDocumentStatus.revoked]) {
        ribbonColour = @"red";
    } else if ([self.documentEntity.status isEqualToString:ADXDocumentStatus.inaccessible]){
        ribbonColour = @"red";
    }
    else {
        ribbonColour = @"red";
    }
    
    if (self.reachabilityOk) {
        ribbonImageName = [NSString stringWithFormat:@"%@_corner.png", ribbonColour];
    } else {
        ribbonImageName = [NSString stringWithFormat:@"%@_corner_offline.png", ribbonColour];
    }
    
    result = [UIImage imageNamed:ribbonImageName];
    return result;
}

#pragma mark - Badge

- (UIImage *)changeBadgeBackgroundImage
{
    static UIImage *changeBadgeBackgroundImage = nil;
    
    if (!changeBadgeBackgroundImage) {
        changeBadgeBackgroundImage = [[UIImage imageNamed:@"changes"] resizableImageWithCapInsets:UIEdgeInsetsMake(0.0f, 14.0f, 0.0f, 14.0f)];
    }
    return changeBadgeBackgroundImage;
}

- (UIImage *)changeBadgeReadBackgroundImage
{
    static UIImage *changeBadgeReadBackgroundImage = nil;
    
    if (!changeBadgeReadBackgroundImage) {
        changeBadgeReadBackgroundImage = [[UIImage imageNamed:@"changes_read"] resizableImageWithCapInsets:UIEdgeInsetsMake(0.0f, 14.0f, 0.0f, 14.0f)];
    }
    return changeBadgeReadBackgroundImage;
}

- (BOOL)shouldShowNewBadge
{
    BOOL result = NO;
    if (self.documentEntity.accessible &&
        self.documentEntity.versionIsLatestValue &&
        !self.documentEntity.mostRecentOpenedDate &&
        [self.documentEntity.changeCount integerValue] == 0) {
        
        result = YES;
    }
    return result;
}

- (BOOL)shouldShowChangeBadge
{
    BOOL result = NO;
    if (self.documentEntity.accessible &&
        !self.documentEntity.mostRecentOpenedDate &&
        [self.documentEntity.changeCount integerValue] > 0) {
        
        result = YES;
    }
    return result;
}

- (BOOL)shouldShowChangeBadgeRead
{
    BOOL result = NO;
    if (self.documentEntity.accessible &&
        self.documentEntity.mostRecentOpenedDate &&
        [self.documentEntity.changeCount integerValue] > 0) {
        
        result = YES;
    }
    return result;
}

- (NSString *)badgeText
{
    NSString *result = nil;
    if ([self shouldShowChangeBadge] || [self shouldShowChangeBadgeRead]) {
        result = [self.documentEntity.changeCount stringValue];
    } else if ([self shouldShowNewBadge]) {
        result = nil;
    }
    return result;
}

- (void)updateBadge
{
    if ([self shouldShowNewBadge]) {
        [self.documentNewBadge setHidden:NO];
        [self.changeBadge setHidden:YES];
    } else if ([self shouldShowChangeBadge]) {
        [self.documentNewBadge setHidden:YES];
        [self.changeBadge setBackgroundImage:self.changeBadgeBackgroundImage forState:UIControlStateNormal];
        [self.changeBadge setTitle:self.badgeText forState:UIControlStateNormal];
//        [self.changeBadge sizeToFit];
        [self.changeBadge setHidden:NO];
    } else if ([self shouldShowChangeBadgeRead]) {
        [self.documentNewBadge setHidden:YES];
        [self.changeBadge setBackgroundImage:self.changeBadgeReadBackgroundImage forState:UIControlStateNormal];
        [self.changeBadge setTitle:self.badgeText forState:UIControlStateNormal];
//        [self.changeBadge sizeToFit];
        [self.changeBadge setHidden:NO];
    } else {
        [self.documentNewBadge setHidden:YES];
        [self.changeBadge setHidden:YES];
    }
    
    [self.changeBadge.superview bringSubviewToFront:self.changeBadge];
}

@end
