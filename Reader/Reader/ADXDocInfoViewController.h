//
//  ADXDocInfoViewController.h
//  Reader
//
//  Created by Gavin McKenzie on 2012-09-13.
//  Copyright (c) 2012 ActivDox Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ActivDoxCore.h"
#import "ADXDocActionsDelegate.h"


@interface ADXDocInfoViewController : UIViewController
@property (strong, nonatomic) ADXDocument *document;
@property (strong, nonatomic) UIImage *thumbnail;

@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *authorLabel;
@property (weak, nonatomic) IBOutlet UILabel *versionLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *authorMessageLabel;
@property (weak, nonatomic) IBOutlet UILabel *publicLabel;
@property (weak, nonatomic) IBOutlet UILabel *errorMessageLabel;
@property (strong, nonatomic) IBOutlet UILabel *unavailableLabel;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) id<ADXDocActionsDelegate> actionDelegate;
@property (assign, nonatomic) BOOL hideShareButton;
+ (UIPopoverController *)popoverControllerWithDocument:(ADXDocument *)document thumbnail:(UIImage *)thumbnail actionDelegate:(id)delegate;
@end

