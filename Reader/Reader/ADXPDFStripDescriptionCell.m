//
//  ADXPDFStripDescriptionCell.m
//  Reader
//
//  Created by Steve Tibbett on 2013-02-11.
//  Copyright (c) 2013 ActivDox Inc. All rights reserved.
//

#import "ADXPDFStripDescriptionCell.h"

@interface ADXPDFStripDescriptionCell ()
@end

@implementation ADXPDFStripDescriptionCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
