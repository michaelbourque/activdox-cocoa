//
//  ADXAnnotationViewController.m
//  PSPDFCatalog
//
//  Copyright (c) 2012 Peter Steinberger. All rights reserved.
//

#import "ADXAnnotationViewController.h"
#import "ADXDescriptionCell.h"
#import "PSPDFWord.h"
#import "ADXNoteAnnotation.h"
#import "ADXAnnotationUtil.h"
#import "ADXHighlightAnnotation.h"
#import "ADXAppDelegate.h"
#import "PSPDFNoteAnnotationView.h"
#import "ADXCollectionViewHorizontalLayout.h"
#import "PSPDFTextBlock.h"
#import "PSPDFGlyph.h"
#import "PSPDFViewController+Internal.h"
#import "ADXAnnotationFilterTableViewController.h"
#import "ADXAnnotationFilter.h"

#define kThumbnailSizeWidth 135
#define kThumbnailSizeHeight 165
#define kThumbnailForPageLanscape 7
#define kThumbnailForPagePortrait 5

typedef void(^ADXAnnotationOperationPerformBlock)();

// 25% of the 44 pixel minimum tap target size - if you don't move by at least this amount,
// then a tap with the highlighter is treated as a tap and not a highlight gesture
#define kMinimumHighlightSize 11

#define KDescriptionFont [UIFont systemFontOfSize:13]
#define KNoteTextFont [UIFont fontWithName:@"Noteworthy-Light" size:13]
#define KHighlightColor [UIColor colorWithRed:0 green:.7 blue:.36 alpha:12]

@interface ADXAnnotationViewController ()

@property (nonatomic, strong)   PSPDFDocument           *document;
@property (nonatomic, weak)   ADXPDFViewController    *pdfController;
@property (nonatomic, strong)   PSPDFSelectionView      *highlightView;

@property (nonatomic, strong)   NSMutableDictionary     *pagesWithAnnotations;
@property (nonatomic, strong)   NSArray                 *sortedPageIndex;

@property (weak, nonatomic) IBOutlet UICollectionView *thumbnailCollectionView;

@property (nonatomic, weak)     IBOutlet UITableView            *detailView;
@property (nonatomic, weak)     IBOutlet UIPageControl          *thumbPageControl;
@property (nonatomic, weak)     IBOutlet UILabel                *noResultFound;

@property (weak, nonatomic) IBOutlet UIBarButtonItem *toggleStripsButton;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *toggleThumbnailsButton;

@property (nonatomic, weak)     IBOutlet UILabel                *annotationsCount;
@property (nonatomic, strong)   IBOutletCollection(UIBarButtonItem) NSArray *buttonItems;
@property (nonatomic, strong) NSAttributedString                *highlightedTextIndicator;

@property (strong, nonatomic) UIPopoverController *currentPopover;

@end

@implementation ADXAnnotationViewController

#pragma mark - NSObject

- (id)initWithDocument:(PSPDFDocument *)document pdfController:(ADXPDFViewController *)pdfController
{
    NSString *ii = UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ? @"Phone" : @"Pad";
    self = [super initWithNibName:[NSString stringWithFormat:@"ADXAnnotationPane-%@", ii] bundle:nil];
    if ( self )
    {
        _pdfController = pdfController;
        _document = document;
        
        UINib *nib = [UINib nibWithNibName:[NSString stringWithFormat:@"ADXAnnotationToolsBar-%@", ii] bundle:nil];
        [nib instantiateWithOwner:self options:nil];
        
        [super setToolbarItems: [self.buttonItems copy]];
        
        UITabBarItem *tabBarItem = [[UITabBarItem alloc] initWithTitle:@"annotations" image:[UIImage imageNamed:@"166-Drawing.png"] tag:1];
        tabBarItem.accessibilityLabel = NSLocalizedString(@"Annotations", nil);
        [super setTabBarItem:tabBarItem];
        
        //FIXME: This will be replaced by _pdfController when we delete the ADXSlidePanelController class.
        [_pdfController addToolsDelegate:self];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(handleDocumentNoteDidChangeNotification:)
                                                     name:kADXDocumentNoteDidChangeNotification
                                                   object:nil];

        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(handleNotificationFilterChanged:)
                                                     name:kADXAnnotationFilterChangedNotification
                                                   object:nil];
        
        NSDictionary *attributes = @{
            NSFontAttributeName: [UIFont italicSystemFontOfSize:[UIFont systemFontSize]],
            NSForegroundColorAttributeName: [UIColor grayColor]
        };
        
        self.highlightedTextIndicator = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"Highlighted Text", nil) attributes:attributes];
    }
    return self;
}


#pragma mark - UIViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self configureThumbnailCollectionView];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self refresh];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self updatePageControl];
}

- (void)dealloc
{
    [self.pdfController removeToolsDelegate:self];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


#pragma mark - Notifications

- (void)handleNotificationFilterChanged:(NSNotification *)notification
{
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        // Annotation filter changed .. turn off highlight modes that would conflict
        if (self.documentAnnotationMode == ADXDocumentAnnotationModeHighlight && (![ADXUserDefaults sharedDefaults].annotationFilterShowHighlights)) {
            self.documentAnnotationMode = ADXDocumentAnnotationModeNone;
        }
        
        if (self.documentAnnotationMode == ADXDocumentAnnotationModeComment) {
            if (![ADXUserDefaults sharedDefaults].annotationFilterShowNotes ||
                ![ADXUserDefaults sharedDefaults].annotationFilterIncludeMyNotes) {
                self.documentAnnotationMode = ADXDocumentAnnotationModeNone;
            }
        }
        
        [self refresh];
    }];
}

- (void)handleDocumentNoteDidChangeNotification:(NSNotification *)notification
{
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        [self refresh];
    }];
}

#pragma mark - IBActions

- (IBAction)toggleStripsButtonTapped:(id)sender
{
    if (self.pdfController.isPDFStripsVisible) {
        [self.pdfController hidePDFStrips];
    } else {
        [[ADXAppDelegate sharedInstance] passCheckpoint:@"Viewed annotations as PDF strips"];
        
        [self refreshPageResultsIndex];
        
        self.documentAnnotationMode = ADXDocumentAnnotationModeNone;
        [self setAnnotationVisible:YES];
        
        if (self.pdfController.presentedViewController == self) {
            // Hide the thumbnails drawer
            [self.pdfController dismissViewControllerAnimated:YES completion:^{
                [self.pdfController togglePDFStripsWithDataSource:self];
            }];
        } else {
            [self.pdfController togglePDFStripsWithDataSource:self];
        }
    }
}

- (void) setAnnotationVisible:(BOOL)visible
{
    [self closePopover];

    self.pdfController.annotationsVisible = visible;
}

- (IBAction)commentButtonPressed:(id)sender
{
    [self closePopover];
    [self.pdfController hidePDFStrips];
    
    [self confirmAndPerformNoteOperation:^{
        [self toggleDocumentAnnotationMode:ADXDocumentAnnotationModeComment];
    }];

}

- (IBAction)highlightButtonPressed:(id)sender
{
    [self closePopover];

    [self.pdfController hidePDFStrips];
    
    [self confirmAndPerformHighlightOperation:^{
        [self toggleDocumentAnnotationMode:ADXDocumentAnnotationModeHighlight];
    }];
}

- (void)confirmAndPerformNoteOperation:(ADXAnnotationOperationPerformBlock)performBlock
{
    if (![ADXUserDefaults sharedDefaults].annotationFilterShowNotes ||
        ![ADXUserDefaults sharedDefaults].annotationFilterIncludeMyNotes) {
        // The note the user is about to add would not be visible.  Prompt whether to turn notes on or cancel.
        PSPDFAlertView *alertView = [[PSPDFAlertView alloc] initWithTitle:NSLocalizedString(@"Notes Filtered", nil)
                                                                  message:NSLocalizedString(@"Notes you create are currently hidden.  You must show notes before you can create one.", nil)];
        [alertView addButtonWithTitle:NSLocalizedString(@"Show Notes", nil)
                                block:^{
                                    [ADXUserDefaults sharedDefaults].annotationFilterShowNotes = YES;
                                    [ADXUserDefaults sharedDefaults].annotationFilterIncludeMyNotes = YES;
                                    
                                    [self.pdfController refreshAnnotations];

                                    performBlock();
                                }];
        [alertView addButtonWithTitle:@"Cancel"];
        [alertView show];
        
    } else {
        // Just do it
        performBlock();
    }
}

- (void)confirmAndPerformHighlightOperation:(ADXAnnotationOperationPerformBlock)performBlock
{
    if (![ADXUserDefaults sharedDefaults].annotationFilterShowHighlights) {
        // The note the user is about to add would not be visible.  Prompt whether to turn notes on or cancel.
        PSPDFAlertView *alertView = [[PSPDFAlertView alloc] initWithTitle:NSLocalizedString(@"Highlights Filtered", nil)
                                                                  message:NSLocalizedString(@"Highlights are currently hidden.  You must show highlights before you can create one.", nil)];
        [alertView addButtonWithTitle:NSLocalizedString(@"Show Highlights", nil)
                                block:^{
                                    [ADXUserDefaults sharedDefaults].annotationFilterShowHighlights = YES;
                                    [self.pdfController refreshAnnotations];

                                    performBlock();
                                }];
        [alertView addButtonWithTitle:@"Cancel"];
        [alertView show];
        
    } else {
        performBlock();
    }
}

#pragma mark - Private

- (NSArray*)toolBarItemsLeft
{
    UIBarButtonItem *fs = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                                       target:nil action:NULL];
    return @[fs,self.annotationsCountBarButton];
}

- (NSArray*)toolBarItemsRight
{
    return @[self.addNoteButton, self.highlightButton];
}

- (void)reloadData
{
    [self.thumbnailCollectionView reloadData];
    
    [self updateAnnotationCountLabel];
}

- (void)updateAnnotationCountLabel
{
    // Count total and hidden annotations
    ADXAnnotationFilter *annotationFilter = [[ADXAnnotationFilter alloc] init];
    int hiddenCount = 0;
    int totalCount = 0;
    for (ADXDocumentNote *note in self.pdfController.adxDocument.metaDocument.notes) {
        if (!note.isAccessible) {
            continue;
        }
        
        if (note.openVersionValue > self.pdfController.adxDocument.versionValue) {
            continue;
        }
        
        if (note.isClosed && note.closeVersionValue < self.pdfController.adxDocument.versionValue) {
            continue;
        }

        totalCount++;
        
        if ([annotationFilter shouldFilterAnnotation:note onDocument:self.pdfController.adxDocument]) {
            hiddenCount++;
        }
    }
    
    NSString *labelText = nil;
    if (totalCount == 1) {
        labelText = NSLocalizedString(@"1 annotation", @"total number of annotation in the annotation tab toolbar (singular)");
    } else {
        NSString *format = NSLocalizedString(@"%d annotations", @"total number of annotation in the annotation tab toolbar (plural)");
        labelText = [NSString stringWithFormat:format, totalCount];
    }
    
    if (hiddenCount > 0) {
        if (hiddenCount == 1) {
            labelText = [labelText stringByAppendingString:NSLocalizedString(@" (1 hidden)", nil)];
        } else {
            labelText = [labelText stringByAppendingFormat:NSLocalizedString(@" (%d hidden)", nil), hiddenCount];
        }
    }
    
    self.annotationsCount.text = labelText;
}

- (void) refresh
{
    [self refreshPageResultsIndex];
    
    ADXAnnotationFilter *filter = [[ADXAnnotationFilter alloc] init];
    BOOL anyAnnotationsVisible = NO;
    for (ADXDocumentNote *note in self.pdfController.adxDocument.metaDocument.notes) {
        if (!note.isDeleted && ![filter shouldFilterAnnotation:note onDocument:self.pdfController.adxDocument]) {
            anyAnnotationsVisible = YES;
            break;
        }
    }

    if ( anyAnnotationsVisible ) {
        self.toggleStripsButton.enabled = YES;
        self.toggleThumbnailsButton.enabled = YES;
        self.noResultFound.hidden = YES;
    } else {
        self.toggleStripsButton.enabled = NO;
        self.toggleThumbnailsButton.enabled = NO;
        self.noResultFound.hidden = NO;
    }

    [self reloadData];
}

- (NSArray *)filterAnnotations:(NSArray *)annotations
{
    return [annotations filteredArrayUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(id evaluatedObject, NSDictionary *bindings) {
        if ([evaluatedObject isDeleted]) {
            return NO;
        }

        if (![evaluatedObject conformsToProtocol:@protocol(ADXAnnotationProtocol)]) {
            return NO;
        }

        if ([evaluatedObject isKindOfClass:[ADXHighlightAnnotation class]]) {
            ADXHighlightAnnotation *highlight = (ADXHighlightAnnotation *)evaluatedObject;
            if (highlight.noteAnnotation != nil) {
                return NO;
            }
        }
        
        return YES;
    }]];
}

- (void)refreshPageResultsIndex
{
    self.pagesWithAnnotations = [NSMutableDictionary dictionary];
    
    NSArray *annotationInPage;
    NSMutableArray *annotationDescriptionByPage;
    PSPDFDocument *document = self.pdfController.document;
    NSUInteger pageCount = [document pageCount];
    
    for(int page = 0; page < pageCount; page++)
    {
        annotationInPage = [document annotationsForPage:page type:[self annotationTypes]];
        annotationInPage = [self filterAnnotations:annotationInPage];
        
        __block NSInteger ac = 0;
        annotationInPage = [annotationInPage filteredArrayUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(id evaluatedObject, NSDictionary *bindings) {
            if ([evaluatedObject isKindOfClass:[ADXNoteAnnotation class]] ||
                ([evaluatedObject isKindOfClass:[ADXHighlightAnnotation class]] && ![(ADXHighlightAnnotation *)evaluatedObject noteAnnotation])) {
                ac++;
                return YES;
            }
            return NO;
        }]];

        if (ac > 0) {
            annotationDescriptionByPage = [NSMutableArray arrayWithCapacity:annotationInPage.count];
            
            for (PSPDFAnnotation* a in annotationInPage) {
                [annotationDescriptionByPage addObject:self.highlightedTextIndicator];
            }
            
            [self.pagesWithAnnotations setObject:annotationDescriptionByPage
                                          forKey:[NSNumber numberWithInt:page]];
        }
    }
    
    self.sortedPageIndex = [[self.pagesWithAnnotations allKeys] sortedArrayUsingSelector:@selector(compare:)];
}

- (NSAttributedString*)descriptionForAnnotation:(PSPDFAnnotation*)annotation_
{
    ADXNoteAnnotation *note;
    ADXHighlightAnnotation *highlight;
    NSMutableAttributedString* descr;
    
    if ([annotation_ isMemberOfClass:[ADXNoteAnnotation class]]) {
        note = (ADXNoteAnnotation*)annotation_;
        highlight = note.highlightAnnotation;
    } else if ([annotation_ isMemberOfClass:[ADXHighlightAnnotation class]]) {
        highlight = (ADXHighlightAnnotation*)annotation_;
    }
    
    if (highlight) {
        CGRect aRect = highlight.boundingBox;
        CGRect pRect = [highlight.document rectBoxForPage:highlight.page];
        
        CGRect snippetRect = CGRectMake(pRect.origin.x, aRect.origin.y, pRect.size.width, aRect.size.height);

        NSArray *snippetGlyphs = [[highlight.document objectsAtPDFRect:snippetRect page:highlight.page
                                                               options:nil]  objectForKey:@"kPSPDFWords"];
        
        NSArray *annotationGlyphs = [[highlight.document objectsAtPDFRect:aRect page:highlight.page
                                                                  options:nil]objectForKey:@"kPSPDFWords"];
        
        NSMutableString *snippeTextBlock = [NSMutableString new];
        [snippetGlyphs enumerateObjectsUsingBlock:^(PSPDFWord* obj, NSUInteger idx, BOOL *stop) {
            [snippeTextBlock appendFormat:@"%@ ", obj.stringValue ];
        }];
        
        NSMutableString *annotationTextBlock = [NSMutableString new];
        [annotationGlyphs enumerateObjectsUsingBlock:^(PSPDFWord* obj, NSUInteger idx, BOOL *stop) {
            [annotationTextBlock appendFormat:@"%@ ", obj.stringValue ];
        }];
        
        descr = [[NSMutableAttributedString alloc]initWithString:snippeTextBlock];
        
        NSRange higthLigthTextRamge = [snippeTextBlock rangeOfString:annotationTextBlock];
        [descr addAttribute:NSBackgroundColorAttributeName value:highlight.color range:higthLigthTextRamge];
        [descr addAttribute:NSFontAttributeName value:KDescriptionFont range:NSMakeRange(0, descr.length)];
    }
    
    if (note) {
        NSString *noteContent = descr ? [NSString stringWithFormat:@"\n%@", note.contents] : note.contents;
        NSRange noteRange = NSMakeRange((descr ? descr.length : 0), noteContent.length);
        
        NSMutableAttributedString *noteSting = [[NSMutableAttributedString alloc]initWithString:noteContent];
        
        if (descr) {
            [descr appendAttributedString:noteSting];
        } else {
            descr = noteSting;
        }
        
        [descr addAttribute:NSFontAttributeName value:KNoteTextFont range:noteRange];
    }
    
    return descr;
}

// allow filtering link annotations
- (PSPDFAnnotationType)annotationTypes
{
    PSPDFAnnotationType annotationTypes = PSPDFAnnotationTypeAll;
    annotationTypes &= ~PSPDFAnnotationTypeLink;

    return annotationTypes;
}

- (void)closePopover
{
    if (self.currentPopover) {
        [self.currentPopover dismissPopoverAnimated:NO];
        self.currentPopover = nil;
    }
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [self.pagesWithAnnotations count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
        
    return [[self.pagesWithAnnotations objectForKey:[self.sortedPageIndex objectAtIndex:section]] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellID = @"PSPDFDescriptionTableViewCell";
    ADXDescriptionCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    
	if (cell == nil) {
        UINib *nib = [UINib nibWithNibName:@"ADXDescriptionCell" bundle:[NSBundle mainBundle]];
        cell = [[nib instantiateWithOwner:self options:nil] objectAtIndex:0];
    }
    
    NSNumber *pageNumber = [self.sortedPageIndex objectAtIndex:indexPath.section];
    NSAttributedString *description = [[self.pagesWithAnnotations objectForKey:pageNumber] objectAtIndex:indexPath.row];
    [self updateResultCell:cell description:description pageIndex:pageNumber.integerValue];
    
    return cell;
}

- (NSAttributedString *)attributedString:(NSAttributedString *)stringToTrim byTrimmingCharactersInSet:(NSCharacterSet *)set
{
    NSCharacterSet *invertedSet = [set invertedSet];
    NSString *str = stringToTrim.string;
    unsigned loc, len;
    
    NSRange range = [str rangeOfCharacterFromSet:invertedSet];
    loc = (range.length > 0) ? range.location : 0;
    
    range = [str rangeOfCharacterFromSet:invertedSet options:NSBackwardsSearch];
    len = (range.length > 0) ? NSMaxRange(range)-loc :
    [str length]-loc;
    
    return [stringToTrim attributedSubstringFromRange:NSMakeRange(loc,len)];
}

- (void)updateResultCell:(ADXDescriptionCell *)cell description:(NSAttributedString *)description pageIndex:(NSInteger)pageNumber
{
    cell.textSampleLabel.attributedText = [self attributedString:description byTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    // do we have a page title here? only use outline if we have > 1 element (some documents just have page title, not what we want)
    NSString *outlineTitle = nil;
    if ([self.document.outlineParser.outline.children count] > 1) {
        outlineTitle = [self.document.outlineParser outlineElementForPage:pageNumber exactPageOnly:NO].title;
        outlineTitle = [outlineTitle substringWithRange:NSMakeRange(0, MIN(25, [outlineTitle length]))]; // limit
    }
    
    NSString *pageTitle = [NSString stringWithFormat:PSPDFLocalize(@"Page %d"), pageNumber+1];
    NSString *cellText = outlineTitle ? [NSString stringWithFormat:@"%@, %@", outlineTitle, pageTitle] : pageTitle;
    cell.pageInfoLabel.text = cellText;
    
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return nil;//[NSString stringWithFormat:@"Page %d", section];
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSAttributedString *description = [[self.pagesWithAnnotations objectForKey:[self.sortedPageIndex objectAtIndex:indexPath.section]] objectAtIndex:indexPath.row];
    
    CGRect rect = [description boundingRectWithSize:CGSizeMake(self.detailView.frame.size.width-20, 0.0)
                                            options:NSStringDrawingUsesLineFragmentOrigin
                                            context:NULL];
    
    return MAX(rect.size.height+15, 70);
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSNumber *pn = [self.sortedPageIndex objectAtIndex:indexPath.section];
    
    [self.pdfController setPage:pn.integerValue animated:NO];
    [(ADXPDFViewController*)self.pdfController setAnnotationsVisible:YES];
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}


#pragma mark -

// similar to PSPDFDrawView; detect touches, lock PDF UI.
- (void)addHighlightAnnotationView {
    [self lockPDFController];
    PSPDFPageView *pageView = [self.pdfController pageViewForPage:self.pdfController.page];
    
    PSPDFSelectionView *highlightView = [[PSPDFSelectionView alloc] initWithFrame:pageView.bounds delegate:self];
    self.highlightView = highlightView;
    [pageView addSubview:highlightView];
    [self.pdfController hideControlsAndPageElements];
}

- (void)removeHighlightAnnotationViewAnimated:(BOOL)animated {
    if (self.highlightView) {
        [self unlockPDFControllerAndEnsureToStayOnTop:animated];
        [self.highlightView removeFromSuperview];
        self.highlightView.delegate = nil;
        self.highlightView = nil;
    }
}

- (void)toggleDocumentAnnotationMode:(ADXDocumentAnnotationMode)documentMode
{
    if (self.documentAnnotationMode == documentMode) {
        self.documentAnnotationMode = ADXDocumentAnnotationModeNone;
    }else {
        
        self.documentAnnotationMode = documentMode;
    }
}

- (void)setDocumentAnnotationMode:(ADXDocumentAnnotationMode)documentAnnotationMode
{
    if( _documentAnnotationMode != documentAnnotationMode )
    {
        _documentAnnotationMode = documentAnnotationMode;
        
        UIColor *definedColor = [[UIBarButtonItem appearanceWhenContainedIn:[ADXToolbar class], nil] tintColor];
        self.highlightButton.tintColor = definedColor;
        self.addNoteButton.tintColor = definedColor;
        switch (documentAnnotationMode)
        {
            case ADXDocumentAnnotationModeNone:
                [self removeHighlightAnnotationViewAnimated:YES];
                break;
            case ADXDocumentAnnotationModeHighlight:
                self.highlightButton.tintColor = KHighlightColor;
                [self removeHighlightAnnotationViewAnimated:YES];
                [self addHighlightAnnotationView];
                break;
            case ADXDocumentAnnotationModeComment:
                self.addNoteButton.tintColor = KHighlightColor;
                [self removeHighlightAnnotationViewAnimated:YES];
                [self addHighlightAnnotationView];
                break;
            default:
                [self addHighlightAnnotationView];
                break;
        }
    }
}

- (ADXNoteAnnotation*)addNoteAtPoint:(CGPoint)pdfPoint inPageView:(PSPDFPageView*)pageView
{
    [self setAnnotationVisible:YES];
//    [self.pdfController lockController];
    
    CGRect highlightRect = CGRectMake(pdfPoint.x, pdfPoint.y, 0, 0);
    highlightRect = CGRectApplyAffineTransform(highlightRect, pageView.pageInfo.pageRotationTransform);
    ADXAppDelegate *appDelegate = (ADXAppDelegate *)[[UIApplication sharedApplication] delegate];
    ADXDocumentNote *adxNote = [ADXDocumentNote insertNoteByUser:appDelegate.currentAccount.user
                                                     forDocument:self.pdfController.adxDocument
                                                            page:pageView.page + 1
                                                            text:@""
                                                  highlightRects:@[[NSValue valueWithCGRect:highlightRect]]];

    adxNote.visibility = [ADXUserDefaults sharedDefaults].defaultNoteVisibility;

    ADXNoteAnnotation *annotation = [ADXNoteAnnotation noteAnnotation:highlightRect
                                                                 page:pageView.page
                                                                 note:adxNote
                                                             document:self.pdfController.adxDocument
                                                                  pdf:pageView.document];
    
    // add annotation and redraw
    [pageView.document addAnnotations:@[annotation] forPage:pageView.page];
    [self refresh];
    [pageView addAnnotation:annotation animated:NO];

    [self saveNoteForAnnotation:annotation withOptionalSync:NO];

    // disable toolbar mode
    self.documentAnnotationMode = ADXDocumentAnnotationModeNone;
    [self removeHighlightAnnotationViewAnimated:YES];
    
    // disrupt current active tap gesture
    [pageView.scrollView setCurrentTouchEventAsProcessed];
    
    // open annotation controller
    [pageView showNoteControllerForAnnotation:annotation showKeyboard:YES animated:YES];
    
    return annotation;
}

- (NSMutableArray *)glyphsByRemovingBlankLinesFromGlyphs:(NSArray *)rawGlyphs
{
    // Remove lines that contain only whitespace
    NSMutableArray *glyphs = [NSMutableArray array];
    NSMutableArray *glyphRun = [NSMutableArray array];
    NSCharacterSet *whitespace = [NSCharacterSet whitespaceAndNewlineCharacterSet];
    BOOL encounteredNonWhitespace = NO;
    for (PSPDFGlyph *glyph in rawGlyphs) {
        NSString *content = [glyph.content stringByTrimmingCharactersInSet:whitespace];
        if (content.length > 0) {
            encounteredNonWhitespace = YES;
        }
        
        if ([glyph.content isEqualToString:@"\n"]) {
            continue;
        }
        
        [glyphRun addObject:glyph];
        
        if (glyph.lineBreaker) {
            if (encounteredNonWhitespace) {
                [glyphs addObjectsFromArray:glyphRun];
                encounteredNonWhitespace = NO;
            }
            glyphRun = [NSMutableArray array];
        }
    }
    if (encounteredNonWhitespace) {
        [glyphs addObjectsFromArray:glyphRun];
    }
    return glyphs;
}

- (ADXHighlightAnnotation *)highlightGlyphs:(NSArray*)rawGlyphs
                           withAttachedNote:(BOOL)attachedNote
                                 inPageView:(PSPDFPageView*)pageView
                              highlightType:(PSPDFHighlightAnnotationType)highlightType
{
    NSMutableArray *glyphs = [self glyphsByRemovingBlankLinesFromGlyphs:rawGlyphs];
    if (glyphs == nil || glyphs.count == 0) {
        // Nothing to highlight
        return nil;
    }
    
    [self setAnnotationVisible:YES];
    
    CGRect boundingBox;
    CGAffineTransform t = pageView.pageInfo.pageRotationTransform;
    NSArray *highlighedRects = PSPDFRectsFromGlyphs(glyphs, t, &boundingBox);
    NSString *noteText = nil;
    if (attachedNote) {
        noteText = @"";
    }

    ADXAppDelegate *appDelegate = (ADXAppDelegate *)[[UIApplication sharedApplication] delegate];
    ADXDocumentNote *adxNote = [ADXDocumentNote insertNoteByUser:appDelegate.currentAccount.user
                                                     forDocument:self.pdfController.adxDocument
                                                            page:pageView.page + 1
                                                            text:noteText
                                                  highlightRects:highlighedRects];

    adxNote.visibility = [ADXUserDefaults sharedDefaults].defaultNoteVisibility;

    ADXHighlightAnnotation *highlightAnnotation = [ADXHighlightAnnotation highlightAnnotation:boundingBox
                                                                                         page:pageView.page
                                                                                         note:adxNote
                                                                                     document:self.pdfController.adxDocument
                                                                                          pdf:pageView.document];

    highlightAnnotation.color = ADXHighlightAnnotationColor;
    highlightAnnotation.rects = highlighedRects;

    BOOL syncNow = NO;
    if (attachedNote) {
        // Highlights with an attached note are synced when the initial note edit is done
        ADXNoteAnnotation *noteAnnotation = highlightAnnotation.noteAnnotation;
        noteAnnotation.color = ADXNoteAnnotationOpenColor;
        [pageView.document addAnnotations:@[highlightAnnotation, noteAnnotation] forPage:pageView.page];
        [pageView addAnnotation:noteAnnotation animated:NO];
    } else {
        // Highlights with no attached note are synced immediately
        syncNow = YES;
        [pageView.document addAnnotations:@[highlightAnnotation] forPage:pageView.page];
    }

    [self refresh];
    [pageView updateView];
    
    [self saveNoteForAnnotation:highlightAnnotation withOptionalSync:syncNow];
    
    [pageView.scrollView setCurrentTouchEventAsProcessed];
    
    return highlightAnnotation;
}

- (void)saveNoteForAnnotation:(PSPDFAnnotation*)annotation withOptionalSync:(BOOL)sync
{
    ADXDocumentNote *note = [ADXAnnotationUtil noteFromAnnotation:(id<ADXAnnotationProtocol>)annotation];
    [note saveWithOptionalSync:sync completion:^(BOOL success, NSError *error) {
        if (!success) {
            LogNSError(@"error saving annotation", error);
        }
    }];
}

- (void)lockPDFController {
    if ([self.pdfController isControllerLocked]) {
        PSPDFLogWarning(@"PDF Controller already locked.");
        return;
    }
    
    [self.pdfController setHUDVisible:YES animated:YES];
    [self.pdfController lockController];
}

- (void)unlockPDFControllerAndEnsureToStayOnTop:(BOOL)stayOnTop {
    if (![self.pdfController isControllerLocked]) {
        PSPDFLogWarning(@"PDF Controller is not locked.");
        return;
    }
    
    [self.pdfController unlockController];
    [self.pdfController showControls];
}


#pragma mark - PSPDFSelectionViewDelegate

// handle comment creation
- (BOOL)selectionView:(PSPDFSelectionView *)selectionView shouldStartSelectionAtPoint:(CGPoint)point
{
    if (self.documentAnnotationMode == ADXDocumentAnnotationModeComment) {
        PSPDFPageView* pageView = [self.pdfController pageViewForPage:self.pdfController.page];
        CGPoint pdfPoint = [pageView convertViewPointToPDFPoint:point];
        ADXNoteAnnotation *pdfNote = [self addNoteAtPoint:CGPointMake(pdfPoint.x, pdfPoint.y) inPageView:pageView];
        if (!pdfNote) {
            LogError(@"Couldn't add note annotation");
        }
        
        [self refresh];
        return NO;
    } else {
        return YES;
    }
}

/// Called when a rect was selected successfully. (touchesEnded)
- (void)selectionView:(PSPDFSelectionView *)selectionView finishedWithSelectedRect:(CGRect)rect
{
    PSPDFHighlightAnnotationType highlightType;
    switch (self.documentAnnotationMode) {
        case ADXDocumentAnnotationModeHighlight:
            highlightType = PSPDFHighlightAnnotationHighlight;
            break;
        case ADXDocumentAnnotationModeStrikeOut:
            highlightType = PSPDFHighlightAnnotationStrikeOut;
            break;
        case ADXDocumentAnnotationModeUnderline:
            highlightType = PSPDFHighlightAnnotationUnderline;
            break;
        default:
            PSPDFLogWarning(@"Invalid toolbar mode.");
            return;
            break;
    }

    if (self.documentAnnotationMode == ADXDocumentAnnotationModeHighlight && rect.size.width < kMinimumHighlightSize && rect.size.height < kMinimumHighlightSize) {
        // Ignore a tap with the highlighter
        return;
    }
    
    // detect objects
    PSPDFPageView *pageView = [self.pdfController pageViewForPage:self.pdfController.page];
    NSDictionary *objects = [pageView objectsAtRect:rect options:nil];
    
    ADXHighlightAnnotation *pdfAnntotation = [self highlightGlyphs:objects[kPSPDFGlyphs]
                                                  withAttachedNote:NO
                                                        inPageView:pageView
                                                     highlightType:highlightType];
    
    if (self.documentAnnotationMode == ADXDocumentAnnotationModeHighlight) {
        // After highlighting, turn off the highlighter
        [self setDocumentAnnotationMode:ADXDocumentAnnotationModeNone];
    }

    if (pdfAnntotation) {
        [self saveNoteForAnnotation:pdfAnntotation withOptionalSync:YES];
        [self refresh];
    } else {
        LogInfo(@"Unable to create highlight annotation");
    }
}

- (NSArray *)menuItemsForSelectedText:(NSString *)selectedText atSuggestedTargetRect:(CGRect)rect inRect:(CGRect)textRect onPageView:(PSPDFPageView *)pageView;
{
    NSArray* items = nil;
    if (selectedText) {
        __weak ADXAnnotationViewController *weakSelf = self;
        NSArray *selectedGlyphs = pageView.selectionView.selectedGlyphs;
        
        ADXHighlightAnnotation *(^highlightText)(BOOL) = ^(BOOL withAttachedNote) {
            ADXHighlightAnnotation *highlightAnnotation;
            highlightAnnotation = [weakSelf highlightGlyphs:selectedGlyphs
                                  withAttachedNote:withAttachedNote
                                        inPageView:pageView
                                     highlightType:[PSPDFHighlightAnnotation highlightTypeFromTypeString:PSPDFAnnotationTypeStringHighlight]];
            return highlightAnnotation;
        };
        
        PSPDFMenuItem *highlightItem = [[PSPDFMenuItem alloc] initWithTitle:NSLocalizedString(@"Highlight", nil) block:^{
            [self confirmAndPerformHighlightOperation:^{
                ADXHighlightAnnotation *annotation = highlightText(NO);
                if (!annotation) {
                    LogError(@"couldn't create highlight annotation");
                }
                [pageView.selectionView discardSelection];
            }];
        } identifier:@"Highlight"];
        
    
        PSPDFMenuItem *noteItem = [[PSPDFMenuItem alloc] initWithTitle:NSLocalizedString(@"Note", nil) block:^{
            [self confirmAndPerformNoteOperation:^{
                ADXHighlightAnnotation *annotation = highlightText(YES);
                if (!annotation) {
                    LogError(@"couldn't create note annotation");
                }
                [pageView.selectionView discardSelection];
                [pageView showNoteControllerForAnnotation:annotation.noteAnnotation showKeyboard:YES animated:YES];
            }];
        } identifier:@"Note"];

        if (pageView.selectionView.selectedText.length > 0) {
            PSPDFMenuItem *copyItem = [[PSPDFMenuItem alloc] initWithTitle:NSLocalizedString(@"Copy", nil) block:^{
                [self confirmAndPerformHighlightOperation:^{
                    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
                    pasteboard.string = pageView.selectionView.selectedText;
                    [pageView.selectionView discardSelection];
                }];
            } identifier:@"Copy"];
            
            items = @[highlightItem, noteItem, copyItem];
        } else {
            items = @[highlightItem, noteItem];
        }
    }
    
    return items;
}

- (BOOL)pdfController:(PSPDFViewController *)pdfController didLongPressOnPageView:(PSPDFPageView *)pageView atPoint:(CGPoint)viewPoint gestureRecognizer:(UILongPressGestureRecognizer *)recognizer
{
    BOOL handled = NO;
    
    switch ([recognizer state]) {
        case UIGestureRecognizerStateBegan: {
            for (UIView *view in pageView.subviews){
                if (![view isKindOfClass:[PSPDFNoteAnnotationView class]]) {
                    continue;
                }

                PSPDFNoteAnnotationView *noteView = (PSPDFNoteAnnotationView *)view;
                PSPDFAnnotation *annotation = noteView.annotation;
                
                if (![annotation conformsToProtocol:@protocol(ADXAnnotationProtocol)]) {
                    continue;
                }
                
                ADXNoteAnnotation *note = (ADXNoteAnnotation*)noteView.annotation;
                ADXHighlightAnnotation *highlight = note.highlightAnnotation;
                CGPoint locationInNoteView = [recognizer locationInView:noteView];
                BOOL pressedOnNote = CGRectContainsPoint(noteView.bounds, locationInNoteView);
                BOOL originatorOfNote = [ADXAnnotationUtil currentUserIsAuthorOfAnnotation:(id<ADXAnnotationProtocol>)annotation];
                
                // We want to consume drag events for notes that have an attached highlight
                handled = (pressedOnNote && highlight) || (pressedOnNote && !originatorOfNote);
                
                if (handled)
                    break;
            }
            
            break;
        }
        default:
            handled = NO;
            break;
    }
    
    return handled;
}

- (void)documentDidChanged:(PSPDFDocument *)document
{
    self.document = document;
    [self refresh];
}

- (IBAction)didTapToolbarMenu:(id)sender
{
    [self closePopover];
    [self openClose:sender];
}

- (IBAction)openClose:(id)sender
{
    __weak ADXAnnotationViewController *weakSelf = self;
    if (self.pdfController.presentedViewController == self){
        [self.pdfController dismissViewControllerAnimated:YES completion:^{
            [weakSelf refresh];
        }];
    } else {
        [self.pdfController presentViewController:self animated:YES completion:NULL];
    }
}

- (void)pdfController:(ADXPDFViewController *)controller selectedToolDidChanged:(UIViewController*)tool
{
    if (tool != self) {
        [self toggleDocumentAnnotationMode:ADXDocumentAnnotationModeNone];
        
        if (self.pdfController.presentedViewController == self) {
            [self.pdfController dismissViewControllerAnimated:YES completion:NULL];
        }

        [self closePopover];
    } else {
        [self refresh];
    }
}

- (void)pdfController:(ADXPDFViewController *)pdfController didChangeViewMode:(PSPDFViewMode)viewMode
{
    BOOL isDocumentMode = viewMode == PSPDFViewModeDocument;
    if( !isDocumentMode )
    {
        [self toggleDocumentAnnotationMode:ADXDocumentAnnotationModeNone];
    }
    
    self.highlightButton.enabled = isDocumentMode;
    self.addNoteButton.enabled = isDocumentMode;
}

#pragma mark - ADXPageThumbnailViewDelegate

- (void)thumbnailDidTapped:(ADXPageThumbnailView *)thumbnail
{
    [self.pdfController setPage:thumbnail.pageIndex animated:YES];
}


#pragma mark - ADXPDFStripsViewControllerDataSource

- (int)numberOfSectionsForPDFStrips
{
    return [self.sortedPageIndex count];
}

- (int)numberOfPDFStripsInSection:(int)section
{
    NSNumber *key = [self.sortedPageIndex objectAtIndex:section];
    PSPDFDocument *document = self.pdfController.document;
    NSArray *annotationsOnPage = [document annotationsForPage:key.intValue type:PSPDFAnnotationTypeAll];
    annotationsOnPage = [self filterAnnotations:annotationsOnPage];
    return annotationsOnPage.count;
}

- (ADXPDFStripInfo *)infoForPDFStripAtIndexPath:(NSIndexPath *)indexPath
{
    // Find the annotation
    NSNumber *key = [self.sortedPageIndex objectAtIndex:indexPath.section];
    PSPDFDocument *document = self.pdfController.document;
    NSArray *annotationsOnPage = [document annotationsForPage:key.intValue type:PSPDFAnnotationTypeAll];
    annotationsOnPage = [self filterAnnotations:annotationsOnPage];
    PSPDFAnnotation *annotation = [annotationsOnPage objectAtIndex:indexPath.item];
    
    NSMutableArray *annotations = [NSMutableArray arrayWithObject:annotation];
    if ([annotation isKindOfClass:[ADXNoteAnnotation class]]) {
        ADXNoteAnnotation *note = (ADXNoteAnnotation *)annotation;
        if (note.highlightAnnotation != nil) {
            [annotations addObject:note.highlightAnnotation];
        }
    }
    
    // Build the strip info
    ADXPDFStripInfo *stripInfo = [[ADXPDFStripInfo alloc] init];
    stripInfo.annotations = annotations;
    stripInfo.pageIndex = annotation.absolutePage;
    stripInfo.stripExtent = annotation.boundingBox;
    
    if ([annotation isKindOfClass:[ADXNoteAnnotation class]]) {
        ADXNoteAnnotation *noteAnnotation = (ADXNoteAnnotation *)annotation;
        ADXDocumentNote *note = [ADXAnnotationUtil noteFromAnnotation:noteAnnotation];
        if (note != nil) {
            NSString *noteText = [note.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            
            // TODO: This isn't really correct but it's getting rewritten soon anyway .. otherwise it means figuring out how
            // much space we have left after the author name (if required) and truncating at that point.
            if (noteText.length > 80) {
                noteText = [noteText substringToIndex:80];
                noteText = [NSString stringWithFormat:@"%@...", noteText];
            }
            
            if (noteText.length > 0) {
                NSString *summaryText = @"";
                if ([ADXAnnotationUtil currentUserIsAuthorOfAnnotation:noteAnnotation]) {
                    summaryText = [NSString stringWithFormat:@"%@ 💬", noteText];
                } else {
                    summaryText = [NSString stringWithFormat:@"%@ 💬 by %@", noteText, note.author.displayNameOrLoginName];
                }
                
                stripInfo.summary = [[NSAttributedString alloc] initWithString:summaryText];
            }
        }
    }
    
    return stripInfo;
}

- (NSString *)titleForPDFStripSection:(int)section
{
    NSNumber *pageNumber = [self.sortedPageIndex objectAtIndex:section];
    NSString *format = NSLocalizedString(@"Annotations on page %d", nil);
    NSString *title = [NSString stringWithFormat:format, pageNumber.integerValue + 1];
    return title;
}

- (ADXHighlightedContentHelper *)highlightedContentForPDFStrips
{
    if (self.pdfController.adxDocument.highlightedContent) {
        ADXHighlightedContentHelper *highlightedContent = [[ADXHighlightedContentHelper alloc] initWithData:self.pdfController.adxDocument.highlightedContent.data];
        return highlightedContent;
    } else {
        return nil;
    }
}


#pragma mark - Collection View

- (void)updatePageControl
{
    // Ask the layout how many pages are displayed
    ADXCollectionViewHorizontalLayout *layout = (ADXCollectionViewHorizontalLayout *)self.thumbnailCollectionView.collectionViewLayout;
    self.thumbPageControl.numberOfPages = layout.numberOfPages;
    
    // Calculate the displayed page number
    int pageNum = (self.thumbnailCollectionView.contentOffset.x + (self.thumbnailCollectionView.bounds.size.width/2)) / self.thumbnailCollectionView.bounds.size.width;
    self.thumbPageControl.currentPage = pageNum;
}

- (void)configureThumbnailCollectionView
{
    ADXCollectionViewHorizontalLayout *layout = (ADXCollectionViewHorizontalLayout *)self.thumbnailCollectionView.collectionViewLayout;
    layout.itemSize = CGSizeMake(135, 166);
    
    self.thumbnailCollectionView.opaque = NO;
    self.thumbnailCollectionView.backgroundColor = [UIColor clearColor];
    
    UINib *nib = [UINib nibWithNibName:@"ADXPageThumbnailView" bundle:nil];
    [self.thumbnailCollectionView registerNib:nib
                   forCellWithReuseIdentifier:@"thumbnail"];
}

#pragma mark - UICollectionViewDelegate

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    [self updatePageControl];
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSNumber *pageIndex = [self.sortedPageIndex objectAtIndex:indexPath.item];
    
    [self.pdfController setPage:pageIndex.intValue animated:YES];
    [(ADXPDFViewController*)self.pdfController setAnnotationsVisible:YES];
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.sortedPageIndex.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    ADXPageThumbnailView *cell = [self.thumbnailCollectionView dequeueReusableCellWithReuseIdentifier:@"thumbnail" forIndexPath:indexPath];
    NSNumber *pageNumber = [self.sortedPageIndex objectAtIndex:indexPath.item];
    cell.pageIndex = pageNumber.intValue;
    cell.pdfDocument = self.pdfController.document;

//    NSArray *annotations = [self.pagesWithAnnotations objectForKey:[self.sortedPageIndex objectAtIndex:indexPath.item]];
//    int numAnnotationsOnPage = 0;
//    for (id<ADXAnnotationProtocol>annotation in annotations) {
//        // Only count note annotations, and highlights without an attached note, to avoid double-counting highights with notes.
//        if ([annotation isKindOfClass:[ADXNoteAnnotation class]] ||
//            ([annotation isKindOfClass:[ADXHighlightAnnotation class]] && ![(ADXHighlightAnnotation *)annotation noteAnnotation])) {
//            numAnnotationsOnPage++;
//        }
//    }

    int numAnnotationsOnPage = [[self.pagesWithAnnotations objectForKey:[self.sortedPageIndex objectAtIndex:indexPath.item]] count];
    
    NSString *metaInfo = nil;
    if (numAnnotationsOnPage == 1) {
        metaInfo = NSLocalizedString(@"1 annotation", nil);
    } else {
        metaInfo = [NSString stringWithFormat:NSLocalizedString(@"%d annotations", nil), numAnnotationsOnPage];
    }
    cell.metaInfo = metaInfo;
    return cell;
}

#pragma mark - UIPopoverControllerDelegate

- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController
{
    if (self.currentPopover == popoverController) {
        self.currentPopover = nil;
    }
}
@end
