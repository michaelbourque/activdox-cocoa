//
//  ADXDocVersionsPopoverCell.h
//  Reader
//
//  Created by Gavin McKenzie on 2012-09-13.
//  Copyright (c) 2012 ActivDox Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ActivDoxCore.h"
#import "ADXActivityImageView.h"

extern CGFloat const kADXDocVersionsPopoverCellHeight;
extern CGFloat const kADXDocVersionsPopoverCellWidth;

@interface ADXDocVersionsPopoverCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *thumbnailImageView;
@property (weak, nonatomic) IBOutlet UILabel *versionLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet ADXActivityImageView *spinner;
@property (strong, nonatomic) ADXDocument *document;

@end
