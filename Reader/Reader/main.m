//
//  main.m
//  Reader
//
//  Created by Gavin McKenzie on 12-04-30.
//  Copyright (c) 2012 ActivDox Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ADXAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([ADXAppDelegate class]));
    }
}
