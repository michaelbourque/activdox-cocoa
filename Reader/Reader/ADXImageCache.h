//
//  ADXImageCache.h
//  Reader
//
//  Created by Steve Tibbett on 2013-02-07.
//  Copyright (c) 2013 ActivDox Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ADXImageCache : NSObject

// Designated initializer
- (id)initWithImageSize:(CGSize)imageSize;

// Retrieve an image for this key, if we have one, otherwise returns nil.
- (UIImage *)imageForKey:(NSString *)key;

// Store an image in the cache
- (void)addImage:(UIImage *)image forKey:(NSString *)key;

// Cached images will be this size or smaller; they will be asynchronously resized if too big
@property (nonatomic) CGSize imageSize;

- (void)resetCacheWithImageSize:(CGSize)imageSize;

@end
