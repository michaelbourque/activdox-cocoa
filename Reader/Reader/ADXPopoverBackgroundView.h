//
//  ADXPopoverBackgroundView.h
//  Reader
//
//  Created by Steve Tibbett on 2013-04-17.
//  Copyright (c) 2013 ActivDox Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ADXPopoverBackgroundView : UIPopoverBackgroundView
@property (nonatomic, readwrite) CGFloat arrowOffset;
@property (nonatomic, readwrite) UIPopoverArrowDirection arrowDirection;
@end

// Blue variant
@interface ADXBluePopoverBackgroundView : ADXPopoverBackgroundView
@end
