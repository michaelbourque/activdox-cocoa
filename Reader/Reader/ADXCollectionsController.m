//
//  ADXCollectionsController.m
//  Reader
//
//  Created by Gavin McKenzie on 2012-12-06.
//  Copyright (c) 2012 ActivDox Inc. All rights reserved.
//

#import "ADXCollectionsController.h"
#import "ADXAppDelegate.h"

@interface ADXCollectionsController () <NSFetchedResultsControllerDelegate>
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@end

@implementation ADXCollectionsController

#pragma mark - Initialization

- (id)initWithManagedObjectContext:(NSManagedObjectContext *)moc
{
    self = [super init];
    if (self) {
        _managedObjectContext = moc;
    }
    return self;
}

- (ADXV2Service *)service
{
    ADXV2Service *service = [ADXV2Service serviceForAccountID:[self.dataSource accountIDForCollectionsController:self]];
    return service;
}

#pragma mark - Data Management

- (void)refresh:(BOOL)forceLoadAll
{
    if (forceLoadAll) {
        [self.service forceRefreshCollectionsAndDocuments:nil downloadContent:YES trackProgress:YES];
    } else {
        [self.service refresh];
    }
}

- (NSInteger)numberOfItems
{
    NSInteger result = 0;
    NSInteger numberOfSections = [self numberOfSections];
    
    for (NSInteger i = 0; i < numberOfSections; i++) {
        result += [self numberOfItemsInSection:i];
    }
    
    return result;
}

- (NSInteger)numberOfItemsInSection:(NSInteger)section
{
    id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
    NSInteger result = [sectionInfo numberOfObjects];
    return result;
}

- (NSInteger)numberOfSections
{
    NSInteger result = [[self.fetchedResultsController sections] count];
    return result;
}

- (id)objectAtIndexPath:indexPath
{
    id object = [self.fetchedResultsController objectAtIndexPath:indexPath];
    return object;
}

- (void)deleteCollection:(NSString *)collectionID localID:(NSString *)localID
{
    ADXAppDelegate *appDelegate = (ADXAppDelegate *)[[UIApplication sharedApplication] delegate];
    [appDelegate deleteCollection:collectionID localID:localID completion:^(BOOL success, NSError *error) {
        if (!success) {
            LogNSError(@"Couldn't delete collection", error);
        }
    }];
}

- (void)addDocument:(NSString *)documentID toCollection:(NSString *)collectionID localID:(NSString *)localID
{
    [[ADXAppDelegate sharedInstance] addDocument:documentID toCollection:collectionID localID:localID completion:nil];
}

- (void)removeDocument:(NSString *)documentID fromCollection:(NSString *)collectionID localID:(NSString *)localID
{
    [[ADXAppDelegate sharedInstance] removeDocument:documentID fromCollection:collectionID localID:localID completion:nil];
}

#pragma mark - Data Management

- (void)updateFetchedResultsControllerPredicate
{
    NSString *accountID = [self.dataSource accountIDForCollectionsController:self];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(%K == %@) AND (%K == %@)", ADXCollectionAttributes.accountID, accountID, ADXCollectionAttributes.status, ADXCollectionStatus.accessible];
    [_fetchedResultsController.fetchRequest setPredicate:predicate];
}

- (void)refetchFetchedResultsController
{
    __weak NSFetchedResultsController *frc = _fetchedResultsController;
    
    [self.managedObjectContext performBlockAndWait:^{
        NSError *error = nil;
        BOOL success = [frc performFetch:&error];
        if (!success) {
            LogNSError(@"Couldn't initialize NSFetchedResultsController", error);
        }
    }];
}

- (NSFetchedResultsController *)prepareFetchedResultsController
{
    __weak ADXCollectionsController *weakSelf = self;
    __block NSFetchedResultsController *frc = nil;
    
    NSArray *sortDescriptors = nil;
    
    switch (weakSelf.sortFilter) {
        case ADXCollectionsControllerSortDate:
            // FIXME: Need to sort based on most recently changed document in each collection
            break;
        case ADXCollectionsControllerSortChanges:
            // FIXME: Need to sort based on number of changes
            break;
        case ADXCollectionsControllerSortType:
            sortDescriptors = @[[[NSSortDescriptor alloc] initWithKey:ADXCollectionAttributes.type ascending:YES]];
            break;
        case ADXCollectionsControllerSortTitle:
            sortDescriptors = @[[[NSSortDescriptor alloc] initWithKey:ADXCollectionAttributes.title ascending:YES selector:@selector(localizedCaseInsensitiveCompare:)]];
            break;
        case ADXCollectionsControllerSortTypeCreated:
        default:
            sortDescriptors = @[[[NSSortDescriptor alloc] initWithKey:ADXCollectionAttributes.type ascending:YES],[[NSSortDescriptor alloc] initWithKey:ADXCollectionAttributes.created ascending:YES]];
            break;
    }
    
    [self.managedObjectContext performBlockAndWait:^{
        frc = [weakSelf.managedObjectContext aps_fetchedResultsControllerForEntityName:[ADXCollection entityName]
                                                                             predicate:nil
                                                                       sortDescriptors:sortDescriptors
                                                                    sectionNameKeyPath:nil
                                                                             cacheName:nil
                                                                              delegate:weakSelf];
    }];
    return frc;
}

- (void)prepareSearchFetchedResultsController
{
    //
}

- (NSFetchedResultsController *)fetchedResultsController
{
    if (!_fetchedResultsController) {
        _fetchedResultsController = [self prepareFetchedResultsController];
        [self updateFetchedResultsControllerPredicate];
        [self refetchFetchedResultsController];
    }
    return _fetchedResultsController;
}

- (void)setSortFilter:(ADXCollectionsControllerSort)sortFilter
{
    _sortFilter = sortFilter;
    _fetchedResultsController = nil;
}

#pragma mark - NSFetchedResultsControllerDelegate

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    if (self.delegate) {
        [self.delegate controllerWillChangeContent:controller];
    }
}

- (void)controller:(NSFetchedResultsController *)controller
  didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex
     forChangeType:(NSFetchedResultsChangeType)type
{
    if (self.delegate) {
        [self.delegate controller:controller didChangeSection:sectionInfo atIndex:sectionIndex forChangeType:type];
    }
}

- (void)controller:(NSFetchedResultsController *)controller
   didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath
     forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath
{
    if (self.delegate) {
        [self.delegate controller:controller didChangeObject:anObject atIndexPath:indexPath forChangeType:type newIndexPath:newIndexPath];
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    if (self.delegate) {
        [self.delegate controllerDidChangeContent:controller];
    }
}

@end
