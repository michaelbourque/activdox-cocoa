//
//  ADXStartViewController_iPhone.m
//  Reader
//
//  Created by Gavin McKenzie on 2012-08-03.
//  Copyright (c) 2012 ActivDox Inc. All rights reserved.
//

#import "ADXStartViewController_iPhone.h"
#import "ActivDoxCore.h"
#import "ADXAppDelegate.h"
#import "ADXLoginViewController_iPhone.h"

@interface ADXStartViewController_iPhone ()
@end

@implementation ADXStartViewController_iPhone
@synthesize menuContainerView;

#pragma mark - View Management

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.buildInfoLabel.text = [[ADXAppDelegate sharedInstance] buildDescription];
    
    if (![self firstLaunch]) {
        [self.activityIndicator stopAnimating];
        [self.menuContainerView setHidden:NO];
    } else {
        [self.activityIndicator startAnimating];
        [self.menuContainerView setHidden:YES];
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];

    // Used only to get a screenshot-ready empty view of the app for use as the default.png file.
    if ([self displayBlankStartupScreen]) {
        [self.activityIndicator stopAnimating];
        [self.menuContainerView setHidden:YES];
        return;
    }
    
    if ([self firstLaunch]) {
        ADXAppDelegate *appDelegate = (ADXAppDelegate *)[[UIApplication sharedApplication] delegate];
        [appDelegate.stashedState setValue:[NSNumber numberWithBool:YES] forKey:kADXStartViewControllerAlreadyLaunched];
        
        if (![self.loginProcessor defaultAccount]) {
            [self.activityIndicator stopAnimating];
            [self.menuContainerView setHidden:NO];
        } else {
            [self performSegueWithIdentifier:@"kADXShowLoginView" sender:nil];
        }
    }
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Storyboard

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"kADXShowLoginView"] || [[segue identifier] isEqualToString:@"kADXShowRegistrationView"]) {
        ADXLoginViewController_iPhone *vc = (ADXLoginViewController_iPhone *)[segue destinationViewController];
        vc.loginProcessor = self.loginProcessor;
    }
}

- (void)viewDidUnload {
    [self setMenuContainerView:nil];
    [self setBuildInfoLabel:nil];
    [super viewDidUnload];
}

@end
