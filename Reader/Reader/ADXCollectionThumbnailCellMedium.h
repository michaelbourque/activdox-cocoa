//
//  ADXCollectionThumbnailCellMedium.h
//  Reader
//
//  Created by Gavin McKenzie on 2013-04-28.
//  Copyright (c) 2013 ActivDox Inc. All rights reserved.
//

#import "ADXCollectionViewCell.h"

@interface ADXCollectionThumbnailCellMedium : ADXCollectionViewCell

@end
