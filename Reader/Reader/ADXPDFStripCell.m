//
//  ADXPDFStripCell.m
//  Reader
//
//  Created by Steve Tibbett on 2012-11-29.
//  Copyright (c) 2012 ActivDox Inc. All rights reserved.
//

#import "ADXPDFStripCell.h"
#import <QuartzCore/QuartzCore.h>
#import "ADXNoteAnnotation.h"
#import "PSPDFNoteAnnotationView.h"
#import "ADXNoteAnnotationView.h"

#define CHANGE_CONTEXT 32
#define TOOTH_HEIGHT 3

@implementation ADXPDFStripCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.layer.shadowColor = [UIColor blackColor].CGColor;
        self.layer.shadowOpacity = 0.3;
        self.layer.shadowOffset = CGSizeMake(0.0, 1.0);
        self.layer.shadowRadius = 2.0;
        self.contentMode = UIViewContentModeRedraw;
        self.layer.shouldRasterize = YES;
        self.exclusiveTouch = NO;
        
        self.backgroundColor = [UIColor clearColor];
    }
    return self;
}

- (CGRect)mapPageRectBlock:(CGRect)pageRect
{
    PSPDFPageInfo *pageInfo = [self.pspdfDocument pageInfoForPage:self.stripInfo.pageIndex];

    pageRect.origin.y = (pageInfo.pageRect.size.height - pageRect.origin.y) - pageRect.size.height;
    
    // Scale
    float scale = self.bounds.size.width / pageInfo.pageRect.size.width;
    pageRect.origin.x *= scale;
    pageRect.origin.y *= scale;
    pageRect.size.width *= scale;
    pageRect.size.height *= scale;
    return pageRect;
}

// Returns the extent on the page that a change slice would be displaying given the extent of a change
+ (CGRect)pageRectForStripExtent:(CGRect)changeExtent pageExtent:(CGRect)pageExtent
{
    float ypos;
    float height;

    ypos = changeExtent.origin.y - (CHANGE_CONTEXT+TOOTH_HEIGHT);
    height = ((CHANGE_CONTEXT*2) + changeExtent.size.height);
    
    return CGRectMake(0, ypos, pageExtent.size.width, height);
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    CGContextRef context = UIGraphicsGetCurrentContext();

    float toothHeight = TOOTH_HEIGHT;
    float toothSpacing = 12;
    float toothVariance = 4;

    // Body
    CGContextSetRGBFillColor(context, 1.0, 1.0, 1.0, 1.0);
    CGContextFillRect(context, CGRectMake(0, 0, self.bounds.size.width, self.bounds.size.height));

    PSPDFPageInfo *pageInfo = [self.pspdfDocument pageInfoForPage:self.stripInfo.pageIndex];
    BOOL isFloating = [self.superview isKindOfClass:[UIWindow class]];
    float scale = self.bounds.size.width / pageInfo.pageRect.size.width;
    if (isFloating && UIInterfaceOrientationIsLandscape([UIApplication sharedApplication].statusBarOrientation)) {
        scale = self.bounds.size.height / pageInfo.pageRect.size.width;
    }
    
    CGContextSaveGState(context);
    
    BOOL verticalTeeth = NO;
    
    if (isFloating) {
        // We're on the hook for orientation compensation when our parent is the root window.
        double rotation = 0;

        float changeCenter = -(pageInfo.pageRect.size.height - (self.stripInfo.stripExtent.origin.y + self.stripInfo.stripExtent.size.height));
        changeCenter -= (self.stripInfo.stripExtent.size.height/2); // Offset to the center of the change
        changeCenter *= scale;

        float xTranslate = 0.0;
        
        // rotate image based on orientation
        switch ([UIApplication sharedApplication].statusBarOrientation) {
            case UIInterfaceOrientationLandscapeLeft:
                rotation = -M_PI/2;
                changeCenter += self.bounds.size.width/2; // Offset to the center of the strip (in view coords)
                xTranslate = -self.bounds.size.height;
                verticalTeeth = YES;
                break;
            case UIInterfaceOrientationLandscapeRight:
                rotation = M_PI/2;
                changeCenter -= self.bounds.size.width/2; // Offset to the center of the strip (in view coords)
                verticalTeeth = YES;
                break;
            case UIInterfaceOrientationPortraitUpsideDown:
                rotation = M_PI;
                xTranslate = -self.bounds.size.width;
                changeCenter -= self.bounds.size.height/2; // Offset to the center of the strip (in view coords)
                break;
            default:
                changeCenter += self.bounds.size.height/2; // Offset to the center of the strip (in view coords)
                break;
        }
        
        CGContextRotateCTM(context, rotation);
        CGContextTranslateCTM(context, xTranslate, changeCenter);
        
    } else {
        float changeCenter = -(pageInfo.pageRect.size.height - (self.stripInfo.stripExtent.origin.y + self.stripInfo.stripExtent.size.height));
        // Offset to the center of the change
        changeCenter -= (self.stripInfo.stripExtent.size.height/2);
        changeCenter *= scale;
        // Offset to the center of the strip
        changeCenter += self.bounds.size.height/2;

        CGContextTranslateCTM(context, 0, changeCenter);
    }

    [self.pspdfDocument renderPage:self.stripInfo.pageIndex
                         inContext:context
                          withSize:CGSizeMake(pageInfo.pageRect.size.width * scale, pageInfo.pageRect.size.height * scale)
                     clippedToRect:CGRectZero
                   withAnnotations:self.stripInfo.annotations
                           options:@{ kPSPDFRenderOverlayAnnotations: @YES}];

    if (self.stripInfo.change) {
        [self.highlightedContent drawChange:self.stripInfo.change
                                             onPage:self.stripInfo.pageIndex
                                                   context:context
                                                 withBlock:^CGRect(CGRect pageRect) {
                                                     PSPDFPageInfo *pageInfo = [self.pspdfDocument pageInfoForPage:self.stripInfo.pageIndex];
                                                     pageRect.origin.y = (pageInfo.pageRect.size.height - pageRect.origin.y) - pageRect.size.height;
                                                     pageRect.origin.x *= scale;
                                                     pageRect.origin.y *= scale;
                                                     pageRect.size.width *= scale;
                                                     pageRect.size.height *= scale;
                                                     return pageRect;
                                                 }];
    }

    for (PSPDFAnnotation *annotation in self.stripInfo.annotations) {
        if ([annotation isKindOfClass:[ADXNoteAnnotation class]]) {
            // The annotation is actually a view that PSPDFKit adds to the page view, that renders the
            // comment marker. PSPDFKit assembles the comment marker from a number of images so it can
            // colour it.  Rather than duplicate all that here, we'll instantiate a PSPDFNoteAnnotationView
            // for the annotation, render its layer into an image, and draw that.
            
            ADXNoteAnnotationView *noteView = [[ADXNoteAnnotationView alloc] initWithAnnotation:(PSPDFNoteAnnotation *)annotation];

            UIGraphicsBeginImageContext(noteView.bounds.size);
            [noteView.layer renderInContext:UIGraphicsGetCurrentContext()];
            UIImage *viewImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();

            CGRect customBoundingBox = annotation.boundingBox;
            customBoundingBox.size = CGSizeMake(32, 32);
            customBoundingBox.origin.y -= customBoundingBox.size.height - annotation.boundingBox.size.height;
            PSPDFPageInfo *pageInfo = [self.pspdfDocument pageInfoForPage:self.stripInfo.pageIndex];
            customBoundingBox.origin.y = (pageInfo.pageRect.size.height - customBoundingBox.origin.y) - customBoundingBox.size.height;
            customBoundingBox.origin.x *= scale;
            customBoundingBox.origin.y *= scale;
            
            [viewImage drawInRect:customBoundingBox];
        }
    }
    
    CGContextRestoreGState(context);

    CGContextSaveGState(context);
    CGContextSetRGBFillColor(context, 1.0, 1.0, 1.0, 0.0);
    CGContextSetBlendMode(context, kCGBlendModeClear);
    
    // Draw the torn page (inversely symmetrical)
    float width = (verticalTeeth ? self.bounds.size.height : self.bounds.size.width);
    float pos = 0;
    while (pos < width) {
        float end;
        if (pos > (width - (toothSpacing + toothVariance))) {
            end = width;
        } else
            if (pos > (width - (toothSpacing * 2))) {
                end = pos + (width-pos)/2;
            } else {
                end = (pos + toothSpacing + ((arc4random() % ((int)toothVariance*2)) - toothVariance));
            }
        
        if (verticalTeeth) {
            // Upper teeth
            CGContextMoveToPoint(context, 0, pos);
            CGContextAddLineToPoint(context, toothHeight, pos + ((end-pos)/2));
            CGContextAddLineToPoint(context, 0, end);
            CGContextFillPath(context);
            
            // Lower teeth
            CGContextMoveToPoint(context, self.bounds.size.width, (width-pos));
            CGContextAddLineToPoint(context, self.bounds.size.width-toothHeight, width - (pos + ((end-pos)/2)));
            CGContextAddLineToPoint(context, self.bounds.size.width, width-end);
            CGContextFillPath(context);
        } else {
            // Upper teeth
            CGContextMoveToPoint(context, pos, 0);
            CGContextAddLineToPoint(context, pos + ((end-pos)/2), toothHeight);
            CGContextAddLineToPoint(context, end, 0);
            CGContextFillPath(context);
            
            // Lower teeth
            CGContextMoveToPoint(context, (width-pos), self.bounds.size.height);
            CGContextAddLineToPoint(context, width - (pos + ((end-pos)/2)), self.bounds.size.height-toothHeight);
            CGContextAddLineToPoint(context, width-end, self.bounds.size.height);
            CGContextFillPath(context);
        }
        
        pos = end;
    }

    CGContextRestoreGState(context);
}

@end
