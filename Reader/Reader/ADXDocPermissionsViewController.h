//
//  ADXDocPermissionsViewController.h
//  Reader
//
//  Created by Kei Turner on 2013-09-09.
//  Copyright (c) 2013 ActivDox Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ADXDocPermissionsViewController : UITableViewController{
    NSMutableArray* includedMembers;
}
@property (strong, nonatomic) IBOutlet UIView *permissionsHeaderView;

- (IBAction)addRecipientsAction:(id)sender;
-(id)initWithStyle: (UITableViewStyle)style andIncludedMembers:(NSArray*) members;

@end
