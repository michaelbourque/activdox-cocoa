//
//  ADXNotificationBubbleManager.h
//  Reader
//
//  Created by Gavin McKenzie on 2013-03-26.
//  Copyright (c) 2013 ActivDox Inc. All rights reserved.
//

#import "ADXNotificationBubble.h"

@protocol ADXNotificationBubbleManagerDelegate <NSObject>
- (void)stickyBubbleWasActioned:(ADXNotificationBubble *)bubble;
- (void)stickyBubbleWasCancelled:(ADXNotificationBubble *)bubble;
@end

@interface ADXNotificationBubbleManager : NSObject <ADXNotificationBubbleDelegate>
+ (ADXNotificationBubbleManager *)sharedInstance;
- (ADXNotificationBubble *)displayBubbleWithMessage:(NSString *)message userInfo:(NSDictionary *)userInfo;
- (ADXNotificationBubble *)displayStickyBubbleWithMessage:(NSString *)message actionLabel:(NSString *)actionLabel userInfo:(NSDictionary *)userInfo delegate:(id<ADXNotificationBubbleManagerDelegate>)delegate;
- (void)removeBubble:(ADXNotificationBubble *)bubble;
@end
