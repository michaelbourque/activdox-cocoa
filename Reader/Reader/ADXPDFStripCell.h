//
//  ADXPDFStripCell.h
//  Reader
//
//  Created by Steve Tibbett on 2012-11-29.
//  Copyright (c) 2012 ActivDox Inc. All rights reserved.
//

#import "ActivDoxCore.h"
#import "ADXPDFStripInfo.h"
#import "PSPDFKit.h"

@class ADXPDFStripCell;
@class ADXNoteAnnotation;

@interface ADXPDFStripCell : UICollectionViewCell

@property (strong, nonatomic) ADXPDFStripInfo *stripInfo;
@property (strong, nonatomic) ADXHighlightedContentHelper *highlightedContent;
@property (strong, nonatomic) PSPDFDocument *pspdfDocument;
@property (strong, nonatomic) ADXDocument *adxDocument;
@property (strong, nonatomic) IBOutlet UILongPressGestureRecognizer *longPressRecognizer;

+ (CGRect)pageRectForStripExtent:(CGRect)changeExtent pageExtent:(CGRect)pageExtent;

@end
