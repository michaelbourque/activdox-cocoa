//
//  ADXPDFStripDescriptionCell.h
//  Reader
//
//  Created by Steve Tibbett on 2013-02-11.
//  Copyright (c) 2013 ActivDox Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ADXPDFStripDescriptionCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *labelView;
@end
