// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to ADXUser.m instead.

#import "_ADXUser.h"


const struct ADXUserAttributes ADXUserAttributes = {
	.avatarSeed = @"avatarSeed",
	.createdAt = @"createdAt",
	.displayName = @"displayName",
	.id = @"id",
	.loginName = @"loginName",
	.primaryEmail = @"primaryEmail",
};



const struct ADXUserRelationships ADXUserRelationships = {
	.account = @"account",
	.authoredDocuments = @"authoredDocuments",
	.authoredNotes = @"authoredNotes",
	.originatedEvents = @"originatedEvents",
};






@implementation ADXUserID
@end

@implementation _ADXUser

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"ADXUser" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"ADXUser";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"ADXUser" inManagedObjectContext:moc_];
}

- (ADXUserID*)objectID {
	return (ADXUserID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
	
	if ([key isEqualToString:@"avatarSeedValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"avatarSeed"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}




@dynamic avatarSeed;



- (int32_t)avatarSeedValue {
	NSNumber *result = [self avatarSeed];
	return [result intValue];
}


- (void)setAvatarSeedValue:(int32_t)value_ {
	[self setAvatarSeed:[NSNumber numberWithInt:value_]];
}


- (int32_t)primitiveAvatarSeedValue {
	NSNumber *result = [self primitiveAvatarSeed];
	return [result intValue];
}

- (void)setPrimitiveAvatarSeedValue:(int32_t)value_ {
	[self setPrimitiveAvatarSeed:[NSNumber numberWithInt:value_]];
}





@dynamic createdAt;






@dynamic displayName;






@dynamic id;






@dynamic loginName;






@dynamic primaryEmail;






@dynamic account;

	

@dynamic authoredDocuments;

	
- (NSMutableSet*)authoredDocumentsSet {
	[self willAccessValueForKey:@"authoredDocuments"];
  
	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"authoredDocuments"];
  
	[self didAccessValueForKey:@"authoredDocuments"];
	return result;
}
	

@dynamic authoredNotes;

	
- (NSMutableSet*)authoredNotesSet {
	[self willAccessValueForKey:@"authoredNotes"];
  
	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"authoredNotes"];
  
	[self didAccessValueForKey:@"authoredNotes"];
	return result;
}
	

@dynamic originatedEvents;

	
- (NSMutableSet*)originatedEventsSet {
	[self willAccessValueForKey:@"originatedEvents"];
  
	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"originatedEvents"];
  
	[self didAccessValueForKey:@"originatedEvents"];
	return result;
}
	






@end




