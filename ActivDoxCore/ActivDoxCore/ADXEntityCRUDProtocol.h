//
//  ADXEntityCRUDProtocol.h
//  ActivDoxCore
//
//  Created by Gavin McKenzie on 12-06-06.
//  Copyright (c) 2012 ActivDox Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

//
// NOTE: These operations do NOT internally wrap their work within either a performBlock or performBlockAndWait call.
//       Therefore, it is your responsibility to enclose calls to these methods within a performBlock/Wait method
//       as appropriate.
//

typedef void(^ADXEntityCRUDProtocolCompletionBlock)(BOOL success, NSError *error);

@protocol ADXEntityCRUDProtocol <NSObject>
+ (id)entityWithID:(NSString *)entityID context:(NSManagedObjectContext *)moc;
+ (BOOL)updateEntity:(id)entity fromDictionary:(NSDictionary *)dictionary context:(NSManagedObjectContext *)moc;
+ (id)addEntityFromDictionary:(NSDictionary *)dictionary context:(NSManagedObjectContext *)moc;
+ (NSString*)entityName;
@optional
- (NSDictionary *)jsonDictionary;
@end
