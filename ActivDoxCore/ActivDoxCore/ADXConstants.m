//
//  ADXConstants.m
//  ActivDoxCore
//
//  Created by Gavin McKenzie on 12-04-30.
//  Copyright (c) 2012 ActivDox Inc. All rights reserved.
//

#import "ADXConstants.h"

NSInteger const kADXHttpStatusOk = 200;
