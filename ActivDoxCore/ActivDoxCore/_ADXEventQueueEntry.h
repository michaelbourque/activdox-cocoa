// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to ADXEventQueueEntry.h instead.

#import <CoreData/CoreData.h>



extern const struct ADXEventQueueEntryAttributes {
	__unsafe_unretained NSString *accountID;
	__unsafe_unretained NSString *payload;
	__unsafe_unretained NSString *timeStamp;
	__unsafe_unretained NSString *type;
} ADXEventQueueEntryAttributes;













@class NSObject;





@interface ADXEventQueueEntryID : NSManagedObjectID {}
@end

@interface _ADXEventQueueEntry : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (ADXEventQueueEntryID*)objectID;





@property (nonatomic, strong) NSString* accountID;



//- (BOOL)validateAccountID:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) id payload;



//- (BOOL)validatePayload:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSDate* timeStamp;



//- (BOOL)validateTimeStamp:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* type;



//- (BOOL)validateType:(id*)value_ error:(NSError**)error_;






@end



@interface _ADXEventQueueEntry (CoreDataGeneratedPrimitiveAccessors)


- (NSString*)primitiveAccountID;
- (void)setPrimitiveAccountID:(NSString*)value;




- (id)primitivePayload;
- (void)setPrimitivePayload:(id)value;




- (NSDate*)primitiveTimeStamp;
- (void)setPrimitiveTimeStamp:(NSDate*)value;




@end
