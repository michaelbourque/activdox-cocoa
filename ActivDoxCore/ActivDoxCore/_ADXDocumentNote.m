// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to ADXDocumentNote.m instead.

#import "_ADXDocumentNote.h"


const struct ADXDocumentNoteAttributes ADXDocumentNoteAttributes = {
	.accountID = @"accountID",
	.closeVersion = @"closeVersion",
	.created = @"created",
	.generation = @"generation",
	.id = @"id",
	.localID = @"localID",
	.locationData = @"locationData",
	.openVersion = @"openVersion",
	.safeToDeleteOnStartup = @"safeToDeleteOnStartup",
	.status = @"status",
	.text = @"text",
	.updated = @"updated",
	.visibility = @"visibility",
};



const struct ADXDocumentNoteRelationships ADXDocumentNoteRelationships = {
	.author = @"author",
	.metaDocument = @"metaDocument",
};






@implementation ADXDocumentNoteID
@end

@implementation _ADXDocumentNote

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"ADXDocumentNote" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"ADXDocumentNote";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"ADXDocumentNote" inManagedObjectContext:moc_];
}

- (ADXDocumentNoteID*)objectID {
	return (ADXDocumentNoteID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
	
	if ([key isEqualToString:@"closeVersionValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"closeVersion"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"generationValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"generation"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"openVersionValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"openVersion"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"safeToDeleteOnStartupValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"safeToDeleteOnStartup"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}




@dynamic accountID;






@dynamic closeVersion;



- (int64_t)closeVersionValue {
	NSNumber *result = [self closeVersion];
	return [result longLongValue];
}


- (void)setCloseVersionValue:(int64_t)value_ {
	[self setCloseVersion:[NSNumber numberWithLongLong:value_]];
}


- (int64_t)primitiveCloseVersionValue {
	NSNumber *result = [self primitiveCloseVersion];
	return [result longLongValue];
}

- (void)setPrimitiveCloseVersionValue:(int64_t)value_ {
	[self setPrimitiveCloseVersion:[NSNumber numberWithLongLong:value_]];
}





@dynamic created;






@dynamic generation;



- (int64_t)generationValue {
	NSNumber *result = [self generation];
	return [result longLongValue];
}


- (void)setGenerationValue:(int64_t)value_ {
	[self setGeneration:[NSNumber numberWithLongLong:value_]];
}


- (int64_t)primitiveGenerationValue {
	NSNumber *result = [self primitiveGeneration];
	return [result longLongValue];
}

- (void)setPrimitiveGenerationValue:(int64_t)value_ {
	[self setPrimitiveGeneration:[NSNumber numberWithLongLong:value_]];
}





@dynamic id;






@dynamic localID;






@dynamic locationData;






@dynamic openVersion;



- (int64_t)openVersionValue {
	NSNumber *result = [self openVersion];
	return [result longLongValue];
}


- (void)setOpenVersionValue:(int64_t)value_ {
	[self setOpenVersion:[NSNumber numberWithLongLong:value_]];
}


- (int64_t)primitiveOpenVersionValue {
	NSNumber *result = [self primitiveOpenVersion];
	return [result longLongValue];
}

- (void)setPrimitiveOpenVersionValue:(int64_t)value_ {
	[self setPrimitiveOpenVersion:[NSNumber numberWithLongLong:value_]];
}





@dynamic safeToDeleteOnStartup;



- (BOOL)safeToDeleteOnStartupValue {
	NSNumber *result = [self safeToDeleteOnStartup];
	return [result boolValue];
}


- (void)setSafeToDeleteOnStartupValue:(BOOL)value_ {
	[self setSafeToDeleteOnStartup:[NSNumber numberWithBool:value_]];
}


- (BOOL)primitiveSafeToDeleteOnStartupValue {
	NSNumber *result = [self primitiveSafeToDeleteOnStartup];
	return [result boolValue];
}

- (void)setPrimitiveSafeToDeleteOnStartupValue:(BOOL)value_ {
	[self setPrimitiveSafeToDeleteOnStartup:[NSNumber numberWithBool:value_]];
}





@dynamic status;






@dynamic text;






@dynamic updated;






@dynamic visibility;






@dynamic author;

	

@dynamic metaDocument;

	






@end




