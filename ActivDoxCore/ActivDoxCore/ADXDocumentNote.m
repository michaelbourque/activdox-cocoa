 

#if TARGET_OS_IPHONE
#import <UIKit/UIKit.h>
#endif

#import "ADXDocumentNote.h"
#import "ADXModel.h"
#import "ADXCommonMacros.h"

NSString *kADXDocumentNoteDidChangeNotification = @"kADXDocumentNoteDidChangeNotification";

const struct ADXJSONDocumentNoteResponseAttributes ADXJSONDocumentNoteResponseAttributes = {
    .resourcesArray = @"resources",
    .resources = {
        .usersArray = @"resources.users",
        .documentsArray = @"resources.documents",
    },
    .notesArray = @"notes",
};

const struct ADXJSONDocumentNoteAttributes ADXJSONDocumentNoteAttributes = {
    .notesArray = @"notes",
    .note = {
        .id = @"id",
        .localID = @"localID",
        .originator = @"originator",
        .created = @"created",
        .updated = @"updated",
        .generation = @"generation",
        .openVersion = @"openVersion",
        .closeVersion = @"closeVersion",
        .text = @"text",
        .document = @"document",
        .visibility = @"visibility",
        .status = @"status",
        .versionsArray = @"versions",
        .versions = {
            .version = @"version",
            .locationArray = @"location",
            .location = {
                .page = @"page",
                .highlightArray = @"highlights",
                .highlight = {
                    .left = @"left",
                    .top = @"top",
                    .right = @"right",
                    .bottom = @"bottom",
                },
            },
        }
    },
};

const struct ADXDocumentNoteVisibility ADXDocumentNoteVisibility = {
    .personal = @"Personal",
    .author = @"Author",
    .everyone = @"Everyone",
};

const struct ADXDocumentNoteStatus ADXDocumentNoteStatus = {
    .accessible = @"Accessible",
    .revoked = @"Revoked",
    .deleted = @"Deleted",
    .inaccessible = @"inaccessible",
};

@implementation ADXDocumentNote

#pragma mark - ADXEntityCRUDProtocol

+ (id)entityWithID:(NSString *)entityID context:(NSManagedObjectContext *)moc
{
    NSAssert(NO, @"ADXDocumentNote entityWithID not supported. Use ADXDocumentNote noteWithID:accountID: instead.");
    return nil;
}

+ (ADXDocumentNote *)noteWithID:(NSString *)noteID accountID:(NSString *)accountID context:(NSManagedObjectContext *)moc
{
    if (IsEmpty(noteID) || IsEmpty(accountID)) {
        LogError(@"noteWithLocalID received a nil note ID or accountID for note: %@", self);
        return nil;
    }

    ADXDocumentNote *result = [moc aps_fetchFirstObjectForEntityName:[ADXDocumentNote entityName] withPredicate:@"(%K == %@) AND (%K == %@)",
                               ADXDocumentNoteAttributes.id, noteID,
                               ADXDocumentNoteAttributes.accountID, accountID];
    return result;
}

+ (ADXDocumentNote *)noteWithLocalID:(NSString *)noteLocalID accountID:(NSString *)accountID context:(NSManagedObjectContext *)moc
{
    if (IsEmpty(noteLocalID) || IsEmpty(accountID)) {
        LogError(@"noteWithLocalID received a nil localID (%@) or accountID (%@)", noteLocalID, accountID);
        return nil;
    }
    
    ADXDocumentNote *result = [moc aps_fetchFirstObjectForEntityName:[ADXDocumentNote entityName] withPredicate:@"(%K == %@) AND (%K == %@)",
                               ADXDocumentNoteAttributes.localID, noteLocalID,
                               ADXDocumentNoteAttributes.accountID, accountID];
    return result;
}

+ (NSArray *)notesToBeDeletedOnStartup:(NSManagedObjectContext *)moc
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K == YES", ADXDocumentNoteAttributes.safeToDeleteOnStartup];
    NSArray *notes = [moc aps_fetchObjectsForEntityName:[ADXDocumentNote entityName] withPredicate:predicate];
    return notes;
}

+ (BOOL)updateEntity:(id)entity fromDictionary:(NSDictionary *)dictionary context:(NSManagedObjectContext *)moc
{
    static ISO8601DateFormatter *dateFormatter = nil;
    if (!dateFormatter) {
        dateFormatter = [[ISO8601DateFormatter alloc] init];
    }

    if (!entity || IsEmpty(dictionary) || !moc) {
        LogError(@"Received bad params");
        return NO;
    }
    
    ADXDocumentNote *note = entity;
    note.safeToDeleteOnStartup = @NO;
    
    // A note may not have an ID until it has round-tripped with the server and been assigned a server-generated ID.
    if (IsEmpty(note.id)) {
        NSString *noteID = [dictionary aps_objectForKeyOrNil:ADXJSONDocumentNoteAttributes.note.id];
        if (IsEmpty(noteID)) {
            LogTrace(@"No note id was supplied in dictionary");
        } else {
            note.id = noteID;
        }
    }

    NSString *accountID = [dictionary aps_objectForKeyOrNil:ADXDocumentNoteAttributes.accountID];
    if (IsEmpty(accountID)) {
        LogError(@"Cannot update a note without an account id.");
        return NO;
    }
    note.accountID = accountID;
    
    NSString *localID = [dictionary aps_objectForKeyOrNil:ADXDocumentNoteAttributes.localID];
    if (IsEmpty(localID)) {
        note.localID = [ADXAccount generateLocalIDForAccountID:accountID];
    } else {
        note.localID = localID;
    }

    // FIXME: We need a quick way to fetch notes from Core Data for a given page. Unfortunately the JSON format for notes allows a note to exist in multiple locations, essentially a range of locations that might span pages. This makes it difficult to lookup a note for a given page without inspecting each element of a note's location array. Ideally a note would include a firstPage and lastPage property outside of the location data array, where firstPage is the page number where the note begins and lastPage is the page number where the note terminates. This would allow us to query Core Data for notes where the document id matches and where the current page number is inclusively between the firstPage and lastPage property values. If the server doesn't one day evolve to expose these properties, we could derive this information upon import of a note. However, we'd then also need to know if/when a note was updated and re-inspect the note location data when a note has changed. For now, I'm doing none of this. After a brief discussion with Richard we agreed that it is acceptable to not support page-spanning notes at this time, and instead simply peek into the first location data element of a note and use the associated page number as the page number for the entire note.
    
    NSArray *versions = (NSArray *)[dictionary aps_objectForKeyOrNil:ADXJSONDocumentNoteAttributes.note.versionsArray];
    if (versions.count) {
        note.locationData = versions;
    }
    
    note.text = [dictionary aps_objectForKeyOrNil:ADXJSONDocumentNoteAttributes.note.text];
    note.created = [dateFormatter dateFromString:[dictionary aps_objectForKeyOrNil:ADXJSONDocumentNoteAttributes.note.created]];
    note.updated = [dateFormatter dateFromString:[dictionary aps_objectForKeyOrNil:ADXJSONDocumentNoteAttributes.note.updated]];
    note.visibility = [dictionary aps_objectForKeyOrNil:ADXJSONDocumentNoteAttributes.note.visibility];
    note.status = [dictionary aps_objectForKeyOrNil:ADXJSONDocumentNoteAttributes.note.status];
    note.generation = [dictionary aps_objectForKeyOrNil:ADXJSONDocumentNoteAttributes.note.generation];
    note.openVersion = [dictionary aps_objectForKeyOrNil:ADXJSONDocumentNoteAttributes.note.openVersion];
    note.closeVersion = [dictionary aps_objectForKey:ADXJSONDocumentNoteAttributes.note.closeVersion defaultIfNotFound:@(0)];

    ADXUser *author = [ADXUser entityWithID:[dictionary aps_objectForKeyOrNil:ADXJSONDocumentNoteAttributes.note.originator] context:moc];
    if (!author) {
        LogError(@"Could not locate author id %@ for note id %@", [dictionary aps_objectForKeyOrNil:ADXJSONDocumentNoteAttributes.note.originator], note.id);
        return NO;
    }
    note.author = author;

    if (!note.metaDocument) {
        NSString *documentID = [dictionary aps_objectForKeyOrNil:ADXJSONDocumentNoteAttributes.note.document];
        ADXMetaDocument *metaDocument = [ADXMetaDocument metaDocumentWithID:documentID accountID:accountID context:moc];
        if (!metaDocument) {
            LogError(@"No meta-document could be established for document with ID %@", documentID);
            return NO;
        }
        note.metaDocument = metaDocument;
    }

    LogTrace(@"Updated note: %@", note);

    return YES;
}

+ (id)addEntityFromDictionary:(NSDictionary *)dictionary context:(NSManagedObjectContext *)moc
{
    if (IsEmpty(dictionary)) {
        LogError(@"Received a nil dictionary.");
        return nil;
    }
    
    NSString *noteID = [dictionary objectForKey:ADXJSONDocumentNoteAttributes.note.id];
    if (IsEmpty(noteID)) {
        LogError(@"Received a dictionary with no note ID");
        return nil;
    }
    
    NSString *accountID = [dictionary objectForKey:ADXDocumentNoteAttributes.accountID];
    if (IsEmpty(accountID)) {
        LogError(@"Received a dictionary with no account ID");
        return nil;
    }
    
    ADXDocumentNote *note = [ADXDocumentNote noteWithID:noteID accountID:accountID context:moc];
    if (note) {
        LogError(@"Note with id %@ already exists.", noteID);
        return nil;
    }
    
    ADXDocumentNote *newNote = [ADXDocumentNote insertInManagedObjectContext:moc];
    
    NSString *localID = [dictionary aps_objectForKeyOrNil:ADXDocumentNoteAttributes.localID];
    if (IsEmpty(localID)) {
        newNote.localID = [ADXAccount generateLocalIDForAccountID:accountID];
    }

    LogTrace(@"Inserted note with id %@", [dictionary aps_objectForKeyOrNil:ADXJSONDocumentNoteAttributes.note.id]);
    
    BOOL success = [ADXDocumentNote updateEntity:newNote fromDictionary:dictionary context:moc];
    if (success) {
        return newNote;
    } else {
        return nil;
    }
}


+ (BOOL)updateNotes:(NSArray *)notes accountID:(NSString *)accountID context:(NSManagedObjectContext *)moc
{
    BOOL success = YES;
    
    if (IsEmpty(notes)) {
        LogError(@"Received a nil/empty array");
        return NO;
    }
    
    for (NSDictionary *dictionary in notes) {
        NSString *noteID = [dictionary objectForKey:ADXJSONDocumentNoteAttributes.note.id];
        if (IsEmpty(noteID)) {
            LogError(@"Received a dictionary with no note ID");
            return NO;
        }
        
        ADXDocumentNote *note = [ADXDocumentNote noteWithID:noteID accountID:accountID context:moc];
        if (!note) {
            NSString *noteID = [dictionary objectForKey:ADXJSONDocumentNoteAttributes.note.id];
            note = [ADXDocumentNote noteWithID:noteID accountID:accountID context:moc];
            if (!note) {
                NSString *noteLocalID = [dictionary objectForKey:ADXJSONDocumentNoteAttributes.note.localID];
                note = [ADXDocumentNote noteWithLocalID:noteLocalID accountID:accountID context:moc];
            }
        }
        
        BOOL addOrUpdateSuccess = NO;

        NSString *status = [dictionary aps_objectForKeyOrNil:ADXDocumentNoteAttributes.status];
        if ([status isEqualToString:ADXDocumentNoteStatus.deleted] || [status isEqualToString:ADXDocumentNoteStatus.revoked]) {
            note.status = status;
            note.safeToDeleteOnStartup = @YES;
            addOrUpdateSuccess = YES;
        } else {
            // Add or update
            NSDictionary *noteProperties = [dictionary aps_dictionaryMergedWithDictionary:@{ADXDocumentNoteAttributes.accountID : accountID}];
            if (note) {
                addOrUpdateSuccess = [ADXDocumentNote updateEntity:note fromDictionary:noteProperties context:moc];
            } else {
                ADXDocumentNote *newNote = [ADXDocumentNote addEntityFromDictionary:noteProperties context:moc];
                if (newNote) {
                    addOrUpdateSuccess = YES;
                } else {
                    addOrUpdateSuccess = NO;
                }
            }
        }
        
        if (!addOrUpdateSuccess) {
            success = NO;
            break;
        }
    }
    
    return success;
}

#pragma mark - Convenience

//- (void)setText:(NSString *)text
//{
//    [super setText:text];
//}

- (BOOL)isAccessible
{
    return [self.status isEqualToString:ADXDocumentNoteStatus.accessible];
}

- (BOOL)isDeletedStatus
{
    return [self.status isEqualToString:ADXDocumentNoteStatus.deleted];
}

- (BOOL)isRevoked
{
    return [self.status isEqualToString:ADXDocumentNoteStatus.revoked];
}

- (BOOL)isInaccessible
{
    return [self.status isEqualToString:ADXDocumentNoteStatus.inaccessible];
}

#pragma mark - Note Insertion

+ (ADXDocumentNote *)insertNoteByUser:(ADXUser *)user forDocument:(ADXDocument *)document page:(NSUInteger)page text:(NSString *)text highlightRects:(NSArray *)highlightRects
{
    NSAssert(page, @"insertNoteByUser received a page number of zero, but expects page numbers to be 1-indexed.");
    
    NSManagedObjectContext *moc = document.managedObjectContext;
    NSError *error = nil;
    ADXUser *userInDocumentContext = (ADXUser *)[moc existingObjectWithID:user.objectID error:&error];
    NSAssert(userInDocumentContext, @"insertNoteByUser could not retrieve user in document context");
    
    ADXDocumentNote *newNote = [ADXDocumentNote insertInManagedObjectContext:moc];
    if (newNote) {
        newNote.safeToDeleteOnStartup = @NO;
        newNote.localID = [ADXAccount generateLocalIDForAccountID:user.account.id];
        newNote.created = [NSDate date];
        newNote.updated = nil;
        newNote.generation = nil;
        newNote.author = userInDocumentContext;
        newNote.accountID = document.accountID;
        newNote.openVersion = document.version;
        [newNote unsetClosed];

        ADXMetaDocument *metaDocument = [ADXMetaDocument metaDocumentWithID:document.id accountID:document.accountID context:moc];
        if (!metaDocument) {
            LogError(@"No meta-document could be established for document with ID %@", document.id);
            return NO;
        }
        newNote.metaDocument = metaDocument;

        newNote.text = text;
        newNote.visibility = ADXDocumentNoteVisibility.personal;
        newNote.status = ADXDocumentNoteStatus.accessible;
        
        NSMutableDictionary *locationDict = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                             [NSNumber numberWithUnsignedInteger:page], ADXJSONDocumentNoteAttributes.note.versions.location.page,
                                             nil];
        if (highlightRects) {
            NSMutableArray *translatedRects = [[NSMutableArray alloc] init];
            for (NSValue *rectValue in highlightRects) {
#if TARGET_OS_IPHONE
                CGRect rect = [rectValue CGRectValue];
                NSDictionary *rectDict = [ADXDocumentNote dictionaryFromRect:rect];
#else
                NSRect rect = [rectValue rectValue];
                NSDictionary *rectDict = [ADXDocumentNote dictionaryFromRect:NSRectToCGRect(rect)];
#endif
                [translatedRects addObject:rectDict];
            }
            [locationDict setObject:translatedRects forKey:ADXJSONDocumentNoteAttributes.note.versions.location.highlightArray];
        }

        NSArray *versions = @[@{ADXJSONDocumentNoteAttributes.note.versions.version: document.version,
                                ADXJSONDocumentNoteAttributes.note.versions.locationArray: @[locationDict]}];
        newNote.locationData = versions;
    }
    return newNote;
}

- (void)saveWithOptionalSync:(BOOL)queueForSync completion:(ADXEntityCRUDProtocolCompletionBlock)completionBlock
{
    __weak ADXDocumentNote *weakSelf = self;
    NSManagedObjectContext *moc = self.managedObjectContext;
    
    [moc performBlock:^{
        if (!self.created) {
            self.created = [NSDate date];
        }
        self.updated = [NSDate date];
        
        NSError *saveError = nil;
        BOOL success = [moc save:&saveError];
        if (!success) {
            LogNSError(@"couldn't save document note", saveError);
        }
        if (completionBlock) {
            dispatch_async(dispatch_get_main_queue(), ^{
                completionBlock(success, (success ? nil : saveError));
            });
        }
        
        if (queueForSync) {
            [weakSelf enqueueForSyncInContext:moc completion:^(BOOL success, NSError *error) {
                if (!success) {
                    LogNSError(@"couldn't enqueue document note for sync", error);
                }
            }];
        }
    }];
}

- (void)enqueueForSyncInContext:(NSManagedObjectContext *)moc completion:(ADXEntityCRUDProtocolCompletionBlock)completionBlock
{
    ADXSyncQueueEntry *syncEntry = nil;

    NSMutableDictionary *payload = [self mutableJSONDictionary];

    if (!self.isDeletedStatus) {
        if (IsEmpty(self.id)) {
            syncEntry = [ADXSyncQueueEntry insertEntryForEntity:self id:self.localID accountID:self.accountID operation:ADXSyncQueueEntryOperation.insert payload:payload userInfo:nil context:moc];
        } else {
            syncEntry = [ADXSyncQueueEntry insertEntryForEntity:self id:self.id accountID:self.accountID operation:ADXSyncQueueEntryOperation.update payload:payload userInfo:nil context:moc];
        }
    } else {
        syncEntry = [ADXSyncQueueEntry insertEntryForEntity:self id:self.id accountID:self.accountID operation:ADXSyncQueueEntryOperation.remove payload:payload userInfo:nil context:moc];
    }
    
    [syncEntry save:^(BOOL success, NSError *error) {
        if (!success) {
            LogNSError(@"Unable to enqueue a synchronization request after saving note.", error);
            if (completionBlock) {
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                    completionBlock(success, error);
                });
            }
            return;
        }
    }];
}

- (void)markDeleted
{
    self.status = ADXDocumentNoteStatus.deleted;
}

#pragma mark - Location Data

- (NSArray *)deepMutableLocationDataCopy
{
    NSError *error = nil;
    NSData *data = nil;
    
    if (![NSJSONSerialization isValidJSONObject:self.locationData]) {
        LogError(@"Can't serialize location data to JSON: %@", self.locationData);
        return nil;
    }
    
    data = [NSJSONSerialization dataWithJSONObject:self.locationData options:0 error:&error];
    if (!data) {
        LogNSError(@"Couldn't serialize location data to make a mutable deep copy", error);
        return nil;
    }
    
    NSMutableArray *mutableLocationData = [NSJSONSerialization JSONObjectWithData:data options:(NSJSONReadingMutableContainers | NSJSONReadingMutableLeaves) error:&error];
    if (!mutableLocationData) {
        LogNSError(@"Couldn't deserialize location data to make a mutable deep copy", error);
    }
    return mutableLocationData;
}

- (NSArray *)locationsForVersion:(NSNumber *)versionID
{
    NSArray *result = nil;

    NSArray *versions = (NSArray *)self.locationData;
    if (IsEmpty(versions)) {
        return nil;
    }
   
    for (NSDictionary *version in versions) {
        NSNumber *locationVersion = (NSNumber *)version[ADXJSONDocumentNoteAttributes.note.versions.version];
        if ([locationVersion isEqualToNumber:versionID]) {
            NSArray *locationData = version[ADXJSONDocumentNoteAttributes.note.versions.locationArray];
            result = locationData;
            break;
        }
    }
    
    return result;
}

- (NSArray *)locationsForVersion:(NSNumber *)versionID page:(NSNumber *)page
{
    NSMutableArray *result = [[NSMutableArray alloc] init];
    NSArray *locationsForVersion = [self locationsForVersion:versionID];

    for (NSDictionary *location in locationsForVersion) {
        NSNumber *locationPage = location[ADXJSONDocumentNoteAttributes.note.versions.location.page];
        if ([locationPage isEqualToNumber:page]) {
            [result addObject:location];
        }
    }
    return result;
}

- (NSSet *)pagesForVersion:(NSNumber *)versionID
{
    NSArray *locations = [self orderedPagesForVersion:versionID];
    NSSet *result = [NSSet setWithArray:locations];
    return result;
}

- (NSArray *)orderedPagesForVersion:(NSNumber *)versionID
{
    NSArray *locations = [self locationsForVersion:versionID];
    NSMutableArray *pages = [[NSMutableArray alloc] init];

    for (NSDictionary *locationDict in locations) {
        NSNumber *page = locationDict[ADXJSONDocumentNoteAttributes.note.versions.location.page];
        [pages addObject:page];
    }
    
    NSSortDescriptor *ascendingSort = [NSSortDescriptor sortDescriptorWithKey:@"self" ascending:YES];
    [pages sortUsingDescriptors:[NSArray arrayWithObject:ascendingSort]];
    
    return [NSArray arrayWithArray:pages];
}

- (NSInteger)firstpageForVersion:(NSNumber *)versionID
{
    NSInteger result = 0;
    NSArray *pages = [self orderedPagesForVersion:versionID];
    if (pages.count) {
        NSNumber *page = pages[0];
        result = [page integerValue];
    }
    return result;
}

- (void)moveNoteToPoint:(CGPoint)point inVersion:(NSNumber *)versionID
{
    NSDictionary *pointDict = [ADXDocumentNote dictionaryFromRect:CGRectMake(point.x, point.y, 0, 0)];
   
    // First create a mutable copy of the location data
    NSArray *mutableLocationData = [self deepMutableLocationDataCopy];
    if (!mutableLocationData) {
        return;
    }
    self.locationData = mutableLocationData;
    
    // Replace the position with a new dictionary rect for this point
    NSArray *locations = [self locationsForVersion:versionID];
    NSAssert(locations.count == 1, @"Notes should have exactly one location");
    
    NSMutableDictionary *location = [locations objectAtIndex:0];
    location[ADXJSONDocumentNoteAttributes.note.versions.location.highlightArray] = @[pointDict];
}

#pragma mark - JSON Encoding

#if TARGET_OS_IPHONE
+ (NSDictionary *)dictionaryFromRect:(CGRect)rect
{
    CGRect translatedRect = rect;
#else
+ (NSDictionary *)dictionaryFromRect:(NSRect)rect
{
    NSRect translatedRect = NSRectToCGRect(rect);
#endif
    NSDictionary *dictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                [NSNumber numberWithFloat:CGRectGetMinX(translatedRect)], ADXJSONDocumentNoteAttributes.note.versions.location.highlight.left,
                                [NSNumber numberWithFloat:CGRectGetMinY(translatedRect)], ADXJSONDocumentNoteAttributes.note.versions.location.highlight.bottom,
                                [NSNumber numberWithFloat:CGRectGetMaxX(translatedRect)], ADXJSONDocumentNoteAttributes.note.versions.location.highlight.right,
                                [NSNumber numberWithFloat:CGRectGetMaxY(translatedRect)], ADXJSONDocumentNoteAttributes.note.versions.location.highlight.top,
                                nil];
    
    return dictionary;
}

+ (CGFloat)floatValueForKey:(NSString *)key dictionary:(NSDictionary *)dictionary
{
    CGFloat result = 0.0f;
    NSString *valueString = nil;
    id<NSObject> dictObject = [dictionary objectForKey:key];
    
    if ([dictObject isKindOfClass:[NSString class]]) {
        NSScanner *scanner = [NSScanner scannerWithString:(NSString *)dictObject];
        NSCharacterSet *charset = [NSCharacterSet characterSetWithCharactersInString:@"01234567890."];
        [scanner scanCharactersFromSet:charset intoString:&valueString];
        result = [valueString floatValue];
    } else if ([dictObject isKindOfClass:[NSNumber class]]) {
        result = [(NSNumber *)dictObject floatValue];
    }

    return result;
}

#if TARGET_OS_IPHONE
+ (CGRect)rectFromDictionary:(NSDictionary *)dictionary
#else
+ (NSRect)rectFromDictionary:(NSDictionary *)dictionary
#endif
{
    CGRect rect = CGRectZero;
    
    CGFloat left = [ADXDocumentNote floatValueForKey:ADXJSONDocumentNoteAttributes.note.versions.location.highlight.left dictionary:dictionary];
    CGFloat top = [ADXDocumentNote floatValueForKey:ADXJSONDocumentNoteAttributes.note.versions.location.highlight.top dictionary:dictionary];
    CGFloat right = [ADXDocumentNote floatValueForKey:ADXJSONDocumentNoteAttributes.note.versions.location.highlight.right dictionary:dictionary];
    CGFloat bottom = [ADXDocumentNote floatValueForKey:ADXJSONDocumentNoteAttributes.note.versions.location.highlight.bottom dictionary:dictionary];
    
    rect = CGRectMake(left, bottom, ABS(right - left), ABS(top - bottom));

#if TARGET_OS_IPHONE
    return rect;
#else 
    return NSRectFromCGRect(rect);
#endif
}

- (BOOL)isClosed
{
    BOOL result = YES;
    if (self.closeVersion == nil || self.closeVersion.integerValue == 0) {
        result = NO;
    }
    return result;
}

- (void)unsetClosed
{
    self.closeVersion = @(0);
}

- (NSDictionary *)jsonDictionary
{
    return [NSDictionary dictionaryWithDictionary:[self mutableJSONDictionary]];
}
    
- (NSMutableDictionary *)mutableJSONDictionary
{
    static ISO8601DateFormatter *dateFormatter = nil;
    if (!dateFormatter) {
        dateFormatter = [[ISO8601DateFormatter alloc] init];
        dateFormatter.format = ISO8601DateFormatCalendar;
        dateFormatter.includeTime = YES;
    }
    
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] initWithObjectsAndKeys:self.author.id, ADXJSONDocumentNoteAttributes.note.originator,
                                       self.metaDocument.documentID, ADXJSONDocumentNoteAttributes.note.document,
                                       nil];
    if (!IsEmpty(self.id)) {
        [dictionary setObject:self.id forKey:ADXJSONDocumentNoteAttributes.note.id];
    }
    if (!IsEmpty(self.localID)) {
        [dictionary setObject:self.localID forKey:ADXJSONDocumentNoteAttributes.note.localID];
    }
    
    if (self.openVersion) {
        [dictionary setObject:self.openVersion forKey:ADXJSONDocumentNoteAttributes.note.openVersion];
    }
    
    if (self.isClosed) {
        [dictionary setObject:self.closeVersion forKey:ADXJSONDocumentNoteAttributes.note.closeVersion];
    } else {
        [dictionary setObject:@(0) forKey:ADXJSONDocumentNoteAttributes.note.closeVersion];
    }

    if (self.generation) {
        [dictionary setObject:self.generation forKey:ADXJSONDocumentNoteAttributes.note.generation];
    }

    if (self.created) {
        [dictionary setObject:[dateFormatter stringFromDate:self.created] forKey:ADXJSONDocumentNoteAttributes.note.created];
    }
    if (self.updated) {
        [dictionary setObject:[dateFormatter stringFromDate:self.updated] forKey:ADXJSONDocumentNoteAttributes.note.updated];
    }
    
    // I don't use IsEmpty here because it returnes YES for zero-length strings, and a zero-length string is a valid note that should be persisted as such.
    if (self.text) {
        [dictionary setObject:self.text forKey:ADXJSONDocumentNoteAttributes.note.text];
    }
    
    if (!IsEmpty(self.status)) {
        [dictionary setObject:self.status forKey:ADXJSONDocumentNoteAttributes.note.status];
    }
    if (!IsEmpty(self.visibility)) {
        [dictionary setObject:self.visibility forKey:ADXJSONDocumentNoteAttributes.note.visibility];
    }
    
    if (self.locationData) {
        [dictionary setObject:self.locationData forKey:ADXJSONDocumentNoteAttributes.note.versionsArray];
    }
    
    return dictionary;
}
    
- (BOOL)shouldBeDeletedOnStartup
{
    BOOL result = self.safeToDeleteOnStartupValue || (!self.isAccessible);
    return result;
}

@end
