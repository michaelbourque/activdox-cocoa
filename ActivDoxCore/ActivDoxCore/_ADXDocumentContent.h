// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to ADXDocumentContent.h instead.

#import <CoreData/CoreData.h>



extern const struct ADXDocumentContentAttributes {
	__unsafe_unretained NSString *data;
} ADXDocumentContentAttributes;



extern const struct ADXDocumentContentRelationships {
	__unsafe_unretained NSString *document;
} ADXDocumentContentRelationships;






@class ADXDocument;




@interface ADXDocumentContentID : NSManagedObjectID {}
@end

@interface _ADXDocumentContent : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (ADXDocumentContentID*)objectID;





@property (nonatomic, strong) NSData* data;



//- (BOOL)validateData:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) ADXDocument *document;

//- (BOOL)validateDocument:(id*)value_ error:(NSError**)error_;





@end



@interface _ADXDocumentContent (CoreDataGeneratedPrimitiveAccessors)


- (NSData*)primitiveData;
- (void)setPrimitiveData:(NSData*)value;





- (ADXDocument*)primitiveDocument;
- (void)setPrimitiveDocument:(ADXDocument*)value;


@end
