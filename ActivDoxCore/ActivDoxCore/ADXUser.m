
#import "ADXUser.h"
#import "ADXModel.h"
#import "ADXCommonMacros.h"

const struct ADXJSONUserAttributes ADXJSONUserAttributes = {
	.id = @"id",
    .loginName = @"loginName",
    .displayName = @"displayName",
};

@implementation ADXUser

#pragma mark - ADXEntityCRUDProtocol

+ (id)entityWithID:(NSString *)entityID context:(NSManagedObjectContext *)moc
{
    ADXUser *result = [moc aps_fetchFirstObjectForEntityName:[ADXUser entityName] withPredicate:@"%K == %@", ADXUserAttributes.id, entityID];
    return result;
}

+ (BOOL)updateEntity:(id)entity fromDictionary:(NSDictionary *)dictionary context:(NSManagedObjectContext *)moc
{    
    if (!entity || IsEmpty(dictionary) || !moc) {
        LogError(@"Received bad params");
        return NO;
    }

    ADXUser *user = entity;
    if (IsEmpty(user.id)) {
        NSString *userID = [dictionary aps_objectForKeyOrNil:ADXJSONUserAttributes.id];
        if (IsEmpty(userID)) {
            LogError(@"No user id was supplied in dictionary");
            return NO;
        }
        user.id = userID;
    }
    
    user.createdAt = [NSDate date];
    user.loginName = [dictionary aps_objectForKey:ADXJSONUserAttributes.loginName defaultIfNotFound:user.loginName];
    user.displayName = [dictionary aps_objectForKey:ADXJSONUserAttributes.displayName defaultIfNotFound:user.displayName];
    
    LogTrace(@"Updated user: %@", user);

    return YES;
}

+ (id)addEntityFromDictionary:(NSDictionary *)dictionary context:(NSManagedObjectContext *)moc
{
    if (IsEmpty(dictionary)) {
        LogError(@"Received a nil dictionary.");
        return nil;
    }
    
    NSString *userID = [dictionary objectForKey:ADXJSONUserAttributes.id];
    if (IsEmpty(userID)) {
        LogError(@"Received a dictionary with no user ID");
        return nil;
    }
    
    ADXUser *user = [ADXUser entityWithID:userID context:moc];
    if (user) {
        LogError(@"User with id %@ already exists.", userID);
        return nil;
    }

    // Determine the next available avatarSeed value
    int avatarSeed = 1;
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    request.entity = [NSEntityDescription entityForName:[ADXUser entityName]
                                 inManagedObjectContext:moc];
    request.fetchLimit = 1;
    request.sortDescriptors = @[ [[NSSortDescriptor alloc] initWithKey:ADXUserAttributes.avatarSeed ascending:NO] ];
    NSError *error = nil;
    NSArray *results = [moc executeFetchRequest:request error:&error];
    if (results.count > 0) {
        ADXUser *user = (ADXUser *)results[0];
        avatarSeed = user.avatarSeedValue + 1;
    }

    ADXUser *newUser = [ADXUser insertInManagedObjectContext:moc];
    newUser.avatarSeedValue = avatarSeed;

    LogTrace(@"Inserted user with id %@", [dictionary aps_objectForKeyOrNil:ADXJSONUserAttributes.id]);

    BOOL success = [ADXUser updateEntity:newUser fromDictionary:dictionary context:moc];
    if (success) {
        return newUser;
    } else {
        return nil;
    }
}

+ (BOOL)addUsers:(NSArray *)users updateExisting:(BOOL)updateExisting context:(NSManagedObjectContext *)moc
{
    BOOL success = YES;
    
    for (NSDictionary *dictionary in users) {
        NSString *userID = [dictionary objectForKey:ADXJSONUserAttributes.id];
        if (IsEmpty(userID)) {
            LogError(@"Received a dictionary with no user ID");
            return NO;
        }

        ADXUser *user = [ADXUser entityWithID:userID context:moc];
        BOOL addOrUpdateSuccess = NO;
        
        if (user) {
            if (updateExisting) {
                addOrUpdateSuccess = [ADXUser updateEntity:user fromDictionary:dictionary context:moc];
            } else {
                addOrUpdateSuccess = YES;
            }
        } else {
            ADXUser *newUser = [ADXUser addEntityFromDictionary:dictionary context:moc];
            if (newUser) {
                addOrUpdateSuccess = YES;
            } else {
                addOrUpdateSuccess = NO;
            }
        }
        
        if (!addOrUpdateSuccess) {
            success = NO;
            break;
        }
    }
    
    return success;
}

- (NSString *)displayNameOrLoginName
{
    NSString *result = self.displayName;
    if (IsEmpty(result)) {
        result = self.loginName;
    }
    return result;
}

@end
