
#import "_ADXEvent.h"
#import "ADXEntityCRUDProtocol.h"

extern const struct ADXEventType {
	__unsafe_unretained NSString *documentPrefix;
	__unsafe_unretained NSString *documentDownloaded;
    __unsafe_unretained NSString *documentOpened;
    __unsafe_unretained NSString *documentClosed;
    __unsafe_unretained NSString *documentPageOpened;
    __unsafe_unretained NSString *documentPageClosed;
    __unsafe_unretained NSString *documentPublished;
    __unsafe_unretained NSString *documentUpdated;
    __unsafe_unretained NSString *documentPropertiesUpdated;
    __unsafe_unretained NSString *documentReadersAdded;
    __unsafe_unretained NSString *documentReadersRemoved;
    __unsafe_unretained NSString *documentReadersPublicDocumentRequest;
} ADXEventType;

extern const struct ADXJSONEventResponseAttributes {
    __unsafe_unretained NSString *resourcesDict;
    struct {
        __unsafe_unretained NSString *users;
        __unsafe_unretained NSString *usersArray;
        __unsafe_unretained NSString *documents;
        __unsafe_unretained NSString *documentsArray;
    } resources;
    __unsafe_unretained NSString *eventsDict;
} ADXJSONEventResponseAttributes;

extern const struct ADXJSONEventAttributes {
    __unsafe_unretained NSString *id;
    __unsafe_unretained NSString *originator;
    __unsafe_unretained NSString *timestamp;
    __unsafe_unretained NSString *eventType;
    __unsafe_unretained NSString *payloadDict;
    struct {
        __unsafe_unretained NSString *document;
        __unsafe_unretained NSString *version;
        __unsafe_unretained NSString *page;
    } payload;
} ADXJSONEventAttributes;


@interface ADXEvent : _ADXEvent 
+ (id)eventWithID:(NSString *)eventID accountID:(NSString *)accountID context:(NSManagedObjectContext *)moc;
+ (BOOL)addOrUpdateEvent:(NSDictionary *)dictionary resources:(NSDictionary *)resources accountID:(NSString *)accountID context:(NSManagedObjectContext *)moc;
+ (BOOL)updateEvents:(NSArray *)events resources:(NSDictionary *)resources accountID:(NSString *)accountID filterOutEventsFromAccount:(NSString *)filterAccountID onlyEventTypes:(NSSet *)eventTypes context:(NSManagedObjectContext *)moc;
- (NSString *)shortDescription;
- (NSString *)fullDescription;
@end
