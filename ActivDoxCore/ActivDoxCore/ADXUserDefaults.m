//
//  ADXUserDefaults.m
//  ActivDoxCore
//
//  Created by Gavin McKenzie on 12-05-28.
//  Copyright (c) 2012 ActivDox Inc. All rights reserved.
//

#import "ADXUserDefaults.h"
#import "ADXCommonMacros.h"

NSString * const kADXDefaultKeyCurrentlyOpenDocumentID = @"kADXDefaultKeyCurrentlyOpenDocumentID";
NSString * const kADXDefaultKeyCurrentlyOpenDocumentVersion = @"kADXDefaultKeyCurrentlyOpenDocumentVersion";
NSString * const kADXDefaultKeyCurrentlyOpenDocumentPage = @"kADXDefaultKeyCurrentlyOpenDocumentPage";
NSString * const kADXDefaultKeyCurrentlyOpenCollectionID = @"kADXDefaultKeyCurrentlyOpenCollectionID";
NSString * const kADXDefaultKeyCurrentlyOpenCollectionLocalID = @"kADXDefaultKeyCurrentlyOpenCollectionLocalID";
NSString * const kADXDefaultKeyCurrentDeviceToken = @"kADXDefaultKeyCurrentDeviceToken";
NSString * const kADXDefaultKeyPreviousDeviceToken = @"kADXDefaultKeyPreviousDeviceToken";
NSString * const kADXDefaultMostRecentAccountID = @"kADXDefaultMostRecentAccountID";
NSString * const kADXDefaultMakeAllDocsShareable = @"all_docs_shareable_preference";
NSString * const kADXDefaultPeriodicRefreshAllow = @"allow_periodic_refresh_preference";
NSString * const kADXDefaultPeriodicRefreshSeconds = @"periodic_refresh_preference";
NSString * const kADXDefaultKeyCurrentDocumentBrowserDisplayMode = @"kADXDefaultKeyCurrentDocumentBrowserDisplayMode";
NSString * const kADXDefaultDocumentSortOrder = @"kADXDefaultDocumentSortOrder";
NSString * const kADXDefaultUserLoggedOut = @"kADXDefaultUserLoggedOut";
NSString * const kADXDefaultRequireLoginOnNextLaunch = @"require_login_on_next_launch_preference";
NSString * const kADXDefaultDisplayRemoteNotifications = @"display_inapp_notifications_preference";
NSString * const kADXDefaultDidShutdownDirty = @"kADXDefaultDidShutdownDirty";
NSString * const kADXDefaultEnableActivityStream = @"enable_activity_stream_preference";
NSString * const kADXDefaultOfflineLoginWarning = @"offline_login_warning";
NSString * const kADXDefaultCurrentlySelectedDocumentTab = @"kADXDefaultCurrentlySelectedDocumentTab";
NSString * const kADXDefaultNoteVisibility = @"kADXDefaultNoteVisibility";

NSString * const kADXDefaultAnnotationFilterShowNotes = @"kADXDefaultAnnotationFilterShowNotes";
NSString * const kADXDefaultAnnotationFilterShowHighlights = @"kADXDefaultAnnotationFilterShowHighlights";
NSString * const kADXDefaultAnnotationFilterShowChanges = @"kADXDefaultAnnotationFilterShowChanges";
NSString * const kADXDefaultAnnotationFilterIncludeMyNotes = @"kADXDefaultNnnotationFilterIncludeMyNotes";
NSString * const kADXDefaultAnnotationFilterIncludeAuthorNotes = @"kADXDefaultAnnotationFilterIncludeAuthorNotes";
NSString * const kADXDefaultAnnotationFilterIncludeOtherNotes = @"kADXDefaultAnnotationFilterIncludeOtherNotes";
NSString * const kADXDefaultAnnotationFilterIncludeResolved = @"kADXDefaultAnnotationFilterIncludeResolved";
NSString * const kADXDefaultShouldOpenLastReadVersion = @"kADXDefaultOpenLastReadVersion";

NSString * const kADXDefaultActivityViewFilterThisDocument = @"kADXDefaultActivityViewFilterThisDocument";
NSString * const kADXDefaultActivityViewFilterThisCollection = @"kADXDefaultActivityViewFilterThisCollection";

NSString * const kADXDefaultUseAlternativeColours = @"kADXDefaultUseAlternativeColours";

@implementation ADXUserDefaults

+ (ADXUserDefaults *)sharedDefaults
{
    static ADXUserDefaults *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[ADXUserDefaults alloc] init];
        [sharedInstance registerInitialSettings];
    });
    return sharedInstance;
}

- (void)registerInitialSettings
{
    NSString *pathStr = [[NSBundle mainBundle] bundlePath];
    NSString *settingsBundlePath = [pathStr stringByAppendingPathComponent:@"Settings.bundle"];
    NSString *finalPath = [settingsBundlePath stringByAppendingPathComponent:@"Root.plist"];
    
    NSDictionary *settingsDict = [NSDictionary dictionaryWithContentsOfFile:finalPath];
    NSArray *prefSpecifierArray = [settingsDict objectForKey:@"PreferenceSpecifiers"];

    NSMutableDictionary *defaultValues = [[NSMutableDictionary alloc] init];
    for (NSDictionary *prefItem in prefSpecifierArray) {
        NSString *key = [prefItem objectForKey:@"Key"];
        NSString *type = [prefItem objectForKey:@"Type"];
        if ([type isEqualToString:@"PSGroupSpecifier"] || [type isEqualToString:@"PSChildPaneSpecifier"]) {
            continue;
        }
        
        id defaultValue = [prefItem objectForKey:@"DefaultValue"];
        defaultValues[key] = defaultValue;
    }
    
    [[NSUserDefaults standardUserDefaults] registerDefaults:defaultValues];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)flush
{
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)resetAll
{
    [self setCurrentlyOpenDocumentID:nil];
    [self setCurrentDeviceToken:nil];
    [self setPreviousDeviceToken:nil];
}

- (NSString *)currentlyOpenDocumentID
{
    return [[NSUserDefaults standardUserDefaults] stringForKey:kADXDefaultKeyCurrentlyOpenDocumentID];    
}

- (void)setCurrentlyOpenDocumentID:(NSString *)documentID
{
    if (IsEmpty(documentID)) {
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:kADXDefaultKeyCurrentlyOpenDocumentID];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:kADXDefaultKeyCurrentlyOpenDocumentVersion];
    } else {
        [[NSUserDefaults standardUserDefaults] setObject:documentID forKey:kADXDefaultKeyCurrentlyOpenDocumentID];
    }
    [self flush];
}

- (NSInteger)currentlyOpenDocumentVersion
{
    return [[NSUserDefaults standardUserDefaults] integerForKey:kADXDefaultKeyCurrentlyOpenDocumentVersion];
}

- (void)setCurrentlyOpenDocumentVersion:(NSInteger)currentlyOpenDocumentVersion
{
    [[NSUserDefaults standardUserDefaults] setInteger:currentlyOpenDocumentVersion forKey:kADXDefaultKeyCurrentlyOpenDocumentVersion];
    [self flush];
}

- (NSInteger)currentlyOpenDocumentPage
{
    return [[NSUserDefaults standardUserDefaults] integerForKey:kADXDefaultKeyCurrentlyOpenDocumentPage];
}

- (void)setCurrentlyOpenDocumentPage:(NSInteger)currentlyOpenDocumentPage
{
    [[NSUserDefaults standardUserDefaults] setInteger:currentlyOpenDocumentPage forKey:kADXDefaultKeyCurrentlyOpenDocumentPage];
    [self flush];
}

- (void)setCurrentlyOpenDocumentID:(NSString *)currentlyOpenDocumentID version:(NSInteger)version
{
    @synchronized(self) {
        [self setCurrentlyOpenDocumentID:currentlyOpenDocumentID];
        [self setCurrentlyOpenDocumentVersion:version];
    }
}

- (void)clearCurrentlyOpenDocument
{
    @synchronized(self) {
        [self setCurrentlyOpenDocumentID:nil];
        [self setCurrentlyOpenDocumentVersion:0];
    }
}

- (NSString *)currentlyOpenCollectionID
{
    return [[NSUserDefaults standardUserDefaults] stringForKey:kADXDefaultKeyCurrentlyOpenCollectionID];
}

- (NSString *)currentlyOpenCollectionLocalID
{
    return [[NSUserDefaults standardUserDefaults] stringForKey:kADXDefaultKeyCurrentlyOpenCollectionLocalID];
}

- (void)setCurrentlyOpenCollectionID:(NSString *)collectionID
{
    if (IsEmpty(collectionID)) {
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:kADXDefaultKeyCurrentlyOpenCollectionID];
    } else {
        [[NSUserDefaults standardUserDefaults] setObject:collectionID forKey:kADXDefaultKeyCurrentlyOpenCollectionID];
    }
    [self flush];
}

- (void)setCurrentlyOpenCollectionLocalID:(NSString *)localID
{
    if (IsEmpty(localID)) {
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:kADXDefaultKeyCurrentlyOpenCollectionLocalID];
    } else {
        [[NSUserDefaults standardUserDefaults] setObject:localID forKey:kADXDefaultKeyCurrentlyOpenCollectionLocalID];
    }
    [self flush];
}

- (void)setCurrentlyOpenCollectionID:(NSString *)collectionID localID:(NSString *)localID
{
    [self setCurrentlyOpenCollectionID:collectionID];
    [self setCurrentlyOpenCollectionLocalID:localID];
    [self flush];
}

// Getter and setter for currentDeviceToken
- (NSData *)currentDeviceToken
{
    return [[NSUserDefaults standardUserDefaults] dataForKey:kADXDefaultKeyCurrentDeviceToken];    
}

- (void)setCurrentDeviceToken:(NSData *)deviceToken
{
    if (IsEmpty(deviceToken)) {
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:kADXDefaultKeyCurrentDeviceToken];
    } else {
        [[NSUserDefaults standardUserDefaults] setObject:deviceToken forKey:kADXDefaultKeyCurrentDeviceToken];
    }
    [self flush];
}

// Getter and setter for lastDeviceToken
- (NSData *)previousDeviceToken
{
    return [[NSUserDefaults standardUserDefaults] dataForKey:kADXDefaultKeyPreviousDeviceToken];    
}

- (void)setPreviousDeviceToken:(NSData *)deviceToken
{
    if (IsEmpty(deviceToken)) {
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:kADXDefaultKeyPreviousDeviceToken];
    } else {
        [[NSUserDefaults standardUserDefaults] setObject:deviceToken forKey:kADXDefaultKeyPreviousDeviceToken];
    }
    [self flush];
}

- (BOOL)allDocsShareableOverride
{
    BOOL result = NO;
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:kADXDefaultMakeAllDocsShareable]) {
        result = [[NSUserDefaults standardUserDefaults] boolForKey:kADXDefaultMakeAllDocsShareable];
    }
    
    return result;
}

- (BOOL)allowPeriodicRefresh
{
    BOOL result = NO;
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:kADXDefaultPeriodicRefreshAllow]) {
        result = [[NSUserDefaults standardUserDefaults] boolForKey:kADXDefaultPeriodicRefreshAllow];
    }
    
    return result;
}

- (NSUInteger)periodicRefreshSeconds
{
    NSUInteger result = 0;
    NSString *refreshPref = [[NSUserDefaults standardUserDefaults] stringForKey:kADXDefaultPeriodicRefreshSeconds];
    result = [refreshPref integerValue];
    return result;
}

- (BOOL)offlineLoginWarning
{
    BOOL result = NO;
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:kADXDefaultOfflineLoginWarning]) {
        result = [[NSUserDefaults standardUserDefaults] boolForKey:kADXDefaultOfflineLoginWarning];
    }
    
    return result;
}

- (NSUInteger) currentDocumentBrowserDisplayMode
{
    NSUInteger result = 0;
    
    if ([[NSUserDefaults standardUserDefaults] integerForKey:kADXDefaultKeyCurrentDocumentBrowserDisplayMode]) {
        result = [[NSUserDefaults standardUserDefaults] integerForKey:kADXDefaultKeyCurrentDocumentBrowserDisplayMode];
    }
    
    return result;
}

- (void)setCurrentDocumentBrowserDisplayMode:(NSUInteger)currentDocumentBrowserDisplayMode
{
    [[NSUserDefaults standardUserDefaults] setInteger:currentDocumentBrowserDisplayMode forKey:kADXDefaultKeyCurrentDocumentBrowserDisplayMode];
    [self flush];
}

- (void) setDocumentSortOrder:(NSUInteger)documentSortOrder
{
    [[NSUserDefaults standardUserDefaults] setInteger:documentSortOrder forKey:kADXDefaultDocumentSortOrder];
    [self flush];
}

- (NSUInteger)documentSortOrder
{
    NSUInteger result = 0;
    
    if ([[NSUserDefaults standardUserDefaults] integerForKey:kADXDefaultDocumentSortOrder]) {
        result = [[NSUserDefaults standardUserDefaults] integerForKey:kADXDefaultDocumentSortOrder];
    }
    return result;
}

- (BOOL)userLoggedOut
{
    BOOL result = NO;
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:kADXDefaultUserLoggedOut]) {
        result = [[NSUserDefaults standardUserDefaults] boolForKey:kADXDefaultUserLoggedOut];
    }
    
    return result;
}

- (void)setUserLoggedOut:(BOOL)userLoggedOutOfPreviousSession
{
    [[NSUserDefaults standardUserDefaults] setBool:userLoggedOutOfPreviousSession forKey:kADXDefaultUserLoggedOut];
    [self flush];
}

- (BOOL)requireLoginOnNextLaunch
{
    BOOL result = NO;
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:kADXDefaultRequireLoginOnNextLaunch]) {
        result = [[NSUserDefaults standardUserDefaults] boolForKey:kADXDefaultRequireLoginOnNextLaunch];
    }
    
    return result;
}

- (void)setRequireLoginOnNextLaunch:(BOOL)requireLoginOnNextLaunch
{
    [[NSUserDefaults standardUserDefaults] setBool:requireLoginOnNextLaunch forKey:kADXDefaultRequireLoginOnNextLaunch];
    [self flush];
}

- (BOOL)activityStreamEnabled
{
    BOOL result = YES;
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:kADXDefaultEnableActivityStream]) {
        result = [[NSUserDefaults standardUserDefaults] boolForKey:kADXDefaultEnableActivityStream];
    }
    
    return result;
}

- (void)setActivityStreamEnabled:(BOOL)activityStreamEnabled
{
    [[NSUserDefaults standardUserDefaults] setBool:activityStreamEnabled forKey:kADXDefaultEnableActivityStream];
    [self flush];
}

- (BOOL)displayRemoteNotifications
{
    BOOL result = NO;
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:kADXDefaultDisplayRemoteNotifications]) {
        result = [[NSUserDefaults standardUserDefaults] boolForKey:kADXDefaultDisplayRemoteNotifications];
    }
    
    return result;
}

- (void)setDisplayRemoteNotifications:(BOOL)displayRemoteNotifications
{
    [[NSUserDefaults standardUserDefaults] setBool:displayRemoteNotifications forKey:kADXDefaultDisplayRemoteNotifications];
    [self flush];
}

- (BOOL)didShutdownDirty
{
    BOOL result = NO;
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:kADXDefaultDidShutdownDirty]) {
        result = [[NSUserDefaults standardUserDefaults] boolForKey:kADXDefaultDidShutdownDirty];
    }
    
    return result;
}

- (void)setDidShutdownDirty:(BOOL)didShutdownDirty
{
    [[NSUserDefaults standardUserDefaults] setBool:didShutdownDirty forKey:kADXDefaultDidShutdownDirty];
    [self flush];
}

- (ADXSelectedDocumentTab)currentlySelectedDocumentTab
{
    ADXSelectedDocumentTab selectedTab = (ADXSelectedDocumentTab)[[NSUserDefaults standardUserDefaults] integerForKey:kADXDefaultCurrentlySelectedDocumentTab];
    return selectedTab;
}

- (void)setCurrentlySelectedDocumentTab:(ADXSelectedDocumentTab)selectedTab
{
    [[NSUserDefaults standardUserDefaults] setInteger:selectedTab forKey:kADXDefaultCurrentlySelectedDocumentTab];
    [self flush];
}

- (BOOL)annotationFilterShowNotes
{
    BOOL result = YES;
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:kADXDefaultAnnotationFilterShowNotes]) {
        result = [[NSUserDefaults standardUserDefaults] boolForKey:kADXDefaultAnnotationFilterShowNotes];
    }
    
    return result;
}

- (void)setAnnotationFilterShowNotes:(BOOL)value
{
    [[NSUserDefaults standardUserDefaults] setBool:value forKey:kADXDefaultAnnotationFilterShowNotes];
    [self flush];
}

- (BOOL)annotationFilterShowHighlights
{
    BOOL result = YES;
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:kADXDefaultAnnotationFilterShowHighlights]) {
        result = [[NSUserDefaults standardUserDefaults] boolForKey:kADXDefaultAnnotationFilterShowHighlights];
    }
    
    return result;
}

- (void)setAnnotationFilterShowHighlights:(BOOL)value
{
    [[NSUserDefaults standardUserDefaults] setBool:value forKey:kADXDefaultAnnotationFilterShowHighlights];
    [self flush];
}

- (BOOL)annotationFilterIncludeMyNotes
{
    BOOL result = YES;
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:kADXDefaultAnnotationFilterIncludeMyNotes]) {
        result = [[NSUserDefaults standardUserDefaults] boolForKey:kADXDefaultAnnotationFilterIncludeMyNotes];
    }
    
    return result;
}

- (void)setAnnotationFilterIncludeMyNotes:(BOOL)value
{
    [[NSUserDefaults standardUserDefaults] setBool:value forKey:kADXDefaultAnnotationFilterIncludeMyNotes];
    [self flush];
}

- (BOOL)annotationFilterIncludeAuthorNotes
{
    BOOL result = YES;
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:kADXDefaultAnnotationFilterIncludeAuthorNotes]) {
        result = [[NSUserDefaults standardUserDefaults] boolForKey:kADXDefaultAnnotationFilterIncludeAuthorNotes];
    }
    
    return result;
}

- (void)setAnnotationFilterIncludeAuthorNotes:(BOOL)value
{
    [[NSUserDefaults standardUserDefaults] setBool:value forKey:kADXDefaultAnnotationFilterIncludeAuthorNotes];
    [self flush];
}

- (BOOL)annotationFilterIncludeOtherNotes
{
    BOOL result = YES;
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:kADXDefaultAnnotationFilterIncludeOtherNotes]) {
        result = [[NSUserDefaults standardUserDefaults] boolForKey:kADXDefaultAnnotationFilterIncludeOtherNotes];
    }
    
    return result;
}

- (void)setAnnotationFilterIncludeOtherNotes:(BOOL)value
{
    [[NSUserDefaults standardUserDefaults] setBool:value forKey:kADXDefaultAnnotationFilterIncludeOtherNotes];
    [self flush];
}

- (BOOL)annotationFilterIncludeResolved
{
    BOOL result = YES;
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:kADXDefaultAnnotationFilterIncludeResolved]) {
        result = [[NSUserDefaults standardUserDefaults] boolForKey:kADXDefaultAnnotationFilterIncludeResolved];
    }
    
    return result;
}

- (void)setAnnotationFilterIncludeResolved:(BOOL)value
{
    [[NSUserDefaults standardUserDefaults] setBool:value forKey:kADXDefaultAnnotationFilterIncludeResolved];
    [self flush];
}



- (BOOL)annotationFilterShowChanges
{
    BOOL result = YES;
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:kADXDefaultAnnotationFilterShowChanges]) {
        result = [[NSUserDefaults standardUserDefaults] boolForKey:kADXDefaultAnnotationFilterShowChanges];
    }
    
    return result;
}

- (void)setAnnotationFilterShowChanges:(BOOL)value
{
    [[NSUserDefaults standardUserDefaults] setBool:value forKey:kADXDefaultAnnotationFilterShowChanges];
    [self flush];
}

- (BOOL)shouldOpenLastReadVersion
{
    return [[NSUserDefaults standardUserDefaults] boolForKey:kADXDefaultShouldOpenLastReadVersion];
}

- (void)setShouldOpenLastReadVersion:(BOOL)value
{
    [[NSUserDefaults standardUserDefaults] setBool:value forKey:kADXDefaultShouldOpenLastReadVersion];
    [self flush];
}

- (NSString *)defaultNoteVisibility
{
    NSString *visibility = [[NSUserDefaults standardUserDefaults] stringForKey:kADXDefaultNoteVisibility];
    if (visibility == nil) {
        return ADXDocumentNoteVisibility.personal;
    } else {
        BOOL isValid = [visibility isEqualToString:ADXDocumentNoteVisibility.personal] || [visibility isEqualToString:ADXDocumentNoteVisibility.author] || [visibility isEqualToString:ADXDocumentNoteVisibility.everyone];
        NSAssert(isValid, @"Unrecognized default note visibility");
        if (!isValid) {
            visibility = ADXDocumentNoteVisibility.personal;
        }
        return visibility;
    }
}

- (void)setDefaultNoteVisibility:(NSString *)noteVisibility
{
    [[NSUserDefaults standardUserDefaults] setObject:noteVisibility forKey:kADXDefaultNoteVisibility];
    [self flush];
}


- (BOOL)activityViewFilterThisDocument
{
    return [[NSUserDefaults standardUserDefaults] boolForKey:kADXDefaultActivityViewFilterThisDocument];
}

- (void)setActivityViewFilterThisDocument:(BOOL)value
{
    [[NSUserDefaults standardUserDefaults] setBool:value forKey:kADXDefaultActivityViewFilterThisDocument];
    [self flush];
}

- (BOOL)activityViewFilterThisCollection
{
    return [[NSUserDefaults standardUserDefaults] boolForKey:kADXDefaultActivityViewFilterThisCollection];
}

- (void)setActivityViewFilterThisCollection:(BOOL)value
{
    [[NSUserDefaults standardUserDefaults] setBool:value forKey:kADXDefaultActivityViewFilterThisCollection];
    [self flush];
}

- (BOOL)useAlternativeColors
{
    return [[NSUserDefaults standardUserDefaults] boolForKey:kADXDefaultUseAlternativeColours];
}

- (void)setUseAlternativeColors:(BOOL)value
{
    [[NSUserDefaults standardUserDefaults] setBool:value forKey:kADXDefaultUseAlternativeColours];
    [self flush];
}

@end
