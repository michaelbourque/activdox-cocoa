// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to ADXDocumentContent.m instead.

#import "_ADXDocumentContent.h"


const struct ADXDocumentContentAttributes ADXDocumentContentAttributes = {
	.data = @"data",
};



const struct ADXDocumentContentRelationships ADXDocumentContentRelationships = {
	.document = @"document",
};






@implementation ADXDocumentContentID
@end

@implementation _ADXDocumentContent

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"ADXDocumentContent" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"ADXDocumentContent";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"ADXDocumentContent" inManagedObjectContext:moc_];
}

- (ADXDocumentContentID*)objectID {
	return (ADXDocumentContentID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
	

	return keyPaths;
}




@dynamic data;






@dynamic document;

	






@end




