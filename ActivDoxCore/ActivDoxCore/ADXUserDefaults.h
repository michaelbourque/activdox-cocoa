//
//  ADXUserDefaults.h
//  ActivDoxCore
//
//  Created by Gavin McKenzie on 12-05-28.
//  Copyright (c) 2012 ActivDox Inc. All rights reserved.
//

//
//  This class is intended to offer high-level services similar to NSUserDefaults.
//  In fact, the implementation may use NSUserDefaults under the covers.
//  The motivation for this class is to give us an easier path to choose a different
//  storage mechanism (e.g. more secure) than NSUserDefaults, or to potentially
//  offer synchronization of these settings across apps via the ActivDox server.
//

#import <Foundation/Foundation.h>
#import "ADXModel.h"

typedef enum {
    ADXSelectedDocumentTabRevisions = 0,
    ADXSelectedDocumentTabAnnotations,
    ADXSelectedDocumentTabSearch,
} ADXSelectedDocumentTab;

@interface ADXUserDefaults : NSObject
@property (strong, nonatomic) NSString *currentlyOpenDocumentID;
@property (assign, nonatomic) NSInteger currentlyOpenDocumentVersion;
@property (assign, nonatomic) NSInteger currentlyOpenDocumentPage;
@property (strong, nonatomic) NSString *currentlyOpenDocumentServerURL;
@property (strong, nonatomic) NSString *currentlyOpenCollectionID;
@property (strong, nonatomic) NSString *currentlyOpenCollectionLocalID;
@property (strong, nonatomic) NSData *currentDeviceToken;
@property (strong, nonatomic) NSData *previousDeviceToken;
@property (assign, nonatomic) NSUInteger currentDocumentBrowserDisplayMode;
@property (assign, nonatomic) NSUInteger documentSortOrder;

@property (readonly, nonatomic) BOOL transmitEventsToServer;
@property (readonly, nonatomic) BOOL allDocsShareableOverride;
@property (readonly, nonatomic) BOOL allowPeriodicRefresh;
@property (readonly, nonatomic) NSUInteger periodicRefreshSeconds;
@property (assign, nonatomic) BOOL userLoggedOut;
@property (assign, nonatomic) BOOL requireLoginOnNextLaunch;
@property (assign, nonatomic) BOOL displayRemoteNotifications;
@property (assign, nonatomic) BOOL didShutdownDirty;
@property (assign, nonatomic) BOOL activityStreamEnabled;
@property (assign, nonatomic) BOOL offlineLoginWarning;
@property (assign, nonatomic) ADXSelectedDocumentTab currentlySelectedDocumentTab;

@property (assign, nonatomic) BOOL annotationFilterShowNotes;
@property (assign, nonatomic) BOOL annotationFilterShowHighlights;
@property (assign, nonatomic) BOOL annotationFilterShowChanges;
@property (assign, nonatomic) BOOL annotationFilterIncludeMyNotes;
@property (assign, nonatomic) BOOL annotationFilterIncludeAuthorNotes;
@property (assign, nonatomic) BOOL annotationFilterIncludeOtherNotes;
@property (assign, nonatomic) BOOL annotationFilterIncludeResolved;

@property (assign, nonatomic) BOOL activityViewFilterThisDocument;
@property (assign, nonatomic) BOOL activityViewFilterThisCollection;

@property (assign, nonatomic) BOOL useAlternativeColors;

@property (assign, nonatomic) NSString *defaultNoteVisibility;

@property (assign, nonatomic) BOOL shouldOpenLastReadVersion;

+ (ADXUserDefaults *)sharedDefaults;
- (void)resetAll;
- (void)setCurrentlyOpenDocumentID:(NSString *)currentlyOpenDocumentID version:(NSInteger)version;
- (void)setCurrentlyOpenCollectionID:(NSString *)collectionID localID:(NSString *)localID;
- (void)clearCurrentlyOpenDocument;
@end
