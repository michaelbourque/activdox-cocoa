// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to ADXDocument.m instead.

#import "_ADXDocument.h"


const struct ADXDocumentAttributes ADXDocumentAttributes = {
	.accountID = @"accountID",
	.changeContentDownloaded = @"changeContentDownloaded",
	.changeCount = @"changeCount",
	.changedPages = @"changedPages",
	.documentDescription = @"documentDescription",
	.downloaded = @"downloaded",
	.errorMessage = @"errorMessage",
	.id = @"id",
	.isPublic = @"isPublic",
	.message = @"message",
	.mostRecentOpenedDate = @"mostRecentOpenedDate",
	.noteCount = @"noteCount",
	.pageCount = @"pageCount",
	.safeToDeleteOnStartup = @"safeToDeleteOnStartup",
	.status = @"status",
	.title = @"title",
	.uploadDate = @"uploadDate",
	.version = @"version",
	.versionIsLatest = @"versionIsLatest",
};



const struct ADXDocumentRelationships ADXDocumentRelationships = {
	.author = @"author",
	.collections = @"collections",
	.content = @"content",
	.highlightedContent = @"highlightedContent",
	.metaDocument = @"metaDocument",
	.thumbnail = @"thumbnail",
};






@implementation ADXDocumentID
@end

@implementation _ADXDocument

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"ADXDocument" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"ADXDocument";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"ADXDocument" inManagedObjectContext:moc_];
}

- (ADXDocumentID*)objectID {
	return (ADXDocumentID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
	
	if ([key isEqualToString:@"changeContentDownloadedValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"changeContentDownloaded"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"changeCountValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"changeCount"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"downloadedValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"downloaded"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"isPublicValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"isPublic"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"noteCountValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"noteCount"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"pageCountValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"pageCount"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"safeToDeleteOnStartupValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"safeToDeleteOnStartup"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"versionValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"version"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"versionIsLatestValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"versionIsLatest"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}




@dynamic accountID;






@dynamic changeContentDownloaded;



- (BOOL)changeContentDownloadedValue {
	NSNumber *result = [self changeContentDownloaded];
	return [result boolValue];
}


- (void)setChangeContentDownloadedValue:(BOOL)value_ {
	[self setChangeContentDownloaded:[NSNumber numberWithBool:value_]];
}


- (BOOL)primitiveChangeContentDownloadedValue {
	NSNumber *result = [self primitiveChangeContentDownloaded];
	return [result boolValue];
}

- (void)setPrimitiveChangeContentDownloadedValue:(BOOL)value_ {
	[self setPrimitiveChangeContentDownloaded:[NSNumber numberWithBool:value_]];
}





@dynamic changeCount;



- (int64_t)changeCountValue {
	NSNumber *result = [self changeCount];
	return [result longLongValue];
}


- (void)setChangeCountValue:(int64_t)value_ {
	[self setChangeCount:[NSNumber numberWithLongLong:value_]];
}


- (int64_t)primitiveChangeCountValue {
	NSNumber *result = [self primitiveChangeCount];
	return [result longLongValue];
}

- (void)setPrimitiveChangeCountValue:(int64_t)value_ {
	[self setPrimitiveChangeCount:[NSNumber numberWithLongLong:value_]];
}





@dynamic changedPages;






@dynamic documentDescription;






@dynamic downloaded;



- (BOOL)downloadedValue {
	NSNumber *result = [self downloaded];
	return [result boolValue];
}


- (void)setDownloadedValue:(BOOL)value_ {
	[self setDownloaded:[NSNumber numberWithBool:value_]];
}


- (BOOL)primitiveDownloadedValue {
	NSNumber *result = [self primitiveDownloaded];
	return [result boolValue];
}

- (void)setPrimitiveDownloadedValue:(BOOL)value_ {
	[self setPrimitiveDownloaded:[NSNumber numberWithBool:value_]];
}





@dynamic errorMessage;






@dynamic id;






@dynamic isPublic;



- (BOOL)isPublicValue {
	NSNumber *result = [self isPublic];
	return [result boolValue];
}


- (void)setIsPublicValue:(BOOL)value_ {
	[self setIsPublic:[NSNumber numberWithBool:value_]];
}


- (BOOL)primitiveIsPublicValue {
	NSNumber *result = [self primitiveIsPublic];
	return [result boolValue];
}

- (void)setPrimitiveIsPublicValue:(BOOL)value_ {
	[self setPrimitiveIsPublic:[NSNumber numberWithBool:value_]];
}





@dynamic message;






@dynamic mostRecentOpenedDate;






@dynamic noteCount;



- (int64_t)noteCountValue {
	NSNumber *result = [self noteCount];
	return [result longLongValue];
}


- (void)setNoteCountValue:(int64_t)value_ {
	[self setNoteCount:[NSNumber numberWithLongLong:value_]];
}


- (int64_t)primitiveNoteCountValue {
	NSNumber *result = [self primitiveNoteCount];
	return [result longLongValue];
}

- (void)setPrimitiveNoteCountValue:(int64_t)value_ {
	[self setPrimitiveNoteCount:[NSNumber numberWithLongLong:value_]];
}





@dynamic pageCount;



- (int64_t)pageCountValue {
	NSNumber *result = [self pageCount];
	return [result longLongValue];
}


- (void)setPageCountValue:(int64_t)value_ {
	[self setPageCount:[NSNumber numberWithLongLong:value_]];
}


- (int64_t)primitivePageCountValue {
	NSNumber *result = [self primitivePageCount];
	return [result longLongValue];
}

- (void)setPrimitivePageCountValue:(int64_t)value_ {
	[self setPrimitivePageCount:[NSNumber numberWithLongLong:value_]];
}





@dynamic safeToDeleteOnStartup;



- (BOOL)safeToDeleteOnStartupValue {
	NSNumber *result = [self safeToDeleteOnStartup];
	return [result boolValue];
}


- (void)setSafeToDeleteOnStartupValue:(BOOL)value_ {
	[self setSafeToDeleteOnStartup:[NSNumber numberWithBool:value_]];
}


- (BOOL)primitiveSafeToDeleteOnStartupValue {
	NSNumber *result = [self primitiveSafeToDeleteOnStartup];
	return [result boolValue];
}

- (void)setPrimitiveSafeToDeleteOnStartupValue:(BOOL)value_ {
	[self setPrimitiveSafeToDeleteOnStartup:[NSNumber numberWithBool:value_]];
}





@dynamic status;






@dynamic title;






@dynamic uploadDate;






@dynamic version;



- (int64_t)versionValue {
	NSNumber *result = [self version];
	return [result longLongValue];
}


- (void)setVersionValue:(int64_t)value_ {
	[self setVersion:[NSNumber numberWithLongLong:value_]];
}


- (int64_t)primitiveVersionValue {
	NSNumber *result = [self primitiveVersion];
	return [result longLongValue];
}

- (void)setPrimitiveVersionValue:(int64_t)value_ {
	[self setPrimitiveVersion:[NSNumber numberWithLongLong:value_]];
}





@dynamic versionIsLatest;



- (BOOL)versionIsLatestValue {
	NSNumber *result = [self versionIsLatest];
	return [result boolValue];
}


- (void)setVersionIsLatestValue:(BOOL)value_ {
	[self setVersionIsLatest:[NSNumber numberWithBool:value_]];
}


- (BOOL)primitiveVersionIsLatestValue {
	NSNumber *result = [self primitiveVersionIsLatest];
	return [result boolValue];
}

- (void)setPrimitiveVersionIsLatestValue:(BOOL)value_ {
	[self setPrimitiveVersionIsLatest:[NSNumber numberWithBool:value_]];
}





@dynamic author;

	

@dynamic collections;

	
- (NSMutableSet*)collectionsSet {
	[self willAccessValueForKey:@"collections"];
  
	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"collections"];
  
	[self didAccessValueForKey:@"collections"];
	return result;
}
	

@dynamic content;

	

@dynamic highlightedContent;

	

@dynamic metaDocument;

	

@dynamic thumbnail;

	






@end




