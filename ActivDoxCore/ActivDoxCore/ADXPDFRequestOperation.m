//
//  ADXPDFRequestOperation.m
//  ActivDoxCore
//
//  Created by Gavin McKenzie on 12-06-07.
//  Copyright (c) 2012 ActivDox Inc. All rights reserved.
//

#import "ADXPDFRequestOperation.h"

static dispatch_queue_t adx_pdf_request_operation_processing_queue;
static dispatch_queue_t pdf_request_operation_processing_queue() {
    if (adx_pdf_request_operation_processing_queue == NULL) {
        adx_pdf_request_operation_processing_queue = dispatch_queue_create("com.activdox.networking.pdf-request.processing", 0);
    }
    
    return adx_pdf_request_operation_processing_queue;
}

@interface ADXPDFRequestOperation ()
@property (readwrite, nonatomic, retain) id responsePDF;
@property (readwrite, nonatomic, retain) NSError *PDFError;
@end;

@implementation ADXPDFRequestOperation
@synthesize responsePDF = _responsePDF;
@synthesize PDFError = _PDFError;

#pragma mark - Class Methods

+ (ADXPDFRequestOperation *)PDFRequestOperationWithRequest:(NSURLRequest *)urlRequest success:(void (^)(NSURLRequest *, NSHTTPURLResponse *, id))success failure:(void (^)(NSURLRequest *, NSHTTPURLResponse *, NSError *, id))failure
{
    ADXPDFRequestOperation *requestOperation = [[self alloc] initWithRequest:urlRequest];
    [requestOperation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        if (success) {
            success(operation.request, operation.response, responseObject);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (failure) {
            failure(operation.request, operation.response, error, [(ADXPDFRequestOperation *)operation responsePDF]);
        }
    }];
    
    return requestOperation;
}

#pragma mark - Response

- (id)responsePDF {
    if (!_responsePDF && [self.responseData length] > 0 && [self isFinished] && !self.PDFError) {
        NSError *error = nil;
        
        if ([self.responseData length] == 0) {
            self.responsePDF = nil;
        } else {
            self.responsePDF = self.responseData;
        }
        
        self.PDFError = error;
    }
    
    return _responsePDF;
}

- (NSError *)error {
    if (_PDFError) {
        return _PDFError;
    } else {
        return [super error];
    }
}

#pragma mark - AFHTTPRequestOperation

+ (NSSet *)acceptableContentTypes {
    return [NSSet setWithObjects:@"application/pdf", nil];
}

+ (BOOL)canProcessRequest:(NSURLRequest *)request {
    return [[[request URL] pathExtension] isEqualToString:@"pdf"] || [super canProcessRequest:request];
}

- (void)setCompletionBlockWithSuccess:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
                              failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure
{
    __weak ADXPDFRequestOperation *weakSelf = self;
    self.completionBlock = ^ {
        if ([weakSelf isCancelled]) {
            return;
        }
        
        if (weakSelf.error) {
            if (failure) {
                dispatch_async(weakSelf.failureCallbackQueue ? weakSelf.failureCallbackQueue : dispatch_get_main_queue(), ^{
                    failure(weakSelf, weakSelf.error);
                });
            }
        } else {
            dispatch_async(pdf_request_operation_processing_queue(), ^{
                id pdf = weakSelf.responsePDF;
                
                if (weakSelf.PDFError) {
                    if (failure) {
                        dispatch_async(weakSelf.failureCallbackQueue ? weakSelf.failureCallbackQueue : dispatch_get_main_queue(), ^{
                            failure(weakSelf, weakSelf.error);
                        });
                    }
                } else {
                    if (success) {
                        dispatch_async(weakSelf.successCallbackQueue ? weakSelf.successCallbackQueue : dispatch_get_main_queue(), ^{
                            success(weakSelf, pdf);
                        });
                    }                    
                }
            });
        }
    };    
}


@end
