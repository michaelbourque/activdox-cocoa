// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to ADXAccount.h instead.

#import <CoreData/CoreData.h>



extern const struct ADXAccountAttributes {
	__unsafe_unretained NSString *accessToken;
	__unsafe_unretained NSString *allowLocalContentAccessWithoutAuthentication;
	__unsafe_unretained NSString *id;
	__unsafe_unretained NSString *loginName;
	__unsafe_unretained NSString *mostRecentAllCollectionsUpdate;
	__unsafe_unretained NSString *mostRecentAllDocumentsUpdate;
	__unsafe_unretained NSString *mostRecentAllEventsUpdate;
	__unsafe_unretained NSString *mostRecentAllEventsUpdateID;
	__unsafe_unretained NSString *mostRecentAllNotesUpdate;
	__unsafe_unretained NSString *mostRecentAllNotesUpload;
	__unsafe_unretained NSString *mostRecentLogin;
	__unsafe_unretained NSString *rememberPassword;
	__unsafe_unretained NSString *serverURL;
} ADXAccountAttributes;



extern const struct ADXAccountRelationships {
	__unsafe_unretained NSString *user;
} ADXAccountRelationships;






@class ADXUser;




























@interface ADXAccountID : NSManagedObjectID {}
@end

@interface _ADXAccount : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (ADXAccountID*)objectID;





@property (nonatomic, strong) NSString* accessToken;



//- (BOOL)validateAccessToken:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* allowLocalContentAccessWithoutAuthentication;




@property (atomic) BOOL allowLocalContentAccessWithoutAuthenticationValue;
- (BOOL)allowLocalContentAccessWithoutAuthenticationValue;
- (void)setAllowLocalContentAccessWithoutAuthenticationValue:(BOOL)value_;


//- (BOOL)validateAllowLocalContentAccessWithoutAuthentication:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* id;



//- (BOOL)validateId:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* loginName;



//- (BOOL)validateLoginName:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSDate* mostRecentAllCollectionsUpdate;



//- (BOOL)validateMostRecentAllCollectionsUpdate:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSDate* mostRecentAllDocumentsUpdate;



//- (BOOL)validateMostRecentAllDocumentsUpdate:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSDate* mostRecentAllEventsUpdate;



//- (BOOL)validateMostRecentAllEventsUpdate:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* mostRecentAllEventsUpdateID;



//- (BOOL)validateMostRecentAllEventsUpdateID:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSDate* mostRecentAllNotesUpdate;



//- (BOOL)validateMostRecentAllNotesUpdate:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSDate* mostRecentAllNotesUpload;



//- (BOOL)validateMostRecentAllNotesUpload:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSDate* mostRecentLogin;



//- (BOOL)validateMostRecentLogin:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* rememberPassword;




@property (atomic) BOOL rememberPasswordValue;
- (BOOL)rememberPasswordValue;
- (void)setRememberPasswordValue:(BOOL)value_;


//- (BOOL)validateRememberPassword:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* serverURL;



//- (BOOL)validateServerURL:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) ADXUser *user;

//- (BOOL)validateUser:(id*)value_ error:(NSError**)error_;





@end



@interface _ADXAccount (CoreDataGeneratedPrimitiveAccessors)


- (NSString*)primitiveAccessToken;
- (void)setPrimitiveAccessToken:(NSString*)value;




- (NSNumber*)primitiveAllowLocalContentAccessWithoutAuthentication;
- (void)setPrimitiveAllowLocalContentAccessWithoutAuthentication:(NSNumber*)value;

- (BOOL)primitiveAllowLocalContentAccessWithoutAuthenticationValue;
- (void)setPrimitiveAllowLocalContentAccessWithoutAuthenticationValue:(BOOL)value_;




- (NSString*)primitiveId;
- (void)setPrimitiveId:(NSString*)value;




- (NSString*)primitiveLoginName;
- (void)setPrimitiveLoginName:(NSString*)value;




- (NSDate*)primitiveMostRecentAllCollectionsUpdate;
- (void)setPrimitiveMostRecentAllCollectionsUpdate:(NSDate*)value;




- (NSDate*)primitiveMostRecentAllDocumentsUpdate;
- (void)setPrimitiveMostRecentAllDocumentsUpdate:(NSDate*)value;




- (NSDate*)primitiveMostRecentAllEventsUpdate;
- (void)setPrimitiveMostRecentAllEventsUpdate:(NSDate*)value;




- (NSString*)primitiveMostRecentAllEventsUpdateID;
- (void)setPrimitiveMostRecentAllEventsUpdateID:(NSString*)value;




- (NSDate*)primitiveMostRecentAllNotesUpdate;
- (void)setPrimitiveMostRecentAllNotesUpdate:(NSDate*)value;




- (NSDate*)primitiveMostRecentAllNotesUpload;
- (void)setPrimitiveMostRecentAllNotesUpload:(NSDate*)value;




- (NSDate*)primitiveMostRecentLogin;
- (void)setPrimitiveMostRecentLogin:(NSDate*)value;




- (NSNumber*)primitiveRememberPassword;
- (void)setPrimitiveRememberPassword:(NSNumber*)value;

- (BOOL)primitiveRememberPasswordValue;
- (void)setPrimitiveRememberPasswordValue:(BOOL)value_;




- (NSString*)primitiveServerURL;
- (void)setPrimitiveServerURL:(NSString*)value;





- (ADXUser*)primitiveUser;
- (void)setPrimitiveUser:(ADXUser*)value;


@end
