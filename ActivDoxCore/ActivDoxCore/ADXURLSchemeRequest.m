//
//  ADXURLHandler.m
//  DesktopReader
//
//  Created by Steve Tibbett on 2012-08-24.
//  Copyright (c) 2012 ActivDox. All rights reserved.
//

#import "ActivDoxCore.h"

NSString *kADXURLScheme = @"adox://";
NSString *kADXURLSchemeActionOpen = @"open";
NSString *kADXURLSchemeActionLogin = @"login";
NSString *kADXURLSchemeActionEvent = @"event";
NSString *kADXURLSchemeRequestNotification = @"reader.URLSchemeRequest";

@interface ADXURLSchemeRequest()
@property (strong, nonatomic) NSString *URLString;
@property (strong, nonatomic) NSString *action;
@property (strong, nonatomic) NSString *userID;
@property (nonatomic) BOOL waitingForLogin;
@end

@implementation ADXURLSchemeRequest

- (id)initWithURL:(NSString *)URLString
{
    self = [super init];
    if (self) {
        [self crackURL:URLString];
    }
    return self;
}

- (void)reset
{
    self.waitingForLogin = NO;
    self.requiredServer = nil;
    self.requiredEventID = nil;
    self.requiredDocumentID = nil;
    self.requiredDocumentVersion = nil;
}

- (void)crackURL:(NSString *)URLString {
    if (![URLString hasPrefix:kADXURLScheme]) {
        LogError(@"Unexpected URL: %@", URLString);
        return;
    }
    
    self.URLString = URLString;
    
    NSString *path = [URLString substringFromIndex:kADXURLScheme.length];
    
    // Check for "adox://open/server/docid"
    // and "adox://open/server/docid/version"
    NSArray *components = [path pathComponents];
    if ([[components objectAtIndex:0] isEqualToString:kADXURLSchemeActionOpen] && (components.count == 3 || components.count == 4)) {
        self.action = [components objectAtIndex:0];
        self.requiredServer = [components objectAtIndex:1];
        if (components.count == 4) {
            NSString *versionString = [components objectAtIndex:3];
            self.requiredDocumentVersion = [NSNumber numberWithInt:[versionString intValue]];
        }
                                            
        NSRange questionMarkRange = [self.requiredServer rangeOfString:@"?"];
        if (questionMarkRange.location != NSNotFound) {
            // Strip off the question mark and anything after it.  This doesn't do us any good right now,
            // but allows us to add parameters without breaking old code.
            self.requiredServer = [self.requiredServer substringToIndex:questionMarkRange.location];
        }
        
        self.requiredDocumentID = [components objectAtIndex:2];
        self.requireThatAppHasNotLoggedIn = NO;
    }
    
    // Check for "adox://login/userid@server:port"
    if ([[components objectAtIndex:0] isEqualToString:kADXURLSchemeActionLogin] && components.count == 2) {
        self.action = [components objectAtIndex:0];
        
        NSArray *serverComponents = [[components objectAtIndex:1] componentsSeparatedByString:@"@"];
        if (serverComponents.count == 1) {
            // No login specified, just the server
            self.requiredServer = [serverComponents objectAtIndex:0];
        } else {
            NSAssert(serverComponents.count == 2, @"Unexpected server string format");
            if (serverComponents.count != 2) {
                // Bail out
                return;
            }
            self.userID = [serverComponents objectAtIndex:0];
            self.userID = [self.userID stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            self.requiredServer = [serverComponents objectAtIndex:1];
        }
        
        self.requiredDocumentID = nil;
        self.requireThatAppHasNotLoggedIn = YES;
    }

    // Check for "adox://event/eventid"
    // This variant is deprecated and can be removed once the server has been updated to use the version below which includes the accountID
    if ([[components objectAtIndex:0] isEqualToString:kADXURLSchemeActionEvent] && components.count == 2) {
        self.action = [components objectAtIndex:0];
        self.requiredEventID = [components objectAtIndex:1];
    }
    
    // Check for "adox://event/#accountid/#eventid"
    if ([[components objectAtIndex:0] isEqualToString:kADXURLSchemeActionEvent] && components.count == 3) {
        // For an event, we need to retrieve the event record from the server to know
        // how to act on it.  Either we can do this right now, or we need to wait for
        // the user to log in.
        NSString *accountID = [components objectAtIndex:1];
        NSString *eventID = [components objectAtIndex:2];
        ADXV2Service *service = [ADXV2Service serviceForAccountID:accountID];
        if (service == nil || !service.networkReachable) {
            self.waitingForLogin = YES;
        } else {
            [service requestDocumentIDForEventWithID:eventID
                                          completion:^(id response, BOOL success, NSError *error) {
                                              if (success) {
                                                  NSDictionary *responseDict = (NSDictionary *)response;
                                                  NSString *documentID = responseDict[ADXDocumentAttributes.id];
                                                  NSString *versionID = responseDict[ADXDocumentAttributes.version];

                                                  NSURL *url = [NSURL URLWithString:service.account.serverURL];
                                                  NSString *server = url.host;
                                                  if (url.port.intValue != 80) {
                                                      server = [NSString stringWithFormat:@"%@:%d", server, url.port.intValue];
                                                  }

                                                  // Transform into an open document request
                                                  NSString *launchURLString = nil;
                                                  if (versionID == nil) {
                                                      launchURLString = [NSString stringWithFormat:@"adox://open/%@/%@", server, documentID];
                                                  } else {
                                                      launchURLString = [NSString stringWithFormat:@"adox://open/%@/%@/%d", server, documentID, [versionID intValue]];
                                                  }
                                                  LogInfo(@"Processing as %@", launchURLString);

                                                  [self reset];
                                                  [self crackURL:launchURLString];
                                                  [[NSNotificationCenter defaultCenter] postNotificationName:kADXURLSchemeRequestNotification object:nil];
                                              }
                                          }];
        }
    }
}

- (void)dispatch {
    NSAssert(self.delegate != nil, @"No delegate");
    
    if ([self.action isEqualToString:kADXURLSchemeActionOpen]) {
        if ([self.delegate respondsToSelector:@selector(urlHandlerOpenDocumentWithID:)]) {
            [self.delegate urlHandlerOpenDocumentWithID:self.requiredDocumentID];
        }
        return;
    }
    
    if ([self.action isEqualToString:kADXURLSchemeActionLogin]) {
        if ([self.delegate respondsToSelector:@selector(urlHandlerShowLoginForUserID:)]) {
            [self.delegate urlHandlerShowLoginForUserID:self.userID];
        }
        return;
    }

    if ([self.action isEqualToString:kADXURLSchemeActionEvent]) {
        NSAssert(NO, @"adox://event URL should have been handled by login");
        return;
    }

    NSLog(@"URL scheme contains unrecognized request: %@", self.action);
}

- (void)applicationDidLoginWithAccount:(ADXAccount *)account
{
    // Translate a pre-login notification request for a particular event ID
    // This is to support the (now deprecated) adox://event/#eventid format
    if (self.requiredEventID != nil) {
        NSString *eventID = self.requiredEventID;
        NSString *openRequest = [NSString stringWithFormat:@"adox://event/%@/%@", account.id, eventID];
        [self reset];
        [self crackURL:openRequest];
        return;
    }
    
    if (self.waitingForLogin) {
        // Retry now that the user is logged in
        [self reset];
        [self crackURL:self.URLString];
        [[NSNotificationCenter defaultCenter] postNotificationName:kADXURLSchemeRequestNotification object:nil];
    }
}

@end
