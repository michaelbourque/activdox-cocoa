// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to ADXEvent.h instead.

#import <CoreData/CoreData.h>



extern const struct ADXEventAttributes {
	__unsafe_unretained NSString *accountID;
	__unsafe_unretained NSString *id;
	__unsafe_unretained NSString *originatorID;
	__unsafe_unretained NSString *payload;
	__unsafe_unretained NSString *referencedDocumentID;
	__unsafe_unretained NSString *resource;
	__unsafe_unretained NSString *timeStamp;
	__unsafe_unretained NSString *type;
} ADXEventAttributes;



extern const struct ADXEventRelationships {
	__unsafe_unretained NSString *originator;
} ADXEventRelationships;






@class ADXUser;









@class NSObject;




@class NSObject;





@interface ADXEventID : NSManagedObjectID {}
@end

@interface _ADXEvent : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (ADXEventID*)objectID;





@property (nonatomic, strong) NSString* accountID;



//- (BOOL)validateAccountID:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* id;



//- (BOOL)validateId:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* originatorID;



//- (BOOL)validateOriginatorID:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) id payload;



//- (BOOL)validatePayload:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* referencedDocumentID;



//- (BOOL)validateReferencedDocumentID:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) id resource;



//- (BOOL)validateResource:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSDate* timeStamp;



//- (BOOL)validateTimeStamp:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* type;



//- (BOOL)validateType:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) ADXUser *originator;

//- (BOOL)validateOriginator:(id*)value_ error:(NSError**)error_;





@end



@interface _ADXEvent (CoreDataGeneratedPrimitiveAccessors)


- (NSString*)primitiveAccountID;
- (void)setPrimitiveAccountID:(NSString*)value;




- (NSString*)primitiveId;
- (void)setPrimitiveId:(NSString*)value;




- (NSString*)primitiveOriginatorID;
- (void)setPrimitiveOriginatorID:(NSString*)value;




- (id)primitivePayload;
- (void)setPrimitivePayload:(id)value;




- (NSString*)primitiveReferencedDocumentID;
- (void)setPrimitiveReferencedDocumentID:(NSString*)value;




- (id)primitiveResource;
- (void)setPrimitiveResource:(id)value;




- (NSDate*)primitiveTimeStamp;
- (void)setPrimitiveTimeStamp:(NSDate*)value;





- (ADXUser*)primitiveOriginator;
- (void)setPrimitiveOriginator:(ADXUser*)value;


@end
