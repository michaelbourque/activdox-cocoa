// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to ADXHighlightedContent.m instead.

#import "_ADXHighlightedContent.h"


const struct ADXHighlightedContentAttributes ADXHighlightedContentAttributes = {
	.data = @"data",
};



const struct ADXHighlightedContentRelationships ADXHighlightedContentRelationships = {
	.document = @"document",
};






@implementation ADXHighlightedContentID
@end

@implementation _ADXHighlightedContent

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"ADXHighlightedContent" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"ADXHighlightedContent";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"ADXHighlightedContent" inManagedObjectContext:moc_];
}

- (ADXHighlightedContentID*)objectID {
	return (ADXHighlightedContentID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
	

	return keyPaths;
}




@dynamic data;






@dynamic document;

	






@end




