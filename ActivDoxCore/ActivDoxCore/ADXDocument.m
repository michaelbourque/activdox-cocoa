
#import "ADXDocument.h"
#import "ADXCategories.h"
#import "ADXCommonMacros.h"
#import "ISO8601DateFormatter.h"
#import "GRMustache.h"

const struct ADXJSONDocumentsResponseAttributes ADXJSONDocumentsResponseAttributes = {
    .documents = @"documents",
    .users = @"users",
    .collections = @"collections",
};

const struct ADXJSONDocumentAttributes ADXJSONDocumentAttributes = {
	.id = @"id",
	.uploadDate = @"uploadDate",
	.version = @"version",
	.collections = @"collections",
    .title = @"title",
    .author = @"author",
    .documentDescription = @"description",
    .errorMessage = @"errorMessage",
    .message = @"message",
    .notes = @"notes",
    .status = @"status",
    .differencesDict = @"differences",
    .differences = {
        .changeCount = @"differences.changeCount",
        .changedPages = @"differences.changedPages",
    },
};

const struct ADXDocumentStatus ADXDocumentStatus = {
    .accessible = @"Accessible",
    .revoked = @"Revoked",
    .deleted = @"Deleted",
    .removed = @"Removed",
    .inaccessible = @"inaccessible",
};

@interface ADXDocument ()
@end

@implementation ADXDocument

#pragma mark - ADXEntityCRUDProtocol

+ (void)dumpInContext:(NSManagedObjectContext *)moc
{
    NSArray *allDocuments = [ADXDocument allDocumentsInContext:moc];
    NSLog(@"BEGIN ADXDocument dump");
    for (ADXDocument *document in allDocuments) {
        NSLog(@"\t Document %@ / %@ / %@", document.id, document.title, document.collections);
    }
    NSLog(@"END ADXDocument dump");
}

+ (id)entityWithID:(NSString *)entityID context:(NSManagedObjectContext *)moc
{
    NSAssert(NO, @"ADXCollection entityWithID not supported. Use ADXCollection collectionWithID:accountID: instead.");
    return nil;
}

+ (BOOL)updateEntity:(id)entity fromDictionary:(NSDictionary *)dictionary context:(NSManagedObjectContext *)moc
{    
    static ISO8601DateFormatter *dateFormatter = nil;
    if (!dateFormatter) {
        dateFormatter = [[ISO8601DateFormatter alloc] init];
    }
    
    if (!entity || IsEmpty(dictionary) || !moc) {
        LogError(@"Received bad params");
        return NO;
    }
    
    ADXDocument *document = entity;
    document.safeToDeleteOnStartup = @NO;
    
    if (IsEmpty(document.id)) {
        NSString *documentID = [dictionary aps_objectForKeyOrNil:ADXJSONDocumentAttributes.id];
        if (IsEmpty(documentID)) {
            LogError(@"No document id was supplied in dictionary");
            return NO;
        }
        document.id = documentID;
    }

    NSString *accountID = [dictionary aps_objectForKeyOrNil:ADXDocumentAttributes.accountID];
    if (IsEmpty(accountID)) {
        LogError(@"Cannot update a document without an account id.");
        return NO;
    }
    document.accountID = accountID;

    if (!document.metaDocument) {
        ADXMetaDocument *metaDocument = [ADXMetaDocument metaDocumentWithID:document.id accountID:document.accountID context:moc];
        if (!metaDocument) {
            LogError(@"No meta-document could be established for document with ID %@", document.id);
            return NO;
        }
        document.metaDocument = metaDocument;
    }
    
    NSString *uploadDateString = [dictionary aps_objectForKeyOrNil:ADXJSONDocumentAttributes.uploadDate];
    if (IsEmpty(uploadDateString)) {
        LogError(@"No upload date was supplied for document id %@", document.id);
        return NO;
    } else {
        document.uploadDate = [dateFormatter dateFromString:uploadDateString];
    }
        
    document.title = [dictionary aps_objectForKeyOrNil:ADXJSONDocumentAttributes.title];
    document.version = [dictionary aps_objectForKey:ADXJSONDocumentAttributes.version defaultIfNotFound:[NSNumber numberWithInteger:0]];
    if (document.versionValue == 0) {
        LogError(@"Received document %@ title %@ with a bad/missing/nil version property: %@", document.id, document.title, [dictionary aps_objectForKeyOrNil:ADXJSONDocumentAttributes.version]);
        return NO;
    }
    
    document.isPublic = [dictionary aps_objectForKey:ADXDocumentAttributes.isPublic defaultIfNotFound:[NSNumber numberWithBool:NO]];
    
    document.changeCount = [dictionary aps_valueForKeyPath:ADXJSONDocumentAttributes.differences.changeCount defaultIfNotFound:[NSNumber numberWithInteger:0]];
    if (!document.changeCount) {
        document.changeCount = [NSNumber numberWithInteger:0];
    }
    
    document.changedPages = [dictionary aps_valueForKeyPathOrNil:ADXJSONDocumentAttributes.differences.changedPages];
    document.message = [dictionary aps_valueForKeyPathOrNil:ADXJSONDocumentAttributes.message];
    document.documentDescription = [dictionary aps_valueForKeyPathOrNil:ADXJSONDocumentAttributes.documentDescription];
    document.status = [dictionary aps_valueForKeyPathOrNil:ADXJSONDocumentAttributes.status];
    document.noteCount = [dictionary aps_valueForKeyPath:ADXJSONDocumentAttributes.notes defaultIfNotFound:[NSNumber numberWithInteger:0]];
    document.errorMessage = [dictionary aps_valueForKeyPath:ADXJSONDocumentAttributes.errorMessage defaultIfNotFound:nil];

    // FIXME: Compare the collection references in the document with the collection(s) set on the entity
    
    NSArray *currentCollectionsJSON = [dictionary aps_objectForKeyOrNil:ADXJSONDocumentAttributes.collections];
    NSMutableSet *currentCollectionObjects = [[NSMutableSet alloc] init];
    
    for (NSString *collectionID in currentCollectionsJSON) {
        ADXCollection *collection = [ADXCollection collectionWithID:collectionID accountID:accountID context:moc];
        if (collection) {
            [currentCollectionObjects addObject:collection];
        } else {
            LogError(@"Could not locate collection id %@ for document id %@. Adding a placeholder collection.", collection, document);
            
            ADXCollection *newCollection = [ADXCollection addEntityFromDictionary:@{
                                            ADXCollectionAttributes.id: collectionID,
                                            ADXCollectionAttributes.status: ADXCollectionStatus.accessible,
                                            ADXCollectionAttributes.accountID: accountID,
                                            ADXCollectionAttributes.title: @"(Unknown)"} context:moc];
            [currentCollectionObjects addObject:newCollection];
        }
    }
    
    if (IsEmpty(currentCollectionObjects)) {
        [document setCollections:nil];
    } else {
        [document setCollections:currentCollectionObjects];
    }
    
    NSString *authorID = [dictionary aps_objectForKeyOrNil:ADXJSONDocumentAttributes.author];
    ADXUser *author = [ADXUser entityWithID:authorID context:moc];
    if (author) {
        document.author = author;
    } else {
        LogError(@"Could not locate author id %@ for document id %@. Adding a placeholder entry.", [dictionary aps_objectForKeyOrNil:ADXJSONDocumentAttributes.author], document.id);

        ADXUser *newUser = [ADXUser addEntityFromDictionary:@{
                            ADXUserAttributes.id: authorID,
                            ADXUserAttributes.loginName: @"(Unknown)",
                            ADXUserAttributes.primaryEmail: @"(Unknown)"} context:moc];
        document.author = newUser;
    }

    LogTrace(@"Updated document: %@", document);
    return YES;
}

+ (id)addEntityFromDictionary:(NSDictionary *)dictionary context:(NSManagedObjectContext *)moc
{    
    if (IsEmpty(dictionary)) {
        LogError(@"Received a nil dictionary.");
        return nil;
    }
    
    NSString *documentID = [dictionary objectForKey:ADXDocumentAttributes.id];
    NSNumber *version = [dictionary aps_objectForKeyOrNil:ADXJSONDocumentAttributes.version];
    if (IsEmpty(documentID)) {
        LogError(@"Received a dictionary with no document ID");
        return nil;
    }

    NSString *accountID = [dictionary objectForKey:ADXDocumentAttributes.accountID];
    if (IsEmpty(accountID)) {
        LogError(@"Received a dictionary with no account id");
        return nil;
    }

    ADXDocument *document = [ADXDocument documentWithID:documentID version:version accountID:accountID context:moc];
    if (document) {
        LogError(@"Document with id %@ version %@ already exists.", documentID, version);
        return nil;
    }
    
    ADXDocument *newDocument = [ADXDocument insertInManagedObjectContext:moc];
    BOOL success = [ADXDocument updateEntity:newDocument fromDictionary:dictionary context:moc];
    if (success) {
        LogTrace(@"Inserted document with id %@ version %@", documentID, version);
        return newDocument;
    } else {
        LogTrace(@"Failed insertion of document with id %@ version %@", documentID, version);
        return nil;
    }
}

+ (BOOL)makeDocumentInaccessible:(NSString *)documentID accountID:(NSString *)accountID status:(NSString *)status context:(NSManagedObjectContext *)moc
{
    if (!([status isEqualToString:ADXDocumentStatus.deleted] || [status isEqualToString:ADXDocumentStatus.revoked] || [status isEqualToString:ADXDocumentStatus.inaccessible])) {
        LogError(@"expected a status of revoked or deleted.");
        return NO;
    }
    
    // Revoke all versions of the document
    NSArray *documents = [ADXDocument allVersionsOfDocument:documentID accountID:(NSString *)accountID context:moc];
    for (ADXDocument *document in documents) {
        document.status = status;
    }

    return YES;
}

+ (void)restoreInaccessibleVersions:(NSString *)documentID accountID:(NSString *)accountID context:(NSManagedObjectContext *)moc
{
    NSArray *documents = [ADXDocument inaccessibleVersionsOfDocument:documentID accountID:accountID context:moc];
    if (IsEmpty(documents)) {
        LogTrace(@"No inaccessible documents to restore");
    } else {
        for (ADXDocument *document in documents) {
            document.status = ADXDocumentStatus.accessible;
        }
        LogTrace(@"restored inaccessible documents: %@", documents);
    }
}

+ (BOOL)updateDocuments:(NSArray *)documents accountID:(NSString *)accountID context:(NSManagedObjectContext *)moc
{
    BOOL success = YES;
    
    if (IsEmpty(documents)) {
        LogError(@"Received a nil/empty array");
        return NO;
    }
    
    for (NSDictionary *dictionary in documents) {
        NSString *documentID = [dictionary aps_objectForKeyOrNil:ADXJSONDocumentAttributes.id];
        NSNumber *version = [dictionary aps_objectForKeyOrNil:ADXJSONDocumentAttributes.version];
        NSString *status = [dictionary aps_objectForKeyOrNil:ADXJSONDocumentAttributes.status];
        
        if (IsEmpty(documentID)) {
            LogError(@"Received a dictionary with no document ID");
            return NO;
        }

        BOOL updateSuccess = NO;
        if ([status isEqualToString:ADXDocumentStatus.deleted] || [status isEqualToString:ADXDocumentStatus.revoked] || [status isEqualToString:ADXDocumentStatus.inaccessible]) {
            updateSuccess = [self makeDocumentInaccessible:documentID accountID:accountID status:status context:moc];
        } else if ([status isEqualToString:ADXDocumentStatus.removed]) {
            // Do nothing - we can't remove the document because it may be related to other collections.
            // Document removal from collections is handled elsewhere.
        } else {
            ADXDocument *document = [ADXDocument documentWithID:documentID version:version accountID:accountID context:moc];
            NSDictionary *documentProperties = [dictionary aps_dictionaryMergedWithDictionary:@{ADXDocumentAttributes.accountID : accountID}];

            if (document) {
                updateSuccess = [ADXDocument updateEntity:document fromDictionary:documentProperties context:moc];
            } else {
                [ADXDocument restoreInaccessibleVersions:documentID accountID:accountID context:moc];
                ADXDocument *newDocument = [ADXDocument addEntityFromDictionary:documentProperties context:moc];
                if (newDocument) {
                    updateSuccess = YES;
                } else {
                    updateSuccess = NO;
                }
            }
        }
        
        if (!updateSuccess) {
            success = NO;
            break;
        }
    }
    
    return success;
}

+ (BOOL)addDocuments:(NSArray *)documents accountID:(NSString *)accountID updateExisting:(BOOL)updateExisting context:(NSManagedObjectContext *)moc
{
    BOOL success = YES;
    
    for (NSDictionary *dictionary in documents) {
        NSString *documentID = [dictionary aps_objectForKeyOrNil:ADXJSONDocumentAttributes.id];
        NSNumber *version = [dictionary aps_objectForKeyOrNil:ADXJSONDocumentAttributes.version];
        if (IsEmpty(documentID)) {
            LogError(@"Received a dictionary with no document ID");
            return NO;
        }

        ADXDocument *document = [ADXDocument documentWithID:documentID version:version accountID:accountID context:moc];
        NSDictionary *documentProperties = [dictionary aps_dictionaryMergedWithDictionary:@{ADXDocumentAttributes.accountID : accountID}];
        
        BOOL addOrUpdateSuccess = NO;
        if (document) {
            if (updateExisting) {
                addOrUpdateSuccess = [ADXDocument updateEntity:document fromDictionary:documentProperties context:moc];
            } else {
                addOrUpdateSuccess = YES;
            }
        } else {
            ADXDocument *newDocument = [ADXDocument addEntityFromDictionary:documentProperties context:moc];
            if (newDocument) {
                addOrUpdateSuccess = YES;
            } else {
                addOrUpdateSuccess = NO;
            }
        }
        
        if (!addOrUpdateSuccess) {
            success = NO;
            break;
        }
    }
    
    return success;
}

+ (ADXDocument *)latestDocumentWithID:(NSString *)documentID accountID:(NSString *)accountID context:(NSManagedObjectContext *)moc
{
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:[ADXDocument entityName]];
    request.predicate = [NSPredicate predicateWithFormat:@"(%K == %@) AND (%K == %@) AND (%K == %@)",
                         ADXDocumentAttributes.id, documentID,
                         ADXDocumentAttributes.accountID, accountID,
                         ADXDocumentAttributes.versionIsLatest, [NSNumber numberWithBool:YES]];
    request.sortDescriptors = [NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:ADXDocumentAttributes.version ascending:NO]];
    
    __block NSArray *results = nil;
    [moc performBlockAndWait:^{
        NSError *error = nil;
        results = [moc executeFetchRequest:request error:&error];
        if (error) {
            LogError(@"Error executing fetch request for latestDocumentWithID=%@: %@", documentID, error);
        }
    }];
    
    if (results && results.count > 0) {
        return [results objectAtIndex:0];
    } else {
        return nil;
    }
}

+ (NSNumber *)versionOfLatestDocumentWithID:(NSString *)documentID accountID:(NSString *)accountID context:(NSManagedObjectContext *)moc
{
    if (IsEmpty(documentID) || IsEmpty(accountID)) {
        return nil;
    }
    
    ADXDocument *document = [ADXDocument latestDocumentWithID:documentID accountID:accountID context:moc];
    if (!document) {
        return nil;
    }
    
    NSNumber *versionID = document.version;
    return versionID;
}

+ (ADXDocument *)mostRecentlyOpenedDocumentWithID:(NSString *)documentID accountID:(NSString *)accountID context:(NSManagedObjectContext *)moc
{
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:[ADXDocument entityName]];
    request.predicate = [NSPredicate predicateWithFormat:@"(%K == %@) AND (%K == %@)", ADXDocumentAttributes.id, documentID, ADXDocumentAttributes.accountID, accountID];
    request.sortDescriptors = [NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:ADXDocumentAttributes.mostRecentOpenedDate ascending:NO]];
    request.fetchLimit = 1;
    
    __block NSArray *results = nil;
    [moc performBlockAndWait:^{
        NSError *error = nil;
        results = [moc executeFetchRequest:request error:&error];
        if (error) {
            LogError(@"Error executing fetch request for latestDocumentWithID=%@: %@", documentID, error);
        }
    }];
    
    if (results && results.count > 0) {
        return [results objectAtIndex:0];
    } else {
        return nil;
    }
}

+ (ADXDocument *)documentWithID:(NSString *)documentID version:(NSNumber *)versionID accountID:(NSString *)accountID context:(NSManagedObjectContext *)moc
{
    NSAssert(versionID, @"documentWithID must receive a non-nil version id");
    ADXDocument *document = [moc aps_fetchFirstObjectForEntityName:[ADXDocument entityName]
                                                     withPredicate:@"(%K == %@) AND (%K == %@) AND (%K == %@)", ADXDocumentAttributes.id, documentID, ADXDocumentAttributes.accountID, accountID, ADXDocumentAttributes.version, versionID];
    return document;
}

+ (NSArray *)allDocumentsInContext:(NSManagedObjectContext *)moc
{
    NSArray *results = [moc aps_fetchAllForEntityName:[ADXDocument entityName]];
    return results;
}

+ (NSArray *)inaccessibleVersionsOfDocument:(NSString *)documentID accountID:(NSString *)accountID context:(NSManagedObjectContext *)moc
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(%K == %@) AND (%K == %@) AND (%K != %@)",
                              ADXDocumentAttributes.id, documentID,
                              ADXDocumentAttributes.accountID, accountID,
                              ADXDocumentAttributes.status, ADXDocumentStatus.accessible];
    NSArray *documents = [moc aps_fetchObjectsForEntityName:[ADXDocument entityName]
                                              withPredicate:predicate];
    return documents;
}

+ (NSArray *)allVersionsOfDocument:(NSString *)documentID accountID:(NSString *)accountID context:(NSManagedObjectContext *)moc
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(%K == %@) AND (%K == %@)",
                              ADXDocumentAttributes.id, documentID,
                              ADXDocumentAttributes.accountID, accountID];
    NSArray *documents = [moc aps_fetchObjectsForEntityName:[ADXDocument entityName]
                                            withPredicate:predicate];
    return documents;
}

+ (ADXDocumentMakeLatestVersionResponse)makeLatestVersionOfDocument:(NSString *)documentID version:(NSNumber *)version accountID:(NSString *)accountID context:(NSManagedObjectContext *)moc
{
    ADXDocumentMakeLatestVersionResponse result = ADXDocumentMakeLatestVersionFailed;

    LogTrace(@"makeLatestVersionOfDocument called with document ID %@ and version %@", documentID, version);

    if (!version) {
        LogError(@"makeLatestVersionOfDocument called with document ID %@ and nil version", documentID);
        return ADXDocumentMakeLatestVersionFailed;
    }
    
    ADXDocument *currentLatestDocument = [ADXDocument latestDocumentWithID:documentID accountID:accountID context:moc];
    if ((currentLatestDocument) && ([currentLatestDocument.version isEqualToNumber:version])) {
        NSLog(@"makeLatestVersionOfDocument: document is already marked as latest.");
        result = ADXDocumentMakeLatestVersionNoChange;
    } else {
        [currentLatestDocument setVersionIsLatestValue:NO];
        [currentLatestDocument setCollections:nil];
        
        NSPredicate *predicateForLatest = [NSPredicate predicateWithFormat:@"(%K == %@) AND (%K == %@) AND (%K == %@)",
                                           ADXDocumentAttributes.id, documentID,
                                           ADXDocumentAttributes.accountID, accountID,
                                           ADXDocumentAttributes.version, version];
        NSArray *documentsMatchingVersion = [moc aps_fetchObjectsForEntityName:[ADXDocument entityName] withPredicate:predicateForLatest];
        
        if (!documentsMatchingVersion || ![documentsMatchingVersion count]) {
            LogError(@"makeLatestVersionOfDocument failing because no document found with ID %@ matching version %@", documentID, version);
            return ADXDocumentMakeLatestVersionFailed;
        }
        if (documentsMatchingVersion.count > 1) {
            LogError(@"makeLatestVersionOfDocument failing because more than one document with ID %@ matches version %@", documentID, version);
            return ADXDocumentMakeLatestVersionFailed;
        }
        
        ADXDocument *newLatestDocument = [documentsMatchingVersion lastObject];
        if (!newLatestDocument) {
            LogError(@"makeLatestVersionOfDocument failing because newLatestDocument is nil, given document with ID %@ matches version %@", documentID, version);
            return ADXDocumentMakeLatestVersionFailed;
        }

        [newLatestDocument setVersionIsLatestValue:YES];
        result = ADXDocumentMakeLatestVersionSuccess;
    }
    
    return result;
}

+ (ADXDocumentMakeLatestVersionResponse)makeDocumentLatestIfNewer:(NSDictionary *)document accountID:(NSString *)accountID context:(NSManagedObjectContext *)moc
{
    ADXDocumentMakeLatestVersionResponse result = ADXDocumentMakeLatestVersionNoChange;
    NSString *documentID = [document aps_objectForKeyOrNil:ADXJSONDocumentAttributes.id];
    NSNumber *documentVersion = [document aps_objectForKey:ADXJSONDocumentAttributes.version defaultIfNotFound:[NSNumber numberWithInteger:0]];
    NSString *documentStatus = [document aps_valueForKeyPathOrNil:ADXJSONDocumentAttributes.status];
    NSNumber *currentlyStoredLatestDocumentVersion = [ADXDocument versionOfLatestDocumentWithID:documentID accountID:accountID context:moc];
    
    if (![ADXDocument accessibleStatus:documentStatus]) {
        return ADXDocumentMakeLatestVersionNoChange;
    }
    
    if (currentlyStoredLatestDocumentVersion == nil || [currentlyStoredLatestDocumentVersion compare:documentVersion] == NSOrderedAscending) {
        result = [ADXDocument makeLatestVersionOfDocument:documentID version:documentVersion accountID:accountID context:moc];
        if (result == ADXDocumentMakeLatestVersionFailed) {
            LogError(@"refreshDocument unable to make document with id %@ and version %@ the latest version", documentID, documentVersion);
        }
    }
    return result;
}

+ (NSArray *)makeDocumentsLatestIfNewer:(NSArray *)documents accountID:(NSString *)accountID context:(NSManagedObjectContext *)moc
{
    NSMutableArray *updatedVersions = [[NSMutableArray alloc] init];
    for (NSDictionary *document in documents) {
        ADXDocumentMakeLatestVersionResponse result = [ADXDocument makeDocumentLatestIfNewer:document accountID:accountID context:moc];
        if (result == ADXDocumentMakeLatestVersionSuccess) {
            [updatedVersions addObject:document];
        } else if (result == ADXDocumentMakeLatestVersionFailed) {
            return nil;
        }
    }
    return updatedVersions;
}

+ (NSArray *)onlyInaccessibleDocumentsFrom:(NSArray *)documents
{
    NSMutableArray *results = [[NSMutableArray alloc] init];
    [documents enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        NSDictionary *document = obj;
        NSString *status = [document aps_valueForKeyPathOrNil:ADXJSONDocumentAttributes.status];
        if (![ADXDocument accessibleStatus:status]) {
            [results addObject:document];
        }
    }];
    return [NSArray arrayWithArray:results];
}

+ (NSArray *)onlyAccessibleDocumentsFrom:(NSArray *)documents
{
    NSMutableArray *results = [[NSMutableArray alloc] init];
    [documents enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        NSDictionary *document = obj;
        NSString *status = [document aps_valueForKeyPathOrNil:ADXJSONDocumentAttributes.status];
        if ([ADXDocument accessibleStatus:status]) {
            [results addObject:document];
        }
    }];
    return [NSArray arrayWithArray:results];
}

+ (NSArray *)allInaccessibleDocumentsInContext:(NSManagedObjectContext *)moc objectIDsOnly:(BOOL)objectIDsOnly
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K != %@",
                              ADXDocumentAttributes.status, ADXDocumentStatus.accessible];

    NSArray *documents = [moc aps_fetchObjectsForEntityName:[ADXDocument entityName] objectIDsOnly:objectIDsOnly sortDescriptors:nil withPredicate:predicate];
    return documents;
}

+ (BOOL)accessibleStatus:(NSString *)status
{
    if ([status isEqualToString:ADXDocumentStatus.deleted] || [status isEqualToString:ADXDocumentStatus.revoked] || [status isEqualToString:ADXDocumentStatus.removed] || [status isEqualToString:ADXDocumentStatus.inaccessible]) {
        return NO;
    } else {
        return YES;
    }
}

- (NSString *)versionDisplayLabel
{
    NSString *result = @"";
    NSString *versionString = [self.version stringValue];
    
    if (!IsEmpty(versionString)) {
        result = [NSString stringWithFormat:NSLocalizedString(@"Thumbnail version subtitle", nil), versionString];
    }
    return result;
}

- (NSString *)publishedDateDisplayLabel:(BOOL)relative
{
    NSString *result = nil;
    NSDate *publishedDate = self.uploadDate;
    
    if (relative) {
        result = [publishedDate stringFromDateCapitalized:YES];
    } else {
        static NSDateFormatter *formatter = nil;
        if (!formatter) {
            formatter = [[NSDateFormatter alloc] init];
            [formatter setDateFormat:@"EEE, MMM d YYYY, h:mm a"];
        }

        result = [formatter stringFromDate:publishedDate];
    }
    
    return result;
}

- (BOOL)accessible
{
    return [ADXDocument accessibleStatus:self.status];
}

+ (NSArray *)documentsToBeDeletedOnStartup:(NSManagedObjectContext *)moc
{
    NSArray *inaccessibleDocuments = [ADXDocument allInaccessibleDocumentsInContext:moc objectIDsOnly:YES];
    return inaccessibleDocuments;
}

- (BOOL)shouldBeDeletedOnStartup
{
    return self.safeToDeleteOnStartupValue;
}

+ (NSArray *)documentIDsToBeDownloadedWithContext:(NSManagedObjectContext *)moc
{
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:[ADXDocument entityName]];
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"(%K == nil || %K == %@) && (metaDocument.%K == nil || metaDocument.%K == %@) && (%K == %@)",
                              ADXDocumentAttributes.downloaded, ADXDocumentAttributes.downloaded, @NO,
                              ADXMetaDocumentAttributes.doNotAutomaticallyDownloadContent, ADXMetaDocumentAttributes.doNotAutomaticallyDownloadContent, @NO,
                              ADXDocumentAttributes.versionIsLatest, [NSNumber numberWithBool:YES]];
    NSError *error = nil;
    NSArray *results = [moc executeFetchRequest:fetchRequest error:&error];
    if (error) {
        LogError(@"Unexpected error finding documents to download: %@", error);
    }

    return [results valueForKey:ADXDocumentAttributes.id];
}


@end
