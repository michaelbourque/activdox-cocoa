
#import "ADXEventQueueEntry.h"
#import "ADXModel.h"
#import "ADXCommonMacros.h"


const struct ADXJSONEventQueueEntryAttributes ADXJSONEventQueueEntryAttributes = {
	.eventType = @"eventType",
    .timestamp = @"timestamp",
    .payloadDict = @"payload",
    .payload = {
        .document = @"document",
        .version = @"version",
        .page = @"page",
    },
};

NSUInteger const kADXEventQueueEntryBatchSize = 50;

@implementation ADXEventQueueEntry

+ (BOOL)insertEventType:(NSString *)type
                accountID:(NSString *)accountID
                payload:(NSDictionary *)payload
                context:(NSManagedObjectContext *)moc
{
    BOOL success = NO;
    ADXEventQueueEntry *entry = [ADXEventQueueEntry insertInManagedObjectContext:moc];
    
    if (entry) {
        entry.accountID = accountID;
        entry.timeStamp = [NSDate date];
        entry.payload = payload;
        entry.type = type;

        success = YES;
    }

    return success;
}

- (NSDictionary *)jsonDictionary
{
    static ISO8601DateFormatter *dateFormatter = nil;
    if (!dateFormatter) {
        dateFormatter = [[ISO8601DateFormatter alloc] init];
        dateFormatter.format = ISO8601DateFormatCalendar;
        dateFormatter.includeTime = YES;
    }

    NSMutableDictionary *mutableDictionary = [[NSMutableDictionary alloc] initWithObjectsAndKeys:
                                              self.type, ADXJSONEventQueueEntryAttributes.eventType,
                                              self.payload, ADXJSONEventQueueEntryAttributes.payloadDict,
                                              nil];
    
    if (self.timeStamp) {
        [mutableDictionary setObject:[dateFormatter stringFromDate:self.timeStamp] forKey:ADXJSONEventQueueEntryAttributes.timestamp];
    }
    
    return [NSDictionary dictionaryWithDictionary:mutableDictionary];
}

@end
