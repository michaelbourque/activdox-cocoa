//
//  ADXHTTPClient.h
//  ActivDoxCore
//
//  Created by Gavin McKenzie on 12-06-06.
//  Copyright (c) 2012 ActivDox Inc. All rights reserved.
//

#import <SystemConfiguration/SystemConfiguration.h>
#import "AFHTTPClient.h"

extern NSString * const kADXServiceJSONKeyAccessToken;

@interface ADXHTTPClient : AFHTTPClient
@property (strong, nonatomic) NSString *accessToken;
- (void)cancelAllHTTPOperationsWithMethod:(NSString *)method;

- (NSMutableURLRequest *)unauthenticatedMultipartFormRequestWithMethod:(NSString *)method path:(NSString *)path parameters:(NSDictionary *)parameters constructingBodyWithBlock:(void (^)(id<AFMultipartFormData>))block;

@end
