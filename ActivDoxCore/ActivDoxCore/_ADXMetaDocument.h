// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to ADXMetaDocument.h instead.

#import <CoreData/CoreData.h>



extern const struct ADXMetaDocumentAttributes {
	__unsafe_unretained NSString *accountID;
	__unsafe_unretained NSString *doNotAutomaticallyDownloadContent;
	__unsafe_unretained NSString *documentID;
	__unsafe_unretained NSString *mostRecentNotesUpdate;
} ADXMetaDocumentAttributes;



extern const struct ADXMetaDocumentRelationships {
	__unsafe_unretained NSString *notes;
	__unsafe_unretained NSString *versions;
} ADXMetaDocumentRelationships;






@class ADXDocumentNote;
@class ADXDocument;










@interface ADXMetaDocumentID : NSManagedObjectID {}
@end

@interface _ADXMetaDocument : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (ADXMetaDocumentID*)objectID;





@property (nonatomic, strong) NSString* accountID;



//- (BOOL)validateAccountID:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* doNotAutomaticallyDownloadContent;




@property (atomic) BOOL doNotAutomaticallyDownloadContentValue;
- (BOOL)doNotAutomaticallyDownloadContentValue;
- (void)setDoNotAutomaticallyDownloadContentValue:(BOOL)value_;


//- (BOOL)validateDoNotAutomaticallyDownloadContent:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* documentID;



//- (BOOL)validateDocumentID:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSDate* mostRecentNotesUpdate;



//- (BOOL)validateMostRecentNotesUpdate:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSSet *notes;

- (NSMutableSet*)notesSet;




@property (nonatomic, strong) NSSet *versions;

- (NSMutableSet*)versionsSet;





@end


@interface _ADXMetaDocument (NotesCoreDataGeneratedAccessors)
- (void)addNotes:(NSSet*)value_;
- (void)removeNotes:(NSSet*)value_;
- (void)addNotesObject:(ADXDocumentNote*)value_;
- (void)removeNotesObject:(ADXDocumentNote*)value_;
@end

@interface _ADXMetaDocument (VersionsCoreDataGeneratedAccessors)
- (void)addVersions:(NSSet*)value_;
- (void)removeVersions:(NSSet*)value_;
- (void)addVersionsObject:(ADXDocument*)value_;
- (void)removeVersionsObject:(ADXDocument*)value_;
@end


@interface _ADXMetaDocument (CoreDataGeneratedPrimitiveAccessors)


- (NSString*)primitiveAccountID;
- (void)setPrimitiveAccountID:(NSString*)value;




- (NSNumber*)primitiveDoNotAutomaticallyDownloadContent;
- (void)setPrimitiveDoNotAutomaticallyDownloadContent:(NSNumber*)value;

- (BOOL)primitiveDoNotAutomaticallyDownloadContentValue;
- (void)setPrimitiveDoNotAutomaticallyDownloadContentValue:(BOOL)value_;




- (NSString*)primitiveDocumentID;
- (void)setPrimitiveDocumentID:(NSString*)value;




- (NSDate*)primitiveMostRecentNotesUpdate;
- (void)setPrimitiveMostRecentNotesUpdate:(NSDate*)value;





- (NSMutableSet*)primitiveNotes;
- (void)setPrimitiveNotes:(NSMutableSet*)value;



- (NSMutableSet*)primitiveVersions;
- (void)setPrimitiveVersions:(NSMutableSet*)value;


@end
