//
//  ADXCommonMacros.h
//  ActivDoxCore
//
//  Created by Gavin McKenzie on 12-05-09.
//  Copyright (c) 2012 ActivDox Inc. All rights reserved.
//

#import "ActivDoxCore.h"

#include "Logging.h"

BOOL IsEmpty(id thing);
BOOL addSkipBackupAttributeToItemAtURL(NSURL *url);
void LogNSError(NSString *msg, NSError *error);
void LogNSErrors(NSString *msg, NSArray *errors);

