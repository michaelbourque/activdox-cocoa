
#import "_ADXSyncQueueEntry.h"
#import "ADXEntityCRUDProtocol.h"

extern NSString * const kADXSyncQueueEntryDidSaveNotification;

extern const struct ADXSyncQueueEntryOperation {
    __unsafe_unretained NSString *insert;
    __unsafe_unretained NSString *update;
    __unsafe_unretained NSString *remove;
    __unsafe_unretained NSString *link;
    __unsafe_unretained NSString *unlink;
} ADXSyncQueueEntryOperation;

@interface ADXSyncQueueEntry : _ADXSyncQueueEntry {}
+ (ADXSyncQueueEntry *)insertEntryForEntity:(id<ADXEntityCRUDProtocol>)entity id:(NSString *)entityID accountID:(NSString *)accountID operation:(NSString *)operation payload:(id)payload userInfo:(id)userInfo context:(NSManagedObjectContext *)moc;
- (void)save:(ADXEntityCRUDProtocolCompletionBlock)completionBlock;
- (NSDictionary *)jsonDictionary;
@end
