// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to ADXEvent.m instead.

#import "_ADXEvent.h"


const struct ADXEventAttributes ADXEventAttributes = {
	.accountID = @"accountID",
	.id = @"id",
	.originatorID = @"originatorID",
	.payload = @"payload",
	.referencedDocumentID = @"referencedDocumentID",
	.resource = @"resource",
	.timeStamp = @"timeStamp",
	.type = @"type",
};



const struct ADXEventRelationships ADXEventRelationships = {
	.originator = @"originator",
};






@implementation ADXEventID
@end

@implementation _ADXEvent

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"ADXEvent" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"ADXEvent";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"ADXEvent" inManagedObjectContext:moc_];
}

- (ADXEventID*)objectID {
	return (ADXEventID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
	

	return keyPaths;
}




@dynamic accountID;






@dynamic id;






@dynamic originatorID;






@dynamic payload;






@dynamic referencedDocumentID;






@dynamic resource;






@dynamic timeStamp;






@dynamic type;






@dynamic originator;

	






@end




