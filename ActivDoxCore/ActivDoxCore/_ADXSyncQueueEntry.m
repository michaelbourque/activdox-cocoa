// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to ADXSyncQueueEntry.m instead.

#import "_ADXSyncQueueEntry.h"


const struct ADXSyncQueueEntryAttributes ADXSyncQueueEntryAttributes = {
	.accountID = @"accountID",
	.entityClassName = @"entityClassName",
	.entityID = @"entityID",
	.operation = @"operation",
	.payload = @"payload",
	.userInfo = @"userInfo",
};








@implementation ADXSyncQueueEntryID
@end

@implementation _ADXSyncQueueEntry

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"ADXSyncQueueEntry" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"ADXSyncQueueEntry";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"ADXSyncQueueEntry" inManagedObjectContext:moc_];
}

- (ADXSyncQueueEntryID*)objectID {
	return (ADXSyncQueueEntryID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
	

	return keyPaths;
}




@dynamic accountID;






@dynamic entityClassName;






@dynamic entityID;






@dynamic operation;






@dynamic payload;






@dynamic userInfo;











@end




