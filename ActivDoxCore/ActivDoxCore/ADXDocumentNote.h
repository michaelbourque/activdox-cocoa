
#import <CoreGraphics/CGGeometry.h>
#import "_ADXDocumentNote.h"
#import "ADXDocument.h"
#import "ADXEntityCRUDProtocol.h"

#if TARGET_OS_IPHONE
#import <UIKit/UIKit.h>
#endif

extern NSString *kADXDocumentNoteDidChangeNotification;

extern const struct ADXJSONDocumentNoteResponseAttributes {
    __unsafe_unretained NSString *resourcesArray;
    struct {
        __unsafe_unretained NSString *usersArray;
        __unsafe_unretained NSString *documentsArray;
    } resources;
    __unsafe_unretained NSString *notesArray;
} ADXJSONDocumentNoteResponseAttributes;

extern const struct ADXJSONDocumentNoteAttributes {
    __unsafe_unretained NSString *notesArray;
    struct {
        __unsafe_unretained NSString *id;
        __unsafe_unretained NSString *localID;
        __unsafe_unretained NSString *originator;
        __unsafe_unretained NSString *created;
        __unsafe_unretained NSString *updated;
        __unsafe_unretained NSString *generation;
        __unsafe_unretained NSString *openVersion;
        __unsafe_unretained NSString *closeVersion;
        __unsafe_unretained NSString *text;
        __unsafe_unretained NSString *document;
        __unsafe_unretained NSString *visibility;
        __unsafe_unretained NSString *status;
        __unsafe_unretained NSString *versionsArray;
        struct {
            __unsafe_unretained NSString *version;
            __unsafe_unretained NSString *locationArray;
            struct {
                __unsafe_unretained NSString *page;
                __unsafe_unretained NSString *highlightArray;
                struct {
                    __unsafe_unretained NSString *left;
                    __unsafe_unretained NSString *top;
                    __unsafe_unretained NSString *right;
                    __unsafe_unretained NSString *bottom;
                } highlight;
            } location;
        } versions;
    } note;
} ADXJSONDocumentNoteAttributes;

extern const struct ADXDocumentNoteVisibility {
    __unsafe_unretained NSString *personal;
    __unsafe_unretained NSString *author;
    __unsafe_unretained NSString *everyone;
} ADXDocumentNoteVisibility;

extern const struct ADXDocumentNoteStatus {
    __unsafe_unretained NSString *accessible;
    __unsafe_unretained NSString *revoked;
    __unsafe_unretained NSString *deleted;
    __unsafe_unretained NSString *inaccessible;
} ADXDocumentNoteStatus;

@interface ADXDocumentNote : _ADXDocumentNote <ADXEntityCRUDProtocol>
+ (BOOL)updateNotes:(NSArray *)notes accountID:(NSString *)accountID context:(NSManagedObjectContext *)moc;

@property (readonly, nonatomic) BOOL isAccessible;
@property (readonly, nonatomic) BOOL isDeletedStatus; // 'isDeleted' is a property signature already used by NSManagedObject; hence why this is called 'isDeletedStatus'
@property (readonly, nonatomic) BOOL isRevoked;
@property (readonly,nonatomic) BOOL isInaccessible;


// Inserts a note into the document's context. Caller is responsible for doing a save on the document's context after insertion.
+ (ADXDocumentNote *)insertNoteByUser:(ADXUser *)user forDocument:(ADXDocument *)document page:(NSUInteger)page text:(NSString *)text highlightRects:(NSArray *)highlightRects;
+ (ADXDocumentNote *)noteWithID:(NSString *)noteID accountID:(NSString *)accountID context:(NSManagedObjectContext *)moc;
+ (ADXDocumentNote *)noteWithLocalID:(NSString *)noteLocalID accountID:(NSString *)accountID context:(NSManagedObjectContext *)moc;
+ (NSArray *)notesToBeDeletedOnStartup:(NSManagedObjectContext *)moc;

#if TARGET_OS_IPHONE
+ (NSDictionary *)dictionaryFromRect:(CGRect)rect;
+ (CGRect)rectFromDictionary:(NSDictionary *)dictionary;
#else
+ (NSDictionary *)dictionaryFromRect:(NSRect)rect;
+ (NSRect)rectFromDictionary:(NSDictionary *)dictionary;
#endif

- (NSArray *)deepMutableLocationDataCopy;
- (NSArray *)locationsForVersion:(NSNumber *)versionID;
- (NSArray *)locationsForVersion:(NSNumber *)versionID page:(NSNumber *)page;
- (NSSet *)pagesForVersion:(NSNumber *)versionID;
- (NSArray *)orderedPagesForVersion:(NSNumber *)versionID;
- (NSInteger)firstpageForVersion:(NSNumber *)versionID;

- (BOOL)isClosed;
- (void)unsetClosed;

- (void)markDeleted;
- (void)saveWithOptionalSync:(BOOL)queueForSync completion:(ADXEntityCRUDProtocolCompletionBlock)completionBlock;
- (void)enqueueForSyncInContext:(NSManagedObjectContext *)moc completion:(ADXEntityCRUDProtocolCompletionBlock)completionBlock;
- (NSDictionary *)jsonDictionary;

- (void)moveNoteToPoint:(CGPoint)point inVersion:(NSNumber *)versionID;

@end
