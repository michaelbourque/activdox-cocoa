
#import "ADXMetaDocument.h"
#import "ADXCategories.h"
#import "ADXCommonMacros.h"
#import "ADXUserDefaults.h"

@implementation ADXMetaDocument

+ (ADXMetaDocument *)existingMetaDocumentWithID:(NSString *)documentID accountID:(NSString *)accountID context:(NSManagedObjectContext *)moc
{
    if (IsEmpty(documentID) || IsEmpty(accountID)) {
        LogError(@"received a nil or empty document ID (%@) or account ID (%@)", documentID, accountID);
        return nil;
    }
    
    ADXMetaDocument *result = [moc aps_fetchFirstObjectForEntityName:[ADXMetaDocument entityName]
                                                       withPredicate:@"(%K == %@) AND (%K == %@)", ADXMetaDocumentAttributes.documentID, documentID, ADXMetaDocumentAttributes.accountID, accountID];
    return result;
}

+ (ADXMetaDocument *)metaDocumentWithID:(NSString *)documentID accountID:(NSString *)accountID context:(NSManagedObjectContext *)moc
{
    if (IsEmpty(documentID) || IsEmpty(accountID)) {
        LogError(@"received a nil or empty document ID (%@) or account ID (%@)", documentID, accountID);
        return nil;
    }

    ADXMetaDocument *metaDocument = [ADXMetaDocument existingMetaDocumentWithID:documentID accountID:accountID context:moc];
    if (metaDocument) {
        return metaDocument;
    }
    
    metaDocument = [ADXMetaDocument insertInManagedObjectContext:moc];
    
    metaDocument.documentID = documentID;
    metaDocument.accountID = accountID;
    metaDocument.mostRecentNotesUpdate = nil;
    
    return metaDocument;
}

+ (NSArray *)metaDocumentsExcludedFromAutomaticContentDownload:(NSManagedObjectContext *)moc
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K == YES", ADXMetaDocumentAttributes.doNotAutomaticallyDownloadContent];
    NSArray *documents = [moc aps_fetchObjectsForEntityName:[ADXMetaDocument entityName] withPredicate:predicate];
    return documents;
}

+ (NSUInteger)numberOfDocumentsForAccount:(NSString *)accountID context:(NSManagedObjectContext *)moc
{
    NSUInteger result = 0;
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(%K == %@)", ADXMetaDocumentAttributes.accountID, accountID];
    NSArray *metaDocuments = [moc aps_fetchObjectsForEntityName:[ADXMetaDocument entityName] withPredicate:predicate];
    result = metaDocuments.count;
    
    return result;
}
@end
