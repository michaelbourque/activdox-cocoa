
#import "_ADXUser.h"
#import "ADXEntityCRUDProtocol.h"

@class ADXCollection;

extern const struct ADXJSONUserAttributes {
	__unsafe_unretained NSString *id;
    __unsafe_unretained NSString *loginName;
    __unsafe_unretained NSString *displayName;
} ADXJSONUserAttributes;

@interface ADXUser : _ADXUser <ADXEntityCRUDProtocol>
+ (BOOL)addUsers:(NSArray *)users updateExisting:(BOOL)updateExisting context:(NSManagedObjectContext *)moc;
- (NSString *)displayNameOrLoginName;
@end
