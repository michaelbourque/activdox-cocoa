// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to ADXUser.h instead.

#import <CoreData/CoreData.h>



extern const struct ADXUserAttributes {
	__unsafe_unretained NSString *avatarSeed;
	__unsafe_unretained NSString *createdAt;
	__unsafe_unretained NSString *displayName;
	__unsafe_unretained NSString *id;
	__unsafe_unretained NSString *loginName;
	__unsafe_unretained NSString *primaryEmail;
} ADXUserAttributes;



extern const struct ADXUserRelationships {
	__unsafe_unretained NSString *account;
	__unsafe_unretained NSString *authoredDocuments;
	__unsafe_unretained NSString *authoredNotes;
	__unsafe_unretained NSString *originatedEvents;
} ADXUserRelationships;






@class ADXAccount;
@class ADXDocument;
@class ADXDocumentNote;
@class ADXEvent;














@interface ADXUserID : NSManagedObjectID {}
@end

@interface _ADXUser : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (ADXUserID*)objectID;





@property (nonatomic, strong) NSNumber* avatarSeed;




@property (atomic) int32_t avatarSeedValue;
- (int32_t)avatarSeedValue;
- (void)setAvatarSeedValue:(int32_t)value_;


//- (BOOL)validateAvatarSeed:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSDate* createdAt;



//- (BOOL)validateCreatedAt:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* displayName;



//- (BOOL)validateDisplayName:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* id;



//- (BOOL)validateId:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* loginName;



//- (BOOL)validateLoginName:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* primaryEmail;



//- (BOOL)validatePrimaryEmail:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) ADXAccount *account;

//- (BOOL)validateAccount:(id*)value_ error:(NSError**)error_;




@property (nonatomic, strong) NSSet *authoredDocuments;

- (NSMutableSet*)authoredDocumentsSet;




@property (nonatomic, strong) NSSet *authoredNotes;

- (NSMutableSet*)authoredNotesSet;




@property (nonatomic, strong) NSSet *originatedEvents;

- (NSMutableSet*)originatedEventsSet;





@end


@interface _ADXUser (AuthoredDocumentsCoreDataGeneratedAccessors)
- (void)addAuthoredDocuments:(NSSet*)value_;
- (void)removeAuthoredDocuments:(NSSet*)value_;
- (void)addAuthoredDocumentsObject:(ADXDocument*)value_;
- (void)removeAuthoredDocumentsObject:(ADXDocument*)value_;
@end

@interface _ADXUser (AuthoredNotesCoreDataGeneratedAccessors)
- (void)addAuthoredNotes:(NSSet*)value_;
- (void)removeAuthoredNotes:(NSSet*)value_;
- (void)addAuthoredNotesObject:(ADXDocumentNote*)value_;
- (void)removeAuthoredNotesObject:(ADXDocumentNote*)value_;
@end

@interface _ADXUser (OriginatedEventsCoreDataGeneratedAccessors)
- (void)addOriginatedEvents:(NSSet*)value_;
- (void)removeOriginatedEvents:(NSSet*)value_;
- (void)addOriginatedEventsObject:(ADXEvent*)value_;
- (void)removeOriginatedEventsObject:(ADXEvent*)value_;
@end


@interface _ADXUser (CoreDataGeneratedPrimitiveAccessors)


- (NSNumber*)primitiveAvatarSeed;
- (void)setPrimitiveAvatarSeed:(NSNumber*)value;

- (int32_t)primitiveAvatarSeedValue;
- (void)setPrimitiveAvatarSeedValue:(int32_t)value_;




- (NSDate*)primitiveCreatedAt;
- (void)setPrimitiveCreatedAt:(NSDate*)value;




- (NSString*)primitiveDisplayName;
- (void)setPrimitiveDisplayName:(NSString*)value;




- (NSString*)primitiveId;
- (void)setPrimitiveId:(NSString*)value;




- (NSString*)primitiveLoginName;
- (void)setPrimitiveLoginName:(NSString*)value;




- (NSString*)primitivePrimaryEmail;
- (void)setPrimitivePrimaryEmail:(NSString*)value;





- (ADXAccount*)primitiveAccount;
- (void)setPrimitiveAccount:(ADXAccount*)value;



- (NSMutableSet*)primitiveAuthoredDocuments;
- (void)setPrimitiveAuthoredDocuments:(NSMutableSet*)value;



- (NSMutableSet*)primitiveAuthoredNotes;
- (void)setPrimitiveAuthoredNotes:(NSMutableSet*)value;



- (NSMutableSet*)primitiveOriginatedEvents;
- (void)setPrimitiveOriginatedEvents:(NSMutableSet*)value;


@end
