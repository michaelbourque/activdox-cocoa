#import "_ADXAccount.h"
#import "ADXEntityCRUDProtocol.h"

extern const struct ADXJSONAccountAttributes {
	__unsafe_unretained NSString *id;
    __unsafe_unretained NSString *accessToken;
    __unsafe_unretained NSString *loginName;
} ADXJSONAccountAttributes;

@interface ADXAccount : _ADXAccount <ADXEntityCRUDProtocol>
+ (NSArray *)allAccountsInContext:(NSManagedObjectContext *)moc;
+ (ADXAccount *)mostRecentlyUsedAccountInContext:(NSManagedObjectContext *)moc;
+ (ADXAccount *)accountWithLoginName:(NSString *)loginName context:(NSManagedObjectContext *)moc;
+ (ADXAccount *)accountWithLoginName:(NSString *)loginName password:(NSString *)password serverURL:(NSString *)serverURL context:(NSManagedObjectContext *)moc;
+ (NSString *)generateLocalIDForAccountID:(NSString *)accountID;

@property (strong, nonatomic) NSString *password;

@end
