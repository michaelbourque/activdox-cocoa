
#import "ADXSyncQueueEntry.h"
#import "ADXModel.h"
#import "ADXCommonMacros.h"

NSString * const kADXSyncQueueEntryDidSaveNotification = @"kADXSyncQueueEntryWillSaveNotification";

const struct ADXSyncQueueEntryOperation ADXSyncQueueEntryOperation = {
    .insert = @"insert",
    .update = @"update",
    .remove = @"remove",
    .link = @"link",
    .unlink = @"unlink",
};

@implementation ADXSyncQueueEntry

+ (ADXSyncQueueEntry *)insertEntryForEntity:(id<ADXEntityCRUDProtocol>)entity id:(NSString *)entityID accountID:(NSString *)accountID operation:(NSString *)operation payload:(id)payload userInfo:(id)userInfo context:(NSManagedObjectContext *)moc
{
    ADXSyncQueueEntry *entry = [ADXSyncQueueEntry insertInManagedObjectContext:moc];
    
    if (entry) {
        entry.entityClassName = [[entity class] entityName];
        entry.entityID = entityID;
        entry.accountID = accountID;
        entry.operation = operation;
        entry.payload = payload;
        entry.userInfo = userInfo;

        if (IsEmpty(entry.accountID) || IsEmpty(entityID) || !entity) {
            LogError(@"Failed attempt to insert sync queue entry with %@ id %@ accountID %@", entity, entityID, accountID);
            return nil;
        }
    }
    
    return entry;
}

- (void)save:(ADXEntityCRUDProtocolCompletionBlock)completionBlock
{
    __weak ADXSyncQueueEntry *weakSelf = self;
    NSManagedObjectContext *moc = self.managedObjectContext;
    
    [moc performBlock:^{
        NSError *saveError = nil;
        BOOL success = [moc save:&saveError];
        if (!success) {
            LogNSError(@"couldn't save sync entry", saveError);
        }
        [[NSNotificationCenter defaultCenter] postNotificationName:kADXSyncQueueEntryDidSaveNotification object:weakSelf];
        if (completionBlock) {
            dispatch_async(dispatch_get_main_queue(), ^{
                completionBlock(success, saveError);
            });
        }
    }];
}

#pragma mark - JSON Support

- (NSDictionary *)jsonDictionary
{
    NSDictionary *result = nil;
    Class<ADXEntityCRUDProtocol> entityClass = NSClassFromString(self.entityClassName);
    id<ADXEntityCRUDProtocol> entity = [entityClass entityWithID:self.entityID context:self.managedObjectContext];
    
    if ([entity respondsToSelector:@selector(jsonDictionary)]) {
        result = [entity jsonDictionary];
    }
    return result;
}

@end
