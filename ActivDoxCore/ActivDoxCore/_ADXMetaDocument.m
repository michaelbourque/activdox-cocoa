// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to ADXMetaDocument.m instead.

#import "_ADXMetaDocument.h"


const struct ADXMetaDocumentAttributes ADXMetaDocumentAttributes = {
	.accountID = @"accountID",
	.doNotAutomaticallyDownloadContent = @"doNotAutomaticallyDownloadContent",
	.documentID = @"documentID",
	.mostRecentNotesUpdate = @"mostRecentNotesUpdate",
};



const struct ADXMetaDocumentRelationships ADXMetaDocumentRelationships = {
	.notes = @"notes",
	.versions = @"versions",
};






@implementation ADXMetaDocumentID
@end

@implementation _ADXMetaDocument

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"ADXMetaDocument" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"ADXMetaDocument";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"ADXMetaDocument" inManagedObjectContext:moc_];
}

- (ADXMetaDocumentID*)objectID {
	return (ADXMetaDocumentID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
	
	if ([key isEqualToString:@"doNotAutomaticallyDownloadContentValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"doNotAutomaticallyDownloadContent"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}




@dynamic accountID;






@dynamic doNotAutomaticallyDownloadContent;



- (BOOL)doNotAutomaticallyDownloadContentValue {
	NSNumber *result = [self doNotAutomaticallyDownloadContent];
	return [result boolValue];
}


- (void)setDoNotAutomaticallyDownloadContentValue:(BOOL)value_ {
	[self setDoNotAutomaticallyDownloadContent:[NSNumber numberWithBool:value_]];
}


- (BOOL)primitiveDoNotAutomaticallyDownloadContentValue {
	NSNumber *result = [self primitiveDoNotAutomaticallyDownloadContent];
	return [result boolValue];
}

- (void)setPrimitiveDoNotAutomaticallyDownloadContentValue:(BOOL)value_ {
	[self setPrimitiveDoNotAutomaticallyDownloadContent:[NSNumber numberWithBool:value_]];
}





@dynamic documentID;






@dynamic mostRecentNotesUpdate;






@dynamic notes;

	
- (NSMutableSet*)notesSet {
	[self willAccessValueForKey:@"notes"];
  
	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"notes"];
  
	[self didAccessValueForKey:@"notes"];
	return result;
}
	

@dynamic versions;

	
- (NSMutableSet*)versionsSet {
	[self willAccessValueForKey:@"versions"];
  
	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"versions"];
  
	[self didAccessValueForKey:@"versions"];
	return result;
}
	






@end




