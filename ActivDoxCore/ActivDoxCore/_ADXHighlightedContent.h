// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to ADXHighlightedContent.h instead.

#import <CoreData/CoreData.h>



extern const struct ADXHighlightedContentAttributes {
	__unsafe_unretained NSString *data;
} ADXHighlightedContentAttributes;



extern const struct ADXHighlightedContentRelationships {
	__unsafe_unretained NSString *document;
} ADXHighlightedContentRelationships;






@class ADXDocument;




@interface ADXHighlightedContentID : NSManagedObjectID {}
@end

@interface _ADXHighlightedContent : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (ADXHighlightedContentID*)objectID;





@property (nonatomic, strong) NSData* data;



//- (BOOL)validateData:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) ADXDocument *document;

//- (BOOL)validateDocument:(id*)value_ error:(NSError**)error_;





@end



@interface _ADXHighlightedContent (CoreDataGeneratedPrimitiveAccessors)


- (NSData*)primitiveData;
- (void)setPrimitiveData:(NSData*)value;





- (ADXDocument*)primitiveDocument;
- (void)setPrimitiveDocument:(ADXDocument*)value;


@end
