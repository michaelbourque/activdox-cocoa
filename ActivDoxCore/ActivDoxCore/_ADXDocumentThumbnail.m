// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to ADXDocumentThumbnail.m instead.

#import "_ADXDocumentThumbnail.h"


const struct ADXDocumentThumbnailAttributes ADXDocumentThumbnailAttributes = {
	.data = @"data",
};



const struct ADXDocumentThumbnailRelationships ADXDocumentThumbnailRelationships = {
	.document = @"document",
};






@implementation ADXDocumentThumbnailID
@end

@implementation _ADXDocumentThumbnail

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"ADXDocumentThumbnail" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"ADXDocumentThumbnail";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"ADXDocumentThumbnail" inManagedObjectContext:moc_];
}

- (ADXDocumentThumbnailID*)objectID {
	return (ADXDocumentThumbnailID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
	

	return keyPaths;
}




@dynamic data;






@dynamic document;

	






@end




