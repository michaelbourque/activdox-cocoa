
#import "_ADXEventQueueEntry.h"
#import "ADXEvent.h"
#import "ADXEntityCRUDProtocol.h"


extern const struct ADXJSONEventQueueEntryAttributes {
    __unsafe_unretained NSString *eventType;
    __unsafe_unretained NSString *timestamp;
    __unsafe_unretained NSString *payloadDict;
    struct {
        __unsafe_unretained NSString *document;
        __unsafe_unretained NSString *version;
        __unsafe_unretained NSString *page;
    } payload;
} ADXJSONEventQueueEntryAttributes;

extern NSUInteger const kADXEventQueueEntryBatchSize;

@class ADXUser;

@interface ADXEventQueueEntry : _ADXEventQueueEntry {}
+ (BOOL)insertEventType:(NSString *)type accountID:(NSString *)accountID payload:(NSDictionary *)payload context:(NSManagedObjectContext *)moc;
- (NSDictionary *)jsonDictionary;
@end
