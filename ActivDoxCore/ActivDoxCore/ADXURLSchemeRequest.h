//
//  ADXURLSchemeRequest.h
//  DesktopReader
//
//  Created by Steve Tibbett on 2012-08-24.
//  Copyright (c) 2012 ActivDox. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString *kADXURLScheme;
extern NSString *kADXURLSchemeActionOpen;

@protocol ADXURLSchemeRequestDelegate <NSObject>
@optional
- (void)urlHandlerOpenDocumentWithID:(NSString *)documentID;
- (void)urlHandlerShowLoginForUserID:(NSString *)userID;
@end

// Notification that a new URL launch request has been received
extern NSString *kADXURLSchemeRequestNotification;

//
// This class is intended to hang on to an adox:// URL request while the
// application launches (if required), connects to the appropriate server,
// downloads a document if required, and finally executes the action in the
// URL.  Pseudocode:
//
//    ADXURLSchemeRequest *request = [[ADXURLSchemeRequest alloc] initWithURL:url];
//    request.delegate = self;
//    if (request.requiredServer != nil && ![self connectedToServer:requiredServer]) {
//      ... connect to server ...
//    }
//
//    if (request.requiredDocumentID != nil && ![self haveDocument:self.requiredDocumentID]) {
//      ... download the document ...
//    }
//
//    [request dispatch];
//
// In reality it will be much more involved because connecting to the server and
// downloading the document are complex, asynchronous actions (that can fail), but
// that's the correct sequence.
//

@interface ADXURLSchemeRequest : NSObject

// Initialize with an adox:// URL
- (id)initWithURL:(NSString *)URLString;

@property (strong, nonatomic) id<ADXURLSchemeRequestDelegate> delegate;

// If not nil, then this is the server the app must be connected to.
@property (strong, nonatomic) NSString *requiredServer;

// If not nil, then this document must be available locally.
@property (strong, nonatomic) NSString *requiredDocumentID;

// If not nil, then if we're opening a document, this is the version to open
@property (strong, nonatomic) NSNumber *requiredDocumentVersion;

// If not nil, then this is the event ID that the user wants actioned
@property (strong, nonatomic) NSString *requiredEventID;

// If this is true then the app must be pre-login.
@property (nonatomic) BOOL requireThatAppHasNotLoggedIn;

// Has the app already initiated an update attempt to fetch this document ID?
@property (nonatomic) BOOL updateAttempted;

// Dispatch the URL.  This will invoke one of the handler methods on the delegate.  Ensure
// the preconditions (requiredServerAddress, requiredDocumentID) are met before calling.
- (void)dispatch;

// Notification that the application has logged in, so that if a server call is required
// to take further action, this is now possible.
- (void)applicationDidLoginWithAccount:(ADXAccount *)account;

@end
