//
//  ADXV1ServiceClient.h
//  ActivDoxCore
//
//  Created by Gavin McKenzie on 12-06-05.
//  Copyright (c) 2012 ActivDox Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <SystemConfiguration/SystemConfiguration.h>

#import "AFHTTPClient.h"
#import "ADXServiceClientCommon.h"

extern NSString * const kADXV1ServiceAPIVersion;
extern NSString * const kADXV1ServiceReceivedDocumentsLiteral;
extern NSString * const kADXV1ServicePublishedDocumentsLiteral;
extern NSString * const kADXV1ServiceDocumentContentLiteral;
extern NSString * const kADXV1ServiceUserMeLiteral;

extern NSString * const kADXV1ServiceJSONKeyID;

typedef void(^ADXV1ServiceRequestCompletionBlock)(NSInteger statusCode, id response, NSError *error);
typedef void(^ADXV1ServiceDownloadProgressBlock)(NSUInteger bytesRead, long long totalBytesRead, long long totalBytesExpectedToRead);

@interface ADXV1ServiceClient : NSObject
@property (readonly, nonatomic) NSURL *baseURL;
@property (strong, readonly, nonatomic) NSDictionary *loginResponse;
@property (readonly, nonatomic) NSString *userID;
@property (readonly, nonatomic) NSString *version;
- (id)initWithBaseURL:(NSURL *)baseURL userID:(NSString *)userID;

- (AFNetworkReachabilityStatus)networkReachabilityStatus;
- (void)setReachabilityStatusChangeBlock:(void (^)(AFNetworkReachabilityStatus status))block;

- (void)getAccessTokenForLoginName:(NSString *)loginName password:(NSString *)password completion:(ADXV1ServiceRequestCompletionBlock)completionBlock;

- (void)registerUserWithLoginName:(NSString *)loginName email:(NSString *)emailAddress password:(NSString *)password completion:(ADXV1ServiceRequestCompletionBlock)completionBlock;

- (void)getContainer:(NSString *)containerType folder:(NSString *)folderID since:(NSDate *)since completion:(ADXV1ServiceRequestCompletionBlock)completionBlock;
- (void)getDocumentProperties:(NSString *)documentID container:(NSString *)containerType completion:(ADXV1ServiceRequestCompletionBlock)completionBlock;
- (void)getDocumentThumbnail:(NSString *)documentID version:(NSUInteger)version container:(NSString *)containerType completion:(ADXV1ServiceRequestCompletionBlock)completionBlock;
- (void)getDocumentVersions:(NSString *)documentID container:(NSString *)containerType completion:(ADXV1ServiceRequestCompletionBlock)completionBlock;
- (NSInteger)numberOfVersionsForDocument:(NSString *)documentID container:(NSString *)containerType completion:(ADXV1ServiceRequestCompletionBlock)completionBlock;

- (void)cancelAllDocumentDownloads;

- (AFHTTPRequestOperation *)getContentOperationForDocumentID:(NSString *)documentID
                                                     version:(NSUInteger)version
                                                 highlighted:(BOOL)highlighted
                                                   container:(NSString *)containerType
                                          backgroundDownload:(BOOL)backgroundDownload
                                                    progress:(ADXV1ServiceDownloadProgressBlock)progressBlock
                                                  completion:(ADXV1ServiceRequestCompletionBlock)completionBlock;
- (void)getDocumentContentViaOperation:(AFHTTPRequestOperation *)operation;
- (void)getDocumentContent:(NSString *)documentID version:(NSUInteger)version container:(NSString *)containerType completion:(ADXV1ServiceRequestCompletionBlock)completionBlock;
- (void)getHighlightedDocumentContent:(NSString *)documentID version:(NSUInteger)version container:(NSString *)containerType completion:(ADXV1ServiceRequestCompletionBlock)completionBlock;
- (void)getDocumentNotes:(NSString *)documentID version:(NSUInteger)version container:(NSString *)containerType completion:(ADXV1ServiceRequestCompletionBlock)completionBlock;
- (void)addDocumentNote:(NSString *)documentID noteJSON:(NSData *)noteJSON version:(NSUInteger)version container:(NSString *)containerType completion:(ADXV1ServiceRequestCompletionBlock)completionBlock;
- (void)updateDocumentNote:(NSString *)documentID noteID:(NSString *)noteID noteJSON:(NSData *)noteJSON version:(NSUInteger)version container:(NSString *)containerType completion:(ADXV1ServiceRequestCompletionBlock)completionBlock;
- (void)deleteDocumentNoteWithID:(NSString *)noteID fromDocumentWithID:(NSString *)documentID version:(NSUInteger)version container:(NSString *)containerType completion:(ADXV1ServiceRequestCompletionBlock)completionBlock;

- (void)getDocumentChanges:(NSString *)documentID version:(NSUInteger)version container:(NSString *)containerType completion:(ADXV1ServiceRequestCompletionBlock)completionBlock;
- (void)getDocumentChanges:(NSString *)documentID betweenVersion:(NSUInteger)version andVersion:(NSUInteger)otherVersion container:(NSString *)containerType completion:(ADXV1ServiceRequestCompletionBlock)completionBlock;

- (void)getFolderProperties:(NSString *)folderID completion:(ADXV1ServiceRequestCompletionBlock)completionBlock;
- (void)createFolder:(NSString *)name inContainer:(NSString *)containerType completion:(ADXV1ServiceRequestCompletionBlock)completionBlock;
- (void)updateFolder:(NSString *)folderID completion:(ADXV1ServiceRequestCompletionBlock)completionBlock;

- (void)updateDeviceToken:(NSData *)newToken oldToken:(NSData *)oldToken completion:(ADXV1ServiceRequestCompletionBlock)completionBlock;

- (void)uploadDocumentAtURL:(NSURL *)documentURL
                      title:(NSString *)title
                 recipients:(NSArray *)recipients
                 completion:(ADXV1ServiceRequestCompletionBlock)completionBlock;
- (void)shareDocumentWithID:(NSString *)documentID withEmailAddresses:(NSArray *)addresses message:(NSString *)message completion:(ADXV1ServiceRequestCompletionBlock)completionBlock;

- (void)getEventID:(NSString *)eventID completion:(ADXV1ServiceRequestCompletionBlock)completionBlock;
- (void)getUserEventID:(NSString *)eventID completion:(ADXV1ServiceRequestCompletionBlock)completionBlock;
- (void)getEventsSinceEventID:(NSString *)eventID type:(NSString *)eventType limit:(NSUInteger)limit completion:(ADXV1ServiceRequestCompletionBlock)completionBlock;
- (void)postEvents:(NSArray *)events completion:(ADXV1ServiceRequestCompletionBlock)completionBlock;

@end
