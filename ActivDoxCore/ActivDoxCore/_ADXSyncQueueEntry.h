// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to ADXSyncQueueEntry.h instead.

#import <CoreData/CoreData.h>



extern const struct ADXSyncQueueEntryAttributes {
	__unsafe_unretained NSString *accountID;
	__unsafe_unretained NSString *entityClassName;
	__unsafe_unretained NSString *entityID;
	__unsafe_unretained NSString *operation;
	__unsafe_unretained NSString *payload;
	__unsafe_unretained NSString *userInfo;
} ADXSyncQueueEntryAttributes;



















@class NSObject;


@class NSObject;

@interface ADXSyncQueueEntryID : NSManagedObjectID {}
@end

@interface _ADXSyncQueueEntry : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (ADXSyncQueueEntryID*)objectID;





@property (nonatomic, strong) NSString* accountID;



//- (BOOL)validateAccountID:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* entityClassName;



//- (BOOL)validateEntityClassName:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* entityID;



//- (BOOL)validateEntityID:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* operation;



//- (BOOL)validateOperation:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) id payload;



//- (BOOL)validatePayload:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) id userInfo;



//- (BOOL)validateUserInfo:(id*)value_ error:(NSError**)error_;






@end



@interface _ADXSyncQueueEntry (CoreDataGeneratedPrimitiveAccessors)


- (NSString*)primitiveAccountID;
- (void)setPrimitiveAccountID:(NSString*)value;




- (NSString*)primitiveEntityClassName;
- (void)setPrimitiveEntityClassName:(NSString*)value;




- (NSString*)primitiveEntityID;
- (void)setPrimitiveEntityID:(NSString*)value;




- (NSString*)primitiveOperation;
- (void)setPrimitiveOperation:(NSString*)value;




- (id)primitivePayload;
- (void)setPrimitivePayload:(id)value;




- (id)primitiveUserInfo;
- (void)setPrimitiveUserInfo:(id)value;




@end
