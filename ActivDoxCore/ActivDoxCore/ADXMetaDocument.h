
#import "_ADXMetaDocument.h"

@interface ADXMetaDocument : _ADXMetaDocument {}
+ (ADXMetaDocument *)existingMetaDocumentWithID:(NSString *)documentID accountID:(NSString *)accountID context:(NSManagedObjectContext *)moc;
+ (ADXMetaDocument *)metaDocumentWithID:(NSString *)documentID accountID:(NSString *)accountID context:(NSManagedObjectContext *)moc;
+ (NSArray *)metaDocumentsExcludedFromAutomaticContentDownload:(NSManagedObjectContext *)moc;
+ (NSUInteger)numberOfDocumentsForAccount:(NSString *)accountID context:(NSManagedObjectContext *)moc;
@end
