
#import "_ADXDocument.h"
#import "ADXEntityCRUDProtocol.h"
#import "ADXHighlightedContentHelper.h"

extern const struct ADXJSONDocumentsResponseAttributes {
    __unsafe_unretained NSString *documents;
    __unsafe_unretained NSString *users;
    __unsafe_unretained NSString *collections;
} ADXJSONDocumentsResponseAttributes;

extern const struct ADXJSONDocumentAttributes {
	__unsafe_unretained NSString *id;
    __unsafe_unretained NSString *uploadDate;
    __unsafe_unretained NSString *version;
    __unsafe_unretained NSString *collections;
    __unsafe_unretained NSString *title;
    __unsafe_unretained NSString *author;
    __unsafe_unretained NSString *documentDescription;
    __unsafe_unretained NSString *errorMessage;
    __unsafe_unretained NSString *message;
    __unsafe_unretained NSString *notes;
    __unsafe_unretained NSString *status;
    __unsafe_unretained NSString *differencesDict;
    struct {
        __unsafe_unretained NSString *changeCount;
        __unsafe_unretained NSString *changedPages;
    } differences;
} ADXJSONDocumentAttributes;

extern const struct ADXDocumentStatus {
    __unsafe_unretained NSString *accessible;
    __unsafe_unretained NSString *revoked;
    __unsafe_unretained NSString *deleted;
    __unsafe_unretained NSString *removed;
    __unsafe_unretained NSString *inaccessible;
} ADXDocumentStatus;

typedef enum {
    ADXDocumentMakeLatestVersionFailed = 0,
    ADXDocumentMakeLatestVersionNoChange,
    ADXDocumentMakeLatestVersionSuccess,
} ADXDocumentMakeLatestVersionResponse;

// FIXME - The methods for adding documents and documentsNeedingContentDownload needs to accept an account id or user id to associate/filter the docs to a user/server.

@interface ADXDocument : _ADXDocument <ADXEntityCRUDProtocol>
+ (void)dumpInContext:(NSManagedObjectContext *)moc;

+ (BOOL)addDocuments:(NSArray *)documents accountID:(NSString *)accountID updateExisting:(BOOL)updateExisting context:(NSManagedObjectContext *)moc;
+ (BOOL)updateDocuments:(NSArray *)documents accountID:(NSString *)accountID context:(NSManagedObjectContext *)moc;

+ (ADXDocument *)latestDocumentWithID:(NSString *)documentID accountID:(NSString *)accountID context:(NSManagedObjectContext *)moc;
+ (NSNumber *)versionOfLatestDocumentWithID:(NSString *)documentID accountID:(NSString *)accountID context:(NSManagedObjectContext *)moc;
+ (ADXDocument *)mostRecentlyOpenedDocumentWithID:(NSString *)documentID accountID:(NSString *)accountID context:(NSManagedObjectContext *)moc;
+ (ADXDocument *)documentWithID:(NSString *)documentID version:(NSNumber *)versionID accountID:(NSString *)accountID context:(NSManagedObjectContext *)moc;

+ (NSArray *)allDocumentsInContext:(NSManagedObjectContext *)moc;
+ (NSArray *)allVersionsOfDocument:(NSString *)documentID accountID:(NSString *)accountID context:(NSManagedObjectContext *)moc;
+ (NSArray *)inaccessibleVersionsOfDocument:(NSString *)documentID accountID:(NSString *)accountID context:(NSManagedObjectContext *)moc;
+ (NSArray *)allInaccessibleDocumentsInContext:(NSManagedObjectContext *)moc objectIDsOnly:(BOOL)objectIDsOnly;
+ (ADXDocumentMakeLatestVersionResponse)makeLatestVersionOfDocument:(NSString *)documentID version:(NSNumber *)version accountID:(NSString *)accountID context:(NSManagedObjectContext *)moc;
+ (ADXDocumentMakeLatestVersionResponse)makeDocumentLatestIfNewer:(NSDictionary *)document accountID:(NSString *)accountID context:(NSManagedObjectContext *)moc;
+ (BOOL)makeDocumentInaccessible:(NSString *)documentID accountID:(NSString *)accountID status:(NSString *)status context:(NSManagedObjectContext *)moc;
+ (void)restoreInaccessibleVersions:(NSString *)documentID accountID:(NSString *)accountID context:(NSManagedObjectContext *)moc;
+ (NSArray *)makeDocumentsLatestIfNewer:(NSArray *)documents accountID:(NSString *)accountID context:(NSManagedObjectContext *)moc;
+ (NSArray *)onlyInaccessibleDocumentsFrom:(NSArray *)documents;
+ (NSArray *)onlyAccessibleDocumentsFrom:(NSArray *)documents;

- (NSString *)versionDisplayLabel;
- (NSString *)publishedDateDisplayLabel:(BOOL)relative;
- (BOOL)accessible;

+ (NSArray *)documentsToBeDeletedOnStartup:(NSManagedObjectContext *)moc;
+ (NSArray *)documentIDsToBeDownloadedWithContext:(NSManagedObjectContext *)moc;

- (BOOL)shouldBeDeletedOnStartup;

//- (ADXHighlightedContentHelper*) highlightedContentHelper;

@end
