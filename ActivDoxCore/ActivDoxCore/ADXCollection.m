
#import "ADXCollection.h"
#import "ADXCategories.h"
#import "ADXCommonMacros.h"

const struct ADXJSONCollectionsResponseAttributes ADXJSONCollectionsResponseAttributes = {
    .collections = @"collections",
};

const struct ADXJSONCollectionAttributes ADXJSONCollectionAttributes = {
    .id = @"id",
    .localID = @"localID",
    .status = @"status",
    .title = @"title",
    .type = @"type",
    .collectionsArray = @"collections",
    .usersArray = @"users",
    .documentsArray = @"documents",
    .documents = {
        .id = @"id",
        .status = @"status",
    },
};

const struct ADXCollectionStatus ADXCollectionStatus = {
    .accessible = @"Accessible",
    .deleted = @"Deleted",
};

const struct ADXCollectionType ADXCollectionType = {
    .system = @"System",
    .user = @"User",
};

@implementation ADXCollection

#pragma mark - ADXEntityCRUDProtocol

+ (void)dumpInContext:(NSManagedObjectContext *)moc
{
    NSArray *allCollections = [ADXCollection allCollectionsInContext:moc];
    NSLog(@"BEGIN ADXCollection dump");
    for (ADXCollection *collection in allCollections) {
        NSLog(@"**COLLECTION id:%@ / localID: %@ / status:%@ / title:%@", collection.id, collection.localID, collection.status, collection.title);
        NSLog(@"%@", collection);
        for (ADXDocument *document in collection.documents) {
            NSLog(@"\t Document %@ / %@ / %@ / %@", document.id, document.title, document.version, document.collections);
        }
    }
    NSLog(@"END ADXFolder dump");
}

+ (id)collectionWithID:(NSString *)collectionID orLocalID:(NSString *)localID accountID:(NSString *)accountID context:(NSManagedObjectContext *)moc
{
    ADXCollection *collection = nil;
    if (!IsEmpty(collectionID)) {
        collection = [ADXCollection collectionWithID:collectionID accountID:accountID context:moc];
    }
    if (!collection) {
        collection = [ADXCollection collectionWithLocalID:localID accountID:accountID context:moc];
    }
    return collection;
}

+ (id)collectionWithID:(NSString *)collectionID accountID:(NSString *)accountID context:(NSManagedObjectContext *)moc
{
    if (IsEmpty(collectionID)) {
        return nil;
    }
    ADXCollection *result = [moc aps_fetchFirstObjectForEntityName:[ADXCollection entityName]
                                                     withPredicate:@"(%K == %@) AND (%K == %@)", ADXCollectionAttributes.id, collectionID, ADXCollectionAttributes.accountID, accountID];
    return result;
}

+ (id)collectionWithLocalID:(NSString *)localID accountID:(NSString *)accountID context:(NSManagedObjectContext *)moc
{
    if (IsEmpty(localID)) {
        return nil;
    }
    ADXCollection *result = [moc aps_fetchFirstObjectForEntityName:[ADXCollection entityName]
                                                     withPredicate:@"(%K == %@) AND (%K == %@)", ADXCollectionAttributes.localID, localID, ADXCollectionAttributes.accountID, accountID];
    return result;
}

+ (id)entityWithID:(NSString *)entityID context:(NSManagedObjectContext *)moc
{
    NSAssert(NO, @"ADXCollection entityWithID not supported. Use ADXCollection collectionWithID:accountID: instead.");
    return nil;
}

+ (BOOL)updateEntity:(id)entity fromDictionary:(NSDictionary *)dictionary context:(NSManagedObjectContext *)moc
{    
    if (!entity || IsEmpty(dictionary) || !moc) {
        LogError(@"Received bad params");
        return NO;
    }
    
    ADXCollection *collection = entity;
    if (IsEmpty(collection.id)) {
        NSString *collectionID = [dictionary aps_objectForKeyOrNil:ADXJSONCollectionAttributes.id];
        if (IsEmpty(collectionID)) {
            LogError(@"No collection id was supplied in dictionary");
            return NO;
        }
        collection.id = collectionID;
    }
    
    NSString *accountID = [dictionary aps_objectForKeyOrNil:ADXCollectionAttributes.accountID];
    if (IsEmpty(accountID)) {
        LogError(@"Cannot update a collection without an account id.");
        return NO;
    }
    collection.accountID = accountID;

    NSString *localID = [dictionary aps_objectForKeyOrNil:ADXJSONCollectionAttributes.localID];
    if (IsEmpty(localID)) {
        collection.localID = [ADXAccount generateLocalIDForAccountID:accountID];
    } else {
        collection.localID = localID;
    }

    collection.title = [dictionary aps_objectForKeyOrNil:ADXJSONCollectionAttributes.title];
    collection.status = [dictionary aps_objectForKeyOrNil:ADXJSONCollectionAttributes.status];
    collection.type = [dictionary aps_objectForKeyOrNil:ADXJSONCollectionAttributes.type];
        
    LogTrace(@"Updated collection: %@", collection);

    return YES;
}

+ (id)addEntityFromDictionary:(NSDictionary *)dictionary context:(NSManagedObjectContext *)moc
{    
    if (IsEmpty(dictionary)) {
        LogError(@"Received a nil dictionary.");
        return nil;
    }
    
    NSString *collectionID = [dictionary objectForKey:ADXCollectionAttributes.id];
    if (IsEmpty(collectionID)) {
        LogError(@"Received a dictionary with no collection id");
        return nil;
    }
    
    NSString *accountID = [dictionary objectForKey:ADXCollectionAttributes.accountID];
    if (IsEmpty(accountID)) {
        LogError(@"Received a dictionary with no account id");
        return nil;
    }
    
    ADXCollection *collection = [ADXCollection collectionWithID:collectionID accountID:accountID context:moc];
    if (collection) {
        LogError(@"Collection with id %@ already exists.", collectionID);
        return nil;
    }
    
    ADXCollection *newCollection = [ADXCollection insertInManagedObjectContext:moc];
    LogTrace(@"Inserted collection with id %@", [dictionary aps_objectForKeyOrNil:ADXJSONCollectionAttributes.id]);
    
    NSString *localID = [dictionary aps_objectForKeyOrNil:ADXJSONCollectionAttributes.localID];
    if (IsEmpty(localID)) {
        newCollection.localID = [ADXAccount generateLocalIDForAccountID:accountID];
    }

    BOOL success = [ADXCollection updateEntity:newCollection fromDictionary:dictionary context:moc];
    if (success) {
        return newCollection;
    } else {
        return nil;
    }
}

+ (BOOL)addOrUpdateCollection:(NSDictionary *)dictionary accountID:(NSString *)accountID context:(NSManagedObjectContext *)moc
{
    BOOL success = NO;

    NSDictionary *dictionaryWithAccountID = [dictionary aps_dictionaryMergedWithDictionary:@{ADXCollectionAttributes.accountID : accountID}];

    NSString *collectionID = [dictionary aps_objectForKeyOrNil:ADXJSONCollectionAttributes.id];
    NSString *collectionLocalID = [dictionary aps_objectForKeyOrNil:ADXJSONCollectionAttributes.localID];
    ADXCollection *collection = [ADXCollection collectionWithID:collectionID orLocalID:collectionLocalID accountID:accountID context:moc];
    
    if (!collection) {
        collection = [ADXCollection addEntityFromDictionary:dictionaryWithAccountID context:moc];
        success = collection ? YES : NO;
    } else {
        success = [ADXCollection updateEntity:collection fromDictionary:dictionaryWithAccountID context:moc];
    }
    
    return success;
}

+ (BOOL)updateCollections:(NSArray *)collections accountID:(NSString *)accountID removeMissingCollections:(BOOL)removeMissingCollections context:(NSManagedObjectContext *)moc
{
    BOOL success = YES;
    
    if (IsEmpty(collections)) {
        LogError(@"Received a nil/empty array");
        return NO;
    }

    if (removeMissingCollections) {
        [ADXCollection removeCollectionsNotContainedInArray:collections accountID:accountID context:moc];
    }
    
    for (NSDictionary *dictionary in collections) {
        NSString *collectionID = [dictionary aps_objectForKeyOrNil:ADXJSONCollectionAttributes.id];
        NSString *collectionLocalID = [dictionary aps_objectForKeyOrNil:ADXJSONCollectionAttributes.localID];
        NSString *status = [dictionary aps_objectForKeyOrNil:ADXJSONCollectionAttributes.status];

        if (IsEmpty(collectionID)) {
            LogError(@"Received a dictionary with no document ID");
            return NO;
        }
        
        ADXCollection *collection = [ADXCollection collectionWithID:collectionID orLocalID:collectionLocalID accountID:accountID context:moc];

        BOOL updateSuccess = NO;
        if ([status isEqualToString:ADXCollectionStatus.deleted]) {
            collection.status = status;
            collection.safeToDeleteOnStartup = @YES;
            updateSuccess = YES;
        } else {
            updateSuccess = [ADXCollection addOrUpdateCollection:dictionary accountID:(NSString *)accountID context:moc];
        }
        
        if (!updateSuccess) {
            success = NO;
            break;
        }
    }
    
    return success;
}

+ (BOOL)removeCollectionsNotContainedInArray:(NSArray *)collections accountID:(NSString *)accountID context:(NSManagedObjectContext *)moc
{
    BOOL success = YES;
    
    if (IsEmpty(collections)) {
        LogError(@"Received a nil/empty array");
        return NO;
    }
    
    NSArray *localCollectionIDs = [[ADXCollection collectionsForAccountID:accountID context:moc] valueForKey:ADXCollectionAttributes.id];
    localCollectionIDs = [localCollectionIDs filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"self != nil"]];
    
    NSArray *keepCollectionIDs = [collections valueForKey:ADXCollectionAttributes.id];
    keepCollectionIDs = [keepCollectionIDs filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"self != nil"]];

    for (NSString *localCollectionID in localCollectionIDs) {
        if (![keepCollectionIDs containsObject:localCollectionID]) {
            ADXCollection *collectionToRemove = [ADXCollection collectionWithID:localCollectionID accountID:accountID context:moc];
            [collectionToRemove markDeleted];
        }
    }
    
    return success;
}

+ (ADXCollection *)insertCollectionForAccountID:(NSString *)accountID type:(NSString *)type context:(NSManagedObjectContext *)moc
{
    ADXCollection *collection = [ADXCollection insertInManagedObjectContext:moc];
    if (collection) {
        collection.id = nil;
        collection.localID = [ADXAccount generateLocalIDForAccountID:accountID];
        collection.accountID = accountID;
        collection.safeToDeleteOnStartup = @NO;
        collection.mostRecentRefresh = nil;
        collection.status = ADXCollectionStatus.accessible;
        collection.title = nil;
        collection.type = type;
        collection.created = [NSDate date];
    } else {
        LogError(@"couldn't insert new collection for accountID:%@", accountID);
    }

    return collection;
}

+ (BOOL)deleteCollection:(NSString *)collectionID localID:(NSString *)localID accountID:(NSString *)accountID context:(NSManagedObjectContext *)moc
{
    BOOL success = NO;

    if (IsEmpty(collectionID) && IsEmpty(localID)) {
        LogError(@"Received a nil/empty collection id and/or localID");
        return NO;
    }
    
    ADXCollection *collection = [ADXCollection collectionWithID:collectionID orLocalID:localID accountID:accountID context:moc];
    [moc deleteObject:collection];
    success = YES;
    
    return success;
}

+ (NSArray *)allCollectionsInContext:(NSManagedObjectContext *)moc
{
    NSArray *results = [moc aps_fetchAllForEntityName:[ADXCollection entityName]];
    return results;
}

+ (NSArray *)allInaccessibleCollectionssInContext:(NSManagedObjectContext *)moc objectIDsOnly:(BOOL)objectIDsOnly
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K != %@",
                              ADXCollectionAttributes.status, ADXCollectionStatus.accessible];
    
    NSArray *collections = [moc aps_fetchObjectsForEntityName:[ADXCollection entityName] objectIDsOnly:objectIDsOnly sortDescriptors:nil withPredicate:predicate];
    return collections;
}

+ (NSArray *)collectionsToBeDeletedOnStartup:(NSManagedObjectContext *)moc
{
    NSArray *inaccessibleCollections = [ADXCollection allInaccessibleCollectionssInContext:moc objectIDsOnly:YES];
    return inaccessibleCollections;
}

+ (NSArray *)collectionsForAccountID:(NSString *)accountID context:(NSManagedObjectContext *)moc
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(%K == %@)", ADXCollectionAttributes.accountID, accountID];
    NSArray *results = [moc aps_fetchObjectsForEntityName:[ADXCollection entityName] withPredicate:predicate];
    return results;
}

+ (ADXCollection *)collectionMatchingTitle:(NSString *)title forAccountID:(NSString *)accountID context:(NSManagedObjectContext *)moc
{
    __block ADXCollection *result = nil;
    NSArray *collections = [ADXCollection collectionsForAccountID:accountID context:moc];
    [collections enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        ADXCollection *collection = (ADXCollection *)obj;
        if ([collection.title isEqualToString:title]) {
            result = collection;
            *stop = YES;
        }
    }];
    return result;
}

- (NSUInteger)numberOfDocuments
{
    __block NSUInteger result = 0;
    [self.documents enumerateObjectsUsingBlock:^(id obj, BOOL *stop) {
        if ([(ADXDocument *)obj versionIsLatestValue]) {
            result++;
        }
    }];
    return result;
}

- (ADXAccount *)account
{
    if (IsEmpty(self.accountID))
        return nil;
    
    ADXAccount *result = [ADXAccount entityWithID:self.accountID context:self.managedObjectContext];
    return result;
}

- (NSData *)thumbnail
{
    if (IsEmpty(self.documents)) {
        return nil;
    }
    
    ADXDocument *document = [self.documents anyObject];
    NSData *thumbnailData = document.thumbnail.data;

    return thumbnailData;
}

- (void)saveWithOptionalSync:(BOOL)queueForSync completion:(ADXEntityCRUDProtocolCompletionBlock)completionBlock
{
    __weak ADXCollection *weakSelf = self;
    NSManagedObjectContext *moc = self.managedObjectContext;
    
    [moc performBlock:^{
        NSError *saveError = nil;
        BOOL success = [moc save:&saveError];
        if (!success) {
            LogNSError(@"couldn't save collection", saveError);
        }
        if (completionBlock) {
            dispatch_async(dispatch_get_main_queue(), ^{
                completionBlock(success, (success ? nil : saveError));
            });
        }
        
        if (queueForSync) {
            [weakSelf enqueueForSyncInContext:moc completion:^(BOOL success, NSError *error) {
                if (!success) {
                    LogNSError(@"couldn't enqueue collection for sync", error);
                }
            }];
        }
    }];
}

- (void)enqueueForSyncInContext:(NSManagedObjectContext *)moc completion:(ADXEntityCRUDProtocolCompletionBlock)completionBlock
{
    ADXSyncQueueEntry *syncEntry = nil;
    
    NSMutableDictionary *payload = [self mutableJSONDictionary];
    
    if (!self.isDeletedStatus) {
        if (IsEmpty(self.id)) {
            syncEntry = [ADXSyncQueueEntry insertEntryForEntity:self id:self.localID accountID:self.accountID operation:ADXSyncQueueEntryOperation.insert payload:payload userInfo:nil context:moc];
        } else {
            syncEntry = [ADXSyncQueueEntry insertEntryForEntity:self id:self.id accountID:self.accountID operation:ADXSyncQueueEntryOperation.update payload:payload userInfo:nil context:moc];
        }
    } else {
        syncEntry = [ADXSyncQueueEntry insertEntryForEntity:self id:self.id accountID:self.accountID operation:ADXSyncQueueEntryOperation.remove payload:payload userInfo:nil context:moc];
    }
    
    [syncEntry save:^(BOOL success, NSError *error) {
        if (!success) {
            LogNSError(@"Unable to enqueue a synchronization request after saving collection.", error);
            if (completionBlock) {
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                    completionBlock(success, error);
                });
            }
            return;
        }
    }];
}

- (BOOL)isAccessible
{
    return [self.status isEqualToString:ADXDocumentNoteStatus.accessible];
}

- (BOOL)isDeletedStatus
{
    return [self.status isEqualToString:ADXDocumentNoteStatus.deleted];
}

- (void)markDeleted
{
    self.status = ADXCollectionStatus.deleted;
}

- (NSDictionary *)jsonDictionary
{
    return [NSDictionary dictionaryWithDictionary:[self mutableJSONDictionary]];
}

- (NSMutableDictionary *)mutableJSONDictionary
{
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] init];
    
    if (!IsEmpty(self.id)) {
        [dictionary setObject:self.id forKey:ADXJSONCollectionAttributes.id];
    }
    
    if (!IsEmpty(self.localID)) {
        [dictionary setObject:self.localID forKey:ADXJSONCollectionAttributes.localID];
    }
    
    if (!IsEmpty(self.status)) {
        [dictionary setObject:self.status forKey:ADXJSONCollectionAttributes.status];
    }
    
    if (!IsEmpty(self.title)) {
        [dictionary setObject:self.title forKey:ADXJSONCollectionAttributes.title];
    }
    
    return dictionary;
}

@end
