
#import "ADXAccount.h"
#import "ADXCategories.h"
#import "ADXCommonMacros.h"
#import "ADXUserDefaults.h"
#import "SSKeychain.h"

#define SSKEYCHAIN_SERVICE @"SavvyDox"

const struct ADXJSONAccountAttributes ADXJSONAccountAttributes = {
    .id = @"id",
    .accessToken = @"accessToken",
    .loginName = @"loginName",
};

@implementation ADXAccount

+ (NSArray *)allAccountsInContext:(NSManagedObjectContext *)moc
{
    NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:ADXAccountAttributes.mostRecentLogin ascending:NO];
    NSArray *results = [moc aps_fetchAllForEntityName:[ADXAccount entityName] objectIDsOnly:NO sortDescriptors:@[sortDescriptor]];
    return results;
}

+ (ADXAccount *)mostRecentlyUsedAccountInContext:(NSManagedObjectContext *)moc
{
    ADXAccount *result = nil;
    NSArray *accounts = [self allAccountsInContext:moc];

    if ([accounts count]) {
        result = accounts[0];
    }
    
    return result;
}

+ (ADXAccount *)accountWithLoginName:(NSString *)loginName context:(NSManagedObjectContext *)moc
{
    ADXAccount *result = [moc aps_fetchFirstObjectForEntityName:[ADXAccount entityName] withPredicate:@"%K == %@", ADXAccountAttributes.loginName, loginName];
    return result;
}

+ (ADXAccount *)accountWithLoginName:(NSString *)loginName password:(NSString *)password serverURL:(NSString *)serverURL context:(NSManagedObjectContext *)moc
{
    NSAssert(password != nil, @"Nil password not allowed");
    
    ADXAccount *result = [moc aps_fetchFirstObjectForEntityName:[ADXAccount entityName]
                                                  withPredicate:@"(%K == %@) AND (%K == %@)", ADXAccountAttributes.loginName, loginName, ADXAccountAttributes.serverURL, serverURL];
    
    if (result != nil) {
        // Check the password
        NSString *storedPassword = [SSKeychain passwordForService:SSKEYCHAIN_SERVICE account:result.id];
        if (![storedPassword isEqualToString:password]) {
            result = nil;
        }
    }
    return result;
}

+ (NSString *)generateLocalIDForAccountID:(NSString *)accountID
{
    NSString *result = [NSString stringWithFormat:@"%@-%@", accountID, [NSString adx_generateUUID]];
    return result;
}

#pragma mark - Keychain password storage and retrieval

- (NSString *)password
{
    NSAssert(self.id != nil, @"Can't look up password without an account ID");
    return [SSKeychain passwordForService:SSKEYCHAIN_SERVICE account:self.id];
}

- (void)setPassword:(NSString *)password
{
    if (password == nil) {
        [SSKeychain deletePasswordForService:SSKEYCHAIN_SERVICE account:self.id];
    } else {
        [SSKeychain setPassword:password forService:SSKEYCHAIN_SERVICE account:self.id];
    }
}

#pragma mark - ADXEntityCRUDProtocol

+ (id)entityWithID:(NSString *)entityID context:(NSManagedObjectContext *)moc
{
    ADXAccount *result = [moc aps_fetchFirstObjectForEntityName:[ADXAccount entityName] withPredicate:@"%K == %@", ADXAccountAttributes.id, entityID];
    return result;
}

+ (BOOL)updateEntity:(id)entity fromDictionary:(NSDictionary *)dictionary context:(NSManagedObjectContext *)moc
{
    if (!entity || IsEmpty(dictionary) || !moc) {
        LogError(@"Received bad params");
        return NO;
    }
    
    ADXAccount *account = entity;
    if (IsEmpty(account.id)) {
        NSString *accountID = [dictionary aps_objectForKeyOrNil:ADXJSONAccountAttributes.id];
        if (IsEmpty(accountID)) {
            LogError(@"No account id was supplied in dictionary");
            return NO;
        }
        account.id = accountID;
    }
    
    account.loginName = [dictionary aps_objectForKeyOrNil:ADXJSONAccountAttributes.loginName];

    LogTrace(@"Updated account: %@", account);

    return YES;
}

+ (id)addEntityFromDictionary:(NSDictionary *)dictionary context:(NSManagedObjectContext *)moc
{
    if (IsEmpty(dictionary)) {
        LogError(@"Received a nil dictionary.");
        return nil;
    }
    
    NSString *accountID = [dictionary objectForKey:ADXAccountAttributes.id];
    if (IsEmpty(accountID)) {
        LogError(@"Received a dictionary with no account id");
        return nil;
    }
    
    ADXAccount *account = [ADXAccount entityWithID:accountID context:moc];
    if (account) {
        LogError(@"Account with id %@ already exists.", accountID);
        return nil;
    }
    
    ADXAccount *newAccount = [ADXAccount insertInManagedObjectContext:moc];

    LogTrace(@"Inserted account with id %@", [dictionary aps_objectForKeyOrNil:ADXJSONAccountAttributes.id]);

    BOOL success = [ADXAccount updateEntity:newAccount fromDictionary:dictionary context:moc];
    if (success) {
        return newAccount;
    } else {
        return nil;
    }
}

@end
