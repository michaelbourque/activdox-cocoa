//
//  NSData+ActivDox.h
//  ActivDoxCore
//
//  Created by Gavin McKenzie on 12-06-07.
//  Copyright (c) 2012 ActivDox Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSData (ActivDox)
- (NSString *)adx_hexString;
- (BOOL)adx_containsData:(NSData *)data;
@end
