//
//  ADXPDFRequestOperation.h
//  ActivDoxCore
//
//  Created by Gavin McKenzie on 12-06-07.
//  Copyright (c) 2012 ActivDox Inc. All rights reserved.
//

#import "AFHTTPRequestOperation.h"

@interface ADXPDFRequestOperation : AFHTTPRequestOperation
@property (readonly, nonatomic, retain) id responsePDF;
+ (ADXPDFRequestOperation *)PDFRequestOperationWithRequest:(NSURLRequest *)urlRequest
                                                   success:(void (^)(NSURLRequest *request, NSHTTPURLResponse *response, id pdf))success 
                                                   failure:(void (^)(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id pdf))failure;
@end
