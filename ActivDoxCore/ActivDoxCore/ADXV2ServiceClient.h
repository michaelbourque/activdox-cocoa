//
//  ADXV2ServiceClient.h
//  ActivDoxCore
//
//  Created by Gavin McKenzie on 2012-12-03.
//  Copyright (c) 2012 ActivDox Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <SystemConfiguration/SystemConfiguration.h>

#import "AFHTTPClient.h"
#import "ADXServiceClientCommon.h"

@class ADXDocumentNote;
@class ADXCollection;

extern NSString * const kADXV2ServiceURLRequestFailedNotification;
extern NSString * const kADXV2ServiceURLRequestSucceededNotification;

extern NSString * const kADXV2ServiceAPIVersion;
extern NSString * const kADXV2ServiceReceivedDocumentsLiteral;
extern NSString * const kADXV2ServicePublishedDocumentsLiteral;
extern NSString * const kADXV2ServiceDocumentContentLiteral;
extern NSString * const kADXV2ServiceUserMeLiteral;
extern NSString * const kADXV2ServiceProcessDownloadQueueNotification;

extern NSString * const kADXV2ServiceJSONKeyID;

typedef void(^ADXV2ServiceRequestCompletionBlock)(NSInteger statusCode, id response, NSError *error);
typedef void(^ADXV2ServiceDownloadProgressBlock)(NSUInteger bytesRead, long long totalBytesRead, long long totalBytesExpectedToRead);

@interface ADXV2ServiceClient : NSObject
@property (readonly, nonatomic) NSURL *baseURL;
@property (assign, readonly, nonatomic) BOOL lastServiceURLRequestSucceeded;
@property (strong, readonly, nonatomic) NSDictionary *loginResponse;
@property (readonly, nonatomic) NSString *userID;
@property (readonly, nonatomic) NSString *version;
@property (readonly) BOOL downloadClientHasSpaceAvailable;

- (id)initWithBaseURL:(NSURL *)baseURL userID:(NSString *)userID;

- (AFNetworkReachabilityStatus)networkReachabilityStatus;
- (void)setReachabilityStatusChangeBlock:(void (^)(AFNetworkReachabilityStatus status))block;

- (void)updateDeviceToken:(NSData *)newToken APNSDeviceID:(NSString *)APNSDeviceID completion:(ADXV2ServiceRequestCompletionBlock)completionBlock;

- (void)getAccessTokenForLoginName:(NSString *)loginName password:(NSString *)password completion:(ADXV2ServiceRequestCompletionBlock)completionBlock;
- (void)registerUserWithLoginName:(NSString *)loginName email:(NSString *)emailAddress password:(NSString *)password completion:(ADXV2ServiceRequestCompletionBlock)completionBlock;
- (void)resetPasswordForEmail:(NSString *)email completion:(ADXV2ServiceRequestCompletionBlock)completionBlock;

- (void)getUsers:(ADXV2ServiceRequestCompletionBlock)completionBlock;
- (void)getUser:(NSString *)userID completion:(ADXV2ServiceRequestCompletionBlock)completionBlock;

- (void)getCollectionsSince:(NSDate *)since completion:(ADXV2ServiceRequestCompletionBlock)completionBlock;
- (void)getCollection:(NSString *)collectionID since:(NSDate *)since completion:(ADXV2ServiceRequestCompletionBlock)completionBlock;
- (void)getCollectionDocuments:(NSString *)collectionID since:(NSDate *)since completion:(ADXV2ServiceRequestCompletionBlock)completionBlock;
- (void)postCollection:(ADXCollection *)collection payload:(NSDictionary *)optionalPayload completion:(ADXV2ServiceRequestCompletionBlock)completionBlock;
- (void)deleteCollection:(NSString *)collectionID completion:(ADXV2ServiceRequestCompletionBlock)completionBlock;
- (void)linkDocument:(NSString *)documentID withCollection:(NSString *)collectionID completion:(ADXV2ServiceRequestCompletionBlock)completionBlock;
- (void)unlinkDocument:(NSString *)documentID withCollection:(NSString *)collectionID completion:(ADXV2ServiceRequestCompletionBlock)completionBlock;

- (AFHTTPRequestOperation *)getContentOperationForDocumentID:(NSString *)documentID
                                                     version:(NSNumber *)versionID
                                          backgroundDownload:(BOOL)backgroundDownload
                                                    progress:(ADXV2ServiceDownloadProgressBlock)progressBlock
                                                  completion:(ADXV2ServiceRequestCompletionBlock)completionBlock;
- (void)getDocumentContentViaOperation:(AFHTTPRequestOperation *)operation;

- (void)getDocumentsSince:(NSDate *)since completion:(ADXV2ServiceRequestCompletionBlock)completionBlock;
- (void)getDocument:(NSString *)documentID completion:(ADXV2ServiceRequestCompletionBlock)completionBlock;
- (void)getDocumentVersions:(NSString *)documentID completion:(ADXV2ServiceRequestCompletionBlock)completionBlock;
- (void)getDocument:(NSString *)documentID version:(NSNumber *)versionID completion:(ADXV2ServiceRequestCompletionBlock)completionBlock;
- (void)getDocumentContent:(NSString *)documentID version:(NSNumber *)versionID completion:(ADXV2ServiceRequestCompletionBlock)completionBlock;
- (void)getDocumentRecipients:(NSString *)documentID version:(NSNumber *)versionID completion:(ADXV2ServiceRequestCompletionBlock)completionBlock;
- (void)getDocumentThumbnail:(NSString *)documentID version:(NSNumber *)versionID completion:(ADXV2ServiceRequestCompletionBlock)completionBlock;
- (void)getDocumentNotes:(NSString *)documentID version:(NSNumber *)versionID since:(NSDate *)since completion:(ADXV2ServiceRequestCompletionBlock)completionBlock;
- (void)getDocumentChanges:(NSString *)documentID version:(NSNumber *)versionID completion:(ADXV2ServiceRequestCompletionBlock)completionBlock;
- (void)getDocumentChanges:(NSString *)documentID version:(NSNumber *)versionID compare:(NSNumber *)compareVersionID completion:(ADXV2ServiceRequestCompletionBlock)completionBlock;
- (void)deleteDocument:(NSString *)documentID completion:(ADXV2ServiceRequestCompletionBlock)completionBlock;

- (void)getServerInfo:(NSString *)details completion:(ADXV2ServiceRequestCompletionBlock)completionBlock;
- (void)cancelAllDownloads;

- (void)uploadDocumentAtURL:(NSURL *)documentURL
                      title:(NSString *)title
                 recipients:(NSArray *)recipients
                 completion:(ADXV2ServiceRequestCompletionBlock)completionBlock;
- (void)shareDocumentWithID:(NSString *)documentID withEmailAddresses:(NSArray *)addresses message:(NSString *)message completion:(ADXV2ServiceRequestCompletionBlock)completionBlock;

- (void)getNotesSince:(NSDate *)since completion:(ADXV2ServiceRequestCompletionBlock)completionBlock;
- (void)getNote:(NSString *)noteID completion:(ADXV2ServiceRequestCompletionBlock)completionBlock;
- (void)getNoteRecipients:(NSString *)noteID completion:(ADXV2ServiceRequestCompletionBlock)completionBlock;
- (void)postNotes:(NSArray *)notes completion:(ADXV2ServiceRequestCompletionBlock)completionBlock;
- (void)postNote:(ADXDocumentNote *)note payload:(NSDictionary *)optionalPayload completion:(ADXV2ServiceRequestCompletionBlock)completionBlock;
- (void)deleteNote:(NSString *)noteID completion:(ADXV2ServiceRequestCompletionBlock)completionBlock;
- (void)updateNote:(ADXDocumentNote *)note payload:(NSDictionary *)optionalPayload completion:(ADXV2ServiceRequestCompletionBlock)completionBlock;

- (void)getEventsSince:(NSString *)sinceEventID completion:(ADXV2ServiceRequestCompletionBlock)completionBlock;
- (void)getEvent:(NSString *)eventID completion:(ADXV2ServiceRequestCompletionBlock)completionBlock;
- (void)postEvents:(NSArray *)events completion:(ADXV2ServiceRequestCompletionBlock)completionBlock;

- (void)downloadImageAtPath:(NSString *)path completion:(ADXV2ServiceRequestCompletionBlock)completionBlock;

@end
