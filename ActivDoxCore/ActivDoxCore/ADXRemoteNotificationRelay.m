//
//  ADXRemoteNotification.m
//  ActivDoxCore
//
//  Created by David Nikkel on 12-06-04.
//  Copyright (c) 2012 ActivDox Inc. All rights reserved.
//

#import "ADXRemoteNotificationRelay.h"
#import "ADXUserDefaults.h"

NSString * const kADXRemoteNotificationEvent = @"kADXRemoteNotificationEvent";
NSString * const kADXUserActivatedNotificationEvent = @"kADXUserActivatedNotificationEvent";
NSString * const kADXNotificationPayloadEventIDKey = @"eid";
NSString * const kADXNotificationPayloadAccountIDKey = @"uid";
NSString * const kADXNotificationPayloadURLKey = @"url";
NSString * const kADXNotificationPayloadAPSKey = @"aps";
NSString * const kADXNotificationPayloadAPSAlertMessageKeyPath = @"aps.alert";

@implementation ADXRemoteNotificationRelay

- (void)processNotificationReceivedWhileNotRunning: (NSDictionary *) dictionary
{
    NSLog(@"Launched from push notification: %@", dictionary);

    //Forward notification to Notification Center
    [[NSNotificationCenter defaultCenter] postNotificationName:kADXRemoteNotificationEvent object:dictionary];
}

- (void)processNotificationReceivedWhileRunning: (NSDictionary *) dictionary
{
	NSLog(@"Received notification: %@", dictionary);
    
    //Forward notification to Notification Center
    [[NSNotificationCenter defaultCenter] postNotificationName:kADXRemoteNotificationEvent object:dictionary];
    
    //if ( application.applicationState == UIApplicationStateActive )
    //      app was already in the foreground
    //else
    //      app was just brought from background to foreground
    
}

- (void)userDidActivateNotification:(NSDictionary *)dictionary
{
	NSLog(@"User activated notification: %@", dictionary);
    
    //Forward notification to Notification Center
    [[NSNotificationCenter defaultCenter] postNotificationName:kADXUserActivatedNotificationEvent object:dictionary];
}

- (void)failedToRegister: (NSError *)err
{
    // TODO - What do we need to do here, if anything?
    NSLog(@"Error in registration. Error: %@", err);
}


@end
