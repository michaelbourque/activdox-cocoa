//
//  ADXHighlightedContentHelper.h
//  ActivDoxCore
//
//  Created by Matteo Ugolini on 2012-12-06.
//  Copyright (c) 2012 ActivDox Inc. All rights reserved.
//

#import <CoreFoundation/CoreFoundation.h>
#import <CoreGraphics/CoreGraphics.h>

extern const struct ADXJSONHighlightedContentAttributes {
    __unsafe_unretained NSString *changesArray;
    struct {
        __unsafe_unretained NSString *changeType;
        __unsafe_unretained NSString *originalContentArray;
        struct {
            __unsafe_unretained NSString *content;
            __unsafe_unretained NSString *contentURI;
            __unsafe_unretained NSString *contentType;
        } originalContent;
        
        __unsafe_unretained NSString *originalLocationArray;
        struct {
            __unsafe_unretained NSString *page;
            __unsafe_unretained NSString *highlightArray;
            struct {
                __unsafe_unretained NSString *left;
                __unsafe_unretained NSString *top;
                __unsafe_unretained NSString *right;
                __unsafe_unretained NSString *bottom;
            } highlight;
        } originalLocation;
        
        __unsafe_unretained NSString *revisedContentArray;
        struct {
            __unsafe_unretained NSString *content;
            __unsafe_unretained NSString *contentType;
        } revisedContent;
        
        __unsafe_unretained NSString *revisedLocationArray;
        struct {
            __unsafe_unretained NSString *page;
            __unsafe_unretained NSString *highlightArray;
            struct {
                __unsafe_unretained NSString *left;
                __unsafe_unretained NSString *top;
                __unsafe_unretained NSString *right;
                __unsafe_unretained NSString *bottom;
            } highlight;
        } revisedLocation;
    } changes;
} ADXJSONHighlightedContentAttributes;

extern NSString *kADXChangeTypeContentInserted;
extern NSString *kADXChangeTypeContentDeleted;
extern NSString *kADXChangeTypeContentUpdated;

@interface ADXHighlightedContentHelper : NSObject
@property (nonatomic, strong) NSData *data;

// Custom logic goes here.

typedef void (^ADXHighlightContentDrawRectBlock)(CGColorRef color, CGRect pageRect);
typedef void (^ADXHighlightedContentEnumerationBlock)(CGRect pageRect, NSString *type, id change);
typedef void (^ADXHighlightedContentOnPageEnumerationBlock)(NSUInteger pageIndex, NSArray *pageRects, NSString *type, id change);

typedef CGRect (^ADXHightedContentConvertPageRect)(CGRect pageRect);

+ (ADXHighlightedContentHelper*) highlightedContentWithData:(NSData*)data;
- (id) initWithData:(NSData*)data;

- (void)drawChangesOnPage:(NSUInteger)pageIndex context:(CGContextRef)context withBlock:(ADXHightedContentConvertPageRect)mapPageRectBlock;

- (void)drawChange:(id)change onPage:(NSUInteger)pageIndex context:(CGContextRef)context withBlock:(ADXHightedContentConvertPageRect)mapPageRectBlock;


- (void)enumerateChangesOnPage:(NSUInteger)pageIndex withBlock:(ADXHighlightedContentEnumerationBlock)callback;

- (void)enumerateChangesWithBlock:(ADXHighlightedContentOnPageEnumerationBlock)callback;

- (NSUInteger)numberOfChangesOnPage:(NSUInteger)pageIndex;

// Provide a description for a change.  Pass in the change that was passed to the callback.
- (NSString *)descriptionForChange:(id)change;
- (NSString *)changedTextForChange:(id)change;
- (NSString *)changeTypeDescriptionForChange:(id)change;

- (NSArray *)revisedPagesForChange:(id)change;

- (NSString *)originalImagePathForChange:(id)change;

+ (NSString*) changeTypeForChange:(id)change;

@property (readonly, nonatomic) BOOL hasChanges;

@end
