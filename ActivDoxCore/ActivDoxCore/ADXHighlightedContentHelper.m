//
//  ADXHighlightedContentHelper.m
//  ActivDoxCore
//
//  Created by Matteo Ugolini on 2012-12-06.
//  Copyright (c) 2012 ActivDox Inc. All rights reserved.
//

#import "ADXHighlightedContentHelper.h"
#import "UIColor+SavvyDox.h"
#import "Logging.h"

// Hauling in UIKit for [NSValue valueWithRect]
#if TARGET_OS_IPHONE
#import <UIKit/UIKit.h>
#else
#import <AppKit/AppKit.h>
#endif


@implementation ADXHighlightedContentHelper
@synthesize data = _data;

const struct ADXJSONHighlightedContentAttributes ADXJSONHighlightedContentAttributes = {
    .changesArray = @"changes",
    .changes = {
        .changeType = @"changeType",
        .originalContentArray = @"originalContent",
        .originalContent = {
            .content = @"content",
            .contentURI = @"contentURI",
            .contentType = @"contentType",
        },
        
        .originalLocationArray = @"originalLocation",
        .originalLocation = {
            .page = @"page",
            .highlightArray = @"highlights",
            .highlight = {
                .left = @"left",
                .top = @"top",
                .right = @"right",
                .bottom = @"bottom",
            },
        },
        
        .revisedContentArray = @"revisedContent",
        .revisedContent = {
            .content = @"content",
            .contentType = @"contentType",
        },
        
        .revisedLocationArray = @"revisedLocation",
        .revisedLocation = {
            .page = @"page",
            .highlightArray = @"highlights",
            .highlight = {
                .left = @"left",
                .top = @"top",
                .right = @"right",
                .bottom = @"bottom",
            },
        },
    },
};

+ (ADXHighlightedContentHelper*) highlightedContentWithData:(NSData *)data
{
    return [[ADXHighlightedContentHelper alloc]initWithData:data];
}

- (id) initWithData:(NSData *)data
{
    self = [super init];
    if (self) {
        _data = data;
    }
    return self;
}

NSString *kADXChangeTypeContentInserted = @"content.inserted";
NSString *kADXChangeTypeContentDeleted = @"content.deleted";
NSString *kADXChangeTypeContentUpdated = @"content.updated";

- (void)drawChange:(id)change rect:(CGRect)changeRect onPage:(NSUInteger)pageIndex context:(CGContextRef)context block:(ADXHightedContentConvertPageRect)mapPageRectBlock
{
#if TARGET_OS_IPHONE
    UIColor *updatedUIColor = [UIColor adxUpdatedContentColor];
    CGColorRef updatedColor = updatedUIColor.CGColor;
    UIColor *insertedUIColor = [UIColor adxInsertedContentColor];
    CGColorRef insertedColor = insertedUIColor.CGColor;
#else
    static CGFloat updatedColorArray[] = { 255/255.0, 192/255.0, 0/255.0, 0.35 };
    static CGFloat insertedColorArray[] = { 0/255.0, 255/255.0, 0/255.0, 0.35 };

    CGColorSpaceRef colorSpaceRGB = CGColorSpaceCreateDeviceRGB();
    CGColorRef updatedColor = CGColorCreate(colorSpaceRGB, updatedColorArray);
    CGColorRef insertedColor = CGColorCreate(colorSpaceRGB, insertedColorArray);
#endif
    
    NSString *changeType = [change valueForKey:ADXJSONHighlightedContentAttributes.changes.changeType];
    CGRect rect = mapPageRectBlock(changeRect);
    
    if ([changeType isEqualToString:kADXChangeTypeContentInserted]) {
        CGContextSetFillColorWithColor(context, insertedColor);
        CGContextFillRect(context, rect);
    }
    
    if ([changeType isEqualToString:kADXChangeTypeContentUpdated]) {
        CGContextSetFillColorWithColor(context, updatedColor);
        CGContextFillRect(context, rect);
    }
    
    if ([changeType isEqualToString:kADXChangeTypeContentDeleted]) {
#if TARGET_OS_IPHONE
        UIImage *deletionImage = [UIImage imageNamed:@"deletionicon"];
        CGPoint center = CGPointMake(rect.origin.x + (rect.size.width/2), rect.origin.y + (rect.size.height/2));
        [deletionImage drawAtPoint:CGPointMake(center.x - (deletionImage.size.width/2), center.y - deletionImage.size.height)];
#else
        NSImage *deletionImage = [NSImage imageNamed:@"deletionicon"];
        CGPoint center = CGPointMake(rect.origin.x + (rect.size.width/2), rect.origin.y + (rect.size.height/2));
        [deletionImage drawAtPoint:CGPointMake(center.x - (deletionImage.size.width/2), center.y - deletionImage.size.height)
                          fromRect:NSZeroRect
                         operation:NSCompositeCopy
                          fraction:1.0];
#endif
    }
    
#if !TARGET_OS_IPHONE
    CGColorRelease(updatedColor);
    CGColorRelease(insertedColor);
    CGColorSpaceRelease(colorSpaceRGB);
#endif
    
}

- (void)drawChange:(id)change onPage:(NSUInteger)pageIndex context:(CGContextRef)context withBlock:(ADXHightedContentConvertPageRect)mapPageRectBlock
{
    [self enumerateChangesOnPage:pageIndex withBlock:^(CGRect pageRect, NSString *changeType, id data) {
        if ([data isEqual:change]) {
            [self drawChange:data rect:pageRect onPage:pageIndex context:context block:mapPageRectBlock];
        }
    }];
}

- (void)drawChangesOnPage:(NSUInteger)pageIndex context:(CGContextRef)context withBlock:(ADXHightedContentConvertPageRect)mapPageRectBlock
{
    [self enumerateChangesOnPage:pageIndex withBlock:^(CGRect pageRect, NSString *changeType, id data) {
        [self drawChange:data rect:pageRect onPage:pageIndex context:context block:mapPageRectBlock];
    }];
}

- (void)enumerateChangesWithBlock:(ADXHighlightedContentOnPageEnumerationBlock)callback
{
    if (self.data == nil) {
        return;
    }
    
    // Parse the change JSON
    NSDictionary *changeDict = [NSJSONSerialization JSONObjectWithData:self.data options:0 error:nil];
    if (changeDict == nil) {
        LogError(@"Unable to parse JSON change data");
        return;
    }
    
    NSArray *changes = (NSArray *)[changeDict objectForKey:ADXJSONHighlightedContentAttributes.changesArray];
    for (NSDictionary *change in changes) {
        NSString *changeType = (NSString *)[change objectForKey:ADXJSONHighlightedContentAttributes.changes.changeType];
        NSArray *revisedPages = [change objectForKey:ADXJSONHighlightedContentAttributes.changes.revisedLocationArray];
        for (NSDictionary *changeOnPage in revisedPages) {
            NSArray *highlights = [changeOnPage valueForKey:ADXJSONHighlightedContentAttributes.changes.revisedLocation.highlightArray];
            NSNumber *pageIndex = (NSNumber *)[changeOnPage objectForKey:ADXJSONHighlightedContentAttributes.changes.revisedLocation.page];
            if (pageIndex == nil) {
                continue;
            }
            
            NSMutableArray *rects = [NSMutableArray array];
            
            for (NSDictionary *highlightRectDict in highlights) {
                CGRect highlightRectOnPage;
                highlightRectOnPage.origin.x = ((NSString *)[highlightRectDict valueForKey:ADXJSONHighlightedContentAttributes.changes.revisedLocation.highlight.left]).floatValue;
                highlightRectOnPage.origin.y = ((NSString *)[highlightRectDict valueForKey:ADXJSONHighlightedContentAttributes.changes.revisedLocation.highlight.bottom]).floatValue;
                highlightRectOnPage.size.width = ((NSString *)[highlightRectDict valueForKey:ADXJSONHighlightedContentAttributes.changes.revisedLocation.highlight.right]).floatValue - highlightRectOnPage.origin.x;
                highlightRectOnPage.size.height = ((NSString *)[highlightRectDict valueForKey:ADXJSONHighlightedContentAttributes.changes.revisedLocation.highlight.top]).floatValue - highlightRectOnPage.origin.y;
                
                highlightRectOnPage = CGRectInset(highlightRectOnPage, -4, 0);
#if TARGET_OS_IPHONE
                [rects addObject:[NSValue valueWithCGRect:highlightRectOnPage]];
#else
                [rects addObject:[NSValue valueWithRect:highlightRectOnPage]];
#endif
            }
            
            callback(pageIndex.unsignedIntValue - 1, rects, changeType, change);
        }
    }
}

- (void)enumerateChangesOnPage:(NSUInteger)pageIndex withBlock:(ADXHighlightedContentEnumerationBlock)callback
{
    if (self.data == nil) {
        return;
    }
    
    // Parse the change JSON
    NSDictionary *changeDict = [NSJSONSerialization JSONObjectWithData:self.data options:0 error:nil];
    if (changeDict == nil) {
        LogError(@"Unable to parse JSON change data");
        return;
    }
    
    NSArray *changes = (NSArray *)[changeDict objectForKey:ADXJSONHighlightedContentAttributes.changesArray];
    for (NSDictionary *change in changes) {
        NSString *changeType = (NSString *)[change objectForKey:ADXJSONHighlightedContentAttributes.changes.changeType];
        NSArray *revisedPages = [change objectForKey:ADXJSONHighlightedContentAttributes.changes.revisedLocationArray];
        for (NSDictionary *changeOnPage in revisedPages) {
            NSString *pageNumString = [changeOnPage valueForKey:ADXJSONHighlightedContentAttributes.changes.revisedLocation.page];
            if (pageNumString != nil && [pageNumString intValue] == (pageIndex+1)) {
                NSArray *highlights = [changeOnPage valueForKey:ADXJSONHighlightedContentAttributes.changes.revisedLocation.highlightArray];
                for (NSDictionary *highlightRectDict in highlights) {
                    CGRect highlightRectOnPage;
                    highlightRectOnPage.origin.x = ((NSString *)[highlightRectDict valueForKey:ADXJSONHighlightedContentAttributes.changes.revisedLocation.highlight.left]).floatValue;
                    highlightRectOnPage.origin.y = ((NSString *)[highlightRectDict valueForKey:ADXJSONHighlightedContentAttributes.changes.revisedLocation.highlight.bottom]).floatValue;
                    highlightRectOnPage.size.width = ((NSString *)[highlightRectDict valueForKey:ADXJSONHighlightedContentAttributes.changes.revisedLocation.highlight.right]).floatValue - highlightRectOnPage.origin.x;
                    highlightRectOnPage.size.height = ((NSString *)[highlightRectDict valueForKey:ADXJSONHighlightedContentAttributes.changes.revisedLocation.highlight.top]).floatValue - highlightRectOnPage.origin.y;
                    
                    highlightRectOnPage = CGRectInset(highlightRectOnPage, -2, 0);
                    
                    callback(highlightRectOnPage, changeType, change);
                }
            }
        }
    }
}

- (NSUInteger)numberOfChangesOnPage:(NSUInteger)pageIndex
{
    // Parse the change JSON
    NSUInteger changesCount = 0;
    NSDictionary *changeDict = [NSJSONSerialization JSONObjectWithData:self.data options:0 error:nil];
    if (changeDict == nil) {
        LogError(@"Unable to parse JSON change data");
    } else {
        NSArray *changes = (NSArray *)[changeDict objectForKey:ADXJSONHighlightedContentAttributes.changesArray];
        for (NSDictionary *change in changes) {
            NSArray *revisedPages = [change objectForKey:ADXJSONHighlightedContentAttributes.changes.revisedLocationArray];
            for (NSDictionary *changeOnPage in revisedPages) {
                NSString *pageNumString = [changeOnPage valueForKey:ADXJSONHighlightedContentAttributes.changes.revisedLocation.page];
                if (pageNumString != nil && [pageNumString intValue] == (pageIndex+1)) {
                    changesCount ++;
                }
            }
        }
    }
    return changesCount;
}


- (NSString *)descriptionForChange:(id)change
{
    NSString *changeType = (NSString *)[change objectForKey:ADXJSONHighlightedContentAttributes.changes.changeType];
    if ([changeType isEqualToString:kADXChangeTypeContentInserted]) {
        return nil;
    } else
    {
        return [self changedTextForChange:change];
    }
    return nil;
}

- (NSString *)changedTextForChange:(id)change
{
    NSString *changedText = nil;
    
    NSString *changeType = (NSString *)[change objectForKey:@"changeType"];
    if ([changeType isEqualToString:kADXChangeTypeContentDeleted] || [changeType isEqualToString:kADXChangeTypeContentUpdated]) {
        
        NSArray *originalContentArray = [change valueForKey:ADXJSONHighlightedContentAttributes.changes.originalContentArray];
        if (originalContentArray.count > 0) {
            NSDictionary *originalContent = [originalContentArray objectAtIndex:0];
            changedText = [originalContent valueForKey:ADXJSONHighlightedContentAttributes.changes.originalContent.content];
        }
    } else
        if ([changeType isEqualToString:kADXChangeTypeContentInserted]) {
            NSArray *revisedContentArray = [change valueForKey:ADXJSONHighlightedContentAttributes.changes.revisedContentArray];
            if (revisedContentArray.count > 0) {
                NSDictionary *revisedContent = [revisedContentArray objectAtIndex:0];
                changedText = [revisedContent valueForKey:ADXJSONHighlightedContentAttributes.changes.revisedContent.content];
            }
        }
    
    return changedText;
}

- (NSString *)changeTypeDescriptionForChange:(id)change
{
    NSString *changeDescription = nil;
    NSString *changeType = (NSString *)[change objectForKey:ADXJSONHighlightedContentAttributes.changes.changeType];
    
    if ([changeType isEqualToString:kADXChangeTypeContentInserted]) {
        changeDescription = NSLocalizedString(@"Insertion", @"Change type title");;
    }
    else if ([changeType isEqualToString:kADXChangeTypeContentDeleted]){
        changeDescription = NSLocalizedString(@"Deleted", @"Change type title");
    }
    else if ([changeType isEqualToString:kADXChangeTypeContentUpdated]) {
        changeDescription = NSLocalizedString(@"Replaced", @"Change type title");
    }
    
    return changeDescription;
}

- (NSArray *)revisedPagesForChange:(id)change
{
    NSMutableArray* pages = [NSMutableArray array];
    
    NSArray *revisedLocations = [change objectForKey:ADXJSONHighlightedContentAttributes.changes.revisedLocationArray];
    for (NSDictionary *revisedLocation in revisedLocations) {
        NSString *pageNumString = [revisedLocation valueForKey:ADXJSONHighlightedContentAttributes.changes.revisedLocation.page];
        if(pageNumString){
            [pages addObject:pageNumString];
        }
    }
    
    return pages;
}

- (NSString *)originalImagePathForChange:(id)change
{
    NSArray *originalLocation = [change objectForKey:ADXJSONHighlightedContentAttributes.changes.originalContentArray];
    if (originalLocation.count > 0) {
        NSDictionary *dict = (NSDictionary *)originalLocation[0];
        NSString *path = [dict valueForKey:ADXJSONHighlightedContentAttributes.changes.originalContent.contentURI];
        if (path.length > 0) {
            return path;
        }
    }
    
    return nil;
}


+ (NSString*) changeTypeForChange:(id)change
{
    return (NSString *)[change objectForKey:ADXJSONHighlightedContentAttributes.changes.changeType];
}

- (BOOL)hasChanges
{
    NSDictionary *changeDict = [NSJSONSerialization JSONObjectWithData:self.data options:0 error:nil];
    if (changeDict == nil) {
        LogError(@"Unable to parse JSON change data");
        return NO;
    }
    
    return changeDict.count > 0;
}

@end
