// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to ADXDocumentNote.h instead.

#import <CoreData/CoreData.h>



extern const struct ADXDocumentNoteAttributes {
	__unsafe_unretained NSString *accountID;
	__unsafe_unretained NSString *closeVersion;
	__unsafe_unretained NSString *created;
	__unsafe_unretained NSString *generation;
	__unsafe_unretained NSString *id;
	__unsafe_unretained NSString *localID;
	__unsafe_unretained NSString *locationData;
	__unsafe_unretained NSString *openVersion;
	__unsafe_unretained NSString *safeToDeleteOnStartup;
	__unsafe_unretained NSString *status;
	__unsafe_unretained NSString *text;
	__unsafe_unretained NSString *updated;
	__unsafe_unretained NSString *visibility;
} ADXDocumentNoteAttributes;



extern const struct ADXDocumentNoteRelationships {
	__unsafe_unretained NSString *author;
	__unsafe_unretained NSString *metaDocument;
} ADXDocumentNoteRelationships;






@class ADXUser;
@class ADXMetaDocument;















@class NSObject;













@interface ADXDocumentNoteID : NSManagedObjectID {}
@end

@interface _ADXDocumentNote : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (ADXDocumentNoteID*)objectID;





@property (nonatomic, strong) NSString* accountID;



//- (BOOL)validateAccountID:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* closeVersion;




@property (atomic) int64_t closeVersionValue;
- (int64_t)closeVersionValue;
- (void)setCloseVersionValue:(int64_t)value_;


//- (BOOL)validateCloseVersion:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSDate* created;



//- (BOOL)validateCreated:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* generation;




@property (atomic) int64_t generationValue;
- (int64_t)generationValue;
- (void)setGenerationValue:(int64_t)value_;


//- (BOOL)validateGeneration:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* id;



//- (BOOL)validateId:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* localID;



//- (BOOL)validateLocalID:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) id locationData;



//- (BOOL)validateLocationData:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* openVersion;




@property (atomic) int64_t openVersionValue;
- (int64_t)openVersionValue;
- (void)setOpenVersionValue:(int64_t)value_;


//- (BOOL)validateOpenVersion:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* safeToDeleteOnStartup;




@property (atomic) BOOL safeToDeleteOnStartupValue;
- (BOOL)safeToDeleteOnStartupValue;
- (void)setSafeToDeleteOnStartupValue:(BOOL)value_;


//- (BOOL)validateSafeToDeleteOnStartup:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* status;



//- (BOOL)validateStatus:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* text;



//- (BOOL)validateText:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSDate* updated;



//- (BOOL)validateUpdated:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* visibility;



//- (BOOL)validateVisibility:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) ADXUser *author;

//- (BOOL)validateAuthor:(id*)value_ error:(NSError**)error_;




@property (nonatomic, strong) ADXMetaDocument *metaDocument;

//- (BOOL)validateMetaDocument:(id*)value_ error:(NSError**)error_;





@end



@interface _ADXDocumentNote (CoreDataGeneratedPrimitiveAccessors)


- (NSString*)primitiveAccountID;
- (void)setPrimitiveAccountID:(NSString*)value;




- (NSNumber*)primitiveCloseVersion;
- (void)setPrimitiveCloseVersion:(NSNumber*)value;

- (int64_t)primitiveCloseVersionValue;
- (void)setPrimitiveCloseVersionValue:(int64_t)value_;




- (NSDate*)primitiveCreated;
- (void)setPrimitiveCreated:(NSDate*)value;




- (NSNumber*)primitiveGeneration;
- (void)setPrimitiveGeneration:(NSNumber*)value;

- (int64_t)primitiveGenerationValue;
- (void)setPrimitiveGenerationValue:(int64_t)value_;




- (NSString*)primitiveId;
- (void)setPrimitiveId:(NSString*)value;




- (NSString*)primitiveLocalID;
- (void)setPrimitiveLocalID:(NSString*)value;




- (id)primitiveLocationData;
- (void)setPrimitiveLocationData:(id)value;




- (NSNumber*)primitiveOpenVersion;
- (void)setPrimitiveOpenVersion:(NSNumber*)value;

- (int64_t)primitiveOpenVersionValue;
- (void)setPrimitiveOpenVersionValue:(int64_t)value_;




- (NSNumber*)primitiveSafeToDeleteOnStartup;
- (void)setPrimitiveSafeToDeleteOnStartup:(NSNumber*)value;

- (BOOL)primitiveSafeToDeleteOnStartupValue;
- (void)setPrimitiveSafeToDeleteOnStartupValue:(BOOL)value_;




- (NSString*)primitiveStatus;
- (void)setPrimitiveStatus:(NSString*)value;




- (NSString*)primitiveText;
- (void)setPrimitiveText:(NSString*)value;




- (NSDate*)primitiveUpdated;
- (void)setPrimitiveUpdated:(NSDate*)value;




- (NSString*)primitiveVisibility;
- (void)setPrimitiveVisibility:(NSString*)value;





- (ADXUser*)primitiveAuthor;
- (void)setPrimitiveAuthor:(ADXUser*)value;



- (ADXMetaDocument*)primitiveMetaDocument;
- (void)setPrimitiveMetaDocument:(ADXMetaDocument*)value;


@end
