//
//  NSString+ActivDox.h
//  ActivDoxCore
//
//  Created by Gavin McKenzie on 12-05-07.
//  Copyright (c) 2012 ActivDox Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (ActivDox)
+ (NSString *)adx_generateUUID;
@end
