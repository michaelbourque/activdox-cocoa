// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to ADXCollection.h instead.

#import <CoreData/CoreData.h>



extern const struct ADXCollectionAttributes {
	__unsafe_unretained NSString *accountID;
	__unsafe_unretained NSString *created;
	__unsafe_unretained NSString *id;
	__unsafe_unretained NSString *localID;
	__unsafe_unretained NSString *matchingTags;
	__unsafe_unretained NSString *mostRecentRefresh;
	__unsafe_unretained NSString *safeToDeleteOnStartup;
	__unsafe_unretained NSString *status;
	__unsafe_unretained NSString *title;
	__unsafe_unretained NSString *type;
} ADXCollectionAttributes;



extern const struct ADXCollectionRelationships {
	__unsafe_unretained NSString *documents;
} ADXCollectionRelationships;





extern const struct ADXCollectionUserInfo {
	__unsafe_unretained NSString *typeGeneric;
	__unsafe_unretained NSString *typePublished;
	__unsafe_unretained NSString *typeReceived;
} ADXCollectionUserInfo;


@class ADXDocument;











@class NSObject;











@interface ADXCollectionID : NSManagedObjectID {}
@end

@interface _ADXCollection : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (ADXCollectionID*)objectID;





@property (nonatomic, strong) NSString* accountID;



//- (BOOL)validateAccountID:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSDate* created;



//- (BOOL)validateCreated:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* id;



//- (BOOL)validateId:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* localID;



//- (BOOL)validateLocalID:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) id matchingTags;



//- (BOOL)validateMatchingTags:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSDate* mostRecentRefresh;



//- (BOOL)validateMostRecentRefresh:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* safeToDeleteOnStartup;




@property (atomic) BOOL safeToDeleteOnStartupValue;
- (BOOL)safeToDeleteOnStartupValue;
- (void)setSafeToDeleteOnStartupValue:(BOOL)value_;


//- (BOOL)validateSafeToDeleteOnStartup:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* status;



//- (BOOL)validateStatus:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* title;



//- (BOOL)validateTitle:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* type;



//- (BOOL)validateType:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSSet *documents;

- (NSMutableSet*)documentsSet;





@end


@interface _ADXCollection (DocumentsCoreDataGeneratedAccessors)
- (void)addDocuments:(NSSet*)value_;
- (void)removeDocuments:(NSSet*)value_;
- (void)addDocumentsObject:(ADXDocument*)value_;
- (void)removeDocumentsObject:(ADXDocument*)value_;
@end


@interface _ADXCollection (CoreDataGeneratedPrimitiveAccessors)


- (NSString*)primitiveAccountID;
- (void)setPrimitiveAccountID:(NSString*)value;




- (NSDate*)primitiveCreated;
- (void)setPrimitiveCreated:(NSDate*)value;




- (NSString*)primitiveId;
- (void)setPrimitiveId:(NSString*)value;




- (NSString*)primitiveLocalID;
- (void)setPrimitiveLocalID:(NSString*)value;




- (id)primitiveMatchingTags;
- (void)setPrimitiveMatchingTags:(id)value;




- (NSDate*)primitiveMostRecentRefresh;
- (void)setPrimitiveMostRecentRefresh:(NSDate*)value;




- (NSNumber*)primitiveSafeToDeleteOnStartup;
- (void)setPrimitiveSafeToDeleteOnStartup:(NSNumber*)value;

- (BOOL)primitiveSafeToDeleteOnStartupValue;
- (void)setPrimitiveSafeToDeleteOnStartupValue:(BOOL)value_;




- (NSString*)primitiveStatus;
- (void)setPrimitiveStatus:(NSString*)value;




- (NSString*)primitiveTitle;
- (void)setPrimitiveTitle:(NSString*)value;





- (NSMutableSet*)primitiveDocuments;
- (void)setPrimitiveDocuments:(NSMutableSet*)value;


@end
