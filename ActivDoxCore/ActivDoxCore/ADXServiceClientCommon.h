//
//  ADXServiceClientConstants.h
//  ActivDoxCore
//
//  Created by Gavin McKenzie on 2012-12-03.
//  Copyright (c) 2012 ActivDox Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

BOOL IsHTTP2XXSuccess(NSInteger statusCode);
