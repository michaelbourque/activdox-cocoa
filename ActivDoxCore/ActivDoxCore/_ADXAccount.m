// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to ADXAccount.m instead.

#import "_ADXAccount.h"


const struct ADXAccountAttributes ADXAccountAttributes = {
	.accessToken = @"accessToken",
	.allowLocalContentAccessWithoutAuthentication = @"allowLocalContentAccessWithoutAuthentication",
	.id = @"id",
	.loginName = @"loginName",
	.mostRecentAllCollectionsUpdate = @"mostRecentAllCollectionsUpdate",
	.mostRecentAllDocumentsUpdate = @"mostRecentAllDocumentsUpdate",
	.mostRecentAllEventsUpdate = @"mostRecentAllEventsUpdate",
	.mostRecentAllEventsUpdateID = @"mostRecentAllEventsUpdateID",
	.mostRecentAllNotesUpdate = @"mostRecentAllNotesUpdate",
	.mostRecentAllNotesUpload = @"mostRecentAllNotesUpload",
	.mostRecentLogin = @"mostRecentLogin",
	.rememberPassword = @"rememberPassword",
	.serverURL = @"serverURL",
};



const struct ADXAccountRelationships ADXAccountRelationships = {
	.user = @"user",
};






@implementation ADXAccountID
@end

@implementation _ADXAccount

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"ADXAccount" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"ADXAccount";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"ADXAccount" inManagedObjectContext:moc_];
}

- (ADXAccountID*)objectID {
	return (ADXAccountID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
	
	if ([key isEqualToString:@"allowLocalContentAccessWithoutAuthenticationValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"allowLocalContentAccessWithoutAuthentication"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"rememberPasswordValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"rememberPassword"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}




@dynamic accessToken;






@dynamic allowLocalContentAccessWithoutAuthentication;



- (BOOL)allowLocalContentAccessWithoutAuthenticationValue {
	NSNumber *result = [self allowLocalContentAccessWithoutAuthentication];
	return [result boolValue];
}


- (void)setAllowLocalContentAccessWithoutAuthenticationValue:(BOOL)value_ {
	[self setAllowLocalContentAccessWithoutAuthentication:[NSNumber numberWithBool:value_]];
}


- (BOOL)primitiveAllowLocalContentAccessWithoutAuthenticationValue {
	NSNumber *result = [self primitiveAllowLocalContentAccessWithoutAuthentication];
	return [result boolValue];
}

- (void)setPrimitiveAllowLocalContentAccessWithoutAuthenticationValue:(BOOL)value_ {
	[self setPrimitiveAllowLocalContentAccessWithoutAuthentication:[NSNumber numberWithBool:value_]];
}





@dynamic id;






@dynamic loginName;






@dynamic mostRecentAllCollectionsUpdate;






@dynamic mostRecentAllDocumentsUpdate;






@dynamic mostRecentAllEventsUpdate;






@dynamic mostRecentAllEventsUpdateID;






@dynamic mostRecentAllNotesUpdate;






@dynamic mostRecentAllNotesUpload;






@dynamic mostRecentLogin;






@dynamic rememberPassword;



- (BOOL)rememberPasswordValue {
	NSNumber *result = [self rememberPassword];
	return [result boolValue];
}


- (void)setRememberPasswordValue:(BOOL)value_ {
	[self setRememberPassword:[NSNumber numberWithBool:value_]];
}


- (BOOL)primitiveRememberPasswordValue {
	NSNumber *result = [self primitiveRememberPassword];
	return [result boolValue];
}

- (void)setPrimitiveRememberPasswordValue:(BOOL)value_ {
	[self setPrimitiveRememberPassword:[NSNumber numberWithBool:value_]];
}





@dynamic serverURL;






@dynamic user;

	






@end




