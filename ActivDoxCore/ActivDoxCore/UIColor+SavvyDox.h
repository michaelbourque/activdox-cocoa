//
//  UIColor+SavvyDox.h
//  Reader
//
//  Created by Steve Tibbett on 2013-05-02.
//  Copyright (c) 2013 ActivDox Inc. All rights reserved.
//

#if TARGET_OS_IPHONE

#import <UIKit/UIKit.h>

@interface UIColor (SavvyDox)

+ (UIColor *)adxPopoverTextColor;
+ (UIColor *)adxInsertedContentColor;
+ (UIColor *)adxUpdatedContentColor;

@end

#endif
