//
//  ADXV2ServiceClient.m
//  ActivDoxCore
//
//  Created by Gavin McKenzie on 2012-12-03.
//  Copyright (c) 2012 ActivDox Inc. All rights reserved.
//

#import "ADXV2ServiceClient.h"

#import "ADXCommonMacros.h"
#import "ADXHTTPClient.h"
#import "ADXV2Service.h"
#import "ADXPDFRequestOperation.h"
#import "ADXJSONRequestOperation.h"

#import "GRMustache.h"
#import "MMMultipartForm.h"

NSString * const kADXV2ServiceURLRequestFailedNotification = @"kADXV2ServiceURLRequestFailedNotification";
NSString * const kADXV2ServiceURLRequestSucceededNotification = @"kADXV2ServiceURLRequestSucceededNotification";
NSString * const kADXV2ServiceProcessDownloadQueueNotification = @"kADXV2ServiceProcessDownloadQueueNotification";

NSString * const kADXV2ServiceAPIVersion = @"2";
NSString * const kADXV2ServiceReceivedDocumentsLiteral = @"received";
NSString * const kADXV2ServicePublishedDocumentsLiteral = @"published";
NSString * const kADXV2ServiceDocumentContentLiteral = @"contents";
NSString * const kADXV2ServiceUserMeLiteral = @"me";

NSString * const kADXV2ServiceJSONKeyID = @"id";

// Login & Registration

static NSString * const kADXV2TemplateLoginPath = @"/2/login";
static NSString * const kADXV2TemplateRegistrationPath = @"/2/registeruser";
static NSString * const kADXV2TemplateValidationPath = @"/2/validateuser";
static NSString * const kADXV2TemplateResetPassword = @"/2/password/reset";

// User

static NSString * const kADXV2TemplateUsers = @"/2/users";
static NSString * const kADXV2TemplateUser = @"/2/users/{{userID}}";

// Collections

static NSString * const kADXV2TemplateCollections = @"/2/collections";
static NSString * const kADXV2TemplateCollectionsSince = @"/2/collections{{#hasSince}}?since={{since}}{{/hasSince}}";
static NSString * const kADXV2TemplateCollection = @"/2/collections/{{collectionID}}{{#hasSince}}?since={{since}}{{/hasSince}}";
static NSString * const kADXV2TemplateCollectionDocuments = @"/2/collections/{{collectionID}}/documents{{#hasSince}}?since={{since}}{{/hasSince}}";
static NSString * const kADXV2TemplateCollectionDocument = @"/2/collections/{{collectionID}}/documents/{{documentID}}";

// Documents

static NSString * const kADXV2TemplateDocuments = @"/2/documents{{#hasSince}}?since={{since}}{{/hasSince}}";
static NSString * const kADXV2TemplateDocument = @"/2/documents/{{documentID}}";
static NSString * const kADXV2TemplateDocumentVersions = @"/2/documents/{{documentID}}/versions";
static NSString * const kADXV2TemplateDocumentVersion = @"/2/documents/{{documentID}}/versions/{{versionID}}";
static NSString * const kADXV2TemplateDocumentVersionContent = @"/2/documents/{{documentID}}{{#hasVersionID}}/versions/{{versionID}}{{/hasVersionID}}/content";
static NSString * const kADXV2TemplateDocumentVersionRecipients = @"/2/documents/{{documentID}}{{#hasVersionID}}/versions/{{versionID}}{{/hasVersionID}}/recipients";
static NSString * const kADXV2TemplateDocumentVersionThumbnail = @"/2/documents/{{documentID}}{{#hasVersionID}}/versions/{{versionID}}{{/hasVersionID}}/thumbnail";
static NSString * const kADXV2TemplateDocumentVersionNotes = @"/2/documents/{{documentID}}{{#hasVersionID}}/versions/{{versionID}}{{/hasVersionID}}/notes{{#hasSince}}?since={{since}}{{/hasSince}}";
static NSString * const kADXV2TemplateDocumentVersionChanges = @"/2/documents/{{documentID}}{{#hasVersionID}}/versions/{{versionID}}{{/hasVersionID}}/changes";
static NSString * const kADXV2TemplateDocumentVersionChangesCompare = @"/2/documents/{{documentID}}/versions/{{versionID}}/changes/{{compareVersionID}}";

// Notes

static NSString * const kADXV2TemplateNotes = @"/2/notes";
static NSString * const kADXV2TemplateNotesSince = @"/2/notes{{#hasSince}}?since={{since}}{{/hasSince}}";
static NSString * const kADXV2TemplateNote = @"/2/notes/{{noteID}}";
static NSString * const kADXV2TemplateNoteRecipients = @"/2/notes/{{noteID}}/recipients";

// Events

static NSString * const kADXV2TemplateEvents = @"/2/events";
static NSString * const kADXV2TemplateEventsSince = @"/2/events{{#hasSince}}?since={{since}}{{/hasSince}}";
static NSString * const kADXV2TemplateEvent = @"/2/events/{{eventID}}";

// Server Status

static NSString * const kADXV2TemplateServerInfo = @"/2/serverinfo?details={{details}}";

// APNS

#if (!TARGET_OS_IPHONE)
static NSString * const kADXV2TemplatePutAPNSToken = @"/2/users/{{userID}}/tokens?apnsMacToken={{apnsToken}}{{#hasOldAPNSToken}}&apnsOldMacToken={{oldAPNSToken}}{{/hasOldAPNSToken}}";
#else
static NSString * const kADXV2TemplatePutAPNSToken = @"/2/users/{{userID}}/tokens?apnsToken={{apnsToken}}&apnsDeviceID={{apnsDeviceID}}";
#endif

// URL Construction Keys

static NSString * const kADXV2TemplateKeyUserID = @"userID";
static NSString * const kADXV2TemplateKeyCollectionID = @"collectionID";
static NSString * const kADXV2TemplateKeyDocumentID = @"documentID";
static NSString * const kADXV2TemplateKeyVersionID = @"versionID";
static NSString * const kADXV2TemplateKeyHasVersionID = @"hasVersionID";
static NSString * const kADXV2TemplateKeyCompareVersionID = @"compareVersionID";
static NSString * const kADXV2TemplateKeyNoteID = @"noteID";
static NSString * const kADXV2TemplateKeyEventID = @"eventID";
static NSString * const kADXV2TemplateKeyNewAPNSToken = @"apnsToken";
static NSString * const kADXV2TemplateKeyAPNSDeviceID = @"apnsDeviceID";
static NSString * const kADXV2TemplateKeySince = @"since";
static NSString * const kADXV2TemplateKeyHasSince = @"hasSince";
static NSString * const kADXV2TemplateKeyDetails = @"details";
static NSString * const kADXV2TemplateKeyEmail = @"email";


@interface ADXV2ServiceClient ()
@property (assign, readwrite, nonatomic) BOOL lastServiceURLRequestSucceeded;
@property (strong, nonatomic) ADXHTTPClient *httpClient;
@property (strong, nonatomic) ADXHTTPClient *downloadClient;
@property (strong, readwrite, nonatomic) NSDictionary *loginResponse;
@property (strong, nonatomic) NSCache *pathCache;
- (void)setupHTTPClientsWithBaseURL:(NSURL *)baseURL userID:(NSString *)userID;
@end

@implementation ADXV2ServiceClient

#pragma mark - Initialization

- (id)initWithBaseURL:(NSURL *)baseURL userID:(NSString *)userID
{
    self = [super init];
    if (self) {
        [self setupHTTPClientsWithBaseURL:baseURL userID:userID];
        
        _pathCache = [NSCache new];
        if (IsEmpty(userID)) {
            _userID = kADXV2ServiceUserMeLiteral;
        } else {
            _userID = [userID copy];
        }
    }
    return self;
}

- (void)setupHTTPClientsWithBaseURL:(NSURL *)baseURL userID:(NSString *)userID
{
    // FIXME: Remove the */* below once the server bug is fixed where it needs the */*
    
    _httpClient = [[ADXHTTPClient alloc] initWithBaseURL:baseURL];
    [_httpClient registerHTTPOperationClass:[ADXJSONRequestOperation class]];
    [_httpClient setDefaultHeader:@"Accept" value:@"application/json"];

    _downloadClient = [[ADXHTTPClient alloc] initWithBaseURL:baseURL];
    [_downloadClient.operationQueue setMaxConcurrentOperationCount:2];
    [_downloadClient registerHTTPOperationClass:[ADXPDFRequestOperation class]];
    [_downloadClient registerHTTPOperationClass:[AFHTTPRequestOperation class]];
    [_downloadClient setDefaultHeader:@"Accept" value:@"application/pdf,image/png,image/jpeg"];
    [_downloadClient.operationQueue addObserver:self forKeyPath:@"operationCount" options:0 context:nil];
}

#pragma mark - Accessors

- (NSURL *)baseURL
{
    return self.httpClient.baseURL;
}

- (NSString *)version
{
    return kADXV2ServiceAPIVersion;
}

- (void)setLastServiceURLRequestSucceeded:(BOOL)lastServiceURLRequestSucceeded
{
    if (_lastServiceURLRequestSucceeded != lastServiceURLRequestSucceeded) {
        _lastServiceURLRequestSucceeded = lastServiceURLRequestSucceeded;
        [[NSNotificationCenter defaultCenter] postNotificationName:kADXV2ServiceURLRequestSucceededNotification
                                                            object:self
                                                          userInfo:nil];
    }
}

- (BOOL)downloadClientHasSpaceAvailable
{
    // This isn't the number of simultaneous downloads; it's a suggested limit for the
    // number of operations to have in the download queue.
    NSUInteger downloadQueueLength = 4;
    
    return self.downloadClient != nil && self.downloadClient.operationQueue.operationCount < downloadQueueLength;
}

#pragma mark - KVO

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if (object == self.downloadClient.operationQueue && [keyPath isEqualToString:@"operationCount"]) {
        if (self.downloadClient.operationQueue.operationCount == 0) {
            [[NSNotificationCenter defaultCenter] postNotificationName:kADXV2ServiceProcessDownloadQueueNotification object:self];
        }
    } else {
        [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
    }
}

#pragma mark - Reachability

- (AFNetworkReachabilityStatus)networkReachabilityStatus
{
    return [self.httpClient networkReachabilityStatus];
}

- (void)setReachabilityStatusChangeBlock:(void (^)(AFNetworkReachabilityStatus status))block
{
    [self.httpClient setReachabilityStatusChangeBlock:block];
}

#pragma mark - APNS Tokens

- (void)updateDeviceToken:(NSData *)newToken APNSDeviceID:(NSString *)APNSDeviceID completion:(ADXV2ServiceRequestCompletionBlock)completionBlock
{
    __weak ADXV2ServiceClient *weakSelf = self;

    NSAssert([newToken length], @"updateDeviceToken expected non-nil new token");
    
    NSString *newTokenAsString = [newToken adx_hexString];
    
    NSString *path = [self pathForPutAPNSToken:newTokenAsString APNSDeviceID:APNSDeviceID user:self.userID];
    
    LogInfo(@"Registering new token (%@) device (%@)", newTokenAsString, APNSDeviceID);

    [self.httpClient postPath:path
                   parameters:nil
                      success:^(AFHTTPRequestOperation *operation, id responseObject) {
                          weakSelf.lastServiceURLRequestSucceeded = YES;
                          if (completionBlock) {
                              completionBlock(operation.response.statusCode, responseObject, nil);
                          }
                      } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                          weakSelf.lastServiceURLRequestSucceeded = NO;
                          if (completionBlock) {
                              completionBlock(operation.response.statusCode, nil, error);
                          }
                      }];
}

#pragma mark - Error Handling

- (void)processError:(NSError *)error fromOperation:(AFHTTPRequestOperation *)operation
{
    LogNSError(@"Service client processed error:", error);

    if ([[error domain] isEqualToString:NSURLErrorDomain]) {
        [[NSNotificationCenter defaultCenter] postNotificationName:kADXV2ServiceURLRequestFailedNotification
                                                            object:self
                                                          userInfo:nil];
    }
   
    if (operation.response.statusCode == 401 || operation.response.statusCode == 403) {
        [[NSNotificationCenter defaultCenter] postNotificationName:kADXServiceURLRequestUnauthorizedNotification
                                                            object:self
                                                          userInfo:nil];
    }
}

#pragma mark - URL Construction

- (GRMustacheTemplate *)pathTemplateForKey:(NSString *)key
{
    GRMustacheTemplate *result = [self.pathCache objectForKey:key];
    
    if (!result) {
        NSError *error = nil;
        result = [GRMustacheTemplate templateFromString:key error:&error];
        if (result) {
            [self.pathCache setObject:result forKey:key];
        } else {
            LogError(@"pathTemplateForKey failed on key %@", key);
            LogNSError(@"pathTemplateForKey error:", error);
        }
    }
    
    return result;
}

- (NSString *)pathForLogin
{
    return kADXV2TemplateLoginPath;
}

- (NSString *)pathForResetPassword
{
    return kADXV2TemplateResetPassword;
}

- (NSString *)pathForRegistration
{
    return kADXV2TemplateRegistrationPath;
}

- (NSString *)pathForUsers
{
    return kADXV2TemplateUsers;
}

- (NSString *)pathForUser:(NSString *)userID
{
    NSString *result = nil;
    NSError *error = nil;
    GRMustacheTemplate *userProfileTemplate = [self pathTemplateForKey:kADXV2TemplateUser];
    NSAssert(userProfileTemplate, @"pathForUserProfile couldn't generate template");

    if (IsEmpty(userID)) {
        userID = kADXV2ServiceUserMeLiteral;
    }
    

    result = [userProfileTemplate renderObject:@{kADXV2TemplateKeyUserID : userID} error:&error];
    return result;
}

- (NSString *)pathForPutAPNSToken:(NSString *)newAPNSToken APNSDeviceID:(NSString *)APNSDeviceID user:(NSString *)userID
{
    NSString *result = nil;
    NSError *error = nil;
    GRMustacheTemplate *putAPNSTokenTemplate = [self pathTemplateForKey:kADXV2TemplatePutAPNSToken];
    NSAssert(putAPNSTokenTemplate, @"pathForPutAPNSToken couldn't generate template");
    
    if (IsEmpty(newAPNSToken) || IsEmpty(userID)) {
        LogError(@"received nil arguments");
        return nil;
    }
    
    NSDictionary *dictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                userID, kADXV2TemplateKeyUserID,
                                newAPNSToken, kADXV2TemplateKeyNewAPNSToken,
                                APNSDeviceID, kADXV2TemplateKeyAPNSDeviceID,
                                nil];
    result = [putAPNSTokenTemplate renderObject:dictionary error:&error];
    return result;
}

- (NSString *)pathForServerInfo:(NSString *)details
{
    NSString *result = nil;
    NSError *error = nil;
    GRMustacheTemplate *serverInfoTemplate = [self pathTemplateForKey:kADXV2TemplateServerInfo];
    NSAssert(serverInfoTemplate, @"pathForServerInfo couldn't generate template");
    
    if (IsEmpty(details)) {
        details = @"none";
    }
    
    result = [serverInfoTemplate renderObject:@{kADXV2TemplateKeyDetails : details} error:&error];
    return result;
}

- (NSString *)pathForCollections
{
    return kADXV2TemplateCollections;
}

- (NSString *)pathForCollectionsSince:(NSDate *)since
{
    NSString *result = nil;
    NSError *error = nil;
    GRMustacheTemplate *collectionssTemplate = [self pathTemplateForKey:kADXV2TemplateCollectionsSince];
    NSAssert(collectionssTemplate, @"pathForCollectionsSince couldn't generate template");
    
    ISO8601DateFormatter *dateFormatter = nil;
    if (!dateFormatter) {
        dateFormatter = [[ISO8601DateFormatter alloc] init];
    }
    dateFormatter.includeTime = YES;
    NSString *sinceDateISO8601 = [dateFormatter stringFromDate:since];
    
    result = [collectionssTemplate renderObject:[NSDictionary dictionaryWithObjectsAndKeys:
                                                 (since ? @YES : @NO), kADXV2TemplateKeyHasSince,
                                                 (sinceDateISO8601 ? sinceDateISO8601 : @""), kADXV2TemplateKeySince,
                                                 nil] error:&error];
    return result;
}

- (NSString *)pathForCollection:(NSString *)collectionID
{
    return [self pathForCollection:collectionID since:nil];
}

- (NSString *)pathForCollection:(NSString *)collectionID since:(NSDate *)since
{
    NSString *result = nil;
    NSError *error = nil;
    GRMustacheTemplate *collectionTemplate = [self pathTemplateForKey:kADXV2TemplateCollection];
    NSAssert(collectionTemplate, @"pathForCollection couldn't generate template");
    
    if (IsEmpty(collectionID)) {
        LogError(@"received nil arguments");
        return nil;
    }

    ISO8601DateFormatter *dateFormatter = nil;
    if (!dateFormatter) {
        dateFormatter = [[ISO8601DateFormatter alloc] init];
    }
    dateFormatter.includeTime = YES;
    NSString *sinceDateISO8601 = [dateFormatter stringFromDate:since];

    result = [collectionTemplate renderObject:[NSDictionary dictionaryWithObjectsAndKeys:
                                               collectionID, kADXV2TemplateKeyCollectionID,
                                               (since ? @YES : @NO), kADXV2TemplateKeyHasSince,
                                               (sinceDateISO8601 ? sinceDateISO8601 : @""), kADXV2TemplateKeySince,
                                               nil] error:&error];
    return result;
}

- (NSString *)pathForCollectionDocuments:(NSString *)collectionID since:(NSDate *)since
{
    NSString *result = nil;
    NSError *error = nil;
    GRMustacheTemplate *collectionTemplate = [self pathTemplateForKey:kADXV2TemplateCollectionDocuments];
    NSAssert(collectionTemplate, @"pathForCollectionDocuments couldn't generate template");
    
    if (IsEmpty(collectionID)) {
        LogError(@"received nil arguments");
        return nil;
    }
    
    ISO8601DateFormatter *dateFormatter = nil;
    if (!dateFormatter) {
        dateFormatter = [[ISO8601DateFormatter alloc] init];
    }
    dateFormatter.includeTime = YES;
    NSString *sinceDateISO8601 = [dateFormatter stringFromDate:since];

    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            collectionID, kADXV2TemplateKeyCollectionID,
                            (since ? @YES : @NO), kADXV2TemplateKeyHasSince,
                            (sinceDateISO8601 ? sinceDateISO8601 : @""), kADXV2TemplateKeySince,
                            nil];
    result = [collectionTemplate renderObject:params error:&error];
    return result;
}

- (NSString *)pathForCollection:(NSString *)collectionID document:(NSString *)documentID
{
    NSString *result = nil;
    NSError *error = nil;
    GRMustacheTemplate *collectionDocumentsTemplate = [self pathTemplateForKey:kADXV2TemplateCollectionDocument];
    NSAssert(collectionDocumentsTemplate, @"pathForCollection:document: couldn't generate template");
        
    result = [collectionDocumentsTemplate renderObject:[NSDictionary dictionaryWithObjectsAndKeys:
                                                        collectionID, kADXV2TemplateKeyCollectionID,
                                                        documentID, kADXV2TemplateKeyDocumentID,
                                                        nil] error:&error];
    return result;
}

- (NSString *)pathForDocumentsSince:(NSDate *)since
{
    NSString *result = nil;
    NSError *error = nil;
    GRMustacheTemplate *documentsTemplate = [self pathTemplateForKey:kADXV2TemplateDocuments];
    NSAssert(documentsTemplate, @"pathForDocumentsSince couldn't generate template");
    
    ISO8601DateFormatter *dateFormatter = nil;
    if (!dateFormatter) {
        dateFormatter = [[ISO8601DateFormatter alloc] init];
    }
    dateFormatter.includeTime = YES;
    NSString *sinceDateISO8601 = [dateFormatter stringFromDate:since];
    
    result = [documentsTemplate renderObject:[NSDictionary dictionaryWithObjectsAndKeys:
                                              (since ? @YES : @NO), kADXV2TemplateKeyHasSince,
                                              (sinceDateISO8601 ? sinceDateISO8601 : @""), kADXV2TemplateKeySince,
                                              nil] error:&error];
    return result;
}

- (NSString *)pathForDocument:(NSString *)documentID
{
    NSString *result = nil;
    NSError *error = nil;
    GRMustacheTemplate *documentTemplate = [self pathTemplateForKey:kADXV2TemplateDocument];
    NSAssert(documentTemplate, @"pathForDocument couldn't generate template");
    
    if (IsEmpty(documentID)) {
        LogError(@"received nil arguments");
        return nil;
    }
    
    result = [documentTemplate renderObject:[NSDictionary dictionaryWithObject:documentID forKey:kADXV2TemplateKeyDocumentID] error:&error];
    return result;
}

- (NSString *)pathForDocumentVersions:(NSString *)documentID
{
    NSString *result = nil;
    NSError *error = nil;
    GRMustacheTemplate *documentTemplate = [self pathTemplateForKey:kADXV2TemplateDocumentVersions];
    NSAssert(documentTemplate, @"pathForDocumentVersions couldn't generate template");
    
    if (IsEmpty(documentID)) {
        LogError(@"received nil arguments");
        return nil;
    }
    
    result = [documentTemplate renderObject:[NSDictionary dictionaryWithObject:documentID forKey:kADXV2TemplateKeyDocumentID] error:&error];
    return result;
}

- (NSString *)pathForDocument:(NSString *)documentID version:(NSNumber *)versionID
{
    NSString *result = nil;
    NSError *error = nil;
    GRMustacheTemplate *documentVersionTemplate = [self pathTemplateForKey:kADXV2TemplateDocumentVersion];
    NSAssert(documentVersionTemplate, @"pathForDocument:version: couldn't generate template");
    
    if (IsEmpty(documentID) || IsEmpty(versionID)) {
        LogError(@"received nil arguments");
        return nil;
    }
    
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            documentID, kADXV2TemplateKeyDocumentID,
                            (versionID ? versionID : [NSNull null]), kADXV2TemplateKeyVersionID,
                            nil];
    result = [documentVersionTemplate renderObject:params error:&error];
    return result;
}

- (NSString *)pathForDocumentSubResource:(NSString *)documentID version:(NSNumber *)versionID template:(NSString *)template
{
    NSString *result = nil;
    NSError *error = nil;
    GRMustacheTemplate *subResourceTemplate = [self pathTemplateForKey:template];
    NSAssert(subResourceTemplate, @"pathForDocumentSubResource:version:template: couldn't generate template");
    
    if (IsEmpty(documentID)) {
        LogError(@"received nil arguments");
        return nil;
    }
    
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            documentID, kADXV2TemplateKeyDocumentID,
                            [NSNumber numberWithBool:(!IsEmpty(versionID))], kADXV2TemplateKeyHasVersionID,
                            (versionID ? versionID : [NSNull null]), kADXV2TemplateKeyVersionID,
                            nil];
    result = [subResourceTemplate renderObject:params error:&error];
    return result;
}

- (NSString *)pathForDocumentSubResource:(NSString *)documentID version:(NSNumber *)versionID since:(NSDate *)since template:(NSString *)template
{
    NSString *result = nil;
    NSError *error = nil;
    GRMustacheTemplate *subResourceTemplate = [self pathTemplateForKey:template];
    NSAssert(subResourceTemplate, @"pathForDocumentSubResource:version:since:template: couldn't generate template");
    
    if (IsEmpty(documentID)) {
        LogError(@"received nil arguments");
        return nil;
    }
    
    ISO8601DateFormatter *dateFormatter = nil;
    if (!dateFormatter) {
        dateFormatter = [[ISO8601DateFormatter alloc] init];
    }
    dateFormatter.includeTime = YES;
    NSString *sinceDateISO8601 = [dateFormatter stringFromDate:since];
    
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            documentID, kADXV2TemplateKeyDocumentID,
                            [NSNumber numberWithBool:(!IsEmpty(versionID))], kADXV2TemplateKeyHasVersionID,
                            (versionID ? versionID : [NSNull null]), kADXV2TemplateKeyVersionID,
                            (since ? @YES : @NO), kADXV2TemplateKeyHasSince,
                            (sinceDateISO8601 ? sinceDateISO8601 : @""), kADXV2TemplateKeySince,
                            nil];
    result = [subResourceTemplate renderObject:params error:&error];
    return result;
}

- (NSString *)pathForDocumentContent:(NSString *)documentID version:(NSNumber *)versionID
{
    return [self pathForDocumentSubResource:documentID version:versionID template:kADXV2TemplateDocumentVersionContent];
}

- (NSString *)pathForDocumentRecipients:(NSString *)documentID version:(NSNumber *)versionID
{
    return [self pathForDocumentSubResource:documentID version:versionID template:kADXV2TemplateDocumentVersionRecipients];
}

- (NSString *)pathForDocumentThumbnail:(NSString *)documentID version:(NSNumber *)versionID
{
    return [self pathForDocumentSubResource:documentID version:versionID template:kADXV2TemplateDocumentVersionThumbnail];
}

- (NSString *)pathForDocumentNotes:(NSString *)documentID version:(NSNumber *)versionID since:(NSDate *)since
{
    return [self pathForDocumentSubResource:documentID version:versionID since:since template:kADXV2TemplateDocumentVersionNotes];
}

- (NSString *)pathForDocumentChanges:(NSString *)documentID version:(NSNumber *)versionID
{
    return [self pathForDocumentSubResource:documentID version:versionID template:kADXV2TemplateDocumentVersionChanges];
}

- (NSString *)pathForDocumentVersionCompare:(NSString *)documentID version:(NSNumber *)versionID compare:(NSNumber *)compareVersionID
{
    NSString *result = nil;
    NSError *error = nil;
    GRMustacheTemplate *comparisonTemplate = [self pathTemplateForKey:kADXV2TemplateDocumentVersionChangesCompare];
    NSAssert(comparisonTemplate, @"pathForDocumentVersionCompare:version:compare: couldn't generate template");
    
    if (IsEmpty(documentID) || IsEmpty(versionID)) {
        LogError(@"received nil arguments");
        return nil;
    }
    
    result = [comparisonTemplate renderObject:[NSDictionary dictionaryWithObjectsAndKeys:
                                               documentID, kADXV2TemplateKeyDocumentID,
                                               versionID, kADXV2TemplateKeyVersionID,
                                               compareVersionID, kADXV2TemplateKeyCompareVersionID,
                                               nil] error:&error];
    return result;
}

- (NSString *)pathForNotes
{
    return kADXV2TemplateNotes;
}

- (NSString *)pathForNotesSince:(NSDate *)since
{
    NSString *result = nil;
    NSError *error = nil;
    GRMustacheTemplate *documentsTemplate = [self pathTemplateForKey:kADXV2TemplateNotesSince];
    NSAssert(documentsTemplate, @"pathForNotesSince couldn't generate template");
    
    ISO8601DateFormatter *dateFormatter = nil;
    if (!dateFormatter) {
        dateFormatter = [[ISO8601DateFormatter alloc] init];
    }
    dateFormatter.includeTime = YES;
    NSString *sinceDateISO8601 = [dateFormatter stringFromDate:since];
    
    result = [documentsTemplate renderObject:[NSDictionary dictionaryWithObjectsAndKeys:
                                              (since ? @YES : @NO), kADXV2TemplateKeyHasSince,
                                              (sinceDateISO8601 ? sinceDateISO8601 : @""), kADXV2TemplateKeySince,
                                              nil] error:&error];
    return result;
}

- (NSString *)pathForNote:(NSString *)noteID
{
    NSString *result = nil;
    NSError *error = nil;
    GRMustacheTemplate *noteTemplate = [self pathTemplateForKey:kADXV2TemplateNote];
    NSAssert(noteTemplate, @"pathForNote couldn't generate template");
    
    if (IsEmpty(noteID)) {
        LogError(@"received nil arguments");
        return nil;
    }
    
    result = [noteTemplate renderObject:[NSDictionary dictionaryWithObject:noteID forKey:kADXV2TemplateKeyNoteID] error:&error];
    return result;
}

- (NSString *)pathForNoteSubResource:(NSString *)noteID template:(NSString *)template
{
    NSString *result = nil;
    NSError *error = nil;
    GRMustacheTemplate *subResourceTemplate = [self pathTemplateForKey:template];
    NSAssert(subResourceTemplate, @"pathForNoteSubResource:template: couldn't generate template");
    
    if (IsEmpty(noteID)) {
        LogError(@"received nil arguments");
        return nil;
    }
    
    result = [subResourceTemplate renderObject:[NSDictionary dictionaryWithObjectsAndKeys:
                                                noteID, kADXV2TemplateKeyNoteID,
                                                nil] error:&error];
    return result;
}

- (NSString *)pathForNoteRecipients:(NSString *)noteID
{
    return [self pathForNoteSubResource:noteID template:kADXV2TemplateNoteRecipients];
}

- (NSString *)pathForEvents
{
    return kADXV2TemplateEvents;
}

- (NSString *)pathForEventsSince:(NSString *)sinceEventID
{
    NSString *result = nil;
    NSError *error = nil;
    GRMustacheTemplate *eventsTemplate = [self pathTemplateForKey:kADXV2TemplateEventsSince];
    NSAssert(eventsTemplate, @"pathForEventsSince couldn't generate template");
        
    result = [eventsTemplate renderObject:[NSDictionary dictionaryWithObjectsAndKeys:
                                              (sinceEventID ? @YES : @NO), kADXV2TemplateKeyHasSince,
                                              (sinceEventID ? sinceEventID : @""), kADXV2TemplateKeySince,
                                              nil] error:&error];
    return result;
}

- (NSString *)pathForEvent:(NSString *)eventID
{
    NSString *result = nil;
    NSError *error = nil;
    GRMustacheTemplate *eventTemplate = [self pathTemplateForKey:kADXV2TemplateEvent];
    NSAssert(eventTemplate, @"pathForEvent couldn't generate template");
    
    if (IsEmpty(eventID)) {
        LogError(@"received nil arguments");
        return nil;
    }
    
    result = [eventTemplate renderObject:[NSDictionary dictionaryWithObject:eventID forKey:kADXV2TemplateKeyEventID] error:&error];
    return result;
}

#pragma mark - Helpers

- (void)getPath:(NSString *)path completionBlock:(ADXV2ServiceRequestCompletionBlock)completionBlock
{
    __weak ADXV2ServiceClient *weakSelf = self;
    [self.httpClient getPath:path
                  parameters:nil
                     success:^(AFHTTPRequestOperation *operation, id responseObject) {
                         weakSelf.lastServiceURLRequestSucceeded = YES;
                         if (completionBlock) {
                             completionBlock(operation.response.statusCode, responseObject, nil);
                         }
                     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                         weakSelf.lastServiceURLRequestSucceeded = NO;
                         [weakSelf processError:error fromOperation:operation];
                         if (completionBlock) {
                             completionBlock(operation.response.statusCode, nil, error);
                         }
                     }];
}

- (void)deletePath:(NSString *)path completionBlock:(ADXV2ServiceRequestCompletionBlock)completionBlock
{
    __weak ADXV2ServiceClient *weakSelf = self;
    [self.httpClient deletePath:path
                     parameters:nil
                        success:^(AFHTTPRequestOperation *operation, id responseObject) {
                            weakSelf.lastServiceURLRequestSucceeded = YES;
                            if (completionBlock) {
                                completionBlock(operation.response.statusCode, responseObject, nil);
                            }
                        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                            weakSelf.lastServiceURLRequestSucceeded = NO;
                            [weakSelf processError:error fromOperation:operation];
                            if (completionBlock) {
                                completionBlock(operation.response.statusCode, nil, error);
                            }
                        }];
}

- (void)downloadPath:(NSString *)path completionBlock:(ADXV2ServiceRequestCompletionBlock)completionBlock
{
    [self downloadPath:path client:self.downloadClient completionBlock:completionBlock];
}

- (void)downloadPath:(NSString *)path client:(ADXHTTPClient *)client completionBlock:(ADXV2ServiceRequestCompletionBlock)completionBlock
{
    __weak ADXV2ServiceClient *weakSelf = self;
    [client getPath:path
         parameters:nil
            success:^(AFHTTPRequestOperation *operation, id responseObject) {
                weakSelf.lastServiceURLRequestSucceeded = YES;
                if (completionBlock) {
                    completionBlock(operation.response.statusCode, responseObject, nil);
                }
            } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                weakSelf.lastServiceURLRequestSucceeded = NO;
                [weakSelf processError:error fromOperation:operation];
                if (completionBlock) {
                    completionBlock(operation.response.statusCode, nil, error);
                }
            }];
}

#pragma mark - Authentication

- (void)getAccessTokenForLoginName:(NSString *)loginName password:(NSString *)password completion:(ADXV2ServiceRequestCompletionBlock)completionBlock;
{
    __weak ADXV2ServiceClient *weakSelf = self;
    NSString *path = [self pathForLogin];
    
    [self.httpClient setAuthorizationHeaderWithUsername:loginName password:password];
    [self.httpClient postPath:path
                   parameters:nil
                      success:^(AFHTTPRequestOperation *operation, id responseObject) {
                          weakSelf.lastServiceURLRequestSucceeded = YES;

                          NSDictionary *json = responseObject;
                          NSString *accessToken = [json objectForKey:kADXServiceJSONKeyAccessToken];
                          [weakSelf setLoginResponse:json];
                          
                          [weakSelf.httpClient clearAuthorizationHeader];
                          [weakSelf.downloadClient clearAuthorizationHeader];
                          
                          // FIXME remove the following line once the server supports access tokens. Whereas now the following line is necessary because our http client class will inject the access token into the query string.
                          [weakSelf.httpClient setAccessToken:accessToken];
                          [weakSelf.downloadClient setAccessToken:accessToken];
                          
                          [weakSelf.httpClient setAuthorizationHeaderWithToken:accessToken];
                          [weakSelf.downloadClient setAuthorizationHeaderWithToken:accessToken];
                          
                          if (completionBlock) {
                              completionBlock(operation.response.statusCode, responseObject, nil);
                          }
                      } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                          weakSelf.lastServiceURLRequestSucceeded = NO;
                          [weakSelf processError:error fromOperation:operation];
                          
                          [weakSelf.httpClient clearAuthorizationHeader];
                          [weakSelf.downloadClient clearAuthorizationHeader];
                          if (completionBlock) {
                              completionBlock(operation.response.statusCode, nil, error);
                          }
                      }];
}

#pragma mark - Registration

- (void)registerUserWithLoginName:(NSString *)loginName
                            email:(NSString *)emailAddress
                         password:(NSString *)password
                       completion:(ADXV2ServiceRequestCompletionBlock)completionBlock
{
    __weak ADXV2ServiceClient *weakSelf = self;
    NSString *path = [self pathForRegistration];
    
    NSMutableURLRequest *request = [self.httpClient multipartFormRequestWithMethod:@"POST"
                                                                              path:path
                                                                        parameters:nil
                                                         constructingBodyWithBlock:^(id<AFMultipartFormData> formData)
                                    {
                                        NSDictionary *registerRequestBody = @{
                                        @"primaryEmail": emailAddress,
                                        @"loginName": loginName,
                                        @"password": password
                                        };
                                        
                                        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:registerRequestBody
                                                                                           options:NSJSONWritingPrettyPrinted
                                                                                             error:nil];
                                        [formData appendPartWithFormData:jsonData name:@"user"];
                                    }];
    
    AFHTTPRequestOperation *registerOperation = [self.httpClient HTTPRequestOperationWithRequest:request success:^(AFHTTPRequestOperation *operation, id responseObject) {
        weakSelf.lastServiceURLRequestSucceeded = YES;
        responseObject = [NSNumber numberWithInteger:operation.response.statusCode];
        if (completionBlock) {
            completionBlock(operation.response.statusCode, responseObject, nil);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        weakSelf.lastServiceURLRequestSucceeded = NO;
        [weakSelf processError:error fromOperation:operation];
        if (completionBlock) {
            completionBlock(operation.response.statusCode, operation, error);
        }
    }];
    
    [self.httpClient enqueueHTTPRequestOperation:registerOperation];
}

- (void)resetPasswordForEmail:(NSString *)email
                   completion:(ADXV2ServiceRequestCompletionBlock)completionBlock
{
    NSString *path = [self pathForResetPassword];
    
    __weak ADXV2ServiceClient *weakSelf = self;
    
    NSMutableURLRequest *request = [self.httpClient unauthenticatedMultipartFormRequestWithMethod:@"POST"
                                                                              path:path
                                                                        parameters:nil
                                                         constructingBodyWithBlock:^(id<AFMultipartFormData> formData)
                                    {
                                        NSDictionary *registerRequestBody = @{
                                                                              @"resetpwdAddress": email,
                                                                              };
                                        
                                        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:registerRequestBody
                                                                                           options:NSJSONWritingPrettyPrinted
                                                                                             error:nil];
                                        [formData appendPartWithFormData:jsonData name:@"user"];
                                    }];
    
    AFHTTPRequestOperation *registerOperation = [self.httpClient HTTPRequestOperationWithRequest:request success:^(AFHTTPRequestOperation *operation, id responseObject) {
        weakSelf.lastServiceURLRequestSucceeded = YES;
        responseObject = [NSNumber numberWithInteger:operation.response.statusCode];
        if (completionBlock) {
            completionBlock(operation.response.statusCode, responseObject, nil);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        weakSelf.lastServiceURLRequestSucceeded = NO;
        [weakSelf processError:error fromOperation:operation];
        if (completionBlock) {
            completionBlock(operation.response.statusCode, operation, error);
        }
    }];
    
    [self.httpClient enqueueHTTPRequestOperation:registerOperation];
}

#pragma mark - Server Status

- (void)getServerInfo:(NSString *)details completion:(ADXV2ServiceRequestCompletionBlock)completionBlock;
{
    [self getPath:[self pathForServerInfo:details] completionBlock:completionBlock];
}

#pragma mark - Users

- (void)getUsers:(ADXV2ServiceRequestCompletionBlock)completionBlock
{
    [self getPath:[self pathForUsers] completionBlock:completionBlock];
}

- (void)getUser:(NSString *)userID completion:(ADXV2ServiceRequestCompletionBlock)completionBlock
{
    [self getPath:[self pathForUser:userID] completionBlock:completionBlock];
}

#pragma mark - Collections

- (void)getCollectionsSince:(NSDate *)since completion:(ADXV2ServiceRequestCompletionBlock)completionBlock
{
    [self getPath:[self pathForCollectionsSince:since] completionBlock:completionBlock];
}

- (void)getCollection:(NSString *)collectionID since:(NSDate *)since completion:(ADXV2ServiceRequestCompletionBlock)completionBlock
{
    [self getPath:[self pathForCollection:collectionID since:since] completionBlock:completionBlock];
}

- (void)getCollectionDocuments:(NSString *)collectionID since:(NSDate *)since completion:(ADXV2ServiceRequestCompletionBlock)completionBlock
{
    [self getPath:[self pathForCollectionDocuments:collectionID since:since] completionBlock:completionBlock];
}

- (void)postCollection:(ADXCollection *)collection payload:(NSDictionary *)optionalPayload completion:(ADXV2ServiceRequestCompletionBlock)completionBlock
{
    __weak ADXV2ServiceClient *weakSelf = self;
    
    if (IsEmpty(collection.localID) && IsEmpty(collection.id)) {
        if (completionBlock) {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                completionBlock(400, nil, nil);
            });
        }
        return;
    }
    
    NSDictionary *jsonRequest = nil;
    if (optionalPayload) {
        jsonRequest = optionalPayload;
    } else {
        jsonRequest = [collection jsonDictionary];
    }
    
    if (IsEmpty(jsonRequest)) {
        LogError(@"Derived invalid payload for collection %@", collection);
        if (completionBlock) {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                completionBlock(500, nil, nil);
            });
        }
        return;
    }
    
    if (![NSJSONSerialization isValidJSONObject:jsonRequest]) {
        LogError(@"Can't serialize request to JSON: %@", jsonRequest);
        if (completionBlock) {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                completionBlock(500, nil, nil);
            });
        }
        return;
    }
    
    NSError *jsonError = nil;
    NSData *payload = nil;
    @try {
        payload = [NSJSONSerialization dataWithJSONObject:jsonRequest
                                                  options:NSJSONWritingPrettyPrinted
                                                    error:&jsonError];
    }
    @catch (NSException *exception) {
        LogError(@"Can't serialize request to JSON: %@", jsonRequest);
        LogNSError(@"Error from JSON serialization attempt:", jsonError);
        @throw exception;
    }
    @finally {
        //
    }
    
    NSString *path = nil;
    if (collection.id) {
        path = [self pathForCollection:collection.id];
    } else {
        path = [self pathForCollections];
    }
    
    NSMutableURLRequest *request = [self.httpClient multipartFormRequestWithMethod:@"POST"
                                                                              path:path
                                                                        parameters:nil
                                                         constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
                                                             NSString *p = [[NSString alloc] initWithData:payload encoding:NSUTF8StringEncoding];
                                                             LogTrace(@"Posting collection payload: %@", p);
                                                             
                                                             [formData appendPartWithFileData:payload
                                                                                         name:@"collection"
                                                                                     fileName:@"collection"
                                                                                     mimeType:@"application/json"];
                                                         }];
    
    AFHTTPRequestOperation *operation = [self.httpClient HTTPRequestOperationWithRequest:request success:^(AFHTTPRequestOperation *operation, id responseObject) {
        weakSelf.lastServiceURLRequestSucceeded = YES;
        if (completionBlock) {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                completionBlock(operation.response.statusCode, responseObject, nil);
            });
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        weakSelf.lastServiceURLRequestSucceeded = NO;
        if (completionBlock) {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                completionBlock(operation.response.statusCode, nil, error);
            });
        }
    }];
    
    [self.httpClient enqueueHTTPRequestOperation:operation];
}

- (void)deleteCollection:(NSString *)collectionID completion:(ADXV2ServiceRequestCompletionBlock)completionBlock
{
    [self deletePath:[self pathForCollection:collectionID] completionBlock:completionBlock];
}

- (void)linkDocument:(NSString *)documentID withCollection:(NSString *)collectionID completion:(ADXV2ServiceRequestCompletionBlock)completionBlock
{
    __weak ADXV2ServiceClient *weakSelf = self;

    NSString *path = [self pathForCollectionDocuments:collectionID since:nil];
    NSDictionary *subresource = @{@"document": documentID};
    NSData *payload = [NSJSONSerialization dataWithJSONObject:subresource
                                                          options:NSJSONWritingPrettyPrinted
                                                            error:nil];

    NSMutableURLRequest *request = [self.httpClient multipartFormRequestWithMethod:@"POST"
                                                                              path:path
                                                                        parameters:nil
                                                         constructingBodyWithBlock:^(id<AFMultipartFormData> formData)
                                    {
                                        NSString *p = [[NSString alloc] initWithData:payload encoding:NSUTF8StringEncoding];
                                        LogTrace(@"Posting payload: %@", p);
                                        [formData appendPartWithFormData:payload name:@"document"];
                                    }];
    
    AFHTTPRequestOperation *operation = [self.httpClient HTTPRequestOperationWithRequest:request success:^(AFHTTPRequestOperation *operation, id responseObject) {
        weakSelf.lastServiceURLRequestSucceeded = YES;
        responseObject = [NSNumber numberWithInteger:operation.response.statusCode];
        if (completionBlock) {
            completionBlock(operation.response.statusCode, responseObject, nil);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        weakSelf.lastServiceURLRequestSucceeded = NO;
        [weakSelf processError:error fromOperation:operation];
        if (completionBlock) {
            completionBlock(operation.response.statusCode, operation, error);
        }
    }];
    
    [self.httpClient enqueueHTTPRequestOperation:operation];
}

- (void)unlinkDocument:(NSString *)documentID withCollection:(NSString *)collectionID completion:(ADXV2ServiceRequestCompletionBlock)completionBlock
{
    [self deletePath:[self pathForCollection:collectionID document:documentID] completionBlock:completionBlock];
}

#pragma mark - Documents

- (AFHTTPRequestOperation *)getContentOperationForDocumentID:(NSString *)documentID
                                                     version:(NSNumber *)versionID
                                          backgroundDownload:(BOOL)backgroundDownload
                                                    progress:(ADXV2ServiceDownloadProgressBlock)progressBlock
                                                  completion:(ADXV2ServiceRequestCompletionBlock)completionBlock
{
    __weak ADXV2ServiceClient *weakSelf = self;
    NSString *path = [self pathForDocumentContent:documentID version:versionID];
    NSMutableURLRequest *request = [self.downloadClient requestWithMethod:@"GET" path:path parameters:nil];

    AFHTTPRequestOperation *operation = [self.downloadClient HTTPRequestOperationWithRequest:request
                                                                                     success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                                                                         weakSelf.lastServiceURLRequestSucceeded = YES;

                                                                                         NSLog(@"getContentOperationForDocumentID SUCCESS original request: %@", request);
                                                                                         NSLog(@"getContentOperationForDocumentID SUCCESS operation request: %@ response: %lu", operation.request, (unsigned long)[(NSData *)responseObject length]);
                                                                                         if (completionBlock) {
                                                                                             completionBlock(operation.response.statusCode, responseObject, nil);
                                                                                         }
                                                                                     }
                                                                                     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                                                                         weakSelf.lastServiceURLRequestSucceeded = NO;
                                                                                         [weakSelf processError:error fromOperation:operation];

                                                                                         NSLog(@"getContentOperationForDocumentID FAILED operation request: %@", operation.request);
                                                                                         NSLog(@"getContentOperationForDocumentID FAILED operation response headers: %@", operation.response.allHeaderFields);
                                                                                         if (error) {
                                                                                             LogError(@"getContentOperationForDocumentID failed operation %@", operation);
                                                                                             LogNSError(@"getContentOperationForDocumentID failed with error ", error);
                                                                                         }
                                                                                         if (completionBlock) {
                                                                                             completionBlock(operation.response.statusCode, nil, error);
                                                                                         }
                                                                                     }];
    if (backgroundDownload) {
#ifndef TARGET_OS_MAC
        [operation setShouldExecuteAsBackgroundTaskWithExpirationHandler:nil];
#endif
    }
    if (progressBlock) {
        [operation setDownloadProgressBlock:progressBlock];
    }
    return operation;
}

- (void)getDocumentContentViaOperation:(AFHTTPRequestOperation *)operation;
{
    [self.downloadClient enqueueHTTPRequestOperation:operation];
}

- (void)getDocumentsSince:(NSDate *)since completion:(ADXV2ServiceRequestCompletionBlock)completionBlock
{
    [self getPath:[self pathForDocumentsSince:since] completionBlock:completionBlock];
}

- (void)getDocument:(NSString *)documentID completion:(ADXV2ServiceRequestCompletionBlock)completionBlock
{
    [self getPath:[self pathForDocument:documentID] completionBlock:completionBlock];
}

- (void)getDocumentVersions:(NSString *)documentID completion:(ADXV2ServiceRequestCompletionBlock)completionBlock
{
    [self getPath:[self pathForDocumentVersions:documentID] completionBlock:completionBlock];
}

- (void)getDocument:(NSString *)documentID version:(NSNumber *)versionID completion:(ADXV2ServiceRequestCompletionBlock)completionBlock
{
    if (IsEmpty(versionID)) {
        [self getPath:[self pathForDocument:documentID] completionBlock:completionBlock];
    } else {
        [self getPath:[self pathForDocument:documentID version:versionID] completionBlock:completionBlock];
    }
}

- (void)getDocumentContent:(NSString *)documentID version:(NSNumber *)versionID completion:(ADXV2ServiceRequestCompletionBlock)completionBlock
{
    [self downloadPath:[self pathForDocumentContent:documentID version:versionID] completionBlock:completionBlock];
}

- (void)getDocumentRecipients:(NSString *)documentID version:(NSNumber *)versionID completion:(ADXV2ServiceRequestCompletionBlock)completionBlock
{
    [self getPath:[self pathForDocumentRecipients:documentID version:versionID] completionBlock:completionBlock];
}

- (void)getDocumentThumbnail:(NSString *)documentID version:(NSNumber *)versionID completion:(ADXV2ServiceRequestCompletionBlock)completionBlock
{
    [self downloadPath:[self pathForDocumentThumbnail:documentID version:versionID] client:self.downloadClient completionBlock:completionBlock];
}

- (void)getDocumentNotes:(NSString *)documentID version:(NSNumber *)versionID since:(NSDate *)since completion:(ADXV2ServiceRequestCompletionBlock)completionBlock
{
    [self getPath:[self pathForDocumentNotes:documentID version:versionID since:since] completionBlock:completionBlock];
}

- (void)getDocumentChanges:(NSString *)documentID version:(NSNumber *)versionID completion:(ADXV2ServiceRequestCompletionBlock)completionBlock
{
    [self getPath:[self pathForDocumentChanges:documentID version:versionID] completionBlock:completionBlock];
}

- (void)getDocumentChanges:(NSString *)documentID version:(NSNumber *)versionID compare:(NSNumber *)compareVersionID completion:(ADXV2ServiceRequestCompletionBlock)completionBlock
{
    [self getPath:[self pathForDocumentVersionCompare:documentID version:versionID compare:compareVersionID] completionBlock:completionBlock];
}

- (void)deleteDocument:(NSString *)documentID completion:(ADXV2ServiceRequestCompletionBlock)completionBlock
{
    [self deletePath:[self pathForDocument:documentID] completionBlock:completionBlock];
}

- (void)cancelAllDownloads
{
    [self.downloadClient cancelAllHTTPOperationsWithMethod:nil];
}

- (void)uploadDocumentAtURL:(NSURL *)documentURL
                      title:(NSString *)title
                 recipients:(NSDictionary *)recipients
                 completion:(ADXV2ServiceRequestCompletionBlock)completionBlock
{
    // FIXME: (GMCK)
//    NSString *loginName = [self.loginResponse valueForKey:@"loginName"];
//    NSURL *url = [NSURL URLWithString:[self pathForUploadDocumentWithAccessToken:loginName] relativeToURL:self.baseURL];
//    
//    // Document information
//    NSDictionary *documentData = @{
//    @"contentType": @"application/pdf",
//    @"title": title,
//    @"recipients": recipients
//    };
//    
//    // Convert to JSON String
//    NSError *error = nil;
//    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:documentData
//                                                       options:NSJSONWritingPrettyPrinted
//                                                         error:&error];
//    
//    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
//    
//    // Build the multipart form submission
//    MMMultipartForm *form = [[MMMultipartForm alloc] initWithURL:url];
//    [form addFile:documentURL.path withFieldName:@"content"];
//    [form addFormField:@"document" withStringData:jsonString];
//    
//    // Build the POST request
//    NSMutableURLRequest *postRequest = [form mpfRequest];
//    
//    [NSURLConnection sendAsynchronousRequest:postRequest
//                                       queue:[NSOperationQueue mainQueue]
//                           completionHandler:^(NSURLResponse *response, NSData *responseData, NSError *error) {
//                               if (error == nil) {
//                                   NSString *responseString = [NSString stringWithCString:[responseData bytes] encoding:NSUTF8StringEncoding];
//#ifdef DEBUG
//                                   NSLog(@"document upload response body: %@", responseString);
//#endif
//                                   completionBlock(200, responseString, nil);
//                               } else {
//                                   completionBlock(500, nil, error);
//                               }
//                           }];
}

- (void)shareDocumentWithID:(NSString *)documentID withEmailAddresses:(NSArray *)addresses message:(NSString *)message completion:(ADXV2ServiceRequestCompletionBlock)completionBlock
{
    // FIXME: (GMCK)
//    NSString *path = [self pathForShareDocumentWithUserID:self.userID documentID:documentID];
//    
//    // Convert the list of addresses into a list of dictionaries with the address as the primaryEmail key of each dictionary
//    NSMutableArray *recipientsArray = [NSMutableArray array];
//    for (NSString *address in addresses) {
//        NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithObject:address forKey:@"primaryEmail"];
//        [recipientsArray addObject:dict];
//    }
//    
//    // Document information
//    NSDictionary *documentData = @{
//    @"recipients": recipientsArray
//    };
//    
//    // Convert to JSON String
//    NSError *error = nil;
//    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:documentData
//                                                       options:NSJSONWritingPrettyPrinted
//                                                         error:&error];
//    
//    // Build the multipart form submission
//    NSMutableURLRequest *request = [self.httpClient multipartFormRequestWithMethod:@"POST"
//                                                                              path:path
//                                                                        parameters:nil
//                                                         constructingBodyWithBlock:^(id<AFMultipartFormData> formData)
//                                    {
//                                        [formData appendPartWithFormData:jsonData name:@"share"];
//                                    }];
//    
//    [NSURLConnection sendAsynchronousRequest:request
//                                       queue:[NSOperationQueue mainQueue]
//                           completionHandler:^(NSURLResponse *response, NSData *responseData, NSError *error) {
//                               if (error == nil) {
//                                   NSString *responseString = [NSString stringWithCString:[responseData bytes] encoding:NSUTF8StringEncoding];
//#ifdef DEBUG
//                                   NSLog(@"shareDocument response body: %@", responseString);
//#endif
//                                   completionBlock(200, responseString, nil);
//                               } else {
//                                   completionBlock(500, nil, error);
//                               }
//                           }];
}

- (void)downloadImageAtPath:(NSString *)path completion:(ADXV2ServiceRequestCompletionBlock)completionBlock
{
    [self downloadPath:path client:self.downloadClient completionBlock:completionBlock];
}

#pragma mark - Notes

- (void)getNotesSince:(NSDate *)since completion:(ADXV2ServiceRequestCompletionBlock)completionBlock;
{
    [self getPath:[self pathForNotesSince:since] completionBlock:completionBlock];
}

- (void)getNote:(NSString *)noteID completion:(ADXV2ServiceRequestCompletionBlock)completionBlock
{
    [self getPath:[self pathForNote:noteID] completionBlock:completionBlock];
}

- (void)deleteNote:(NSString *)noteID completion:(ADXV2ServiceRequestCompletionBlock)completionBlock
{
    [self deletePath:[self pathForNote:noteID] completionBlock:completionBlock];
}

- (void)getNoteRecipients:(NSString *)noteID completion:(ADXV2ServiceRequestCompletionBlock)completionBlock
{
    [self getPath:[self pathForNoteRecipients:noteID] completionBlock:completionBlock];
}

- (void)updateNote:(ADXDocumentNote *)note payload:(NSDictionary *)optionalPayload completion:(ADXV2ServiceRequestCompletionBlock)completionBlock
{
    __weak ADXV2ServiceClient *weakSelf = self;
    
    // We can't update a note that doesn't have an id. A note without an ID is a note that hasn't been added to the server yet.
    if (IsEmpty(note.id)) {
        if (completionBlock) {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                completionBlock(400, nil, nil);
            });
        }
        return;
    }
    
    NSDictionary *jsonRequest = nil;
    if (optionalPayload) {
        jsonRequest = optionalPayload;
    } else {
        jsonRequest = [note jsonDictionary];
    }

    if (IsEmpty(jsonRequest)) {
        LogError(@"Derived invalid payload for note %@", note);
        if (completionBlock) {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                completionBlock(500, nil, nil);
            });
        }
        return;
    }

    if (![NSJSONSerialization isValidJSONObject:jsonRequest]) {
        LogError(@"Can't serialize request to JSON: %@", jsonRequest);
        if (completionBlock) {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                completionBlock(500, nil, nil);
            });
        }
        return;
    }

    NSError *jsonError = nil;
    NSData *payload = nil;
    @try {
        payload = [NSJSONSerialization dataWithJSONObject:jsonRequest
                                                  options:NSJSONWritingPrettyPrinted
                                                    error:&jsonError];
    }
    @catch (NSException *exception) {
        LogError(@"Can't serialize request to JSON: %@", jsonRequest);
        LogNSError(@"Error from JSON serialization attempt:", jsonError);
        @throw exception;
    }
    @finally {
        //
    }
    
    NSMutableURLRequest *request = [self.httpClient multipartFormRequestWithMethod:@"POST"
                                                                              path:[self pathForNote:note.id]
                                                                        parameters:nil
                                                         constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
                                                             NSString *p = [[NSString alloc] initWithData:payload encoding:NSUTF8StringEncoding];
                                                             LogTrace(@"Posting note payload: %@", p);
                                                             
                                                             [formData appendPartWithFileData:payload
                                                                                         name:@"note"
                                                                                     fileName:@"note"
                                                                                     mimeType:@"application/json"];
                                                         }];
    
    AFHTTPRequestOperation *operation = [self.httpClient HTTPRequestOperationWithRequest:request success:^(AFHTTPRequestOperation *operation, id responseObject) {
        weakSelf.lastServiceURLRequestSucceeded = YES;
        if (completionBlock) {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                completionBlock(operation.response.statusCode, responseObject, nil);
            });
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        weakSelf.lastServiceURLRequestSucceeded = NO;
        if (completionBlock) {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                id responseObject = ((AFJSONRequestOperation *)operation).responseJSON;
                completionBlock(operation.response.statusCode, responseObject, error);
            });
        }
    }];
    
    [self.httpClient enqueueHTTPRequestOperation:operation];
}

- (void)postNote:(ADXDocumentNote *)note payload:(NSDictionary *)optionalPayload completion:(ADXV2ServiceRequestCompletionBlock)completionBlock
{
    __weak ADXV2ServiceClient *weakSelf = self;
    
    // When adding a note, it should have only a localID. The presence of an ID attribute indicates a note that has already been added.
    if (IsEmpty(note.localID) || !IsEmpty(note.id)) {
        if (completionBlock) {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                completionBlock(400, nil, nil);
            });
        }
        return;
    }
    
    NSArray *jsonNotes = nil;
    if (optionalPayload) {
        jsonNotes = @[optionalPayload];
    } else {
        jsonNotes = @[[note jsonDictionary]];
    }
    
    NSDictionary *jsonRequest = @{ADXJSONDocumentNoteAttributes.notesArray: jsonNotes};
    if (IsEmpty(jsonRequest)) {
        LogError(@"Derived invalid payload for note %@", note);
        if (completionBlock) {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                completionBlock(500, nil, nil);
            });
        }
        return;
    }

    if (![NSJSONSerialization isValidJSONObject:jsonRequest]) {
        LogError(@"Can't serialize request to JSON: %@", jsonRequest);
        if (completionBlock) {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                completionBlock(500, nil, nil);
            });
        }
        return;
    }

    NSError *jsonError = nil;
    NSData *payload = nil;
    @try {
        payload = [NSJSONSerialization dataWithJSONObject:jsonRequest
                                                  options:NSJSONWritingPrettyPrinted
                                                    error:&jsonError];
    }
    @catch (NSException *exception) {
        LogError(@"Can't serialize request to JSON: %@", jsonRequest);
        LogNSError(@"Error from JSON serialization attempt:", jsonError);
        @throw exception;
    }
    @finally {
        //
    }
    
    NSMutableURLRequest *request = [self.httpClient multipartFormRequestWithMethod:@"POST"
                                                                              path:[self pathForNotes]
                                                                        parameters:nil
                                                         constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
                                                             NSString *p = [[NSString alloc] initWithData:payload encoding:NSUTF8StringEncoding];
                                                             LogTrace(@"Posting note payload: %@", p);
                                                             
                                                             [formData appendPartWithFileData:payload
                                                                                         name:@"note"
                                                                                     fileName:@"note"
                                                                                     mimeType:@"application/json"];
                                                         }];
    
    AFHTTPRequestOperation *operation = [self.httpClient HTTPRequestOperationWithRequest:request success:^(AFHTTPRequestOperation *operation, id responseObject) {
        weakSelf.lastServiceURLRequestSucceeded = YES;
        if (completionBlock) {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                completionBlock(operation.response.statusCode, responseObject, nil);
            });
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        weakSelf.lastServiceURLRequestSucceeded = NO;
        if (completionBlock) {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                completionBlock(operation.response.statusCode, nil, error);
            });
        }
    }];
    
    [self.httpClient enqueueHTTPRequestOperation:operation];
}

- (void)postNotes:(NSArray *)notes completion:(ADXV2ServiceRequestCompletionBlock)completionBlock
{
    __weak ADXV2ServiceClient *weakSelf = self;
    
    NSMutableArray *jsonNotes = [[NSMutableArray alloc] init];
    for (ADXDocumentNote *note in notes) {
        [jsonNotes addObject:[note jsonDictionary]];
    }

    NSDictionary *jsonRequest = @{ADXJSONDocumentNoteAttributes.notesArray: jsonNotes};
    if (IsEmpty(jsonRequest)) {
        LogError(@"Derived invalid payload for notes %@", notes);
        if (completionBlock) {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                completionBlock(500, nil, nil);
            });
        }
        return;
    }
    
    if (![NSJSONSerialization isValidJSONObject:jsonRequest]) {
        LogError(@"Can't serialize request to JSON: %@", jsonRequest);
        if (completionBlock) {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                completionBlock(500, nil, nil);
            });
        }
        return;
    }

    NSError *jsonError = nil;
    NSData *payload = nil;
    @try {
        payload = [NSJSONSerialization dataWithJSONObject:jsonRequest
                                                  options:NSJSONWritingPrettyPrinted
                                                    error:&jsonError];
    }
    @catch (NSException *exception) {
        LogError(@"Can't serialize request to JSON: %@", jsonRequest);
        LogNSError(@"Error from JSON serialization attempt:", jsonError);
        @throw exception;
    }
    @finally {
        //
    }
    
    NSMutableURLRequest *request = [self.httpClient multipartFormRequestWithMethod:@"POST"
                                                                              path:[self pathForNotes]
                                                                        parameters:nil
                                                         constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
                                                             NSString *p = [[NSString alloc] initWithData:payload encoding:NSUTF8StringEncoding];
                                                             LogTrace(@"Posting note payload: %@", p);

                                                             [formData appendPartWithFileData:payload
                                                                                         name:@"note"
                                                                                     fileName:@"note"
                                                                                     mimeType:@"application/json"];
                                                         }];
    
    AFHTTPRequestOperation *operation = [self.httpClient HTTPRequestOperationWithRequest:request success:^(AFHTTPRequestOperation *operation, id responseObject) {
        weakSelf.lastServiceURLRequestSucceeded = YES;
        if (completionBlock) {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                completionBlock(operation.response.statusCode, responseObject, nil);
            });
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        weakSelf.lastServiceURLRequestSucceeded = NO;
        if (completionBlock) {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                completionBlock(operation.response.statusCode, nil, error);
            });
        }
    }];
    
    [self.httpClient enqueueHTTPRequestOperation:operation];
}

#pragma mark - Events

- (void)getEventsSince:(NSString *)sinceEventID completion:(ADXV2ServiceRequestCompletionBlock)completionBlock
{
    [self getPath:[self pathForEventsSince:sinceEventID] completionBlock:completionBlock];
}

- (void)getEvent:(NSString *)eventID completion:(ADXV2ServiceRequestCompletionBlock)completionBlock
{
    [self getPath:[self pathForEvent:eventID] completionBlock:completionBlock];
}

- (void)postEvents:(NSArray *)events completion:(ADXV2ServiceRequestCompletionBlock)completionBlock
{
    NSError *jsonError = nil;
    NSString *path = [self pathForEvents];
    
    NSMutableArray *jsonEvents = [[NSMutableArray alloc] init];
    for (ADXEventQueueEntry *entry in events) {
        [jsonEvents addObject:[entry jsonDictionary]];
    }
    
    NSDictionary *jsonRequest = [NSDictionary dictionaryWithObject:jsonEvents forKey:@"events"];
    if (IsEmpty(jsonRequest)) {
        LogError(@"Derived invalid payload for events %@", events);
        if (completionBlock) {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                completionBlock(500, nil, nil);
            });
        }
        return;
    }
    
    if (![NSJSONSerialization isValidJSONObject:jsonRequest]) {
        LogError(@"Can't serialize request to JSON: %@", jsonRequest);
        if (completionBlock) {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                completionBlock(500, nil, nil);
            });
        }
        return;
    }

    NSData *payload = nil;
    @try {
        payload = [NSJSONSerialization dataWithJSONObject:jsonRequest
                                                  options:NSJSONWritingPrettyPrinted
                                                    error:&jsonError];
    }
    @catch (NSException *exception) {
        LogError(@"Can't serialize request to JSON: %@", jsonRequest);
        LogNSError(@"Error from JSON serialization attempt:", jsonError);
        @throw exception;
    }
    @finally {
        //
    }
    
    if (!payload) {
        LogNSError(@"Error converting event to JSON", jsonError);
        if (completionBlock) {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                completionBlock(400, nil, jsonError);
            });
        }
    }
    
    NSMutableURLRequest *request = [self.httpClient multipartFormRequestWithMethod:@"POST"
                                                                              path:path
                                                                        parameters:nil
                                                         constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
                                                             [formData appendPartWithFileData:payload
                                                                                         name:@"events"
                                                                                     fileName:@"events"
                                                                                     mimeType:@"application/json"];
                                                         }];
    
    AFHTTPRequestOperation *operation = [self.httpClient HTTPRequestOperationWithRequest:request success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if (completionBlock) {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                completionBlock(operation.response.statusCode, responseObject, nil);
            });
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (completionBlock) {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                completionBlock(operation.response.statusCode, nil, error);
            });
        }
    }];
    
    [self.httpClient enqueueHTTPRequestOperation:operation];
}

@end
