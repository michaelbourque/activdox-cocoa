#import <CoreFoundation/CoreFoundation.h>
#import <CoreGraphics/CoreGraphics.h>

#import "_ADXHighlightedContent.h"

@interface ADXHighlightedContent : _ADXHighlightedContent {}

// Custom logic goes here.
@end
