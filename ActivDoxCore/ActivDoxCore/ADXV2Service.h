//
//  ADXV2Service.h
//  ActivDoxCore
//
//  Created by Gavin McKenzie on 2012-12-05.
//  Copyright (c) 2012 ActivDox Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import <SystemConfiguration/SystemConfiguration.h>
#import "AFHTTPClient.h"
#import "ADXModel.h"

extern NSString * const kADXServiceURLRequestUnauthorizedNotification;

extern NSString * const kADXServiceDocumentDownloadedNotification;
extern NSString * const kADXServiceAllDocumentsDownloadedNotification;
extern NSString * const kADXServiceDocumentThumbnailDownloadedNotification;
extern NSString * const kADXServiceDocumentThumbnailNotDownloadedNotification;
extern NSString * const kADXServiceAllNotesDownloadedNotification;
extern NSString * const kADXServiceDocumentNotesDownloadedNotification;
extern NSString * const kADXServiceCollectionRefreshCompletedNotification;
extern NSString * const kADXServiceAllCollectionsRefreshCompletedNotification;
extern NSString * const kADXServiceAllDocumentsRefreshCompletedNotification;
extern NSString * const kADXServiceAllEventsRefreshCompletedNotification;
extern NSString * const kADXServiceNotificationPayload;
extern NSString * const kADXServiceNotificationProgressUpdate;
extern NSString * const kADXServiceNotificationProgressStarted;
extern NSString * const kADXServiceNotificationProgressUpdateFinished;
extern NSString * const kADXServiceNotificationProgressUpdateUserInfoBytesReadKey;
extern NSString * const kADXServiceNotificationProgressUpdateUserInfoBytesExpectedKey;
extern NSString * const kADXServiceNotificationReachabilityChanged;
extern NSString * const kADXServiceNotificationDocumentInaccessible;
extern NSString * const kADXServiceNotificationLatestDocumentVersionChanged;
extern NSString * const kADXServiceNotificationAPNSTokenReceived;

extern NSString * const kADXServiceConflictingNoteChangesDiscardedNotification;

extern NSString * const kADXServiceServerInfoNone;
extern NSString * const kADXServiceServerInfoMin;
extern NSString * const kADXServiceServerInfoFull;

extern NSString * const kADXServiceServerInfoAPNSProduction;
extern NSString * const kADXServiceServerInfoAPNSSandbox;

typedef void(^ADXServiceCompletionBlock)(BOOL success, NSError *error);
typedef void(^ADXServiceCompletionBlockWithStatusCode)(NSUInteger statusCode, BOOL success, NSError *error);
typedef void(^ADXServiceCompletionBlockWithResponse)(id response, BOOL success, NSError *error);

typedef void(^ADXServiceDateResultCompletionBlock)(NSDate *date);
typedef void(^ADXServiceIDResultCompletionBlock)(NSString *identifier);
typedef void(^ADXServiceArrayResultCompletionBlock)(NSArray *results);

@class ADXDocument;
@class ADXCollection;
@class ADXAccount;
@class ADXUser;

@interface ADXV2Service : NSObject
@property (strong, nonatomic, readonly) NSManagedObjectContext *managedObjectContext;
@property (nonatomic) BOOL automaticallyDownloadDocumentContent;
@property (nonatomic, readonly) BOOL networkReachable;
@property (nonatomic, assign, readonly) BOOL serverReachable;
@property (nonatomic, readonly) BOOL hasAccessToken;
@property (nonatomic, readonly) BOOL hasUploadedDeviceToken;
@property (readonly, nonatomic) NSString *accessToken;
@property (readonly, nonatomic) ADXAccount *account;
@property (readonly, nonatomic) NSString *loginResponseAccountID;
@property (readonly, nonatomic) NSDictionary *loginResponse;

+ (ADXV2Service *)createServiceForServerURL:(NSURL *)serverURL parentManagedObjectContext:(NSManagedObjectContext *)moc;
+ (ADXV2Service *)createServiceForAccount:(ADXAccount *)account parentManagedObjectContext:(NSManagedObjectContext *)moc;
+ (ADXV2Service *)serviceForAccount:(ADXAccount *)account;
+ (ADXV2Service *)serviceForAccountID:(NSString *)accountID;
+ (void)registerService:(ADXV2Service *)service forAccountID:(NSString *)accountID;
+ (NSDictionary *)registeredServices;
+ (void)deregisterServiceForAccount:(NSString *)accountID;
+ (BOOL)deregisterService:(ADXV2Service *)service;
+ (void)deregisterAllServices;
+ (ADXV2Service *)serviceForManagedObject:(NSManagedObject *)managedObject;

+ (void)saveContextsForAllRegisteredServices;
- (void)saveContexts:(ADXServiceCompletionBlock)completionBlock;

- (void)startServiceForAccount:(ADXAccount *)account password:(NSString *)password completion:(ADXServiceCompletionBlock)completionBlock;
- (void)restartService:(ADXServiceCompletionBlock)completionBlock;

- (void)getAccessToken:(ADXServiceCompletionBlockWithStatusCode)completionBlock;
- (void)getAccessTokenForLoginName:(NSString *)loginName password:(NSString *)password completion:(ADXServiceCompletionBlockWithStatusCode)completionBlock;
- (void)reauthenticateWithLoginName:(NSString *)loginName password:(NSString *)password completion:(ADXServiceCompletionBlockWithStatusCode)completionBlock;
- (void)uploadDeviceTokens:(ADXServiceCompletionBlock)completionBlock;
- (void)updateDeviceToken:(NSData *)newToken APNSDeviceID:(NSString *)APNSDeviceID completion:(ADXServiceCompletionBlock)completionBlock;
// Returns the registration response HTTP status code as the completion block response object, wrapped in an NSNumber.
- (void)registerUserWithLoginName:(NSString *)loginName email:(NSString *)email password:(NSString *)password completion:(ADXServiceCompletionBlockWithResponse)completionBlock;

- (void)resetPasswordForEmail:(NSString *)email
                   completion:(ADXServiceCompletionBlockWithResponse)completionBlock;

//
// Generic Refresh
//

- (void)refresh;

//
// Server Info
//

- (void)getServerInfo:(NSString *)details completion:(ADXServiceCompletionBlockWithResponse)completionBlock;
- (void)getServerInfoAPNSMode:(ADXServiceCompletionBlockWithResponse)completionBlock;

//
// Collections
//

- (void)refreshCollections:(ADXServiceCompletionBlock)completionBlock;
- (void)refreshCollection:(NSString *)collectionID completion:(ADXServiceCompletionBlock)completionBlock;
- (void)refreshCollectionsAndDocuments:(ADXServiceCompletionBlock)refreshOnlyCompletionBlock downloadContent:(BOOL)downloadContent trackProgress:(BOOL)trackProgress;
- (void)forceRefreshCollectionsAndDocuments:(ADXServiceCompletionBlock)refreshOnlyCompletionBlock downloadContent:(BOOL)downloadContent trackProgress:(BOOL)trackProgress;

//
// Documents
//

- (void)refreshDocuments:(ADXServiceCompletionBlock)refreshOnlyCompletionBlock downloadContent:(BOOL)downloadContent trackProgress:(BOOL)trackProgress;
- (void)refreshDocumentsInCollections:(NSArray *)collectionIDs downloadContent:(BOOL)downloadContent trackProgress:(BOOL)trackProgress;
- (void)refreshDocumentsInCollection:(NSString *)collectionID downloadContent:(BOOL)downloadContent trackProgress:(BOOL)trackProgress;
- (void)forceRefreshDocumentsInCollection:(NSString *)collectionID downloadContent:(BOOL)downloadContent trackProgress:(BOOL)trackProgress;
- (void)refreshDocumentsVersionHistory:(NSArray *)documents;
- (void)refreshDocumentVersions:(NSString *)documentID completion:(ADXServiceCompletionBlock)completionBlock;
- (void)refreshDocument:(NSString *)documentID versionID:(NSNumber *)versionID downloadContent:(BOOL)downloadContent trackProgress:(BOOL)trackProgress completion:(ADXServiceCompletionBlock)completionBlock;

- (void)downloadContentForDocument:(NSString *)documentID versionID:(NSNumber *)versionID trackProgress:(BOOL)trackProgress completion:(ADXServiceCompletionBlock)completionBlock;
- (void)forceDownloadContentForDocument:(NSString *)documentID versionID:(NSNumber *)versionID trackProgress:(BOOL)trackProgress completion:(ADXServiceCompletionBlock)completionBlock;
- (void)excludeDocumentFromAutomaticContentDownload:(NSString *)documentID removeContentNow:(BOOL)removeContentNow completion:(ADXServiceCompletionBlock)completionBlock;
- (void)includeDocumentInAutomaticContentDownload:(NSString *)documentID completion:(ADXServiceCompletionBlock)completionBlock;

- (void)deleteDocument:(NSString *)documentID completion:(ADXServiceCompletionBlock)completionBlock;

- (void)downloadThumbnailForDocument:(NSString *)documentID versionID:(NSNumber *)versionID completion:(ADXServiceCompletionBlock)completionBlock;
- (void)forceDownloadThumbnailForDocument:(NSString *)documentID versionID:(NSNumber *)versionID completion:(ADXServiceCompletionBlock)completionBlock;

- (void)changesForDocument:(NSString *)documentID versionID:(NSNumber *)versionID compare:(NSNumber *)compareVersionID completion:(ADXServiceCompletionBlockWithResponse)completionBlock;
- (void)downloadChangesForDocument:(NSString *)documentID versionID:(NSNumber *)versionID completion:(ADXServiceCompletionBlock)completionBlock;
- (void)downloadChangesForDocument:(NSString *)documentID versionID:(NSNumber *)versionID compare:(NSNumber *)compareVersionID completion:(ADXServiceCompletionBlockWithResponse)completionBlock;

- (void)downloadImageAtPath:(NSString *)path completion:(ADXServiceCompletionBlockWithResponse)completionBlock;

//
// Publishing and Sharing
//

- (void)shareDocument:(ADXDocument *)document withEmailAddresses:(NSArray *)addresses message:(NSString *)message completion:(ADXServiceCompletionBlockWithResponse)completionBlock;
- (void)uploadDocumentAtURL:(NSURL *)documentURL completion:(ADXServiceCompletionBlock)completionBlock;
- (NSURL *)shareableURLForDocument:(ADXDocument *)document;

//
// Notes
//

- (void)refreshNotes:(ADXServiceCompletionBlock)completionBlock;
- (void)refreshNotesForDocument:(NSString *)documentID completion:(ADXServiceCompletionBlock)completionBlock;
- (void)refreshNotesForDocument:(NSString *)documentID versionID:(NSNumber *)versionID completion:(ADXServiceCompletionBlock)completionBlock;

// TODO
// - (void)uploadNotesForDocument:(NSString *)documentID versionID:(NSNumber *)versionID completion:(ADXServiceCompletionBlock)completionBlock;

//
// Events (Analytics)
//

- (void)refreshEvents:(ADXServiceCompletionBlock)completionBlock;
- (void)getEvent:(NSString *)eventID completion:(ADXServiceCompletionBlockWithResponse)completionBlock;
- (void)submitEventsWithCompletion:(ADXServiceCompletionBlock)completionBlock;
- (void)emitEventDidOpenDocument:(NSString *)documentID versionID:(NSNumber *)versionID completion:(ADXServiceCompletionBlock)completionBlock;
- (void)emitEventDidCloseDocument:(NSString *)documentID versionID:(NSNumber *)versionID completion:(ADXServiceCompletionBlock)completionBlock;
- (void)emitEventDidOpenPageOfDocument:(NSString *)documentID versionID:(NSNumber *)versionID page:(NSUInteger)page completion:(ADXServiceCompletionBlock)completionBlock;
- (void)emitEventDidClosePageOfDocument:(NSString *)documentID versionID:(NSNumber *)versionID page:(NSUInteger)page completion:(ADXServiceCompletionBlock)completionBlock;
- (void)requestDocumentIDForEventWithID:(NSString *)eventID completion:(ADXServiceCompletionBlockWithResponse)completionBlock;

@end
