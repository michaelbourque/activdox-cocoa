//
//  ADXJSONRequestOperation.m
//  ActivDoxCore
//
//  Created by Gavin McKenzie on 12-06-05.
//  Copyright (c) 2012 ActivDox Inc. All rights reserved.
//

#import "ADXJSONRequestOperation.h"

@implementation ADXJSONRequestOperation

+ (NSSet *)acceptableContentTypes {
return [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/plain", nil];
}

@end
