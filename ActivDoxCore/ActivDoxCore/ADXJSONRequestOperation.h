//
//  ADXJSONRequestOperation.h
//  ActivDoxCore
//
//  Created by Gavin McKenzie on 12-06-05.
//  Copyright (c) 2012 ActivDox Inc. All rights reserved.
//

#import "AFJSONRequestOperation.h"

@interface ADXJSONRequestOperation : AFJSONRequestOperation

@end
