//
//  ADXCommonMacros.c
//  ActivDoxCore
//
//  Created by Gavin McKenzie on 12-05-10.
//  Copyright (c) 2012 ActivDox Inc. All rights reserved.
//

#import "ADXCommonMacros.h"
#import <CoreData/CoreData.h>
#include <sys/xattr.h>

BOOL IsEmpty(id thing) 
{
    return thing == nil
	|| ([thing respondsToSelector:@selector(length)]
        && [(NSData *)thing length] == 0)
	|| ([thing respondsToSelector:@selector(count)]
        && [(NSArray *)thing count] == 0);
}

BOOL addSkipBackupAttributeToItemAtURL(NSURL *url)
{
    const char* filePath = [[url path] fileSystemRepresentation];
    const char* attrName = "com.apple.MobileBackup";
    u_int8_t attrValue = 1;
    
    int result = setxattr(filePath, attrName, &attrValue, sizeof(attrValue), 0, 0);
    return result == 0;
}

//
// Logging
// From a comment by Bill Hollings on the following blog article:
// http://iphoneincubator.com/blog/debugging/the-evolution-of-a-replacement-for-nslog
//

void LogNSError(NSString *msg, NSError *error) {
    if (!IsEmpty(msg)) {
        LogError(@"%@", msg);
    }
    NSArray* detailedErrors = [[error userInfo] objectForKey:NSDetailedErrorsKey];
    if (detailedErrors != nil && [detailedErrors count] > 0) {
        for(NSError* detailedError in detailedErrors) {
            LogError(@"  DetailedError: %@", [detailedError userInfo]);
        }
    } else {
        LogError(@"  %@", [error userInfo]);
    }
}


void LogNSErrors(NSString *msg, NSArray *errors) {
	for (NSError *error in errors) {
		LogNSError(msg, error);
	}
}