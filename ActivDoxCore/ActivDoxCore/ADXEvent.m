
#import "ADXEvent.h"
#import "ADXCategories.h"
#import "ADXCommonMacros.h"
#import "ADXUserDefaults.h"

const struct ADXEventType ADXEventType = {
    .documentPrefix = @"document.",
	.documentDownloaded = @"document.downloaded",
    .documentOpened = @"document.opened",
    .documentClosed = @"document.closed",
	.documentPageOpened = @"document.page.opened",
	.documentPageClosed = @"document.page.closed",
	.documentPublished = @"document.published",
    .documentUpdated = @"document.updated",
    .documentPropertiesUpdated = @"document.properties.updated",
    .documentReadersAdded = @"document.readers.added",
    .documentReadersRemoved = @"document.readers.removed",
    .documentReadersPublicDocumentRequest = @"document.readers.selfInvitation",
};

const struct ADXJSONEventResponseAttributes ADXJSONEventResponseAttributes = {
    .resourcesDict = @"resources",
    .resources = {
        .users = @"users",
        .usersArray = @"resources.users",
        .documents = @"documents",
        .documentsArray = @"resources.documents",
    },
    .eventsDict = @"events",
};

const struct ADXJSONEventAttributes ADXJSONEventAttributes = {
    .id = @"id",
    .originator = @"originator",
    .timestamp = @"timestamp",
    .eventType = @"eventType",
    .payloadDict = @"payload",
    .payload = {
        .document = @"document",
        .version = @"version",
        .page = @"page",
    },
};

@implementation ADXEvent

+ (id)entityWithID:(NSString *)entityID context:(NSManagedObjectContext *)moc
{
    ADXEvent *result = [moc aps_fetchFirstObjectForEntityName:[ADXEvent entityName] withPredicate:@"%K == %@", ADXEventAttributes.id, entityID];
    return result;
}

+ (id)eventWithID:(NSString *)eventID accountID:(NSString *)accountID context:(NSManagedObjectContext *)moc
{
    ADXEvent *result = [moc aps_fetchFirstObjectForEntityName:[ADXEvent entityName]
                                                withPredicate:@"(%K == %@) AND (%K == %@)", ADXEventAttributes.id, eventID, ADXEventAttributes.accountID, accountID];
    return result;
}

+ (NSDictionary *)resourceForDocument:(NSString *)documentID resources:(NSDictionary *)resources
{
    __block NSDictionary *documentResource = nil;
    
    if (IsEmpty(documentID) || IsEmpty(resources)) {
        return nil;
    }
    
    NSArray *documents = resources[ADXJSONEventResponseAttributes.resources.documents];
    if (IsEmpty(documents)) {
        return nil;
    }
    
    [documents enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        NSDictionary *document = obj;
        NSString *documentResourceID = document[ADXJSONEventAttributes.id];
        if ([documentResourceID isEqualToString:documentID]) {
            documentResource = document;
            *stop = YES;
        }
    }];
    
    return documentResource;
}

+ (BOOL)updateEntity:(id)entity fromDictionary:(NSDictionary *)dictionary resources:(NSDictionary *)resources context:(NSManagedObjectContext *)moc
{
    static ISO8601DateFormatter *dateFormatter = nil;
    if (!dateFormatter) {
        dateFormatter = [[ISO8601DateFormatter alloc] init];
    }

    if (!entity || IsEmpty(dictionary) || !moc) {
        LogError(@"Received bad params");
        return NO;
    }
    
    ADXEvent *event = entity;
    if (IsEmpty(event.id)) {
        NSString *eventID = [dictionary aps_objectForKeyOrNil:ADXJSONEventAttributes.id];
        if (IsEmpty(eventID)) {
            LogError(@"No event id was supplied in dictionary");
            return NO;
        }
        event.id = eventID;
    }

    NSString *accountID = [dictionary aps_objectForKeyOrNil:ADXEventAttributes.accountID];
    if (IsEmpty(accountID)) {
        LogError(@"Cannot update an event without an account id.");
        return NO;
    }
    
    event.accountID = accountID;

    NSString *originatorID = [dictionary aps_objectForKeyOrNil:ADXJSONEventAttributes.originator];
    event.originatorID = originatorID;
    
    ADXUser *originator = [ADXUser entityWithID:originatorID context:moc];
    if (originator) {
        event.originator = originator;
    } else {
        LogError(@"Could not locate originator id %@ for event id %@. Adding a placeholder entry.", originatorID, event.id);
        
        ADXUser *newUser = [ADXUser addEntityFromDictionary:@{
                            ADXUserAttributes.id: originatorID,
                            ADXUserAttributes.loginName: @"(Unknown)",
                            ADXUserAttributes.primaryEmail: @"(Unknown)"} context:moc];
        event.originator = newUser;
    }
    
    NSString *timestamp = [dictionary aps_objectForKeyOrNil:ADXJSONEventAttributes.timestamp];
    if (IsEmpty(timestamp)) {
        NSString *msg = [NSString stringWithFormat:@"Invalid or missing timestamp on event: %@", dictionary];
        NSAssert(NO, msg);
    }
    event.timeStamp = [dateFormatter dateFromString:timestamp];
    
    event.type = [dictionary aps_objectForKeyOrNil:ADXJSONEventAttributes.eventType];
    event.payload = [dictionary aps_objectForKeyOrNil:ADXJSONEventAttributes.payloadDict];
    
    if ([event.type hasPrefix:ADXEventType.documentPrefix]) {
        NSString *documentID = event.payload[ADXJSONEventAttributes.payload.document];
        NSDictionary *documentResource = [ADXEvent resourceForDocument:documentID resources:resources];
        event.resource = documentResource;
        event.referencedDocumentID = documentID;
    }
    
    LogTrace(@"Updated event: %@", event);
    
    return YES;
}

+ (id)addEntityFromDictionary:(NSDictionary *)dictionary resources:(NSDictionary *)resources context:(NSManagedObjectContext *)moc
{
    if (IsEmpty(dictionary)) {
        LogError(@"Received a nil dictionary.");
        return nil;
    }
    
    NSString *eventID = [dictionary objectForKey:ADXJSONEventAttributes.id];
    if (IsEmpty(eventID)) {
        LogError(@"Received a dictionary with no event id");
        return nil;
    }
    
    ADXEvent *event = [ADXEvent entityWithID:eventID context:moc];
    if (event) {
        LogError(@"Event with id %@ already exists.", eventID);
        return nil;
    }
    
    ADXEvent *newEvent = [ADXEvent insertInManagedObjectContext:moc];
    
    LogTrace(@"Inserted event with id %@", eventID);
    
    BOOL success = [ADXEvent updateEntity:newEvent fromDictionary:dictionary resources:resources context:moc];
    if (success) {
        return newEvent;
    } else {
        return nil;
    }
}

+ (BOOL)addOrUpdateEvent:(NSDictionary *)dictionary resources:(NSDictionary *)resources accountID:(NSString *)accountID context:(NSManagedObjectContext *)moc
{
    BOOL success = NO;
    
    NSString *eventID = [dictionary aps_objectForKeyOrNil:ADXJSONEventAttributes.id];
    ADXEvent *event = [ADXEvent eventWithID:eventID accountID:accountID context:moc];
    NSDictionary *dictionaryWithAccountID = [dictionary aps_dictionaryMergedWithDictionary:@{ADXEventAttributes.accountID : accountID}];
    
    if (!event) {
        event = [ADXEvent addEntityFromDictionary:dictionaryWithAccountID resources:resources context:moc];
        success = event ? YES : NO;
    } else {
        success = [ADXEvent updateEntity:event fromDictionary:dictionaryWithAccountID resources:resources context:moc];
    }
    
    return success;
}

+ (BOOL)updateEvents:(NSArray *)events resources:(NSDictionary *)resources accountID:(NSString *)accountID filterOutEventsFromAccount:(NSString *)filterAccountID onlyEventTypes:(NSSet *)eventTypes context:(NSManagedObjectContext *)moc
{
    BOOL success = YES;
    
    if (IsEmpty(events)) {
        LogError(@"Received a nil/empty array");
        return NO;
    }
    
    for (NSDictionary *dictionary in events) {
        NSString *eventID = [dictionary aps_objectForKeyOrNil:ADXJSONEventAttributes.id];
        NSString *originatorID = [dictionary aps_objectForKeyOrNil:ADXJSONEventAttributes.originator];
        NSString *eventType = [dictionary aps_objectForKeyOrNil:ADXJSONEventAttributes.eventType];
        
        if (filterAccountID && [originatorID isEqualToString:filterAccountID]) {
            continue;
        }
        
        if (eventTypes && ![eventTypes containsObject:eventType]) {
            continue;
        }
        
        if (IsEmpty(eventID)) {
            LogError(@"Received a dictionary with no event ID");
            return NO;
        }
        
        BOOL updateSuccess = [ADXEvent addOrUpdateEvent:dictionary resources:resources accountID:(NSString *)accountID context:moc];
        
        if (!updateSuccess) {
            success = NO;
            break;
        }
    }
    
    return success;
}

- (NSString *)shortDescriptionFormat
{
    NSString *key = [NSString stringWithFormat:@"short.%@", self.type];
    NSString *format = NSLocalizedString(key, nil);
    return format;
}

- (NSString *)fullDescriptionFormat
{
    NSString *key = [NSString stringWithFormat:@"full.%@", self.type];
    NSString *format = NSLocalizedString(key, nil);
    return format;
}

- (NSString *)descriptionWithFormat:(NSString *)format
{
    NSString *result = nil;
    NSError *error = nil;
    static NSString *unknown = @"unknown";
    
    NSString *versionID = [(NSNumber *)self.payload[ADXJSONEventAttributes.payload.version] stringValue];
    if (IsEmpty(versionID)) {
        versionID = unknown;
    }
    NSString *documentTitle = self.resource[ADXJSONDocumentAttributes.title];
    if (IsEmpty(documentTitle)) {
        documentTitle = unknown;
    }
    NSString *originator = [self.originator displayNameOrLoginName];
    if (IsEmpty(originator)) {
        originator = unknown;
    }
    
    GRMustacheTemplate *template = [GRMustacheTemplate templateFromString:format error:&error];
    NSDictionary *params = @{@"document": documentTitle, @"originator": originator, @"version": versionID};
    
    result = [template renderObject:params error:&error];
    return result;
}

- (NSString *)shortDescription
{
    NSString *result = [self descriptionWithFormat:[self shortDescriptionFormat]];
    return result;
}

- (NSString *)fullDescription
{
    NSString *result = [self descriptionWithFormat:[self fullDescriptionFormat]];
    return result;
}

@end
