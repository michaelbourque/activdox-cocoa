// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to ADXDocumentThumbnail.h instead.

#import <CoreData/CoreData.h>



extern const struct ADXDocumentThumbnailAttributes {
	__unsafe_unretained NSString *data;
} ADXDocumentThumbnailAttributes;



extern const struct ADXDocumentThumbnailRelationships {
	__unsafe_unretained NSString *document;
} ADXDocumentThumbnailRelationships;






@class ADXDocument;




@interface ADXDocumentThumbnailID : NSManagedObjectID {}
@end

@interface _ADXDocumentThumbnail : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (ADXDocumentThumbnailID*)objectID;





@property (nonatomic, strong) NSData* data;



//- (BOOL)validateData:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) ADXDocument *document;

//- (BOOL)validateDocument:(id*)value_ error:(NSError**)error_;





@end



@interface _ADXDocumentThumbnail (CoreDataGeneratedPrimitiveAccessors)


- (NSData*)primitiveData;
- (void)setPrimitiveData:(NSData*)value;





- (ADXDocument*)primitiveDocument;
- (void)setPrimitiveDocument:(ADXDocument*)value;


@end
