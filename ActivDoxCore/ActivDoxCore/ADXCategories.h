//
//  ADXCategories.h
//  ActivDoxCore
//
//  Created by Gavin McKenzie on 12-05-09.
//  Copyright (c) 2012 ActivDox Inc. All rights reserved.
//

#import "NSString+ActivDox.h"
#import "NSData+ActivDox.h"

#import "NSDictionary+AngryPumpkin.h"
#import "NSManagedObjectContext+AngryPumpkin.h"

#import "NSDate+NaturalDates.h"
#import "NSDate+Helper.h"

#import "NSData+Base64.h"