//
//  ADXLoginProcessor.m
//  ActivDoxCore
//
//  Created by Gavin McKenzie on 2012-08-03.
//  Copyright (c) 2012 ActivDox Inc. All rights reserved.
//

#import "ADXLoginProcessor.h"
#import "ADXCommonMacros.h"
#import "ADXV2Service.h"
#import "ADXUserDefaults.h"

@interface ADXLoginProcessor ()
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (strong, nonatomic) ADXV2Service *service;
@property (strong, nonatomic) ADXAccount *currentAccount;
@property (strong, nonatomic) NSString *stashedLoginName;
@property (strong, nonatomic) NSString *stashedPassword;
@property (strong, nonatomic) NSString *stashedServer;
@property (strong, nonatomic) NSString *stashedAccessToken;
@property (strong, nonatomic) NSString *stashedAccountID;
@property (assign, nonatomic) BOOL stashedRememberPassword;
- (void)startServiceWithAccount:(ADXAccount *)account completion:(ADXLoginPerformedCompletionBlock)completionBlock;
@end

@implementation ADXLoginProcessor

#pragma mark - Initializers

- (id)initWithParentManagedObjectContext:(NSManagedObjectContext *)context
{
    self = [super init];
    if (self) {
        _managedObjectContext = context;
    }
    return self;
}

#pragma mark - Destruction

- (void)dealloc
{
    //
}

#pragma mark - Accessors

- (ADXAccount *)currentAccount
{
    if (!_currentAccount) {
        ADXAccount *defaultAccount = [self defaultAccount];
        if (defaultAccount) {
            _currentAccount = defaultAccount;
        }
    }
    return _currentAccount;
}

#pragma mark - Helpers

- (void)resetStashedData
{
    self.stashedLoginName = nil;
    self.stashedPassword = nil;
    self.stashedRememberPassword = NO;
    self.stashedServer = nil;
    self.stashedAccessToken = nil;
}

- (ADXAccount *)defaultAccount
{
    __weak NSManagedObjectContext *moc = self.managedObjectContext;
    __block ADXAccount *account = nil;

    [moc performBlockAndWait:^{
        account = [ADXAccount mostRecentlyUsedAccountInContext:moc];
        if (!account) {
            account = [[ADXAccount allAccountsInContext:moc] lastObject];
        }
    }];
    return account;
}

- (NSURL *)serverURLFromString:(NSString *)server
{
    NSURL *serverURL = nil;
    
    NSRange range = [server rangeOfString:@"://"];
    if (range.location == NSNotFound) {
        server = [NSString stringWithFormat:@"https://%@", server];
    }

    serverURL = [NSURL URLWithString:server];
    return serverURL;
}

#pragma mark - Login

- (void)handleOnlineLoginSuccessWithCompletionBlock:(ADXLoginPerformedCompletionBlock)completionBlock
{
    __weak ADXLoginProcessor *weakSelf = self;
    __weak NSManagedObjectContext *moc = self.managedObjectContext;
    
    __block ADXAccount *account = nil;
    __block BOOL contextSaveSuccess = NO;
    __block NSError *contextSaveError = nil;
    
    NSString *accountID = self.service.loginResponseAccountID;
    NSString *displayName = self.service.loginResponse[ADXUserAttributes.displayName];
    NSString *primaryEmail = self.service.loginResponse[ADXUserAttributes.primaryEmail];
    
    if (IsEmpty(accountID)) {
        LogError(@"Couldn't retrieve account ID from login response. Aborting login attempt.");
        if (completionBlock) {
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                completionBlock(NO, NO, nil, nil, nil);
            }];
        }
        return;
    }
    
    [moc performBlockAndWait:^{
        account = [ADXAccount entityWithID:accountID context:moc];
        if (!account) {
            account = [ADXAccount insertInManagedObjectContext:moc];
            if (!account) {
                LogError(@"Couldn't create new account in local store. Aborting login attempt.");
                if (completionBlock) {
                    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                        completionBlock(NO, NO, nil, nil, nil);
                    }];
                }
                return;
            }
        }
                
        ADXUser *user = [ADXUser entityWithID:accountID context:moc];
        if (!user) {
            user = [ADXUser insertInManagedObjectContext:moc];
            if (!user) {
                LogError(@"Couldn't create new user for account in local store. Aborting login attempt.");
                if (completionBlock) {
                    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                        completionBlock(NO, NO, nil, nil, nil);
                    }];
                }
                return;
            }
            user.id = accountID;
            user.loginName = weakSelf.stashedLoginName;
            user.createdAt = [NSDate date];
        }

        account.user = user;

        user.displayName = displayName;
        user.primaryEmail = primaryEmail;

        account.loginName = weakSelf.stashedLoginName;
        account.id = accountID;
        account.serverURL = weakSelf.stashedServer;
        account.rememberPasswordValue = weakSelf.stashedRememberPassword;
        
        if (weakSelf.stashedRememberPassword) {
            account.password = weakSelf.stashedPassword;
            account.accessToken = weakSelf.stashedAccessToken;
        } else {
            account.password = nil;
            account.accessToken = nil;
        }
        
        account.mostRecentLogin = [NSDate date];
        
        contextSaveSuccess = [moc save:&contextSaveError];
        if (!contextSaveSuccess) {
            LogError(@"Couldn't save user and/or account in local store after login. Aborting login attempt.");
            if (completionBlock) {
                [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                    completionBlock(NO, NO, nil, nil, contextSaveError);
                }];
            }
            return;
        }
    }];

    self.currentAccount = account;
    if (!self.currentAccount || IsEmpty(self.currentAccount.id)) {
        LogError(@"Account doesn't appear to be in a valid state: %@. Aborting login attempt.", self.currentAccount);
        if (completionBlock) {
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                completionBlock(NO, NO, nil, nil, nil);
            }];
        }
        return;
    }
    
    [ADXV2Service registerService:self.service forAccountID:self.currentAccount.id];
    
    [self startServiceWithAccount:account completion:^(BOOL success, BOOL online, ADXAccount *account, ADXV2Service *service, NSError *error) {
        if (success && online && !IsEmpty([account id])) {
            [weakSelf.service refreshCollections:^(BOOL success, NSError *error) {
                if (success) {
                    [weakSelf.service refreshDocuments:^(BOOL success, NSError *error) {
                        if (!success) {
                            LogError(@"A problem occurred performing the initial documents refresh. Continuing with login process anyway.");
                            LogNSError(@"Initial documents refresh failed with error:", error);
                        }
                        if (completionBlock) {
                            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                                completionBlock(YES, YES, account, service, nil);
                            });
                        }
                    } downloadContent:YES trackProgress:YES];
                } else {
                    if (completionBlock) {
                        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                            completionBlock(success, YES, account, service, nil);
                        });
                    }
                }
            }];
        } else {
            // Assume we're in offline mode. Return success with offline qualifier.
            if (completionBlock) {
                [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                    completionBlock(YES, NO, weakSelf.currentAccount, weakSelf.service, nil);
                }];
            }
        }
    }];
}

- (void)handleOfflineLoginSuccessWithAccount:(ADXAccount *)account completionBlock:(ADXLoginPerformedCompletionBlock)completionBlock
{
    __weak ADXLoginProcessor *weakSelf = self;
    __weak NSManagedObjectContext *moc = self.managedObjectContext;
    
    __block BOOL contextSaveSuccess = NO;
    __block NSError *contextSaveError = nil;

    [moc performBlock:^{
        if (!account || IsEmpty(account.id)) {
            LogInfo(@"No account received, which is required for OFFLINE mode. Aborting login.");
            if (completionBlock) {
                [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                    completionBlock(NO, NO, nil, nil, nil);
                }];
            }
            return;
        }

        if (!weakSelf.stashedRememberPassword) {
            account.password = nil;
            account.accessToken = nil;
        }

        account.mostRecentLogin = [NSDate date];
        
        contextSaveSuccess = [moc save:&contextSaveError];
        if (!contextSaveSuccess) {
            LogError(@"Couldn't save user and/or account in local store after updating during OFFLINE login processing. Aborting login attempt.");
            if (completionBlock) {
                [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                    completionBlock(NO, NO, nil, nil, contextSaveError);
                }];
            }
            return;
        }
    }];

    self.currentAccount = account;
    
    if (!self.currentAccount || IsEmpty(self.currentAccount.id)) {
        LogError(@"Account doesn't appear to be in a valid state during OFFLINE login processing: %@. Aborting login attempt.", self.currentAccount);
        if (completionBlock) {
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                completionBlock(NO, NO, nil, nil, nil);
            }];
        }
        return;
    }
    
    [ADXV2Service registerService:self.service forAccountID:self.currentAccount.id];
    
    if ([ADXV2Service serviceForAccountID:self.currentAccount.id] != self.service) {
        LogError(@"Accoung registration failed during OFFLINE login processing: %@. Aborting login attempt.", self.currentAccount);
        if (completionBlock) {
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                completionBlock(NO, NO, nil, nil, nil);
            }];
        }
        return;
    }
    
    [self.service startServiceForAccount:account password:self.stashedPassword completion:^(BOOL success, NSError *error) {
        // Return success with offline qualifier.
        if (completionBlock) {
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                completionBlock(YES, NO, weakSelf.currentAccount, weakSelf.service, nil);
            }];
        }
    }];
    
}

- (void)performLoginWithName:(NSString *)loginName password:(NSString *)password server:(NSString *)server rememberPassword:(BOOL)rememberPassword completion:(ADXLoginPerformedWithStatusCompletionBlock)completionBlock
{
    __weak ADXLoginProcessor *weakSelf = self;
    
    [self resetStashedData];
    self.stashedLoginName = loginName;
    self.stashedPassword = password;
    self.stashedRememberPassword = rememberPassword;
    self.stashedServer = server;
        
    self.service = [ADXV2Service createServiceForServerURL:[self serverURLFromString:server] parentManagedObjectContext:self.managedObjectContext];
    [self.service getAccessTokenForLoginName:loginName password:password completion:^(NSUInteger statusCode, BOOL success, NSError *error) {
        if (success) {
            weakSelf.stashedAccessToken = weakSelf.service.accessToken;
            [weakSelf handleOnlineLoginSuccessWithCompletionBlock:^(BOOL success, BOOL online, ADXAccount *account, ADXV2Service *service, NSError *error) {
                [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                    completionBlock(success, statusCode, online, account, service, error);
                }];
            }];
        } else {
            if (statusCode == 401 || statusCode == 403) {
                [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                    completionBlock(NO, statusCode, YES, nil, weakSelf.service, error);
                }];
            } else {
                LogInfo(@"Couldn't get access token. Attempting OFFLINE mode...");
                // Attempt an offline authentication
                [weakSelf performOfflineLoginWithName:loginName password:password server:server rememberPassword:rememberPassword completion:^(BOOL success, BOOL online, ADXAccount *account, ADXV2Service *service, NSError *error) {
                    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                        completionBlock(success, statusCode, online, account, service, error);
                    }];
                }];
            }
        }
    }];
}

- (void)performOfflineLoginWithName:(NSString *)loginName password:(NSString *)password server:(NSString *)server rememberPassword:(BOOL)rememberPassword completion:(ADXLoginPerformedCompletionBlock)completionBlock
{
    __weak ADXLoginProcessor *weakSelf = self;
    __weak NSManagedObjectContext *moc = self.managedObjectContext;
    
    [moc performBlock:^{
        ADXAccount *account = [ADXAccount accountWithLoginName:loginName password:password serverURL:server context:moc];
        if (account) {
            weakSelf.stashedAccountID = account.id;
            [weakSelf handleOfflineLoginSuccessWithAccount:account completionBlock:completionBlock];
        } else {
            if (completionBlock) {
                [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                    completionBlock(NO, NO, nil, weakSelf.service, nil);
                }];
            }
        }
    }];
}

- (void)performLoginWithAcccount:(ADXAccount *)account completion:(ADXLoginPerformedWithStatusCompletionBlock)completionBlock
{
    NSAssert(account, @"performLoginWithAccount received nil account");
    
    [self performLoginWithName:account.loginName
                      password:account.password
                        server:account.serverURL
              rememberPassword:account.rememberPasswordValue
                    completion:completionBlock];
}

- (void)testLoginWithName:(NSString *)loginName password:(NSString *)password server:(NSString *)server completion:(ADXLoginTestedCompletionBlock)completionBlock
{   
    [self resetStashedData];
    self.stashedLoginName = loginName;
    self.stashedPassword = password;
    self.stashedServer = server;
    
    self.service = [ADXV2Service createServiceForServerURL:[self serverURLFromString:server] parentManagedObjectContext:self.managedObjectContext];
    [self.service getAccessTokenForLoginName:loginName password:password completion:^(NSUInteger statusCode, BOOL success, NSError *error) {
        if (error) {
            if (completionBlock) {
                [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                    completionBlock(NO, error);
                }];
            }
        } else {
            if (completionBlock) {
                [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                    completionBlock(YES, nil);
                }];
            }
        }
    }];
}

- (void)startServiceWithAccount:(ADXAccount *)account completion:(ADXLoginPerformedCompletionBlock)completionBlock
{
    __weak ADXLoginProcessor *weakSelf = self;
    
    [self.service startServiceForAccount:account password:self.stashedPassword completion:^(BOOL success, NSError *error) {
        BOOL serviceStartedOk = error ? NO : YES;
        BOOL online = (weakSelf.service.networkReachable && weakSelf.service.serverReachable);

        // If service didn't start, return with an error.
        if (!serviceStartedOk) {
            LogNSError(@"startServiceWithAccount could not start service", error);
            if (completionBlock) {
                [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                    completionBlock(NO, online, nil, nil, error);
                }];
            }
        } else {
            // Everything is great. Return success;
            if (completionBlock) {
                [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                    completionBlock(YES, online, weakSelf.currentAccount, weakSelf.service, nil);
                }];
            }
        }
    }];
}

#pragma mark - Registration

- (void)registerWithName:(NSString *)loginName
                   email:(NSString *)email
                password:(NSString *)password
                  server:(NSString *)server
              completion:(ADXRegistrationPerformedCompletionBlock)completionBlock;
{
    [self resetStashedData];
    self.stashedLoginName = loginName;
    self.stashedPassword = password;
    self.stashedServer = server;
    
    self.service = [ADXV2Service createServiceForServerURL:[self serverURLFromString:server] parentManagedObjectContext:self.managedObjectContext];
    
    [self.service registerUserWithLoginName:loginName email:email password:password completion:^(id response, BOOL success, NSError *error) {
        ADXLoginProcessorRegistrationResponse status = ADXLoginProcessorRegistrationFailed;
        if ([response isKindOfClass:[NSNumber class]]) {
            NSInteger responseStatus = [(NSNumber *)response integerValue];
            if (responseStatus == 201) { // Account ready for login
                status = ADXLoginProcessorRegistrationAcceptedAndReadyForLogin;
            } else if (responseStatus == 202) { // Account not ready for login; awaiting confirmation.
                status = ADXLoginProcessorRegistrationAcceptedForConfirmation;
            }
        }
        if (status == ADXLoginProcessorRegistrationFailed || !success) {
            if (completionBlock) {
                [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                    completionBlock(ADXLoginProcessorRegistrationFailed, error);
                }];
            }
        } else {
            if (completionBlock) {
                [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                    completionBlock(status, nil);
                }];
            }
        }
    }];
}

- (void)performPasswordResetWithServer:(NSString *)server
                                 email:(NSString *)email
                            completion:(ADXPasswordResetCompletionBlock)completion
{
    NSURL *serverURL = [self serverURLFromString:server];
    ADXV2Service *service = [ADXV2Service createServiceForServerURL:serverURL
                                parentManagedObjectContext:self.managedObjectContext];
    
    [service resetPasswordForEmail:email completion:^(id response, BOOL success, NSError *error) {
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            completion(success, error);
        }];
    }];
}

@end
