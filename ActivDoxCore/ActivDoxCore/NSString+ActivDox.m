//
//  NSString+ActivDox.m
//  ActivDoxCore
//
//  Created by Gavin McKenzie on 12-05-07.
//  Copyright (c) 2012 ActivDox Inc. All rights reserved.
//

#import "NSString+ActivDox.h"

@implementation NSString (ActivDox)

+ (NSString *)adx_generateUUID
{
    CFUUIDRef uuidObj = CFUUIDCreate(nil);
    NSString *uuidString = (__bridge_transfer NSString*)CFUUIDCreateString(nil, uuidObj);
    CFRelease(uuidObj);
    return uuidString;
}

@end
