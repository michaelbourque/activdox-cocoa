// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to ADXEventQueueEntry.m instead.

#import "_ADXEventQueueEntry.h"


const struct ADXEventQueueEntryAttributes ADXEventQueueEntryAttributes = {
	.accountID = @"accountID",
	.payload = @"payload",
	.timeStamp = @"timeStamp",
	.type = @"type",
};








@implementation ADXEventQueueEntryID
@end

@implementation _ADXEventQueueEntry

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"ADXEventQueueEntry" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"ADXEventQueueEntry";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"ADXEventQueueEntry" inManagedObjectContext:moc_];
}

- (ADXEventQueueEntryID*)objectID {
	return (ADXEventQueueEntryID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
	

	return keyPaths;
}




@dynamic accountID;






@dynamic payload;






@dynamic timeStamp;






@dynamic type;











@end




