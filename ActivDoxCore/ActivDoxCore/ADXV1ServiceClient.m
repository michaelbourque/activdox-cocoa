//
//  ADXV1ServiceClient.m
//  ActivDoxCore
//
//  Created by Gavin McKenzie on 12-06-05.
//  Copyright (c) 2012 ActivDox Inc. All rights reserved.
//

#import "ADXV1ServiceClient.h"
#import "ADXCommonMacros.h"
#import "ADXHTTPClient.h"
#import "ADXService.h"
#import "ADXPDFRequestOperation.h"
#import "GRMustache.h"
#import "MMMultipartForm.h"

#import "ADXJSONRequestOperation.h"

NSString * const kADXV1ServiceAPIVersion = @"1";
NSString * const kADXV1ServiceReceivedDocumentsLiteral = @"received";
NSString * const kADXV1ServicePublishedDocumentsLiteral = @"published";
NSString * const kADXV1ServiceDocumentContentLiteral = @"contents";
NSString * const kADXV1ServiceUserMeLiteral = @"me";

NSString * const kADXV1ServiceJSONKeyID = @"id";

static NSString * const kADXV1TemplateKeyVersionID = @"versionID";
static NSString * const kADXV1TemplateKeyContainerType = @"containerType";
static NSString * const kADXV1TemplateKeyHasFolderID = @"hasFolderID";
static NSString * const kADXV1TemplateKeyFolderID = @"folderID";
static NSString * const kADXV1TemplateKeyDocumentID = @"documentID";
static NSString * const kADXV1TemplateKeySince = @"since";
static NSString * const kADXV1TemplateKeyUserID = @"userID";
static NSString * const kADXV1TemplateKeyNoteID = @"noteID";
static NSString * const kADXV1TemplateKeyNewAPNSToken = @"apnsToken";
static NSString * const kADXV1TemplateKeyOldAPNSToken = @"apnsOldToken";
static NSString * const kADXV1TemplateKeyHasOldAPNSToken = @"hasOldAPNSToken";
static NSString * const kADXV1TemplateKeyHasVersion = @"hasVersion";
static NSString * const kADXV1TemplateKeyVersion = @"version";
static NSString * const kADXV1TemplateKeyAccessToken = @"accessToken";
static NSString * const kADXV1TemplateKeyEventID = @"eventID";
static NSString * const kADXV1TemplateKeyEventSinceID = @"since";
static NSString * const kADXV1TemplateKeyEventType = @"type";
static NSString * const kADXV1TemplateKeyEventLimit = @"limit";
static NSString * const kADXV1TemplateKeyPrev = @"prev";

static NSString * const kADXV1TemplateGetContainer = @"/{{versionID}}/users/{{userID}}/{{containerType}}{{#hasFolderID}}/{{folderID}}{{/hasFolderID}}";
static NSString * const kADXV1TemplateGetContainerSince = @"/{{versionID}}/users/{{userID}}/{{containerType}}{{#hasFolderID}}/{{folderID}}{{/hasFolderID}}?since={{since}}";
static NSString * const kADXV1TemplateGetDocumentContent = @"/{{versionID}}/users/{{userID}}/{{containerType}}/documents/{{documentID}}/content";
static NSString * const kADXV1TemplateGetDocumentVersionContent = @"/{{versionID}}/users/{{userID}}/{{containerType}}/documents/{{documentID}}/versions/{{version}}/content";
static NSString * const kADXV1TemplateGetDocumentVersions = @"/{{versionID}}/users/{{userID}}/{{containerType}}/documents/{{documentID}}/versions";
static NSString * const kADXV1TemplateGetHighlightedDocumentContent = @"/{{versionID}}/users/{{userID}}/{{containerType}}/documents/{{documentID}}/differences";
static NSString * const kADXV1TemplateGetHighlightedDocumentVersionContent = @"/{{versionID}}/users/{{userID}}/{{containerType}}/documents/{{documentID}}/versions/{{version}}/differences";
static NSString * const kADXV1TemplateGetDocumentThumbnail = @"/{{versionID}}/users/{{userID}}/{{containerType}}/documents/{{documentID}}/thumbnail";
static NSString * const kADXV1TemplateGetDocumentVersionThumbnail = @"/{{versionID}}/users/{{userID}}/{{containerType}}/documents/{{documentID}}/versions/{{version}}/thumbnail";
static NSString * const kADXV1TemplateGetDocumentNotes = @"/{{versionID}}/users/{{userID}}/{{containerType}}/documents/{{documentID}}/notes";
static NSString * const kADXV1TemplateGetDocumentVersionNotes = @"/{{versionID}}/users/{{userID}}/{{containerType}}/documents/{{documentID}}/versions/{{version}}/notes";
static NSString * const kADXV1TemplateAddDocumentVersionNote = @"/{{versionID}}/users/{{userID}}/{{containerType}}/documents/{{documentID}}/versions/{{version}}/notes";
static NSString * const kADXV1TemplateDeleteDocumentNote = @"/{{versionID}}/users/{{userID}}/{{containerType}}/documents/{{documentID}}/versions/{{version}}/notes/{{noteID}}";
static NSString * const kADXV1TemplateDeleteDocumentVersionNote = @"/{{versionID}}/users/{{userID}}/{{containerType}}/documents/{{documentID}}/versions/{{version}}/notes/{{noteID}}";
static NSString * const kADXV1TemplateUpdateDocumentVersionNote = @"/{{versionID}}/users/{{userID}}/{{containerType}}/documents/{{documentID}}/versions/{{version}}/notes/{{noteID}}";
static NSString * const kADXV1TemplateGetDocumentChanges = @"/{{versionID}}/users/{{userID}}/{{containerType}}/documents/{{documentID}}/versions/{{version}}/changes/{{prev}}";
static NSString * const kADXV1TemplateLoginPath = @"/{{versionID}}/users/login";
static NSString * const kADXV1TemplateRegistrationPath = @"/{{versionID}}/users/register";
#if (!TARGET_OS_IPHONE)
static NSString * const kADXV1TemplatePutAPNSToken = @"/{{versionID}}/users/{{userID}}/properties?apnsMacToken={{apnsToken}}{{#hasOldAPNSToken}}&apnsOldMacToken={{oldAPNSToken}}{{/hasOldAPNSToken}}";
#else
static NSString * const kADXV1TemplatePutAPNSToken = @"/{{versionID}}/users/{{userID}}/properties?apnsToken={{apnsToken}}{{#hasOldAPNSToken}}&apnsOldToken={{oldAPNSToken}}{{/hasOldAPNSToken}}";
#endif
static NSString * const kADXV1TemplateUploadDocument = @"/{{versionID}}/users/me/published/documents?accessToken={{accessToken}}";
static NSString * const kADXV1TemplateShareDocument = @"/{{versionID}}/users/{{userID}}/received/documents/{{documentID}}/shares";
static NSString * const kADXV1TemplateGetUserEvent = @"/{{versionID}}/users/{{userID}}/events/{{eventID}}";
static NSString * const kADXV1TemplateGetEvent = @"/{{versionID}}/users/events/{{eventID}}";
static NSString * const kADXV1TemplateGetEvents = @"/{{versionID}}/users/{{userID}}/events";
static NSString * const kADXV1TemplatePostEvents = @"/{{versionID}}/users/{{userID}}/events";

@interface ADXV1ServiceClient ()
@property (strong, nonatomic) ADXHTTPClient *httpClient;
@property (strong, nonatomic) ADXHTTPClient *downloadClient;
@property (strong, readwrite, nonatomic) NSDictionary *loginResponse;
@property (strong, nonatomic) NSCache *pathCache;
- (void)setupHTTPClientsWithBaseURL:(NSURL *)baseURL userID:(NSString *)userID;
@end

@implementation ADXV1ServiceClient
@synthesize httpClient = _httpClient;
@synthesize downloadClient = _downloadClient;
@synthesize loginResponse = _loginResponse;
@synthesize userID = _userID;

#pragma mark - Initialization

- (id)initWithBaseURL:(NSURL *)baseURL userID:(NSString *)userID
{
    self = [super init];
    if (self) {
        [self setupHTTPClientsWithBaseURL:baseURL userID:userID];
        
        _pathCache = [NSCache new];
        if (IsEmpty(userID)) {
            _userID = kADXV1ServiceUserMeLiteral;
        } else {
            _userID = [userID copy];
        }
    }
    return self;
}

- (void)setupHTTPClientsWithBaseURL:(NSURL *)baseURL userID:(NSString *)userID
{
    _httpClient = [[ADXHTTPClient alloc] initWithBaseURL:baseURL];
    [_httpClient registerHTTPOperationClass:[ADXJSONRequestOperation class]];
    [_httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    
    _downloadClient = [[ADXHTTPClient alloc] initWithBaseURL:baseURL];
    [_downloadClient.operationQueue setMaxConcurrentOperationCount:2];
    [_downloadClient registerHTTPOperationClass:[ADXPDFRequestOperation class]];
    [_downloadClient registerHTTPOperationClass:[AFImageRequestOperation class]];
    [_downloadClient setDefaultHeader:@"Accept" value:@"application/pdf,image/png"];
}


#pragma mark - Accessors

- (NSURL *)baseURL
{
    return self.httpClient.baseURL;
}

- (NSString *)version
{
    return kADXV1ServiceAPIVersion;
}

- (GRMustacheTemplate *)pathTemplateForKey:(NSString *)key
{
    GRMustacheTemplate *result = [self.pathCache objectForKey:key];
    
    if (!result) {
        NSError *error = nil;
        result = [GRMustacheTemplate templateFromString:key error:&error];
        if (result) {
            [self.pathCache setObject:result forKey:key];
        } else {
            LogError(@"pathTemplateForKey failed on key %@", key);
            LogNSError(@"pathTemplateForKey error:", error);
        }
    }
    
    return result;
}

#pragma mark - Reachability

- (AFNetworkReachabilityStatus)networkReachabilityStatus
{
    return [self.httpClient networkReachabilityStatus];
}

- (void)setReachabilityStatusChangeBlock:(void (^)(AFNetworkReachabilityStatus status))block
{
    [self.httpClient setReachabilityStatusChangeBlock:block];
}

#pragma mark - URL Construction

- (NSString *)pathForLogin
{
    NSString *result = nil;
    GRMustacheTemplate *loginTemplate = [self pathTemplateForKey:kADXV1TemplateLoginPath];
    NSAssert(loginTemplate, @"pathForLogin couldn't generate template");
    
    result = [loginTemplate renderObject:[NSDictionary dictionaryWithObject:kADXV1ServiceAPIVersion forKey:kADXV1TemplateKeyVersionID]];
    return result;
}

- (NSString *)pathForUserRegistration
{
    NSString *result = nil;
    GRMustacheTemplate *registerTemplate = [self pathTemplateForKey:kADXV1TemplateRegistrationPath];
    NSAssert(registerTemplate, @"pathForUserRegistration couldn't generate template");

    result = [registerTemplate renderObject:[NSDictionary dictionaryWithObject:kADXV1ServiceAPIVersion forKey:kADXV1TemplateKeyVersionID]];
    return result;
}

- (NSString *)pathForGetContainer:(NSString *)containerType folder:(NSString *)folderID since:(NSDate *)since user:(NSString *)userID
{
    NSString *result = nil;
    GRMustacheTemplate *getContainerTemplate = nil;
    GRMustacheTemplate *getContainerTemplateSince = nil;
    NSString *sinceDateISO8601 = nil;
    
    if (since) {
        getContainerTemplateSince = [self pathTemplateForKey:kADXV1TemplateGetContainerSince];
        NSAssert(getContainerTemplateSince, @"pathForGetContainer couldn't generate template");
        
        ISO8601DateFormatter *dateFormatter = nil;
        if (!dateFormatter) {
            dateFormatter = [[ISO8601DateFormatter alloc] init];
        }
        dateFormatter.includeTime = YES;
        sinceDateISO8601 = [dateFormatter stringFromDate:since];
    } else {
        getContainerTemplate = [self pathTemplateForKey:kADXV1TemplateGetContainer];
        NSAssert(getContainerTemplate, @"pathForGetContainer couldn't generate template");
    }
    
    if (IsEmpty(containerType) || IsEmpty(userID)) {
        LogError(@"received nil arguments");
        return nil;
    }
    
    NSDictionary *dictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                kADXV1ServiceAPIVersion, kADXV1TemplateKeyVersionID,
                                userID, kADXV1TemplateKeyUserID,
                                containerType, kADXV1TemplateKeyContainerType,
                                [NSNumber numberWithBool:(!IsEmpty(folderID))], kADXV1TemplateKeyHasFolderID,
                                (folderID ? folderID : @""), kADXV1TemplateKeyFolderID,
                                (sinceDateISO8601 ? sinceDateISO8601 : @""), kADXV1TemplateKeySince,
                                nil];
    
    if (since) {
        result = [getContainerTemplateSince renderObject:dictionary];
    } else {
        result = [getContainerTemplate renderObject:dictionary];
    }
    
    return result;
}

- (NSString *)pathForGetHighlightedDocumentContent:(NSString *)containerType version:(NSUInteger)version documentID:(NSString *)documentID user:(NSString *)userID
{
    NSString *result = nil;
    GRMustacheTemplate *getHighlightedDocumentContentTemplate = nil;
    GRMustacheTemplate *getHighlightedDocumentVersionContentTemplate = nil;
    
    if (version > 0) {
        getHighlightedDocumentVersionContentTemplate = [self pathTemplateForKey:kADXV1TemplateGetHighlightedDocumentVersionContent];
        NSAssert(getHighlightedDocumentVersionContentTemplate, @"pathForGetHighlightedDocumentContent couldn't generate template");
    } else {
        if (!getHighlightedDocumentContentTemplate) {
            getHighlightedDocumentContentTemplate = [self pathTemplateForKey:kADXV1TemplateGetHighlightedDocumentContent];
            NSAssert(getHighlightedDocumentContentTemplate, @"pathForGetHighlightedDocumentContent couldn't generate template");
        }
    }
    
    if (IsEmpty(containerType) || IsEmpty(userID)) {
        LogError(@"received nil arguments");
        return nil;
    }
    
    NSDictionary *dictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                kADXV1ServiceAPIVersion, kADXV1TemplateKeyVersionID,
                                userID, kADXV1TemplateKeyUserID,
                                containerType, kADXV1TemplateKeyContainerType,
                                documentID, kADXV1TemplateKeyDocumentID,
                                [NSString stringWithFormat:@"%d", (int)version], kADXV1TemplateKeyVersion,
                                nil];

    if (version > 0) {
        result = [getHighlightedDocumentVersionContentTemplate renderObject:dictionary];
    } else {
        result = [getHighlightedDocumentContentTemplate renderObject:dictionary];
    }
    
    return result;
}

- (NSString *)pathForGetDocumentContent:(NSString *)containerType version:(NSUInteger)version documentID:(NSString *)documentID user:(NSString *)userID
{
    NSString *result = nil;
    GRMustacheTemplate *getDocumentContentTemplate = nil;
    GRMustacheTemplate *getDocumentVersionContentTemplate = nil;
    
    if (version > 0) {
        getDocumentVersionContentTemplate = [self pathTemplateForKey:kADXV1TemplateGetDocumentVersionContent];
        NSAssert(getDocumentVersionContentTemplate, @"pathForGetDocumentContent couldn't generate template");
    } else {
        if (!getDocumentContentTemplate) {
            getDocumentContentTemplate = [self pathTemplateForKey:kADXV1TemplateGetDocumentContent];
            NSAssert(getDocumentContentTemplate, @"pathForGetDocumentContent couldn't generate template");
        }
    }
    
    if (IsEmpty(containerType) || IsEmpty(userID)) {
        LogError(@"received nil arguments");
        return nil;
    }
    
    NSDictionary *dictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                kADXV1ServiceAPIVersion, kADXV1TemplateKeyVersionID,
                                userID, kADXV1TemplateKeyUserID,
                                containerType, kADXV1TemplateKeyContainerType,
                                documentID, kADXV1TemplateKeyDocumentID,
                                [NSString stringWithFormat:@"%d", (int)version], kADXV1TemplateKeyVersion,
                                nil];

    if (version > 0) {
        result = [getDocumentVersionContentTemplate renderObject:dictionary];
    } else {
        result = [getDocumentContentTemplate renderObject:dictionary];
    }
    
    return result;
}

- (NSString *)pathForGetDocumentThumbnail:(NSString *)containerType version:(NSUInteger)version documentID:(NSString *)documentID user:(NSString *)userID
{
    NSString *result = nil;
    GRMustacheTemplate *getDocumentThumbnailTemplate = nil;
    GRMustacheTemplate *getDocumentVersionThumbnailTemplate = nil;
    
    if (version > 0) {
        getDocumentVersionThumbnailTemplate = [self pathTemplateForKey:kADXV1TemplateGetDocumentVersionThumbnail];
        NSAssert(getDocumentVersionThumbnailTemplate, @"pathForGetDocumentThumbnail couldn't generate template");
    } else {
        getDocumentThumbnailTemplate = [self pathTemplateForKey:kADXV1TemplateGetDocumentThumbnail];
        NSAssert(getDocumentThumbnailTemplate, @"pathForGetDocumentThumbnail couldn't generate template");
    }
    
    if (IsEmpty(containerType) || IsEmpty(userID)) {
        LogError(@"received nil arguments");
        return nil;
    }
    
    NSDictionary *dictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                kADXV1ServiceAPIVersion, kADXV1TemplateKeyVersionID,
                                userID, kADXV1TemplateKeyUserID,
                                containerType, kADXV1TemplateKeyContainerType,
                                documentID, kADXV1TemplateKeyDocumentID,
                                [NSString stringWithFormat:@"%d", (int)version], kADXV1TemplateKeyVersion,
                                nil];
    
    if (version > 0) {
        result = [getDocumentVersionThumbnailTemplate renderObject:dictionary];
    } else {
        result = [getDocumentThumbnailTemplate renderObject:dictionary];
    }
    
    return result;
}

- (NSString *)pathForGetDocumentVersions:(NSString *)containerType documentID:(NSString *)documentID user:(NSString *)userID
{
    NSString *result = nil;
    GRMustacheTemplate *getDocumentVersionsTemplate = [self pathTemplateForKey:kADXV1TemplateGetDocumentVersions];
    NSAssert(getDocumentVersionsTemplate, @"pathForGetDocumentVersions couldn't generate template");
    
    if (IsEmpty(containerType) || IsEmpty(userID)) {
        LogError(@"received nil arguments");
        return nil;
    }
    
    NSDictionary *dictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                kADXV1ServiceAPIVersion, kADXV1TemplateKeyVersionID,
                                userID, kADXV1TemplateKeyUserID,
                                containerType, kADXV1TemplateKeyContainerType,
                                documentID, kADXV1TemplateKeyDocumentID,
                                nil];
    result = [getDocumentVersionsTemplate renderObject:dictionary];
    
    return result;
}

- (NSString *)pathForGetDocumentNotes:(NSString *)containerType version:(NSUInteger)version documentID:(NSString *)documentID user:(NSString *)userID
{
    NSString *result = nil;
    GRMustacheTemplate *getDocumentNotesTemplate = nil;
    GRMustacheTemplate *getDocumentVersionNotesTemplate = nil;
    
    if (version > 0) {
        getDocumentVersionNotesTemplate = [self pathTemplateForKey:kADXV1TemplateGetDocumentVersionNotes];
        NSAssert(getDocumentVersionNotesTemplate, @"pathForGetDocumentNotes couldn't generate template");
    } else {
        getDocumentNotesTemplate = [self pathTemplateForKey:kADXV1TemplateGetDocumentNotes];
        NSAssert(getDocumentNotesTemplate, @"pathForGetDocumentNotes couldn't generate template");
    }
    
    if (IsEmpty(containerType) || IsEmpty(userID)) {
        LogError(@"received nil arguments");
        return nil;
    }
    
    NSDictionary *dictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                kADXV1ServiceAPIVersion, kADXV1TemplateKeyVersionID,
                                userID, kADXV1TemplateKeyUserID,
                                containerType, kADXV1TemplateKeyContainerType,
                                documentID, kADXV1TemplateKeyDocumentID,
                                [NSString stringWithFormat:@"%d", (int)version], kADXV1TemplateKeyVersion,
                                nil];
    
    if (version > 0) {
        result = [getDocumentVersionNotesTemplate renderObject:dictionary];
    } else {
        result = [getDocumentNotesTemplate renderObject:dictionary];
    }
    
    return result;
}

- (NSString *)pathForAddDocumentNote:(NSString *)containerType version:(NSUInteger)version documentID:(NSString *)documentID user:(NSString *)userID
{
    NSString *result = nil;
    GRMustacheTemplate *addDocumentNotesTemplate = nil;
    GRMustacheTemplate *addDocumentVersionNotesTemplate = nil;
    
    if (version > 0) {
        addDocumentVersionNotesTemplate = [self pathTemplateForKey:kADXV1TemplateAddDocumentVersionNote];
        NSAssert(addDocumentVersionNotesTemplate, @"pathForAddDocumentNote couldn't generate template");
    } else {
        addDocumentNotesTemplate = [self pathTemplateForKey:kADXV1TemplateAddDocumentVersionNote];
        NSAssert(addDocumentNotesTemplate, @"pathForAddDocumentNote couldn't generate template");
    }
    
    if (IsEmpty(containerType) || IsEmpty(userID)) {
        LogError(@"received nil arguments");
        return nil;
    }
    
    NSDictionary *dictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                kADXV1ServiceAPIVersion, kADXV1TemplateKeyVersionID,
                                userID, kADXV1TemplateKeyUserID,
                                containerType, kADXV1TemplateKeyContainerType,
                                documentID, kADXV1TemplateKeyDocumentID,
                                [NSString stringWithFormat:@"%d", (int)version], kADXV1TemplateKeyVersion,
                                nil];
    
    if (version > 0) {
        result = [addDocumentVersionNotesTemplate renderObject:dictionary];
    } else {
        result = [addDocumentNotesTemplate renderObject:dictionary];
    }
    
    return result;
}

- (NSString *)pathForUpdateDocumentNote:(NSString *)containerType version:(NSUInteger)version documentID:(NSString *)documentID noteID:(NSString *)noteID user:(NSString *)userID
{
    NSString *result = nil;
    GRMustacheTemplate *updateDocumentNotesTemplate = nil;
    GRMustacheTemplate *updateDocumentVersionNotesTemplate = nil;
    
    if (version > 0) {
        updateDocumentVersionNotesTemplate = [self pathTemplateForKey:kADXV1TemplateAddDocumentVersionNote];
        NSAssert(updateDocumentVersionNotesTemplate, @"pathForUpdateDocumentNote couldn't generate template");
    } else {
        updateDocumentNotesTemplate = [self pathTemplateForKey:kADXV1TemplateAddDocumentVersionNote];
        NSAssert(updateDocumentNotesTemplate, @"pathForUpdateDocumentNote couldn't generate template");
    }
    
    if (IsEmpty(containerType) || IsEmpty(userID)) {
        LogError(@"received nil arguments");
        return nil;
    }
    
    NSDictionary *dictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                kADXV1ServiceAPIVersion, kADXV1TemplateKeyVersionID,
                                userID, kADXV1TemplateKeyUserID,
                                containerType, kADXV1TemplateKeyContainerType,
                                documentID, kADXV1TemplateKeyDocumentID,
                                noteID, kADXV1TemplateKeyNoteID,
                                [NSString stringWithFormat:@"%d", (int)version], kADXV1TemplateKeyVersion,
                                nil];
    
    if (version > 0) {
        result = [updateDocumentVersionNotesTemplate renderObject:dictionary];
    } else {
        result = [updateDocumentNotesTemplate renderObject:dictionary];
    }
    
    return result;
}

- (NSString *)pathForDeleteDocumentNoteWithID:(NSString *)noteID documentID:(NSString *)documentID version:(NSUInteger)docVersion containerType:(NSString *)containerType user:(NSString *)userID
{
    if (IsEmpty(containerType) || IsEmpty(noteID) || IsEmpty(documentID)) {
        LogError(@"received nil arguments");
        return nil;
    }

    NSString *result = nil;
    GRMustacheTemplate *deleteDocumentNotesTemplate = nil;
    GRMustacheTemplate *deleteDocumentVersionNotesTemplate = nil;
    
    if (docVersion > 0) {
        deleteDocumentVersionNotesTemplate = [self pathTemplateForKey:kADXV1TemplateDeleteDocumentVersionNote];
        NSAssert(deleteDocumentVersionNotesTemplate, @"pathForDeleteDocumentNote couldn't generate template");
    } else {
        deleteDocumentNotesTemplate = [self pathTemplateForKey:kADXV1TemplateDeleteDocumentNote];
        NSAssert(deleteDocumentNotesTemplate, @"pathForDeleteDocumentNote couldn't generate template");
    }
    
    NSDictionary *dictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                kADXV1ServiceAPIVersion, kADXV1TemplateKeyVersionID,
                                userID, kADXV1TemplateKeyUserID,
                                containerType, kADXV1TemplateKeyContainerType,
                                documentID, kADXV1TemplateKeyDocumentID,
                                noteID, kADXV1TemplateKeyNoteID,
                                [NSString stringWithFormat:@"%d", (int)docVersion], kADXV1TemplateKeyVersion,
                                nil];
    
    if (docVersion > 0) {
        result = [deleteDocumentVersionNotesTemplate renderObject:dictionary];
    } else {
        result = [deleteDocumentNotesTemplate renderObject:dictionary];
    }
    
    return result;
}

- (NSString *)pathForGetDocumentChanges:(NSString *)containerType version:(NSUInteger)version documentID:(NSString *)documentID user:(NSString *)userID
{
    return [self pathForGetDocumentChanges:containerType betweenVersion:version andVersion:version-1 documentID:documentID user:userID];
}

- (NSString *)pathForGetDocumentChanges:(NSString *)containerType betweenVersion:(NSUInteger)version andVersion:(NSUInteger)withVersion documentID:(NSString *)documentID user:(NSString *)userID
{
    NSString *result = nil;
    GRMustacheTemplate *getDocumentChangesTemplate = [self pathTemplateForKey:kADXV1TemplateGetDocumentChanges];
    NSAssert(getDocumentChangesTemplate, @"pathForGetDocumentChanges couldn't generate template");
    
    if (IsEmpty(containerType) || IsEmpty(userID)) {
        LogError(@"received nil arguments");
        return nil;
    }

    NSDictionary *dictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                kADXV1ServiceAPIVersion, kADXV1TemplateKeyVersionID,
                                userID, kADXV1TemplateKeyUserID,
                                containerType, kADXV1TemplateKeyContainerType,
                                documentID, kADXV1TemplateKeyDocumentID,
                                documentID, kADXV1TemplateKeyDocumentID,
                                [NSString stringWithFormat:@"%d", (int)version], kADXV1TemplateKeyVersion,
                                [NSString stringWithFormat:@"%d", (int)withVersion], kADXV1TemplateKeyPrev,
                                nil];
    
    result = [getDocumentChangesTemplate renderObject:dictionary];
    
    return result;
}

- (NSString *)pathForPutAPNSToken:(NSString *)newAPNSToken oldAPNSToken:(NSString *)oldAPNSToken user:(NSString *)userID
{
    NSString *result = nil;
    GRMustacheTemplate *putAPNSTokenTemplate = [self pathTemplateForKey:kADXV1TemplatePutAPNSToken];
    NSAssert(putAPNSTokenTemplate, @"pathForPutAPNSToken couldn't generate template");
    
    if (IsEmpty(newAPNSToken) || IsEmpty(userID)) {
        LogError(@"received nil arguments");
        return nil;
    }
    
    NSDictionary *dictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                kADXV1ServiceAPIVersion, kADXV1TemplateKeyVersionID,
                                userID, kADXV1TemplateKeyUserID,
                                newAPNSToken, kADXV1TemplateKeyNewAPNSToken,
                                [NSNumber numberWithBool:(!IsEmpty(oldAPNSToken))], kADXV1TemplateKeyHasOldAPNSToken,
                                oldAPNSToken, kADXV1TemplateKeyOldAPNSToken,
                                nil];
    result = [putAPNSTokenTemplate renderObject:dictionary];
    
    return result;
}

- (NSString *)pathForUploadDocumentWithAccessToken:(NSString *)accessToken {
    NSString *result = nil;
    GRMustacheTemplate *uploadDocumentTemplate = [self pathTemplateForKey:kADXV1TemplateUploadDocument];
    NSAssert(uploadDocumentTemplate, @"pathForUploadDocumentWithAccessToken couldn't generate template");
    
    NSDictionary *dictionary = @{
        kADXV1TemplateKeyAccessToken: accessToken,
        kADXV1TemplateKeyVersionID: kADXV1ServiceAPIVersion
    };
    
    result = [uploadDocumentTemplate renderObject:dictionary];
    
    return result;
}

- (NSString *)pathForShareDocumentWithUserID:(NSString *)userID documentID:(NSString *)documentID {
    NSString *result = nil;
    GRMustacheTemplate *shareDocumentTemplate = [self pathTemplateForKey:kADXV1TemplateShareDocument];
    NSAssert(shareDocumentTemplate, @"pathForShareDocumentWithUserID couldn't generate template");
    
    NSDictionary *dictionary = @{
        kADXV1TemplateKeyUserID: userID,
        kADXV1TemplateKeyVersionID: kADXV1ServiceAPIVersion,
        kADXV1TemplateKeyDocumentID: documentID
    };
    
    result = [shareDocumentTemplate renderObject:dictionary];
    
    return result;
}

- (NSString *)pathForGetEventID:(NSString *)eventID
{
    NSString *result = nil;
    GRMustacheTemplate *getEventTemplate = [self pathTemplateForKey:kADXV1TemplateGetEvent];
    NSAssert(getEventTemplate, @"pathForGetEventID couldn't generate template");
    
    if (IsEmpty(eventID)) {
        LogError(@"received nil arguments");
        return nil;
    }
    
    NSDictionary *dictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                kADXV1ServiceAPIVersion, kADXV1TemplateKeyVersionID,
                                eventID, kADXV1TemplateKeyUserID,
                                nil];
    result = [getEventTemplate renderObject:dictionary];
    
    return result;
}

- (NSString *)pathForGetEventID:(NSString *)eventID forUser:(NSString *)userID
{
    NSString *result = nil;
    GRMustacheTemplate *getUserEventTemplate = [self pathTemplateForKey:kADXV1TemplateGetUserEvent];
    NSAssert(getUserEventTemplate, @"pathForGetEventID couldn't generate template");
    
    if (IsEmpty(eventID)) {
        LogError(@"received nil arguments");
        return nil;
    }
    
    NSDictionary *dictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                kADXV1ServiceAPIVersion, kADXV1TemplateKeyVersionID,
                                userID, kADXV1TemplateKeyUserID,
                                eventID, kADXV1TemplateKeyEventID,
                                nil];
    result = [getUserEventTemplate renderObject:dictionary];
    
    return result;
}

- (NSString *)pathForGetEventsForUser:(NSString *)userID
{
    NSString *result = nil;
    GRMustacheTemplate *getEventsTemplate = [self pathTemplateForKey:kADXV1TemplatePostEvents];
    NSAssert(getEventsTemplate, @"pathForGetEventsForUser couldn't generate template");
    
    if (IsEmpty(userID)) {
        LogError(@"received nil arguments");
        return nil;
    }
    
    NSDictionary *dictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                kADXV1ServiceAPIVersion, kADXV1TemplateKeyVersionID,
                                userID, kADXV1TemplateKeyUserID,
                                nil];
    result = [getEventsTemplate renderObject:dictionary];
    
    return result;
}

- (NSString *)pathForPostEventsForUser:(NSString *)userID
{
    NSString *result = nil;
    GRMustacheTemplate *postEventsTemplate = [self pathTemplateForKey:kADXV1TemplatePostEvents];
    NSAssert(postEventsTemplate, @"pathForPostEventsForUser couldn't generate template");
    
    if (IsEmpty(userID)) {
        LogError(@"received nil arguments");
        return nil;
    }
    
    NSDictionary *dictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                kADXV1ServiceAPIVersion, kADXV1TemplateKeyVersionID,
                                userID, kADXV1TemplateKeyUserID,
                                nil];
    result = [postEventsTemplate renderObject:dictionary];
    
    return result;
}

#pragma mark - Authentication Operations

- (void)getAccessTokenForLoginName:(NSString *)loginName password:(NSString *)password completion:(ADXV1ServiceRequestCompletionBlock)completionBlock;
{
    NSString *path = [self pathForLogin];
    
    __weak ADXV1ServiceClient *weakSelf = self;
    [self.httpClient setAuthorizationHeaderWithUsername:loginName password:password];    
    [self.httpClient postPath:path 
                   parameters:nil
                      success:^(AFHTTPRequestOperation *operation, id responseObject) {
                          NSDictionary *json = responseObject;
                          NSString *accessToken = [json objectForKey:kADXServiceJSONKeyAccessToken];
                          [weakSelf setLoginResponse:json];
                          
                          [weakSelf.httpClient clearAuthorizationHeader];
                          [weakSelf.downloadClient clearAuthorizationHeader];
                          // FIXME remove the following line once the server supports access tokens. Whereas now the following line is necessary because our http client class will inject the access token into the query string.
                          [weakSelf.httpClient setAccessToken:accessToken];
                          [weakSelf.downloadClient setAccessToken:accessToken];
                          [weakSelf.httpClient setAuthorizationHeaderWithToken:accessToken];
                          [weakSelf.downloadClient setAuthorizationHeaderWithToken:accessToken];
                          
                          if (completionBlock) {
                              completionBlock(operation.response.statusCode, responseObject, nil);
                          }
                      } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                          [weakSelf.httpClient clearAuthorizationHeader];
                          [weakSelf.downloadClient clearAuthorizationHeader];
                          if (completionBlock) {
                              completionBlock(operation.response.statusCode, nil, error);
                          }
                      }];
}

- (void)registerUserWithLoginName:(NSString *)loginName
                            email:(NSString *)emailAddress
                         password:(NSString *)password
                       completion:(ADXV1ServiceRequestCompletionBlock)completionBlock
{
    NSString *path = [self pathForUserRegistration];
    
    NSMutableURLRequest *request = [self.httpClient multipartFormRequestWithMethod:@"POST"
                                                                              path:path
                                                                        parameters:nil
                                                         constructingBodyWithBlock:^(id<AFMultipartFormData> formData)
    {
        NSDictionary *registerRequestBody = @{
            @"primaryEmail": emailAddress,
            @"loginName": loginName,
            @"password": password
        };
        
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:registerRequestBody
                                                           options:NSJSONWritingPrettyPrinted
                                                             error:nil];
        [formData appendPartWithFormData:jsonData name:@"user"];
    }];

    AFHTTPRequestOperation *registerOperation = [self.httpClient HTTPRequestOperationWithRequest:request success:^(AFHTTPRequestOperation *operation, id responseObject) {
        responseObject = [NSNumber numberWithInteger:operation.response.statusCode];
        if (completionBlock) {
            completionBlock(operation.response.statusCode, responseObject, nil);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (completionBlock) {
            completionBlock(operation.response.statusCode, operation, error);
        }
    }];
    
    [self.httpClient enqueueHTTPRequestOperation:registerOperation];
}

#pragma mark - Container Operations

- (void)getContainer:(NSString *)containerType folder:(NSString *)folderID since:(NSDate *)since completion:(ADXV1ServiceRequestCompletionBlock)completionBlock
{
    NSString *path = [self pathForGetContainer:containerType folder:folderID since:(NSDate *)since user:self.userID];
    
    [self.httpClient getPath:path 
                  parameters:nil
                     success:^(AFHTTPRequestOperation *operation, id responseObject) {
                         if (completionBlock) {
                             completionBlock(operation.response.statusCode, responseObject, nil);
                         }
                     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                         if (completionBlock) {
                             completionBlock(operation.response.statusCode, nil, error);
                         }
                     }];
}

#pragma mark - Document Operations

- (void)getDocumentProperties:(NSString *)documentID container:(NSString *)containerType completion:(ADXV1ServiceRequestCompletionBlock)completionBlock
{
    // TODO - complete implementation
}

- (void)getDocumentThumbnail:(NSString *)documentID version:(NSUInteger)version container:(NSString *)containerType completion:(ADXV1ServiceRequestCompletionBlock)completionBlock;
{
    NSString *path = [self pathForGetDocumentThumbnail:containerType version:version documentID:documentID user:self.userID];

    [self.downloadClient getPath:path
                      parameters:nil
                         success:^(AFHTTPRequestOperation *operation, id responseObject) {
                             if (completionBlock) {
                                 completionBlock(operation.response.statusCode, responseObject, nil);
                             }
                         } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                             if (completionBlock) {
                                 completionBlock(operation.response.statusCode, nil, error);
                             }
                         }];
}

- (void)getDocumentVersions:(NSString *)documentID container:(NSString *)containerType completion:(ADXV1ServiceRequestCompletionBlock)completionBlock
{
    NSString *path = [self pathForGetDocumentVersions:containerType documentID:documentID user:self.userID];
    
    [self.httpClient getPath:path
                  parameters:nil
                     success:^(AFHTTPRequestOperation *operation, id responseObject) {
                         if (completionBlock) {
                             completionBlock(operation.response.statusCode, responseObject, nil);
                         }
                     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                         if (completionBlock) {
                             completionBlock(operation.response.statusCode, nil, error);
                         }
                     }];
}

- (NSInteger)numberOfVersionsForDocument:(NSString *)documentID container:(NSString *)containerType completion:(ADXV1ServiceRequestCompletionBlock)completionBlock
{
    // TODO - complete implementation
    return 0;
}


- (AFHTTPRequestOperation *)getContentOperationForDocumentID:(NSString *)documentID
                                                     version:(NSUInteger)version
                                                 highlighted:(BOOL)highlighted
                                                   container:(NSString *)containerType
                                          backgroundDownload:(BOOL)backgroundDownload
                                                    progress:(ADXV1ServiceDownloadProgressBlock)progressBlock
                                                  completion:(ADXV1ServiceRequestCompletionBlock)completionBlock
{
    NSString *path = nil;
    if (highlighted) {
        path = [self pathForGetHighlightedDocumentContent:containerType version:version documentID:documentID user:self.userID];
    } else {
        path = [self pathForGetDocumentContent:containerType version:version documentID:documentID user:self.userID];
    }
    NSMutableURLRequest *request = [self.downloadClient requestWithMethod:@"GET" path:path parameters:nil];
    AFHTTPRequestOperation *operation = [self.downloadClient HTTPRequestOperationWithRequest:request
                                                                                     success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                                                                         NSLog(@"getContentOperationForDocumentID SUCCESS original request: %@", request);
                                                                                         NSLog(@"getContentOperationForDocumentID SUCCESS operation request: %@ response: %d", operation.request, [(NSData *)responseObject length]);
                                                                                         if (completionBlock) {
                                                                                             completionBlock(operation.response.statusCode, responseObject, nil);
                                                                                         }
                                                                                     } 
                                                                                     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                                                                         NSLog(@"getContentOperationForDocumentID FAILED operation request: %@", operation.request);
                                                                                         NSLog(@"getContentOperationForDocumentID FAILED operation response headers: %@", operation.response.allHeaderFields);
                                                                                         if (error) {
                                                                                             LogError(@"getContentOperationForDocumentID failed operation %@", operation);
                                                                                             LogNSError(@"getContentOperationForDocumentID failed with error ", error);
                                                                                         }
                                                                                         if (completionBlock) {
                                                                                             completionBlock(operation.response.statusCode, nil, error);
                                                                                         }
                                                                                     }];
    if (backgroundDownload) {
#ifndef TARGET_OS_MAC
        [operation setShouldExecuteAsBackgroundTaskWithExpirationHandler:nil];
#endif
    }
    if (progressBlock) {
        [operation setDownloadProgressBlock:progressBlock];
    }
    return operation;
}

- (void)getDocumentContentViaOperation:(AFHTTPRequestOperation *)operation;
{
    [self.downloadClient enqueueHTTPRequestOperation:operation];
}

- (void)getDocumentContent:(NSString *)documentID version:(NSUInteger)version container:(NSString *)containerType completion:(ADXV1ServiceRequestCompletionBlock)completionBlock
{
    NSString *path = [self pathForGetDocumentContent:containerType version:version documentID:documentID user:self.userID];
    
    [self.downloadClient getPath:path
                      parameters:nil
                         success:^(AFHTTPRequestOperation *operation, id responseObject) {
                             if (completionBlock) {
                                 completionBlock(operation.response.statusCode, responseObject, nil);
                             }
                         } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                             if (completionBlock) {
                                 completionBlock(operation.response.statusCode, nil, error);
                             }
                         }];
}

- (void)getHighlightedDocumentContent:(NSString *)documentID version:(NSUInteger)version container:(NSString *)containerType completion:(ADXV1ServiceRequestCompletionBlock)completionBlock
{
    NSString *path = [self pathForGetHighlightedDocumentContent:containerType version:version documentID:documentID user:self.userID];
    
    [self.downloadClient getPath:path
                      parameters:nil
                         success:^(AFHTTPRequestOperation *operation, id responseObject) {
                             if (completionBlock) {
                                 completionBlock(operation.response.statusCode, responseObject, nil);
                             }
                         } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                             if (completionBlock) {
                                 completionBlock(operation.response.statusCode, nil, error);
                             }
                         }];
}

- (void)getDocumentNotes:(NSString *)documentID version:(NSUInteger)version container:(NSString *)containerType completion:(ADXV1ServiceRequestCompletionBlock)completionBlock
{
    NSString *path = [self pathForGetDocumentNotes:containerType version:version documentID:documentID user:self.userID];
    
    [self.httpClient getPath:path
                  parameters:nil
                     success:^(AFHTTPRequestOperation *operation, id responseObject) {
                         if (completionBlock) {
                             completionBlock(operation.response.statusCode, responseObject, nil);
                         }
                     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                         if (completionBlock) {
                             completionBlock(operation.response.statusCode, nil, error);
                         }
                     }];
}

- (void)addDocumentNote:(NSString *)documentID noteJSON:(NSData *)noteJSON version:(NSUInteger)version container:(NSString *)containerType completion:(ADXV1ServiceRequestCompletionBlock)completionBlock
{
    NSString *path = [self pathForAddDocumentNote:containerType version:version documentID:documentID user:self.userID];
    
    NSMutableURLRequest *request = [self.httpClient multipartFormRequestWithMethod:@"POST"
                                                                              path:path
                                                                        parameters:nil
                                                         constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
                                                             NSString *p = [[NSString alloc] initWithData:noteJSON encoding:NSUTF8StringEncoding];
                                                             NSLog(@"payload: %@", p);
                                                             [formData appendPartWithFileData:noteJSON
                                                                                         name:@"note"
                                                                                     fileName:@"note"
                                                                                     mimeType:@"application/json"];
                                                         }];
    
    AFHTTPRequestOperation *operation = [self.httpClient HTTPRequestOperationWithRequest:request success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if (completionBlock) {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                completionBlock(operation.response.statusCode, responseObject, nil);
            });
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (completionBlock) {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                completionBlock(operation.response.statusCode, nil, error);
            });
        }
    }];
    
    [self.httpClient enqueueHTTPRequestOperation:operation];
}

- (void)updateDocumentNote:(NSString *)documentID noteID:(NSString *)noteID noteJSON:(NSData *)noteJSON version:(NSUInteger)version container:(NSString *)containerType completion:(ADXV1ServiceRequestCompletionBlock)completionBlock
{
    NSString *path = [self pathForUpdateDocumentNote:containerType version:version documentID:documentID noteID:noteID user:self.userID];
    
    NSMutableURLRequest *request = [self.httpClient multipartFormRequestWithMethod:@"POST"
                                                                              path:path
                                                                        parameters:nil
                                                         constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
                                                             [formData appendPartWithFileData:noteJSON
                                                                                         name:@"note"
                                                                                     fileName:@"note"
                                                                                     mimeType:@"application/json"];
                                                         }];
    
    AFHTTPRequestOperation *operation = [self.httpClient HTTPRequestOperationWithRequest:request success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if (completionBlock) {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                completionBlock(operation.response.statusCode, responseObject, nil);
            });
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (completionBlock) {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                completionBlock(operation.response.statusCode, nil, error);
            });
        }
    }];
    
    [self.httpClient enqueueHTTPRequestOperation:operation];
}

- (void)deleteDocumentNoteWithID:(NSString *)noteID fromDocumentWithID:(NSString *)documentID version:(NSUInteger)docVersion container:(NSString *)containerType completion:(ADXV1ServiceRequestCompletionBlock)completionBlock
{
    NSString *path = [self pathForDeleteDocumentNoteWithID:noteID documentID:documentID version:docVersion containerType:containerType user:self.userID];

    [self.httpClient deletePath:path parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        completionBlock(operation.response.statusCode, nil, nil);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        completionBlock(operation.response.statusCode, nil, error);
    }];

}

- (void)getDocumentChanges:(NSString *)documentID betweenVersion:(NSUInteger)version andVersion:(NSUInteger)otherVersion container:(NSString *)containerType completion:(ADXV1ServiceRequestCompletionBlock)completionBlock
{
    NSString *path = [self pathForGetDocumentChanges:containerType betweenVersion:version andVersion:otherVersion documentID:documentID user:self.userID];
    
    [self.httpClient getPath:path
                  parameters:nil
                     success:^(AFHTTPRequestOperation *operation, id responseObject) {
                         if (completionBlock) {
                             completionBlock(200, responseObject, nil);
                         }
                     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                         if (completionBlock) {
                             completionBlock(500, nil, error);
                         }
                     }];
}

- (void)uploadDocumentAtURL:(NSURL *)documentURL
                      title:(NSString *)title
                 recipients:(NSDictionary *)recipients
                 completion:(ADXV1ServiceRequestCompletionBlock)completionBlock
{
    NSString *loginName = [self.loginResponse valueForKey:@"loginName"];
    NSURL *url = [NSURL URLWithString:[self pathForUploadDocumentWithAccessToken:loginName] relativeToURL:self.baseURL];

    // Document information
    NSDictionary *documentData = @{
        @"contentType": @"application/pdf",
        @"title": title,
        @"recipients": recipients
    };
    
    // Convert to JSON String
    NSError *error = nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:documentData
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:&error];
    
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    // Build the multipart form submission
    MMMultipartForm *form = [[MMMultipartForm alloc] initWithURL:url];
    [form addFile:documentURL.path withFieldName:@"content"];
    [form addFormField:@"document" withStringData:jsonString];
    
    // Build the POST request
    NSMutableURLRequest *postRequest = [form mpfRequest];

    [NSURLConnection sendAsynchronousRequest:postRequest
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *responseData, NSError *error) {
                               if (error == nil) {
                                   NSString *responseString = [NSString stringWithCString:[responseData bytes] encoding:NSUTF8StringEncoding];
#ifdef DEBUG
                                   NSLog(@"document upload response body: %@", responseString);
#endif
                                   completionBlock(200, responseString, nil);
                               } else {
                                   completionBlock(500, nil, error);
                               }
                           }];
}

- (void)shareDocumentWithID:(NSString *)documentID withEmailAddresses:(NSArray *)addresses message:(NSString *)message completion:(ADXV1ServiceRequestCompletionBlock)completionBlock {
    NSString *path = [self pathForShareDocumentWithUserID:self.userID documentID:documentID];
    
    // Convert the list of addresses into a list of dictionaries with the address as the primaryEmail key of each dictionary
    NSMutableArray *recipientsArray = [NSMutableArray array];
    for (NSString *address in addresses) {
        NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithObject:address forKey:@"primaryEmail"];
        [recipientsArray addObject:dict];
    }
    
    // Document information
    NSDictionary *documentData = @{
        @"recipients": recipientsArray
    };
    
    // Convert to JSON String
    NSError *error = nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:documentData
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:&error];
    
    // Build the multipart form submission
    NSMutableURLRequest *request = [self.httpClient multipartFormRequestWithMethod:@"POST"
                                                                              path:path
                                                                        parameters:nil
                                                         constructingBodyWithBlock:^(id<AFMultipartFormData> formData)
                                    {
                                        [formData appendPartWithFormData:jsonData name:@"share"];
                                    }];
    
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *responseData, NSError *error) {
                               if (error == nil) {
                                   NSString *responseString = [NSString stringWithCString:[responseData bytes] encoding:NSUTF8StringEncoding];
#ifdef DEBUG
                                   NSLog(@"shareDocument response body: %@", responseString);
#endif
                                   completionBlock(200, responseString, nil);
                               } else {
                                   completionBlock(500, nil, error);
                               }
                           }];
}

#pragma mark - Folder Operations

- (void)getFolderProperties:(NSString *)folderID completion:(ADXV1ServiceRequestCompletionBlock)completionBlock
{
    
}

- (void)createFolder:(NSString *)name inContainer:(NSString *)containerType completion:(ADXV1ServiceRequestCompletionBlock)completionBlock
{
    
}

- (void)updateFolder:(NSString *)folderID completion:(ADXV1ServiceRequestCompletionBlock)completionBlock
{
    
}

- (void)updateDeviceToken:(NSData *)newToken oldToken:(NSData *)oldToken completion:(ADXV1ServiceRequestCompletionBlock)completionBlock
{
    NSAssert([newToken length], @"updateDeviceToken expected non-nil new token");
    
    NSString *newTokenAsString = [newToken adx_hexString];
    NSString *oldTokenAsString = [oldToken length] ? [oldToken adx_hexString] : nil;
    
    NSString *path = [self pathForPutAPNSToken:newTokenAsString oldAPNSToken:oldTokenAsString user:self.userID];
    
#ifdef DEBUG
    LogInfo(@"Registering new token (%@) old token (%@)", newTokenAsString, (oldTokenAsString ? oldTokenAsString : @"") );
#endif
    [self.httpClient postPath:path
                   parameters:nil
                      success:^(AFHTTPRequestOperation *operation, id responseObject) {
                          if (completionBlock) {
                              completionBlock(operation.response.statusCode, responseObject, nil);
                          }
                      } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                          if (completionBlock) {
                              completionBlock(operation.response.statusCode, nil, error);
                          }
                      }];
}

- (void)cancelAllDocumentDownloads
{
    [self.downloadClient cancelAllHTTPOperationsWithMethod:nil];
}

#pragma mark - Events

- (void)getEventsSinceEventID:(NSString *)eventID type:(NSString *)eventType limit:(NSUInteger)limit completion:(ADXV1ServiceRequestCompletionBlock)completionBlock
{
    NSString *path = [self pathForGetEventsForUser:self.userID];
    NSDictionary *parameters = [NSDictionary dictionaryWithObjectsAndKeys:
                                eventID, kADXV1TemplateKeyEventSinceID,
                                eventType, kADXV1TemplateKeyEventType,
                                [NSNumber numberWithUnsignedInteger:limit], kADXV1TemplateKeyEventLimit,
                                nil];

    [self.httpClient getPath:path
                  parameters:parameters
                     success:^(AFHTTPRequestOperation *operation, id responseObject) {
                         if (completionBlock) {
                             completionBlock(operation.response.statusCode, responseObject, nil);
                         }
                     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                         if (completionBlock) {
                             completionBlock(operation.response.statusCode, nil, error);
                         }
                     }];
}

- (void)getEventID:(NSString *)eventID completion:(ADXV1ServiceRequestCompletionBlock)completionBlock
{
    NSString *path = [self pathForGetEventID:eventID forUser:self.userID];
    
    [self.httpClient getPath:path
                  parameters:nil
                     success:^(AFHTTPRequestOperation *operation, id responseObject) {
                         if (completionBlock) {
                             completionBlock(operation.response.statusCode, responseObject, nil);
                         }
                     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                         if (completionBlock) {
                             completionBlock(operation.response.statusCode, nil, error);
                         }
                     }];
}

- (void)getUserEventID:(NSString *)eventID completion:(ADXV1ServiceRequestCompletionBlock)completionBlock
{
    NSString *path = [self pathForGetEventID:eventID forUser:self.userID];
    
    [self.httpClient getPath:path
                  parameters:nil
                     success:^(AFHTTPRequestOperation *operation, id responseObject) {
                         if (completionBlock) {
                             completionBlock(operation.response.statusCode, responseObject, nil);
                         }
                     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                         if (completionBlock) {
                             completionBlock(operation.response.statusCode, nil, error);
                         }
                     }];
}

- (void)postEvents:(NSArray *)events completion:(ADXV1ServiceRequestCompletionBlock)completionBlock
{
    NSError *jsonError = nil;
    NSString *path = [self pathForPostEventsForUser:kADXV1ServiceUserMeLiteral];
    
    NSMutableArray *jsonEvents = [[NSMutableArray alloc] init];
    for (ADXEventQueueEntry *entry in events) {
        [jsonEvents addObject:[entry jsonDictionary]];
    }
    
    NSData *payload = [NSJSONSerialization dataWithJSONObject:[NSDictionary dictionaryWithObject:jsonEvents forKey:@"events"]
                                                      options:NSJSONWritingPrettyPrinted
                                                        error:&jsonError];
    if (!payload) {
        LogNSError(@"Error converting event to JSON", jsonError);
        if (completionBlock) {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                completionBlock(400, nil, jsonError);
            });
        }
    }
    
    NSMutableURLRequest *request = [self.httpClient multipartFormRequestWithMethod:@"POST"
                                                                              path:path
                                                                        parameters:nil
                                                         constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
                                                             [formData appendPartWithFileData:payload
                                                                                         name:@"events"
                                                                                     fileName:@"events"
                                                                                     mimeType:@"application/json"];
                                                         }];
    
    AFHTTPRequestOperation *operation = [self.httpClient HTTPRequestOperationWithRequest:request success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if (completionBlock) {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                completionBlock(operation.response.statusCode, responseObject, nil);
            });
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (completionBlock) {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                completionBlock(operation.response.statusCode, nil, error);
            });
        }
    }];
    
    [self.httpClient enqueueHTTPRequestOperation:operation];
}

@end
