//
//  ADXHTTPClient.m
//  ActivDoxCore
//
//  Created by Gavin McKenzie on 12-06-06.
//  Copyright (c) 2012 ActivDox Inc. All rights reserved.
//

#import "ADXHTTPClient.h"
#import "ADXCommonMacros.h"

NSString * const kADXServiceJSONKeyAccessToken = @"accessToken";

@implementation ADXHTTPClient
@synthesize accessToken = _accessToken;

- (NSDictionary *)injectAccessTokenIntoQueryString:(NSDictionary *)parameters
{
    if (IsEmpty(self.accessToken)) {
        return parameters;
    }
    NSMutableDictionary *result = [NSMutableDictionary dictionaryWithDictionary:parameters];
    [result setObject:self.accessToken forKey:kADXServiceJSONKeyAccessToken];
    return [NSDictionary dictionaryWithDictionary:result];
}

- (void)getPath:(NSString *)path 
     parameters:(NSDictionary *)parameters 
        success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
        failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure
{
    parameters = [self injectAccessTokenIntoQueryString:parameters];
    [super getPath:path parameters:parameters success:success failure:failure];
}

- (void)postPath:(NSString *)path 
      parameters:(NSDictionary *)parameters 
         success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
         failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure
{
    parameters = [self injectAccessTokenIntoQueryString:parameters];
    [super postPath:path parameters:parameters success:success failure:failure];
}

- (void)putPath:(NSString *)path 
     parameters:(NSDictionary *)parameters 
        success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
        failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure
{
    parameters = [self injectAccessTokenIntoQueryString:parameters];
    [super putPath:path parameters:parameters success:success failure:failure];
}

- (void)deletePath:(NSString *)path 
        parameters:(NSDictionary *)parameters 
           success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
           failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure
{
    parameters = [self injectAccessTokenIntoQueryString:parameters];
    [super deletePath:path parameters:parameters success:success failure:failure];
}

- (NSMutableURLRequest *)requestWithMethod:(NSString *)method path:(NSString *)path parameters:(NSDictionary *)parameters
{
    parameters = [self injectAccessTokenIntoQueryString:parameters];
    NSMutableURLRequest *request = [super requestWithMethod:method path:path parameters:parameters];
    return request;
}

- (NSMutableURLRequest *)multipartFormRequestWithMethod:(NSString *)method path:(NSString *)path parameters:(NSDictionary *)parameters constructingBodyWithBlock:(void (^)(id<AFMultipartFormData>))block
{
    NSDictionary *accessToken = [self injectAccessTokenIntoQueryString:parameters];
    NSString *pathAndQuery = [NSString stringWithFormat:@"%@?%@=%@", path, kADXServiceJSONKeyAccessToken, [accessToken objectForKey:kADXServiceJSONKeyAccessToken]];
    NSMutableURLRequest *request = [super multipartFormRequestWithMethod:method path:pathAndQuery parameters:nil constructingBodyWithBlock:block];
    return request;
}

- (NSMutableURLRequest *)unauthenticatedMultipartFormRequestWithMethod:(NSString *)method path:(NSString *)path parameters:(NSDictionary *)parameters constructingBodyWithBlock:(void (^)(id<AFMultipartFormData>))block
{
    return [super multipartFormRequestWithMethod:method path:path parameters:nil constructingBodyWithBlock:block];
}

- (void)cancelAllHTTPOperationsWithMethod:(NSString *)method
{
    for (NSOperation *operation in [self.operationQueue operations]) {
        if (![operation isKindOfClass:[AFHTTPRequestOperation class]]) {
            continue;
        }
        
        if ((!method || [method isEqualToString:[[(AFHTTPRequestOperation *)operation request] HTTPMethod]])) {
            [operation cancel];
        }
    }
}

@end
