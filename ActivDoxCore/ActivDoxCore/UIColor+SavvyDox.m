//
//  UIColor+SavvyDox.m
//  Reader
//
//  Created by Steve Tibbett on 2013-05-02.
//  Copyright (c) 2013 ActivDox Inc. All rights reserved.
//

#import <ActivDoxCore.h>
#import "UIColor+SavvyDox.h"

@implementation UIColor (SavvyDox)

+ (UIColor *)adxPopoverTextColor
{
    return [UIColor colorWithRed:76/255.0 green:76/255.0 blue:76/255.0 alpha:1.000];
}

+ (UIColor *)adxInsertedContentColor
{
    if ([ADXUserDefaults sharedDefaults].useAlternativeColors) {
        return [UIColor colorWithHue:195.0/360.0 saturation:0.45 brightness:0.79 alpha:0.5];
    } else {
        return [UIColor colorWithRed:0 green:0.75 blue:0.0 alpha:0.35];
    }
}

+ (UIColor *)adxUpdatedContentColor
{
    if ([ADXUserDefaults sharedDefaults].useAlternativeColors) {
        return [UIColor colorWithHue:29/360.0 saturation:0.57 brightness:0.99 alpha:0.5];
    } else {
        return [UIColor colorWithRed:1.0 green:0.75 blue:0.0 alpha:0.35];
    }
}

@end
