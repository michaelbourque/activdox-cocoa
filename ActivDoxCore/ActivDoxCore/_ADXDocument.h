// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to ADXDocument.h instead.

#import <CoreData/CoreData.h>



extern const struct ADXDocumentAttributes {
	__unsafe_unretained NSString *accountID;
	__unsafe_unretained NSString *changeContentDownloaded;
	__unsafe_unretained NSString *changeCount;
	__unsafe_unretained NSString *changedPages;
	__unsafe_unretained NSString *documentDescription;
	__unsafe_unretained NSString *downloaded;
	__unsafe_unretained NSString *errorMessage;
	__unsafe_unretained NSString *id;
	__unsafe_unretained NSString *isPublic;
	__unsafe_unretained NSString *message;
	__unsafe_unretained NSString *mostRecentOpenedDate;
	__unsafe_unretained NSString *noteCount;
	__unsafe_unretained NSString *pageCount;
	__unsafe_unretained NSString *safeToDeleteOnStartup;
	__unsafe_unretained NSString *status;
	__unsafe_unretained NSString *title;
	__unsafe_unretained NSString *uploadDate;
	__unsafe_unretained NSString *version;
	__unsafe_unretained NSString *versionIsLatest;
} ADXDocumentAttributes;



extern const struct ADXDocumentRelationships {
	__unsafe_unretained NSString *author;
	__unsafe_unretained NSString *collections;
	__unsafe_unretained NSString *content;
	__unsafe_unretained NSString *highlightedContent;
	__unsafe_unretained NSString *metaDocument;
	__unsafe_unretained NSString *thumbnail;
} ADXDocumentRelationships;






@class ADXUser;
@class ADXCollection;
@class ADXDocumentContent;
@class ADXHighlightedContent;
@class ADXMetaDocument;
@class ADXDocumentThumbnail;









@class NSObject;































@interface ADXDocumentID : NSManagedObjectID {}
@end

@interface _ADXDocument : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (ADXDocumentID*)objectID;





@property (nonatomic, strong) NSString* accountID;



//- (BOOL)validateAccountID:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* changeContentDownloaded;




@property (atomic) BOOL changeContentDownloadedValue;
- (BOOL)changeContentDownloadedValue;
- (void)setChangeContentDownloadedValue:(BOOL)value_;


//- (BOOL)validateChangeContentDownloaded:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* changeCount;




@property (atomic) int64_t changeCountValue;
- (int64_t)changeCountValue;
- (void)setChangeCountValue:(int64_t)value_;


//- (BOOL)validateChangeCount:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) id changedPages;



//- (BOOL)validateChangedPages:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* documentDescription;



//- (BOOL)validateDocumentDescription:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* downloaded;




@property (atomic) BOOL downloadedValue;
- (BOOL)downloadedValue;
- (void)setDownloadedValue:(BOOL)value_;


//- (BOOL)validateDownloaded:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* errorMessage;



//- (BOOL)validateErrorMessage:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* id;



//- (BOOL)validateId:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* isPublic;




@property (atomic) BOOL isPublicValue;
- (BOOL)isPublicValue;
- (void)setIsPublicValue:(BOOL)value_;


//- (BOOL)validateIsPublic:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* message;



//- (BOOL)validateMessage:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSDate* mostRecentOpenedDate;



//- (BOOL)validateMostRecentOpenedDate:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* noteCount;




@property (atomic) int64_t noteCountValue;
- (int64_t)noteCountValue;
- (void)setNoteCountValue:(int64_t)value_;


//- (BOOL)validateNoteCount:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* pageCount;




@property (atomic) int64_t pageCountValue;
- (int64_t)pageCountValue;
- (void)setPageCountValue:(int64_t)value_;


//- (BOOL)validatePageCount:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* safeToDeleteOnStartup;




@property (atomic) BOOL safeToDeleteOnStartupValue;
- (BOOL)safeToDeleteOnStartupValue;
- (void)setSafeToDeleteOnStartupValue:(BOOL)value_;


//- (BOOL)validateSafeToDeleteOnStartup:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* status;



//- (BOOL)validateStatus:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* title;



//- (BOOL)validateTitle:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSDate* uploadDate;



//- (BOOL)validateUploadDate:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* version;




@property (atomic) int64_t versionValue;
- (int64_t)versionValue;
- (void)setVersionValue:(int64_t)value_;


//- (BOOL)validateVersion:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* versionIsLatest;




@property (atomic) BOOL versionIsLatestValue;
- (BOOL)versionIsLatestValue;
- (void)setVersionIsLatestValue:(BOOL)value_;


//- (BOOL)validateVersionIsLatest:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) ADXUser *author;

//- (BOOL)validateAuthor:(id*)value_ error:(NSError**)error_;




@property (nonatomic, strong) NSSet *collections;

- (NSMutableSet*)collectionsSet;




@property (nonatomic, strong) ADXDocumentContent *content;

//- (BOOL)validateContent:(id*)value_ error:(NSError**)error_;




@property (nonatomic, strong) ADXHighlightedContent *highlightedContent;

//- (BOOL)validateHighlightedContent:(id*)value_ error:(NSError**)error_;




@property (nonatomic, strong) ADXMetaDocument *metaDocument;

//- (BOOL)validateMetaDocument:(id*)value_ error:(NSError**)error_;




@property (nonatomic, strong) ADXDocumentThumbnail *thumbnail;

//- (BOOL)validateThumbnail:(id*)value_ error:(NSError**)error_;





@end


@interface _ADXDocument (CollectionsCoreDataGeneratedAccessors)
- (void)addCollections:(NSSet*)value_;
- (void)removeCollections:(NSSet*)value_;
- (void)addCollectionsObject:(ADXCollection*)value_;
- (void)removeCollectionsObject:(ADXCollection*)value_;
@end


@interface _ADXDocument (CoreDataGeneratedPrimitiveAccessors)


- (NSString*)primitiveAccountID;
- (void)setPrimitiveAccountID:(NSString*)value;




- (NSNumber*)primitiveChangeContentDownloaded;
- (void)setPrimitiveChangeContentDownloaded:(NSNumber*)value;

- (BOOL)primitiveChangeContentDownloadedValue;
- (void)setPrimitiveChangeContentDownloadedValue:(BOOL)value_;




- (NSNumber*)primitiveChangeCount;
- (void)setPrimitiveChangeCount:(NSNumber*)value;

- (int64_t)primitiveChangeCountValue;
- (void)setPrimitiveChangeCountValue:(int64_t)value_;




- (id)primitiveChangedPages;
- (void)setPrimitiveChangedPages:(id)value;




- (NSString*)primitiveDocumentDescription;
- (void)setPrimitiveDocumentDescription:(NSString*)value;




- (NSNumber*)primitiveDownloaded;
- (void)setPrimitiveDownloaded:(NSNumber*)value;

- (BOOL)primitiveDownloadedValue;
- (void)setPrimitiveDownloadedValue:(BOOL)value_;




- (NSString*)primitiveErrorMessage;
- (void)setPrimitiveErrorMessage:(NSString*)value;




- (NSString*)primitiveId;
- (void)setPrimitiveId:(NSString*)value;




- (NSNumber*)primitiveIsPublic;
- (void)setPrimitiveIsPublic:(NSNumber*)value;

- (BOOL)primitiveIsPublicValue;
- (void)setPrimitiveIsPublicValue:(BOOL)value_;




- (NSString*)primitiveMessage;
- (void)setPrimitiveMessage:(NSString*)value;




- (NSDate*)primitiveMostRecentOpenedDate;
- (void)setPrimitiveMostRecentOpenedDate:(NSDate*)value;




- (NSNumber*)primitiveNoteCount;
- (void)setPrimitiveNoteCount:(NSNumber*)value;

- (int64_t)primitiveNoteCountValue;
- (void)setPrimitiveNoteCountValue:(int64_t)value_;




- (NSNumber*)primitivePageCount;
- (void)setPrimitivePageCount:(NSNumber*)value;

- (int64_t)primitivePageCountValue;
- (void)setPrimitivePageCountValue:(int64_t)value_;




- (NSNumber*)primitiveSafeToDeleteOnStartup;
- (void)setPrimitiveSafeToDeleteOnStartup:(NSNumber*)value;

- (BOOL)primitiveSafeToDeleteOnStartupValue;
- (void)setPrimitiveSafeToDeleteOnStartupValue:(BOOL)value_;




- (NSString*)primitiveStatus;
- (void)setPrimitiveStatus:(NSString*)value;




- (NSString*)primitiveTitle;
- (void)setPrimitiveTitle:(NSString*)value;




- (NSDate*)primitiveUploadDate;
- (void)setPrimitiveUploadDate:(NSDate*)value;




- (NSNumber*)primitiveVersion;
- (void)setPrimitiveVersion:(NSNumber*)value;

- (int64_t)primitiveVersionValue;
- (void)setPrimitiveVersionValue:(int64_t)value_;




- (NSNumber*)primitiveVersionIsLatest;
- (void)setPrimitiveVersionIsLatest:(NSNumber*)value;

- (BOOL)primitiveVersionIsLatestValue;
- (void)setPrimitiveVersionIsLatestValue:(BOOL)value_;





- (ADXUser*)primitiveAuthor;
- (void)setPrimitiveAuthor:(ADXUser*)value;



- (NSMutableSet*)primitiveCollections;
- (void)setPrimitiveCollections:(NSMutableSet*)value;



- (ADXDocumentContent*)primitiveContent;
- (void)setPrimitiveContent:(ADXDocumentContent*)value;



- (ADXHighlightedContent*)primitiveHighlightedContent;
- (void)setPrimitiveHighlightedContent:(ADXHighlightedContent*)value;



- (ADXMetaDocument*)primitiveMetaDocument;
- (void)setPrimitiveMetaDocument:(ADXMetaDocument*)value;



- (ADXDocumentThumbnail*)primitiveThumbnail;
- (void)setPrimitiveThumbnail:(ADXDocumentThumbnail*)value;


@end
