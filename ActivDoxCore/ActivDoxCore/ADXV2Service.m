 //
//  ADXV2Service.m
//  ActivDoxCore
//
//  Created by Gavin McKenzie on 2012-12-05.
//  Copyright (c) 2012 ActivDox Inc. All rights reserved.
//

#import "ADXV2Service.h"
#import "ADXCommonMacros.h"
#import "ADXV2ServiceClient.h"
#import "ADXHTTPClient.h"
#import "ADXHighlightedContentHelper.h"

#if !TARGET_OS_IPHONE
#import <unistd.h>
#endif

NSString * const kADXServiceURLRequestUnauthorizedNotification = @"kADXV2ServiceURLRequestUnauthorizedNotification";

NSString * const kADXServiceDocumentDownloadedNotification = @"kADXServiceDocumentDownloadedNotification";
NSString * const kADXServiceDocumentThumbnailDownloadedNotification = @"kADXServiceDocumentThumbnailDownloadedNotification";
NSString * const kADXServiceDocumentThumbnailNotDownloadedNotification = @"kADXServiceDocumentThumbnailNotDownloadedNotification";
NSString * const kADXServiceDocumentChangesDownloadedNotification = @"kADXServiceDocumentChangesDownloadedNotification";
NSString * const kADXServiceDocumentChangesNotDownloadedNotification = @"kADXServiceDocumentChangesNotDownloadedNotification";
NSString * const kADXServiceDocumentNotesDownloadedNotification = @"kADXServiceDocumentNotesDownloadedNotification";
NSString * const kADXServiceDocumentNotesNotDownloadedNotification = @"kADXServiceDocumentNotesNotDownloadedNotification";
NSString * const kADXServiceAllNotesDownloadedNotification = @"kADXServiceAllNotesDownloadedNotification";
NSString * const kADXServiceCollectionRefreshCompletedNotification = @"kADXServiceContainerRefreshedCompletedNotification";
NSString * const kADXServiceAllCollectionsRefreshCompletedNotification = @"kADXServiceAllCollectionsRefreshCompletedNotification";
NSString * const kADXServiceAllDocumentsRefreshCompletedNotification = @"kADXServiceAllDocumentsRefreshCompletedNotification";
NSString * const kADXServiceAllEventsRefreshCompletedNotification = @"kADXServiceAllEventsRefreshCompletedNotification";
NSString * const kADXServiceNotificationPayload = @"kADXServiceNotificationPayload";
NSString * const kADXServiceNotificationProgressUpdate = @"kADXServiceNotificationProgressUpdate";
NSString * const kADXServiceNotificationProgressStarted = @"kADXServiceNotificationProgressStarted";
NSString * const kADXServiceNotificationProgressUpdateFinished = @"kADXServiceNotificationProgressUpdateFinished";
NSString * const kADXServiceNotificationProgressUpdateUserInfoBytesReadKey = @"kADXServiceNotificationProgressUpdateUserInfoBytesReadKey";
NSString * const kADXServiceNotificationProgressUpdateUserInfoBytesExpectedKey = @"kADXServiceNotificationProgressUpdateUserInfoBytesExpectedKey";
NSString * const kADXServiceNotificationReachabilityChanged = @"kADXServiceNotificationReachabilityChanged";
NSString * const kADXServiceNotificationLatestDocumentVersionChanged = @"kADXServiceNotificationLatestDocumentVersionChanged";
NSString * const kADXServiceNotificationDocumentInaccessible = @"kADXServiceNotificationDocumentInaccessible";
NSString * const kADXServiceNotificationAPNSTokenReceived = @"kADXServiceNotificationAPNSTokenReceived";

NSString * const kADXServiceConflictingNoteChangesDiscardedNotification = @"kADXServiceConflictingNoteChangesDiscardedNotification";

NSString * const kADXServiceServerInfoNone = @"none";
NSString * const kADXServiceServerInfoMin = @"min";
NSString * const kADXServiceServerInfoFull = @"full";

NSString * const kADXServiceServerInfoAPNSProduction = @"production";
NSString * const kADXServiceServerInfoAPNSSandbox = @"sandbox";

static NSMutableDictionary *ADXServiceRegistry = nil;

static NSTimeInterval kADXServiceThrottleSeconds = 3.0f;

@interface ADXV2Service ()
@property (strong, nonatomic, readwrite) NSManagedObjectContext *managedObjectContext;
@property (strong, nonatomic) ADXV2ServiceClient *serviceClient;
@property (assign, nonatomic) BOOL installedReachabilityBlock;
@property (nonatomic, assign, readwrite) BOOL serverReachable;

@property (assign, nonatomic) BOOL hasUploadedDeviceToken;
@property (strong, nonatomic) ADXAccount *account;
@property (strong, nonatomic) NSString *stashedPassword;

@property (strong, nonatomic) NSNotificationQueue *downloadNotificationQueue;
@property (strong, nonatomic) NSTimer *periodicRefreshTimer;

@property (assign, nonatomic) BOOL syncQueueProcessingInProgress;
@property (assign, nonatomic) BOOL collectionsRefreshInProgress;
@property (assign, nonatomic) BOOL allDocumentsRefreshInProgress;
@property (assign, nonatomic) BOOL allNotesRefreshOrUploadInProgress;
@property (assign, nonatomic) BOOL eventSubmissionInProgress;
@property (assign, nonatomic) BOOL eventRefreshInProgress;
@property (strong, nonatomic) NSMutableDictionary *collectionsUpdating;
@property (strong, nonatomic) NSMutableDictionary *documentsUpdating;
@property (strong, nonatomic) NSMutableDictionary *documentVersionHistoryUpdating;
@property (strong, nonatomic) NSMutableDictionary *documentContentsUpdating;
@property (strong, nonatomic) NSMutableDictionary *documentNotesUpdating;
@property (strong, nonatomic) NSMutableDictionary *documentThumbnailsUpdating;
@property (strong, nonatomic) NSCache *throttleRegistry;

@property (assign, nonatomic) BOOL forceNextRefreshCollections;
@property (assign, nonatomic) BOOL forceNextRefreshDocuments;
@property (assign, nonatomic) BOOL forceNextRefreshDocumentsInCollection;

@end

@implementation ADXV2Service

#pragma mark - Class Methods

+ (ADXV2Service *)createServiceForServerURL:(NSURL *)serverURL parentManagedObjectContext:(NSManagedObjectContext *)moc
{
    ADXV2ServiceClient *serviceClient = [[ADXV2ServiceClient alloc] initWithBaseURL:serverURL userID:nil];
    ADXV2Service *service = [[ADXV2Service alloc] initWithServiceClient:serviceClient parentManagedObjectContext:moc];
    
    return service;
}

+ (ADXV2Service *)createServiceForAccount:(ADXAccount *)account parentManagedObjectContext:(NSManagedObjectContext *)moc
{
    NSString *userID = account.user.id;
    ADXV2ServiceClient *serviceClient = [[ADXV2ServiceClient alloc] initWithBaseURL:[NSURL URLWithString:account.serverURL] userID:userID];
    ADXV2Service *service = [[ADXV2Service alloc] initWithServiceClient:serviceClient parentManagedObjectContext:moc];
    
    return service;
}

+ (ADXV2Service *)serviceForAccountID:(NSString *)accountID
{
    ADXV2Service *service = [ADXServiceRegistry objectForKey:accountID];
    return service;
}

+ (ADXV2Service *)serviceForAccount:(ADXAccount *)account
{
    ADXV2Service *service = [ADXV2Service serviceForAccountID:account.id];
    return service;
}

+ (void)registerService:(ADXV2Service *)service forAccountID:(NSString *)accountID
{
    if (!ADXServiceRegistry) {
        ADXServiceRegistry = [NSMutableDictionary dictionary];
    }
    
    [ADXServiceRegistry setObject:service forKey:accountID];
}

+ (NSDictionary *)registeredServices
{
    return [NSDictionary dictionaryWithDictionary:ADXServiceRegistry];
}

+ (void)deregisterServiceForAccount:(NSString *)accountID
{
    ADXV2Service *service = [ADXV2Service serviceForAccountID:accountID];
    if (!service) {
        LogError(@"coulding deregister service for account %@", accountID);
        return;
    }
    
    [service teardownPeriodicRefresh];
    
    [ADXServiceRegistry removeObjectForKey:accountID];
}

+ (BOOL)deregisterService:(ADXV2Service *)service
{
    __block BOOL result = NO;
    NSDictionary *registeredServicesCopy = [NSDictionary dictionaryWithDictionary:ADXServiceRegistry];
    
    [registeredServicesCopy enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
        NSString *accountID = key;
        ADXV2Service *registeredService = obj;
        
        if (service == registeredService) {
            [self deregisterServiceForAccount:accountID];
            result = YES;
            *stop = YES;
        }
    }];
    
    return result;
}

+ (void)deregisterAllServices
{
    NSDictionary *registeredServicesCopy = [NSDictionary dictionaryWithDictionary:ADXServiceRegistry];
    
    [registeredServicesCopy enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
        NSString *accountID = key;
        
        [self deregisterServiceForAccount:accountID];
    }];
}

+ (void)saveContextsForAllRegisteredServices;
{
    NSArray *allRegisteredServices = [ADXServiceRegistry allValues];
    for (ADXV2Service *service in allRegisteredServices) {
        [service saveContexts:^(BOOL success, NSError *error) {
            if (!success) {
                LogNSError(@"Couldn't save service context", error);
            }
        }];
    }
}

+ (ADXV2Service *)serviceForManagedObject:(NSManagedObject *)managedObject
{
    ADXV2Service *result = nil;
    NSString *accountID = nil;
    
    if ([managedObject isKindOfClass:[ADXDocument class]]) {
        accountID = [(ADXDocument *)managedObject accountID];
    } else if ([managedObject isKindOfClass:[ADXCollection class]]) {
        accountID = [(ADXCollection *)managedObject accountID];
    } else if ([managedObject isKindOfClass:[ADXUser class]]) {
        accountID = ((ADXUser *)managedObject).account.id;
    }

    if (accountID) {
        result = [ADXV2Service serviceForAccountID:accountID];
    }

    return result;
}

#pragma mark - Initialization and Destruction

- (id)init
{
    self = [super init];
    if (self) {
        
    }
    return self;
}

- (id)initWithServiceClient:(id)serviceClient parentManagedObjectContext:(NSManagedObjectContext *)moc
{
    self = [super init];
    if (self) {
        _serviceClient = serviceClient;
        _automaticallyDownloadDocumentContent = YES;
        
        _managedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
        _managedObjectContext.parentContext = moc;
        
        _downloadNotificationQueue = [[NSNotificationQueue alloc] initWithNotificationCenter:[NSNotificationCenter defaultCenter]];
        _collectionsUpdating = [[NSMutableDictionary alloc] init];
        _documentsUpdating = [[NSMutableDictionary alloc] init];
        _documentContentsUpdating = [[NSMutableDictionary alloc] init];

        _throttleRegistry = [[NSCache alloc] init];

        [self watchNotifications];
        
    }
    return self;
}

- (void)dealloc
{
    [self unwatchNotifications];
}

#pragma mark - Notifications

- (void)watchNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleServiceURLRequestSucceededNotification:)
                                                 name:kADXV2ServiceURLRequestSucceededNotification
                                               object:self.serviceClient];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleServiceURLRequestFailedNotification:)
                                                 name:kADXV2ServiceURLRequestFailedNotification
                                               object:self.serviceClient];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleSyncQueueEntrySavedNotification:)
                                                 name:kADXSyncQueueEntryDidSaveNotification
                                               object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleRemoteNotificationEvent:)
                                                 name:kADXRemoteNotificationEvent
                                               object:nil];
  
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleManagedObjectContextDidSaveNotification:)
                                                 name:NSManagedObjectContextDidSaveNotification
                                               object:self.managedObjectContext.parentContext];
    
    NSString *didBecomeActiveNotificationName = nil;
#if TARGET_OS_IPHONE
    didBecomeActiveNotificationName = UIApplicationDidBecomeActiveNotification;
#else
    didBecomeActiveNotificationName = NSApplicationDidBecomeActiveNotification;
#endif
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleDidBecomeActiveNotification:)
                                                 name:didBecomeActiveNotificationName
                                               object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleServiceNotificationAPNSTokenReceived:)
                                                 name:kADXServiceNotificationAPNSTokenReceived
                                               object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleServiceProcessDownloadQueueNotification:)
                                                 name:kADXV2ServiceProcessDownloadQueueNotification
                                               object:nil];
}

- (void)unwatchNotifications
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kADXV2ServiceURLRequestSucceededNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kADXV2ServiceURLRequestFailedNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kADXSyncQueueEntryDidSaveNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kADXRemoteNotificationEvent object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:NSManagedObjectContextDidSaveNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kADXServiceNotificationAPNSTokenReceived object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kADXV2ServiceProcessDownloadQueueNotification object:nil];
    
    NSString *didBecomeActiveNotificationName = nil;
#if TARGET_OS_IPHONE
    didBecomeActiveNotificationName = UIApplicationDidBecomeActiveNotification;
#else
    didBecomeActiveNotificationName = NSApplicationDidBecomeActiveNotification;
#endif
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:didBecomeActiveNotificationName object:nil];
}

- (void)handleServiceURLRequestSucceededNotification:(NSNotification *)notification
{
    [self setServerReachable:YES];
}

- (void)handleServiceURLRequestFailedNotification:(NSNotification *)notification
{
    LogError(@"Service received kADXV2ServiceURLRequestFailedNotification");
    [self setServerReachable:NO];
}

- (void)handleSyncQueueEntrySavedNotification:(NSNotification *)notification
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [self submitAnyQueuedSyncEntries];
    });
}

- (void)handleRemoteNotificationEvent:(NSNotification *)notification
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [self refreshCollections:^(BOOL success, NSError *error) {
            if (success) {
                [self refreshDocuments:nil downloadContent:YES trackProgress:NO];
                [self refreshNotes:nil];
            }
        }];
    });
}

- (void)handleManagedObjectContextDidSaveNotification:(NSNotification *)notification
{
    if (notification.object != self.managedObjectContext) {
        [self.managedObjectContext performBlock:^{
            [self.managedObjectContext mergeChangesFromContextDidSaveNotification:notification];
        }];
    }
}

- (void)handleDidBecomeActiveNotification:(NSNotification *)notification
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [self refreshCollections:^(BOOL success, NSError *error) {
            if (success) {
                [self refreshDocuments:nil downloadContent:YES trackProgress:NO];
                [self refreshNotes:nil];
            }
        }];
    });
}

- (void)handleServiceNotificationAPNSTokenReceived:(NSNotification *)notification
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        if (!self.hasUploadedDeviceToken && self.hasAccessToken) {
            // APNS token received after we've already logged in; upload it now
            [self uploadDeviceTokens:^(BOOL success, NSError *error) {
                if (success) {
                    self.hasUploadedDeviceToken = YES;
                } else {
                    LogNSError(@"startService unable to upload device token.", error);
                }
            }];
        }
    });
}

- (void)handleServiceProcessDownloadQueueNotification:(NSNotification *)notification
{
    [self checkForPendingDocumentContentDownloads];
}

- (void)postServiceNotification:(NSString *)type payload:(id)payload
{
    __weak ADXV2Service *weakSelf = self;

    dispatch_async(dispatch_get_main_queue(), ^{
        NSDictionary *payloadDictionary = nil;
        if (payload) {
            payloadDictionary = [NSDictionary dictionaryWithObject:payload forKey:kADXServiceNotificationPayload];
        }
        [[NSNotificationCenter defaultCenter] postNotificationName:type
                                                            object:weakSelf
                                                          userInfo:payloadDictionary];
    });
}

#pragma mark - Reachability

- (void)installReachabilityBlock
{
    __weak ADXV2Service *weakSelf = self;
    
    @synchronized(self) {
        if (!self.installedReachabilityBlock) {
            self.installedReachabilityBlock = YES;
            [self.serviceClient setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                    [weakSelf postServiceNotification:kADXServiceNotificationReachabilityChanged payload:nil];
                    if (status == AFNetworkReachabilityStatusReachableViaWiFi || status == AFNetworkReachabilityStatusReachableViaWWAN) {
                        int64_t delayInSeconds = 5.0;
                        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
                        dispatch_after(popTime, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
                            [weakSelf restartService:nil];
                        });
                    }
                });
            }];
        }
    }
}

- (BOOL)networkReachable
{
    BOOL result = NO;
    AFNetworkReachabilityStatus status = [self.serviceClient networkReachabilityStatus];
    if (status == AFNetworkReachabilityStatusReachableViaWiFi || status == AFNetworkReachabilityStatusReachableViaWWAN) {
        result = YES;
    }
    return result;
}

- (void)setServerReachable:(BOOL)serverReachable
{
    __weak ADXV2Service *weakSelf = self;
    @synchronized(self) {
        if (_serverReachable != serverReachable) {
            _serverReachable = serverReachable;
            [weakSelf postServiceNotification:kADXServiceNotificationReachabilityChanged payload:nil];
        }
    }
}

#pragma mark - Periodic Refresh

- (void)setupPeriodicRefresh
{
    if (self.periodicRefreshTimer) {
        [self.periodicRefreshTimer invalidate];
    }
    
    BOOL allowPeriodicRefresh = [[ADXUserDefaults sharedDefaults] allowPeriodicRefresh];
    NSUInteger periodicRefreshSeconds = [[ADXUserDefaults sharedDefaults] periodicRefreshSeconds];
    if (allowPeriodicRefresh && periodicRefreshSeconds) {
        LogInfo(@"periodic refresh timer set to %d", (int)periodicRefreshSeconds);
        self.periodicRefreshTimer = [NSTimer timerWithTimeInterval:periodicRefreshSeconds target:self selector:@selector(periodicRefreshTimerFired:) userInfo:nil repeats:YES];
        [[NSRunLoop mainRunLoop] addTimer:self.periodicRefreshTimer forMode:NSDefaultRunLoopMode];
    } else {
        LogTrace(@"periodic refresh disallowed");
    }
}

- (void)teardownPeriodicRefresh
{
    if (self.periodicRefreshTimer) {
        [self.periodicRefreshTimer invalidate];
    }
    self.periodicRefreshTimer = nil;
}

- (void)refresh
{
    __weak ADXV2Service *weakSelf = self;
    LogTrace(@"Performing generic server refresh");
    
    [self submitAnyQueuedSyncEntries];
    [self refreshCollections:^(BOOL success, NSError *error) {
        if (success) {
            [weakSelf refreshDocuments:nil downloadContent:YES trackProgress:NO];
            [weakSelf refreshNotes:nil];
        }
    }];
    [weakSelf refreshEvents:nil];
}

- (void)periodicRefreshTimerFired:(NSTimer*)theTimer
{
    __weak ADXV2Service *weakSelf = self;
    LogTrace(@"periodicRefreshTimerFired");

    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [weakSelf refresh];
    });
}

#pragma mark - Accessors

- (NSDictionary *)loginResponse
{
    return self.serviceClient.loginResponse;
}

- (NSString *)loginResponseAccountID
{
    NSDictionary *loginResponse = self.loginResponse;
    NSString *result = [loginResponse objectForKey:kADXV2ServiceJSONKeyID];
    return result;
}

- (NSString *)accessToken
{
    NSString *result = [self.loginResponse objectForKey:kADXServiceJSONKeyAccessToken];
    return result;
}

- (BOOL)hasAccessToken
{
    BOOL result = YES;
    if (IsEmpty(self.accessToken)) {
        result = NO;
    }
    return result;
}

#pragma mark - Helpers

- (void)mostRecentUpdateForAllCollections:(ADXServiceDateResultCompletionBlock)completionBlock
{
    __weak ADXV2Service *weakSelf = self;
    __weak NSManagedObjectContext *moc = self.managedObjectContext;
    
    [moc performBlock:^{
        ADXAccount *account = weakSelf.account;
        NSDate *mostRecentUpdate = account.mostRecentAllCollectionsUpdate;

        LogTrace(@"Update timestamp for all-collections-refresh on account %@ is %@", account.id, mostRecentUpdate);

        if (completionBlock) {
            // Don't dispatch; just invoke the completion block directly.
            if (weakSelf.forceNextRefreshCollections) {
                weakSelf.forceNextRefreshCollections = NO;
                completionBlock(nil);
            } else {
                completionBlock(mostRecentUpdate);
            }
        }
    }];
}

- (void)advanceMostRecentUpdateTimeStampForAllCollections:(NSDate *)timestamp completion:(ADXServiceCompletionBlock)completionBlock
{
    __weak ADXV2Service *weakSelf = self;
    __weak NSManagedObjectContext *moc = self.managedObjectContext;
    
    [moc performBlock:^{
        ADXAccount *account = weakSelf.account;
        account.mostRecentAllCollectionsUpdate = timestamp;

        LogTrace(@"Advanced update timestamp for all-collections-refresh on account %@ to %@", account.id, timestamp);

        // The caller is responsible for saving the context.
        if (completionBlock) {
            // Don't dispatch; just invoke the completion block directly.
            completionBlock(YES, nil);
        }
    }];
}

- (void)mostRecentUpdateForAllDocuments:(ADXServiceDateResultCompletionBlock)completionBlock
{
    __weak ADXV2Service *weakSelf = self;
    __weak NSManagedObjectContext *moc = self.managedObjectContext;
    
    [moc performBlock:^{
        ADXAccount *account = weakSelf.account;
        NSDate *mostRecentUpdate = account.mostRecentAllDocumentsUpdate;

        LogTrace(@"Update timestamp for all-documents-refresh on account %@ is %@", account.id, mostRecentUpdate);

        if (completionBlock) {
            // Don't dispatch; just invoke the completion block directly.
            if (weakSelf.forceNextRefreshDocuments) {
                weakSelf.forceNextRefreshDocuments = NO;
                completionBlock(nil);
            } else {
                completionBlock(mostRecentUpdate);
            }
        }
    }];
}

- (void)advanceMostRecentUpdateTimeStampForAllDocuments:(NSDate *)timestamp completion:(ADXServiceCompletionBlock)completionBlock
{
    __weak ADXV2Service *weakSelf = self;
    __weak NSManagedObjectContext *moc = self.managedObjectContext;
        
    [moc performBlock:^{
        ADXAccount *account = weakSelf.account;
        account.mostRecentAllDocumentsUpdate = timestamp;

        LogTrace(@"Advanced update timestamp for all-documents-refresh on account %@ to %@", account.id, timestamp);

        // The caller is responsible for saving the context.
        if (completionBlock) {
            // Don't dispatch; just invoke the completion block directly.
            completionBlock(YES, nil);
        }
    }];
}

- (void)mostRecentUpdateForAllNotes:(ADXServiceDateResultCompletionBlock)completionBlock
{
    __weak ADXV2Service *weakSelf = self;
    __weak NSManagedObjectContext *moc = self.managedObjectContext;
    
    [moc performBlock:^{
        ADXAccount *account = weakSelf.account;
        NSDate *mostRecentUpdate = account.mostRecentAllNotesUpload;

        LogTrace(@"Update timestamp for all-notes-refresh on account %@ is %@", account.id, mostRecentUpdate);

        if (completionBlock) {
            // Don't dispatch; just invoke the completion block directly.
            completionBlock(mostRecentUpdate);
        }
    }];
}

- (void)advanceMostRecentUpdateTimeStampForAllNotes:(NSDate *)timestamp completion:(ADXServiceCompletionBlock)completionBlock
{
    __weak ADXV2Service *weakSelf = self;
    __weak NSManagedObjectContext *moc = self.managedObjectContext;
    
    [moc performBlock:^{
        ADXAccount *account = weakSelf.account;
        account.mostRecentAllNotesUpload = timestamp;

        LogTrace(@"Advanced update timestamp for all-notes-refresh on account %@ to %@", account.id, timestamp);

        // The caller is responsible for saving the context.
        if (completionBlock) {
            // Don't dispatch; just invoke the completion block directly.
            completionBlock(YES, nil);
        }
    }];
}

- (void)mostRecentUploadForAllNotes:(ADXServiceDateResultCompletionBlock)completionBlock
{
    __weak ADXV2Service *weakSelf = self;
    __weak NSManagedObjectContext *moc = self.managedObjectContext;
    
    [moc performBlock:^{
        ADXAccount *account = weakSelf.account;
        NSDate *mostRecentUpload = account.mostRecentAllNotesUpload;
        
        LogTrace(@"Upload timestamp for all-notes-refresh on account %@ is %@", account.id, mostRecentUpload);
        
        if (completionBlock) {
            // Don't dispatch; just invoke the completion block directly.
            completionBlock(mostRecentUpload);
        }
    }];
}

- (void)advanceMostRecentUploadTimeStampForAllNotes:(NSDate *)timestamp completion:(ADXServiceCompletionBlock)completionBlock
{
    __weak ADXV2Service *weakSelf = self;
    __weak NSManagedObjectContext *moc = self.managedObjectContext;
    
    [moc performBlock:^{
        ADXAccount *account = weakSelf.account;
        account.mostRecentAllNotesUpload = timestamp;
        
        LogTrace(@"Advanced upload timestamp for all-notes-refresh on account %@ to %@", account.id, timestamp);
        
        // The caller is responsible for saving the context.
        if (completionBlock) {
            // Don't dispatch; just invoke the completion block directly.
            completionBlock(YES, nil);
        }
    }];
}

- (void)mostRecentUpdateForDocumentNotes:(NSString *)documentID completion:(ADXServiceDateResultCompletionBlock)completionBlock
{
    __weak ADXV2Service *weakSelf = self;
    __weak NSManagedObjectContext *moc = self.managedObjectContext;
    
    [moc performBlock:^{
        NSDate *mostRecentUpdate = nil;
        ADXMetaDocument *metaDocument = [ADXMetaDocument existingMetaDocumentWithID:documentID accountID:weakSelf.account.id context:moc];
        if (!metaDocument) {
            LogError(@"Couldn't locate meta-document for document ID %@", documentID);
        } else {
            mostRecentUpdate = metaDocument.mostRecentNotesUpdate;
            LogTrace(@"Update timestamp for notes refresh on document %@ is %@", documentID, mostRecentUpdate);
        }        
        
        if (completionBlock) {
            // Don't dispatch; just invoke the completion block directly.
            completionBlock(mostRecentUpdate);
        }
    }];
}

- (void)advanceMostRecentUpdateTimeStamp:(NSDate *)timestamp forDocumentNotes:(NSString *)documentID completion:(ADXServiceCompletionBlock)completionBlock
{
    __weak ADXV2Service *weakSelf = self;
    __weak NSManagedObjectContext *moc = self.managedObjectContext;
    
    [moc performBlock:^{
        ADXMetaDocument *metaDocument = [ADXMetaDocument existingMetaDocumentWithID:documentID accountID:weakSelf.account.id context:moc];
        if (!metaDocument) {
            LogError(@"Couldn't locate meta-document for document ID %@", documentID);
        } else {
            metaDocument.mostRecentNotesUpdate = timestamp;
            LogTrace(@"Advanced update timestamp for notes refresh on document %@ to %@", documentID, timestamp);
        }
        
        // The caller is responsible for saving the context.
        if (completionBlock) {
            // Don't dispatch; just invoke the completion block directly.
            completionBlock(YES, nil);
        }
    }];
}

- (void)mostRecentUpdateForCollection:(NSString *)collectionID completion:(ADXServiceDateResultCompletionBlock)completionBlock
{
    __weak ADXV2Service *weakSelf = self;
    __weak NSManagedObjectContext *moc = self.managedObjectContext;

    [moc performBlock:^{
        NSDate *mostRecentUpdate = nil;
        ADXCollection *collection = [ADXCollection collectionWithID:collectionID accountID:weakSelf.account.id context:moc];
        mostRecentUpdate = collection.mostRecentRefresh;

        LogTrace(@"Update timestamp for collection %@ is %@", collectionID, mostRecentUpdate);

        if (completionBlock) {
            // Don't dispatch; just invoke the completion block directly.
            if (weakSelf.forceNextRefreshDocumentsInCollection) {
                weakSelf.forceNextRefreshDocumentsInCollection = NO;
                completionBlock(nil);
            } else {
                completionBlock(mostRecentUpdate);
            }
        }
    }];
}

- (void)advanceMostRecentUpdateTimeStampForCollections:(NSArray *)collectionIDs timestamp:(NSDate *)timestamp completion:(ADXServiceCompletionBlock)completionBlock
{
    __weak ADXV2Service *weakSelf = self;
    __weak NSManagedObjectContext *moc = self.managedObjectContext;
    
    [moc performBlock:^{
        for (NSString *collectionID in collectionIDs) {
            
            ADXCollection *collection = [ADXCollection collectionWithID:collectionID accountID:weakSelf.account.id context:moc];
            collection.mostRecentRefresh = timestamp;

            LogTrace(@"Advanced update timestamp on collection %@ to %@", collectionID, timestamp);

        }
        // The caller is responsible for saving the context.
        if (completionBlock) {
            // Don't dispatch; just invoke the completion block directly.
            completionBlock(YES, nil);
        }
    }];
}

- (void)mostRecentUpdateForAllEvents:(ADXServiceIDResultCompletionBlock)completionBlock
{
    __weak ADXV2Service *weakSelf = self;
    __weak NSManagedObjectContext *moc = self.managedObjectContext;
    
    [moc performBlock:^{
        ADXAccount *account = weakSelf.account;
        NSString *mostRecentUpdate = account.mostRecentAllEventsUpdateID;
        
        LogTrace(@"Update timestamp for all-events-refresh on account %@ is %@", account.id, mostRecentUpdate);
        
        if (completionBlock) {
            // Don't dispatch; just invoke the completion block directly.
            completionBlock(mostRecentUpdate);
        }
    }];
}

- (void)advanceMostRecentUpdateForAllEvents:(NSString *)eventID timestamp:(NSDate *)timestamp completion:(ADXServiceCompletionBlock)completionBlock
{
    __weak ADXV2Service *weakSelf = self;
    __weak NSManagedObjectContext *moc = self.managedObjectContext;
    
    [moc performBlock:^{
        ADXAccount *account = weakSelf.account;
        
        account.mostRecentAllEventsUpdate = timestamp;
        account.mostRecentAllEventsUpdateID = eventID;
        LogTrace(@"Advanced update timestamp for all-events-refresh on account %@ to %@", account.id, eventID);
        
        // The caller is responsible for saving the context.
        if (completionBlock) {
            // Don't dispatch; just invoke the completion block directly.
            completionBlock(YES, nil);
        }
    }];
}

- (void)saveContexts:(ADXServiceCompletionBlock)completionBlock
{
    __weak ADXV2Service *weakSelf = self;
    __weak NSManagedObjectContext *moc = self.managedObjectContext;

    [moc performBlock:^{
        NSError *saveError = nil;
        BOOL saveSuccess = [moc save:&saveError];
        if (!saveSuccess) {
            LogNSError(@"couldn't save service context", saveError);
            if (completionBlock) {
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                    completionBlock(NO, saveError);
                });
            }
            return;
        }
        [[moc parentContext] performBlock:^{
            NSError *parentSaveError = nil;
            BOOL parentSaveSuccess = [[weakSelf.managedObjectContext parentContext] save:&parentSaveError];
            if (!parentSaveSuccess) {
                LogNSError(@"couldn't save parent context", parentSaveError);
                if (completionBlock) {
                    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                        completionBlock(NO, parentSaveError);
                    });
                }
                return;
            }
            if (completionBlock) {
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                    completionBlock(YES, nil);
                });
            }
        }];
    }];
}

#pragma mark - Service Startup

- (void)restartService:(ADXServiceCompletionBlock)completionBlock
{
    __weak ADXV2Service *weakSelf = self;
    __weak NSManagedObjectContext *moc = self.managedObjectContext;
    
    [moc performBlock:^{
        [weakSelf startServiceForAccount:self.account password:self.stashedPassword completion:completionBlock];
    }];
}

- (void)startServiceForAccount:(ADXAccount *)account password:(NSString *)password completion:(ADXServiceCompletionBlock)completionBlock;
{
    __weak ADXV2Service *weakSelf = self;
    __weak NSManagedObjectContext *moc = self.managedObjectContext;
    
    [moc performBlock:^{
        self.account = (ADXAccount *)[self.managedObjectContext existingObjectWithID:account.objectID error:nil];

        if (weakSelf.hasAccessToken && weakSelf.hasUploadedDeviceToken) {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                [weakSelf performPostStartupTasks];
            });
            if (completionBlock) {
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                    completionBlock(YES, nil);
                });
            }
            return;
        }
        
        weakSelf.stashedPassword = password;
        
        [weakSelf getAccessToken:^(NSUInteger statusCode, BOOL success, NSError *error) {
            if (success) {
                [weakSelf uploadDeviceTokens:^(BOOL success, NSError *error) {
                    if (success) {
                        weakSelf.hasUploadedDeviceToken = YES;
                    } else {
                        LogNSError(@"startService unable to upload device token.", error);
                        weakSelf.hasUploadedDeviceToken = NO;
                    }
                    
                    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                        [weakSelf performPostStartupTasks];
                    });
                    
                    if (completionBlock) {
                        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                            completionBlock(success, error);
                        });
                    }
                }];
            } else {
                LogNSError(@"Attempt to obtain access token failed.", error);
                if (completionBlock) {
                    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                        completionBlock(success, error);
                    });
                }
            }
        }];
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            [weakSelf installReachabilityBlock];
        });
    }];
}

- (void)performPostStartupTasks
{
    [self submitAnyQueuedEvents];
    [self submitAnyQueuedSyncEntries];
    [self refreshEvents:nil];
    [self setupPeriodicRefresh];
    [self checkForPendingDocumentContentDownloads];
}

#pragma mark - User Registration and Authentication

- (void)getAccessTokenForLoginName:(NSString *)loginName password:(NSString *)password completion:(ADXServiceCompletionBlockWithStatusCode)completionBlock
{
    LogInfo(@"Attempting to handshake with server (get access token).");
    
    [self.serviceClient getAccessTokenForLoginName:loginName password:password completion:^(NSInteger statusCode, id response, NSError *error) {
        if (error) {
            LogNSError(@"startService unable to obtain access token.", error);
        } else {
            // Do nothing
        }
        if (completionBlock) {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                completionBlock(statusCode, (error ? NO : YES), error);
            });
        }
    }];
}

- (void)getAccessToken:(ADXServiceCompletionBlockWithStatusCode)completionBlock
{
    __weak ADXV2Service *weakSelf = self;
    __weak NSManagedObjectContext *moc = self.managedObjectContext;
    
    [moc performBlock:^{
        NSString *loginName = weakSelf.account.loginName;
        NSString *password = weakSelf.account.password;
        
        if (IsEmpty(password)) {
            password = weakSelf.stashedPassword;
        }
        
        [weakSelf getAccessTokenForLoginName:loginName password:password completion:completionBlock];
    }];

}

- (void)reauthenticateWithLoginName:(NSString *)loginName password:(NSString *)password completion:(ADXServiceCompletionBlockWithStatusCode)completionBlock
{
    __weak ADXV2Service *weakSelf = self;
    __weak NSManagedObjectContext *moc = self.managedObjectContext;
    
    [moc performBlock:^{
        ADXAccount *account = weakSelf.account;
        account.loginName = loginName;
        account.password = password;
        
        [weakSelf getAccessTokenForLoginName:loginName password:password completion:completionBlock];
    }];
}

- (void)registerUserWithLoginName:(NSString *)loginName
                            email:(NSString *)emailAddress
                         password:(NSString *)password
                       completion:(ADXServiceCompletionBlockWithResponse)completionBlock
{
    [self.serviceClient registerUserWithLoginName:loginName email:emailAddress password:password completion:^(NSInteger statusCode, id response, NSError *error){
        if (completionBlock) {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                completionBlock(response, (error ? NO : YES), error);
            });
        }
    }];
}

- (void)resetPasswordForEmail:(NSString *)email
                   completion:(ADXServiceCompletionBlockWithResponse)completionBlock
{
    [self.serviceClient resetPasswordForEmail:email
                                   completion:^(NSInteger statusCode, id response, NSError *error) {
                                       completionBlock(response, error ? NO : YES, error);
    }];
}

#pragma mark - Server Info

- (void)getServerInfo:(NSString *)details completion:(ADXServiceCompletionBlockWithResponse)completionBlock
{
    [self.serviceClient getServerInfo:details completion:^(NSInteger statusCode, id response, NSError *error) {
        if (completionBlock) {
            BOOL success = IsHTTP2XXSuccess(statusCode);
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                completionBlock(response, success, error);
            });
        }
    }];
}

- (void)getServerInfoAPNSMode:(ADXServiceCompletionBlockWithResponse)completionBlock
{
    [self getServerInfo:kADXServiceServerInfoMin completion:^(id response, BOOL success, NSError *error) {
        if (success && [response isKindOfClass:[NSDictionary class]]) {
            NSString *apnsMode = response[@"notificationMode"];
            
            if (completionBlock) {
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                    completionBlock(apnsMode, success, error);
                });
            }
        } else {
            if (completionBlock) {
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                    completionBlock(nil, NO, error);
                });
            }
        }
    }];
}

#pragma mark - APNS

#if TARGET_OS_MAC
NSString *getHostUUID()
{
    uuid_t u;
    struct timespec t = { 0, 0 };
    char hwuuid[36];
    
    gethostuuid(u, &t);
    uuid_unparse_upper(u, hwuuid);
    
    return [NSString stringWithCString:hwuuid encoding:NSUTF8StringEncoding];
}
#endif

- (void)uploadDeviceTokens:(ADXServiceCompletionBlock)completionBlock
{
    __weak ADXV2Service *weakSelf = self;
    
    NSData *currentToken = [[ADXUserDefaults sharedDefaults] currentDeviceToken];

#if TARGET_OS_IPHONE
    NSString *APNSDeviceID = [UIDevice currentDevice].identifierForVendor.UUIDString;
#else
    NSString *APNSDeviceID = getHostUUID();
#endif
    
    if (IsEmpty(currentToken)) {
        LogError(@"startService did not send device token to server because token is nil");
        if (completionBlock) {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                completionBlock(NO, nil);
            });
        }
    } else {
        [weakSelf updateDeviceToken:currentToken APNSDeviceID:APNSDeviceID completion:^(BOOL success, NSError *error) {
            if (error) {
                LogNSError(@"startService unable to update device token with server.", error);
            } else {
                LogInfo(@"Successfully registered device token with server");
            }
            if (completionBlock) {
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                    completionBlock(success, error);
                });
            }
        }];
    }
}

- (void)updateDeviceToken:(NSData *)newToken APNSDeviceID:(NSString *)APNSDeviceID completion:(ADXServiceCompletionBlock)completionBlock
{
//    if (!self.networkReachable) {
//        NSError *error = [NSError errorWithDomain:NSURLErrorDomain code:NSURLErrorNotConnectedToInternet userInfo:nil];
//        if (completionBlock) {
//            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
//                completionBlock(NO, error);
//            });
//        }
//        return;
//    }
    
    NSString *str1 = [NSString stringWithFormat:@"%@", newToken];
    NSString *newString = [str1 stringByReplacingOccurrencesOfString:@" " withString:@""];
    newString = [newString stringByReplacingOccurrencesOfString:@"<" withString:@""];
    newString = [newString stringByReplacingOccurrencesOfString:@">" withString:@""];
    
    NSLog(@"updateDeviceToken: uploading token %@ for device %@", newString, APNSDeviceID);
    
    [self.serviceClient updateDeviceToken:newToken APNSDeviceID:APNSDeviceID completion:^(NSInteger statusCode, id response, NSError *error) {
        if (completionBlock) {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                completionBlock((error ? NO : YES), error);
            });
        }
    }];
}

#pragma mark - Collections

- (void)refreshCollections:(ADXServiceCompletionBlock)completionBlock
{
    __weak ADXV2Service *weakSelf = self;

    @synchronized(self) {
        if (self.collectionsRefreshInProgress) {
            // Update already in progress. Do nothing.
            if (completionBlock) {
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                    completionBlock(NO, nil);
                });
            }
            return;
        } else {
            self.collectionsRefreshInProgress = YES;
        }
    }
    
    [self mostRecentUpdateForAllCollections:^(NSDate *date) {
        NSDate *currentUpdate = [NSDate date];

        // Currently, the server doesn't do anything with the since param on /collections; and we want all collections
        // returned in the response anwyay so that we can detect collections that have been removed from the server.
        // So, we pass nil in as the since value.
        [weakSelf.serviceClient getCollectionsSince:nil completion:^(NSInteger statusCode, id response, NSError *error) {
            if (!IsHTTP2XXSuccess(statusCode)) {
                weakSelf.collectionsRefreshInProgress = NO;
                LogNSError(@"refreshCollections failed", error);
                if (completionBlock) {
                    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                        completionBlock(NO, nil);
                    });
                }
                return;
            } else {
                NSArray *collections = ((NSDictionary *)response)[ADXJSONCollectionsResponseAttributes.collections];

                LogTrace(@"Refreshed collections: %@", collections);

                if (IsEmpty(collections)) {
                    weakSelf.collectionsRefreshInProgress = NO;
                    [weakSelf postServiceNotification:kADXServiceAllCollectionsRefreshCompletedNotification payload:nil];
                    
                    if (completionBlock) {
                        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                            completionBlock(YES, nil);
                        });
                    }
                    return;
                } else {
                    [weakSelf advanceMostRecentUpdateTimeStampForAllCollections:currentUpdate completion:^(BOOL success, NSError *error) {
                        BOOL updateSuccess = [ADXCollection updateCollections:collections accountID:weakSelf.account.id removeMissingCollections:YES context:weakSelf.managedObjectContext];
                        if (updateSuccess) {
                            [weakSelf saveContexts:^(BOOL success, NSError *error) {
                                if (!success) {
                                    LogNSError(@"couldn't save context", error);
                                    if (completionBlock) {
                                        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                                            completionBlock(NO, nil);
                                        });
                                    }
                                    return;
                                }
                                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                                    NSArray *collectionIDs = [collections valueForKey:ADXCollectionAttributes.id];
                                    [weakSelf advanceMostRecentUpdateTimeStampForCollections:collectionIDs timestamp:currentUpdate completion:^(BOOL success, NSError *error) {
                                        [weakSelf saveContexts:^(BOOL success, NSError *error) {
                                            if (!success) {
                                                LogNSError(@"couldn't save context", error);
                                                if (completionBlock) {
                                                    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                                                        completionBlock(NO, nil);
                                                    });
                                                }
                                                return;
                                            }
                                            weakSelf.collectionsRefreshInProgress = NO;
                                            [weakSelf postServiceNotification:kADXServiceAllCollectionsRefreshCompletedNotification payload:nil];
                                            
                                            if (completionBlock) {
                                                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                                                    completionBlock(success, error);
                                                });
                                            }
                                        }];
                                    }];
                                });
                            }];
                        } else {
                            weakSelf.collectionsRefreshInProgress = NO;
                            [weakSelf postServiceNotification:kADXServiceAllCollectionsRefreshCompletedNotification payload:nil];
                            
                            LogError(@"Couldn't update collections");
                            if (completionBlock) {
                                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                                    completionBlock(NO, nil);
                                });
                            }
                            return;
                        }
                    }];
                }
            }
        }];
    }];
}

- (void)refreshCollection:(NSString *)collectionID completion:(ADXServiceCompletionBlock)completionBlock
{
    __weak ADXV2Service *weakSelf = self;
    
    @synchronized(self) {
        if (self.collectionsUpdating[collectionID]) {
            // Update already in progress. Do nothing.
            if (completionBlock) {
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                    completionBlock(NO, nil);
                });
            }
            return;
        } else {
            self.collectionsUpdating[collectionID] = @YES;
        }
    }
    
    [self refreshDocumentsInCollection:collectionID downloadContent:YES trackProgress:YES];
    
    [self internalRefreshCollection:collectionID completion:^(BOOL success, NSError *error) {
        @synchronized(weakSelf.collectionsUpdating) {
            [weakSelf.collectionsUpdating removeObjectForKey:collectionID];
        }
        [weakSelf postServiceNotification:kADXServiceCollectionRefreshCompletedNotification payload:collectionID];
        if (completionBlock) {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                completionBlock(success, error);
            });
        }
        return;
    }];
}

- (void)internalRefreshCollection:(NSString *)collectionID completion:(ADXServiceCompletionBlock)completionBlock
{
    __weak ADXV2Service *weakSelf = self;
    __weak NSManagedObjectContext *moc = self.managedObjectContext;

    // It is important that we don't supply a 'since' timestamp, because we want the full set of collections
    // If this code changes so that we supply a since date, then take care to consider the impact on the
    // removeMissingCollections on updateCollections below to a NO value.
    [self.serviceClient getCollection:collectionID since:nil completion:^(NSInteger statusCode, id response, NSError *error) {
        if (!IsHTTP2XXSuccess(statusCode)) {
            [weakSelf postServiceNotification:kADXServiceCollectionRefreshCompletedNotification payload:collectionID];
            LogNSError(@"refreshCollection failed", error);
            if (completionBlock) {
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                    completionBlock(NO, nil);
                });
            }
        } else {
            [moc performBlock:^{
                BOOL updateCollectionSuccess = [ADXCollection updateCollections:@[response] accountID:weakSelf.account.id removeMissingCollections:NO context:weakSelf.managedObjectContext];
                if (!updateCollectionSuccess) {
                    LogError(@"Couldn't update collection %@", collectionID);
                    if (completionBlock) {
                        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                            completionBlock(NO, nil);
                        });
                    }
                    return;
                }
                
                NSArray *documents = ((NSDictionary *)response)[ADXJSONCollectionAttributes.documentsArray];
                NSArray *accessibleDocuments = [ADXDocument onlyAccessibleDocumentsFrom:documents];
                ADXCollection *collection = [ADXCollection collectionWithID:collectionID accountID:weakSelf.account.id context:weakSelf.managedObjectContext];

                if (IsEmpty(accessibleDocuments)) {
                    LogTrace(@"Removed all documents from collection %@", collectionID);
                    collection.documents = nil;
                } else {
                    NSMutableSet *documentSet = [[NSMutableSet alloc] init];
                    
                    for (NSDictionary *documentDict in accessibleDocuments) {
                        NSString *documentID = [documentDict aps_objectForKeyOrNil:ADXJSONDocumentAttributes.id];
                        ADXDocument *document = [ADXDocument latestDocumentWithID:documentID accountID:weakSelf.account.id context:weakSelf.managedObjectContext];
                        [documentSet addObject:document];
                    }
                    
                    [collection setDocuments:documentSet];
                }
                [weakSelf saveContexts:^(BOOL success, NSError *error) {
                    if (completionBlock) {
                        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                            completionBlock(success, error);
                        });
                    } 
                }];
            }];
        }
    }];
}

- (void)forceRefreshCollectionsAndDocuments:(ADXServiceCompletionBlock)refreshOnlyCompletionBlock downloadContent:(BOOL)downloadContent trackProgress:(BOOL)trackProgress
{
    self.forceNextRefreshCollections = YES;
    self.forceNextRefreshDocuments = YES;
    [self refreshCollectionsAndDocuments:refreshOnlyCompletionBlock downloadContent:downloadContent trackProgress:trackProgress];
}

- (void)refreshCollectionsAndDocuments:(ADXServiceCompletionBlock)refreshOnlyCompletionBlock downloadContent:(BOOL)downloadContent trackProgress:(BOOL)trackProgress
{
    __weak ADXV2Service *weakSelf = self;
    
    [self refreshCollections:^(BOOL success, NSError *error) {
        if (success) {
            [weakSelf refreshDocuments:refreshOnlyCompletionBlock downloadContent:downloadContent trackProgress:trackProgress];
        } else {
            if (refreshOnlyCompletionBlock) {
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                    refreshOnlyCompletionBlock(NO, nil);
                });
            }
            LogNSError(@"refreshCollectionsAndDocuments failed", error);
        }
    }];
}

#pragma mark - Documents

- (void)refreshDocuments:(ADXServiceCompletionBlock)refreshOnlyCompletionBlock downloadContent:(BOOL)downloadContent trackProgress:(BOOL)trackProgress
{
    __weak ADXV2Service *weakSelf = self;
    __weak NSManagedObjectContext *moc = self.managedObjectContext;
    
    @synchronized(self) {
        if (self.allDocumentsRefreshInProgress) {
            // Update already in progress. Do nothing.
            return;
        } else {
            self.allDocumentsRefreshInProgress = YES;
        }
    }
    
    [self mostRecentUpdateForAllDocuments:^(NSDate *date) {        
        NSDate *previousUpdate = date;
        NSDate *currentUpdate = [NSDate date];
        
        [weakSelf.serviceClient getDocumentsSince:previousUpdate completion:^(NSInteger statusCode, id response, NSError *error) {
            if (!IsHTTP2XXSuccess(statusCode)) {
                weakSelf.allDocumentsRefreshInProgress = NO;
                [weakSelf postServiceNotification:kADXServiceAllDocumentsRefreshCompletedNotification payload:nil];
                
                if (refreshOnlyCompletionBlock) {
                    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                        refreshOnlyCompletionBlock(NO, nil);
                    });
                }
                LogNSError(@"refreshDocuments failed", error);
                return;
            } else {
                NSArray *documents = ((NSDictionary *)response)[ADXJSONDocumentsResponseAttributes.documents];
                LogTrace(@"Refreshed documents: %@", documents);
                
                if (IsEmpty(documents)) {
                    weakSelf.allDocumentsRefreshInProgress = NO;
                    [weakSelf postServiceNotification:kADXServiceAllDocumentsRefreshCompletedNotification payload:nil];
                    if (refreshOnlyCompletionBlock) {
                        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                            refreshOnlyCompletionBlock(YES, nil);
                        });
                    }
                    return;
                } else {
                    [moc performBlock:^{
                        NSArray *users = ((NSDictionary *) response)[ADXJSONDocumentsResponseAttributes.users];
                        if (users.count) {
                            BOOL updateUsersSuccess = [ADXUser addUsers:users updateExisting:YES context:moc];
                            if (!updateUsersSuccess) {
                                LogError(@"couldn't update users returned from a documents refresh");
                            }
                        }
                        
                        NSArray *collections = ((NSDictionary *) response)[ADXJSONDocumentsResponseAttributes.collections];
                        if (collections.count) {
                            BOOL updateCollectionsSuccess = [ADXCollection updateCollections:collections accountID:weakSelf.account.id removeMissingCollections:NO context:moc];
                            if (!updateCollectionsSuccess) {
                                LogError(@"couldn't update collections returned from a documents refresh");
                            }
                        }

                        BOOL updateSuccess = [ADXDocument updateDocuments:documents accountID:weakSelf.account.id context:moc];
                        if (updateSuccess) {
                            [weakSelf advanceMostRecentUpdateTimeStampForAllDocuments:currentUpdate completion:^(BOOL success, NSError *error) {
                                NSArray *newerDocuments = [ADXDocument makeDocumentsLatestIfNewer:documents accountID:weakSelf.account.id context:moc];
                                if (newerDocuments == nil) {
                                    LogError(@"refreshDocuments received an error from makeDocumentsLatestIfNewer");
                                }
                                NSArray *inaccessibleDocuments = [ADXDocument onlyInaccessibleDocumentsFrom:documents];
                                
                                [weakSelf saveContexts:^(BOOL success, NSError *error) {
                                    weakSelf.allDocumentsRefreshInProgress = NO;
                                    [weakSelf postServiceNotification:kADXServiceAllDocumentsRefreshCompletedNotification payload:nil];
                                    
                                    for (NSDictionary *inaccessibleDocument in inaccessibleDocuments) {
                                        [weakSelf postServiceNotification:kADXServiceNotificationDocumentInaccessible payload:inaccessibleDocument[ADXDocumentAttributes.id]];
                                    }
                                    
                                    for (NSDictionary *newerDocument in newerDocuments) {
                                        [weakSelf postServiceNotification:kADXServiceNotificationLatestDocumentVersionChanged
                                                                  payload:@{ADXDocumentAttributes.id : newerDocument[ADXDocumentAttributes.id], ADXDocumentAttributes.version : newerDocument[ADXDocumentAttributes.version]}];
                                    }
                                    
                                    if (refreshOnlyCompletionBlock) {
                                        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                                            refreshOnlyCompletionBlock(YES, nil);
                                        });
                                    }
                                    
                                    if (downloadContent) {
                                        [self checkForPendingDocumentContentDownloads];
                                    }
                                    return;
                                }];
                            }];
                        } else {
                            weakSelf.allDocumentsRefreshInProgress = NO;
                            [weakSelf postServiceNotification:kADXServiceAllDocumentsRefreshCompletedNotification payload:nil];
                            
                            LogError(@"Couldn't update documents");
                            if (refreshOnlyCompletionBlock) {
                                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                                    refreshOnlyCompletionBlock(NO, nil);
                                });
                            }
                            return;
                        }
                    }];
                }
            }
        }];
    }];
}

- (void)refreshDocumentsVersionHistory:(NSArray *)documents
{
    __weak ADXV2Service *weakSelf = self;
    
    // TODO: This may overwhelm GCD if we have lots of documents. Should probably control the rate of dispatch.
    for (NSString *documentID in documents) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            [weakSelf refreshDocumentVersions:documentID completion:nil];
        });
    }
}

- (void)refreshDocumentVersions:(NSString *)documentID completion:(ADXServiceCompletionBlock)completionBlock
{
    __weak ADXV2Service *weakSelf = self;
    __weak NSManagedObjectContext *moc = self.managedObjectContext;

    @synchronized(self) {
        if (self.documentVersionHistoryUpdating[documentID]) {
            // Update already in progress. Do nothing.
            return;
        } else {
            self.documentVersionHistoryUpdating[documentID] = @YES;
        }
    }

    [weakSelf.serviceClient getDocumentVersions:documentID completion:^(NSInteger statusCode, id response, NSError *error) {
        if (!IsHTTP2XXSuccess(statusCode)) {
            LogNSError(@"refreshDocumentVersions failed", error);
            @synchronized(weakSelf.documentVersionHistoryUpdating) {
                [weakSelf.documentVersionHistoryUpdating removeObjectForKey:documentID];
            }
            if (completionBlock) {
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                    completionBlock(NO, nil);
                });
            }
            return;
        } else {
            [moc performBlock:^{
                NSArray *documents = ((NSDictionary *)response)[ADXJSONCollectionAttributes.documentsArray];
                NSArray *accessibleDocuments = [ADXDocument onlyAccessibleDocumentsFrom:documents];
                if (IsEmpty(accessibleDocuments)) {
                    LogError(@"Received only inaccessible documents with id %@", documentID);
                    @synchronized(weakSelf.documentVersionHistoryUpdating) {
                        [weakSelf.documentVersionHistoryUpdating removeObjectForKey:documentID];
                    }
                    if (completionBlock) {
                        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                            completionBlock(NO, nil);
                        });
                    }
                    return;
                }
                
                BOOL updateSuccess = [ADXDocument updateDocuments:documents accountID:weakSelf.account.id context:weakSelf.managedObjectContext];
                if (!updateSuccess) {
                    LogError(@"Attempted to add or update versions failed for document with id %@", documentID);
                    @synchronized(weakSelf.documentVersionHistoryUpdating) {
                        [weakSelf.documentVersionHistoryUpdating removeObjectForKey:documentID];
                    }
                    if (completionBlock) {
                        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                            completionBlock(NO, nil);
                        });
                    }
                } else {
                    [weakSelf saveContexts:^(BOOL success, NSError *error) {
                        @synchronized(weakSelf.documentVersionHistoryUpdating) {
                            [weakSelf.documentVersionHistoryUpdating removeObjectForKey:documentID];
                        }
                        if (!success) {
                            LogNSError(@"couldn't save refreshed document versions", error);
                            if (completionBlock) {
                                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                                    completionBlock(success, error);
                                });
                            }
                            return;
                        } else {
                            [self.managedObjectContext performBlock:^{
                                // Check if there is a newer version of the document
                                NSNumber *highestVersionNumber = [NSNumber numberWithUnsignedInteger:0];
                                for (NSDictionary *document in documents) {
                                    NSNumber *documentVersion = document[ADXJSONDocumentAttributes.version];
                                    if ([highestVersionNumber compare:documentVersion] == NSOrderedAscending) {
                                        highestVersionNumber = documentVersion;
                                    }
                                }
                                
                                ADXDocument *latestDocument = [ADXDocument latestDocumentWithID:documentID accountID:weakSelf.account.id context:moc];
                                if ([latestDocument.version compare:highestVersionNumber] == NSOrderedAscending) {
                                    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                                        [weakSelf refreshDocument:documentID versionID:highestVersionNumber downloadContent:YES trackProgress:YES completion:completionBlock];
                                    });
                                } else {
                                    if (completionBlock) {
                                        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                                            completionBlock(success, nil);
                                        });
                                    }
                                }
                            }];
                        }
                    }];
                }
            }];
        }
    }];
}

- (void)refreshDocumentsInCollections:(NSArray *)collectionIDs downloadContent:(BOOL)downloadContent trackProgress:(BOOL)trackProgress
{
    for (NSString *collectionID in collectionIDs) {
        [self refreshDocumentsInCollection:collectionID downloadContent:downloadContent trackProgress:NO];
    }
}

- (void)forceRefreshDocumentsInCollection:(NSString *)collectionID downloadContent:(BOOL)downloadContent trackProgress:(BOOL)trackProgress
{
    self.forceNextRefreshDocumentsInCollection = YES;
    [self refreshDocumentsInCollection:collectionID downloadContent:downloadContent trackProgress:trackProgress];
}

- (void)refreshDocumentsInCollection:(NSString *)collectionID downloadContent:(BOOL)downloadContent trackProgress:(BOOL)trackProgress
{
    __weak ADXV2Service *weakSelf = self;
    __weak NSManagedObjectContext *moc = self.managedObjectContext;

    @synchronized(self) {
        if (self.collectionsUpdating[collectionID]) {
            // Update already in progress. Do nothing.
            return;
        } else {
            self.collectionsUpdating[collectionID] = @YES;
        }
    }
    
    [self mostRecentUpdateForCollection:collectionID completion:^(NSDate *date) {
        NSDate *previousUpdate = date;
        NSDate *currentUpdate = [NSDate date];

        [weakSelf.serviceClient getCollectionDocuments:collectionID since:previousUpdate completion:^(NSInteger statusCode, id response, NSError *error) {
            if (!IsHTTP2XXSuccess(statusCode)) {
                @synchronized(weakSelf.collectionsUpdating) {
                    [weakSelf.collectionsUpdating removeObjectForKey:collectionID];
                }
                [weakSelf postServiceNotification:kADXServiceCollectionRefreshCompletedNotification payload:collectionID];
                LogNSError(@"refreshDocumentsInCollection failed", error);
                return;
            } else {
                [moc performBlock:^{
                    NSArray *users = ((NSDictionary *) response)[ADXJSONCollectionAttributes.usersArray];
                    if (users.count) {
                        BOOL updateUsersSuccess = [ADXUser addUsers:users updateExisting:YES context:moc];
                        if (!updateUsersSuccess) {
                            LogError(@"couldn't update users returned from a collection documents refresh");
                        }
                    }
                    
                    NSArray *collections = ((NSDictionary *) response)[ADXJSONCollectionAttributes.collectionsArray];
                    if (collections.count) {
                        BOOL updateCollectionsSuccess = [ADXCollection updateCollections:collections accountID:weakSelf.account.id removeMissingCollections:NO context:moc];
                        if (!updateCollectionsSuccess) {
                            LogError(@"couldn't update collections returned from a collection documents refresh");
                        }
                    }
                    
                    NSArray *documents = ((NSDictionary *)response)[ADXJSONCollectionAttributes.documentsArray];
                    
                    LogTrace(@"Refreshed collection %@ documents: %@", collectionID, documents);
                    
                    if (IsEmpty(documents)) {
                        @synchronized(weakSelf.collectionsUpdating) {
                            [weakSelf.collectionsUpdating removeObjectForKey:collectionID];
                        }
                        [weakSelf postServiceNotification:kADXServiceCollectionRefreshCompletedNotification payload:collectionID];
                        return;
                    }

                    BOOL updateSuccess = [ADXDocument updateDocuments:documents accountID:weakSelf.account.id context:weakSelf.managedObjectContext];
                    if (updateSuccess) {
                        [weakSelf advanceMostRecentUpdateTimeStampForCollections:@[collectionID] timestamp:currentUpdate completion:^(BOOL success, NSError *error) {
                            [self.managedObjectContext performBlock:^{
                                NSArray *newerDocuments = [ADXDocument makeDocumentsLatestIfNewer:documents accountID:weakSelf.account.id context:moc];
                                if (newerDocuments == nil) {
                                    LogError(@"refreshDocuments received an error from makeDocumetnsLatestIfNewer");
                                }
                                NSArray *inaccessibleDocuments = [ADXDocument onlyInaccessibleDocumentsFrom:documents];
                                
                                [weakSelf saveContexts:^(BOOL success, NSError *error) {
                                    @synchronized(weakSelf.collectionsUpdating) {
                                        [weakSelf.collectionsUpdating removeObjectForKey:collectionID];
                                    }
                                    [weakSelf postServiceNotification:kADXServiceCollectionRefreshCompletedNotification payload:collectionID];
                                    
                                    for (NSDictionary *inaccessibleDocument in inaccessibleDocuments) {
                                        [weakSelf postServiceNotification:kADXServiceNotificationDocumentInaccessible payload:inaccessibleDocument[ADXDocumentAttributes.id]];
                                    }
                                    
                                    for (NSDictionary *newerDocument in newerDocuments) {
                                        [weakSelf postServiceNotification:kADXServiceNotificationLatestDocumentVersionChanged
                                                                  payload:@{ADXDocumentAttributes.id : newerDocument[ADXDocumentAttributes.id], ADXDocumentAttributes.version : newerDocument[ADXDocumentAttributes.version]}];
                                    }
                                    
                                    if (!success) {
                                        LogNSError(@"Couldn't save contexts from within refreshDocumentsInCollection", error);
                                    } else if (downloadContent) {
                                        NSArray *accessibleDocuments = [ADXDocument onlyAccessibleDocumentsFrom:documents];
                                        NSArray *documentIDs = [accessibleDocuments valueForKey:ADXDocumentAttributes.id];
                                        if (!IsEmpty(documentIDs)) {
                                            [self checkForPendingDocumentContentDownloads];
                                        }
                                    }
                                    return;
                                }];
                            }];
                        }];
                    } else {
                        @synchronized(weakSelf.collectionsUpdating) {
                            [weakSelf.collectionsUpdating removeObjectForKey:collectionID];
                        }
                        [weakSelf postServiceNotification:kADXServiceCollectionRefreshCompletedNotification payload:collectionID];
                        LogError(@"Couldn't update documents");
                        return;
                    }
                }];

            }
        }];
    }];
}

- (void)refreshDocument:(NSString *)documentID versionID:(NSNumber *)versionID downloadContent:(BOOL)downloadContent trackProgress:(BOOL)trackProgress completion:(ADXServiceCompletionBlock)completionBlock
{
    __weak ADXV2Service *weakSelf = self;
    __weak NSManagedObjectContext *moc = self.managedObjectContext;
    
    @synchronized(self) {
        if (self.documentsUpdating[documentID]) {
            // Update already in progress. Do nothing.
            return;
        } else {
            self.documentsUpdating[documentID] = @YES;
        }
    }
    
    [self.serviceClient getDocument:documentID version:versionID completion:^(NSInteger statusCode, id response, NSError *error) {
        if (!IsHTTP2XXSuccess(statusCode)) {
            LogNSError(@"refreshDocument:downloadContent: failed", error);
            @synchronized(weakSelf.documentsUpdating) {
                [weakSelf.documentsUpdating removeObjectForKey:documentID];
            }
            if (completionBlock) {
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                    completionBlock(NO, nil);
                });
            }
        } else {
            [moc performBlock:^{
                BOOL updatedVersion = NO;
                NSDictionary *documentDictionary = (NSDictionary *)response;
                BOOL updateSuccess = [ADXDocument updateDocuments:@[documentDictionary] accountID:weakSelf.account.id context:moc];                
                
                if (!updateSuccess) {
                    LogError(@"Attempted to add or update failed for document with id %@", documentID);
                    @synchronized(weakSelf.documentsUpdating) {
                        [weakSelf.documentsUpdating removeObjectForKey:documentID];
                    }
                    if (completionBlock) {
                        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                            completionBlock(NO, nil);
                        });
                    }
                } else {
                    ADXDocumentMakeLatestVersionResponse latestVersionResponse = [ADXDocument makeDocumentLatestIfNewer:documentDictionary accountID:weakSelf.account.id context:moc];
                    if (latestVersionResponse == ADXDocumentMakeLatestVersionSuccess) {
                        updatedVersion = YES;
                    } else if (latestVersionResponse == ADXDocumentMakeLatestVersionFailed) {
                        LogError(@"refreshDocument unable to make document with id %@ and version %@ the latest version", documentID, versionID);
                    }
                    
                    [weakSelf saveContexts:^(BOOL success, NSError *error) {
                        @synchronized(weakSelf.documentsUpdating) {
                            [weakSelf.documentsUpdating removeObjectForKey:documentID];
                        }

                        if (!success) {
                            LogNSError(@"couldn't save refreshed document", error);
                            if (completionBlock) {
                                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                                    completionBlock(NO, nil);
                                });
                            }
                        } else {
                            if (updatedVersion) {
                                [weakSelf postServiceNotification:kADXServiceNotificationLatestDocumentVersionChanged
                                                          payload:@{ADXDocumentAttributes.id : documentID, ADXDocumentAttributes.version : documentDictionary[ADXDocumentAttributes.version]}];
                            }
                            if (downloadContent) {
                                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                                    [weakSelf downloadContentForDocument:documentID versionID:versionID trackProgress:trackProgress completion:completionBlock];
                                });
                            } else {
                                if (completionBlock) {
                                    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                                        completionBlock(YES, nil);
                                    });
                                }
                            }
                        }
                    }];
               }
            }];
        }
    }];
}

- (void)checkForPendingDocumentContentDownloads
{
    if (self.serviceClient.downloadClientHasSpaceAvailable && self.documentContentsUpdating.count < 4) {
        // Queue these on the main thread, to ensure we're not queueing up work while the main thread is unresponsive
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{

            [self.managedObjectContext performBlock:^{
                NSArray *documentIDsToDownload = [ADXDocument documentIDsToBeDownloadedWithContext:self.managedObjectContext];
                NSUInteger numQueued = 0;
                for (NSString *documentID in documentIDsToDownload) {

                    // Check for already downloading
                    if (self.documentContentsUpdating[documentID]) {
                        continue;
                    }

                    // Check for throttle
                    NSString *throttleKey = [self documentThrottleKeyForDocument:documentID versionID:nil];
                    NSDate *lastAttempt = [self.throttleRegistry objectForKey:throttleKey];
                    NSTimeInterval secondsSinceLastAttempt = fabs([lastAttempt timeIntervalSinceNow]);
                    if (lastAttempt && secondsSinceLastAttempt < kADXServiceThrottleSeconds) {
                        continue;
                    }

                    [self downloadContentForDocument:documentID versionID:nil trackProgress:YES completion:nil];
                    
                    numQueued++;
                    if (numQueued > 3) {
                        break;
                    }
                }
            }];
        }];
    }
}


- (NSString *)documentThrottleKeyForDocument:(NSString *)documentID versionID:(NSNumber *)versionID
{
    NSString *throttleKey = [NSString stringWithFormat:@"document:%@:%@", documentID, versionID];
    return throttleKey;
}

- (void)downloadContentForDocument:(NSString *)documentID versionID:(NSNumber *)versionID trackProgress:(BOOL)trackProgress completion:(ADXServiceCompletionBlock)completionBlock
{
    // Note that NSCache is thread-safe. No need to @synchronize
    NSString *throttleKey = [self documentThrottleKeyForDocument:documentID versionID:versionID];
    NSDate *lastAttempt = [self.throttleRegistry objectForKey:throttleKey];
    NSTimeInterval secondsSinceLastAttempt = fabs([lastAttempt timeIntervalSinceNow]);
    
    if (lastAttempt && secondsSinceLastAttempt < kADXServiceThrottleSeconds) {
        LogTrace(@"Throttling request to fetch content for document %@ version %@", documentID, versionID);
        if (completionBlock) {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                completionBlock(NO, nil);
            });
        }
        return;
    }
    
    __weak ADXV2Service *weakSelf = self;
    __weak NSManagedObjectContext *moc = self.managedObjectContext;
    
    [moc performBlock:^{
        ADXMetaDocument *metaDocument = [ADXMetaDocument metaDocumentWithID:documentID accountID:weakSelf.account.id context:moc];
        if (metaDocument.doNotAutomaticallyDownloadContent) {
            if (completionBlock) {
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                    completionBlock(NO, nil);
                });
            }
        } else {
            [weakSelf forceDownloadContentForDocument:documentID versionID:versionID trackProgress:trackProgress completion:completionBlock];
        }
    }];
}

- (void)forceDownloadContentForDocument:(NSString *)documentID versionID:(NSNumber *)versionID trackProgress:(BOOL)trackProgress completion:(ADXServiceCompletionBlock)completionBlock
{
    __weak ADXV2Service *weakSelf = self;
    __weak NSManagedObjectContext *moc = self.managedObjectContext;
    __block ADXV2ServiceDownloadProgressBlock progressBlock = nil;
    
    @synchronized(self) {
        if (self.documentContentsUpdating[documentID]) {
            // Update already in progress. Do nothing.
            if (completionBlock) {
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                    completionBlock(NO, nil);
                });
            }
            return;
        } else {
            self.documentContentsUpdating[documentID] = @YES;
        }
    }
    
    if (trackProgress) {
        __block CFAbsoluteTime lastUpdateTime = CFAbsoluteTimeGetCurrent();
        
        
        progressBlock = ^(NSUInteger bytesRead, long long totalBytesRead, long long totalBytesExpectedToRead) {
            // Throttle updates to 4 per second for this download
            CFAbsoluteTime now = CFAbsoluteTimeGetCurrent();
            if (now < (lastUpdateTime + 0.25)) {
                return;
            }
            lastUpdateTime = now;
            
            [moc performBlock:^{
                [weakSelf.downloadNotificationQueue
                 enqueueNotification:[NSNotification
                                      notificationWithName:kADXServiceNotificationProgressUpdate
                                      object:self
                                      userInfo:[NSDictionary dictionaryWithObjectsAndKeys:
                                                documentID, ADXDocumentAttributes.id,
                                                [NSNumber numberWithLongLong:totalBytesRead], kADXServiceNotificationProgressUpdateUserInfoBytesReadKey,
                                                [NSNumber numberWithLongLong:totalBytesExpectedToRead], kADXServiceNotificationProgressUpdateUserInfoBytesExpectedKey,
                                                nil]]
                 postingStyle:NSPostNow];
            }];
        };
    }

    ADXV2ServiceRequestCompletionBlock operationCompletionBlock = ^(NSInteger statusCode, id response, NSError *error) {
        if (!IsHTTP2XXSuccess(statusCode) || IsEmpty(response)) {
            LogError(@"downloadContentForDocument:versionID:trackProgress: failed.");
            @synchronized(weakSelf.documentContentsUpdating) {
                [weakSelf.documentContentsUpdating removeObjectForKey:documentID];
            }
            if (completionBlock) {
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                    completionBlock(NO, nil);
                });
            }
            return;
        } else {
            [moc performBlock:^{
                ADXDocument *document = nil;
                if (!IsEmpty(versionID)) {
                    document = [ADXDocument documentWithID:documentID version:versionID accountID:weakSelf.account.id context:moc];
                } else {
                    document = [ADXDocument latestDocumentWithID:documentID accountID:weakSelf.account.id context:moc];
                }
                
                NSAssert(document.managedObjectContext == self.managedObjectContext, @"Unexpected context");
                
                if (!document) {
                    LogError(@"couldn't retrieve document to update content");
                    @synchronized(weakSelf.documentContentsUpdating) {
                        [weakSelf.documentContentsUpdating removeObjectForKey:documentID];
                    }

                    if (completionBlock) {
                        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                            completionBlock(NO, nil);
                        });
                    }
                    return;
                }
                
                ADXDocumentContent *documentContent = document.content;
                if (!documentContent) {
                    documentContent = [ADXDocumentContent insertInManagedObjectContext:moc];
                }
                documentContent.data = [NSData dataWithData:response];
                document.content = documentContent;
                document.downloaded = [NSNumber numberWithBool:YES];

                LogTrace(@"Inserted content of length %ld on document with id %@", (long)[documentContent.data length], documentID);

                [weakSelf saveContexts:^(BOOL success, NSError *error) {
                    [self.managedObjectContext performBlock:^{
                        @synchronized(weakSelf.documentContentsUpdating) {
                            [weakSelf.documentContentsUpdating removeObjectForKey:documentID];
                        }
                        
                        [self checkForPendingDocumentContentDownloads];

                        if (!success) {
                            LogNSError(@"couldn't save downloaded document content", error);
                            
                            if (completionBlock) {
                                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                                    completionBlock(NO, error);
                                });
                            }
                            return;
                        }
                        
                        if (document.changeCountValue) {
                            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                                [weakSelf downloadChangesForDocument:documentID versionID:versionID completion:nil];
                            });
                        }
                        if (document.noteCountValue) {
                            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                                [weakSelf refreshNotesForDocument:documentID versionID:versionID completion:nil];
                            });
                        }
                        
                        [weakSelf.downloadNotificationQueue
                         enqueueNotification:[NSNotification
                                              notificationWithName:kADXServiceNotificationProgressUpdateFinished
                                              object:self
                                              userInfo:[NSDictionary dictionaryWithObjectsAndKeys:
                                                        documentID, ADXDocumentAttributes.id,
                                                        nil]]
                         postingStyle:NSPostNow];
                        
                        [weakSelf postServiceNotification:kADXServiceDocumentDownloadedNotification payload:document.id];
                        [weakSelf emitEventDidDownloadDocument:document.id versionID:document.version completion:nil];
                        
                        if (completionBlock) {
                            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                                completionBlock(YES, nil);
                            });
                        }
                    }];
                }];
            }];
        }
    };

    NSString *throttleKey = [self documentThrottleKeyForDocument:documentID versionID:versionID];
    [self.throttleRegistry setObject:[NSDate date] forKey:throttleKey];

    [moc performBlock:^{
        ADXDocument *document = nil;
        if (!IsEmpty(versionID)) {
            document = [ADXDocument documentWithID:documentID version:versionID accountID:weakSelf.account.id context:moc];
        } else {
            document = [ADXDocument latestDocumentWithID:documentID accountID:weakSelf.account.id context:moc];
        }
        ADXDocumentContent *content = document.content;
        ADXDocumentThumbnail *thumbnail = document.thumbnail;
        
        if (IsEmpty(content.data)) {
            AFHTTPRequestOperation *operation = [weakSelf.serviceClient getContentOperationForDocumentID:documentID
                                                                                             version:versionID
                                                                                  backgroundDownload:YES
                                                                                            progress:progressBlock
                                                                                          completion:operationCompletionBlock];
            
            NSLog(@"downloadContentForDocument queueing operation %@ for document %@ ", operation, documentID);
            
            if (IsEmpty(thumbnail.data)) {
                [weakSelf downloadThumbnailForDocument:documentID versionID:versionID completion:nil];
            }
            [weakSelf.downloadNotificationQueue
             enqueueNotification:[NSNotification
                                  notificationWithName:kADXServiceNotificationProgressStarted
                                  object:self
                                  userInfo:[NSDictionary dictionaryWithObjectsAndKeys:
                                            documentID, ADXDocumentAttributes.id,
                                            nil]]
             postingStyle:NSPostNow];
            [weakSelf.serviceClient getDocumentContentViaOperation:operation];
        } else {
            LogInfo(@"Document with id %@ and version %@ already has content. Content download skipped.", documentID, versionID);
            @synchronized(weakSelf.documentContentsUpdating) {
                [weakSelf.documentContentsUpdating removeObjectForKey:documentID];
            }
            
            if (completionBlock) {
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                    completionBlock(YES, nil);
                });
            }
        }
    }];
    
}

- (void)excludeDocumentFromAutomaticContentDownload:(NSString *)documentID removeContentNow:(BOOL)removeContentNow completion:(ADXServiceCompletionBlock)completionBlock;
{
    __weak ADXV2Service *weakSelf = self;
    __weak NSManagedObjectContext *moc = self.managedObjectContext;

    [moc performBlock:^{
        ADXMetaDocument *metaDocument = [ADXMetaDocument metaDocumentWithID:documentID accountID:weakSelf.account.id context:moc];
        metaDocument.doNotAutomaticallyDownloadContent = @YES;

        if (removeContentNow) {
            ADXDocument *document = [ADXDocument latestDocumentWithID:documentID accountID:weakSelf.account.id context:moc];
            document.downloaded = @NO;
            ADXDocumentContent *content = document.content;
            content.data = nil;
        }
        
        [weakSelf saveContexts:^(BOOL success, NSError *error) {
            if (!success) {
                LogNSError(@"couldn't save", error);
            }
            if (completionBlock) {
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                    completionBlock(success, error);
                });
            }
        }];
    }];
}

- (void)includeDocumentInAutomaticContentDownload:(NSString *)documentID completion:(ADXServiceCompletionBlock)completionBlock
{
    __weak ADXV2Service *weakSelf = self;
    __weak NSManagedObjectContext *moc = self.managedObjectContext;
    
    [moc performBlock:^{
        ADXMetaDocument *metaDocument = [ADXMetaDocument metaDocumentWithID:documentID accountID:weakSelf.account.id context:moc];
        metaDocument.doNotAutomaticallyDownloadContent = @NO;
        [weakSelf saveContexts:^(BOOL success, NSError *error) {
            if (!success) {
                LogNSError(@"couldn't save", error);
            }
            if (completionBlock) {
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                    completionBlock(success, error);
                });
            }
        }];
    }];
}

- (void)deleteDocument:(NSString *)documentID completion:(ADXServiceCompletionBlock)completionBlock
{
    __weak ADXV2Service *weakSelf = self;
    __weak NSManagedObjectContext *moc = self.managedObjectContext;

    [self.serviceClient deleteDocument:documentID completion:^(NSInteger statusCode, id response, NSError *error) {
        if (IsHTTP2XXSuccess(statusCode)) {
            [moc performBlock:^{
                BOOL success = [ADXDocument makeDocumentInaccessible:documentID accountID:weakSelf.account.id status:ADXDocumentStatus.deleted context:moc];
                if (!success) {
                    LogError(@"Error removing documents from local cache after author-initiated deletion on document %@", documentID);
                }
                if (completionBlock) {
                    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                        completionBlock(success, error);
                    });
                }
            }];
        } else {
            if (completionBlock) {
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                    completionBlock(NO, error);
                });
            }
        }
    }];
}

- (NSString *)thumbnailThrottleKeyForDocument:(NSString *)documentID versionID:(NSNumber *)versionID
{
    NSString *throttleKey = [NSString stringWithFormat:@"thumbnail:%@:%@", documentID, versionID];
    return throttleKey;
}

- (void)downloadThumbnailForDocument:(NSString *)documentID versionID:(NSNumber *)versionID completion:(ADXServiceCompletionBlock)completionBlock
{
    // Note that NSCache is thread-safe. No need to @synchronize
    NSString *throttleKey = [self thumbnailThrottleKeyForDocument:documentID versionID:versionID];
    NSDate *lastAttempt = [self.throttleRegistry objectForKey:throttleKey];
    NSTimeInterval secondsSinceLastAttempt = fabs([lastAttempt timeIntervalSinceNow]);
    
    if (lastAttempt && secondsSinceLastAttempt < kADXServiceThrottleSeconds) {
        LogTrace(@"Throttling request to fetch thumbnail for document %@ version %@", documentID, versionID);
        if (completionBlock) {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                completionBlock(NO, nil);
            });
        }
        return;
    }
    
    [self forceDownloadThumbnailForDocument:documentID versionID:versionID completion:completionBlock];
}

- (void)forceDownloadThumbnailForDocument:(NSString *)documentID versionID:(NSNumber *)versionID completion:(ADXServiceCompletionBlock)completionBlock
{
    __weak ADXV2Service *weakSelf = self;
    __weak NSManagedObjectContext *moc = self.managedObjectContext;
    
    @synchronized(self) {
        if (self.documentThumbnailsUpdating[documentID]) {
            // Update already in progress. Do nothing.
            if (completionBlock) {
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                    completionBlock(NO, nil);
                });
            }
            return;
        } else {
            self.documentThumbnailsUpdating[documentID] = @YES;
        }
    }
    
    NSString *throttleKey = [self thumbnailThrottleKeyForDocument:documentID versionID:versionID];
    [self.throttleRegistry setObject:[NSDate date] forKey:throttleKey];
    
    [self.serviceClient getDocumentThumbnail:documentID version:versionID completion:^(NSInteger statusCode, id response, NSError *error) {
        if (!IsHTTP2XXSuccess(statusCode) || IsEmpty(response)) {
            LogError(@"downloadThumbnailForDocument:versionID: failed.");
            @synchronized(weakSelf.documentThumbnailsUpdating) {
                [weakSelf.documentThumbnailsUpdating removeObjectForKey:documentID];
            }
            [weakSelf postServiceNotification:kADXServiceDocumentThumbnailNotDownloadedNotification payload:documentID];
            if (completionBlock) {
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                    completionBlock(NO, nil);
                });
            }
            return;
        } else {
            [moc performBlock:^{
                ADXDocument *document = nil;
                if (!IsEmpty(versionID)) {
                    document = [ADXDocument documentWithID:documentID version:versionID accountID:weakSelf.account.id context:moc];
                } else {
                    document = [ADXDocument latestDocumentWithID:documentID accountID:weakSelf.account.id context:moc];
                }
                
                if (!document) {
                    LogError(@"couldn't retrieve document to update thumbnail");
                    @synchronized(weakSelf.documentThumbnailsUpdating) {
                        [weakSelf.documentThumbnailsUpdating removeObjectForKey:documentID];
                    }
                    [weakSelf postServiceNotification:kADXServiceDocumentThumbnailNotDownloadedNotification payload:documentID];
                    if (completionBlock) {
                        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                            completionBlock(NO, nil);
                        });
                    }
                    return;
                }
                
                ADXDocumentThumbnail *documentThumbnail = document.thumbnail;
                if (!documentThumbnail) {
                    documentThumbnail = [ADXDocumentThumbnail insertInManagedObjectContext:moc];
                }
                documentThumbnail.data = [NSData dataWithData:response];
                document.thumbnail = documentThumbnail;

                LogTrace(@"Inserted thumbnail content of length %ld on document with id %@", (long)[documentThumbnail.data length], documentID);

                [weakSelf saveContexts:^(BOOL success, NSError *error) {
                    @synchronized(weakSelf.documentThumbnailsUpdating) {
                        [weakSelf.documentThumbnailsUpdating removeObjectForKey:documentID];
                    }

                    if (!success) {
                        LogNSError(@"couldn't save downloaded document thumbnail", error);
                        [weakSelf postServiceNotification:kADXServiceDocumentThumbnailNotDownloadedNotification payload:documentID];
                        if (completionBlock) {
                            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                                completionBlock(NO, error);
                            });
                        }
                        return;
                    }
                                        
                    [weakSelf postServiceNotification:kADXServiceDocumentThumbnailDownloadedNotification payload:documentID];
                    if (completionBlock) {
                        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                            completionBlock(YES, nil);
                        });
                    }
                }];
            }];
        }
    }];
}

- (void)downloadChangesForDocument:(NSString *)documentID versionID:(NSNumber *)versionID completion:(ADXServiceCompletionBlock)completionBlock
{
    __weak ADXV2Service *weakSelf = self;
    __weak NSManagedObjectContext *moc = self.managedObjectContext;

    LogTrace(@"Request to download changes for document %@", documentID);

    [self.serviceClient getDocumentChanges:documentID version:versionID completion:^(NSInteger statusCode, id response, NSError *error) {
        LogTrace(@"Changes download for document %@ yielded status code %d and error %@", documentID, (int)statusCode, error);

        if (!IsHTTP2XXSuccess(statusCode) || error) {
            LogNSError(@"getDocumentChanges error:", error);
            [weakSelf postServiceNotification:kADXServiceDocumentChangesNotDownloadedNotification payload:documentID];
            if (completionBlock) {
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                    completionBlock(NO, error);
                });
            }
            return;
        }
 
        if (IsEmpty(response)) {
            LogError(@"Received status OK, but null response.");
            if (completionBlock) {
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                    completionBlock(NO, nil);
                });
            }
            return;
        }
        
        // Turn the response, which is an NSDictionary, back into JSON for storage
        if (![NSJSONSerialization isValidJSONObject:response]) {
            LogError(@"Can't serialize response to JSON: %@", response);
            if (completionBlock) {
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                    completionBlock(NO, nil);
                });
            }
            return;
        }

        NSError *jsonError = nil;
        NSData *jsonData = nil;
        @try {
            jsonData = [NSJSONSerialization dataWithJSONObject:response options:0 error:&jsonError];
        }
        @catch (NSException *exception) {
            LogError(@"Can't serialize request to JSON: %@", response);
            LogNSError(@"Error from JSON serialization attempt:", jsonError);
            @throw exception;
        }
        @finally {
            //
        }
        
        if (jsonData == nil) {
            LogError(@"Unable to serialize JSON data: %@", jsonError);
            [weakSelf postServiceNotification:kADXServiceDocumentChangesNotDownloadedNotification payload:documentID];            
            if (completionBlock) {
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                    completionBlock(NO, jsonError);
                });
            }
            return;
        }
        
        [moc performBlock:^{
            ADXDocument *document = nil;
            if (versionID) {
                document = [ADXDocument documentWithID:documentID version:versionID accountID:weakSelf.account.id context:moc];
            } else {
                document = [ADXDocument latestDocumentWithID:documentID accountID:weakSelf.account.id context:moc];
            }
            
            if (document.highlightedContent) {
                document.highlightedContent.data = jsonData;
            } else {
                ADXHighlightedContent *highlightedContent = [ADXHighlightedContent insertInManagedObjectContext:moc];
                if (highlightedContent) {
                    document.highlightedContent = highlightedContent;
                    document.highlightedContent.data = jsonData;
                } else {
                    LogError(@"Unable to insert an ADXHighlightedContent object into the current context");
                    [weakSelf postServiceNotification:kADXServiceDocumentChangesNotDownloadedNotification payload:documentID];
                    if (completionBlock) {
                        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                            completionBlock(NO, nil);
                        });
                    }
                    return;
                }
            }
            
            document.changeContentDownloadedValue = YES;

            LogTrace(@"Inserted highlighted content of length %d on document with id %@", (int)[jsonData length], document.id);
            
            [weakSelf saveContexts:^(BOOL success, NSError *error) {
                if (success) {
                    [weakSelf postServiceNotification:kADXServiceDocumentChangesDownloadedNotification payload:documentID];
                } else {
                    [weakSelf postServiceNotification:kADXServiceDocumentChangesNotDownloadedNotification payload:documentID];
                }
                if (completionBlock) {
                    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                        completionBlock(success, nil);
                    });
                }
                return;
            }];
        }];
    }];
}

- (void)downloadChangesForDocument:(NSString *)documentID versionID:(NSNumber *)versionID compare:(NSNumber *)compareVersionID completion:(ADXServiceCompletionBlockWithResponse)completionBlock
{
    LogTrace(@"Request to download changes for document %@", documentID);
    
    [self.serviceClient getDocumentChanges:documentID version:versionID compare:compareVersionID completion:^(NSInteger statusCode, id response, NSError *error) {
        LogTrace(@"Changes download for document %@ yielded status code %d and error %@", documentID, (int)statusCode, error);

        if (!IsHTTP2XXSuccess(statusCode) || error) {
            LogNSError(@"getDocumentChanges error:", error);
            if (completionBlock) {
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                    completionBlock(nil, NO, error);
                });
            }
            return;
        }

        if (IsEmpty(response)) {
            LogError(@"Received status OK, but null response.");
            if (completionBlock) {
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                    completionBlock(nil, NO, nil);
                });
            }
            return;
        }

        if (![NSJSONSerialization isValidJSONObject:response]) {
            LogError(@"Can't serialize response to JSON: %@", response);
            if (completionBlock) {
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                    completionBlock(nil, NO, nil);
                });
            }
            return;
        }

        // Turn the response, which is an NSDictionary, back into JSON for storage
        NSError *jsonError = nil;
        NSData *jsonData = nil;
        @try {
            jsonData = [NSJSONSerialization dataWithJSONObject:response options:0 error:&jsonError];
        }
        @catch (NSException *exception) {
            LogError(@"Can't serialize request to JSON: %@", response);
            LogNSError(@"Error from JSON serialization attempt:", jsonError);
            @throw exception;
        }
        @finally {
            //
        }
        
        if (jsonData == nil) {
            LogError(@"Unable to serialize JSON data: %@", jsonError);
            
            if (completionBlock) {
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                    completionBlock(response, NO, jsonError);
                });
            }
        } else {
            if (completionBlock) {
                ADXHighlightedContentHelper *documentContent = [ADXHighlightedContentHelper highlightedContentWithData:jsonData];
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                    completionBlock(documentContent, YES, nil);
                });
            }
        }
    }];
}

- (void)downloadImageAtPath:(NSString *)path completion:(ADXServiceCompletionBlockWithResponse)completionBlock
{
    LogTrace(@"Downloading image at path %@", path);
    
    [self.serviceClient downloadImageAtPath:path completion:^(NSInteger statusCode, id response, NSError *error) {
        if (IsHTTP2XXSuccess(statusCode)) {
            completionBlock(response, YES, nil);
        } else {
            completionBlock(nil, NO, error);
        }
    }];
}

- (void)changesForDocument:(NSString *)documentID versionID:(NSNumber *)versionID compare:(NSNumber *)compareVersionID completion:(ADXServiceCompletionBlockWithResponse)completionBlock
{
    __weak ADXV2Service *weakSelf = self;
    __weak NSManagedObjectContext *moc = self.managedObjectContext;
    
    [self downloadChangesForDocument:documentID versionID:versionID compare:compareVersionID completion:^(id response, BOOL success, NSError *error) {
        if (success) {
            if (completionBlock) {
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                    completionBlock(response, YES, nil);
                });
            }
        } else {
            // TODO: In the future, extend this method to check for offline cached change content between any two document versions.
            // For now, assume that we only have content for the previous version.
            if ([versionID integerValue] != ([compareVersionID integerValue] + 1)) {
                if (completionBlock) {
                    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                        completionBlock(nil, NO, nil);
                    });
                }
                return;
            }

            [moc performBlock:^{
                ADXDocument *document = nil;
                if (!IsEmpty(versionID)) {
                    document = [ADXDocument documentWithID:documentID version:versionID accountID:weakSelf.account.id context:moc];
                } else {
                    document = [ADXDocument latestDocumentWithID:documentID accountID:weakSelf.account.id context:moc];
                }
                if (document.changeContentDownloadedValue && !IsEmpty(document.highlightedContent.data)) {
                    NSData *changeContent = document.highlightedContent.data;
                    ADXHighlightedContentHelper *helper = [ADXHighlightedContentHelper highlightedContentWithData:changeContent];
                    if (completionBlock) {
                        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                            completionBlock(helper, YES, nil);
                        });
                    }
                    return;
                }
            }];
        }
    }];
}

#pragma mark - Publishing and Sharing

- (void)uploadDocumentAtURL:(NSURL *)documentURL completion:(ADXServiceCompletionBlock)completionBlock
{
    if (![[NSFileManager defaultManager] fileExistsAtPath:documentURL.path]) {
        LogError(@"uploadDocument document not found");
        if (completionBlock) {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                completionBlock(NO, nil);
            });
        }
    } else {
        [self.serviceClient uploadDocumentAtURL:documentURL
                                          title:documentURL.lastPathComponent
                                     recipients:@[ @{ @"id": self.loginResponseAccountID } ]
                                     completion:^(NSInteger statusCode, id response, NSError *error) {
                                         if (error) {
                                             LogNSError(@"uploadDocument failed", error);
                                         } else {
                                             LogInfo(@"Successfully uploaded document to server");
                                         }
                                         if (completionBlock) {
                                             dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                                                 completionBlock((error ? NO : YES), error);
                                             });
                                         }
                                     }];
    }
}


- (void)shareDocument:(ADXDocument *)document withEmailAddresses:(NSArray *)addresses message:(NSString *)message completion:(ADXServiceCompletionBlockWithResponse)completionBlock {
    // Simply call through to the serviceClient
    [self.serviceClient shareDocumentWithID:document.id withEmailAddresses:addresses message:message completion:^(NSInteger statusCode, id response, NSError *error) {
        if (completionBlock) {
            completionBlock(nil, error == nil, error);
        }
    }];
}

- (NSURL *)shareableURLForDocument:(ADXDocument *)document
{
    NSString *serverAddress = self.serviceClient.baseURL.host;
    NSNumber *port = self.serviceClient.baseURL.port;
    if (port != nil) {
        serverAddress = [NSString stringWithFormat:@"%@:%d", serverAddress, port.intValue];
    }
    
    NSString *urlString = [NSString stringWithFormat:@"adox://open/%@/%@", serverAddress, document.id];
    return [NSURL URLWithString:urlString];
}

#pragma mark - Notes

- (void)refreshNotes:(ADXServiceCompletionBlock)completionBlock
{
    __weak ADXV2Service *weakSelf = self;
    __weak NSManagedObjectContext *moc = self.managedObjectContext;
    
    @synchronized(self) {
        if (self.allNotesRefreshOrUploadInProgress) {
            // Update already in progress. Do nothing.
            return;
        } else {
            self.allNotesRefreshOrUploadInProgress = YES;
        }
    }
    
    [self mostRecentUpdateForAllNotes:^(NSDate *date) {
        NSDate *previousUpdate = date;
        NSDate *currentUpdate = [NSDate date];

        [weakSelf.serviceClient getNotesSince:previousUpdate completion:^(NSInteger statusCode, id response, NSError *error) {
            if (!IsHTTP2XXSuccess(statusCode)) {
                LogNSError(@"refreshNotes failed", error);
                @synchronized(weakSelf) {
                    weakSelf.allNotesRefreshOrUploadInProgress = NO;
                }
            } else {
                if (IsEmpty(response)) {
                    LogError(@"get notes received an empty response");
                    @synchronized(weakSelf.documentNotesUpdating) {
                        weakSelf.allNotesRefreshOrUploadInProgress = NO;
                    }
                    if (completionBlock) {
                        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                            completionBlock(NO, nil);
                        });
                    }
                    return;
                }
                
                NSArray *notes = ((NSDictionary *)response)[ADXJSONDocumentNoteAttributes.notesArray];
                if (IsEmpty(notes)) {
                    LogInfo(@"No notes found on server account %@", weakSelf.account.id);

                    @synchronized(weakSelf) {
                        weakSelf.allNotesRefreshOrUploadInProgress = NO;
                    }
                    if (completionBlock) {
                        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                            completionBlock(YES, nil);
                        });
                    }
                    return;
                }
                LogInfo(@"%lu notes found on server for account %@", (unsigned long)notes.count, weakSelf.account.id);

                [weakSelf advanceMostRecentUpdateTimeStampForAllNotes:currentUpdate completion:^(BOOL success, NSError *error) {
                    NSArray *users = [(NSDictionary *)response valueForKeyPath:ADXJSONDocumentNoteResponseAttributes.resources.usersArray];
                    if (users.count) {
                        BOOL updateUsersSuccess = [ADXUser addUsers:users updateExisting:YES context:moc];
                        if (!updateUsersSuccess) {
                            LogError(@"couldn't update users returned from a notes refresh");
                        }
                    }

                    BOOL updateSuccess = [ADXDocumentNote updateNotes:notes accountID:weakSelf.account.id context:moc];
                    
                    if (!updateSuccess) {
                        LogError(@"Failed to update notes for account %@", weakSelf.account.id);
                        @synchronized(weakSelf) {
                            weakSelf.allNotesRefreshOrUploadInProgress = NO;
                        }
                        if (completionBlock) {
                            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                                completionBlock(NO, nil);
                            });
                        }
                        return;
                    } else {
                        [weakSelf saveContexts:^(BOOL success, NSError *error) {
                            @synchronized(weakSelf) {
                                weakSelf.allNotesRefreshOrUploadInProgress = NO;
                            }
                            
                            if (!success) {
                                LogNSError(@"couldn't save notes", error);
                                if (completionBlock) {
                                    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                                        completionBlock(NO, error);
                                    });
                                }
                            } else {
                                [weakSelf postServiceNotification:kADXServiceAllNotesDownloadedNotification payload:nil];

                                [moc performBlock:^{
                                    for (NSDictionary *dictionary in notes) {
                                        NSString *noteID = [dictionary objectForKey:ADXJSONDocumentNoteAttributes.note.id];
                                        ADXDocumentNote *note = [ADXDocumentNote noteWithID:noteID accountID:weakSelf.account.id context:moc];
                                        [weakSelf postServiceNotification:kADXServiceDocumentNotesDownloadedNotification payload:note.metaDocument.documentID];
                                    }
                                }];
                                
                                if (completionBlock) {
                                    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                                        completionBlock(YES, nil);
                                    });
                                }
                            }
                        }];
                    }
                }];
            }
        }];
    }];
}

- (void)refreshNotesForDocument:(NSString *)documentID completion:(ADXServiceCompletionBlock)completionBlock
{
    __weak ADXV2Service *weakSelf = self;
    __weak NSManagedObjectContext *moc = self.managedObjectContext;
    
    @synchronized(self) {
        if (self.documentNotesUpdating[documentID]) {
            // Update already in progress. Do nothing.
            return;
        } else {
            self.documentNotesUpdating[documentID] = @YES;
        }
    }
    
    [self mostRecentUpdateForDocumentNotes:documentID completion:^(NSDate *date) {
        NSDate *previousUpdate = date;
        NSDate *currentUpdate = [NSDate date];
        
        [self.serviceClient getDocumentNotes:documentID version:nil since:previousUpdate completion:^(NSInteger statusCode, id response, NSError *error) {
            if (!IsHTTP2XXSuccess(statusCode)) {
                LogNSError(@"refreshNotesForDocument:version: failed", error);
                @synchronized(weakSelf.documentNotesUpdating) {
                    [weakSelf.documentNotesUpdating removeObjectForKey:documentID];
                }
                [weakSelf postServiceNotification:kADXServiceDocumentNotesNotDownloadedNotification payload:documentID];
                if (completionBlock) {
                    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                        completionBlock(NO, nil);
                    });
                }
                return;
            } else {
                if (IsEmpty(response)) {
                    LogError(@"get document notes received an empty response");
                    @synchronized(weakSelf.documentNotesUpdating) {
                        [weakSelf.documentNotesUpdating removeObjectForKey:documentID];
                    }
                    [weakSelf postServiceNotification:kADXServiceDocumentNotesNotDownloadedNotification payload:documentID];
                    if (completionBlock) {
                        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                            completionBlock(NO, nil);
                        });
                    }
                    return;
                }
                
                NSArray *notes = ((NSDictionary *)response)[ADXJSONDocumentNoteAttributes.notesArray];
                if (IsEmpty(notes)) {
                    LogInfo(@"No notes found on server for document %@", documentID);
                    
                    @synchronized(weakSelf.documentNotesUpdating) {
                        [weakSelf.documentNotesUpdating removeObjectForKey:documentID];
                    }
                    [weakSelf postServiceNotification:kADXServiceDocumentNotesDownloadedNotification payload:documentID];
                    if (completionBlock) {
                        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                            completionBlock(YES, nil);
                        });
                    }
                    return;
                }
                LogInfo(@"%lu notes found on server for document %@", (unsigned long)notes.count, documentID);
                
                [moc performBlock:^{
                    NSArray *users = [(NSDictionary *)response valueForKeyPath:ADXJSONDocumentNoteResponseAttributes.resources.usersArray];
                    if (users.count) {
                        BOOL updateUsersSuccess = [ADXUser addUsers:users updateExisting:YES context:moc];
                        if (!updateUsersSuccess) {
                            LogError(@"couldn't update users returned from a notes refresh");
                        }
                    }
                    
                    BOOL updateSuccess = [ADXDocumentNote updateNotes:notes accountID:weakSelf.account.id context:moc];
                    
                    if (!updateSuccess) {
                        LogError(@"Failed to update notes for document %@", documentID);
                        @synchronized(weakSelf.documentNotesUpdating) {
                            [weakSelf.documentNotesUpdating removeObjectForKey:documentID];
                        }
                        [weakSelf postServiceNotification:kADXServiceDocumentNotesNotDownloadedNotification payload:documentID];
                        if (completionBlock) {
                            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                                completionBlock(NO, nil);
                            });
                        }
                        return;
                    } else {
                        [weakSelf advanceMostRecentUpdateTimeStamp:currentUpdate forDocumentNotes:documentID completion:^(BOOL success, NSError *error) {
                            [weakSelf saveContexts:^(BOOL success, NSError *error) {
                                @synchronized(weakSelf.documentNotesUpdating) {
                                    [weakSelf.documentNotesUpdating removeObjectForKey:documentID];
                                }
                                
                                if (!success) {
                                    LogNSError(@"couldn't save notes", error);
                                    [weakSelf postServiceNotification:kADXServiceDocumentNotesNotDownloadedNotification payload:documentID];
                                    if (completionBlock) {
                                        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                                            completionBlock(NO, error);
                                        });
                                    }
                                } else {
                                    [weakSelf postServiceNotification:kADXServiceDocumentNotesDownloadedNotification
                                                              payload:documentID];
                                    if (completionBlock) {
                                        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                                            completionBlock(YES, nil);
                                        });
                                    }
                                }
                            }];
                        }];
                    }
                }];
            }
        }];
    }];
}

- (void)refreshNotesForDocument:(NSString *)documentID versionID:(NSNumber *)versionID completion:(ADXServiceCompletionBlock)completionBlock
{
    __weak ADXV2Service *weakSelf = self;
    __weak NSManagedObjectContext *moc = self.managedObjectContext;

    if (IsEmpty(versionID)) {
        [self refreshNotesForDocument:documentID completion:completionBlock];
        return;
    }
    
    [self.serviceClient getDocumentNotes:documentID version:versionID since:nil completion:^(NSInteger statusCode, id response, NSError *error) {
        if (!IsHTTP2XXSuccess(statusCode)) {
            LogNSError(@"refreshNotesForDocument:version: failed", error);
            [weakSelf postServiceNotification:kADXServiceDocumentNotesNotDownloadedNotification payload:documentID];
            if (completionBlock) {
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                    completionBlock(NO, nil);
                });
            }
            return;
        } else {
            if (IsEmpty(response)) {
                LogError(@"get document notes received an empty response");
                [weakSelf postServiceNotification:kADXServiceDocumentNotesNotDownloadedNotification payload:documentID];
                if (completionBlock) {
                    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                        completionBlock(NO, nil);
                    });
                }
                return;
            }
            
            NSArray *notes = ((NSDictionary *)response)[ADXJSONDocumentNoteAttributes.notesArray];
            if (IsEmpty(notes)) {
                LogInfo(@"No notes found on server for document %@", documentID);
                
                [weakSelf postServiceNotification:kADXServiceDocumentNotesDownloadedNotification payload:documentID];
                if (completionBlock) {
                    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                        completionBlock(YES, nil);
                    });
                }
                return;
            }
            LogInfo(@"%lu notes found on server for document %@", (unsigned long)notes.count, documentID);
            
            [moc performBlock:^{
                NSArray *users = [(NSDictionary *)response valueForKeyPath:ADXJSONDocumentNoteResponseAttributes.resources.usersArray];
                if (users.count) {
                    BOOL updateUsersSuccess = [ADXUser addUsers:users updateExisting:YES context:moc];
                    if (!updateUsersSuccess) {
                        LogError(@"couldn't update users returned from a notes refresh");
                    }
                }
                
                BOOL updateSuccess = [ADXDocumentNote updateNotes:notes accountID:weakSelf.account.id context:moc];
                
                if (!updateSuccess) {
                    LogError(@"Failed to update notes for document %@", documentID);

                    [weakSelf postServiceNotification:kADXServiceDocumentNotesNotDownloadedNotification payload:documentID];
                    if (completionBlock) {
                        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                            completionBlock(NO, nil);
                        });
                    }
                    return;
                } else {
                    [weakSelf saveContexts:^(BOOL success, NSError *error) {
                        if (!success) {
                            LogNSError(@"couldn't save notes", error);
                            [weakSelf postServiceNotification:kADXServiceDocumentNotesNotDownloadedNotification payload:documentID];
                            if (completionBlock) {
                                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                                    completionBlock(NO, error);
                                });
                            }
                        } else {
                            [weakSelf postServiceNotification:kADXServiceDocumentNotesDownloadedNotification
                                                      payload:documentID];
                            if (completionBlock) {
                                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                                    completionBlock(YES, nil);
                                });
                            }
                        }
                    }];
                }
            }];
        }
    }];
}

#pragma mark - Events

- (void)submitAnyQueuedEvents
{
    __weak ADXV2Service *weakSelf = self;
    __weak NSManagedObjectContext *moc = self.managedObjectContext;
    
    [moc performBlock:^{
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K == %@", ADXEventQueueEntryAttributes.accountID, weakSelf.account.id];
        ADXEventQueueEntry *entry = [moc aps_fetchFirstObjectForEntityName:[ADXEventQueueEntry entityName] withPredicate:predicate];
        if (entry) {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                [weakSelf submitEventsWithCompletion:nil];
            });
        }
    }];
}

- (void)refreshEvents:(ADXServiceCompletionBlock)completionBlock
{
    __weak ADXV2Service *weakSelf = self;
    __weak NSManagedObjectContext *moc = self.managedObjectContext;
    
    static NSSet *processOnlyEventTypes = nil;
    if (!processOnlyEventTypes) {
        NSArray *types = @[@"document.deleted",
                           @"document.readers.added", @"document.readers.removed",
                           @"document.closed", @"document.opened", @"document.downloaded",
                           @"document.note.added", @"document.note.deleted", @"document.note.readers.added", @"document.note.readers.removed", @"document.note.updated"];
        processOnlyEventTypes = [NSSet setWithArray:types];
    }
    
    @synchronized(self) {
        if (self.eventRefreshInProgress) {
            // Update already in progress. Do nothing.
            return;
        } else {
            self.eventRefreshInProgress = YES;
        }
    }

    [self mostRecentUpdateForAllEvents:^(NSString *identifier) {
        NSString *previousUpdate = identifier;
        
        [weakSelf.serviceClient getEventsSince:previousUpdate completion:^(NSInteger statusCode, id response, NSError *error) {
            if (!IsHTTP2XXSuccess(statusCode)) {
                @synchronized(weakSelf) {
                    weakSelf.eventRefreshInProgress = NO;
                }
                [weakSelf postServiceNotification:kADXServiceAllEventsRefreshCompletedNotification payload:nil];
                
                if (completionBlock) {
                    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                        completionBlock(NO, nil);
                    });
                }
                LogNSError(@"refreshEvents failed", error);
                return;
            } else {
                NSArray *events = ((NSDictionary *) response)[ADXJSONEventResponseAttributes.eventsDict];
                if (IsEmpty(events)) {
                    LogInfo(@"No events found on server account %@", weakSelf.account.id);
                    
                    @synchronized(weakSelf) {
                        weakSelf.eventRefreshInProgress = NO;
                    }
                    [weakSelf postServiceNotification:kADXServiceAllEventsRefreshCompletedNotification payload:nil];
                    if (completionBlock) {
                        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                            completionBlock(YES, nil);
                        });
                    }
                    return;
                }
                
                [moc performBlock:^{
                    NSArray *users = [(NSDictionary *)response valueForKeyPath:ADXJSONEventResponseAttributes.resources.usersArray];
                    if (users.count) {
                        BOOL updateUsersSuccess = [ADXUser addUsers:users updateExisting:YES context:moc];
                        if (!updateUsersSuccess) {
                            LogError(@"couldn't update users returned from an events refresh");
                        }
                    }
                    
                    LogInfo(@"%lu events found on server for account %@", (unsigned long)events.count, weakSelf.account.id);
                    
                    NSDictionary *resources = ((NSDictionary *) response)[ADXJSONEventResponseAttributes.resourcesDict];
                    NSDictionary *mostRecentEvent = [events objectAtIndex:0];
                    NSString *mostRecentEventID = mostRecentEvent[ADXJSONEventAttributes.id];
                    
                    ISO8601DateFormatter *dateFormatter = [[ISO8601DateFormatter alloc] init];
                    NSDate *mostRecentEventTimestamp = [dateFormatter dateFromString:mostRecentEvent[ADXJSONEventAttributes.timestamp]];
                    
                    [weakSelf advanceMostRecentUpdateForAllEvents:mostRecentEventID timestamp:mostRecentEventTimestamp completion:^(BOOL success, NSError *error) {
                        BOOL updateSuccess = [ADXEvent updateEvents:events resources:resources accountID:weakSelf.account.id filterOutEventsFromAccount:weakSelf.account.id onlyEventTypes:processOnlyEventTypes context:moc];
                        
                        if (!updateSuccess) {
                            LogError(@"Failed to update events for account %@", weakSelf.account.id);
                            @synchronized(weakSelf) {
                                weakSelf.eventRefreshInProgress = NO;
                            }
                            [weakSelf postServiceNotification:kADXServiceAllEventsRefreshCompletedNotification payload:nil];
                            if (completionBlock) {
                                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                                    completionBlock(NO, nil);
                                });
                            }
                            return;
                        } else {
                            [weakSelf saveContexts:^(BOOL success, NSError *error) {
                                @synchronized(weakSelf) {
                                    weakSelf.eventRefreshInProgress = NO;
                                }
                                
                                if (!success) {
                                    LogNSError(@"couldn't save events", error);
                                    [weakSelf postServiceNotification:kADXServiceAllEventsRefreshCompletedNotification payload:nil];
                                    if (completionBlock) {
                                        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                                            completionBlock(NO, error);
                                        });
                                    }
                                } else {
                                    [weakSelf postServiceNotification:kADXServiceAllEventsRefreshCompletedNotification payload:nil];
                                    if (completionBlock) {
                                        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                                            completionBlock(YES, nil);
                                        });
                                    }
                                }
                            }];
                        }
                    }];
                }];
            }
        }];
    }];
}

- (void)getEvent:(NSString *)eventID completion:(ADXServiceCompletionBlockWithResponse)completionBlock
{
    [self.serviceClient getEvent:eventID completion:^(NSInteger statusCode, id response, NSError *error) {
        if (completionBlock) {
            if (!response || error) {
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                    completionBlock(nil, NO, error);
                });
            } else {
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                    completionBlock(response, YES, nil);
                });
            }
        }
    }];
}

- (void)submitEventsWithCompletion:(ADXServiceCompletionBlock)completionBlock
{
    @synchronized(self) {
        if (self.eventSubmissionInProgress) {
            if (completionBlock) {
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                    completionBlock(NO, nil);
                });
            }
            return;
        } else {
            self.eventSubmissionInProgress = YES;
        }
    }
    
    __weak ADXV2Service *weakSelf = self;
    __weak NSManagedObjectContext *moc = self.managedObjectContext;
    
    [moc performBlock:^{
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K == %@", ADXEventQueueEntryAttributes.accountID, weakSelf.account.id];
        NSArray *events = [moc aps_fetchObjectsForEntityName:[ADXEventQueueEntry entityName] withPredicate:predicate];
        if (IsEmpty(events)) {
            weakSelf.eventSubmissionInProgress = NO;
            if (completionBlock) {
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                    completionBlock(YES, nil);
                });
            }
            return;
        }

        [weakSelf.serviceClient postEvents:events completion:^(NSInteger statusCode, id response, NSError *error) {
            LogTrace(@"postEvents returned statusCode %lu", (unsigned long)statusCode);

            // A 400 status code means that some or all of the events were refused by the server, but we should delete
            // those events regardless.
            if (!error || statusCode == 400) {
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                    [weakSelf deleteSubmittedEvents:events completion:^(BOOL success, NSError *error) {
                        weakSelf.eventSubmissionInProgress = NO;
                        if (success) {
                            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                                [weakSelf submitAnyQueuedEvents];
                            });
                        }
                        if (completionBlock) {
                            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                                completionBlock(success, error);
                            });
                        }
                    }];
                });
            } else {
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                    weakSelf.eventSubmissionInProgress = NO;
                    LogNSError(@"event submission POST failed", error);
                    if (completionBlock) {
                        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                            completionBlock(NO, error);
                        });
                    }
                });
            }
        }];
    }];
}

- (void)deleteSubmittedEvents:(NSArray *)events completion:(ADXServiceCompletionBlock)completionBlock
{
    __weak ADXV2Service *weakSelf = self;
    __weak NSManagedObjectContext *moc = self.managedObjectContext;
    
    [moc performBlock:^{
        for (ADXEventQueueEntry *event in events) {
            [moc deleteObject:event];
        }
        
        [weakSelf saveContexts:^(BOOL success, NSError *error) {
            if (!success) {
                LogError(@"Couldn't delete submitted events");
            }
            if (completionBlock) {
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                    completionBlock(success, error);
                });
            }
            return;
        }];
    }];
}

- (void)emitEventType:(NSString *)type payload:(id)payload completion:(ADXServiceCompletionBlock)completionBlock
{
    __weak ADXV2Service *weakSelf = self;
    __weak NSManagedObjectContext *moc = self.managedObjectContext;
    
    LogInfo(@"emitting event %@", type);
    
    [moc performBlock:^{
        [ADXEventQueueEntry insertEventType:type
                                    accountID:weakSelf.account.id
                                    payload:payload
                                    context:weakSelf.managedObjectContext];
        
        [weakSelf saveContexts:^(BOOL success, NSError *error) {
            if (!success) {
                LogError(@"Couldn't insert event of type %@", type);
            }
            if (completionBlock) {
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                    completionBlock(success, error);
                });
            }
            return;
        }];
    }];
}

- (void)emitEventDidOpenDocument:(NSString *)documentID versionID:(NSNumber *)versionID completion:(ADXServiceCompletionBlock)completionBlock
{
    __weak ADXV2Service *weakSelf = self;

    [self emitEventType:ADXEventType.documentOpened
                payload:[NSDictionary dictionaryWithObjectsAndKeys:
                         documentID, ADXJSONEventQueueEntryAttributes.payload.document,
                         versionID, ADXDocumentAttributes.version,
                         nil]
             completion:^(BOOL success, NSError *error) {
                 // We don't care whether success is YES, there may be old events queued.
                 [weakSelf submitAnyQueuedEvents];
                 if (completionBlock) {
                     dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                         completionBlock(success, error);
                     });
                 }
             }];
}

- (void)emitEventDidCloseDocument:(NSString *)documentID versionID:(NSNumber *)versionID completion:(ADXServiceCompletionBlock)completionBlock
{
    __weak ADXV2Service *weakSelf = self;
    
    [self emitEventType:ADXEventType.documentClosed
                payload:[NSDictionary dictionaryWithObjectsAndKeys:
                         documentID, ADXJSONEventQueueEntryAttributes.payload.document,
                         versionID, ADXDocumentAttributes.version,
                         nil]
             completion:^(BOOL success, NSError *error) {
                 // We don't care whether success is YES, there may be old events queued.
                 [weakSelf submitAnyQueuedEvents];
                 if (completionBlock) {
                     dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                         completionBlock(success, error);
                     });
                 }
             }];
}

- (void)emitEventDidDownloadDocument:(NSString *)documentID versionID:(NSNumber *)versionID completion:(ADXServiceCompletionBlock)completionBlock
{
    __weak ADXV2Service *weakSelf = self;
    
    [self emitEventType:ADXEventType.documentDownloaded
                payload:[NSDictionary dictionaryWithObjectsAndKeys:
                         documentID, ADXJSONEventQueueEntryAttributes.payload.document,
                         versionID, ADXDocumentAttributes.version,
                         nil]
             completion:^(BOOL success, NSError *error) {
                 // We don't care whether success is YES, there may be old events queued.
                 [weakSelf submitAnyQueuedEvents];
                 if (completionBlock) {
                     dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                         completionBlock(success, error);
                     });
                 }
             }];
}

- (void)emitEventDidOpenPageOfDocument:(NSString *)documentID versionID:(NSNumber *)versionID page:(NSUInteger)page completion:(ADXServiceCompletionBlock)completionBlock
{
    __weak ADXV2Service *weakSelf = self;
    
    [self emitEventType:ADXEventType.documentPageOpened
                payload:[NSDictionary dictionaryWithObjectsAndKeys:
                         documentID, ADXJSONEventQueueEntryAttributes.payload.document,
                         versionID, ADXDocumentAttributes.version,
                         [NSNumber numberWithUnsignedInteger:page], @"page",
                         nil]
             completion:^(BOOL success, NSError *error) {
                 // We don't care whether success is YES, there may be old events queued.
                 [weakSelf submitAnyQueuedEvents];
                 if (completionBlock) {
                     dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                         completionBlock(success, error);
                     });
                 }
             }];
}

- (void)emitEventDidClosePageOfDocument:(NSString *)documentID versionID:(NSNumber *)versionID page:(NSUInteger)page completion:(ADXServiceCompletionBlock)completionBlock
{
    __weak ADXV2Service *weakSelf = self;
    
    [self emitEventType:ADXEventType.documentPageClosed
                payload:[NSDictionary dictionaryWithObjectsAndKeys:
                         documentID, ADXJSONEventQueueEntryAttributes.payload.document,
                         versionID, ADXDocumentAttributes.version,
                         [NSNumber numberWithUnsignedInteger:page], @"page",
                         nil]
             completion:^(BOOL success, NSError *error) {
                 // We don't care whether success is YES, there may be old events queued.
                 [weakSelf submitAnyQueuedEvents];
                 if (completionBlock) {
                     dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                         completionBlock(success, error);
                     });
                 }
             }];
}

- (void)requestDocumentIDForEventWithID:(NSString *)eventID
                             completion:(ADXServiceCompletionBlockWithResponse)completionBlock
{
    NSAssert(completionBlock != nil, @"requestDocumentIDForEventWithID requires a completion block");
    
    [self.serviceClient getEvent:eventID completion:^(NSInteger statusCode, id response, NSError *error) {
        if (error) {
            completionBlock(nil, NO, error);
        } else {
            NSDictionary *responseDict = (NSDictionary *)response;
            NSArray *events = [responseDict valueForKey:@"events"];
            if (events != nil && events.count > 0) {
                NSDictionary *event = [events objectAtIndex:0];
                NSDictionary *payload = [event valueForKey:ADXJSONEventQueueEntryAttributes.payloadDict];
                if (payload != nil) {
                    NSString *documentID = [payload valueForKey:ADXJSONEventQueueEntryAttributes.payload.document];
                    NSString *versionID = [payload valueForKey:ADXJSONEventQueueEntryAttributes.payload.version];
                    NSDictionary *result = @{ADXDocumentAttributes.id: (IsEmpty(documentID) ? [NSNull null] : documentID),
                                             ADXDocumentAttributes.version: (IsEmpty(versionID) ? [NSNull null] : versionID)};
                    if (documentID != nil) {
                        completionBlock(result, YES, nil);
                    }
                }
            }
            
            // We don't have an associated document ID for this event
            completionBlock(nil, NO, nil);
        }
    }];
}

#pragma mark - Synchronization


//
// NOTE: All of the sync related methods are designed to operate in a manner where a single
// sync operation works to completion before the next sync operation is processed.
// All of sync related methods invoke their completion blocks directly, not via dispatch_async.
//

- (void)submitAnyQueuedSyncEntries
{
    // No need to @synchronize or set syncQueueProcessingInProgress, as this happens in submitNextSyncQueueEntry...
    if (self.syncQueueProcessingInProgress) {
        return;
    }
    
    if (!self.networkReachable || !self.serverReachable) {
        return;
    }
    
    __weak ADXV2Service *weakSelf = self;
    __weak NSManagedObjectContext *moc = self.managedObjectContext;
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [moc performBlock:^{
            NSUInteger count = [moc aps_countObjectsForEntityName:[ADXSyncQueueEntry entityName]];
            if (count) {
                [weakSelf submitNextSyncQueueEntryWithCompletion:^(BOOL success, NSError *error) {
                    NSUInteger remainder = [moc aps_countObjectsForEntityName:[ADXSyncQueueEntry entityName]];
                    if (remainder) {
                        [weakSelf submitAnyQueuedSyncEntries];
                    }
                }];
            }
        }];
    });
}

- (void)submitNextSyncQueueEntryWithCompletion:(ADXServiceCompletionBlock)completionBlock
{
    __weak ADXV2Service *weakSelf = self;
    __weak NSManagedObjectContext *moc = self.managedObjectContext;
    
    @synchronized(self) {
        if (self.syncQueueProcessingInProgress) {
            if (completionBlock) {
                completionBlock(NO, nil);
            }
            return;
        } else {
            self.syncQueueProcessingInProgress = YES;
        }
    }
        
    [moc performBlock:^{
        ADXSyncQueueEntry *entry = [moc aps_fetchFirstObjectForEntityName:[ADXSyncQueueEntry entityName] withPredicate:@"%K == %@", ADXSyncQueueEntryAttributes.accountID, weakSelf.account.id];
        if (!entry) {
            @synchronized(weakSelf) {
                weakSelf.syncQueueProcessingInProgress = NO;
            }
            // No pending sync entry fetched, so exit
            if (completionBlock) {
                completionBlock(YES, nil);
            }
        } else {
            ADXServiceCompletionBlock submitCompletionBlock = ^(BOOL success, NSError *error) {
                if (!success) {
                    LogNSError(@"A sync entry failed with error:", error);
                    LogError(@"Removing the failed sync entry from the queue. The sync entry that failed was: %@", entry);
                }
                [weakSelf deleteProcessedSyncEntry:entry completion:^(BOOL success, NSError *error) {
                    @synchronized(weakSelf) {
                        weakSelf.syncQueueProcessingInProgress = NO;
                    }
                    if (completionBlock) {
                        completionBlock(success, error);
                    }
                }];
            };
            
            if ([entry.entityClassName isEqualToString:[ADXDocumentNote entityName]]) {
                [weakSelf submitNoteSyncQueueEntry:entry completion:submitCompletionBlock];
            } else if ([entry.entityClassName isEqualToString:[ADXCollection entityName]]) {
                [weakSelf submitCollectionSyncQueueEntry:entry completion:submitCompletionBlock];
            } else if ([entry.entityClassName isEqualToString:[ADXDocument entityName]]) {
                [weakSelf submitDocumentSyncQueueEntry:entry completion:submitCompletionBlock];
            }
        }
    }];
}

- (void)deleteProcessedSyncEntry:(ADXSyncQueueEntry *)entry completion:(ADXServiceCompletionBlock)completionBlock
{
    __weak ADXV2Service *weakSelf = self;
    __weak NSManagedObjectContext *moc = self.managedObjectContext;

    [moc performBlock:^{
        [moc deleteObject:entry];
        [weakSelf saveContexts:^(BOOL success, NSError *error) {
            if (!success) {
                LogNSError(@"couldn't save deletion of sync entry", error);
            }
            if (completionBlock) {
                completionBlock(success, error);
            }
        }];
    }];
}

- (void)cancelOtherSyncEntriesLike:(ADXSyncQueueEntry *)entry completion:(ADXServiceCompletionBlock)completionBlock
{
    __weak ADXV2Service *weakSelf = self;
    __weak NSManagedObjectContext *moc = self.managedObjectContext;
    
    [moc performBlock:^{
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(%K == %@) AND (%K == %@) AND (%K == %@)",
                                  ADXSyncQueueEntryAttributes.accountID, entry.accountID,
                                  ADXSyncQueueEntryAttributes.entityClassName, entry.entityClassName,
                                  ADXSyncQueueEntryAttributes.entityID, entry.entityID];
        NSArray *entries = [moc aps_fetchObjectsForEntityName:[ADXSyncQueueEntry entityName] withPredicate:predicate];
        
        for (ADXSyncQueueEntry *pendingEntry in entries) {
            if (pendingEntry != entry) {
                LogTrace(@"Cancelling pending sync entry %@", pendingEntry);
                [moc deleteObject:pendingEntry];
            }
        }

        [weakSelf saveContexts:^(BOOL success, NSError *error) {
            if (!success) {
                LogNSError(@"couldn't save context after cancelling pending sync entries", error);
            }
            if (completionBlock) {
                completionBlock(success, error);
            }
        }];
    }];
}

- (void)submitDocumentSyncQueueEntry:(ADXSyncQueueEntry *)entry completion:(ADXServiceCompletionBlock)completionBlock
{
    __weak ADXV2Service *weakSelf = self;
    __weak NSManagedObjectContext *moc = self.managedObjectContext;

    [moc performBlock:^{
        NSString *documentID = entry.entityID;
        NSString *collectionID = nil;
        NSString *collectionLocalID = nil;
        
        if ([entry.userInfo[ADXCollectionAttributes.id] isKindOfClass:[NSString class]]) {
            collectionID = entry.userInfo[ADXCollectionAttributes.id];
        }
        if ([entry.userInfo[ADXCollectionAttributes.localID] isKindOfClass:[NSString class]]) {
            collectionLocalID = entry.userInfo[ADXCollectionAttributes.localID];
        }

        // At the time the user attempted to associate the document with a collection, the collection may not have been sync'd with
        // the server yet. So, attempt to fetch the collection from the local store and ensure it has a server-generated ID.
        ADXCollection *collection = [ADXCollection collectionWithID:collectionID orLocalID:collectionLocalID accountID:weakSelf.account.id context:moc];
        if (!collection) {
            LogError(@"Couldn't locate collection");
            if (completionBlock) {
                completionBlock(NO, nil);
            }
            return;
        }
        
        collectionID = collection.id;
        if (IsEmpty(collectionID)) {
            LogError(@"Collection does not yet have a server-generated ID.");
            if (completionBlock) {
                completionBlock(NO, nil);
            }
            return;
        }
        
        if ([entry.operation isEqualToString:ADXSyncQueueEntryOperation.link]) {
            [weakSelf.serviceClient linkDocument:documentID withCollection:collectionID completion:^(NSInteger statusCode, id response, NSError *error) {
                LogTrace(@"Request to link document %@ with collection %@ returned statusCode %d", documentID, collectionID, statusCode);
                if (IsHTTP2XXSuccess(statusCode)) {
                    if (completionBlock) {
                        completionBlock(YES, nil);
                    }
                } else {
                    LogError(@"Attempt to link document %@ with collection %@ failed", documentID, collectionID);
                    if (completionBlock) {
                        completionBlock(NO, error);
                    }
                }
            }];
        } else if ([entry.operation isEqualToString:ADXSyncQueueEntryOperation.unlink]) {
            [weakSelf.serviceClient unlinkDocument:documentID withCollection:collectionID completion:^(NSInteger statusCode, id response, NSError *error) {
                LogTrace(@"Request to unlink document %@ with collection %@ returned statusCode %d", documentID, collectionID, statusCode);
                if (IsHTTP2XXSuccess(statusCode)) {
                    if (completionBlock) {
                        completionBlock(YES, nil);
                    }
                } else {
                    LogError(@"Attempt to link document %@ with collection %@ failed", documentID, collectionID);
                    if (completionBlock) {
                        completionBlock(NO, error);
                    }
                }
            }];
        }
    }];
}

- (void)submitCollectionSyncQueueEntry:(ADXSyncQueueEntry *)entry completion:(ADXServiceCompletionBlock)completionBlock
{
    __weak ADXV2Service *weakSelf = self;
    __weak NSManagedObjectContext *moc = self.managedObjectContext;
    
    [moc performBlock:^{
        ADXCollection *collection = nil;
        if ([entry.operation isEqualToString:ADXSyncQueueEntryOperation.insert]) {
            collection = [ADXCollection collectionWithLocalID:entry.entityID accountID:entry.accountID context:moc];
        } else {
            collection = [ADXCollection collectionWithID:entry.entityID accountID:entry.accountID context:moc];
        }
        
        if (!collection) {
            LogError(@"submitCollectionSyncQueueEntry was unable to retrieve the collection referenced from sync queue entry: %@", entry);
            if (completionBlock) {
                completionBlock(NO, nil);
            }
            return;
        }
        
        NSString *operation = entry.operation;
        NSMutableDictionary *payload = entry.payload;

        if ([operation isEqualToString:ADXSyncQueueEntryOperation.insert]) {
            if (!IsEmpty(collection.id)) {
                // Treat this as a collection update operation.
                [payload setObject:collection.id forKey:ADXJSONCollectionAttributes.id];
                operation = ADXSyncQueueEntryOperation.update;
            }
        }
        
        if ([operation isEqualToString:ADXSyncQueueEntryOperation.remove]) {
            if (IsEmpty(collection.id)) {
                // Collection has never been sync'd. No need to issue a delete to the server.
                collection.safeToDeleteOnStartup = @YES;
                [self saveContexts:^(BOOL success, NSError *error) {
                    if (!success) {
                        LogNSError(@"couldn't save context after setting safeToDeleteOnStartup for collection", error);
                    }
                    if (completionBlock) {
                        completionBlock(success, (success ? nil : error));
                    }
                }];
            } else {
                [weakSelf.serviceClient deleteCollection:collection.id completion:^(NSInteger statusCode, id response, NSError *error) {
                    if (IsHTTP2XXSuccess(statusCode)) {
                        collection.safeToDeleteOnStartup = @YES;
                        [self saveContexts:^(BOOL success, NSError *error) {
                            if (!success) {
                                LogNSError(@"couldn't save context after setting safeToDeleteOnStartup for collection", error);
                            }
                            if (completionBlock) {
                                completionBlock(success, (success ? nil : error));
                            }
                        }];
                    } else {
                        LogError(@"Attempt to delete collection with ID %@ failed", collection.id);
                        if (completionBlock) {
                            completionBlock(NO, error);
                        }
                    }
                }];
            }
        } else if ([operation isEqualToString:ADXSyncQueueEntryOperation.insert] || [operation isEqualToString:ADXSyncQueueEntryOperation.update]) {
            [weakSelf.serviceClient postCollection:collection payload:payload completion:^(NSInteger statusCode, id response, NSError *error) {
                if (IsHTTP2XXSuccess(statusCode) || statusCode == 409) {  // A "409 Conflict" is also considered success; there is a newer collection on the server.
                    if (IsEmpty(response)) {
                        LogError(@"server response from collection insertion was missing");
                        if (completionBlock) {
                            completionBlock(NO, error);
                        }
                    } else {
                        [weakSelf processCollectionSyncResponse:response completion:completionBlock];
                        
                        if (statusCode == 409) {
                            LogInfo(@"Received 409 Conflict for collection with localID: %@", collection.localID);
                            [weakSelf cancelOtherSyncEntriesLike:entry completion:completionBlock];
                        }
                    }
                } else if (completionBlock) {
                    completionBlock(NO, error);
                }
            }];
        } else {
            LogError(@"Unrecognized sync operation: %@", operation);
            if (completionBlock) {
                completionBlock(NO, nil);
            }
        }
    }];
}

- (void)processCollectionSyncResponse:(NSDictionary *)collection completion:(ADXServiceCompletionBlock)completionBlock
{
    __weak ADXV2Service *weakSelf = self;
    __weak NSManagedObjectContext *moc = self.managedObjectContext;
    
    if (!collection) {
        LogInfo(@"No collection found in response");
        if (completionBlock) {
            completionBlock(YES, nil);
        }
        return;
    }
    
    [moc performBlock:^{
        BOOL updateSuccess = [ADXCollection updateCollections:@[collection] accountID:weakSelf.account.id removeMissingCollections:NO context:moc];
        
        if (!updateSuccess) {
            LogError(@"Failed to update collection");
            if (completionBlock) {
                completionBlock(NO, nil);
            }
            return;
        } else {
            [weakSelf saveContexts:^(BOOL success, NSError *error) {
                if (!success) {
                    LogNSError(@"couldn't save collection", error);
                }
                if (completionBlock) {
                    completionBlock(success, (success ? nil : error));
                }
            }];
        }
    }];
}

- (void)submitNoteSyncQueueEntry:(ADXSyncQueueEntry *)entry completion:(ADXServiceCompletionBlock)completionBlock
{
    __weak ADXV2Service *weakSelf = self;
    __weak NSManagedObjectContext *moc = self.managedObjectContext;
    
    [moc performBlock:^{
        ADXDocumentNote *note = nil;
        if ([entry.operation isEqualToString:ADXSyncQueueEntryOperation.insert]) {
            note = [ADXDocumentNote noteWithLocalID:entry.entityID accountID:entry.accountID context:moc];
        } else {
            note = [ADXDocumentNote noteWithID:entry.entityID accountID:entry.accountID context:moc];
        }
        
        if (!note) {
            LogError(@"submitDocumentNoteSyncQueueEntry was unable to retrieve the note referenced from sync queue entry: %@", entry);
            if (completionBlock) {
                completionBlock(NO, nil);
            }
            return;
        }

        NSString *operation = entry.operation;
        NSMutableDictionary *payload = entry.payload;
        
        // It's possible that the sync queue may contain several 'insert' entries and entities with only localIDs.
        // This can occur when the user creates a note and then makes a number of edits to the note, all while offline.
        // But, once the first 'insert' is processed by the server, the local version of that object will have a server-generated ID
        // and the follow-on inserts should be processed as updates to the created object on the server using the server-generated ID.
        // So, we need to check if the entity in the local store already has an ID. If it does, we know the entity exists on the server
        // and subsequent sync insert operations should be changed to updates.
        
        if ([operation isEqualToString:ADXSyncQueueEntryOperation.insert]) {
            if (!IsEmpty(note.id)) {
                // Treat this as a note update operation.
                [payload setObject:note.id forKey:ADXJSONDocumentNoteAttributes.note.id];
                operation = ADXSyncQueueEntryOperation.update;
            }
        }
        
        if ([operation isEqualToString:ADXSyncQueueEntryOperation.remove]) {
            [weakSelf.serviceClient deleteNote:note.id completion:^(NSInteger statusCode, id response, NSError *error) {
                if (IsHTTP2XXSuccess(statusCode)) {
                    note.safeToDeleteOnStartup = @YES;
                    [self saveContexts:^(BOOL success, NSError *error) {
                        if (!success) {
                            LogNSError(@"couldn't save context after setting safeToDeleteOnStartup for note", error);
                        }
                        if (completionBlock) {
                            completionBlock(success, (success ? nil : error));
                        }
                    }];
                } else {
                    LogError(@"Attempt to delete note with ID %@ failed", note.id);
                    if (completionBlock) {
                        completionBlock(NO, error);
                    }
                }
            }];
        } else if ([operation isEqualToString:ADXSyncQueueEntryOperation.insert]) {
            [weakSelf.serviceClient postNote:note payload:payload completion:^(NSInteger statusCode, id response, NSError *error) {
                if (IsHTTP2XXSuccess(statusCode) || statusCode == 409) {  // A "409 Conflict" is also considered success; there is a newer note on the server.
                    if (IsEmpty(response)) {
                        LogError(@"server response from note insertion was missing");
                        if (completionBlock) {
                            completionBlock(NO, error);
                        }
                    } else {
                        NSArray *notes = ((NSDictionary *)response)[ADXJSONDocumentNoteAttributes.notesArray];
                        [weakSelf processNoteSyncResponse:notes completion:completionBlock];

                        if (statusCode == 409) {
                            LogInfo(@"Received 409 Conflict for note with localID: %@", note.localID);
                            [weakSelf cancelOtherSyncEntriesLike:entry completion:completionBlock];

                            [weakSelf saveContexts:^(BOOL success, NSError *error) {
                                [weakSelf.managedObjectContext performBlock:^{
                                    // Notify of the conflict.  Don't need to worry about localID lookup because there
                                    // can't be a conflict if the note wasn't already sync'ed with the server.
                                    for (NSDictionary *dictionary in notes) {
                                        NSString *noteID = [dictionary objectForKey:ADXJSONDocumentNoteAttributes.note.id];
                                        [self postServiceNotification:kADXServiceConflictingNoteChangesDiscardedNotification payload:noteID];
                                    }
                                }];
                            }];
                        }
                    }
                } else if (completionBlock) {
                    completionBlock(NO, error);
                }
            }];
        } else if ([operation isEqualToString:ADXSyncQueueEntryOperation.update]) {
            [weakSelf.serviceClient updateNote:note payload:payload completion:^(NSInteger statusCode, id response, NSError *error) {
                if (IsHTTP2XXSuccess(statusCode) || statusCode == 409) {  // A "409 Conflict" is also considered success; there is a newer note on the server.
                    if (IsEmpty(response)) {
                        LogError(@"server response from note update was missing");
                        if (completionBlock) {
                            completionBlock(NO, error);
                        }
                    } else {
                        NSArray *notes = @[(NSDictionary *)response];
                        [weakSelf processNoteSyncResponse:notes completion:completionBlock];

                        if (statusCode == 409) {
                            LogInfo(@"Received 409 Conflict for note with ID: %@", note.id);
                            [weakSelf cancelOtherSyncEntriesLike:entry completion:completionBlock];

                            [weakSelf saveContexts:^(BOOL success, NSError *error) {
                                [weakSelf.managedObjectContext performBlock:^{
                                    // Notify of the conflict.  Don't need to worry about localID lookup because there
                                    // can't be a conflict if the note wasn't already sync'ed with the server.
                                    for (NSDictionary *dictionary in notes) {
                                        NSString *noteID = [dictionary objectForKey:ADXJSONDocumentNoteAttributes.note.id];
                                        [self postServiceNotification:kADXServiceConflictingNoteChangesDiscardedNotification payload:noteID];
                                    }
                                }];
                            }];
                        }
                    }
                } else if (completionBlock) {
                    completionBlock(NO, error);
                }
            }];
        } else {
            LogError(@"Unrecognized sync operation: %@", operation);
            if (completionBlock) {
                completionBlock(NO, nil);
            }
        }
    }];
}

- (void)processNoteSyncResponse:(NSArray *)notes completion:(ADXServiceCompletionBlock)completionBlock
{
    __weak ADXV2Service *weakSelf = self;
    __weak NSManagedObjectContext *moc = self.managedObjectContext;

    if (IsEmpty(notes)) {
        LogInfo(@"No notes found in response");
        if (completionBlock) {
            completionBlock(YES, nil);
        }
        return;
    }
    LogInfo(@"%lu notes received in response", (unsigned long)notes.count);
    
    [moc performBlock:^{
        BOOL updateSuccess = [ADXDocumentNote updateNotes:notes accountID:weakSelf.account.id context:moc];
        
        if (!updateSuccess) {
            LogError(@"Failed to update notes response");
            if (completionBlock) {
                completionBlock(NO, nil);
            }
            return;
        } else {
            [weakSelf saveContexts:^(BOOL success, NSError *error) {
                if (!success) {
                    LogNSError(@"couldn't save notes", error);
                }
                if (completionBlock) {
                    completionBlock(success, (success ? nil : error));
                }
            }];
        }
    }];
}

@end
