#import "_ADXCollection.h"
#import "ADXAccount.h"
#import "ADXEntityCRUDProtocol.h"

extern const struct ADXJSONCollectionsResponseAttributes {
    __unsafe_unretained NSString *collections;
} ADXJSONCollectionsResponseAttributes;

extern const struct ADXJSONCollectionAttributes {
	__unsafe_unretained NSString *id;
	__unsafe_unretained NSString *localID;
	__unsafe_unretained NSString *status;
    __unsafe_unretained NSString *title;
    __unsafe_unretained NSString *type;
    __unsafe_unretained NSString *usersArray;
    __unsafe_unretained NSString *collectionsArray;
    __unsafe_unretained NSString *documentsArray;
    struct {
        __unsafe_unretained NSString *id;
        __unsafe_unretained NSString *status;
    } documents;
} ADXJSONCollectionAttributes;

extern const struct ADXCollectionStatus {
    __unsafe_unretained NSString *accessible;
    __unsafe_unretained NSString *deleted;
} ADXCollectionStatus;

extern const struct ADXCollectionType {
    __unsafe_unretained NSString *system;
    __unsafe_unretained NSString *user;
} ADXCollectionType;

@interface ADXCollection : _ADXCollection <ADXEntityCRUDProtocol>
@property (readonly, nonatomic) BOOL isAccessible;
@property (readonly, nonatomic) BOOL isDeletedStatus; // 'isDeleted' is a property signature already used by NSManagedObject; hence why this is called 'isDeletedStatus'

+ (void)dumpInContext:(NSManagedObjectContext *)moc;

+ (id)collectionWithID:(NSString *)collectionID orLocalID:(NSString *)localID accountID:(NSString *)accountID context:(NSManagedObjectContext *)moc;
+ (id)collectionWithID:(NSString *)collectionID accountID:(NSString *)accountID context:(NSManagedObjectContext *)moc;
+ (id)collectionWithLocalID:(NSString *)localID accountID:(NSString *)accountID context:(NSManagedObjectContext *)moc;
+ (NSArray *)collectionsForAccountID:(NSString *)accountID context:(NSManagedObjectContext *)moc;
+ (ADXCollection *)collectionMatchingTitle:(NSString *)title forAccountID:(NSString *)accountID context:(NSManagedObjectContext *)moc;
+ (NSArray *)allCollectionsInContext:(NSManagedObjectContext *)moc;
+ (NSArray *)allInaccessibleCollectionssInContext:(NSManagedObjectContext *)moc objectIDsOnly:(BOOL)objectIDsOnly;
+ (NSArray *)collectionsToBeDeletedOnStartup:(NSManagedObjectContext *)moc;

+ (BOOL)updateCollections:(NSArray *)collections accountID:(NSString *)accountID removeMissingCollections:(BOOL)removeMissingCollections context:(NSManagedObjectContext *)moc;
+ (BOOL)addOrUpdateCollection:(NSDictionary *)dictionary accountID:(NSString *)accountID context:(NSManagedObjectContext *)moc;
+ (BOOL)deleteCollection:(NSString *)collectionID localID:(NSString *)localID accountID:(NSString *)accountID context:(NSManagedObjectContext *)moc;

+ (ADXCollection *)insertCollectionForAccountID:(NSString *)accountID type:(NSString *)type context:(NSManagedObjectContext *)moc;

- (NSUInteger)numberOfDocuments;
- (NSData *)thumbnail;
- (ADXAccount *)account;
- (void)markDeleted;

- (void)saveWithOptionalSync:(BOOL)queueForSync completion:(ADXEntityCRUDProtocolCompletionBlock)completionBlock;
- (void)enqueueForSyncInContext:(NSManagedObjectContext *)moc completion:(ADXEntityCRUDProtocolCompletionBlock)completionBlock;

@end
