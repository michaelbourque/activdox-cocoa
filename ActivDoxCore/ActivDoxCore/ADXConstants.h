//
//  ADXConstants.h
//  ActivDoxCore
//
//  Created by Gavin McKenzie on 12-04-30.
//  Copyright (c) 2012 ActivDox Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSInteger const kADXHttpStatusOk;


#define ADX_APP_ERROR_DOMAIN @"com.activdox.Reader.ErrorDomain"

enum {
    kADXBadServerResponseError = 1001,
};