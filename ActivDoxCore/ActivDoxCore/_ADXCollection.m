// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to ADXCollection.m instead.

#import "_ADXCollection.h"


const struct ADXCollectionAttributes ADXCollectionAttributes = {
	.accountID = @"accountID",
	.created = @"created",
	.id = @"id",
	.localID = @"localID",
	.matchingTags = @"matchingTags",
	.mostRecentRefresh = @"mostRecentRefresh",
	.safeToDeleteOnStartup = @"safeToDeleteOnStartup",
	.status = @"status",
	.title = @"title",
	.type = @"type",
};



const struct ADXCollectionRelationships ADXCollectionRelationships = {
	.documents = @"documents",
};





const struct ADXCollectionUserInfo ADXCollectionUserInfo = {
	.typeGeneric = @"0",
	.typePublished = @"2",
	.typeReceived = @"1",
};


@implementation ADXCollectionID
@end

@implementation _ADXCollection

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"ADXCollection" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"ADXCollection";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"ADXCollection" inManagedObjectContext:moc_];
}

- (ADXCollectionID*)objectID {
	return (ADXCollectionID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
	
	if ([key isEqualToString:@"safeToDeleteOnStartupValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"safeToDeleteOnStartup"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}




@dynamic accountID;






@dynamic created;






@dynamic id;






@dynamic localID;






@dynamic matchingTags;






@dynamic mostRecentRefresh;






@dynamic safeToDeleteOnStartup;



- (BOOL)safeToDeleteOnStartupValue {
	NSNumber *result = [self safeToDeleteOnStartup];
	return [result boolValue];
}


- (void)setSafeToDeleteOnStartupValue:(BOOL)value_ {
	[self setSafeToDeleteOnStartup:[NSNumber numberWithBool:value_]];
}


- (BOOL)primitiveSafeToDeleteOnStartupValue {
	NSNumber *result = [self primitiveSafeToDeleteOnStartup];
	return [result boolValue];
}

- (void)setPrimitiveSafeToDeleteOnStartupValue:(BOOL)value_ {
	[self setPrimitiveSafeToDeleteOnStartup:[NSNumber numberWithBool:value_]];
}





@dynamic status;






@dynamic title;






@dynamic type;






@dynamic documents;

	
- (NSMutableSet*)documentsSet {
	[self willAccessValueForKey:@"documents"];
  
	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"documents"];
  
	[self didAccessValueForKey:@"documents"];
	return result;
}
	






@end




