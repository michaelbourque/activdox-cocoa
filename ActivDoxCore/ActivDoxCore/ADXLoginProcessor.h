//
//  ADXLoginProcessor.h
//  ActivDoxCore
//
//  Created by Gavin McKenzie on 2012-08-03.
//  Copyright (c) 2012 ActivDox Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "ADXModel.h"

@class ADXV2Service;

typedef enum {
    ADXLoginProcessorRegistrationFailed = 0,
    ADXLoginProcessorRegistrationAcceptedForConfirmation,
    ADXLoginProcessorRegistrationAcceptedAndReadyForLogin,
} ADXLoginProcessorRegistrationResponse;

typedef void(^ADXRegistrationPerformedCompletionBlock)(ADXLoginProcessorRegistrationResponse status, NSError *error);
typedef void(^ADXLoginPerformedCompletionBlock)(BOOL success, BOOL online, ADXAccount *account, ADXV2Service *service, NSError *error);
typedef void(^ADXLoginPerformedWithStatusCompletionBlock)(BOOL success, NSUInteger statusCode, BOOL online, ADXAccount *account, ADXV2Service *service, NSError *error);
typedef void(^ADXLoginTestedCompletionBlock)(BOOL success, NSError *error);
typedef void(^ADXPasswordResetCompletionBlock)(BOOL success, NSError *error);

@interface ADXLoginProcessor : NSObject
- (id)initWithParentManagedObjectContext:(NSManagedObjectContext *)context;
- (ADXAccount *)defaultAccount;
- (void)performLoginWithAcccount:(ADXAccount *)account completion:(ADXLoginPerformedWithStatusCompletionBlock)completionBlock;
- (void)performLoginWithName:(NSString *)loginName password:(NSString *)password server:(NSString *)server rememberPassword:(BOOL)rememberPassword completion:(ADXLoginPerformedWithStatusCompletionBlock)completionBlock;
- (void)testLoginWithName:(NSString *)loginName password:(NSString *)password server:(NSString *)server completion:(ADXLoginTestedCompletionBlock)completionBlock;
- (void)registerWithName:(NSString *)loginName email:(NSString *)email password:(NSString *)password server:(NSString *)server completion:(ADXRegistrationPerformedCompletionBlock)completionBlock;
- (void)performPasswordResetWithServer:(NSString *)server email:(NSString *)email completion:(ADXPasswordResetCompletionBlock)completion;
@end
