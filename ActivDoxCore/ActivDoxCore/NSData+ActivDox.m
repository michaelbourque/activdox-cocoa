//
//  NSData+ActivDox.m
//  ActivDoxCore
//
//  Created by Gavin McKenzie on 12-06-07.
//  Copyright (c) 2012 ActivDox Inc. All rights reserved.
//

#import "NSData+ActivDox.h"

@implementation NSData (ActivDox)

- (NSString *)adx_hexString {
    const unsigned char *dataBuffer = (const unsigned char *)[self bytes];
    
    if (!dataBuffer)
        return nil;
    
    NSUInteger dataLength  = [self length];
    NSMutableString *hexString  = [NSMutableString stringWithCapacity:(dataLength * 2)];
    
    for (int i = 0; i < dataLength; ++i) {
        [hexString appendString:[NSString stringWithFormat:@"%02lx", (unsigned long)dataBuffer[i]]];
    }
    
    return [NSString stringWithString:hexString];
}

- (BOOL)adx_containsData:(NSData *)data
{
    unsigned const char *selfPtr, *selfEnd, *selfRestart, *ptr, *ptrRestart,
    *end;
    
    ptrRestart = [data bytes];
    end = ptrRestart + [data length];
    selfRestart = [self bytes];
    selfEnd = selfRestart + [self length] - [data length];
    
    while(selfRestart <= selfEnd) {
        selfPtr = selfRestart;
        ptr = ptrRestart;
        while(ptr < end) {
            if (*ptr++ != *selfPtr++)
                break;
        }
        if (ptr == end)
            return YES;
        selfRestart++;
    }
    return NO;
}

@end
