//
//  ADXRemoteNotification.h
//  ActivDoxCore
//
//  Created by David Nikkel on 12-06-04.
//  Copyright (c) 2012 ActivDox Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString * const kADXRemoteNotificationEvent;
extern NSString * const kADXUserActivatedNotificationEvent;
extern NSString * const kADXNotificationPayloadEventIDKey;
extern NSString * const kADXNotificationPayloadAccountIDKey;
extern NSString * const kADXNotificationPayloadURLKey;
extern NSString * const kADXNotificationPayloadAPSKey;
extern NSString * const kADXNotificationPayloadAPSAlertMessageKeyPath;


@interface ADXRemoteNotificationRelay: NSObject
- (void)processNotificationReceivedWhileNotRunning: (NSDictionary *) dictionary;
- (void)processNotificationReceivedWhileRunning: (NSDictionary *) dictionary;
- (void)userDidActivateNotification: (NSDictionary *)dictionary;
- (void)failedToRegister: (NSError *)err;
@end
