//
//  NSManagedObjectContext+AngryPumpkin.m
//
//  Created by Gavin McKenzie on 11-02-22.
//  Copyright 2011 Angry Pumpkin Software Inc. All rights reserved.
//

#import "NSManagedObjectContext+AngryPumpkin.h"
#import "ADXCommonMacros.h"

@implementation NSManagedObjectContext (NSManagedObjectContext_AngryPumpkin)

- (NSFetchRequest *)aps_fetchRequestForEntityName:(NSString *)newEntityName withPredicate:(id)stringOrPredicate andParameters:(va_list)variadicArguments
{
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:newEntityName inManagedObjectContext:self];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entity];
    
    if (stringOrPredicate) {
        NSPredicate *predicate;
        if ([stringOrPredicate isKindOfClass:[NSString class]]) {
            predicate = [NSPredicate predicateWithFormat:stringOrPredicate
                                               arguments:variadicArguments];
        } else {
            NSAssert2([stringOrPredicate isKindOfClass:[NSPredicate class]],
                      @"Second parameter passed to %s is of unexpected class %@",
                      sel_getName(_cmd), NSStringFromClass([stringOrPredicate class]));
            predicate = (NSPredicate *)stringOrPredicate;
        }
        [request setPredicate:predicate];
    }

    return request;
}

- (NSFetchRequest *)aps_fetchRequestForEntityName:(NSString *)newEntityName withPredicate:(id)stringOrPredicate, ... 
{
    NSFetchRequest *request = nil;
    
    va_list variadicArguments;
    va_start(variadicArguments, stringOrPredicate);
    request = [self aps_fetchRequestForEntityName:newEntityName withPredicate:stringOrPredicate andParameters:variadicArguments];
    va_end(variadicArguments);

    return request;
}

// Convenience method to fetch the array of objects for a given Entity
// name in the context, optionally limiting by a predicate or by a predicate
// made from a format NSString and variable arguments.
//
- (NSArray *)aps_fetchObjectsForEntityName:(NSString *)newEntityName
                             objectIDsOnly:(BOOL)idsOnly
                           sortDescriptors:(NSArray *)sortDescriptors
                             withPredicate:(id)stringOrPredicate, ... ;
{
    NSFetchRequest *request = nil;
    
    va_list variadicArguments;
    va_start(variadicArguments, stringOrPredicate);
    request = [self aps_fetchRequestForEntityName:newEntityName withPredicate:stringOrPredicate andParameters:variadicArguments];
    va_end(variadicArguments);

    if (idsOnly) {
        [request setIncludesPropertyValues:NO];
    }
    if (sortDescriptors) {
        [request setSortDescriptors:sortDescriptors];
    }

    NSError *error = nil;
    NSArray *results = [self executeFetchRequest:request error:&error];
    if (error != nil) {
        [NSException raise:NSGenericException format:@"%@", [error description]];
    }
    
    return results;
}

- (NSArray *)aps_fetchObjectsForEntityName:(NSString *)newEntityName
                           withPredicate:(id)stringOrPredicate, ... {
    NSArray *result = [self aps_fetchObjectsForEntityName:newEntityName objectIDsOnly:NO sortDescriptors:nil withPredicate:stringOrPredicate];
    return result;
}

- (NSArray *)aps_fetchObjectsForEntityName:(NSString *)newEntityName limit:(NSUInteger)limit withPredicate:(id)stringOrPredicate, ...
{
    NSFetchRequest *request = nil;
    
    va_list variadicArguments;
    va_start(variadicArguments, stringOrPredicate);
    request = [self aps_fetchRequestForEntityName:newEntityName withPredicate:stringOrPredicate andParameters:variadicArguments];
    va_end(variadicArguments);
    
    [request setFetchLimit:limit];
    
    NSError *error = nil;
    NSArray *results = [self executeFetchRequest:request error:&error];
    if (error != nil) {
        [NSException raise:NSGenericException format:@"%@", [error description]];
    }
    
    return results;
}

- (id)aps_fetchFirstObjectForEntityName:(NSString *)newEntityName withPredicate:(id)stringOrPredicate, ... {
    NSFetchRequest *request = nil;
    id result = nil;
    
    va_list variadicArguments;
    va_start(variadicArguments, stringOrPredicate);
    request = [self aps_fetchRequestForEntityName:newEntityName withPredicate:stringOrPredicate andParameters:variadicArguments];
    va_end(variadicArguments);
    
    [request setFetchLimit:1];
    
    NSError *error = nil;
    NSArray *results = [self executeFetchRequest:request error:&error];
    if (results == nil && error != nil) {
        [NSException raise:NSGenericException format:@"%@", [error description]];
    } else {
        result = [results lastObject];
    }
    
    return result;
}

#if TARGET_OS_IPHONE
- (NSFetchedResultsController *)aps_fetchedResultsControllerForEntityName:(NSString *)newEntityName
                                                                predicate:(NSPredicate *)predicate
                                                          sortDescriptors:(NSArray *)sortDescriptors 
                                                       sectionNameKeyPath:(NSString *)sectionNameKeyPath 
                                                                cacheName:(NSString *)cacheName 
                                                                 delegate:(id)theDelegate;
{
    NSFetchedResultsController *frc = nil;
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:newEntityName inManagedObjectContext:self];    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entity];
    [request setSortDescriptors:sortDescriptors];
    [request setPredicate:predicate];
    
    frc = [[NSFetchedResultsController alloc] 
           initWithFetchRequest:request
           managedObjectContext:self
           sectionNameKeyPath:sectionNameKeyPath
           cacheName:cacheName];
    frc.delegate = theDelegate;
    
    return frc;
}
#endif

- (NSArray *)aps_fetchAllForEntityName:(NSString *)anEntityName
                         objectIDsOnly:(BOOL)idsOnly
                       sortDescriptors:(NSArray *)sortDescriptors
{
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:anEntityName inManagedObjectContext:self];    
	NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];	
	[fetchRequest setEntity:entity];
    if (idsOnly) {
        [fetchRequest setIncludesPropertyValues:NO];
    }
    if (sortDescriptors) {
        [fetchRequest setSortDescriptors:sortDescriptors];
    }
    
    NSError *error = nil;
	NSArray *results = [self executeFetchRequest:fetchRequest error:&error];
    if (error) {
        LogNSError(@"fetchAllForEntityName encountered error.", error);
    }
	
	return results;
}

- (NSArray *)aps_fetchAllForEntityName:(NSString *)anEntityName
{
    return [self aps_fetchAllForEntityName:anEntityName objectIDsOnly:NO sortDescriptors:nil];
}

- (BOOL)aps_objectsExistForEntityName:(NSString *)anEntityName
{
    BOOL rc = NO;
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:anEntityName inManagedObjectContext:self];    
	NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];	
	[fetchRequest setEntity:entity];
    [fetchRequest setFetchLimit:1];
    
    NSError *error = nil;
	NSArray *results = [self executeFetchRequest:fetchRequest error:&error];
    if (error) {
        LogNSError(@"aps_objectsExistForEntityName encountered error.", error);
    }
    
    if ([results count])
        rc = YES;
	
	return rc;    
}

- (BOOL)aps_moreThanOneObjectsExistForEntityName:(NSString *)anEntityName
{
    BOOL rc = NO;
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:anEntityName inManagedObjectContext:self];    
	NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];	
	[fetchRequest setEntity:entity];
    [fetchRequest setFetchLimit:2];
    
    NSError *error = nil;
	NSArray *results = [self executeFetchRequest:fetchRequest error:&error];
    if (error) {
        LogNSError(@"aps_moreThanOneObjectsExistForEntityName encountered error.", error);
    }
    
    if ([results count] > 1)
        rc = YES;
	
	return rc;    
}

- (NSUInteger)aps_countObjectsForEntityName:(NSString *)anEntityName
{
    NSUInteger result = 0;
    NSArray *fetchedIDs = [self aps_fetchAllForEntityName:anEntityName objectIDsOnly:YES sortDescriptors:nil];
    result = [fetchedIDs count];
    
    return result;
}

@end
