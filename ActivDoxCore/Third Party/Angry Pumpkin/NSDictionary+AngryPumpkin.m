//
//  NSDictionary+AngryPumpkin.m
//  TweetKeeper
//
//  Created by Gavin McKenzie on 12-03-24.
//  Copyright (c) 2012 ActivDox Inc. All rights reserved.
//

#import "NSDictionary+AngryPumpkin.h"

@implementation NSDictionary (AngryPumpkin)

- (id)aps_objectForKey:(NSString *)key defaultIfNotFound:(id)defaultValue
{
    id object = [self objectForKey:key];
    if (!object|| object == [NSNull null]) {
        object = defaultValue;
    }
    return object;
}

- (id)aps_objectForKeyOrNil:(NSString *)key
{
    id object = [self objectForKey:key];
    if (!object || object == [NSNull null]) {
        object = nil;
    }
    return object;
}

- (id)aps_valueForKeyPathOrNil:(NSString *)keyPath
{
    id object = [self valueForKeyPath:keyPath];
    if (!object || object == [NSNull null]) {
        object = nil;
    }
    return object;
}

- (id)aps_valueForKeyPath:(NSString *)keyPath defaultIfNotFound:(id)defaultValue
{
    id object = [self valueForKeyPath:keyPath];
    if (!object || object == [NSNull null]) {
        object = defaultValue;
    }
    return object;
}

- (id)aps_dictionaryMergedWithDictionary:(NSDictionary *)dictionary
{
    NSMutableDictionary *mutableDictionary = [NSMutableDictionary dictionaryWithDictionary:self];
    [mutableDictionary addEntriesFromDictionary:dictionary];
    return [NSDictionary dictionaryWithDictionary:mutableDictionary];
}

@end
