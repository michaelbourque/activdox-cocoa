//
//  NSDictionary+AngryPumpkin.h
//  TweetKeeper
//
//  Created by Gavin McKenzie on 12-03-24.
//  Copyright (c) 2012 ActivDox Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (AngryPumpkin)

- (id)aps_objectForKey:(NSString *)key defaultIfNotFound:(id)defaultValue;
- (id)aps_objectForKeyOrNil:(NSString *)key;
- (id)aps_valueForKeyPathOrNil:(NSString *)keyPath;
- (id)aps_valueForKeyPath:(NSString *)keyPath defaultIfNotFound:(id)defaultValue;
- (id)aps_dictionaryMergedWithDictionary:(NSDictionary *)dictionary;

@end
