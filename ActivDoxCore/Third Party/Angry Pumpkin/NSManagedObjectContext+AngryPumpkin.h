//
//  NSManagedObjectContext+AngryPumpkin.h
//
//  Created by Gavin McKenzie on 11-02-22.
//  Copyright 2011 Angry Pumpkin Software Inc. All rights reserved.
//

#import <CoreData/CoreData.h>


@interface NSManagedObjectContext (NSManagedObjectContext_AngryPumpkin)
- (NSFetchRequest *)aps_fetchRequestForEntityName:(NSString *)newEntityName withPredicate:(id)stringOrPredicate, ... ;
- (NSFetchRequest *)aps_fetchRequestForEntityName:(NSString *)newEntityName withPredicate:(id)stringOrPredicate andParameters:(va_list)variadicArguments;
- (NSArray *)aps_fetchObjectsForEntityName:(NSString *)newEntityName withPredicate:(id)stringOrPredicate, ... ;
- (NSArray *)aps_fetchObjectsForEntityName:(NSString *)newEntityName objectIDsOnly:(BOOL)idsOnly sortDescriptors:(NSArray *)sortDescriptors withPredicate:(id)stringOrPredicate, ... ;
- (NSArray *)aps_fetchObjectsForEntityName:(NSString *)newEntityName limit:(NSUInteger)limit withPredicate:(id)stringOrPredicate, ... ;
- (id)aps_fetchFirstObjectForEntityName:(NSString *)newEntityName withPredicate:(id)stringOrPredicate, ... ;
#if TARGET_OS_IPHONE
- (NSFetchedResultsController *)aps_fetchedResultsControllerForEntityName:(NSString *)newEntityName
                                                                predicate:(NSPredicate *)predicate
                                                          sortDescriptors:(NSArray *)sortDescriptors 
                                                       sectionNameKeyPath:(NSString *)sectionNameKeyPath 
                                                                cacheName:(NSString *)cacheName
                                                                 delegate:(id)theDelegate;
#endif
- (NSArray *)aps_fetchAllForEntityName:(NSString *)anEntityName;
- (NSArray *)aps_fetchAllForEntityName:(NSString *)anEntityName objectIDsOnly:(BOOL)idsOnly sortDescriptors:(NSArray *)sortDescriptors;
- (BOOL)aps_objectsExistForEntityName:(NSString *)anEntityName;
- (BOOL)aps_moreThanOneObjectsExistForEntityName:(NSString *)anEntityName;
- (NSUInteger)aps_countObjectsForEntityName:(NSString *)anEntityName;
@end
