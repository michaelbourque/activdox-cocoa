
#import <Foundation/Foundation.h>

@interface NSDate (NaturalDates)
+ (NSDate *)todayMidnight;
+ (NSDate *)dateAtMidnightFromDate:(NSDate *)aDate;
- (BOOL)isSameDay:(NSDate *)anotherDate;
- (BOOL)isToday;
- (BOOL)isYesterday;
- (BOOL)isTomorrow;
- (BOOL)isLastWeek;
- (BOOL)isThisWeek:(BOOL)withinSixDays;
- (BOOL)isThisWeek;
- (NSString *)stringFromDateCapitalized:(BOOL)capitalize;
- (NSString *)stringFromDateCapitalizedNoTime:(BOOL)capitalize;
@end
