#import "NSDate+NaturalDates.h"
#import "NSDate+Helper.h"

@implementation NSDate (NaturalDates)

static NSCalendar *gsCachedCurrentCalendar;

+ (NSCalendar *)cachedCurrentCalendar
{
    if (gsCachedCurrentCalendar == nil) {
        gsCachedCurrentCalendar = [NSCalendar currentCalendar];
    }
    
    return gsCachedCurrentCalendar;
}

+ (NSDate *)todayMidnight {
    static NSCalendar *gregorian;
    static NSDateComponents *dateComponents;
    if (gregorian == nil) {
        gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
        dateComponents = [gregorian components:
                         (NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit)
                                     fromDate:[NSDate date]];
    }
    
    NSDate * today = [gregorian dateFromComponents:dateComponents];
    return today;
}


+ (NSDate *)dateAtMidnightFromDate:(NSDate *)aDate {
    static NSCalendar *gregorian;
    static NSDateComponents *components;
    if (gregorian == nil) {
        gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
        components = [gregorian components:
                                     (NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit)
                                                 fromDate:aDate];
    }
    NSDate *dateAtMidnight = [gregorian dateFromComponents:components];
    return dateAtMidnight;    
}

- (BOOL)isSameDay:(NSDate*)anotherDate 
{
    NSCalendar* calendar = [NSDate cachedCurrentCalendar];
    NSDateComponents* components1 = [calendar components:(NSYearCalendarUnit 
                                                          | NSMonthCalendarUnit | NSDayCalendarUnit) fromDate:self]; 
    NSDateComponents* components2 = [calendar components:(NSYearCalendarUnit 
                                                          | NSMonthCalendarUnit | NSDayCalendarUnit) fromDate:anotherDate]; 
    return ([components1 year] == [components2 year] && [components1 month] 
            == [components2 month] && [components1 day] == [components2 day]); 
} 

- (BOOL)isToday 
{ 
    return [self isSameDay:[NSDate date]]; 
} 

- (BOOL)isYesterday
{ 
    NSCalendar* calendar = [NSDate cachedCurrentCalendar];
    NSDateComponents *comps = [[NSDateComponents alloc] init];
    [comps setDay:-1]; 
    NSDate *yesterday = [calendar dateByAddingComponents:comps 
                                                  toDate:[NSDate date]  options:0]; 
    return [self isSameDay:yesterday]; 
} 

- (BOOL)isTomorrow
{ 
    NSCalendar* calendar = [NSDate cachedCurrentCalendar];
    NSDateComponents *comps = [[NSDateComponents alloc] init];
    [comps setDay:+1]; 
    NSDate *tomorrow = [calendar dateByAddingComponents:comps 
                                                  toDate:[NSDate date]  options:0]; 
    return [self isSameDay:tomorrow]; 
} 

- (BOOL)isLastWeek 
{ 
    NSCalendar* calendar = [NSDate cachedCurrentCalendar];
    NSDateComponents *comps = [calendar
                               components:NSWeekCalendarUnit|NSDayCalendarUnit fromDate:[NSDate date] 
                               toDate:self options:0]; 
    NSInteger week = [comps week]; 
    NSInteger days = [comps day]; 
    return (0==week && days<=0); 
} 

- (BOOL)isThisWeek:(BOOL)withinSixDays 
{ 
    NSCalendar* calendar = [NSDate cachedCurrentCalendar];
    NSDateComponents *comps = [calendar
                               components:NSWeekCalendarUnit|NSDayCalendarUnit fromDate:[NSDate date] 
                               toDate:self options:0]; 
    NSInteger week = [comps week]; 
    NSInteger days = [comps day];
    
    BOOL rc = NO;
    if (withinSixDays) {
        rc = (0==week && days<6);
    } else {
        rc = (0==week && days>=0);
    }
    return rc; 
} 

- (BOOL)isThisWeek {
    return [self isThisWeek:NO];
}


- (NSString *)stringFromDateCapitalized:(BOOL)capitalize; 
{ 
    NSString *label = nil; 
    static NSDateFormatter *dateFormatter;
    if (dateFormatter == nil) {
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeStyle:NSDateFormatterShortStyle];
    }
    
    [dateFormatter setDateStyle:NSDateFormatterNoStyle]; // Will display hour only, we are building the day ourselves

    NSString *dateFormatPrefix = nil;
    if([self isToday]) {
        if(capitalize) dateFormatPrefix = NSLocalizedString(@"Today at", @""); 
        else dateFormatPrefix = NSLocalizedString(@"today at", @""); 
    } else if([self isYesterday]) { 
        if(capitalize) dateFormatPrefix = NSLocalizedString(@"Yesterday at", @""); 
        else dateFormatPrefix = NSLocalizedString(@"yesterday at", @""); 
    } else if ([self isTomorrow]) {
        if(capitalize) dateFormatPrefix = NSLocalizedString(@"Tomorrow at", @""); 
        else dateFormatPrefix = NSLocalizedString(@"tomorrow at", @""); 
    } else if([self isThisWeek:YES]) { 
        static NSDateFormatter *weekDayFormatter;
        if (weekDayFormatter == nil) {
            weekDayFormatter = [[NSDateFormatter alloc] init];
            // We will set the locale to US to have the weekday in english.
            // The NSLocalizedString(weekDayString, @"") below will make it localized.
            NSLocale *locale = [[NSLocale alloc]
                                initWithLocaleIdentifier:@"en_US"];
            [weekDayFormatter setLocale:locale];
            
            [weekDayFormatter setDateFormat:@"EEEE"];
        }
        
        NSString *weekDayString = [NSString stringWithFormat:@"%@ at",
                                   [weekDayFormatter stringFromDate:self]]; 
        dateFormatPrefix = NSLocalizedString(weekDayString, @""); 
    } else if([self isLastWeek]) { 
        static NSDateFormatter *weekDayFormatter;
        if (weekDayFormatter == nil) {
            weekDayFormatter = [[NSDateFormatter alloc] init];
            // We will set the locale to US to have the weekday in english.
            // The NSLocalizedString(weekDayString, @"") below will make it localized.
            NSLocale *locale = [[NSLocale alloc]
                                initWithLocaleIdentifier:@"en_US"];
            [weekDayFormatter setLocale:locale];
            
            [weekDayFormatter setDateFormat:@"EEEE"];
        }

        NSString *weekDayString = [NSString stringWithFormat:@"%@ at",
                                   [weekDayFormatter stringFromDate:self]]; 
        dateFormatPrefix = NSLocalizedString(weekDayString, @""); 
    } 
    else 
    { 
        [dateFormatter setDateStyle:NSDateFormatterShortStyle]; // Display the date as well 
    } 
    if(dateFormatPrefix != nil) { // We have a day string, add hour only 
        label = [NSString stringWithFormat:@"%@ %@", dateFormatPrefix, 
                 [dateFormatter stringFromDate:self]]; 
    } 
    else { // Use the full date 
        label = [dateFormatter stringFromDate:self]; 
    } 

    return label; 
}

- (NSString *)stringFromDateCapitalizedNoTime:(BOOL)capitalize; 
{ 
    NSString *label = nil; 
    static NSDateFormatter *dateFormatter;
    if (dateFormatter == nil) {
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeStyle:NSDateFormatterShortStyle];
    }
    
    [dateFormatter setDateStyle:NSDateFormatterNoStyle]; // Will display hour only, we are building the day ourselves
    
    NSString *dateFormatPrefix = nil;
    if([self isToday]) {
        if(capitalize) dateFormatPrefix = NSLocalizedString(@"Today", @""); 
        else dateFormatPrefix = NSLocalizedString(@"today", @""); 
    } else if([self isYesterday]) { 
        if(capitalize) dateFormatPrefix = NSLocalizedString(@"Yesterday", @""); 
        else dateFormatPrefix = NSLocalizedString(@"yesterday", @""); 
    } else if ([self isTomorrow]) {
        if(capitalize) dateFormatPrefix = NSLocalizedString(@"Tomorrow", @""); 
        else dateFormatPrefix = NSLocalizedString(@"tomorrow", @""); 
    } else if([self isThisWeek:YES]) { 
        static NSDateFormatter *weekDayFormatter;
        if (weekDayFormatter == nil) {
            weekDayFormatter = [[NSDateFormatter alloc] init];
            // We will set the locale to US to have the weekday in english.
            // The NSLocalizedString(weekDayString, @"") below will make it localized.
            NSLocale *locale = [[NSLocale alloc]
                                initWithLocaleIdentifier:@"en_US"];
            [weekDayFormatter setLocale:locale];
            [weekDayFormatter setDateFormat:@"EEEE"];
        }

        NSString *weekDayString = [NSString stringWithFormat:@"%@",
                                   [weekDayFormatter stringFromDate:self]]; 
        dateFormatPrefix = NSLocalizedString(weekDayString, @""); 
    } else if([self isLastWeek]) {
        static NSDateFormatter *weekDayFormatter;
        if (weekDayFormatter == nil) {
            weekDayFormatter = [[NSDateFormatter alloc] init];
            // We will set the locale to US to have the weekday in english.
            // The NSLocalizedString(weekDayString, @"") below will make it localized.
            NSLocale *locale = [[NSLocale alloc]
                                initWithLocaleIdentifier:@"en_US"];
            [weekDayFormatter setLocale:locale];
            [weekDayFormatter setDateFormat:@"EEEE"];
        }

        NSString *weekDayString = [NSString stringWithFormat:@"%@",
                                   [weekDayFormatter stringFromDate:self]]; 
        dateFormatPrefix = NSLocalizedString(weekDayString, @""); 
    }
    else 
    { 
        [dateFormatter setDateStyle:NSDateFormatterLongStyle]; // Display the date as well 
    } 
    if(dateFormatPrefix != nil) { // We have a day string, add hour only 
        label = dateFormatPrefix; 
    } 
    else { // Use the full date 
        label = [self stringWithDateStyle:[dateFormatter dateStyle] timeStyle:NSDateFormatterNoStyle]; 
    } 
    return label; 
}

@end