//
//  ADXImageBrowserCell.m
//  DesktopReader
//
//  Created by Steve Tibbett on 2012-07-24.
//  Copyright (c) 2012 ActivDox. All rights reserved.
//

#import <Quartz/Quartz.h>
#import "ADXImageBrowserDocumentCell.h"
#import "ADXDocumentBrowserViewItem.h"

@interface ADXImageBrowserDocumentCell()
@property (strong) CALayer *foregroundLayer;
@property (strong) id imageRepresentation;
@end

@implementation ADXImageBrowserDocumentCell
@synthesize foregroundLayer = _foregroundLayer;

- (id)initWithItem:(ADXDocumentBrowserViewItem *)viewItem {
    self = [super init];
    if (self) {
        self.imageRepresentation = [viewItem.imageRepresentation copy];
    }
    return self;
}


#pragma mark - Rendering

// When asked for a foreground layer, return a new layer that we'll render the icon decorations into
- (CALayer *)layerForType:(NSString *)type {
    if ([type isEqualToString:IKImageBrowserCellForegroundLayer]) {
        self.foregroundLayer = [[CALayer alloc] init];
        self.foregroundLayer.delegate = self;
        self.foregroundLayer.needsDisplayOnBoundsChange = YES;
        [self.foregroundLayer setNeedsDisplay];
        return self.foregroundLayer;
    } else {
        return [super layerForType:type];
    }
}

- (void)drawBadgeForItem:(ADXDocumentBrowserViewItem *)item inContext:(CGContextRef)theContext {
    // Calculate the size of the badge text
    NSString *badgeText = item.badgeText;
    NSFont *badgeFont = [NSFont systemFontOfSize:12];
    NSDictionary *attributes = [NSDictionary dictionaryWithObject:badgeFont forKey:NSFontAttributeName];
    NSSize badgeTextSize = [badgeText sizeWithAttributes:attributes];
    badgeTextSize.width += 12; // padding
    NSRect badgeRect = NSMakeRect(((self.imageFrame.origin.x - self.frame.origin.x) + self.imageFrame.size.width - 2) - (badgeTextSize.width + 1),
                                  ((self.imageFrame.origin.y - self.frame.origin.y) + self.imageFrame.size.height - 2) - badgeTextSize.height,
                                  badgeTextSize.width, badgeTextSize.height);
    
    
    [NSGraphicsContext saveGraphicsState];
    NSGraphicsContext *gc = [NSGraphicsContext graphicsContextWithGraphicsPort:theContext flipped:NO];
    [NSGraphicsContext setCurrentContext:gc];
    
    // Determine the badge colours
    NSColor *baseColor;
    NSColor *shineColor;
    if ([item shouldShowNewBadge]) {
        shineColor = [NSColor colorWithDeviceRed:0.6 green:0.6 blue:1.0 alpha:1.0];
        baseColor = [NSColor blueColor];
    } else {
        shineColor = [NSColor colorWithDeviceRed:1.0 green:0.6 blue:0.6 alpha:1.0];
        baseColor = [NSColor redColor];
    }
    
    NSGradient *fillGradient = [[NSGradient alloc] initWithStartingColor:shineColor
                                                             endingColor:baseColor];
    
    // Draw the background shadow
    NSBezierPath *path = [NSBezierPath bezierPathWithRoundedRect:NSMakeRect(badgeRect.origin.x, badgeRect.origin.y-1.0, badgeRect.size.width, badgeRect.size.height)
                                                         xRadius:8
                                                         yRadius:8];
    
    path.lineWidth = 3.0;
    [[NSColor blackColor] set];
    [path stroke];
    
    // Draw the white outline

    path = [NSBezierPath bezierPathWithRoundedRect:NSMakeRect(badgeRect.origin.x-0.5, badgeRect.origin.y-0.5, badgeRect.size.width, badgeRect.size.height)
                                           xRadius:8
                                           yRadius:8];
    path.lineWidth = 2.5;
    [[NSColor whiteColor] set];
    [path stroke];

    // Fill the interior with the gradient
    [path addClip];
    [fillGradient drawInBezierPath:path angle:-90];
    [NSGraphicsContext restoreGraphicsState];
    
    // Draw the text
    [NSGraphicsContext saveGraphicsState];
    gc = [NSGraphicsContext graphicsContextWithGraphicsPort:theContext flipped:NO];
    [NSGraphicsContext setCurrentContext:gc];
    NSMutableDictionary *textAttributes = [attributes mutableCopy];
    [textAttributes setValue:[NSColor whiteColor] forKey:NSForegroundColorAttributeName];
    badgeRect.origin.x += 5;
    badgeRect.origin.y += 3;
    [badgeText drawWithRect:badgeRect options:0 attributes:textAttributes];
    
    [NSGraphicsContext restoreGraphicsState];
}

- (void)drawLayer:(CALayer *)theLayer
        inContext:(CGContextRef)theContext
{
    if ([self.representedItem isKindOfClass:[ADXDocumentBrowserViewItem class]]) {
        ADXDocumentBrowserViewItem *item = (ADXDocumentBrowserViewItem *)self.representedItem;

        if (self.imageRepresentation == nil) {
            // See if there's an image ready and use it if so.  This is usually done in advance, but
            // will happen here if an image is downloaded (and thus the image representation becomes
            // available) after the view is populated.
            self.imageRepresentation = [item.imageRepresentation copy];
        }
        
        // Draw the corner image if we have a thumbnail and the document is current
        if (self.imageRepresentation != nil && [self.representedItem respondsToSelector:@selector(ribbonImage)]) {
            NSImage *ribbonImage = [self.representedItem ribbonImage];
            NSRect cornerRect = NSMakeRect(0, 0, 50, 50);
            
            CGRect targetRect = CGRectMake(self.imageFrame.origin.x - self.frame.origin.x,
                                           (self.imageFrame.origin.y - self.frame.origin.y) + (self.imageFrame.size.height - cornerRect.size.height),
                                           ribbonImage.size.width, ribbonImage.size.height);
            
            NSGraphicsContext *context = [NSGraphicsContext graphicsContextWithGraphicsPort:theContext flipped:NO];
            [NSGraphicsContext saveGraphicsState];
            [NSGraphicsContext setCurrentContext:context];
            [ribbonImage drawInRect:targetRect fromRect:NSZeroRect operation:NSCompositeCopy fraction:1.0];
            [NSGraphicsContext restoreGraphicsState];
        }
        
        // Draw the progress indicator if the document is currently downloading
        if (item.isDownloading && item.percentDone < 1.0) {
            CGContextSaveGState(theContext);
            
            // Render the progress indicator:  Start with an outer black rectangle,
            // then an inner white rectangle, then the red indicator on top of that
            int margin = 16;
            int height = 8;
            CGRect progressRect = self.imageFrame;
            progressRect.origin.y = margin;
            progressRect.size.height = height;
            progressRect.origin.x = margin;
            progressRect.size.width = self.frame.size.width - (margin*2);
            
            CGColorRef blackColor = CGColorCreateGenericRGB(0.0, 0.0, 0.0, 1.0);
            CGContextSetFillColorWithColor(theContext, blackColor);
            CGContextFillRect(theContext, progressRect);
            CGColorRelease(blackColor);
            
            CGColorRef whiteColor = CGColorCreateGenericRGB(1.0, 1.0, 1.0, 1.0);
            CGContextSetFillColorWithColor(theContext, whiteColor);
            CGColorRelease(whiteColor);
            
            progressRect = CGRectInset(progressRect, 1, 1);
            CGContextFillRect(theContext, progressRect);
            
            progressRect = CGRectInset(progressRect, 1, 1);
            progressRect.size.width = (progressRect.size.width * item.percentDone);
            CGColorRef redColor = CGColorCreateGenericRGB(1.0, 0.0, 0.0, 1.0);
            CGContextSetFillColorWithColor(theContext, redColor);
            CGColorRelease(redColor);
            CGContextFillRect(theContext, progressRect);
            
            CGContextRestoreGState(theContext);
        }
        
        // Badge
        if (item.shouldShowNewBadge || item.shouldShowChangeBadge) {
            CGContextSaveGState(theContext);
            [self drawBadgeForItem:item inContext:theContext];
            CGContextRestoreGState(theContext);
        }
    }
}


@end
