//
//  ADXPageBrowserViewController.h
//  DesktopReader
//
//  Created by Steve Tibbett on 2012-11-05.
//  Copyright (c) 2012 ActivDox. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "ADXPDFViewController.h"

@interface ADXPageBrowserViewController : NSViewController

// This is the PDF view controller whose content we're following
@property (strong, nonatomic) ADXPDFViewController *PDFViewController;

@property (nonatomic) BOOL showOnlyChangedPages;

@end
