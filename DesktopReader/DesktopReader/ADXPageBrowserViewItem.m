//
//  ADXPageBrowserViewItem.m
//  DesktopReader
//
//  Created by Steve Tibbett on 2012-11-05.
//  Copyright (c) 2012 ActivDox. All rights reserved.
//

#import <Quartz/Quartz.h>
#import "ADXPageBrowserViewItem.h"
#import "ActivDoxCore.h"

@interface ADXPageBrowserViewItem()
@property (strong, nonatomic) ADXDocument *document;
@property (nonatomic) NSUInteger pageIndex;
@property (nonatomic) NSInteger numChangesOnPage;
@property (nonatomic) NSUInteger version;
@property (strong, nonatomic) PDFPage *page;
@end

@implementation ADXPageBrowserViewItem

+ (ADXPageBrowserViewItem *)itemWithDocument:(ADXDocument *)document pageIndex:(NSUInteger)pageIndex page:(PDFPage *)page
{
    ADXPageBrowserViewItem *item = [[ADXPageBrowserViewItem alloc] init];
    item.document = document;
    item.pageIndex = pageIndex;
    item.page = page;
    item.version = 1;

    if (document.highlightedContent) {
        ADXHighlightedContentHelper *highlightedContent = [ADXHighlightedContentHelper highlightedContentWithData:document.highlightedContent.data];
        NSUInteger foundChanges = [highlightedContent numberOfChangesOnPage:pageIndex];
        item.numChangesOnPage = foundChanges;
    } else {
        item.numChangesOnPage = -1;
    }

    return item;
}

- (BOOL)isLatestVersion
{
    return YES;
}


#pragma mark - IKImageBrowserItem informal protocol

- (NSString *)imageUID
{
    NSString *imageid = [NSString stringWithFormat:@"%@:%d", self.document.objectID, (int)self.pageIndex+1];
    return imageid;
}

- (NSString *)imageRepresentationType {
    return IKImageBrowserPDFPageRepresentationType;
}

- (id)imageRepresentation
{
    return self.page;
}

- (NSString *)imageTitle
{
    if (self.numChangesOnPage > 0) {
        return [NSString stringWithFormat:@"Page %d, %d changes", (int)self.pageIndex + 1, (int)self.numChangesOnPage];
    } else {
        return [NSString stringWithFormat:@"Page %d", (int)self.pageIndex + 1];
    }
}

- (BOOL)isSelectable {
    return YES;
}

- (NSUInteger)imageVersion
{
    return self.version;
}

@end
