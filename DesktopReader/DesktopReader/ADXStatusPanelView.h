//
//  ADXStatusPanelView.h
//  DesktopReader
//
//  Created by Steve Tibbett on 12-04-30.
//  Copyright (c) 2012 ActivDox. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface ADXStatusPanelView : NSView

@property (strong, nonatomic) NSString *topStatusLine;

- (void)showMessage:(NSString *)message;

@end
