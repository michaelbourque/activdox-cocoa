//
//  ADXPageBrowserViewItem.h
//  DesktopReader
//
//  Created by Steve Tibbett on 2012-11-05.
//  Copyright (c) 2012 ActivDox. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Quartz/Quartz.h>

@class ADXDocument;

@interface ADXPageBrowserViewItem : NSObject

@property (weak, nonatomic) IKImageBrowserView *imageBrowserView;

+ (ADXPageBrowserViewItem *)itemWithDocument:(ADXDocument *)document pageIndex:(NSUInteger)pageIndex page:(PDFPage *)page;

@end
