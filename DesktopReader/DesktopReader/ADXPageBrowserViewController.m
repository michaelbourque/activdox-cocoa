//
//  ADXPageBrowserViewController.m
//  DesktopReader
//
//  Created by Steve Tibbett on 2012-11-05.
//  Copyright (c) 2012 ActivDox. All rights reserved.
//

#import "ADXPageBrowserViewController.h"
#import "ADXPageBrowserViewItem.h"
#import "ADXImageBrowserView.h"

@interface ADXPageBrowserViewController ()
@property (weak) IBOutlet ADXImageBrowserView *imageBrowserView;
@property (strong, nonatomic) NSArray *pages;
@end

@implementation ADXPageBrowserViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Initialization code here.
    }
    
    return self;
}


- (void)dealloc
{
    [self removeNotifications];
}

- (void)addNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(documentChangedNotification:)
                                                 name:PDFViewDocumentChangedNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(pageChangedNotification:)
                                                 name:PDFViewPageChangedNotification
                                               object:nil];
}

- (void)removeNotifications
{
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:PDFViewDocumentChangedNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:PDFViewPageChangedNotification
                                                  object:nil];
}

- (void)populatePages
{
    // Deselect all before repopulating so we don't end up scrolling the view
    [self.imageBrowserView setSelectionIndexes:[NSIndexSet indexSet] byExtendingSelection:NO];
    
    PDFDocument *document = self.PDFViewController.pdfView.document;
    NSUInteger pageCount = document.pageCount;
    
    NSMutableArray *pages = [NSMutableArray array];

    if (self.showOnlyChangedPages) {
        // Walk the changed pages list
        for (NSNumber *pageIndex in self.PDFViewController.document.changedPages) {
            NSUInteger pageIdx = pageIndex.integerValue - 1;
            PDFPage *page = [document pageAtIndex:pageIdx];
            ADXPageBrowserViewItem *item = [ADXPageBrowserViewItem itemWithDocument:self.PDFViewController.document pageIndex:pageIdx page:page];
            [pages addObject:item];
        }
    } else {
        // Add all pages
        for (NSUInteger pageIdx = 0; pageIdx < pageCount; pageIdx++) {
            PDFPage *page = [document pageAtIndex:pageIdx];
            ADXPageBrowserViewItem *item = [ADXPageBrowserViewItem itemWithDocument:self.PDFViewController.document pageIndex:pageIdx page:page];
            [pages addObject:item];
        }
    }
    
    self.pages = pages;
    
    [self.imageBrowserView reloadData];
    [self selectCurrentPage];
}

- (void)setPDFViewController:(ADXPDFViewController *)PDFViewController
{
    if (_PDFViewController) {
        [self removeNotifications];
    }
    
    _PDFViewController = PDFViewController;

    [self populatePages];
    
    [self addNotifications];
}

- (void)selectCurrentPage
{
    PDFDocument *document = self.PDFViewController.pdfView.document;
    NSUInteger newPageIndex = [document indexForPage:self.PDFViewController.pdfView.currentPage];
    
    if (self.showOnlyChangedPages) {
        NSUInteger pageIdx = 0;
        for (NSNumber *pageNumber in self.PDFViewController.document.changedPages) {
            if ((pageNumber.integerValue-1) == newPageIndex) {
                [self.imageBrowserView setSelectionIndexes:[NSIndexSet indexSetWithIndex:pageIdx] byExtendingSelection:NO];
                [self.imageBrowserView scrollIndexToVisible:pageIdx];
                break;
            }
            pageIdx++;
        }
    } else {
        [self.imageBrowserView setSelectionIndexes:[NSIndexSet indexSetWithIndex:newPageIndex] byExtendingSelection:NO];
        [self.imageBrowserView scrollIndexToVisible:newPageIndex];
    }
}

// The document has scrolled to a new page; select the associated thumbnail
- (void)pageChangedNotification:(NSNotification *)notification
{
    [self selectCurrentPage];
}

- (void)documentChangedNotification:(NSNotification *)notification
{
    [self populatePages];
}

- (void)setShowOnlyChangedPages:(BOOL)showOnlyChangedPages
{
    [self willChangeValueForKey:@"showOnlyChangedPages"];
    _showOnlyChangedPages = showOnlyChangedPages;
    [self didChangeValueForKey:@"showOnlyChangedPages"];
    
    [self populatePages];
}

#pragma mark - IKImageBrowserDataSource informal protocol

- (NSUInteger)numberOfItemsInImageBrowser:(IKImageBrowserView *)aBrowser
{
    return self.pages.count;
}

- (id)imageBrowser:(IKImageBrowserView *)aBrowser itemAtIndex:(NSUInteger)index
{
    return [self.pages objectAtIndex:index];
}

#pragma mark - IKImageBrowserDelegate informal protocol

- (void)imageBrowserSelectionDidChange:(IKImageBrowserView *)aBrowser
{
    NSIndexSet *selectionIndexes = [aBrowser selectionIndexes];
    if (selectionIndexes.count == 1) {
        NSUInteger index = [selectionIndexes firstIndex];
        if (self.showOnlyChangedPages) {
            index = [[self.PDFViewController.document.changedPages objectAtIndex:index] intValue] - 1;
        }
        
        PDFPage *page = [self.PDFViewController.pdfView.document pageAtIndex:index];
        [self.PDFViewController.pdfView goToPage:page];
    }
}


@end
