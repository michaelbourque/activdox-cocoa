//
//  ADXLoginPanelWindowController.m
//  DesktopReader
//
//  Created by Steve Tibbett on 12-06-29.
//  Copyright (c) 2012 ActivDox. All rights reserved.
//

#import "ADXLoginPanelWindowController.h"
#import "ADXAppDelegate.h"
#import "ADXAppModel.h"
#import "ADXMainWindowController.h"
#import "ADXStatusManager.h"
#import <ActivDoxCore.h>

@interface ADXLoginPanelWindowController ()
@property (strong, nonatomic) ADXV2Service *service;
@property (strong, nonatomic) ADXLoginProcessor *loginProcessor;
@property (strong, nonatomic) NSView *currentPanel;

@property (weak) IBOutlet NSTextField *loginField;
@property (weak) IBOutlet NSSecureTextField *passwordField;
@property (weak) IBOutlet NSTextField *serverField;
@property (weak) IBOutlet NSButton *loginButton;
@property (weak) IBOutlet NSButton *cancelButton;
@property (strong) IBOutlet NSView *loginPanel;
@property (strong) IBOutlet NSView *registerPanel;
@property (weak) IBOutlet NSView *placeholderPanel;
@property (strong) IBOutlet NSView *loginOrRegisterPanel;

@property (weak) IBOutlet NSLayoutConstraint *containerHeightConstraint;
@end

@implementation ADXLoginPanelWindowController
@synthesize containerHeightConstraint = _containerHeightConstraint;
@synthesize registerPanel = _registerPanel;

- (id)initWithWindow:(NSWindow *)window
{
    self = [super initWithWindow:window];
    if (self) {
        self.loginProcessor = [[ADXLoginProcessor alloc] initWithParentManagedObjectContext:self.managedObjectContext];
        self.rememberPassword = [NSNumber numberWithBool:YES];
    }
    
    return self;
}

- (NSManagedObjectContext *)managedObjectContext
{
    ADXAppDelegate *appDelegate = (ADXAppDelegate *)[[NSApplication sharedApplication] delegate];
    return [appDelegate managedObjectContext];
}

- (void)windowDidLoad
{
    [super windowDidLoad];

    ADXAccount *defaultAccount = [self.loginProcessor defaultAccount];
    if (defaultAccount) {
        // Start at the Login panel
        self.loginName = defaultAccount.loginName;
        self.password = defaultAccount.password;
        self.serverURL = defaultAccount.serverURL;
        self.rememberPassword = [defaultAccount.rememberPassword copy];;

        // TODO: Default this to yes until remembering passwords works
        self.rememberPassword = [NSNumber numberWithBool:YES];

        [self showPanel:self.loginPanel];
    } else {
#ifdef DEBUG
        if (self.serverURL == nil || self.serverURL.length == 0) {
            self.serverURL = @"http://fresh.activdox.com:8080/";
        }
#endif
        
        [self showPanel:self.loginOrRegisterPanel];
    }

    [self processPendingURLRequest];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(processURLSchemeNotification:) name:kADXURLSchemeRequestNotification object:nil];
}

- (void)showPanel:(NSView *)panel {
    // Remove the old panel
    [self.currentPanel removeFromSuperview];
    [self.currentPanel setHidden:YES];
    
    // Add and reveal the new panel
    [self.placeholderPanel addSubview:panel];
    [panel setHidden:NO];

    NSView *newFirstResponder = [panel viewWithTag:1000];
    [newFirstResponder becomeFirstResponder];
    
    self.currentPanel = panel;

    // Set the height constraint on the placeholder to the height of the panel we just added
    self.containerHeightConstraint.constant = self.currentPanel.frame.size.height;
}

- (IBAction)exitClicked:(id)sender {
    [NSApp stop:nil];
}

- (IBAction)cancelClicked:(id)sender {
    [self showPanel:self.loginOrRegisterPanel];
}

- (IBAction)needAccountClicked:(id)sender {
    [self showPanel:self.registerPanel];
}

- (IBAction)haveAccountClicked:(id)sender {
    [self showPanel:self.loginPanel];
}

- (void)registrationAcceptedForConfirmation {
    // Show a message letting the user know to expect an email
    NSAlert *alert = [NSAlert alertWithMessageText:NSLocalizedString(@"A response to your registration request will be sent to your email address.", nil)
                                     defaultButton:NSLocalizedString(@"OK", nil)
                                   alternateButton:nil
                                       otherButton:nil
                         informativeTextWithFormat:@""];

    [alert runModal];
    
    // Switch to the Login panel
    self.loginInProgress = [NSNumber numberWithBool:NO];
    [self haveAccountClicked:self];
}

- (IBAction)registerClicked:(id)sender {
    self.loginInProgress = [NSNumber numberWithBool:YES];
    
    ADXAppModel *appModel = [ADXAppDelegate instance].appModel;
    
    appModel.statusManager.statusTitle = [NSString stringWithFormat:NSLocalizedString(@"Registering account %@", nil), self.loginName];
    [appModel.statusManager showStatusMessage:NSLocalizedString(@"Connecting", nil)];
    
    [self.loginProcessor registerWithName:self.loginName
                                    email:self.email
                                 password:self.password
                                   server:self.serverURL completion:^(ADXLoginProcessorRegistrationResponse status, NSError *error) {
                                       dispatch_async(dispatch_get_main_queue(), ^{
                                           switch (status) {
                                               case ADXLoginProcessorRegistrationAcceptedAndReadyForLogin:
                                                   // Continue to login
                                                   [self loginClicked:self];
                                                   break;
                                               case ADXLoginProcessorRegistrationAcceptedForConfirmation:
                                                   [self registrationAcceptedForConfirmation];
                                                   break;
                                               default:
                                                   NSLog(@"Registration failure: %@", error);
                                                   self.loginInProgress = [NSNumber numberWithBool:NO];
                                                   [[ADXAppModel instance].statusManager showStatusMessage:@"\u2757 Unable to register"];
                                                   break;
                                           }
                                       });
                                   }];
}

- (void)handleLoginFailure:(ADXAccount *)account {
    [ADXAppModel instance].statusManager.statusTitle = NSLocalizedString(@"Not logged in", nil);

    [[ADXAppModel instance].statusManager showStatusMessage:@"\u2757 Unable to log in"];
    
    self.loginInProgress = [NSNumber numberWithBool:NO];
}

- (void)handleLoginSuccessWithAccount:(ADXAccount *)account service:(ADXV2Service *)service {
    [ADXAppModel instance].account = account;
    [ADXAppModel instance].service = service;

    [ADXAppModel instance].statusManager.statusTitle = NSLocalizedString(@"Welcome to ADoxReader", nil);
    [[ADXAppModel instance].statusManager showStatusMessage:@""];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kADXURLSchemeRequestNotification object:nil];
    
    [NSApp endSheet:self.window];
    [self.window orderOut:self.window];
}

- (IBAction)loginClicked:(id)sender {
    self.loginInProgress = [NSNumber numberWithBool:YES];
    
    ADXAppModel *appModel = [ADXAppDelegate instance].appModel;
    
    appModel.statusManager.statusTitle = NSLocalizedString(@"Logging in", nil);
    [appModel.statusManager showStatusMessage:NSLocalizedString(@"Connecting", nil)];
    
    [self.loginProcessor performLoginWithName:self.loginName
                                     password:self.password
                                       server:self.serverURL
                             rememberPassword:self.rememberPassword.boolValue
                                  completion:^(BOOL success, NSUInteger statusCode, BOOL online, ADXAccount *account, ADXV2Service *service, NSError *error) {
                                      dispatch_async(dispatch_get_main_queue(), ^{
                                          if (success) {
                                              [self handleLoginSuccessWithAccount:account service:service];
                                          } else {
                                              [NSApp presentError:error];
                                              [self handleLoginFailure:account];
                                          }
                                      });
                                  }];
}

#pragma mark - URL Scheme Handling

- (void)urlHandlerOpenDocumentWithID:(NSString *)documentID
{
    NSAssert(false, @"This should not be handled here");
}

- (void)urlHandlerShowLoginForUserID:(NSString *)userID
{
    [self showPanel:self.loginPanel];
    self.loginName = userID;
    self.password = @"";
}

- (void)processPendingURLRequest
{
    /// Use the URL from the launch request, if we have one
    if ([ADXAppModel instance].pendingURLSchemeRequest != nil) {
        if ([ADXAppModel instance].pendingURLSchemeRequest.requiredServer) {
            self.serverURL = [NSString stringWithFormat:@"http://%@", [ADXAppModel instance].pendingURLSchemeRequest.requiredServer];
        }
        
        if ([ADXAppModel instance].pendingURLSchemeRequest.requireThatAppHasNotLoggedIn) {
            // We're ready
            [ADXAppModel instance].pendingURLSchemeRequest.delegate = self;
            [[ADXAppModel instance].pendingURLSchemeRequest dispatch];
            [ADXAppModel instance].pendingURLSchemeRequest = nil;
        }
    }
    
}

- (void)processURLSchemeNotification:(NSNotification *)notification
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.window makeKeyAndOrderFront:self.window];
        [self processPendingURLRequest];
    });
}
@end
