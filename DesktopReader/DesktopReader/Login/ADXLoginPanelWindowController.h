//
//  ADXLoginPanelWindowController.h
//  DesktopReader
//
//  Created by Steve Tibbett on 12-06-29.
//  Copyright (c) 2012 ActivDox. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <ActivDoxCore.h>

@class ADXMainWindowController;

@interface ADXLoginPanelWindowController : NSWindowController <ADXURLSchemeRequestDelegate>

@property (assign, nonatomic) ADXMainWindowController *parentWindowController;

@property (strong, nonatomic) NSString *loginName;
@property (strong, nonatomic) NSString *password;
@property (strong, nonatomic) NSString *passwordVerify;
@property (strong, nonatomic) NSString *serverURL;
@property (strong, nonatomic) NSString *email;
@property (strong, nonatomic) NSNumber *rememberPassword;

// This is a boolean that the UI binds to to disable itself while login is in progress
@property (strong, nonatomic) NSNumber *loginInProgress;

@end
