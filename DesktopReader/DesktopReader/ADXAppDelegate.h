//
//  ADXAppDelegate.h
//  DesktopReader
//
//  Created by Steve Tibbett on 12-04-30.
//  Copyright (c) 2012 ActivDox. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ActivDoxCore.h"

@class ADXAppDocument;
@class ADXAppModel;
@class ADXStatusManager;

@interface ADXAppDelegate : NSObject <NSUserNotificationCenterDelegate>

@property (readonly, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (strong, nonatomic) ADXAppModel *appModel;
@property (strong, nonatomic) ADXStatusManager *statusManager;
@property (strong, nonatomic) ADXRemoteNotificationRelay *remoteNotificationRelay;

+ (ADXAppDelegate *)instance;

- (void)setupCoreDataStack;
@end
