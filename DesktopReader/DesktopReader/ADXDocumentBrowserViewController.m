//
//  ADXDocumentBrowserViewController.m
//  DesktopReader
//
//  Created by Steve Tibbett on 12-05-24.
//  Copyright (c) 2012 ActivDox. All rights reserved.
//

#import "ADXDocumentBrowserViewController.h"
#import "ADXAppDelegate.h"
#import "ADXAppModel.h"
#import "ADXDocumentBrowserViewItem.h"
#import "ADXMainWindowController.h"
#import "ADXImageBrowserView.h"
#import "ADXImageBrowserDocumentCell.h"
#import "ADXDocumentBrowserViewItem.h"

@interface ADXDocumentBrowserViewController ()
@property (strong, nonatomic) NSArray *items;
@property BOOL observing;
@property BOOL pendingRefresh;
@property (strong, nonatomic) NSString *searchString;
@end

@implementation ADXDocumentBrowserViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Initialization code here.
    }
    
    return self;
}

- (void)loadView
{
    [super loadView];

    if (!self.observing) {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleProgressNotificationUpdate:) name:kADXServiceNotificationProgressUpdate object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleProgressNotificationFinished:) name:kADXServiceNotificationProgressUpdateFinished object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(containerRefreshedNotification:) name:kADXServiceCollectionRefreshCompletedNotification object:nil];
        
        self.observing = YES;
    }
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)reloadData {
    ADXMainWindowController *windowController = self.view.window.windowController;
    if ([windowController class] != [ADXMainWindowController class]) {
        // This can happen if the user switches away from the view
        return;
    }

    // Remember the currently selected objects
    NSMutableArray *selectedObjects = [NSMutableArray array];
    [self.imageBrowserView.selectionIndexes enumerateIndexesUsingBlock:^(NSUInteger idx, BOOL *stop) {
        ADXDocument *doc = [[self.items objectAtIndex:idx] document];
        [selectedObjects addObject:doc];
    }];

    NSSet *documents = windowController.selectedFolder.documents;
    
    NSArray *sortDescriptors = [windowController mainListSortDescriptors];
    if (sortDescriptors == nil) {
        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"title" ascending:YES];
        sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    }

    NSMutableArray *mutableItems = [NSMutableArray array];
    NSArray *sortedArray = [documents sortedArrayUsingDescriptors:sortDescriptors];
    NSArray *finalArray = [sortedArray filteredArrayUsingPredicate:[ADXAppModel filterPredicateForSearchString:self.searchString]];
    for (ADXDocument *doc in finalArray) {
        [mutableItems addObject:[ADXDocumentBrowserViewItem itemWithImageBrowser:self.imageBrowserView document:doc]];
    }
    self.items = mutableItems;
    
    [self.imageBrowserView reloadData];
    
    // Restore the selection
    NSMutableIndexSet *newSelection = [NSMutableIndexSet indexSet];
    for (int newIdx = 0; newIdx < self.items.count; newIdx++) {
        ADXDocument *doc = [[self.items objectAtIndex:newIdx] document];
        NSUInteger idx = [selectedObjects indexOfObject:doc];
        if (idx != NSNotFound) {
            [newSelection addIndex:newIdx];
        }
    }
    
    [self.imageBrowserView setSelectionIndexes:newSelection
                          byExtendingSelection:NO];
}

#pragma mark - IKImageBrowserDataSource informal protocol

- (NSUInteger)numberOfItemsInImageBrowser:(IKImageBrowserView *)aBrowser {
    return self.items.count;
}

- (id)imageBrowser:(IKImageBrowserView *)aBrowser itemAtIndex:(NSUInteger)index {
    return [self.items objectAtIndex:index];
}

#pragma mark - Notifications

- (void)handleProgressNotificationFinished:(NSNotification *)notification
{
    dispatch_async(dispatch_get_main_queue(), ^{
        NSString *documentID = [notification.userInfo objectForKey:ADXDocumentAttributes.id];
        for (ADXDocumentBrowserViewItem *item in self.items) {
            if ([documentID isEqualToString:item.document.id]) {
                [item updateFromDocument];
                [self.imageBrowserView reloadData];
                break;
            }
        }
    });
}

- (void)handleProgressNotificationUpdate:(NSNotification *)notification
{
    dispatch_async(dispatch_get_main_queue(), ^{
        NSString *documentID = [notification.userInfo objectForKey:ADXDocumentAttributes.id];
        for (ADXDocumentBrowserViewItem *item in self.items) {
            if ([documentID isEqualToString:item.document.id]) {

                NSNumber *newReadBytes = [notification.userInfo objectForKey:kADXServiceNotificationProgressUpdateUserInfoBytesReadKey];
                NSNumber *newExpectedBytes = [notification.userInfo objectForKey:kADXServiceNotificationProgressUpdateUserInfoBytesExpectedKey];
                
                if (item.document.downloadedValue) {
                    item.isDownloading = NO;
                } else {
                    if (newExpectedBytes.longLongValue > 0) {
                        item.isDownloading = YES;
                        item.percentDone = (float)newReadBytes.floatValue / (float)newExpectedBytes.floatValue;
                    } else {
                        item.isDownloading = NO;
                    }
                }
                
                item.isLatestVersion = item.document.versionIsLatestValue;
                
                [item incrementVersion];
                
                // Throttle refresh to 10 times per second
                @synchronized(self) {
                    if (!self.pendingRefresh) {
                        self.pendingRefresh = YES;
                        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, NSEC_PER_SEC*(0.10)),
                                       dispatch_get_main_queue(), ^{
                                           [self.imageBrowserView setNeedsDisplay:YES];
                                           self.pendingRefresh = NO;
                                       });
                    }
                }
                
                return;
            }
        }
    });
}

- (void)containerRefreshedNotification:(NSNotification *)notification {
    [self reloadData];
}

#pragma mark - IKImageBrowserDelegate informal protocol

- (void)imageBrowser:(IKImageBrowserView *)aBrowser cellWasDoubleClickedAtIndex:(NSUInteger)index {
    ADXDocument *document = [[self.items objectAtIndex:index] document];
    if (document.downloaded.boolValue) {
        [self.delegate openDocument:document];
    } else {
        [[ADXAppDelegate instance].appModel.service downloadContentForDocument:document.id
                                                                     versionID:document.version
                                                                 trackProgress:YES
                                                                    completion:^(BOOL success, NSError *error) {
                                                                        NSLog(@"Version download complete");
                                                                    }];
    }
}

- (void)imageBrowserSelectionDidChange:(IKImageBrowserView *)aBrowser
{
    NSIndexSet *selectionIndexes = [aBrowser selectionIndexes];
    ADXDocument *document = nil;
    if (selectionIndexes != nil && selectionIndexes.count > 0) {
        document = [[self.items objectAtIndex:[selectionIndexes firstIndex]] document];
    }

    [self.delegate documentBrowserSelectionChanged:document];
}

#pragma mark - Search

- (void)findString:(NSString *)string {
    self.searchString = string;
    [self reloadData];
}

@end
