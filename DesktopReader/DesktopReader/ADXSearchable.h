//
//  ADXSearchable.h
//  DesktopReader
//
//  Created by Steve Tibbett on 2012-09-11.
//  Copyright (c) 2012 ActivDox. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ADXSearchable <NSObject>
- (void)findString:(NSString *)string;
@end
