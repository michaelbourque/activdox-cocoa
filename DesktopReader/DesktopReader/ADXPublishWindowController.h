//
//  ADXPublishWindowController.h
//  DesktopReader
//
//  Created by Steve Tibbett on 1/08/12.
//  Copyright (c) 2012 ActivDox. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface ADXPublishWindowController : NSWindowController

@property (weak) IBOutlet NSTextField *uploadLabel;
@property (weak) IBOutlet NSProgressIndicator *progressIndicator;

- (void)publishDocumentsWithPaths:(NSArray *)paths;

@end
