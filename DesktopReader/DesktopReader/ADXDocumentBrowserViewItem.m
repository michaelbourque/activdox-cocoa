//
//  ADXDocumentBrowserViewItem.m
//  DesktopReader
//
//  Created by Steve Tibbett on 12-05-23.
//  Copyright (c) 2012 ActivDox. All rights reserved.
//

#import <Quartz/Quartz.h>
#import "ADXDocumentBrowserViewItem.h"
#import "ActivDoxCore.h"
#import "ADXAppDelegate.h"
#import "ADXAppModel.h"

@interface ADXDocumentBrowserViewItem()
@property (strong, atomic) PDFPage *page;
@property BOOL downloaded;
@property (atomic) int version;
@end

@implementation ADXDocumentBrowserViewItem

+ (ADXDocumentBrowserViewItem *)itemWithImageBrowser:(IKImageBrowserView *)theImageBrowserView document:(ADXDocument *)theDocument {
    ADXDocumentBrowserViewItem *item = [[ADXDocumentBrowserViewItem alloc] init];
    item.document = theDocument;
    item.imageBrowserView = theImageBrowserView;
    
    [item updateFromDocument];

    return item;
}

- (void)updateFromDocument
{
    self.downloaded = self.document.downloaded.boolValue;
    self.isLatestVersion = self.document.versionIsLatestValue;
    [self incrementVersion];
}

- (void)incrementVersion
{
    self.version++;
}

- (BOOL)shouldShowNewBadge
{
    BOOL result = NO;
    if (self.document.versionIsLatestValue &&
        !self.document.mostRecentOpenedDate &&
        [self.document.changeCount integerValue] == 0) {
        
        result = YES;
    }
    return result;
}

- (BOOL)shouldShowChangeBadge
{
    BOOL result = NO;
    if ([self.document.changeCount integerValue] > 0 && ![self shouldShowNewBadge]) {
        result = YES;
    }
    return result;
}

- (NSString *)badgeText
{
    NSString *result = nil;
    if ([self shouldShowChangeBadge]) {
        result = [self.document.changeCount stringValue];
    } else if ([self shouldShowNewBadge]) {
        result = @"New";
    }
    return result;
}


#pragma mark - IKImageBrowserItem informal protocol

- (NSString *)imageUID {
    NSString *imageid = self.document.objectID.URIRepresentation.absoluteString;
    return imageid;
}

- (NSString *)imageRepresentationType {
    return IKImageBrowserPDFPageRepresentationType;
}

- (id)imageRepresentation {
    if (self.page == nil) {
        PDFDocument *doc = [[PDFDocument alloc] initWithData:self.document.content.data];
        self.page = [doc pageAtIndex:0];
    }
    
    return self.page;
}
        
- (NSString *)imageTitle {
    NSString *title = [self.document.title stringByDeletingPathExtension];
    if (self.useVersionAsTitle) {
        title = [NSString stringWithFormat:NSLocalizedString(@"Version %d", nil), (int)self.document.version.integerValue];
    }
    return title;
}

- (BOOL)isSelectable {
    return YES;
}

- (NSUInteger)imageVersion {
    return self.version;
}


- (NSImage *)ribbonImage
{
    NSImage *result = nil;
    NSString *ribbonColour = nil;
    NSString *ribbonImageName = nil;
    
    if ([self.document.status isEqualToString:ADXDocumentStatus.accessible]) {
        ribbonColour = @"green";
    } else if ([self.document.status isEqualToString:ADXDocumentStatus.revoked]) {
        ribbonColour = @"orange";
    } else if ([self.document.status isEqualToString:ADXDocumentStatus.inaccessible]) {
        ribbonColour = @"orange";
        ribbonImageName = [NSString stringWithFormat:@"%@_corner_bw", ribbonColour];
    }
    else {
        ribbonColour = @"orange";
    }
    
    ADXV2Service *service = [ADXV2Service serviceForManagedObject:self.document];
    if ([service networkReachable] && [service serverReachable]) {
        ribbonImageName = [NSString stringWithFormat:@"%@_corner", ribbonColour];
    } else {
        ribbonImageName = [NSString stringWithFormat:@"%@_corner_bw", ribbonColour];
    }
    
    result = [NSImage imageNamed:ribbonImageName];
    return result;
}

@end
