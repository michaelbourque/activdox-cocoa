//
//  ADXShareWindowController.h
//  DesktopReader
//
//  Created by Steve Tibbett on 2012-09-25.
//  Copyright (c) 2012 ActivDox. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "ActivDoxCore.h"

@interface ADXShareWindowController : NSWindowController <NSTextFieldDelegate>

@property (strong, nonatomic) ADXDocument *documentToShare;

@end
