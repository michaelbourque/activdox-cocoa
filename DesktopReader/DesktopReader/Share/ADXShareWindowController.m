//
//  ADXShareWindowController.m
//  DesktopReader
//
//  Created by Steve Tibbett on 2012-09-25.
//  Copyright (c) 2012 ActivDox. All rights reserved.
//

#import "ADXShareWindowController.h"
#import <AddressBook/AddressBook.h>
#import "ADXAppModel.h"

@interface ADXShareWindowController ()
@property (weak) IBOutlet NSTextField *emailField;
@property (nonatomic) BOOL isCompleting;
@property (strong, nonatomic) NSString *previousText;
@property (strong, nonatomic) NSTimer *timer;
@property (strong, nonatomic) NSString *emailFieldText;
@end

@implementation ADXShareWindowController

- (id)initWithWindow:(NSWindow *)window
{
    self = [super initWithWindow:window];
    if (self) {
        // Initialization code here.
    }
    
    return self;
}

- (void)windowDidLoad
{
    [super windowDidLoad];
    
    [self.window center];
    self.window.level = NSStatusWindowLevel;
}

- (NSArray *)control:(NSControl *)control textView:(NSTextView *)textView completions:(NSArray *)words forPartialWordRange:(NSRange)charRange indexOfSelectedItem:(NSInteger *)index {
    
    NSMutableArray *result = [NSMutableArray array];
    
    NSString *textSoFar = [textView.string substringWithRange:charRange];
    
    ABAddressBook *addressBook = [ABAddressBook sharedAddressBook];
    ABSearchElement *searchElement = [ABPerson searchElementForProperty:kABEmailProperty label:nil key:nil value:textSoFar comparison:kABPrefixMatchCaseInsensitive];
    NSArray *results = [addressBook recordsMatchingSearchElement:searchElement];
    for (ABPerson *resultPerson in results) {
        ABMultiValue *addresses = [resultPerson valueForKey:kABEmailProperty];
        if (addresses != nil) {
            NSUInteger numEntries = addresses.count;
            for (int idx = 0; idx < numEntries; idx++) {
                NSString *addr = [addresses valueAtIndex:idx];
                if ([addr rangeOfString:textSoFar options:NSCaseInsensitiveSearch].location != NSNotFound) {
                    [result addObject:addr];
                }
            }
        }
    }

    return result;
}


- (void)showComplete:(NSTimer *)timer {
    self.isCompleting = YES;
    [timer.userInfo complete:self];
    self.isCompleting = NO;
}

- (void)controlTextDidChange:(NSNotification *)obj {
    if (self.isCompleting) {
        return;
    }
    
    if (self.timer) {
        [self.timer invalidate];
    }
    
    NSTextView *textView = [[obj userInfo] objectForKey:@"NSFieldEditor"];
    
    self.timer = [NSTimer timerWithTimeInterval:0.25 target:self selector:@selector(showComplete:) userInfo:textView repeats:NO];
    [[NSRunLoop mainRunLoop] addTimer:self.timer forMode:NSRunLoopCommonModes];
}

- (void)showShareSuccessAlert
{
    // Show a message letting the user know to expect an email
    NSAlert *alert = [NSAlert alertWithMessageText:NSLocalizedString(@"Sharing complete.", nil)
                                     defaultButton:NSLocalizedString(@"OK", nil)
                                   alternateButton:nil
                                       otherButton:nil
                         informativeTextWithFormat:@""];
    
    [alert runModal];
}

- (IBAction)performShare:(id)sender {
    // TODO: replace placeholder
    [[ADXAppModel instance].service shareDocument:self.documentToShare
                               withEmailAddresses:[NSArray arrayWithObject:self.emailFieldText]
                                          message:@"A document has been shared with you.  (PLACEHOLDER TEXT)"
                                       completion:^(id response, BOOL success, NSError *error) {
                                           if (error != nil) {
                                               [NSApp presentError:error];
                                           } else {
                                               [self close];
                                               [self showShareSuccessAlert];
                                           }
                                       }];
}

@end
