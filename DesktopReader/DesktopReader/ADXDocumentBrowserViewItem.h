//
//  ADXDocumentBrowserViewItem.h
//  DesktopReader
//
//  Created by Steve Tibbett on 12-05-23.
//  Copyright (c) 2012 ActivDox. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "ActivDoxCore.h"

@interface ADXDocumentBrowserViewItem : NSObject

@property (strong, nonatomic) ADXDocument *document;
@property (weak, nonatomic) IKImageBrowserView *imageBrowserView;
@property BOOL useVersionAsTitle;

// Cached values
@property (atomic) BOOL isDownloading;
@property (atomic) BOOL isLatestVersion;
@property (atomic) float percentDone;

- (BOOL)shouldShowNewBadge;
- (BOOL)shouldShowChangeBadge;
- (NSString *)badgeText;

+ (ADXDocumentBrowserViewItem *)itemWithImageBrowser:(IKImageBrowserView *)theImageBrowserView document:(ADXDocument *)theDocument;
- (void)incrementVersion;

- (void)updateFromDocument;

- (NSImage *)ribbonImage;

@end
