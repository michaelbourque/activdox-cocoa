//
//  ADXStatusPanelView.m
//  DesktopReader
//
//  Created by Steve Tibbett on 12-04-30.
//  Copyright (c) 2012 ActivDox. All rights reserved.
//

#import "ADXStatusPanelView.h"
#import "NSShadow+MCAdditions.h"
#import "NSBezierPath+MCAdditions.h"
#import "ADXMainWindowController.h"
#import "ADXAppDelegate.h"
#import "ADXAppModel.h"
#import "ADXMainWindowController.h"

#define IMAGE_RIGHT_MARGIN 12

@interface ADXStatusPanelMessage : NSObject
@property (strong, nonatomic) NSString *message;
@end

@implementation ADXStatusPanelMessage
@synthesize message = _message;
@end

@interface ADXStatusPanelView()
@property (strong, nonatomic) NSMutableArray *messageStack;
@property (strong, nonatomic) NSTextField *topLabel;
@property (strong, nonatomic) NSTextField *topMessageLabel;
@property (strong, nonatomic) NSTextField *nextMessageLabel;
@property (strong, nonatomic) NSView *bottomLabelContainer;
@property BOOL animating;
@property (strong, nonatomic) NSImageView *documentStatusImageView;
@end

@implementation ADXStatusPanelView
@dynamic topStatusLine;
@synthesize topLabel = _topLabel;
@synthesize topMessageLabel = _topMessageLabel;
@synthesize nextMessageLabel = _nextMessageLabel;
@synthesize bottomLabelContainer = _bottomLabelContainer;
@synthesize animating = _animating;
@synthesize messageStack = _messageStack;

- (id)initWithFrame:(NSRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.messageStack = [NSMutableArray array];

        // Pick fonts for the top and bottom message, and determine their height
        NSFont *topMessageFont = [NSFont systemFontOfSize:[NSFont systemFontSize]];
        NSFont *bottomMessageFont = [NSFont systemFontOfSize:[NSFont systemFontSize]*.8];
        NSDictionary *fontDict = [NSDictionary dictionaryWithObject:topMessageFont
                                                             forKey:NSFontAttributeName];
        CGSize topFontSize = [@"Iy" sizeWithAttributes:fontDict];
        fontDict = [NSDictionary dictionaryWithObject:bottomMessageFont
                                               forKey:NSFontAttributeName];
        CGSize bottomFontSize = [@"Iy" sizeWithAttributes:fontDict];

        // Create the top label
        self.topLabel = [[NSTextField alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, topFontSize.height)];
        self.topLabel.backgroundColor = [NSColor clearColor];
        self.topLabel.drawsBackground = NO;
        self.topLabel.alignment = NSCenterTextAlignment;
        self.topLabel.editable = NO;
        [self.topLabel setBordered:NO];
        [self addSubview:self.topLabel];

        // Create a container for the bottom labels, to clip them as they animate.
        self.bottomLabelContainer = [[NSView alloc] initWithFrame:CGRectMake(0, 4, frame.size.width, bottomFontSize.height+4)];
        [self addSubview:self.bottomLabelContainer];

        // Create the first of the two bottom labels
        self.topMessageLabel = [[NSTextField alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, topFontSize.height)];
        self.topMessageLabel.font = [NSFont systemFontOfSize:[NSFont systemFontSize]*.8];
        self.topMessageLabel.backgroundColor = [NSColor clearColor];
        self.topMessageLabel.drawsBackground = NO;
        [self.topMessageLabel setBordered:NO];
        self.topMessageLabel.stringValue = @"";
        self.nextMessageLabel.editable = NO;
        self.topMessageLabel.alignment = NSCenterTextAlignment;
        [self.bottomLabelContainer addSubview:self.topMessageLabel];
        
        // Create the second of the two bottom labels
        self.nextMessageLabel = [[NSTextField alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, bottomFontSize.height)];
        self.nextMessageLabel.font = bottomMessageFont;
        self.nextMessageLabel.backgroundColor = [NSColor clearColor];
        self.nextMessageLabel.drawsBackground = NO;
        [self.nextMessageLabel setBordered:NO];
        self.nextMessageLabel.stringValue = @"";
        self.nextMessageLabel.editable = NO;
        self.nextMessageLabel.alignment = NSCenterTextAlignment;
        [self.bottomLabelContainer addSubview:self.nextMessageLabel];
        
        NSImage *statusImage = [NSImage imageNamed:@"document_latest"];
        self.documentStatusImageView = [[NSImageView alloc] initWithFrame:NSMakeRect(0, 0, statusImage.size.width, statusImage.size.height)];
        self.documentStatusImageView.image = statusImage;
        self.documentStatusImageView.hidden = YES;
        [self addSubview:self.documentStatusImageView];
        
        [self layout];
        
        [self addNotifications];
    }
    
    return self;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (ADXMainWindowController *)mainWindow
{
    return [ADXAppDelegate instance].appModel.mainWindow;
}

- (void)addNotifications
{
    ADXMainWindowController *mainWindow = [self mainWindow];
    
    // TODO: switch to addObserver or release the notification correctly
    [[NSNotificationCenter defaultCenter] addObserverForName:kADXWindowSelectionChangedNotification
                                                      object:mainWindow
                                                       queue:[NSOperationQueue mainQueue]
                                                  usingBlock:^(NSNotification *note) {
                                                      [self updateStatusIndicator];
                                                  }];

    // TODO: switch to addObserver or release the notification correctly
    [[NSNotificationCenter defaultCenter] addObserverForName:kADXServiceNotificationReachabilityChanged object:nil queue:nil usingBlock:^(NSNotification *note) {
        [self updateStatusIndicator];
    }];
    
    // TODO: switch to addObserver or release the notification correctly
    [[NSNotificationCenter defaultCenter] addObserverForName:kADXServiceNotificationLatestDocumentVersionChanged object:nil queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification *note) {
        [self updateStatusIndicator];
    }];
    
    [mainWindow addObserver:self forKeyPath:@"contentMode" options:NSKeyValueObservingOptionNew context:nil];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    [self updateStatusIndicator];
}

#pragma mark - Layout and Rendering

- (void)layout {
    // Position the top label
    CGRect frame = self.topLabel.frame;
    frame.origin.x = self.bounds.size.width/2 - (frame.size.width/2);
    frame.origin.y = self.bounds.size.height - (frame.size.height + 6);
    self.topLabel.frame = frame;

    self.documentStatusImageView.frame = CGRectMake(self.bounds.size.width - (self.documentStatusImageView.frame.size.width + IMAGE_RIGHT_MARGIN),
                                                self.bounds.size.height/2 - (self.documentStatusImageView.frame.size.height/2),
                                                self.documentStatusImageView.frame.size.width,
                                                self.documentStatusImageView.frame.size.height);
    
    [super layout];
}

- (void)drawRect:(NSRect)dirtyRect {

    static NSShadow *kDropShadow = nil;
    static NSShadow *kInnerShadow = nil;
    static NSGradient *kBackgroundGradient = nil;
    static NSColor *kBorderColor = nil;
    
    if (kDropShadow == nil) {
        kDropShadow = [[NSShadow alloc] initWithColor:[NSColor colorWithCalibratedWhite:.863 alpha:.75] offset:NSMakeSize(0, -1.0) blurRadius:1.0];
        kInnerShadow = [[NSShadow alloc] initWithColor:[NSColor colorWithCalibratedWhite:0.0 alpha:.52] offset:NSMakeSize(0.0, -1.0) blurRadius:4.0];
        kBorderColor = [NSColor colorWithCalibratedWhite:0.569 alpha:1.0];
        kBackgroundGradient = [[NSGradient alloc] initWithColorsAndLocations:[NSColor colorWithCalibratedRed:0.957 green:0.976 blue:1.0 alpha:1.0],0.0,[NSColor colorWithCalibratedRed:0.871 green:0.894 blue:0.918 alpha:1.0],0.5,[NSColor colorWithCalibratedRed:0.831 green:0.851 blue:0.867 alpha:1.0],0.5,[NSColor colorWithCalibratedRed:0.82 green:0.847 blue:0.89 alpha:1.0],1.0, nil];
    }
    
    NSRect bounds = [self bounds];
    bounds.size.height -= 1.0;
    bounds.origin.y += 1.0;
    
    NSBezierPath *path = [NSBezierPath bezierPathWithRoundedRect:bounds xRadius:3.5 yRadius:3.5];
    
    [NSGraphicsContext saveGraphicsState];
    [kDropShadow set];
    [path fill];
    [NSGraphicsContext restoreGraphicsState];
    
    [kBackgroundGradient drawInBezierPath:path angle:-90.0];
    
    [kBorderColor setStroke];
    [path strokeInside];
    
    [path fillWithInnerShadow:kInnerShadow];
}

#pragma mark - Messages

- (NSString *)topStatusLine {
    return self.topLabel.stringValue;
}

- (void)setTopStatusLine:(NSString *)topStatusLine {
    self.topLabel.stringValue = topStatusLine;
}

- (void)messageAnimationComplete {
    int containerHeight = self.bottomLabelContainer.frame.size.height;
    
    // Swap the message into the top label now that the animation is done,
    // and queue up the next one
    self.topMessageLabel.stringValue = self.nextMessageLabel.stringValue;
    self.topMessageLabel.frame = self.nextMessageLabel.frame;
    self.nextMessageLabel.frame = CGRectMake(0, -containerHeight, 400, containerHeight);
    self.nextMessageLabel.stringValue = @"";
    
    [self.messageStack removeObjectAtIndex:0];
    
    if (self.messageStack.count > 0) {
        ADXStatusPanelMessage *message = (ADXStatusPanelMessage *)[self.messageStack objectAtIndex:0];
        self.nextMessageLabel.stringValue = message.message;

        __weak ADXStatusPanelView *weakSelf = self;

        [NSAnimationContext beginGrouping];
        [[NSAnimationContext currentContext] setDuration:0.5];
        [[NSAnimationContext currentContext] setCompletionHandler:^{
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 125 * USEC_PER_SEC), dispatch_get_main_queue(), ^{
                [weakSelf messageAnimationComplete];
            });
        }];
        [[self.topMessageLabel animator] setFrame:CGRectMake(0, containerHeight, 400, containerHeight)];
        [[self.nextMessageLabel animator] setFrame:CGRectMake(0, 0, 400, containerHeight)];
        [NSAnimationContext endGrouping];
    } else {
        self.animating = NO;
    }
}

- (void)showMessage:(NSString *)messageString {
    ADXStatusPanelMessage *message = [[ADXStatusPanelMessage alloc] init];
    message.message = messageString;
    [self.messageStack addObject:message];
    
    if (!self.animating) {
        self.animating = YES;
        
        self.nextMessageLabel.stringValue = messageString;
        
        int containerHeight = self.bottomLabelContainer.frame.size.height;

        CGRect frame = self.nextMessageLabel.frame;
        frame.origin.y = -containerHeight;
        self.nextMessageLabel.frame = frame;

        __weak ADXStatusPanelView *weakSelf = self;

        [NSAnimationContext beginGrouping];
        [[NSAnimationContext currentContext] setDuration:0.5];
        [[NSAnimationContext currentContext] setCompletionHandler:^{
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 125 * USEC_PER_SEC), dispatch_get_main_queue(), ^{
                [weakSelf messageAnimationComplete];
            });
        }];
        [[self.nextMessageLabel animator] setFrame:CGRectMake(0, 0, 400, containerHeight)];
        [[self.topMessageLabel animator] setFrame:CGRectMake(0, 20, 400, containerHeight)];
        [NSAnimationContext endGrouping];
    }
}

#pragma mark - Document status indicator

- (void)updateStatusIndicator
{
    ADXMainWindowController *mainWindow = [self mainWindow];

    if (mainWindow.contentMode == ADXMainWindowContentModeDocumentViewer) {
        ADXDocument *document = mainWindow.selectedDocument;
        if (document == nil) {
            self.documentStatusImageView.hidden = YES;
        } else {
            ADXV2Service *service = [ADXAppDelegate instance].appModel.service;
            BOOL online = [service networkReachable] && [service serverReachable];

            if (document.versionIsLatestValue) {
                if (online) {
                    self.documentStatusImageView.image = [NSImage imageNamed:@"document_latest"];
                } else {
                    self.documentStatusImageView.image = [NSImage imageNamed:@"offline_document_latest"];
                }
            } else {
                if (online) {
                    self.documentStatusImageView.image = [NSImage imageNamed:@"document_old"];
                } else {
                    self.documentStatusImageView.image = [NSImage imageNamed:@"offline_document_old"];
                }
            }

            self.documentStatusImageView.hidden = NO;
        }
    } else {
        self.documentStatusImageView.hidden = YES;
    }
}


@end
