//
//  ADXSourceListController.m
//  DesktopReader
//
//  Created by Steve Tibbett on 12-04-30.
//  Copyright (c) 2012 ActivDox. All rights reserved.
//

#import "ADXSourceListController.h"
#import "ADXSourceListItem.h"
#import "ADXAppModel.h"
#import <ActivDoxCore.h>
#import "ADXMainWindowController.h"
#import "ADXAppDelegate.h"

NSString *kADXSourceListIdentifierMyActivDox = @"myactivdox";

@interface ADXSourceListController()
@property (strong, nonatomic) ADXSourceListItem *publishedItem;
@end

@implementation ADXSourceListController
@synthesize sourceListItems = _sourceListItems;
@synthesize mainWindow = _mainWindow;
@synthesize sourceList = _sourceList;

- (id)initWithSourceList:(PXSourceList *)sourceList {
    self = [super init];
    if (self) {
        self.sourceList = sourceList;
        self.sourceListItems = [[NSMutableArray alloc] init];

        [self.sourceList registerForDraggedTypes:[NSArray arrayWithObject:(NSString*)kUTTypeFileURL]];
    }
    return self;
}

- (NSUInteger)findIndexForFolder:(ADXCollection *)folderToFind {
    // TODO: actually find the folder...
    return 1;
}


#pragma mark Source List Data Source Methods

- (NSUInteger)sourceList:(PXSourceList*)sourceList numberOfChildrenOfItem:(id)item
{
	//Works the same way as the NSOutlineView data source: `nil` means a parent item
	if(item==nil) {
		return [self.sourceListItems count];
	}
	else {
		return [[item children] count];
	}
}


- (id)sourceList:(PXSourceList*)aSourceList child:(NSUInteger)index ofItem:(id)item
{
	//Works the same way as the NSOutlineView data source: `nil` means a parent item
	if(item==nil) {
		return [self.sourceListItems objectAtIndex:index];
	}
	else {
		return [[item children] objectAtIndex:index];
	}
}


- (id)sourceList:(PXSourceList*)aSourceList objectValueForItem:(id)item
{
	return [item title];
}


- (void)sourceList:(PXSourceList*)aSourceList setObjectValue:(id)object forItem:(id)item
{
	[item setTitle:object];
}


- (BOOL)sourceList:(PXSourceList*)aSourceList isItemExpandable:(id)item
{
	return [item hasChildren];
}


- (BOOL)sourceList:(PXSourceList*)aSourceList itemHasBadge:(id)item
{
	return [item hasBadge];
}


- (NSInteger)sourceList:(PXSourceList*)aSourceList badgeValueForItem:(id)item
{
	return [item badgeValue];
}


- (BOOL)sourceList:(PXSourceList*)aSourceList itemHasIcon:(id)item
{
	return [item hasIcon];
}


- (NSImage*)sourceList:(PXSourceList*)aSourceList iconForItem:(id)item
{
	return [item icon];
}

- (NSMenu*)sourceList:(PXSourceList*)aSourceList menuForEvent:(NSEvent*)theEvent item:(id)item
{
	if ([theEvent type] == NSRightMouseDown || ([theEvent type] == NSLeftMouseDown && ([theEvent modifierFlags] & NSControlKeyMask) == NSControlKeyMask)) {
		NSMenu * m = [[NSMenu alloc] init];
		if (item != nil) {
			[m addItemWithTitle:[item title] action:nil keyEquivalent:@""];
		} else {
			[m addItemWithTitle:@"clicked outside" action:nil keyEquivalent:@""];
		}
		return m;
	}
	return nil;
}

- (void)refresh
{
    ADXAccount *account = [ADXAppModel instance].account;
    NSManagedObjectContext *moc = account.managedObjectContext;
    moc = [ADXAppModel instance].service.managedObjectContext;
    NSArray *collections = [ADXCollection collectionsForAccountID:account.id context:moc];

    self.sourceListItems = [NSMutableArray array];
    
    //Set up the "Library" parent item and children
    ADXSourceListItem *collectionsItem = [ADXSourceListItem itemWithTitle:@"COLLECTIONS" identifier:@"inbox"];
    
    NSMutableArray *collectionItems = [NSMutableArray array];
    for (ADXCollection *collection in collections) {
        ADXSourceListItem *item = [ADXSourceListItem itemWithTitle:collection.title identifier:collection.id];
        [collectionItems addObject:item];
    }

    [collectionsItem setChildren:collectionItems];

    [self.sourceListItems addObject:collectionsItem];

    [self.sourceList reloadData];
}

#pragma mark - Drag & Drop

- (NSDragOperation)sourceList:(PXSourceList *)sourceList validateDrop:(id<NSDraggingInfo>)info proposedItem:(id)item proposedChildIndex:(NSInteger)index {

    // Ensure drag is onto the Published item
    if (!(item == self.publishedItem && index == -1)) {
        return NSDragOperationNone;
    }

    // Ensure there is only one file being transferred (for now)
    if ([info numberOfValidItemsForDrop] != 1) {
        return NSDragOperationNone;
    }

    // Ensure it looks like a PDF file
    NSPasteboard *pasteboard = [info draggingPasteboard];
    if (pasteboard.pasteboardItems.count != 1) {
        return NSDragOperationNone;
    }

    // Ensure what's being offered is a file
    if ([pasteboard.types containsObject:NSFilenamesPboardType]) {
        NSArray *files = [pasteboard propertyListForType:NSFilenamesPboardType];
        NSString *path = [files lastObject];

        // Ensure it's a PDF file
        if ([[path lowercaseString].pathExtension isEqualToString:@"pdf"]) {
            return NSDragOperationCopy;
        }
    }
    
    return NSDragOperationNone;
}

- (BOOL)sourceList:(PXSourceList *)aSourceList acceptDrop:(id<NSDraggingInfo>)info item:(id)item childIndex:(NSInteger)index {
    NSPasteboard *pasteboard = [info draggingPasteboard];
    NSArray *files = [pasteboard propertyListForType:NSFilenamesPboardType];
    NSString *path = [files lastObject];

    [self.mainWindow publishDocumentsWithPaths:[NSArray arrayWithObject:path]];

    return YES;
}

@end
