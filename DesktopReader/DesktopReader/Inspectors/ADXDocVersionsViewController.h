//
//  ADXDocVersionsViewController.h
//  DesktopReader
//
//  Created by Steve Tibbett on 2012-08-17.
//  Copyright (c) 2012 ActivDox. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "ADXMainWindowController.h"
#import "ADXDocumentBrowserViewController.h"

@interface ADXDocVersionsViewController : NSViewController
@property (strong, nonatomic) id<ADXSelectionDataSource> dataSource;
@property (strong, nonatomic) id<ADXDocumentBrowserDelegate> delegate;
@end
