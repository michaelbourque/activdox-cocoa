//
//  ADXDocVersionsViewController.m
//  DesktopReader
//
//  Created by Steve Tibbett on 2012-08-17.
//  Copyright (c) 2012 ActivDox. All rights reserved.
//

#import "ActivDoxCore.h"
#import "ADXDocVersionsViewController.h"
#import "ADXMainWindowController.h"
#import "ADXImageBrowserView.h"
#import "ADXDocumentBrowserViewItem.h"
#import "ADXAppDelegate.h"
#import "ADXAppModel.h"

@interface ADXDocVersionsViewController ()
@property (weak) IBOutlet ADXImageBrowserView *imageBrowserView;
@property (strong, nonatomic) NSArrayController *arrayController;
@property (strong, nonatomic) ADXDocument *selectedDocument;
@end

@implementation ADXDocVersionsViewController

- (void)loadView
{
    [super loadView];

    [ADXDocument dumpInContext:[ADXAppDelegate instance].managedObjectContext];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(selectionChanged:) name:kADXWindowSelectionChangedNotification object:nil];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)requery
{
    // Remember the currently selected objects
    NSMutableArray *selectedObjects = [NSMutableArray array];
    [self.imageBrowserView.selectionIndexes enumerateIndexesUsingBlock:^(NSUInteger idx, BOOL *stop) {
        [selectedObjects addObject:[self.arrayController.arrangedObjects objectAtIndex:idx]];
    }];
        
    // Update the array controller
    self.arrayController = [[NSArrayController alloc] init];
    self.arrayController.managedObjectContext = self.selectedDocument.managedObjectContext;
    self.arrayController.entityName = [ADXDocument entityName];
    self.arrayController.sortDescriptors = [NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:ADXDocumentAttributes.version ascending:NO]];
    self.arrayController.filterPredicate = [NSPredicate predicateWithFormat:@"%K == %@", ADXDocumentAttributes.id, self.selectedDocument.id];
    NSError *error = nil;
    if (![self.arrayController fetchWithRequest:nil merge:YES error:&error]) {
        LogError(@"Unexpected error: %@", error);
    }
    
    // Reload the view
    [self.imageBrowserView reloadData];

    // Restore the selection
    NSMutableIndexSet *newSelection = [NSMutableIndexSet indexSet];
    int newIdx = 0;
    for (ADXDocument *document in self.arrayController.arrangedObjects) {
        ADXDocument *document = [self.arrayController.arrangedObjects objectAtIndex:newIdx];
        NSUInteger idx = [selectedObjects indexOfObject:document];
        if (idx != NSNotFound) {
            [newSelection addIndex:newIdx];
        }
        newIdx++;
    }
    [self.imageBrowserView setSelectionIndexes:newSelection byExtendingSelection:NO];
}

// Notification that somewhere in the app, the selection has changed
- (void)selectionChanged:(NSNotification *)notification
{
    id selectedItem = [self.dataSource selectedItem];
    if ([selectedItem isKindOfClass:[ADXDocument class]]) {
        self.selectedDocument = (ADXDocument *)selectedItem;

        [self requery];

        // Queue up a refresh of the document's versions
        ADXV2Service *service = [ADXAppDelegate instance].appModel.service;
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            [service refreshDocumentVersions:self.selectedDocument.id completion:^(BOOL success, NSError *error) {
                [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                    [self requery];
                }];
            }];
        });
    } else {
        // Reset to no objects
        self.arrayController = [[NSArrayController alloc] init];
        [self.imageBrowserView reloadData];
    }
}

#pragma mark - IKImageBrowserDataSource

- (NSUInteger)numberOfItemsInImageBrowser:(IKImageBrowserView *)aBrowser {
    return [self.arrayController.arrangedObjects count];
}

- (id)imageBrowser:(IKImageBrowserView *)aBrowser itemAtIndex:(NSUInteger)index {
    ADXDocument *document = [self.arrayController.arrangedObjects objectAtIndex:index];
    ADXDocumentBrowserViewItem *item = [ADXDocumentBrowserViewItem itemWithImageBrowser:self.imageBrowserView document:document];
    item.useVersionAsTitle = YES;
    return item;
}

#pragma mark - IKImageBrowserDelegate

- (void)imageBrowser:(IKImageBrowserView *)aBrowser cellWasDoubleClickedAtIndex:(NSUInteger)index {
    ADXDocument *document = [self.arrayController.arrangedObjects objectAtIndex:index];
    if (document.downloaded.boolValue) {
        [self.delegate openDocument:document];
    } else {
        [[ADXAppDelegate instance].appModel.service downloadContentForDocument:document.id
                                                                     versionID:document.version
                                                                 trackProgress:YES
                                                                    completion:^(BOOL success, NSError *error) {
                                                                        NSLog(@"Version download complete");
                                                                    }];
    }
}

@end
