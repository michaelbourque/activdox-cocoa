//
//  ADXAppDelegate.m
//  DesktopReader
//
//  Created by Steve Tibbett on 12-04-30.
//  Copyright (c) 2012 ActivDox. All rights reserved.
//

#import "ADXAppDelegate.h"
#import "ADXAppModel.h"
#import "ADXMainWindowController.h"
#import "ActivDoxCore.h"

@interface ADXAppDelegate ()
@property (strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@property (strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@end

@implementation ADXAppDelegate
@synthesize managedObjectContext = _managedObjectContext;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize appModel = _appModel;

- (void)applicationWillFinishLaunching:(NSNotification *)aNotification {
    self.appModel = [[ADXAppModel alloc] init];

    // Register ourselves as a URL handler for this URL
    [[NSAppleEventManager sharedAppleEventManager] setEventHandler:self
                                                       andSelector:@selector(getUrl:withReplyEvent:)
                                                     forEventClass:kInternetEventClass
                                                        andEventID:kAEGetURL];
}

- (void)applicationDidFinishLaunching:(NSNotification*)notification {
    NSLog(@"applicationDidFinishLaunching");

    [[NSUserNotificationCenter defaultUserNotificationCenter] setDelegate:self];
    
    self.remoteNotificationRelay = [[ADXRemoteNotificationRelay alloc] init];

    NSDictionary *launchOptions = notification.userInfo;

    if ([NSEvent modifierFlags] == NSShiftKeyMask) {
        [self promptForResetApplicationStateWithOptions:(NSDictionary *)launchOptions];
    } else {
        [self completeLaunchWithOptions:launchOptions];
    }
}

-(BOOL)applicationShouldTerminateAfterLastWindowClosed:(NSApplication *)theApplication {
    return NO;
}

// Convenience method to save some casting
+ (ADXAppDelegate *)instance {
    return (ADXAppDelegate *)[[NSApplication sharedApplication] delegate];
}

// Continue application launch after reset (if reset was requested)
- (void)completeLaunchWithOptions:(NSDictionary *)launchOptions {
    [self setupCoreDataStack];
    
    self.appModel.mainWindow = [[ADXMainWindowController alloc] initWithWindowNibName:@"MainWindow"];
    [self.appModel.mainWindow showWindow:self];
    
    [self setupRemoteNotifications];
    
    NSDictionary *notificationPayload = [launchOptions valueForKey:NSApplicationLaunchRemoteNotificationKey];
    if (notificationPayload) {
        [self.remoteNotificationRelay processNotificationReceivedWhileNotRunning:notificationPayload];
    }
}

#pragma mark - Reset application state

- (void)performReset {
    // Just set the flag so it gets reset when we open the Core Data store later
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"reset_storage"];
}

// Ask the user if they really want to reset the application state.
- (void)promptForResetApplicationStateWithOptions:(NSDictionary *)launchOptions {
    NSString *title = NSLocalizedString(@"Reset", nil);
    NSString *message = NSLocalizedString(@"Reset all application state to default values.  Proceed?", nil);
    NSAlert *alert = [[NSAlert alloc] init];
    alert.messageText = title;
    alert.informativeText = message;
    [alert addButtonWithTitle:NSLocalizedString(@"Reset", nil)];
    [alert addButtonWithTitle:NSLocalizedString(@"Cancel", nil)];
    NSInteger result = [alert runModal];
    if (result == NSAlertFirstButtonReturn) {
        [self performReset];
    }
    
    [self completeLaunchWithOptions:launchOptions];
}

#pragma mark - Core Data Setup

- (NSString *)applicationDocumentsDirectory 
{	
    BOOL isDir = NO;
    NSError *error = nil;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
    NSString *documentsPath = [paths objectAtIndex:0];
    documentsPath = [documentsPath stringByAppendingPathComponent:@"Private Documents"];
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:documentsPath isDirectory:&isDir] && isDir == NO) {
        [[NSFileManager defaultManager] createDirectoryAtPath:documentsPath withIntermediateDirectories:NO attributes:nil error:&error];
    }
    return documentsPath;
}

- (NSString *)applicationCachesDirectory 
{
    BOOL isDir = NO;
    NSError *error = nil;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *cachePath = [paths objectAtIndex:0];
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:cachePath isDirectory:&isDir] && isDir == NO) {
        [[NSFileManager defaultManager] createDirectoryAtPath:cachePath withIntermediateDirectories:NO attributes:nil error:&error];
    }
    
    return cachePath;
}

- (NSManagedObjectContext *)managedObjectContext 
{	
    if (!_managedObjectContext) {
        if (_persistentStoreCoordinator) {
            _managedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
            [_managedObjectContext performBlockAndWait:^{
                [_managedObjectContext setPersistentStoreCoordinator:_persistentStoreCoordinator];
            }];
        }
    }
    return _managedObjectContext;
}

- (NSManagedObjectModel *)managedObjectModel 
{	
    if (!_managedObjectModel) {
        NSURL *activDoxCoreModelBundleURL = [[NSBundle mainBundle] URLForResource:@"ActivDoxCoreModel" withExtension:@"bundle"];
        NSBundle *activDoxCoreModelBundle = [NSBundle bundleWithURL:activDoxCoreModelBundleURL];
        _managedObjectModel = [NSManagedObjectModel mergedModelFromBundles:[NSArray arrayWithObject:activDoxCoreModelBundle]];
    }
    return _managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator 
{	
    if (!_persistentStoreCoordinator) {
        NSURL *storeUrl = [NSURL fileURLWithPath: [[self applicationDocumentsDirectory] stringByAppendingPathComponent: @"ActivDox.sqlite"]];
        
        NSError *error = nil;
        NSMutableDictionary *options = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                        [NSNumber numberWithBool:YES], NSMigratePersistentStoresAutomaticallyOption,
                                        [NSNumber numberWithBool:YES], NSInferMappingModelAutomaticallyOption,
                                        nil]; 
        NSManagedObjectModel *model = self.managedObjectModel;
        _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] 
                                       initWithManagedObjectModel:model];
        if (![_persistentStoreCoordinator 
              addPersistentStoreWithType:NSSQLiteStoreType
              configuration:nil
              URL:storeUrl
              options:options
              error:&error]) {
            LogError(@"Unresolved error %@, %@", error, [error userInfo]);
            exit(-1);  // Fail
        }    
    }
    
    return _persistentStoreCoordinator;
}

- (void)setupCoreDataStack
{
    BOOL resetStorage = [[NSUserDefaults standardUserDefaults] boolForKey:@"reset_storage"];
    if (resetStorage) {
        [[ADXUserDefaults sharedDefaults] resetAll];
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"reset_storage"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        NSURL *storeUrl = [NSURL fileURLWithPath: [[self applicationDocumentsDirectory] stringByAppendingPathComponent: @"ActivDox.sqlite"]];
        NSError *error = nil;
        BOOL success = [[NSFileManager defaultManager] removeItemAtURL:storeUrl error:&error];
        if (!success) {
            LogNSError(@"Couldn't remove persistent store.", error);
        }
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    NSAssert(coordinator, @"Could not initialize persistent store coorinator");
    
    NSManagedObjectContext *moc = [self managedObjectContext];
    NSAssert(moc, @"Could not initialize managed object context");
    
    // TODO - Check and initialize the database if necessary
}

#pragma mark - URL Scheme Dispatching

- (void)getUrl:(NSAppleEventDescriptor *)event withReplyEvent:(NSAppleEventDescriptor *)replyEvent {
    NSString *url = [[event paramDescriptorForKeyword:keyDirectObject] stringValue];
    
    [ADXAppModel instance].pendingURLSchemeRequest = [[ADXURLSchemeRequest alloc] initWithURL:url];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kADXURLSchemeRequestNotification object:nil];
}
    
#pragma mark - Notifications

- (void)setupRemoteNotifications
{
    // Register app to receive remote (push) notifications
    [NSApp registerForRemoteNotificationTypes:NSRemoteNotificationTypeBadge | NSRemoteNotificationTypeAlert | NSRemoteNotificationTypeSound];
}

- (void)application:(NSApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    // Get current token from UserDefaults and check whether it's changed.  If not, it's a NO-OP.  However, if it has changed, we need to update UserDefaults and send the updated token to the ActivDox server (our provider)
    NSData *currentToken=[[ADXUserDefaults sharedDefaults] currentDeviceToken];

    BOOL notify = NO;
    if (currentToken == nil)
    {
        // Token has never been set - save new token as the current token in user defaults
        [[ADXUserDefaults sharedDefaults] setCurrentDeviceToken:deviceToken];
        notify = YES;
    }
    else {
        if ([deviceToken isEqualToData:currentToken] == NO)
        {
            // There is an existing token and it's changed.  Set last token in user defaults to the current token and set the current token to the new one.
            [[ADXUserDefaults sharedDefaults] setPreviousDeviceToken:currentToken];
            [[ADXUserDefaults sharedDefaults] setCurrentDeviceToken:deviceToken];
            notify = YES;
        }
    }
    
    if (notify) {
        [[NSNotificationCenter defaultCenter] postNotificationName:kADXServiceNotificationAPNSTokenReceived object:deviceToken];
    }
}

- (void)application:(NSApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    NSLog(@"Failed to register for remote notifications: %@", error);
    [self.remoteNotificationRelay failedToRegister:error];
}

- (void)application:(NSApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    NSLog(@"Received remote notification");
    [self.remoteNotificationRelay processNotificationReceivedWhileRunning:userInfo];
}

- (void)userNotificationCenter:(NSUserNotificationCenter *)center didActivateNotification:(NSUserNotification *)notification {
    NSLog(@"User activated notification");
    [self.remoteNotificationRelay userDidActivateNotification:notification.userInfo];
    [[NSUserNotificationCenter defaultUserNotificationCenter] removeDeliveredNotification:notification];
    
}

@end
