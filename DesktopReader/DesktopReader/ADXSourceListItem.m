//
//  SourceListItem.m
//  PXSourceList
//
//  Created by Alex Rozanski on 08/01/2010.
//  Copyright 2010 Alex Rozanski http://perspx.com
//
//  GC-enabled code revised by Stefan Vogt http://byteproject.net
//

#import "ADXSourceListItem.h"


@implementation ADXSourceListItem

#pragma mark -
#pragma mark Init/Dealloc/Finalize

- (id)init
{
	if(self=[super init])
	{
		self.badgeValue = -1;	//We don't want a badge value by default
	}
	
	return self;
}


+ (id)itemWithTitle:(NSString*)aTitle identifier:(NSString*)anIdentifier
{	
	ADXSourceListItem *item = [ADXSourceListItem itemWithTitle:aTitle identifier:anIdentifier icon:nil];
	
	return item;
}


+ (id)itemWithTitle:(NSString*)aTitle identifier:(NSString*)anIdentifier icon:(NSImage*)anIcon
{
	ADXSourceListItem *item = [[ADXSourceListItem alloc] init];
	
	[item setTitle:aTitle];
	[item setIdentifier:anIdentifier];
	[item setIcon:anIcon];
	
	return item;
}

- (void)finalize
{
	self.title = nil;
	self.identifier = nil;
	self.icon = nil;
	self.children = nil;
	
	[super finalize];
}

#pragma mark -
#pragma mark Custom Accessors

- (BOOL)hasBadge
{
	return self.badgeValue!=-1;
}

- (BOOL)hasChildren
{
	return [self.children count]>0;
}

- (BOOL)hasIcon
{
	return self.icon!=nil;
}

#pragma mark -
#pragma mark Custom Accessors

- (NSString *)description
{
	return [NSString stringWithFormat:@"<%@: %p | identifier = %@ | title = %@ >", [self class], self, self.identifier, self.title];
}
@end
