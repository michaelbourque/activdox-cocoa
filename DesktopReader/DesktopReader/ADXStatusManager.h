//
//  ADXStatusManager.h
//  DesktopReader
//
//  Created by Steve Tibbett on 2012-07-26.
//  Copyright (c) 2012 ActivDox. All rights reserved.
//

#import <Foundation/Foundation.h>

@class ADXStatusPanelView;

// This is the interface to status reporting that the application uses; the
// status manager knows about places to report status (like any status
// panel views that exist on any windows we have open).

@interface ADXStatusManager : NSObject

@property (strong, nonatomic) NSString *statusTitle;

- (void)addStatusPanelView:(ADXStatusPanelView *)statusPanelView;
- (void)removeStatusPanelView:(ADXStatusPanelView *)statusPanelView;

- (void)showStatusMessage:(NSString *)message;

@end
