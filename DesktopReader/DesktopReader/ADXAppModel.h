//
//  ADXAppDocument.h
//  DesktopReader
//
//  Created by Steve Tibbett on 12-04-25.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "ActivDoxCore.h"

@class ADXMainWindowController;
@class ADXStatusPanelView;
@class ADXStatusManager;

@interface ADXAppModel : NSObject

@property (readonly) NSArray *receivedDocuments;
@property (strong, nonatomic) ADXMainWindowController *mainWindow;
@property (strong, nonatomic) ADXV2Service *service;
@property (strong, nonatomic) ADXAccount *account;
@property (strong, nonatomic) ADXStatusManager *statusManager;
@property (strong, nonatomic) ADXURLSchemeRequest *pendingURLSchemeRequest;

+ (ADXAppModel *)instance;

+ (NSPredicate *)filterPredicateForSearchString:(NSString *)searchString;

@end
