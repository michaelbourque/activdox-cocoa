//
//  ADXAnnotationsWindowController.h
//  DesktopReader
//
//  Created by Steve Tibbett on 2012-09-13.
//  Copyright (c) 2012 ActivDox. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "ADXMainWindowController.h"
#import "ADXPDFViewController.h"
#import "ADXPDFView.h"

@interface ADXAnnotationsWindowController : NSWindowController <ADXPDFViewModalClickDelegate, NSTableViewDelegate>

@property (strong, nonatomic) ADXMainWindowController *mainWindow;
@property (strong, nonatomic) ADXPDFViewController *pdfViewController;

@end
