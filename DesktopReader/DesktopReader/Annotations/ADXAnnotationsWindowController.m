//
//  ADXAnnotationsWindowController.m
//  DesktopReader
//
//  Created by Steve Tibbett on 2012-09-13.
//  Copyright (c) 2012 ActivDox. All rights reserved.
//

#import "ADXAnnotationsWindowController.h"
#import "ADXAppModel.h"

@interface ADXAnnotationsWindowController ()
@property (weak) IBOutlet NSButton *highlightButton;
@property (weak) IBOutlet NSButton *addCommentButton;
@property (readonly) ADXPDFView *pdfView;
@property (nonatomic) BOOL suppressCommentSelection;
@property (weak) IBOutlet NSTableView *commentsTable;
@property (weak) IBOutlet NSButton *deleteCommentButton;
- (IBAction)deleteComment:(id)sender;
- (IBAction)highlightButtonPressed:(id)sender;
@end

@implementation ADXAnnotationsWindowController
@synthesize highlightButton = _highlightButton;
@synthesize addCommentButton = _addCommentButton;

- (id)initWithWindow:(NSWindow *)window
{
    self = [super initWithWindow:window];
    if (self) {
        // Initialization code here.
    }
    
    return self;
}

- (void)windowDidLoad
{
    [super windowDidLoad];
    
    // Implement this method to handle any initialization after your window controller's window has been loaded from its nib file.

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(selectionChangedNotification:)
                                                 name:PDFViewSelectionChangedNotification
                                               object:nil];

    [self.highlightButton setEnabled:self.pdfView.currentSelection != nil];

    // Bind the comments table
    self.suppressCommentSelection = YES;
    NSTableColumn *pageNumberColumn = [self.commentsTable tableColumnWithIdentifier:@"pageNumber"];
    [pageNumberColumn bind:@"value" toObject:self.pdfViewController.commentsArrayController withKeyPath:@"arrangedObjects.pageNumber" options:nil];
    NSTableColumn *commentTextColumn = [self.commentsTable tableColumnWithIdentifier:@"commentText"];
    [commentTextColumn bind:@"value" toObject:self.pdfViewController.commentsArrayController withKeyPath:@"arrangedObjects.text" options:nil];
    self.suppressCommentSelection = NO;

    [self.pdfViewController.commentsArrayController setSelectionIndexes:[NSIndexSet indexSet]];
    
    // Bind the Delete Comment button's availability to the selection of a row in the table
    [self.deleteCommentButton bind:@"enabled" toObject:self.pdfViewController.commentsArrayController withKeyPath:@"selectedObjects.@count" options:nil];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    self.pdfViewController.pdfView.modalClickDelegate = nil;
    self.pdfViewController = nil;
}

- (ADXPDFView *)pdfView {
    return self.pdfViewController.pdfView;
}

- (void)dumpAnnotations
{
    PDFDocument *document = self.pdfView.document;
    LogInfo(@"[Dumping annotations]");
    NSUInteger pageCount = self.pdfView.document.pageCount;
    for (NSUInteger pageIdx = 0; pageIdx < pageCount; pageIdx++) {
        PDFPage *page = [document pageAtIndex:pageIdx];
        for (PDFAnnotation *annotation in page.annotations) {
            LogInfo(@"On page %ld: %@", pageIdx, annotation);
        }
    }
}

#pragma mark - User Actions

- (IBAction)deleteComment:(id)sender
{
    NSArrayController *commentsArrayController = self.pdfViewController.commentsArrayController;
    NSAssert(commentsArrayController.selectedObjects.count == 1, @"should be a comment selected");

    if (commentsArrayController.selectedObjects.count == 1) {
        NSDictionary *selected = [commentsArrayController.selectedObjects objectAtIndex:0];
        NSLog(@"Item to delete: %@", selected);
    }
}

- (IBAction)highlightButtonPressed:(id)sender {
    PDFView *pdfView = self.pdfView;
    PDFDocument *document = pdfView.document;
    
    ADXUser *user = [ADXAppModel instance].account.user;
    
    PDFSelection *selection = pdfView.currentSelection;
    if (selection != nil) {
        int currentPageIndex = -1;
        NSMutableArray *rects = [NSMutableArray array];
        
        ADXDocumentNote *note = nil;
        for (PDFSelection *lineSelection in selection.selectionsByLine) {
            if (lineSelection.pages && lineSelection.pages.count == 1) {
                PDFPage *page = [lineSelection.pages objectAtIndex:0];
                int thisPageIndex = (int)[document indexForPage:page];
                if (currentPageIndex != -1 && currentPageIndex != thisPageIndex) {
                    // Flush the note, we're moving to a different page
                    note = [ADXDocumentNote insertNoteByUser:user forDocument:self.pdfViewController.document page:currentPageIndex text:@"" highlightRects:rects];
                    rects = [NSMutableArray array];
                } else {
                    currentPageIndex = thisPageIndex;
                }
                
                NSRect bounds = [lineSelection boundsForPage:page];
                bounds = NSInsetRect(bounds, -2, -2);
                [rects addObject:[NSValue valueWithRect:bounds]];
            }
        }

        if (rects.count > 0) {
            // Flush the notes for the last page
            note = [ADXDocumentNote insertNoteByUser:user forDocument:self.pdfViewController.document page:(currentPageIndex + 1) text:@"" highlightRects:rects];
        }
        
        if (note) {
            [note saveWithOptionalSync:YES completion:nil];
        }
        
        [pdfView setCurrentSelection:nil];
        [pdfView setNeedsDisplay:YES];
        [pdfView becomeFirstResponder];

        NSManagedObjectContext *contextToSave = self.pdfViewController.document.managedObjectContext;
        [contextToSave performBlock:^{
            NSError *error = nil;
            [contextToSave save:&error];
            if (error) {
                [[NSOperationQueue mainQueue] performBlock:^{
                    [NSApp presentError:error];
                }];
            }
        }];
        
        [self.pdfViewController populateAnnotations];
    }
    
    [self dumpAnnotations];
}

- (IBAction)addCommentButtonPressed:(id)sender {
    if (self.addCommentButton.state == NSOnState) {
        self.pdfViewController.pdfView.modalClickDelegate = self;
        [self.pdfView.window makeKeyWindow];
    } else {
        self.pdfViewController.pdfView.modalClickDelegate = nil;
        [self.pdfView.window makeKeyWindow];
    }
}

#pragma mark - PDF view interaction

- (void)selectionChangedNotification:(NSNotification *)notification
{
    if (notification.object == self.pdfView) {
        [self.highlightButton setEnabled:self.pdfView.currentSelection != nil];
    }
}

- (void)handleMouseDownInPDFView:(ADXPDFView *)view {
    NSPoint windowPoint = [view.window convertScreenToBase:[NSEvent mouseLocation]];
    NSPoint viewPoint = [view convertPoint:windowPoint fromView:nil];
    PDFPage *page = [view pageForPoint:viewPoint nearest:NO];
    NSPoint pointOnPage = [view convertPoint:viewPoint toPage:page];
    
    PDFAnnotationText *annotation = [[PDFAnnotationText alloc] initWithBounds:NSMakeRect(pointOnPage.x - (NOTE_SIZE/2), pointOnPage.y - (NOTE_SIZE/2), NOTE_SIZE, NOTE_SIZE)];
    annotation.iconType = kPDFTextAnnotationIconComment;
    annotation.color = [NSColor yellowColor];
    [page addAnnotation:annotation];
    
    view.modalClickDelegate = nil;
    self.addCommentButton.state = NSOffState;
    
    [view setNeedsDisplay:YES];
    [view becomeFirstResponder];
    [view.window makeKeyWindow];
}

#pragma mark - NSTableViewDelegate

- (void)tableViewSelectionDidChange:(NSNotification *)notification
{
    // Select this annotation in the document
    if (!self.suppressCommentSelection) {
        self.suppressCommentSelection = YES;
        NSArrayController *commentsArrayController = self.pdfViewController.commentsArrayController;
        
        if (commentsArrayController.selectedObjects.count > 0) {
            NSDictionary *selected = [commentsArrayController.selectedObjects objectAtIndex:0];
            [self.pdfViewController showAnnotation:[selected valueForKey:@"note"]];
        } else {
            [self.pdfViewController.pdfView hideAnnotationDetail];
        }
        self.suppressCommentSelection = NO;
    }
}

@end
