//
//  ADXMainWindowController.m
//  DesktopReader
//
//  Created by Steve Tibbett on 12-04-30.
//  Copyright (c) 2012 ActivDox. All rights reserved.
//

#import "ADXMainWindowController.h"
#import "ADXStatusPanelView.h"
#import "ADXSourceListController.h"
#import "ADXAppDelegate.h"
#import "ActivDoxCore.h"
#import "ADXAppModel.h"
#import "ADXDocumentBrowserViewController.h"
#import "ADXPDFViewController.h"
#import "ADXLoginPanelWindowController.h"
#import "PXSourceList.h"
#import "ADXSourceListItem.h"
#import "ADXStatusManager.h"
#import "ADXPDFView.h"
#import "ADXPublishWindowController.h"
#import "ADXFolderTableViewController.h"
#import "ADXImageBrowserView.h"
#import "ADXDocVersionsViewController.h"
#import "ADXAnnotationsWindowController.h"
#import "ADXShareWindowController.h"
#import "ADXMainWindowBrowserViewController.h"
#import "ADXPageBrowserViewController.h"

// Prefs Keys and Values
NSString *kADXArrangeBy = @"activdox.arrangeBy";
NSString *kADXArrangeByTitle = @"title";
NSString *kADXArrangeByAuthor = @"author";
NSString *kADXArrangeByDateUploaded = @"dateUploaded";
NSString *kADXMainWindowDocumentListViewMode = @"mainWindowViewMode";

#define TOOLBAR_TAG_TOGGLECHANGES 100
#define TOOLBAR_TAG_SEARCH 200

NSString *kADXWindowSelectionChangedNotification = @"reader.mainWindowSelectionChanged";

typedef enum {
    ADXMainWindowDocumentListViewModeIcons,
    ADXMainWindowDocumentListViewModeList,
} ADXMainWindowDocumentListViewMode;

static NSString *statusPaneToolbarIdentifier = @"activdox.statuspane";
static NSString *refreshToolbarIdentifier = @"activdox.refresh";
static NSString *panelSelectorToolbarIdentifier = @"activdox.panelselector";

@interface ADXMainWindowController ()
@property (strong, nonatomic) NSViewController *contentViewController;
@property (strong, nonatomic) ADXDocumentBrowserViewController *iconViewController;
@property (strong, nonatomic) ADXFolderTableViewController *detailViewController;
@property (strong, nonatomic) ADXPDFViewController *pdfViewController;
@property (readonly) ADXAppModel *appModel;
@property (weak) IBOutlet NSSplitView *leftSplitView;
@property (weak) IBOutlet PXSourceList *sourceList;
@property (strong) IBOutlet NSScrollView *sourcePanel;
@property (strong, nonatomic) ADXSourceListController *sourceListController;
@property (weak) IBOutlet NSView *mainViewParent;
@property (weak) IBOutlet NSOutlineView *outlineView;
@property (strong) IBOutlet NSTabView *tabView;
@property (weak) IBOutlet NSSegmentedControl *panelSelector;
@property (weak) IBOutlet NSTabViewItem *versionsTab;
@property (strong, nonatomic) ADXDocVersionsViewController *versionsViewController;
@property (readonly) PDFOutline *outline;
@property (strong, nonatomic) ADXAnnotationsWindowController *annotationsWindowController;
@property (strong, nonatomic) NSMutableArray *tabItems;
@property (strong, nonatomic) ADXPageBrowserViewController *pageBrowser;
@property (weak) IBOutlet NSView *pageBrowserPlaceholder;

@property (weak) IBOutlet NSTextField *infoPanelVersionLabel;
@property (weak) IBOutlet NSTextField *infoPanelDateLabel;
@property (weak) IBOutlet NSTextField *infoPanelVisibilityLabel;

- (IBAction)allOrChangedPagesChanged:(id)sender;
@property (nonatomic) int allOrChangedPagesSelectedIndex;

@property (strong, nonatomic) ADXShareWindowController *shareWindow;

- (IBAction)toggleHighlightChanges:(id)sender;
@end

@implementation ADXMainWindowController

@dynamic appModel;

- (void)awakeFromNib {
    [super awakeFromNib];

    [self.leftSplitView setPosition:100.0 ofDividerAtIndex:0];

    ADXSourceListController *sourceListController = [[ADXSourceListController alloc] initWithSourceList:self.sourceList];
    self.sourceListController = sourceListController;
    self.sourceList.dataSource = sourceListController;
    sourceListController.mainWindow = self;

    [[ADXAppModel instance].statusManager addStatusPanelView:self.statusPanelView];
    [ADXAppModel instance].statusManager.statusTitle = @"Not logged in";
    
    self.versionsViewController = [[ADXDocVersionsViewController alloc] initWithNibName:@"ADXDocVersionsView" bundle:nil];
    [self.versionsViewController loadView];
    [self.versionsTab.view addSubview:self.versionsViewController.view];
    self.versionsViewController.view.frame = [self.versionsTab.view bounds];
    self.versionsViewController.dataSource = self;
    self.versionsViewController.delegate = self;

    [self.statusAreaItem.view addSubview:self.statusPanelView];

    [self showDocumentBrowser];
}

- (void)windowDidLoad {
    [super windowDidLoad];
    [self.sourceList reloadData];

    [self useIdleWindowTitle];

    // Hide the inspector panel
    [self.tabView setHidden:YES];
    self.leftSplitView.autoresizingMask = NSViewWidthSizable | NSViewHeightSizable;
    [self.leftSplitView setNeedsLayout:YES];
    [self splitView:self.leftSplitView resizeSubviewsWithOldSize:NSMakeSize(0, 0)];

    // Show the login panel
    [self centerStatusPanel];
    [self showLoginPanel];


    // TODO: switch to addObserver or release the notification correctly
    [[NSNotificationCenter defaultCenter] addObserverForName:kADXURLSchemeRequestNotification object:nil queue:nil usingBlock:^(NSNotification *note) {
        dispatch_async(dispatch_get_main_queue(), ^{
            // TODO: check application state, make sure it's a good time to be doing this
            [self.window makeKeyAndOrderFront:self];
            [self processPendingURLRequest];
        });
    }];

    // TODO: switch to addObserver or release the notification correctly
    [[NSNotificationCenter defaultCenter] addObserverForName:kADXServiceDocumentDownloadedNotification object:nil queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification *note) {
        // Check to see if we've got a pending request for this document
        NSString *documentID = [note.userInfo valueForKey:kADXServiceNotificationPayload];
        if ([[[[ADXAppModel instance] pendingURLSchemeRequest] requiredDocumentID] isEqualToString:documentID]) {
            [self processPendingURLRequest];
        }
    }];
    
    self.searchItem.target = self;
    self.searchItem.action = @selector(searchFieldDidChange:);
    
    // This is sensitive to the original order of these views in the nib file.
    self.tabItems = [self.tabView.tabViewItems copy];
    
    [self updateInfoPanel];
}

- (NSString *)windowTitleForDocumentDisplayName:(NSString *)displayName {
    return @"ADox Reader";
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    [[ADXAppModel instance].statusManager removeStatusPanelView:self.statusPanelView];
}


#pragma mark - Convenience

- (void)useIdleWindowTitle {
    self.window.title = NSLocalizedString(@"ADoxReader", @"Main Window Title (Idle)");
}

- (void)useReadingWindowTitle {
    self.window.title = [NSString stringWithFormat:NSLocalizedString(@"ADoxReader - %@", @"Main Window Title (Document)"), self.selectedDocument.title];
}

- (ADXAppModel *)appModel {
    return [ADXAppModel instance];
}


#pragma mark - Panels

- (IBAction)togglePanel:(id)sender {
    NSView *panel = [self.leftSplitView.subviews objectAtIndex:2];
    [panel setHidden:!panel.isHidden];
    [self.leftSplitView setNeedsLayout:YES];
    [self splitView:self.leftSplitView resizeSubviewsWithOldSize:NSMakeSize(0, 0)];

    NSSegmentedControl *segmentedControl = (NSSegmentedControl *)sender;
    if (segmentedControl) {
        [segmentedControl setSelected:!panel.isHidden forSegment:0];
    }
}

/**
 * The status panel is in a container that allows it to move within the space that
 * is allocated for the container in the toolbar.  This method figures out the
 * offset from the start of the container required to center the status area
 * in the window.
 */
- (void)centerStatusPanel {
    // Prepare some values we'll need
    float windowWidth = self.window.frame.size.width;
    NSPoint containerOffset = [self.statusPanelView convertPoint:NSMakePoint(0, 0) toView:nil];
    
    // Locate the offset that the container is relative to, in window coords
    float newOrigin = (containerOffset.x - self.statusPanelView.frame.origin.x);
    
    // Determine the center coordinate
    newOrigin = (windowWidth/2) - newOrigin;
    
    // And center the status panel at that coordinate
    newOrigin -= self.statusPanelView.frame.size.width / 2;
    
    // Update its frame
    NSRect rect = self.statusPanelView.frame;
    rect.origin.x = newOrigin;
    self.statusPanelView.frame = rect;
}

- (void)windowDidResize:(NSNotification *)notification {
    [self centerStatusPanel];
}

- (void)updateInfoPanel {
    ADXDocument *document = self.selectedDocument;
    if (document != nil) {
        self.infoPanelDateLabel.stringValue = [document publishedDateDisplayLabel:YES];
        self.infoPanelVersionLabel.stringValue = [NSString stringWithFormat:@"%ld", (long)document.versionValue];
        self.infoPanelVisibilityLabel.stringValue = document.isPublicValue ? NSLocalizedString(@"Public", nil) : NSLocalizedString(@"Private", nil);
    }
}

#pragma mark - Split View Delegate

#define kMinOutlineViewSplit 180.0

- (CGFloat)splitView:(NSSplitView *)splitView constrainMinCoordinate:(CGFloat)proposedCoordinate ofSubviewAt:(long)index {
	return proposedCoordinate + kMinOutlineViewSplit;
}

- (CGFloat)splitView:(NSSplitView *)splitView constrainMaxCoordinate:(CGFloat)proposedCoordinate ofSubviewAt:(long)index {
	return proposedCoordinate - kMinOutlineViewSplit;
}

// Resize the views in the splitter, allowing only the content area to change size, keeping the side
// panels at their current sizes
- (void)splitView:(NSSplitView *)sender resizeSubviewsWithOldSize:(NSSize)oldSize {
    
    CGRect frames[3];
    
    frames[0] = [[sender.subviews objectAtIndex:0] frame];
    frames[1] = [[sender.subviews objectAtIndex:1] frame];
    frames[2] = [[sender.subviews objectAtIndex:2] frame];

    // They all get the new height
    frames[0].size.height = sender.bounds.size.height;
    frames[1].size.height = sender.bounds.size.height;
    frames[2].size.height = sender.bounds.size.height;

    // If the rightmost panel is collapsed
    if ([[sender.subviews objectAtIndex:2] isHidden]) {
        // Size the middle panel just using the left panel
        frames[1].size.width = sender.bounds.size.width - frames[0].size.width;
    } else {
        // Middle frame width changes, right frame moves
        frames[1].size.width = sender.bounds.size.width - (frames[0].size.width + frames[2].size.width);
        frames[2].origin.x = sender.bounds.size.width - frames[2].size.width;
    }
    
    [[sender.subviews objectAtIndex:0] setFrame:frames[0]];
    [[sender.subviews objectAtIndex:1] setFrame:frames[1]];
    [[sender.subviews objectAtIndex:2] setFrame:frames[2]];

    // Update thew window's minimum size
    NSSize minSize = NSMakeSize(0, 300);
    minSize.width = frames[0].size.width + kMinOutlineViewSplit + frames[2].size.width;
    [self.window setMinSize:minSize];
}

// We don't do collapse on drag
- (BOOL)splitView:(NSSplitView *)splitView canCollapseSubview:(NSView *)subview {
    return NO;
}

// We don't do collapse on drag
- (BOOL)splitView:(NSSplitView *)splitView shouldCollapseSubview:(NSView *)subview forDoubleClickOnDividerAtIndex:(NSInteger)dividerIndex {
    return NO;
}

- (BOOL)splitView:(NSSplitView *)splitView shouldHideDividerAtIndex:(NSInteger)dividerIndex {
    return YES;
}

#pragma mark - Main View

- (void)setContentViewController:(NSViewController *)viewController {
    if (self.contentViewController != nil) {
        if (self.contentViewController == viewController) {
            // Nothing to do - no change
            return;
        }
      
        // Remove it from the view
        [self.contentViewController.view removeFromSuperview];
    }
    
    // Add it
    [[self.leftSplitView.subviews objectAtIndex:1] addSubview:viewController.view];
    _contentViewController = viewController;

    // Update contentMode
    if ([self.contentViewController isKindOfClass:[ADXMainWindowBrowserViewController class]]) {
        self.contentMode = ADXMainWindowContentModeBrowser;
    } else
    if ([self.contentViewController isKindOfClass:[ADXPDFViewController class]]) {
        self.contentMode = ADXMainWindowContentModeDocumentViewer;
    } else
    if ([self.contentViewController isKindOfClass:[ADXDocumentBrowserViewController class]] ||
        [self.contentViewController isKindOfClass:[ADXFolderTableViewController class]]) {
        self.contentMode = ADXMainWindowContentModeDocumentList;
    } else {
        self.contentMode = ADXMainWindowContentModeIndeterminate;
    }

    viewController.view.frame = [[self.leftSplitView.subviews objectAtIndex:1] bounds];
    
    viewController.view.autoresizingMask = NSViewWidthSizable | NSViewHeightSizable;
}

- (NSArray *)mainListSortDescriptors {
    NSString *arrangeBy = [[NSUserDefaults standardUserDefaults] stringForKey:kADXArrangeBy];
    NSSortDescriptor *sortDescriptor = nil;
    if ([arrangeBy isEqualToString:kADXArrangeByAuthor]) {
        sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"author.loginName" ascending:YES];
    }
    if ([arrangeBy isEqualToString:kADXArrangeByDateUploaded]) {
        sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"uploadDate" ascending:NO];
    }
    if ([arrangeBy isEqualToString:kADXArrangeByTitle]) {
        sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"title" ascending:YES];
    }
    
    if (sortDescriptor) {
        return [NSArray arrayWithObject:sortDescriptor];
    } else {
        return nil;
    }
}

- (void)updateTabViewTabs {
    if (!self.tabItems) {
        return;
    }
    
    ADXMainWindowContentMode contentMode = self.contentMode;
    
    while (self.tabView.tabViewItems.count > 0) {
        [self.tabView removeTabViewItem:[self.tabView.tabViewItems objectAtIndex:0]];
    }
    
    if (contentMode == ADXMainWindowContentModeDocumentList) {
        [self.tabView addTabViewItem:[self.tabItems objectAtIndex:0]];
        [self.tabView addTabViewItem:[self.tabItems objectAtIndex:3]];
    } else
    if (contentMode == ADXMainWindowContentModeDocumentViewer) {
        [self.tabView addTabViewItem:[self.tabItems objectAtIndex:1]];
        [self.tabView addTabViewItem:[self.tabItems objectAtIndex:2]];
        [self.tabView addTabViewItem:[self.tabItems objectAtIndex:3]];
    } else
    if (contentMode == ADXMainWindowContentModeBrowser) {
        // No tabs
    } else {
        NSAssert(FALSE, @"Unrecognized content mode");
    }
}

- (void)preparePageBrowser
{
    if (self.pageBrowser == nil) {
        self.pageBrowser = [[ADXPageBrowserViewController alloc] initWithNibName:@"ADXPageBrowserViewController" bundle:nil];
        [self.pageBrowser loadView];
        [self.pageBrowserPlaceholder addSubview:self.pageBrowser.view];
        self.pageBrowser.view.frame = self.pageBrowserPlaceholder.bounds;
    }
    
    self.pageBrowser.PDFViewController = self.pdfViewController;
}

- (void)showDocumentViewer {
    [self.sourceList deselectAll:self];

    self.iconViewController = nil;
    self.detailViewController = nil;
    
    self.pdfViewController = [[ADXPDFViewController alloc] initWithNibName:@"PDFView" bundle:nil];
    [self.showChangesToolbarItem setEnabled:YES];
    [self.showChangesSegmentedControl setSelected:NO forSegment:0];
    
    // Attach the outline view to the PDF view controller
    [self.outlineView deselectAll:self];
    self.pdfViewController.outlineView = self.outlineView;

    // Instantiate the PDF view
    PDFView *pdfView = (PDFView *)self.pdfViewController.view;
    NSAssert(self.pdfViewController != nil, @"Loading view controller failed");
    NSAssert(pdfView != nil, @"Loading view failed");

    [self preparePageBrowser];
    
    [self.showChangesToolbarItem setEnabled:YES];

    [self setContentViewController:self.pdfViewController];

    [self resetSearch];

    [self useReadingWindowTitle];

    [self updateTabViewTabs];
}

- (void)showDocumentBrowser {
    NSLog(@"Showing the document browser");

    [self.annotationsWindowController close];
    self.annotationsWindowController = nil;

    self.pdfViewController = nil;
    
    [self useIdleWindowTitle];

    [self.showChangesToolbarItem setEnabled:NO];
    [self.showChangesSegmentedControl setSelected:NO forSegment:0];

    NSInteger mode = [[NSUserDefaults standardUserDefaults] integerForKey:kADXMainWindowDocumentListViewMode];
    if (mode == ADXMainWindowDocumentListViewModeIcons) {
        // Icon view
        if (self.iconViewController == nil) {
            self.iconViewController = [[ADXDocumentBrowserViewController alloc] initWithNibName:@"ADXDocumentBrowserViewController" bundle:nil];
            NSView *documentBrowserView = self.iconViewController.view;
            NSAssert(self.iconViewController != nil, @"Loading view controller failed");
            NSAssert(documentBrowserView != nil, @"Loading view failed");
            self.iconViewController.delegate = self;
        }
        
        [self setContentViewController:self.iconViewController];
        [self.iconViewController reloadData];
    }

    if (mode == ADXMainWindowDocumentListViewModeList) {
        // Detail view
        if (self.detailViewController == nil) {
            ADXFolderTableViewController *folderView = [[ADXFolderTableViewController alloc] initWithNibName:@"ADXFolderTableViewController"
                                                                                       bundle:nil];
            self.detailViewController = folderView;
            self.detailViewController.documentBrowserDelegate = self;
        }

        self.detailViewController.folder = self.selectedFolder;

        NSArray *sortDescriptors = [self mainListSortDescriptors];
        if (sortDescriptors) {
            [self.detailViewController.tableView setSortDescriptors:sortDescriptors];
        }
        
        [self setContentViewController:self.detailViewController];
    }

    [self resetSearch];
    
    self.selectedDocument = nil;
    [self updateInfoPanel];
    
    [self updateTabViewTabs];
}

- (void)showWebBrowser
{
    NSLog(@"Showing the web browser");
   
    [self.annotationsWindowController close];
    self.annotationsWindowController = nil;

    ADXMainWindowBrowserViewController *browserViewController = [[ADXMainWindowBrowserViewController alloc] initWithNibName:@"ADXMainWindowBrowserView" bundle:nil];
    [browserViewController navigateToAnalyticsPageForDocument:self.selectedDocument account:[ADXAppModel instance].account];
    
    [self setContentViewController:browserViewController];
    
    [self.showChangesToolbarItem setEnabled:NO];
    [self.showChangesSegmentedControl setSelected:NO forSegment:0];
    
    self.pdfViewController = nil;
    self.selectedDocument = nil;
    [self updateInfoPanel];
    [self updateTabViewTabs];
}


#pragma mark - Context

// Used through bindings to enable the analytics button
- (BOOL)isSelectedDocumentPublishedByMe
{
    return [self.selectedDocument.author isEqual:[ADXAppModel instance].account.user];
}

- (void)setSelectedDocument:(ADXDocument *)selectedDocument {
    _selectedDocument = selectedDocument;

    [[NSNotificationCenter defaultCenter] postNotificationName:kADXWindowSelectionChangedNotification object:self];
}

- (void)selectFolder:(ADXCollection *)folderToSelect {
    NSUInteger folderIndex = [self.sourceListController findIndexForFolder:folderToSelect];
    if (folderIndex != NSNotFound) {
        [self.sourceList selectRowIndexes:[NSIndexSet indexSetWithIndex:1] byExtendingSelection:NO];
    } else {
        [self.sourceList deselectAll:self];
    }

    [[NSNotificationCenter defaultCenter] postNotificationName:kADXWindowSelectionChangedNotification object:self];
}

- (id)selectedItem {
    if (self.selectedDocument) {
        return self.selectedDocument;
    }
    
    if (self.selectedFolder) {
        return self.selectedFolder;
    }
    
    return nil;
}

#pragma mark - ADXURLSchemeRequestDelegate

- (void)urlHandlerOpenDocumentWithID:(NSString *)documentID {
    NSManagedObjectContext *moc = [ADXAppDelegate instance].managedObjectContext;
    ADXDocument *document = [ADXDocument latestDocumentWithID:documentID accountID:[ADXAppModel instance].account.id context:moc];
    if (document) {
        if (document.downloadedValue) {
            [self openDocument:document];
        } else {
            // TODO: Download it, don't just tell the user they don't have it
            NSAlert *alert = [[NSAlert alloc] init];
            [alert addButtonWithTitle:NSLocalizedString(@"OK", nil)];
            [alert setMessageText:NSLocalizedString(@"Document Not Downloaded", nil)];
            [alert setInformativeText:NSLocalizedString(@"You have access to this document, but it's not currently downloaded.", nil)];
            [alert setAlertStyle:NSWarningAlertStyle];
            [alert beginSheetModalForWindow:self.window modalDelegate:nil didEndSelector:nil contextInfo:nil];
        }
    } else {
        NSAlert *alert = [[NSAlert alloc] init];
        [alert addButtonWithTitle:NSLocalizedString(@"OK", nil)];
        [alert setMessageText:NSLocalizedString(@"Document Not Found", nil)];
        [alert setInformativeText:NSLocalizedString(@"Your account does not have the requested document.", nil)];
        [alert setAlertStyle:NSWarningAlertStyle];
        [alert beginSheetModalForWindow:self.window modalDelegate:nil didEndSelector:nil contextInfo:nil];
    }
}

- (void)processPendingURLRequest {
    if ([ADXAppModel instance].account == nil) {
        // We're not ready for this yet
        return;
    }
    
    // Process any pending URL scheme requests
    ADXURLSchemeRequest *pendingRequest = [ADXAppModel instance].pendingURLSchemeRequest;
    if (pendingRequest != nil) {
        if (pendingRequest.requireThatAppHasNotLoggedIn) {
            // Discard and ignore
            [ADXAppModel instance].pendingURLSchemeRequest = nil;
            return;
        }
        
        // See if we have the required document
        if (pendingRequest.requiredDocumentID) {
            NSManagedObjectContext *moc = [ADXAppModel instance].service.managedObjectContext;
            [moc performBlock:^{
                ADXDocument *document = [ADXDocument latestDocumentWithID:pendingRequest.requiredDocumentID accountID:[ADXAppModel instance].account.id context:moc];
                [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                    if (document == nil) {
                        // We don't know anything about this document, so a refresh is in order. Make sure we
                        // haven't already come to this same conclusion for this exact document ID, lest we
                        // get stuck in an update loop.
                        if (pendingRequest.updateAttempted) {
                            // Refresh has already been triggered (but may not have completed).  Nothing to do right now.
                            return;
                        }
                        
                        // Trigger an update, so the document is downloaded.
                        ADXAccount *account = [ADXAppModel instance].account;
                        ADXCollection *received = [ADXCollection collectionMatchingTitle:@"received" forAccountID:account.id context:account.managedObjectContext];
                        self.selectedFolder = received;
                        [self showDocumentBrowser];
                        [self refresh];
                        
                        // Remember that we triggered an update in an attempt to get this document
                        pendingRequest.updateAttempted = YES;
                        return;
                    } else {
                        // Okay we have a document.  Open it.  If there's downloading to be done,
                        // this will be done by the document viewer.
                        [ADXAppModel instance].pendingURLSchemeRequest = nil;
                        self.selectedDocument = document;
                        [self openDocument:document];
                        return;
                    }
                    
                    pendingRequest.delegate = self;
                    [pendingRequest dispatch];
                }];
            }];
        }
    }
}

#pragma mark - Actions

- (void)sheetDidEnd:(NSWindow *)sheet returnCode:(NSInteger)code contextInfo:(void *)context {
    [self.sourceListController refresh];

    // Login complete; if there's a pending launch request then process it
    [self processPendingURLRequest];
}

- (void)showLoginPanel {
    self.loginPanel = [[ADXLoginPanelWindowController alloc] initWithWindowNibName:@"LoginPanel"];
    self.loginPanel.parentWindowController = self;
    [NSApp beginSheet:self.loginPanel.window
       modalForWindow:self.window
        modalDelegate:self
       didEndSelector:@selector(sheetDidEnd:returnCode:contextInfo:)
          contextInfo:nil];
}

- (void)refresh {
    ADXSourceListItem *item = (ADXSourceListItem *)[self.sourceList itemAtRow:self.sourceList.selectedRow];
    if (item == nil) {
        return;
    }
    
    [[ADXAppModel instance].statusManager showStatusMessage:@"Refreshing"];
    
    ADXV2Service *service = [ADXV2Service serviceForAccount:[ADXAppModel instance].account];

    // FIXME: (GMCK) Should be calling refreshCollection:collectionID (not containerType)
    [service refreshCollection:item.identifier completion:^(BOOL success, NSError *error) {
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            [[ADXAppModel instance].statusManager showStatusMessage:@"Refresh complete"];
            
            if (error) {
                LogNSError(@"received documents refresh failed with error", error);
                return;
            }
            
            [service refreshDocumentsInCollection:item.identifier downloadContent:YES trackProgress:YES];
            
            double delayInSeconds = 0.5;
            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
            dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                [self.iconViewController reloadData];
                [self.detailViewController reloadData];
            });
            
            [self processPendingURLRequest];
        }];
    }];
}

#pragma mark - Search

- (void)resetSearch {
    NSToolbarItem *searchItem = (NSToolbarItem *)[self searchItem];
    NSSearchField *searchField = (NSSearchField *)searchItem.view;
    [searchField setEnabled:YES];
    
    if (self.contentViewController != nil) {
        NSString *contentClassName = NSStringFromClass([self.contentViewController class]);
        searchField.recentsAutosaveName = contentClassName;
    }

    searchField.stringValue = @"";
}

- (NSToolbarItem *)searchItem {
    // Seriously?  We have to resort to this?
    for (NSView *view in self.toolBar.items) {
        if (view.tag == TOOLBAR_TAG_SEARCH) {
            return (NSToolbarItem *)view;
        }
    }
  
    return nil;
}


- (IBAction)searchFieldDidChange:(id)sender {
    NSString *searchString = [sender stringValue];
    if ([self.contentViewController conformsToProtocol:@protocol(ADXSearchable)]) {
        id<ADXSearchable> searchable = (id<ADXSearchable>)self.contentViewController;
        [searchable findString:searchString];
    }
}

#pragma mark - ADXDocumentBrowserDelegate

- (void)openDocument:(ADXDocument *)document {
    [self showDocumentViewer];

    LogInfo(@"Event: Opening document %@", document.title);
    [[ADXAppModel instance].service emitEventDidOpenDocument:document.id versionID:document.version completion:^(BOOL success, NSError *error) {
        if (!success) {
            LogInfo(@"Unexpected error recording that document was opened: %@", error);
        }
    }];
    
    self.selectedDocument = document;
    self.pdfViewController.document = document;
    
    [self.outlineView reloadData];
    
    // Expand All
    [self.outlineView expandItem:nil expandChildren:YES];
}

- (void)documentBrowserSelectionChanged:(ADXDocument *)newSelection {
    self.selectedDocument = newSelection;
    
    [self updateInfoPanel];
}

#pragma mark - Source List Delegate

// Check [group identifier] to see if this item should always be expanded
- (BOOL)sourceList:(PXSourceList *)aSourceList isGroupAlwaysExpanded:(id)group {
    return YES;
}

- (void)sourceListSelectionDidChange:(NSNotification *)notification {
    // Find the selected item
    PXSourceList *sourceList = (PXSourceList *)notification.object;
    if (sourceList != self.sourceList) {
        // This notification isn't for our source list
        return;
    }
    
    ADXSourceListItem *item = (ADXSourceListItem *)[sourceList itemAtRow:sourceList.selectedRow];
    if (item == nil) {
        self.selectedFolder = nil;
    } else {
        if ([item.identifier isEqualToString:kADXSourceListIdentifierMyActivDox]) {
            [self showWebBrowser];
        } else {
            // Find the associated folder
            [[ADXAppDelegate instance].managedObjectContext performBlockAndWait:^{
                
                NSManagedObjectContext *moc = [ADXAppDelegate instance].managedObjectContext;
                
                ADXAccount *account = [ADXAppModel instance].account;
                self.selectedFolder = [ADXCollection collectionWithID:item.identifier accountID:account.id context:moc];
            }];
            
            [self showDocumentBrowser];
        }
    }
    
}

#pragma mark - PDF Outline data source

- (PDFOutline *)outline {
    return self.pdfViewController.pdfView.document.outlineRoot;
}

- (NSInteger)outlineView:(NSOutlineView *)outlineView numberOfChildrenOfItem:(id)item {
    if (item == NULL)
    {
        if (self.outline)
            return [self.outline numberOfChildren];
        else
            return 0;
    }
    else
        return [(PDFOutline *)item numberOfChildren];
}

-(id)outlineView:(NSOutlineView *)outlineView child:(NSInteger)index ofItem:(id)item {
    if (item == NULL) {
        if (self.outline) {
            return [self.outline childAtIndex:index];
        } else {
            return NULL;
        }
    }
    else {
        return [(PDFOutline *)item childAtIndex: index];
    }
}

-(BOOL)outlineView:(NSOutlineView *)outlineView isItemExpandable:(id)item {
    if (item == NULL) {
        if (self.outline) {
            return ([self.outline numberOfChildren] > 0);
        } else {
            return NO;
        }
    }
    else {
        return ([(PDFOutline *)item numberOfChildren] > 0);
    }
}

- (id)outlineView:(NSOutlineView *)outlineView objectValueForTableColumn:(NSTableColumn *)tableColumn byItem:(id)item {
    return [(PDFOutline *)item label];
}

- (void)outlineViewSelectionDidChange:(NSNotification *)notification {
    [self.pdfViewController.pdfView goToDestination:[[self.outlineView itemAtRow:
                                                       [self.outlineView selectedRow]] destination]];
}

#pragma mark - Publish

- (void)publishDocumentsWithPaths:(NSArray *)paths {
    NSAssert(paths.count == 1, @"Only one document at a time is currently supported for publishing");

    ADXPublishWindowController *publishController = [[ADXPublishWindowController alloc] initWithWindowNibName:@"ADXPublishWindowController"];

    [publishController showWindow:self];
    
    [publishController publishDocumentsWithPaths:paths];
}

#pragma mark - User Actions

- (IBAction)signOut:(id)sender {
    [ADXAppDelegate instance].appModel.account = nil;
    
    self.selectedFolder = nil;
    [self showDocumentBrowser];
    [self showLoginPanel];
}

- (void)viewAsIcons:(id)sender {
    [[NSUserDefaults standardUserDefaults] setInteger:ADXMainWindowDocumentListViewModeIcons
                                               forKey:kADXMainWindowDocumentListViewMode];
    [self showDocumentBrowser];
}

- (void)viewAsList:(id)sender {
    [[NSUserDefaults standardUserDefaults] setInteger:ADXMainWindowDocumentListViewModeList
                                               forKey:kADXMainWindowDocumentListViewMode];
    [self showDocumentBrowser];
}

- (IBAction)arrangeByTitle:(id)sender {
    [[NSUserDefaults standardUserDefaults] setValue:kADXArrangeByTitle forKey:kADXArrangeBy];
    [self showDocumentBrowser];
}

- (IBAction)arrangeByAuthor:(id)sender {
    [[NSUserDefaults standardUserDefaults] setValue:kADXArrangeByAuthor forKey:kADXArrangeBy];
    [self showDocumentBrowser];
}

- (IBAction)arrangeByDateUploaded:(id)sender {
    [[NSUserDefaults standardUserDefaults] setValue:kADXArrangeByDateUploaded forKey:kADXArrangeBy];
    [self showDocumentBrowser];
}


- (BOOL)validateMenuItem:(NSMenuItem *)menuItem {
    if (menuItem.action == @selector(viewAsList:)) {
        ADXMainWindowDocumentListViewMode viewMode = (ADXMainWindowDocumentListViewMode)[[NSUserDefaults standardUserDefaults] integerForKey:kADXMainWindowDocumentListViewMode];
        menuItem.state = (viewMode == ADXMainWindowDocumentListViewModeList) ? NSOnState : NSOffState;
        return YES;
    }

    if (menuItem.action == @selector(viewAsIcons:)) {
        ADXMainWindowDocumentListViewMode viewMode = (ADXMainWindowDocumentListViewMode)[[NSUserDefaults standardUserDefaults] integerForKey:kADXMainWindowDocumentListViewMode];
        menuItem.state = (viewMode == ADXMainWindowDocumentListViewModeIcons) ? NSOnState : NSOffState;
        return YES;
    }

    if (menuItem.action == @selector(signOut:)) {
        return YES;
    }

    if (menuItem.action == @selector(showAnalytics:)) {
        return self.selectedDocument != nil;
    }

    if (menuItem.action == @selector(arrangeByAuthor:) ||
        menuItem.action == @selector(arrangeByDateUploaded:) ||
        menuItem.action == @selector(arrangeByTitle:)) {
        // TODO: determine if we're reading or browsing, return NO for reading
        return YES;
    }

    if (menuItem.action == @selector(showAnnotationsInspector:)) {
        if (self.contentMode == ADXMainWindowContentModeDocumentViewer) {
            return YES;
        }
        
        return NO;
    }
    
    if (menuItem.action == @selector(copySharingURL:)) {
        return self.selectedDocument != nil;
    }
    
    return [super validateMenuItem:menuItem];
}

- (IBAction)toggleHighlightChanges:(id)sender {
    if (self.pdfViewController != nil) {
        self.pdfViewController.highlightChanges = !self.pdfViewController.highlightChanges;
        [self.showChangesSegmentedControl setSelected:self.pdfViewController.highlightChanges forSegment:0];
    }
}

- (IBAction)showAnnotationsInspector:(id)sender {
    if (self.annotationsWindowController) {
        [self.annotationsWindowController close];
        self.annotationsWindowController = nil;
    } else {
        self.annotationsWindowController = [[ADXAnnotationsWindowController alloc] initWithWindowNibName:@"AnnotationsWindow"];
        self.annotationsWindowController.mainWindow = self;
        self.annotationsWindowController.pdfViewController = self.pdfViewController;
        [self.annotationsWindowController showWindow:self];
    }
}

- (IBAction)performFind:(id)sender {
    NSToolbarItem *searchItem = (NSToolbarItem *)[self searchItem];
    NSSearchField *searchField = (NSSearchField *)searchItem.view;
    [searchField becomeFirstResponder];
}

- (IBAction)shareDocumentWithNewRecipient:(id)sender {
    if (self.shareWindow != nil) {
        [self.shareWindow close];
        self.shareWindow = nil;
    }
    
    self.shareWindow = [[ADXShareWindowController alloc] initWithWindowNibName:@"ADXShareWindowController"];
    self.shareWindow.documentToShare = self.selectedDocument;
    [self.shareWindow showWindow:self];
}

- (void)copySharingURL:(id)sender {
    NSURL *shareableURL = [[ADXAppModel instance].service shareableURLForDocument:self.selectedDocument];
    NSPasteboard *pasteboard = [NSPasteboard generalPasteboard];
    [pasteboard declareTypes:[NSArray arrayWithObjects:NSURLPboardType, NSPasteboardTypeString, nil] owner:nil];
    [shareableURL writeToPasteboard:pasteboard];
    [pasteboard setString:shareableURL.absoluteString forType:NSPasteboardTypeString];
    NSLog(@"Sharing URL: %@", shareableURL);
}

- (IBAction)refreshClicked:(id)sender {
    [self refresh];
}

- (IBAction)allOrChangedPagesChanged:(id)sender
{
    self.pageBrowser.showOnlyChangedPages = self.allOrChangedPagesSelectedIndex == 1;
}

- (void)showAnalytics:(id)sender
{
    [self showWebBrowser];
}


@end
