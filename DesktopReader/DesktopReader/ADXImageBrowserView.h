//
//  ADXImageBrowserView.h
//  DesktopReader
//
//  Created by Steve Tibbett on 2012-07-24.
//  Copyright (c) 2012 ActivDox. All rights reserved.
//

#import <Quartz/Quartz.h>

// Subclass of IKImageBrowser view just for the purpose of supplying our
// own cell class.
@interface ADXImageBrowserView : IKImageBrowserView

@end
