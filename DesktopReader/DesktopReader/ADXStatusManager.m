//
//  ADXStatusManager.m
//  DesktopReader
//
//  Created by Steve Tibbett on 2012-07-26.
//  Copyright (c) 2012 ActivDox. All rights reserved.
//

#import "ADXStatusManager.h"
#import "ADXStatusPanelView.h"
#import "ActivDoxCore/Logging.h"

@interface ADXStatusManager()
@property (strong, nonatomic) NSMutableArray *statusViews;
@end

@implementation ADXStatusManager
@synthesize statusViews = _statusViews;
@synthesize statusTitle = _statusTitle;

- (id)init {
    self = [super init];
    if (self) {
        self.statusViews = [NSMutableArray array];
    }
    return self;
}

- (void)addStatusPanelView:(ADXStatusPanelView *)statusPanelView {
    [self.statusViews addObject:statusPanelView];
}

- (void)removeStatusPanelView:(ADXStatusPanelView *)statusPanelView {
    [self.statusViews removeObject:statusPanelView];
}

- (void)setStatusTitle:(NSString *)statusTitle {
    LogInfo(@"Status: %@", statusTitle);
    
    _statusTitle = statusTitle;
    for (ADXStatusPanelView *view in self.statusViews) {
        view.topStatusLine = statusTitle;
    }
}

- (void)showStatusMessage:(NSString *)message {
    for (ADXStatusPanelView *view in self.statusViews) {
        [view showMessage:message];
    }
}
@end
