//
//  ADXDocumentBrowserViewController.h
//  DesktopReader
//
//  Created by Steve Tibbett on 12-05-24.
//  Copyright (c) 2012 ActivDox. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <Quartz/Quartz.h>
#import "ADXSearchable.h"

@class ADXDocument;
@class ADXImageBrowserView;

@protocol ADXDocumentBrowserDelegate
- (void)openDocument:(ADXDocument *)document;
- (void)documentBrowserSelectionChanged:(ADXDocument *)newSelection;
@end

@interface ADXDocumentBrowserViewController : NSViewController <ADXSearchable>

@property (weak) IBOutlet ADXImageBrowserView *imageBrowserView;
@property (strong, nonatomic) id<ADXDocumentBrowserDelegate> delegate;

- (void)reloadData;

@end
