//
//  ADXPDFViewController.m
//  DesktopReader
//
//  Created by Steve Tibbett on 12-05-24.
//  Copyright (c) 2012 ActivDox. All rights reserved.
//

#import "ADXPDFViewController.h"
#import "ADXPDFView.h"
#import "ADXAppModel.h"
#import <Quartz/Quartz.h>
#import "ADXDownloadProgressOverlayView.h"
#import <objc/runtime.h>

NSString *kADXDocumentAnnotationsChangedNotification = @"kADXDocumentAnnotationsChangedNotification";

static int kAnnotationKey;

@interface ADXPDFViewController ()
@property (strong, nonatomic) PDFDocument *pdfDocument;
@property (strong, nonatomic) NSMutableIndexSet *reportedPages;
@property (strong, nonatomic) NSTimer *pageIdleTimer;
@property (weak) IBOutlet ADXDownloadProgressOverlayView *progressContainer;
@property (weak) IBOutlet NSProgressIndicator *activityIndicator;
@property (strong, nonatomic) NSMutableArray *searchResults;
@property (strong, nonatomic) NSString *currentSearchString;
@property int currentSearchResultIndex;
@end

@implementation ADXPDFViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Initialization code here.
        self.reportedPages = [NSMutableIndexSet indexSet];
    }
    
    return self;
}

- (void)loadView {
    [super loadView];
    
    // Assign the event reporting delegate
    self.pdfView.eventsDelegate = self;
    
    // Initialization code here.
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(pageChangedNotification:)
                                                 name:PDFViewPageChangedNotification
                                               object:nil];
}

- (void)dealloc {
    [self.pdfView stopPageTracking];

    if (_document != nil) {
        LogInfo(@"Event: Closing document %@", _document.title);
        [[ADXAppModel instance].service emitEventDidCloseDocument:_document.id versionID:_document.version completion:nil];
    }
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)downloadChanges
{
    __weak ADXPDFViewController* weakSelf = self;
    ADXV2Service *service = [ADXAppModel instance].service;
    ADXDocument *document = self.document;
    
    if (!document.changeContentDownloaded && document.changeCountValue) {
        [service downloadChangesForDocument:document.id versionID:document.version completion:^(BOOL success, NSError *error) {
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                if (error) {
                    LogNSError(@"couldn't load changes", error);
                } else {
                    weakSelf.pdfView.showChanges = YES;
                    ADXHighlightedContentHelper *highlightedContent = [ADXHighlightedContentHelper highlightedContentWithData:weakSelf.document.highlightedContent.data];
                    weakSelf.pdfView.changes = highlightedContent;
                }
                
                [weakSelf.progressContainer setHidden:YES];
            }];
        }];
    };
}

- (void)loadDocumentContentWithCompletion:(void (^)())completion {
    self.pdfDocument = [[PDFDocument alloc] initWithData:self.document.content.data];
    [self.pdfView setDocument:self.pdfDocument];
    [self.outlineView reloadData];
    if (completion) {
        completion();
    }
    
    [self refreshComments];
    [self refreshAnnotations];

    // Workaround for 10.7.4 bug
    SInt32 majorVersion, minorVersion, bugFixVersion;
    Gestalt(gestaltSystemVersionMajor, &majorVersion);
    Gestalt(gestaltSystemVersionMinor, &minorVersion);
    Gestalt(gestaltSystemVersionBugFix, &bugFixVersion);
    if (majorVersion == 10 && minorVersion == 7 && bugFixVersion == 4) {
        dispatch_async(dispatch_get_main_queue(), ^(void) {
            NSRect originalFrame = self.pdfView.frame;
            NSRect newFrame = originalFrame;
            newFrame.size.width -= 2;
            [self.pdfView setFrame:newFrame];
            dispatch_async(dispatch_get_main_queue(), ^(void) {
                [self.pdfView setFrame:originalFrame];
            });
        });
    }
}

- (void)setDocument:(ADXDocument *)document {
    [self willChangeValueForKey:@"document"];

    if (_document != nil) {
        LogInfo(@"Event: Closing document %@", _document.title);
        [[ADXAppModel instance].service emitEventDidCloseDocument:_document.id versionID:_document.version completion:nil];
    }

    _document = document;
    
    self.reportedPages = [NSMutableIndexSet indexSet];
    self.pdfView.autoresizingMask = NSViewWidthSizable | NSViewHeightSizable;
    self.currentSearchResultIndex = -1;
    self.currentSearchString = nil;

    if (!self.document.downloadedValue) {
        // Waiting for the document to download.
        // TODO: need a progress indicator
        // TODO: switch to addObserver or release the notification correctly
        [[NSNotificationCenter defaultCenter] addObserverForName:kADXServiceDocumentDownloadedNotification object:nil queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification *note) {
            
            [[NSNotificationCenter defaultCenter] removeObserver:self name:kADXServiceDocumentDownloadedNotification object:nil];
            
            NSString *documentID = [note.userInfo valueForKey:kADXServiceNotificationPayload];
            if ([documentID isEqualToString:self.document.id] && self.document.downloadedValue) {
                // Download complete, load the document.
                [self loadDocumentContentWithCompletion:^{
                    self.pdfView.document.delegate = self;
                }];
            }
        }];
    } else {
        [self loadDocumentContentWithCompletion:^{
            self.pdfView.document.delegate = self;
        }];
    }

    [self didChangeValueForKey:@"document"];
}

// Sync with the table of contents as the user navigates
- (void)pageChangedNotification:(NSNotification *)notification {
    NSUInteger newPageIndex;
    NSInteger numRows;
    int i;
    int newlySelectedRow;
    
    if ([[self.pdfView document] outlineRoot] == NULL)
        return;
    
    newPageIndex = [[self.pdfView document] indexForPage:
                    [self.pdfView currentPage]];
    
    // Walk outline view looking for best firstpage number match.
    newlySelectedRow = -1;
    numRows = [self.outlineView numberOfRows];
    for (i = 0; i < numRows; i++) {
        PDFOutline  *outlineItem;
        
        // Get the destination of the given row....
        outlineItem = (PDFOutline *)[self.outlineView itemAtRow:i];
        
        if ([[self.pdfView document] indexForPage:
             [[outlineItem destination] page]] == newPageIndex) {
            newlySelectedRow = i;
            break;
        }
        else if ([[self.pdfView document] indexForPage:
                  [[outlineItem destination] page]] > newPageIndex) {
            newlySelectedRow = i - 1;
            
            break;
        }
    }
    
    if (newlySelectedRow != -1) {
        // We're programmatically changing the selection in the NSOutlineView, so temporarily
        // disconnect the delegate so it doesn't route back to changing the scroll location.
        id delegate = self.outlineView.delegate;
        self.outlineView.delegate = nil;
        [self.outlineView selectRowIndexes:[NSIndexSet indexSetWithIndex:newlySelectedRow]
                      byExtendingSelection: NO];
        self.outlineView.delegate = delegate;

        [_outlineView scrollRowToVisible: newlySelectedRow];
    }
}

#pragma mark - Annotation

- (void)refreshAnnotations {
    NSLog(@"Refreshing annotations");

    // FIXME: (GMCK)
//    [[ADXAppModel instance].service refreshNotesOfDocument:self.document version:self.document.versionValue completion:^(BOOL success, NSError *error) {
//        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
//            [self populateAnnotations];
//        }];
//    }];
}

// Replace all the annotations in the document with the annotations in the database
- (void)populateAnnotations {
    // Remove all annotations
    int pageCount = (int)self.pdfDocument.pageCount;
    for (int pageIdx = 0; pageIdx < pageCount; pageIdx++) {
        PDFPage *page = [self.pdfDocument pageAtIndex:pageIdx];
        while (page.annotations.count > 0) {
            [page removeAnnotation:[page.annotations objectAtIndex:0]];
        }
    }
    
    for (ADXDocumentNote *note in self.document.metaDocument.notes) {
        if (!note.isAccessible) {
            continue;
        }

        NSArray *locationData = (NSArray *)note.locationData;
        
        for (NSDictionary *locationDataForOnePage in locationData) {
            NSUInteger pageNumber = [(NSNumber *)[locationDataForOnePage valueForKey:ADXJSONDocumentNoteAttributes.note.versions.location.page] unsignedIntegerValue];
            if (pageNumber <= pageCount) {
                PDFPage *page = [self.pdfDocument pageAtIndex:pageNumber-1];
                NSArray *highlightRects = [locationDataForOnePage valueForKey:ADXJSONDocumentNoteAttributes.note.versions.location.highlightArray];
                
                for (NSDictionary *highlightRect in highlightRects) {
                    NSRect rect = [ADXDocumentNote rectFromDictionary:highlightRect];
                    PDFAnnotation *annotation = nil;
                    if (rect.size.width == 0 && rect.size.height == 0) {
                        // It's a comment marker
                        PDFAnnotationText *textAnnotation = [[PDFAnnotationText alloc] initWithBounds:NSMakeRect(rect.origin.x - (NOTE_SIZE/2), rect.origin.y - (NOTE_SIZE/2), NOTE_SIZE, NOTE_SIZE)];

                        textAnnotation.iconType = kPDFTextAnnotationIconComment;
                        textAnnotation.color = [NSColor yellowColor];
                        textAnnotation.contents = note.text == nil ? @"" : note.text;
                       
                        [page addAnnotation:textAnnotation];
                        annotation = textAnnotation;
                    } else {
                        // It's a highlight rect
                        PDFAnnotationMarkup *markup = [[PDFAnnotationMarkup alloc] initWithBounds:rect];
                        markup.color = [NSColor yellowColor];
                        [page addAnnotation:markup];
                        annotation = markup;
                    }
                    
                    // Navigating from a PDFAnnotation back to the ADXDocumentNote that spawned it is not
                    // reliable, so associate the note with the annotation.
                    objc_setAssociatedObject(annotation, &kAnnotationKey, note, OBJC_ASSOCIATION_RETAIN);
                }
            } else {
                NSAssert(FALSE, @"Invalid page number on note");
            }
        }
    }
    
    [self.pdfView setNeedsDisplay:YES];
}

- (void)showAnnotation:(ADXDocumentNote *)note
{
    if (note == nil) {
        // Just ask the view to hide the annotation
        [self.pdfView hideAnnotationDetail];
        return;
    }
    
    // Scroll to this annotation
    NSArray *locationData = (NSArray *)note.locationData;
    if (locationData.count > 0) {
        NSDictionary *locationDataForOnePage = [locationData objectAtIndex:0];
        NSUInteger pageNumber = [(NSNumber *)[locationDataForOnePage valueForKey:ADXJSONDocumentNoteAttributes.note.versions.location.page] unsignedIntegerValue];
        
        if (pageNumber > 0 && pageNumber <= [self.pdfDocument pageCount]) {
            PDFPage *page = [self.pdfDocument pageAtIndex:pageNumber - 1];
            NSArray *highlightRects = [locationDataForOnePage valueForKey:ADXJSONDocumentNoteAttributes.note.versions.location.highlightArray];
            if (highlightRects.count > 0) {
                NSDictionary *highlightRect = [highlightRects objectAtIndex:0];
                NSRect rect = [ADXDocumentNote rectFromDictionary:highlightRect];
                rect = NSInsetRect(rect, -144, -144);
                [self.pdfView goToRect:rect onPage:page];
                
                // This is awkward - we need to find the PDFAnnotation associated with a particular note.
                // So we're searching the annotations for the associated object we established earlier.
                size_t pageCount = self.pdfView.document.pageCount;
                for (size_t pageIdx = 0; pageIdx < pageCount; pageIdx++) {
                    PDFPage *page = [self.pdfView.document pageAtIndex:pageIdx];
                    for (PDFAnnotation *annotation in page.annotations) {
                        ADXDocumentNote *thisNote = (ADXDocumentNote *)objc_getAssociatedObject(annotation, &kAnnotationKey);
                        if (thisNote == note) {
                            // Found it
                            [self.pdfView showDetailForAnnotation:annotation];
                        }
                    }
                }
            }
        }
    }
}


// TODO: Right now, there is no way to "edit" an annotation
// this simply creates and saves a new document note.
- (void)editedAnnotation:(PDFAnnotation *)annotation
{
    // Save the annotation
    ADXUser *user = [ADXAppModel instance].account.user;

    NSRect rect = annotation.bounds;
    rect = NSMakeRect(rect.origin.x + (rect.size.width/2), rect.origin.y + (rect.size.height/2), 0, 0);
    NSArray *rects = [NSMutableArray arrayWithObject:[NSValue valueWithRect:rect]];
    
    ADXDocumentNote *note = [ADXDocumentNote insertNoteByUser:user
                                                  forDocument:self.document
                                                         page:[self.pdfDocument indexForPage:annotation.page]+1
                                                         text:annotation.contents
                                               highlightRects:rects];

    [note saveWithOptionalSync:YES completion:^(BOOL success, NSError *error) {
        if (success) {
            [self refreshComments];
            [self populateAnnotations];

            [[NSNotificationCenter defaultCenter] postNotificationName:kADXDocumentAnnotationsChangedNotification object:self.document];
        } else {
            LogError(@"Unexpected error saving annotation: %@", error);
        }
    }];
    
    [self populateAnnotations];
}

- (void)deleteAnnotation:(PDFAnnotation *)annotation
{
    [self.pdfView hideAnnotationDetail];
    
    ADXDocumentNote *note = (ADXDocumentNote *)objc_getAssociatedObject(annotation, &kAnnotationKey);
    [note markDeleted];
    [self populateAnnotations];
}

- (void)refreshComments
{
    PDFDocument *document = self.pdfView.document;
    if (self.commentsArrayController == nil) {
        self.commentsArrayController = [[NSArrayController alloc] init];
    }
    
    for (ADXDocumentNote *note in self.document.metaDocument.notes) {
        if (!note.isAccessible) {
            continue;
        }
        
        NSArray *locationData = (NSArray *)note.locationData;
        
        for (NSDictionary *locationDataForOnePage in locationData) {
            NSUInteger pageNumber = [(NSNumber *)[locationDataForOnePage valueForKey:ADXJSONDocumentNoteAttributes.note.versions.location.page] unsignedIntegerValue];
            if (pageNumber <= document.pageCount) {
                NSArray *highlightRects = [locationDataForOnePage valueForKey:ADXJSONDocumentNoteAttributes.note.versions.location.highlightArray];
                
                for (NSDictionary *highlightRect in highlightRects) {
                    NSRect rect = [ADXDocumentNote rectFromDictionary:highlightRect];
                    if (rect.size.width == 0 && rect.size.height == 0) {
                        // Find this comment if it exists
                        BOOL found = NO;
                        for (NSMutableDictionary *commentEntry in self.commentsArrayController.arrangedObjects) {
                            if ([[commentEntry valueForKey:@"noteID"] isEqual:note.objectID]) {
                                // Update the existing note
                                // TODO: notes never change right now
                                found = YES;
                            }
                        }
                        if (!found) {
                            // Add a new note
                            NSMutableDictionary *commentEntry = [NSMutableDictionary dictionary];
                            [commentEntry setValue:[NSNumber numberWithUnsignedLong:pageNumber] forKey:@"pageNumber"];
                            [commentEntry setValue:note.text forKey:@"text"];
                            [commentEntry setValue:note.objectID forKey:@"noteID"];
                            [commentEntry setValue:[NSValue valueWithPoint:NSMakePoint(rect.origin.x, rect.origin.y)] forKey:@"location"];
                            [commentEntry setValue:note forKey:@"note"];
                            [self.commentsArrayController addObject:commentEntry];
                        }
                    }
                }
            }
        }
    }
}

- (void)pdfView:(ADXPDFView *)pdfView didSelectAnnotation:(PDFAnnotation *)annotation
{
    if (annotation == nil) {
        [self.commentsArrayController setSelectionIndexes:[NSIndexSet indexSet]];
    } else {
        // Update the selected item in the comments array controller.
        ADXDocumentNote *note = (ADXDocumentNote *)objc_getAssociatedObject(annotation, &kAnnotationKey);

        if (note != nil) {
            for (NSDictionary *commentEntry in self.commentsArrayController.arrangedObjects) {
                if ([[commentEntry valueForKey:@"note"] isEqual:note]) {
                    [self.commentsArrayController setSelectedObjects:[NSArray arrayWithObject:commentEntry]];
                    break;
                }
            }
            
        }
    }
}

#pragma mark - Analytics

- (void)eventPageViewed:(size_t)pageNum {
    // TODO: not currently used
    NSAssert(FALSE, @"page.viewed is not currently implemented", nil);
}

- (void)eventPageOpened:(size_t)pageNum {
    // Only report the page if it hasn't already been reported
    if (![self.reportedPages containsIndex:pageNum]) {
        [self.reportedPages addIndex:pageNum];
        LogInfo(@"Reporting opened page %d", (int)pageNum);
        [[ADXAppModel instance].service emitEventDidOpenPageOfDocument:self.document.id versionID:self.document.version page:pageNum completion:nil];
    }
}

- (void)eventPageClosed:(size_t)pageNum {
    LogInfo(@"Reporting closed page %d", (int)pageNum);
    [[ADXAppModel instance].service emitEventDidClosePageOfDocument:self.document.id versionID:self.document.version page:pageNum completion:nil];
}

#pragma mark - Change Highlighting

-(void)setHighlightChanges:(BOOL)highlightChanges {
    self.pdfView.showChanges = highlightChanges;
    _highlightChanges = highlightChanges;

    if (!self.document.changeContentDownloadedValue) {
        self.pdfView.changes = nil;
        [self downloadChanges];
    } else {
        ADXHighlightedContentHelper *highlightedContent = [ADXHighlightedContentHelper highlightedContentWithData:self.document.highlightedContent.data];
        self.pdfView.changes = highlightedContent;
    }

    if (highlightChanges && !self.document.changeContentDownloadedValue) {
        // Show the progress panel
        [self.progressContainer setHidden:NO];
    } else {
        [self.progressContainer setHidden:YES];
    }
}

#pragma mark - Search 

- (void)findString:(NSString *)searchString {
    // If the search is for the same string, then it's the user hitting Enter again in the search field.
    // Select the next occurrence.
    if ([searchString isEqualToString:self.currentSearchString]) {
        if (self.searchResults != nil && self.searchResults.count > 0) {
            // Navigate to the next search result
            self.currentSearchResultIndex = (self.currentSearchResultIndex + 1) % self.searchResults.count;
            // Set the selection without animation
            [self.pdfView setCurrentSelection:[self.searchResults objectAtIndex:self.currentSearchResultIndex]];
            // Scroll the view so it's visible
            [self.pdfView scrollSelectionToVisible:nil];
            // And animate it
            [self.pdfView setCurrentSelection:[self.searchResults objectAtIndex:self.currentSearchResultIndex] animate:YES];
        }
        
        return;
    }

    // Start with no selection, assuming we're not going to find it.
    [self.pdfView clearSelection];
    
    if (self.pdfView.document.isFinding) {
        [self.pdfView.document cancelFindString];
    }

    self.searchResults = [NSMutableArray array];
    self.currentSearchResultIndex = -1;
    self.currentSearchString = searchString;
    
    [self.pdfView.document beginFindString:searchString withOptions:NSCaseInsensitiveSearch];
}

- (void)didMatchString:(PDFSelection *)instance {
    [self.searchResults addObject:[instance copy]];
    
    if (self.currentSearchResultIndex == -1) {
        // We haven't selected any of the search results yet, and we've found one, so select it.
        self.currentSearchResultIndex = 0;
        
        // Set the selection without animation
        [self.pdfView setCurrentSelection:[self.searchResults objectAtIndex:self.currentSearchResultIndex]];
        
        // Scroll the view so it's visible
        [self.pdfView scrollSelectionToVisible:nil];
        
        // And animate it
        [self.pdfView setCurrentSelection:[self.searchResults objectAtIndex:self.currentSearchResultIndex] animate:YES];
    }
}

@end
