//
//  ADXPDFView.h
//  DesktopReader
//
//  Created by Steve Tibbett on 12-06-29.
//  Copyright (c) 2012 ActivDox. All rights reserved.
//

#import <Quartz/Quartz.h>

@class ADXDocument;
@class ADXPDFView;
@class ADXHighlightedContentHelper;

@protocol ADXPDFViewEventsDelegate
- (void)eventPageViewed:(size_t)pageNum;
- (void)eventPageOpened:(size_t)pageNum;
- (void)eventPageClosed:(size_t)pageNum;
- (void)editedAnnotation:(PDFAnnotation *)annotation;
- (void)deleteAnnotation:(PDFAnnotation *)annotation;
- (void)pdfView:(ADXPDFView *)pdfView didSelectAnnotation:(PDFAnnotation *)annotation;
@end

@protocol ADXPDFViewModalClickDelegate
- (void)handleMouseDownInPDFView:(ADXPDFView *)view;
@end

@interface ADXPDFView : PDFView

@property (assign, nonatomic) id<ADXPDFViewEventsDelegate> eventsDelegate;
@property (assign, nonatomic) id<ADXPDFViewModalClickDelegate> modalClickDelegate;

@property (nonatomic) BOOL showChanges;
@property (strong, nonatomic) ADXHighlightedContentHelper *changes;

// The view is about to disappear.  Emit the appropriate analytic events to indicate
// the document and current page are closing.
- (void)stopPageTracking;

- (void)showDetailForAnnotation:(PDFAnnotation *)annotation;
- (void)hideAnnotationDetail;

@end
