//
//  ADXDownloadProgressOverlayView.h
//  DesktopReader
//
//  Created by Steve Tibbett on 2012-08-30.
//  Copyright (c) 2012 ActivDox. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface ADXDownloadProgressOverlayView : NSView

@end
