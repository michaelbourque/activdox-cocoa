//
//  ADXPDFView.m
//  DesktopReader
//
//  Created by Steve Tibbett on 12-06-29.
//  Copyright (c) 2012 ActivDox. All rights reserved.
//

#import "ADXPDFView.h"
#import "ADXAppModel.h"
#import <ApplicationServices/ApplicationServices.h>
#import <Quartz/Quartz.h>
#import "ADXChangeTipView.h"
#import <objc/runtime.h>

const float kADXPDFViewPageEventTimeInterval = 3.0;
static int kADXContextMenuAnnotationKey;

#define TOOLTIP_EDGE_MARGIN 4

// This ADXPDFPageTracker runs a timer while it's alive.  If the timer goes off, then
// the page is considered "viewed", and an event is generated.  ADXPDFView updates the
// list of tracked pages in drawPage:
@interface ADXPDFPageTracker : NSObject
@property (strong, nonatomic) NSTimer *timer;
@property (nonatomic) size_t pageNumber;
@property (assign, nonatomic) id<ADXPDFViewEventsDelegate> eventsDelegate;
@property (nonatomic) BOOL lingered;
- (id)initWithPageNumber:(size_t)pageNumber delegate:(id<ADXPDFViewEventsDelegate>)delegate;
@end

@implementation ADXPDFPageTracker

- (id)initWithPageNumber:(size_t)pageNumber delegate:(id<ADXPDFViewEventsDelegate>)delegate {
    self = [super init];
    if (self) {
        self.pageNumber = pageNumber;
        self.eventsDelegate = delegate;
        self.timer = [NSTimer scheduledTimerWithTimeInterval:kADXPDFViewPageEventTimeInterval
                                                      target:self
                                                    selector:@selector(handleTimer:)
                                                    userInfo:nil
                                                     repeats:NO];
    }
    
    return self;
}

- (void)dealloc {
    // Stop the timer
    [self.timer invalidate];
}

- (void)handleTimer:(NSTimer *)timer {
    self.lingered = YES;
    [self.eventsDelegate eventPageOpened:self.pageNumber];
}

- (void)leavePage {
    if (self.lingered) {
        [self.eventsDelegate eventPageClosed:self.pageNumber];
    }
    [self.timer invalidate];
}


@end


@interface ADXPDFView()
@property (strong, nonatomic) NSMutableArray *trackedPages;
@property (strong, nonatomic) NSView *annotationPopup;
@property (strong, nonatomic) PDFAnnotation *annotationPopupAssociatedAnnotation;
@property (strong, nonatomic) PDFPage *annotationPopupOnPage;
@property (strong, nonatomic) NSMutableArray *changeViews;
@property (strong, nonatomic) NSMutableArray *changeTrackingAreas;
@property (strong, nonatomic) ADXChangeTipView *changePopup;
@property (nonatomic) NSRect boundsWhenPopupAppeared;
@end

@implementation ADXPDFView

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        self.trackedPages = [NSMutableArray array];
        self.changeViews = [NSMutableArray array];
        
        // Initialization code here.
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(pageChangedNotification:)
                                                     name:PDFViewPageChangedNotification
                                                   object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(boundsDidChangeNotification:)
                                                     name:NSViewBoundsDidChangeNotification
                                                   object:nil];
     
        [self setPostsBoundsChangedNotifications:YES];
    }
    return self;
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)mouseMoved:(NSEvent *)theEvent {
    if (self.modalClickDelegate) {
        [[NSCursor pointingHandCursor] set];
    } else {
        [super mouseMoved:theEvent];
    }
}

- (void)mouseDown:(NSEvent *)theEvent {
    if (self.modalClickDelegate) {
        [self.modalClickDelegate handleMouseDownInPDFView:self];
        return;
    }

    // Manual hit testing for annotation (this is a workaround, suggested by an Apple support engineer, for
    // the fact that PDFViewAnnotationWillHitNotification doesn't currently work).
    NSPoint windowPoint = [self.window convertScreenToBase:[NSEvent mouseLocation]];
    NSPoint viewPoint = [self convertPoint:windowPoint fromView:nil];
    PDFAnnotation *annotation = [self annotationAtPoint:viewPoint];

    if (annotation != nil) {
        // PDFAnnotation.type corresponds to the PDF annotation Subtype
        if ([annotation.type isEqualToString:@"Text"]) {
            // New annotation
            [self toggleDetailForAnnotation:annotation];
            [self.eventsDelegate pdfView:self didSelectAnnotation:annotation];
            return;
        }
    }
    
    // Click somewhere other than on an annotation, when there's an annotation popup, closes it
    if (self.annotationPopup != nil) {
        [self closeAnnotationPopup];
        [self.eventsDelegate pdfView:self didSelectAnnotation:nil];
        return;
    }
    
    [super mouseDown:theEvent];
}

- (void)stopPageTracking {
    for (ADXPDFPageTracker *trackedPage in self.trackedPages) {
        [trackedPage leavePage];
    }
}

- (void)drawPage:(PDFPage *)page {

    if (self.eventsDelegate) {
        // Build an NSIndexSet of visible page numbers
        NSMutableIndexSet *visiblePageNumbers = [NSMutableIndexSet indexSet];
        for (PDFPage *page in self.visiblePages) {
            [visiblePageNumbers addIndex:CGPDFPageGetPageNumber(page.pageRef)];
        }
        
        NSMutableArray *pagesToRemove = [NSMutableArray array];
        
        // Remove any pages that aren't currently visible
        for (ADXPDFPageTracker *tracker in self.trackedPages) {
            if (![visiblePageNumbers containsIndex:tracker.pageNumber]) {
                [pagesToRemove addObject:tracker];
            }
        }
        
        for (ADXPDFPageTracker *tracker in pagesToRemove) {
            [tracker leavePage];
            [self.trackedPages removeObject:tracker];
        }
        
        // Add the visible pages if they're not already being tracked
        NSMutableIndexSet *trackedPageNumbers = [NSMutableIndexSet indexSet];
        for (ADXPDFPageTracker *tracker in self.trackedPages) {
            [trackedPageNumbers addIndex:tracker.pageNumber];
        }
        
        for (PDFPage *page in self.visiblePages) {
            size_t pageNum = CGPDFPageGetPageNumber(page.pageRef);
            if (![trackedPageNumbers containsIndex:pageNum]) {
                [self.trackedPages addObject:[[ADXPDFPageTracker alloc] initWithPageNumber:pageNum delegate:self.eventsDelegate]];
            }
        }
    }
    
    [super drawPage:page];
}

- (void)drawPagePost:(PDFPage *)page {
    [super drawPagePost:page];

    // In SinglePage mode, if the annotation isn't on the current then it should hide
    if (self.displayMode == kPDFDisplaySinglePage || self.displayMode == kPDFDisplayTwoUp) {
        BOOL isVisible = NO;
        
        if (self.displayMode == kPDFDisplayTwoUp) {
            NSUInteger curPage = [self.document indexForPage:self.currentPage];
            if (curPage < (self.document.pageCount - 1) && self.annotationPopupOnPage == [self.document pageAtIndex:curPage + 1]) {
                isVisible = YES;
            }
        }

        if (self.currentPage == self.annotationPopupOnPage) {
            isVisible = YES;
        }
        
        if (!isVisible) {
            [self closeAnnotationPopup];
        }
    }

    if (self.annotationPopup && page == self.annotationPopupOnPage) {
        NSRect rect = self.annotationPopupAssociatedAnnotation.bounds;
        rect.origin.x -= 72;
        rect.size.width += 144;
        rect.origin.y -= 72 - (self.annotationPopupAssociatedAnnotation.bounds.size.height / 2);
        rect.size.height = 72;
        NSRect popupRect = [self convertRect:rect fromPage:self.annotationPopupOnPage];
        self.annotationPopup.frame = popupRect;
    }

    if (self.showChanges && self.changes) {
        [[NSGraphicsContext currentContext] saveGraphicsState];

        NSUInteger pageIndex = [page.document indexForPage:page];

        [self.changes drawChangesOnPage:pageIndex context:[NSGraphicsContext currentContext].graphicsPort withBlock:^(CGRect rect) {
            rect = [self convertRect:[self convertRect:rect fromPage:page]
                                     toView:[self documentView]];
            return rect;
        }];
        
        [[NSGraphicsContext currentContext] restoreGraphicsState];
    }
}

- (void)closeAnnotationPopup {
    NSTextField *textField = (NSTextField *)self.annotationPopup;
    if (textField != nil) {
        if (![self.annotationPopupAssociatedAnnotation.contents isEqualToString:textField.stringValue]) {
            self.annotationPopupAssociatedAnnotation.contents = textField.stringValue;
            [self.eventsDelegate editedAnnotation:self.annotationPopupAssociatedAnnotation];
        }

        self.annotationPopupAssociatedAnnotation.contents = textField.stringValue;
        [self.annotationPopup removeFromSuperview];
        self.annotationPopup = nil;
    }
}

- (void)toggleDetailForAnnotation:(PDFAnnotation *)annotation {
    if (self.annotationPopup) {
        [self closeAnnotationPopup];
        if (annotation == self.annotationPopupAssociatedAnnotation) {
            // Clicked on the one we just closed, so don't reopen it
            return;
        }
    }

    // Create and add the note editor.
    NSTextField *textField = [[NSTextField alloc] initWithFrame:NSZeroRect];
    textField.backgroundColor = [NSColor yellowColor];
    textField.alphaValue = 0.8;
    textField.stringValue = annotation.contents;
    [self addSubview:textField];
    
    // Trigger a layout so it gets positioned correctly.
    self.annotationPopup = textField;
    self.annotationPopupAssociatedAnnotation = annotation;
    self.annotationPopupOnPage = annotation.page;
    [self layoutDocumentView];
}

- (void)showDetailForAnnotation:(PDFAnnotation *)annotation {
    [self closeAnnotationPopup];
    [self toggleDetailForAnnotation:annotation];
}

- (void)hideAnnotationDetail
{
    [self closeAnnotationPopup];
}

#pragma mark - Menu Handling

- (PDFAnnotation *)annotationAtPoint:(CGPoint)viewPoint
{
    PDFPage *page = [self pageForPoint:viewPoint nearest:NO];
    
    if (page != nil) {
        NSPoint pointOnPage = [self convertPoint:viewPoint toPage:page];
        
        for (PDFAnnotation *annotation in page.annotations) {
            NSRect		annotationBounds;
            
            // Hit test annotation.
            annotationBounds = [annotation bounds];
            if (NSPointInRect(pointOnPage, annotationBounds))
            {
                return annotation;
            }
        }
    }
    
    return nil;
}

- (void)removeHighlight:(id)sender
{
    PDFAnnotation *annotationToRemove = (PDFAnnotation *)objc_getAssociatedObject(sender, &kADXContextMenuAnnotationKey);
    [self.eventsDelegate deleteAnnotation:annotationToRemove];
}

- (NSMenu *)menuForEvent:(NSEvent *)event
{
    NSMenu *menu = [super menuForEvent:event];

    if (menu != nil) {
        // Determine if there's an annotation under the mouse
        NSPoint windowPoint = [self.window convertScreenToBase:[NSEvent mouseLocation]];
        NSPoint viewPoint = [self convertPoint:windowPoint fromView:nil];
        PDFAnnotation *annotation = [self annotationAtPoint:viewPoint];
        NSMenuItem *menuItem = nil;
        if ([annotation.type isEqualToString:@"Text"]) {
            menuItem = [[NSMenuItem alloc] initWithTitle:@"Remove Note" action:@selector(removeHighlight:) keyEquivalent:@""];
        }
        if ([annotation.type isEqualToString:@"MarkUp"]) {
            menuItem = [[NSMenuItem alloc] initWithTitle:@"Remove Highlight" action:@selector(removeHighlight:) keyEquivalent:@""];
        }

        // Use the annotation's index as the menu item's tag, so we can associate them later
        if (menuItem != nil) {
            [menu addItem:[NSMenuItem separatorItem]];
            objc_setAssociatedObject(menuItem, &kADXContextMenuAnnotationKey, annotation, OBJC_ASSOCIATION_RETAIN);
            [menu addItem:menuItem];
        }
    }
    
    return menu;
}

#pragma mark - Change Rendering

- (void)setShowChanges:(BOOL)showChanges
{
    _showChanges = showChanges;
    [self setNeedsDisplay:YES];
}

- (void)reflectScrolledClipView:(NSClipView *)aClipView
{
    [super reflectScrolledClipView:aClipView];
    [self updateTrackingAreas];
}

- (void)pageChangedNotification:(NSNotification *)notification
{
    [self updateTrackingAreas];
}

- (void)boundsDidChangeNotification:(NSNotification *)notification
{
    if (self.changePopup && !NSEqualRects(self.documentView.enclosingScrollView.contentView.bounds, self.boundsWhenPopupAppeared)) {
        [self.changePopup removeFromSuperview];
    }
    
    [self updateTrackingAreas];
}

- (void)mouseEntered:(NSEvent *)theEvent
{
    [super mouseEntered:theEvent];
    
    NSDictionary *userData = (__bridge NSDictionary *)theEvent.userData;
    if (userData != nil) {
        NSString *type = (NSString *)[userData valueForKey:@"type"];
        id data = (NSString *)[userData valueForKey:@"data"];
        
        NSPoint windowPoint = [self.window convertScreenToBase:[NSEvent mouseLocation]];
        NSPoint viewPoint = [self convertPoint:windowPoint fromView:nil];
        
        [self showPopupForChangeType:type data:data atLocation:viewPoint];
    }
    
}

- (void)mouseExited:(NSEvent *)theEvent
{
    [super mouseExited:theEvent];

    [self.changePopup removeFromSuperview];
}

- (void)showPopupForChangeType:(NSString *)type data:(id)data atLocation:(NSPoint)point
{
    [self.changePopup removeFromSuperview];

    NSString *changeText = [self.changes descriptionForChange:data];
    if (changeText == nil) {
        // Nothing to show
        return;
    }
    
    NSViewController *viewController = [[NSViewController alloc] initWithNibName:@"ADXChangeTipView" bundle:nil];
    self.changePopup = (ADXChangeTipView *)viewController.view;
    
    self.changePopup.textView.string = changeText;
    
    self.changePopup.frame = NSMakeRect(point.x, point.y - self.changePopup.frame.size.height, self.changePopup.frame.size.width, self.changePopup.frame.size.height);

    [self.changePopup sizeToFit];

    // Try to avoid having the popup stick out of the view
    NSRect frame = self.changePopup.frame;
    if (frame.origin.x + frame.size.width > self.bounds.origin.x + self.bounds.size.width) {
        frame.origin.x = self.bounds.origin.x + self.bounds.size.width - (frame.size.width + TOOLTIP_EDGE_MARGIN);
    }
    if (frame.origin.y < TOOLTIP_EDGE_MARGIN) {
        frame.origin.y = TOOLTIP_EDGE_MARGIN;
    }
    self.changePopup.frame = frame;

    self.boundsWhenPopupAppeared = self.documentView.enclosingScrollView.contentView.bounds;

    [self addSubview:self.changePopup];
}


- (void)updateTrackingAreas
{
    [super updateTrackingAreas];

    for (NSTrackingArea *area in self.changeTrackingAreas) {
        [self removeTrackingArea:area];
    }
    
    self.changeTrackingAreas = [NSMutableArray array];
    
    for (PDFPage *visiblePage in self.visiblePages) {
        NSUInteger pageIndex = [self.document indexForPage:visiblePage];
        [self.changes enumerateChangesOnPage:pageIndex withBlock:^(CGRect pageRect, NSString *type, id data) {
            CGRect rect = [self convertRect:pageRect fromPage:visiblePage];

            NSMutableDictionary *userInfo = [NSMutableDictionary dictionary];
            [userInfo setValue:type forKey:@"type"];
            [userInfo setValue:data forKey:@"data"];
            
            NSTrackingArea *trackingArea = [[NSTrackingArea alloc] initWithRect:rect
                                                                        options:NSTrackingActiveInActiveApp | NSTrackingMouseEnteredAndExited
                                                                          owner:self
                                                                       userInfo:userInfo];

            [self.changeTrackingAreas addObject:trackingArea];
            

            [self addTrackingArea:trackingArea];
        }];
    }

}
@end
