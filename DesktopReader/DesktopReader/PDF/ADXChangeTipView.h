//
//  ADXChangeTipView.h
//  DesktopReader
//
//  Created by Steve Tibbett on 2012-10-16.
//  Copyright (c) 2012 ActivDox. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface ADXChangeTipView : NSView

@property (unsafe_unretained) IBOutlet NSTextView *textView;

- (void)sizeToFit;

@end
