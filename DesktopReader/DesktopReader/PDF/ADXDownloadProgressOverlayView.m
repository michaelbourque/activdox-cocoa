//
//  ADXDownloadProgressOverlayView.m
//  DesktopReader
//
//  Created by Steve Tibbett on 2012-08-30.
//  Copyright (c) 2012 ActivDox. All rights reserved.
//

#import "ADXDownloadProgressOverlayView.h"
#import <Cocoa/Cocoa.h>

@implementation ADXDownloadProgressOverlayView

- (id)initWithFrame:(NSRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code here.
        self.layer.borderColor = CGColorGetConstantColor(kCGColorWhite);
        self.layer.borderWidth = 1.0;
        self.layer.opaque = NO;
        self.layer.cornerRadius = 5.0;
    }
    
    return self;
}

- (BOOL)wantsLayer {
    return YES;
}

- (void)drawRect:(NSRect)dirtyRect
{
    NSBezierPath* roundRectPath = [NSBezierPath bezierPathWithRoundedRect:[self bounds] xRadius:5 yRadius:5];
    [roundRectPath addClip];
    [[NSColor colorWithCalibratedWhite:0.15 alpha:0.75] set];
    [NSBezierPath fillRect:self.bounds];
}

@end
