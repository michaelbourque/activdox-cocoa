//
//  ADXChangeTipView.m
//  DesktopReader
//
//  Created by Steve Tibbett on 2012-10-16.
//  Copyright (c) 2012 ActivDox. All rights reserved.
//

#import "ADXChangeTipView.h"

#define TOOLTIP_MAX_HEIGHT 400
#define TIPMARGIN 4

// Extra space to leave inside the tooltip
#define HORZMARGIN 24


@implementation ADXChangeTipView

- (id)initWithFrame:(NSRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
    }
    
    return self;
}

- (void)drawRect:(NSRect)dirtyRect
{
    [[NSColor yellowColor] set];
    NSRectFill(dirtyRect);
}

- (void)sizeToFit
{
    NSString *string = self.textView.string;
    if (string.length == 0) {
        return;
    }
    
    NSRange glyphRange = [self.textView.layoutManager glyphRangeForTextContainer:self.textView.textContainer];
    NSRect rect = [self.textView.layoutManager boundingRectForGlyphRange:glyphRange inTextContainer:self.textView.textContainer];
    
    NSRect frame = self.frame;
    frame.origin.y += (frame.size.height - rect.size.height);
    frame.size.width = rect.size.width + TIPMARGIN + HORZMARGIN + [NSScroller scrollerWidthForControlSize:NSRegularControlSize scrollerStyle:NSScrollerStyleOverlay];
    frame.size.height = rect.size.height + TIPMARGIN;
    
    // Arbitrary maximum height for the tooltip
    frame.size.height = MIN(frame.size.height, TOOLTIP_MAX_HEIGHT);
    self.frame = frame;
}
@end
