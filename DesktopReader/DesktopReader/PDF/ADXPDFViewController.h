//
//  ADXPDFViewController.h
//  DesktopReader
//
//  Created by Steve Tibbett on 12-05-24.
//  Copyright (c) 2012 ActivDox. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "ActivDoxCore.h"
#import "ADXPDFView.h"
#import "ADXSearchable.h"

// Point size of the note icon used for annotations
#define NOTE_SIZE 16

extern NSString *kADXDocumentAnnotationsChangedNotification;


@interface ADXPDFViewController : NSViewController <ADXPDFViewEventsDelegate, ADXSearchable>

@property (strong, nonatomic) ADXDocument *document;
@property (strong) IBOutlet ADXPDFView *pdfView;
@property (nonatomic) BOOL highlightChanges;
@property (weak, nonatomic) NSOutlineView *outlineView;
@property (strong) IBOutlet NSView *searchView;

- (void)populateAnnotations;
- (void)showAnnotation:(ADXDocumentNote *)annotation;

@property (strong) IBOutlet NSArrayController *commentsArrayController;

@end
