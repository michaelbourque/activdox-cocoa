//
//  ADXSourceListController.h
//  DesktopReader
//
//  Created by Steve Tibbett on 12-04-30.
//  Copyright (c) 2012 ActivDox. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PXSourceListDataSource.h"
#import "PXSourceListDelegate.h"
#import "PXSourceList.h"

@class ADXCollection;

extern NSString *kADXSourceListIdentifierMyActivDox;

@class ADXMainWindowController;

@interface ADXSourceListController : NSObject <PXSourceListDataSource, PXSourceListDelegate>

@property (nonatomic, strong) NSMutableArray *sourceListItems;
@property (assign, nonatomic) ADXMainWindowController *mainWindow;
@property (weak, nonatomic) PXSourceList *sourceList;

- (id)initWithSourceList:(PXSourceList *)sourceList;
- (NSUInteger)findIndexForFolder:(ADXCollection *)folder;
- (void)refresh;

@end
