//
//  ADXImageBrowserDocumentCell.h
//  DesktopReader
//
//  Created by Steve Tibbett on 2012-07-24.
//  Copyright (c) 2012 ActivDox. All rights reserved.
//

#import <Quartz/Quartz.h>

@class ADXDocumentBrowserViewItem;

// Custom cell subclass for rendering ADXDocument icons
@interface ADXImageBrowserDocumentCell : IKImageBrowserCell

- (id)initWithItem:(ADXDocumentBrowserViewItem *)viewItem;

@end
