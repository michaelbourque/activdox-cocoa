//
//  ADXPublishWindowController.m
//  DesktopReader
//
//  Created by Steve Tibbett on 1/08/12.
//  Copyright (c) 2012 ActivDox. All rights reserved.
//

#import "ADXPublishWindowController.h"
#import "ActivDoxCore.h"
#import "ADXAppModel.h"

@interface ADXPublishWindowController ()
@property (strong, nonatomic) ADXPublishWindowController *selfReference;
@end

@implementation ADXPublishWindowController
@synthesize uploadLabel;
@synthesize progressIndicator;

- (id)initWithWindow:(NSWindow *)window
{
    self = [super initWithWindow:window];
    if (self) {
        // We'll clear this when the publish is done
        self.selfReference = self;
    }
    
    return self;
}

- (void)windowDidLoad
{
    [super windowDidLoad];
    
    // Implement this method to handle any initialization after your window controller's window has been loaded from its nib file.
    
    [self.window setOrderedIndex:0];
}

- (void)publishDocumentsWithPaths:(NSArray *)paths {
    NSString *path = [paths objectAtIndex:0];
    self.uploadLabel.stringValue = [NSString stringWithFormat:NSLocalizedString(@"Uploading %@", nil), [path lastPathComponent]];

    ADXV2Service *service = [ADXV2Service serviceForAccount:[ADXAppModel instance].account];
    NSURL *url = [NSURL fileURLWithPath:path];
    [service uploadDocumentAtURL:url
                      completion:^(BOOL success, NSError *error) {
                          [self.progressIndicator startAnimation:nil];
                          self.selfReference = nil;
                      }];
}

@end
