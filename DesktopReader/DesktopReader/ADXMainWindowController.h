//
//  ADXMainWindowController.h
//  DesktopReader
//
//  Created by Steve Tibbett on 12-04-30.
//  Copyright (c) 2012 ActivDox. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <Quartz/Quartz.h>
#import <ActivDoxCore.h>
#import "PXSourceList.h"
#import "PXSourceListDataSource.h"
#import "ADXDocumentBrowserViewController.h"
#import "ADXSourceListController.h"

@class ADXCollection;
@class ADXStatusPanelView;
@class ADXLoginPanelWindowController;
@class ADXSourceListDataSource;

extern NSString *kADXArrangeBy;
extern NSString *kADXArrangeByTitle;
extern NSString *kADXArrangeByAuthor;
extern NSString *kADXArrangeByDateUploaded;
extern NSString *kADXMainWindowDocumentListViewMode;

extern NSString *kADXWindowSelectionChangedNotification;
extern NSString *kADXURLSchemeRequestNotification;

typedef enum {
    ADXMainWindowContentModeIndeterminate,
    ADXMainWindowContentModeDocumentList,
    ADXMainWindowContentModeDocumentViewer,
    ADXMainWindowContentModeBrowser,
} ADXMainWindowContentMode;

@protocol ADXSelectionDataSource
- (id)selectedItem;
@end

@interface ADXMainWindowController : NSWindowController <NSToolbarDelegate, NSSplitViewDelegate, ADXDocumentBrowserDelegate, PXSourceListDelegate, NSOutlineViewDataSource,
                                                         NSOutlineViewDelegate, ADXSelectionDataSource, ADXURLSchemeRequestDelegate, NSWindowDelegate>

@property (strong, nonatomic) ADXLoginPanelWindowController *loginPanel;
@property (weak) IBOutlet ADXStatusPanelView *statusPanelView;
@property (weak) IBOutlet NSView *statusPanelContainer;
@property (weak) IBOutlet NSToolbar *toolBar;
@property (weak) IBOutlet NSToolbarItem *statusAreaItem;
@property (weak) IBOutlet NSToolbarItem *showChangesToolbarItem;
@property (weak) IBOutlet NSSegmentedControl *showChangesSegmentedControl;
@property (nonatomic) ADXMainWindowContentMode contentMode;

// Switch to the indicated folder, updating the source list and main view as required.
- (void)selectFolder:(ADXCollection *)folderToSelect;

// The currently selected folder for this window.  Folder selection is managed
// at the window level in anticipation that someday we may have multiple windows.
@property (strong, nonatomic) ADXCollection *selectedFolder;

// The currently selected document for this window.
@property (strong, nonatomic) ADXDocument *selectedDocument;

- (void)publishDocumentsWithPaths:(NSArray *)paths;

- (NSArray *)mainListSortDescriptors;

@end
