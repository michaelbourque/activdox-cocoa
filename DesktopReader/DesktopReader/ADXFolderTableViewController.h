//
//  ADXFolderTableViewController.h
//  DesktopReader
//
//  Created by Steve Tibbett on 14/08/12.
//  Copyright (c) 2012 ActivDox. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <ActivDoxCore.h>
#import <ADXSearchable.h>

#import "ADXDocumentBrowserViewController.h"

@interface ADXFolderTableViewController : NSViewController <NSTableViewDelegate, ADXSearchable>

@property (weak) IBOutlet NSTableView *tableView;
@property (strong, nonatomic) ADXCollection *folder;
@property (assign, nonatomic) id<ADXDocumentBrowserDelegate> documentBrowserDelegate;

- (void)reloadData;

@end
