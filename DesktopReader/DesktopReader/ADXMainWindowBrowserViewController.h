//
//  ADXMainWindowBrowserViewController.h
//  DesktopReader
//
//  Created by Steve Tibbett on 2012-10-26.
//  Copyright (c) 2012 ActivDox. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "ActivDoxCore.h"

@interface ADXMainWindowBrowserViewController : NSViewController

- (void)navigateToAnalyticsPageForDocument:(ADXDocument *)document account:(ADXAccount *)account;

@end
