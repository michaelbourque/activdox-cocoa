//
//  ADXImageBrowserView.m
//  DesktopReader
//
//  Created by Steve Tibbett on 2012-07-24.
//  Copyright (c) 2012 ActivDox. All rights reserved.
//

#import "ADXImageBrowserView.h"
#import "ADXImageBrowserDocumentCell.h"

@implementation ADXImageBrowserView

- (id)initWithFrame:(NSRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code here.
    }
    
    return self;
}

// TODO: this is an ugly workaround for what seems to be an IKImageBrowserView bug that causes it to
// drop images while resizing.  
- (void)setFrame:(NSRect)frameRect {
    NSIndexSet *selections = self.selectionIndexes;
    [super setFrame:frameRect];
    [self reloadData];
    [self setSelectionIndexes:selections byExtendingSelection:NO];
}

// Return our custom cell type
- (IKImageBrowserCell *)newCellForRepresentedItem:(id)anItem {
    ADXImageBrowserDocumentCell *cell = [[ADXImageBrowserDocumentCell alloc] initWithItem:anItem];
    return cell;
}

@end
