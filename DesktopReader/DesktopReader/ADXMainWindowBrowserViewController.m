//
//  ADXMainWindowBrowserViewController.m
//  DesktopReader
//
//  Created by Steve Tibbett on 2012-10-26.
//  Copyright (c) 2012 ActivDox. All rights reserved.
//

#import "ADXMainWindowBrowserViewController.h"
#import "ADXAppDelegate.h"
#import "ADXAppModel.h"

#import <WebKit/WebKit.h>

@interface ADXMainWindowBrowserViewController ()

@end

@implementation ADXMainWindowBrowserViewController


// This is overridden to change the cache policy, so we always get fresh data
- (NSURLRequest *)webView:(WebView *)sender
                 resource:(id)identifier
          willSendRequest:(NSURLRequest *)request
         redirectResponse:(NSURLResponse *)redirectResponse
           fromDataSource:(WebDataSource *)dataSource
{
    request = [NSURLRequest requestWithURL:[request URL] cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:[request timeoutInterval]];
    return request;
}

- (void)navigateToAnalyticsPageForDocument:(ADXDocument *)document account:(ADXAccount *)account
{
    WebView *webView = (WebView *)self.view;
    webView.resourceLoadDelegate = self;

    NSURL *rootURL = [NSURL URLWithString:account.serverURL];
    
    // TODO: this URL string should be configurable or come from somewhere other than being baked
    // into the app here.
    NSURL *pageURL = [NSURL URLWithString:@"savvydox/docAnalytics.html" relativeToURL:rootURL];

    NSHTTPCookieStorage *cookieJar = [NSHTTPCookieStorage sharedHTTPCookieStorage];

    NSDictionary *cookieProperties = [NSMutableDictionary dictionary];
    [cookieProperties setValue:pageURL.host forKey:NSHTTPCookieDomain];
    [cookieProperties setValue:@"/" forKey:NSHTTPCookiePath];
    [cookieProperties setValue:@"adox_accessToken" forKey:NSHTTPCookieName];
    [cookieProperties setValue:account.accessToken forKey:NSHTTPCookieValue];
    [cookieJar setCookie:[NSHTTPCookie cookieWithProperties:cookieProperties]];

    cookieProperties = [NSMutableDictionary dictionary];
    [cookieProperties setValue:pageURL.host forKey:NSHTTPCookieDomain];
    [cookieProperties setValue:@"/" forKey:NSHTTPCookiePath];
    [cookieProperties setValue:@"adox_docId" forKey:NSHTTPCookieName];
    [cookieProperties setValue:document.id forKey:NSHTTPCookieValue];
    [cookieJar setCookie:[NSHTTPCookie cookieWithProperties:cookieProperties]];

    [webView setMainFrameURL:pageURL.absoluteString];
}

@end
