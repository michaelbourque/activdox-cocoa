//
//  ADXAppModel.m
//  DesktopReader
//
//  Created by Steve Tibbett on 12-04-25.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ADXAppModel.h"
#import "ADXAppDelegate.h"
#import "ADXMainWindowController.h"
#import "ADXStatusManager.h"

@implementation ADXAppModel

@dynamic receivedDocuments;
@synthesize mainWindow = _mainWindow;
@synthesize account = _account;
@synthesize statusManager = _statusManager;

- (id)init
{
    self = [super init];
    if (self) {
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(processUserActivatedNotification:)
                                                     name:kADXUserActivatedNotificationEvent
                                                   object:nil];
    }
    return self;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (NSArray *)receivedDocuments {
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:[ADXDocument entityName]];
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"1 == 1"];
    NSError *error = nil;
    NSArray *result = [[ADXAppDelegate instance].managedObjectContext executeFetchRequest:fetchRequest
                                             error:&error];
    
    if (result == nil) {
        NSLog(@"Unexpected failed fetch request: %@", error);
    }
    
    return result;
}

#pragma mark - Server Interaction

+ (ADXAppModel *)instance {
    return [ADXAppDelegate instance].appModel;
}

#pragma mark - Status manager accessor

- (ADXStatusManager *)statusManager {
    if (_statusManager == nil) {
        _statusManager = [[ADXStatusManager alloc] init];
    }
    return _statusManager;
}

#pragma mark - Search Helper

+ (NSPredicate *)filterPredicateForSearchString:(NSString *)searchString {
    if ([searchString length] > 0) {
        return [NSPredicate predicateWithFormat:@"self.title CONTAINS[c] %@ || self.author.loginName CONTAINS[c] %@"
                                  argumentArray:[NSArray arrayWithObjects:searchString, searchString, nil]];
        
    } else {
        return [NSPredicate predicateWithValue:YES];
    }
}

#pragma mark - Notifications

- (void)processUserActivatedNotification:(NSNotification *)notification
{
    NSString *eventID = [notification.object valueForKey:kADXNotificationPayloadEventIDKey];
    LogInfo(@"Requesting associated document ID for event ID %@", eventID);
    
    ADXAppModel *appModel = [ADXAppModel instance];
    [appModel.service requestDocumentIDForEventWithID:eventID
                                           completion:^(id response, BOOL success, NSError *error) {
                                               if (success) {
                                                   NSDictionary *responseDict = (NSDictionary *)response;
                                                   NSString *documentID = responseDict[ADXDocumentAttributes.id];
                                                   NSString *versionID = responseDict[ADXDocumentAttributes.version];
                                                   
                                                   NSURL *url = [NSURL URLWithString:appModel.service.account.serverURL];
                                                   NSString *server = url.host;
                                                   if (url.port.intValue != 80) {
                                                       server = [NSString stringWithFormat:@"%@:%d", server, url.port.intValue];
                                                   }
                                                   
                                                   NSString *launchURLString = nil;
                                                   
                                                   if (versionID == nil) {
                                                       launchURLString = [NSString stringWithFormat:@"adox://open/%@/%@", server, documentID];
                                                   } else {
                                                       launchURLString = [NSString stringWithFormat:@"adox://open/%@/%@/%d", server, documentID, [versionID intValue]];
                                                   }
                                                   
                                                   LogInfo(@"Processing as %@", launchURLString);
                                                   
                                                   [ADXAppModel instance].pendingURLSchemeRequest = [[ADXURLSchemeRequest alloc] initWithURL:launchURLString];
                                                   
                                                   [[NSNotificationCenter defaultCenter] postNotificationName:kADXURLSchemeRequestNotification object:nil];
                                               }
                                           }];
}


@end
