//
//  ADXFolderTableViewController.m
//  DesktopReader
//
//  Created by Steve Tibbett on 14/08/12.
//  Copyright (c) 2012 ActivDox. All rights reserved.
//

#import "ADXFolderTableViewController.h"
#import "ADXAppDelegate.h"
#import "ADXAppModel.h"

@interface ADXFolderTableViewController ()
@property (strong) IBOutlet NSArrayController *arrayController;
@property (strong, nonatomic) NSString *searchString;
@end

@implementation ADXFolderTableViewController

- (void)loadView {
    [super loadView];
    [self.tableView setTarget:self];
    [self.tableView setDoubleAction:@selector(doubleAction:)];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(objectsDidChange:)
                                                 name:NSManagedObjectContextObjectsDidChangeNotification
                                               object:[ADXAppDelegate instance].managedObjectContext];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)reloadData {
    [self.tableView reloadData];
}

- (void)objectsDidChange:(NSNotification *)notification
{
    // TODO: Better would be to intelligently refresh the view, not reload it
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        [self reloadData];
    }];
}

- (void)doubleAction:(id)sender {
    NSInteger rowNumber = [self.tableView clickedRow];
    ADXDocument *document = [self.arrayController.arrangedObjects objectAtIndex:rowNumber];
    [self.documentBrowserDelegate openDocument:document];
}

- (void)tableViewSelectionDidChange:(NSNotification *)notification {
    NSInteger rowNumber = [self.tableView selectedRow];
    if (rowNumber != -1) {
        ADXDocument *document = [self.arrayController.arrangedObjects objectAtIndex:rowNumber];
        [self.documentBrowserDelegate documentBrowserSelectionChanged:document];
    }
}

- (void)findString:(NSString *)string {
    self.searchString = string;
    self.arrayController.filterPredicate = [ADXAppModel filterPredicateForSearchString:string];
}
@end
